﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using System.IO;
using System.Data;

namespace iTECH.Library
{
    public class LogWriter
    {
        string _dir;
        string _fileName;

        public LogWriter(string fileName)
        {
            _dir = ConfigurationManager.AppSettings["LogDirectory"];
            if (string.IsNullOrEmpty(_dir))
            {
                _dir = System.IO.Path.GetTempPath();
            }
            _fileName = _dir + fileName;
        }

        public void AppendLog(string dataToAppend)
        {
            try
            {
                // Preparing Log Message
                string strMessage = string.Format("[{0}]:\n\t{1}\n", DateTime.Now, dataToAppend);
                // Writing Log File
                System.IO.File.AppendAllText(_fileName, strMessage);
            }
            catch (Exception ex)
            {
                //if (blnITECHLog)
                //{
                //    MessageBox.Show(("Error Occured while writing Log File " + LogFile), gblStrAppName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                //    blnITECHLog = false;
                //}
            }
        }        
    }

    public class SqlExecutionLogger
    {
        public DateTime ExecutionStartTime { get; set; }
        public DateTime ExecutionEndTime { get; set; }
        public System.Data.IDbCommand SqlCommand { get; set; }

        public void Write()
        {
            try
            {
                LogWriter lw = new LogWriter(string.Format("itech_sql_execution_log_{00}_{01}_{02}.log", DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day));
                string logMessage = string.Format("\r\nExecution Start========================{0}========================\r\n", this.ExecutionStartTime);
                logMessage += string.Format("Command:\r\n\t{0}\r\n", this.SqlCommand.CommandText);
                logMessage += string.Format("Parameters:\r\n");
                foreach (IDataParameter item in this.SqlCommand.Parameters)
                {
                    logMessage += string.Format("\t\t{0}:\t{1}\r\n", item.ParameterName, item.Value);
                }
                TimeSpan timeToExecute = this.ExecutionEndTime.Subtract(this.ExecutionStartTime);
                logMessage += string.Format("\r\nExecution End========================{0}========================[Total: {1} ms.]\r\n\r\n", this.ExecutionEndTime, timeToExecute.TotalMilliseconds);
                lw.AppendLog(logMessage);
            }
            catch
            {

            }
        }        
    }
}
