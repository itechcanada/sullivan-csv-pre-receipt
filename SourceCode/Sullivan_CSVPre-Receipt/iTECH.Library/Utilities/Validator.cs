﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace iTECH.Library.Utilities
{
    public static class Validator
    {

        #region Methods

        #region Public

        /// <summary>
        /// Validates the string argument is not null or empty string.
        /// </summary>
        /// <param name="argument">The argument.</param>
        /// <param name="argumentName">Name of the argument.</param>
        public static void ValidateStringArgumentIsNotNullOrEmptyString(string argument, string argumentName)
        {
            if (string.IsNullOrEmpty(argument))
            {
                throw new ArgumentException("ArgumentException", argumentName);
            }
        }

        /// <summary>
        /// Validates the argument is not null.
        /// </summary>
        /// <param name="argument">The argument.</param>
        /// <param name="argumentName">Name of the argument.</param>
        public static void ValidateArgumentIsNotNull(object argument, string argumentName)
        {
            if (argument == null)
            {
                throw new ArgumentNullException(argumentName, "ArgumentNullException");
            }
        }

        /// <summary>
        /// Validates the object is not null.
        /// </summary>
        /// <param name="obj">The obj.</param>
        /// <param name="objectName">Name of the object.</param>
        public static void ValidateObjectIsNotNull(object obj, string objectName)
        {
            if (obj == null)
            {
                throw new InvalidOperationException(string.Format("ObjectNullExceptionMessage::{0}", objectName));
            }
        }

        /// <summary>
        /// Validates the type of the object.
        /// </summary>
        /// <param name="obj">The obj.</param>
        /// <param name="type">The type.</param>
        public static void ValidateObjectType(object obj, Type type)
        {
            if (obj.GetType() != type)
            {
                throw new ArgumentException(string.Format("ObjectTypeExceptionMessage::{0}", type, obj.GetType()));
            }
        }

        /// <summary>
        /// Validates the enum is defined.
        /// </summary>
        /// <param name="type">The type.</param>
        /// <param name="argument">The argument.</param>
        public static void ValidateEnumIsDefined(Type type, string argument)
        {
            if (!Enum.IsDefined(type, argument))
            {
                throw new ArgumentOutOfRangeException(
                  string.Format("EnumDefinitionExceptionMessage::Type:{0}::Arg:{1}", type, argument));
            }
        }

        /// <summary>
        /// Validates the integer is greater than zero.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <param name="argumentName">Name of the argument.</param>
        public static void ValidateIntegerIsGreaterThanZero(int value, string argumentName)
        {
            if (value <= 0)
            {
                throw new ArgumentException(string.Format("ArgumentGreaterThanZeroExceptionMessage::{0}", argumentName));
            }
        }

        /// <summary>
        /// Validates the argument is serializable.
        /// </summary>
        /// <param name="obj">The obj.</param>
        /// <param name="argumentName">Name of the argument.</param>
        public static void ValidateArgumentIsSerializable(object obj, string argumentName)
        {
            if (!obj.GetType().IsSerializable)
            {
                throw new ArgumentOutOfRangeException(argumentName, string.Format("ArgumentMustBeSerializableExceptionMessage::{0}", obj.GetType().Name));
            }
        }

        #endregion

        #endregion

    }
}
