﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Text;
using System.Security.Cryptography;

namespace iTECH.Library.Utilities
{
    public class EncryptionUtility
    {
        private const string cryptoKey = "cryptoKey";

        // The Initialization Vector for the DES encryption routine
        private static readonly byte[] IV =
            new byte[8] { 240, 3, 45, 29, 0, 76, 173, 59 };

        public static string Encrypt<T>(T obj) {
            string serializeData = Newtonsoft.Json.JsonConvert.SerializeObject(obj, Newtonsoft.Json.Formatting.None);
            try
            {
                return Encrypt(serializeData);
            }
            catch 
            {
                //If encryption failed due to some security exception then by default it will just return serialized data base 64 string
                return Convert.ToBase64String(UTF8Encoding.ASCII.GetBytes(serializeData));
            }           
        }

        public static T Decrypt<T>(string strEncData) {        
            strEncData = Decrypt(strEncData);

            try
            {
                T obj = (T)Newtonsoft.Json.JsonConvert.DeserializeObject<T>(strEncData);
                return obj;
            }
            catch 
            {
                //If decryption failed due to some security exception then by default it will just return deserialized object from base 64 string
                T obj = (T)Newtonsoft.Json.JsonConvert.DeserializeObject<T>(UTF8Encoding.ASCII.GetString(Convert.FromBase64String(strEncData)));
                return obj;
            }                        
        }

        public static T DecryptDataFromQueryString<T>(string strEncData)
        {
            strEncData = strEncData.Replace(" ", "+");
            strEncData = Decrypt(strEncData);

            try
            {
                T obj = (T)Newtonsoft.Json.JsonConvert.DeserializeObject<T>(strEncData);
                return obj;
            }
            catch
            {
                //If decryption failed due to some security exception then by default it will just return deserialized object from base 64 string
                T obj = (T)Newtonsoft.Json.JsonConvert.DeserializeObject<T>(UTF8Encoding.ASCII.GetString(Convert.FromBase64String(strEncData)));
                return obj;
            }             
        }

        

        /// <summary>
        /// Encrypts provided string parameter
        /// </summary>
        public static string Encrypt(string s)
        {
            if (s == null || s.Length == 0) return string.Empty;

            string result = string.Empty;

            try
            {
                byte[] buffer = Encoding.ASCII.GetBytes(s);

                TripleDESCryptoServiceProvider des =
                    new TripleDESCryptoServiceProvider();

                MD5CryptoServiceProvider MD5 =
                    new MD5CryptoServiceProvider();

                des.Key =
                    MD5.ComputeHash(ASCIIEncoding.ASCII.GetBytes(cryptoKey));

                des.IV = IV;
                result = Convert.ToBase64String(
                    des.CreateEncryptor().TransformFinalBlock(
                        buffer, 0, buffer.Length));
            }
            catch
            {
                throw;
            }

            return result;
        }

        /// <summary>
        /// Decrypts provided string parameter
        /// </summary>
        public static string Decrypt(string s)
        {
            if (s == null || s.Length == 0) return string.Empty;

            string result = string.Empty;

            try
            {
                byte[] buffer = Convert.FromBase64String(s);

                TripleDESCryptoServiceProvider des =
                    new TripleDESCryptoServiceProvider();

                MD5CryptoServiceProvider MD5 =
                    new MD5CryptoServiceProvider();

                des.Key =
                    MD5.ComputeHash(ASCIIEncoding.ASCII.GetBytes(cryptoKey));

                des.IV = IV;

                result = Encoding.ASCII.GetString(
                    des.CreateDecryptor().TransformFinalBlock(
                    buffer, 0, buffer.Length));
            }
            catch
            {
                throw;
            }

            return result;
        }
    }
}
