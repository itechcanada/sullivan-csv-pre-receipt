﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Net.Mail;
using System.Configuration;
using System.Threading;
using System.Data;
using System.Text;

using iMysql = iTECH.Library.DataAccess.MySql;
using iMsSql = iTECH.Library.DataAccess.MsSql;

namespace iTECH.Library.Utilities
{
    internal class EmailManager
    {
        private MailMessage _emailMessage;
        private bool _isHtml = false;

        public EmailManager() {
                   
        }

        public EmailManager(bool isHtml)
        {
            _isHtml = isHtml;           
        }

        public bool SendEmail(string subject, string body, string fromAddress, string toAddress) {            
            try
            {
                _emailMessage = new MailMessage(fromAddress, toAddress, subject, body);
                 if (EmailSettings.IsAsynchronousMail)
                 {
                     Thread thread = new Thread(new ThreadStart(AsyncMail));
                     thread.Start();
                 }
                 else
                 {
                     AsyncMail();
                 }

                 return true;
            }
            catch 
            {
                
                throw;
            }
        }

        public bool SendEmail(string subject, string body, MailAddress fromAddress, MailAddress toAddress)
        {
            try
            {
                _emailMessage = new MailMessage(fromAddress, toAddress);
                _emailMessage.Subject = subject;
                _emailMessage.Body = body;
                if (EmailSettings.IsAsynchronousMail)
                {
                    Thread thread = new Thread(new ThreadStart(AsyncMail));
                    thread.Start();
                }
                else
                {
                    AsyncMail();
                }

                return true;
            }
            catch
            {

                throw;
            }
        }

        public bool SendEmail(string subject, string body, MailAddress fromAddress, List<MailAddress> toAddresses)
        {
            try
            {
                _emailMessage = new MailMessage();
                _emailMessage.From = fromAddress;
                foreach (MailAddress item in toAddresses)
                {
                    _emailMessage.To.Add(item);
                }
                _emailMessage.Subject = subject;
                _emailMessage.Body = body;
                if (EmailSettings.IsAsynchronousMail)
                {
                    Thread thread = new Thread(new ThreadStart(AsyncMail));
                    thread.Start();
                }
                else
                {
                    AsyncMail();
                }

                return true;
            }
            catch
            {

                throw;
            }
        }

        public bool SendEmail(string subject, string body, string fromAddress, string toAddress, string attahcedFilePath)
        {
            try
            {
                _emailMessage = new MailMessage(fromAddress, toAddress, subject, body);
                _emailMessage.Attachments.Add(new Attachment(attahcedFilePath));
                if (EmailSettings.IsAsynchronousMail)
                {
                    Thread thread = new Thread(new ThreadStart(AsyncMail));
                    thread.Start();
                }
                else
                {
                    AsyncMail();
                }

                return true;
            }
            catch
            {

                throw;
            }
        }

        private void AsyncMail()
        {
            try
            {
                _emailMessage.IsBodyHtml = _isHtml;

                SmtpClient emailClient = new SmtpClient(EmailSettings.SMTPServer);
                emailClient.Host = EmailSettings.SMTPServer;
                emailClient.UseDefaultCredentials = EmailSettings.UseDefaultCredentials;
                emailClient.Port = EmailSettings.SMTPPort;
                if (EmailSettings.UseDefaultCredentials)
                {
                    emailClient.Credentials = new System.Net.NetworkCredential(EmailSettings.SMTPUserEmail, EmailSettings.SMTPPassword);
                }
                emailClient.Send(_emailMessage);
            }
            catch
            {
                throw;
            }
        }
        
    }

    internal class EmailSettings
    {
        public static string SMTPServer
        {
            get
            {
                return ConfigurationManager.AppSettings["SMTPServer"];
            }
        }
        public static int SMTPPort
        {
            get
            {
                if (ConfigurationManager.AppSettings["SMTPPort"] != null)
                {
                    int id = 25;
                    if (int.TryParse(ConfigurationManager.AppSettings["SMTPPort"], out id))
                    {
                        return id;
                    }
                    return 25;
                }
                return 25;
            }
        }
        public static string SMTPUserEmail
        {
            get
            {
                if (ConfigurationManager.AppSettings["SMTPUserEmail"] != null)
                {
                    return ConfigurationManager.AppSettings["SMTPUserEmail"];
                }
                return string.Empty;
            }
        }
        public static string SMTPPassword
        {
            get
            {
                if (ConfigurationManager.AppSettings["SMTPPassword"] != null)
                {
                    return ConfigurationManager.AppSettings["SMTPPassword"];
                }
                return string.Empty;
            }
        }

        public static bool IsAsynchronousMail
        {
            get
            {
                if (ConfigurationManager.AppSettings["IsAsynchronousMail"] != null)
                {
                    return ConfigurationManager.AppSettings["IsAsynchronousMail"].ToLower() == "yes";
                }
                return false;
            }
        }

        public static bool UseDefaultCredentials
        {
            get
            {
                if (ConfigurationManager.AppSettings["SMTPUseDefaultCredentials"] != null)
                {
                    return ConfigurationManager.AppSettings["SMTPUseDefaultCredentials"].ToLower() == "yes";
                }
                return false;
            }
        }
        public static string ErrorEmailFrom
        {
            get
            {
                if (ConfigurationManager.AppSettings["ErrorEmailFrom"] != null)
                {
                    return ConfigurationManager.AppSettings["ErrorEmailFrom"];
                }
                return string.Empty;
            }
        }
        public static string ErrorEmailTo
        {
            get
            {
                if (ConfigurationManager.AppSettings["ErrorEmailTo"] != null)
                {
                    return ConfigurationManager.AppSettings["ErrorEmailTo"];
                }
                return string.Empty;
            }
        }
        public static string ErrorSubject
        {
            get
            {
                if (ConfigurationManager.AppSettings["ErrorSubject"] != null)
                {
                    return ConfigurationManager.AppSettings["ErrorSubject"];
                }
                return "Error Email";
            }
        }
    }

    public class EmailHelper
    {
        public static bool SendEmail(string fromAddress, string toAddress, string message, string sub, string attachedFilePath)
        {
            try
            {
                EmailManager manager = new EmailManager(true);
                return manager.SendEmail(sub, message, fromAddress, toAddress, attachedFilePath);
            }
            catch
            {
                throw;
            }
        }

        public static bool SendEmail(string fromAddress, string toAddress, string message, string sub, bool isHtml)
        {
            try
            {                
                EmailManager manager = new EmailManager(isHtml);
                return manager.SendEmail(sub, message, fromAddress, toAddress);
            }
            catch
            {

                throw;
            }
        }

        public static bool SendEmail(string fromAddress, string fromName, string toAddress, string toName, string message, string sub, bool isHtml)
        {            
            try
            {               
                EmailManager manager = new EmailManager(isHtml);
                return manager.SendEmail(sub, message, new MailAddress(fromAddress, fromName), new MailAddress(toAddress, toName));
            }
            catch
            {

                throw;
            }
        }

        public static bool SendEmail(MailAddress from, List<MailAddress> toList, string message, string sub, bool isHtml)
        {            
            try
            {                
                EmailManager manager = new EmailManager(isHtml);
                return manager.SendEmail(sub, message, from, toList);
            }
            catch
            {
                throw;
            }
        }

        public static bool SendErrorMail(string message)
        {           
            try
            {                                
                EmailManager manager = new EmailManager(true);
                return manager.SendEmail(EmailSettings.ErrorSubject, message, EmailSettings.ErrorEmailFrom, EmailSettings.ErrorEmailTo);
            }
            catch
            {

                throw;
            }
        }

        /// <summary>
        /// Source of help: http://www.ahmedblog.com/Blogs/ShowBlog.aspx?Id=2dc7e9e7-92b4-48cc-a7d1-d6535e5554ab
        /// 
        /// </summary>
        /// <param name="from"></param>
        /// <param name="to"></param>
        /// <param name="sub">Character lenght limit is 65</param>
        /// <param name="body"></param>
        /// <param name="location"></param>
        /// <param name="starUtctDate"></param>
        /// <param name="endUtcDate"></param>
        /// <returns></returns>
        public static bool SendAppointment(string from, string to, string body, string sub, string location, DateTime starUtctDate, DateTime endUtcDate)
        {
            try
            {
                SmtpClient sc = new SmtpClient(EmailSettings.SMTPServer);
                sc.Host = EmailSettings.SMTPServer;
                sc.UseDefaultCredentials = EmailSettings.UseDefaultCredentials;
                if (sc.UseDefaultCredentials )
                {
                    sc.Credentials = new System.Net.NetworkCredential(EmailSettings.SMTPUserEmail, EmailSettings.SMTPPassword);
                }
                sc.Port = EmailSettings.SMTPPort;

                MailMessage msg = new MailMessage();
                msg.From = new MailAddress(from);
                msg.To.Add(new MailAddress(to));
                msg.Subject = sub;
                msg.Body = body;

                StringBuilder str = new StringBuilder();
                str.AppendLine("BEGIN:VCALENDAR");
                str.AppendLine("PRODID:-//Appointment");
                str.AppendLine("VERSION:2.0");
                str.AppendLine("METHOD:REQUEST");
                str.AppendLine("BEGIN:VEVENT");
                str.AppendLine(string.Format("DTSTART:{0:yyyyMMddTHHmmssZ}", starUtctDate));
                str.AppendLine(string.Format("DTSTAMP:{0:yyyyMMddTHHmmssZ}", DateTime.UtcNow));
                str.AppendLine(string.Format("DTEND:{0:yyyyMMddTHHmmssZ}", endUtcDate));
                str.AppendLine(string.Format("LOCATION: {0}", location));
                str.AppendLine(string.Format("UID:{0}", Guid.NewGuid()));
                str.AppendLine(string.Format("DESCRIPTION:{0}", BusinessUtility.StripHtml(msg.Body)));
                str.AppendLine(string.Format("X-ALT-DESC;FMTTYPE=text/html:{0}", msg.Body));               
                str.AppendLine(string.Format("SUMMARY:{0}", msg.Subject));
                str.AppendLine(string.Format("ORGANIZER:MAILTO:{0}", msg.From.Address));

                str.AppendLine(string.Format("ATTENDEE;CN=\"{0}\";RSVP=TRUE:mailto:{1}", msg.To[0].DisplayName, msg.To[0].Address));

                str.AppendLine("BEGIN:VALARM");
                str.AppendLine("TRIGGER:-PT15M");
                str.AppendLine("ACTION:DISPLAY");
                str.AppendLine("DESCRIPTION:Reminder");
                str.AppendLine("END:VALARM");
                str.AppendLine("END:VEVENT");
                str.AppendLine("END:VCALENDAR");
                System.Net.Mime.ContentType ct = new System.Net.Mime.ContentType("text/calendar");
                ct.Parameters.Add("method", "REQUEST");
                AlternateView avCal = AlternateView.CreateAlternateViewFromString(str.ToString(), ct);
                msg.AlternateViews.Add(avCal);
                AlternateView avBody = AlternateView.CreateAlternateViewFromString(msg.Body, new System.Net.Mime.ContentType("text/html"));
                msg.AlternateViews.Add(avBody);


                sc.Send(msg);
                return true;
            }
            catch
            {

                throw;
            }
        }
    }

    //public class EmailTemplate {
    //    private int _emailTemplateID;
    //    private string _emailTemplateName;
    //    private string _subjectFormat;
    //    private bool _isBodyHtml;
    //    private string _bodyFormat;
    //    private bool _isActive;
    //    private string _description;
    //    private bool _allowHtmlEncoding;
        
    //    private DatabaseProvider _provider;

    //    public bool IsBodyHtmlEncoded {
    //        get {
    //            return _allowHtmlEncoding;
    //        }
    //    }

    //    public bool IsActive
    //    {
    //        get { return _isActive; }
    //        set { _isActive = value; }
    //    }

    //    public string Description
    //    {
    //        get { return _description; }
    //        set { _description = value; }
    //    }


    //    public string EmailTemplateName
    //    {
    //        get { return _emailTemplateName; }
    //        set { _emailTemplateName = value; }
    //    }

    //    public string BodyFormat
    //    {
    //        get { return _bodyFormat; }
    //        set { _bodyFormat = value; }
    //    }

    //    public bool IsBodyHtml
    //    {
    //        get { return _isBodyHtml; }
    //        set { _isBodyHtml = value; }
    //    }

    //    public string SubjectFormat
    //    {
    //        get { return _subjectFormat; }
    //        set { _subjectFormat = value; }
    //    }        

    //    public int EmailTemplateID
    //    {
    //        get { return _emailTemplateID; }
    //        set { _emailTemplateID = value; }
    //    }

    //    public EmailTemplate(DatabaseProvider provider, bool allowHtmlEncoding) {
    //        this._provider = provider;

    //        this._bodyFormat = string.Empty;
    //        this._emailTemplateID = 0;
    //        this._emailTemplateName = string.Empty;
    //        this._isActive = true;
    //        this._isBodyHtml = false;
    //        this._allowHtmlEncoding = allowHtmlEncoding;
    //        this._subjectFormat = string.Empty;
    //    }

    //    public EmailTemplate(DatabaseProvider provider, string templateName, bool allowHtmlEncoding)
    //    {
    //        this._provider = provider;
    //        this._allowHtmlEncoding = allowHtmlEncoding;            
    //    }

    //    public void SaveTemplate() {
    //        switch (_provider)
    //        {
    //            case DatabaseProvider.MySql:
    //                break;
    //            case DatabaseProvider.MsSql:
    //                iMsSql.DbHelper msDbHelp = new iMsSql.DbHelper();
    //                List<System.Data.SqlClient.SqlParameter> p = new List<System.Data.SqlClient.SqlParameter>();
    //                p.Add(iMsSql.DbUtility.GetParameter("BodyFormat", _allowHtmlEncoding ? HttpUtility.HtmlEncode(this.BodyFormat) : this.BodyFormat, typeof(string)));
    //                p.Add(iMsSql.DbUtility.GetParameter("EmailTemplateName", this.EmailTemplateName, typeof(string)));
    //                p.Add(iMsSql.DbUtility.GetParameter("IsActive", this.IsActive, typeof(bool)));
    //                p.Add(iMsSql.DbUtility.GetParameter("IsBodyHtml", this.IsBodyHtml, typeof(bool)));
    //                p.Add(iMsSql.DbUtility.GetParameter("AllowHtmlEncoding", this._allowHtmlEncoding, typeof(bool)));
    //                p.Add(iMsSql.DbUtility.GetParameter("SubjectFormat", this.SubjectFormat, typeof(string)));
    //                p.Add(iMsSql.DbUtility.GetParameter("Description", this.Description, typeof(string)));
    //                p.Add(iMsSql.DbUtility.GetParameter("EmailTemplateID", this.EmailTemplateID, typeof(string)));

    //                try
    //                {
    //                    object scalarData = msDbHelp.GetValue("cmn_SaveEmailTemplate", CommandType.StoredProcedure, p.ToArray());
    //                    this.EmailTemplateID = BusinessUtility.GetInt(scalarData);
    //                }
    //                catch
    //                {
    //                    throw;
    //                }
    //                finally {
    //                    msDbHelp.CloseDatabaseConnection();
    //                }

    //                break;                
    //        }
    //    }

    //    public void PopulateTemplate(string templateName) {
    //        switch (_provider)
    //        {
    //            case DatabaseProvider.MySql:
    //                break;
    //            case DatabaseProvider.MsSql:
    //                iMsSql.DbHelper msDbHelp = new iMsSql.DbHelper();
    //                List<System.Data.SqlClient.SqlParameter> p = new List<System.Data.SqlClient.SqlParameter>();                    
    //                p.Add(iMsSql.DbUtility.GetParameter("EmailTemplateName", templateName, typeof(string)));
    //                System.Data.SqlClient.SqlDataReader dr = null;
    //                try
    //                {
    //                    dr = msDbHelp.GetDataReader("cmn_GetEmailTemplate", CommandType.StoredProcedure, p.ToArray());
    //                    if (dr.Read()) {
    //                        this._allowHtmlEncoding = BusinessUtility.GetBool(dr["AllowHtmlEncoding"]);
    //                        this.BodyFormat = this.IsBodyHtmlEncoded ? HttpUtility.HtmlDecode(BusinessUtility.GetString(dr["BodyFormat"])) : BusinessUtility.GetString(dr["BodyFormat"]);
    //                        this.EmailTemplateID = BusinessUtility.GetInt(dr["EmailTemplateID"]);
    //                        this.EmailTemplateName = BusinessUtility.GetString(dr["EmailTemplateName"]);
    //                        this.IsActive = BusinessUtility.GetBool(dr["IsActive"]);
    //                        this.IsBodyHtml = BusinessUtility.GetBool(dr["IsBodyHtml"]);
    //                        this.Description = BusinessUtility.GetString(dr["Description"]);                            
    //                        this.SubjectFormat = BusinessUtility.GetString(dr["SubjectFormat"]);                            
    //                    }
    //                }
    //                catch
    //                {
    //                    throw;
    //                }
    //                finally
    //                {
    //                    msDbHelp.CloseDatabaseConnection(dr);
    //                }

    //                break;
    //        }
    //    }


    //    #region Serialization Methods

    //    /// <summary>
    //    /// News from XML.
    //    /// </summary>
    //    /// <param name="xml">The XML.</param>
    //    /// <returns></returns>
    //    public object NewFromXml(string xml)
    //    {            
    //        return SerializerUtil.DeserializeObject(xml, typeof(EmailTemplate).AssemblyQualifiedName);
    //    }

    //    /// <summary>
    //    /// Toes the XML.
    //    /// </summary>
    //    /// <returns></returns>
    //    public string ToXml()
    //    {            
    //        return SerializerUtil.SerializeObject(this, typeof(EmailTemplate));
    //    }

    //    #endregion
    //}

    
}