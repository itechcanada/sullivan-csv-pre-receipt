﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Globalization;
using System.Text.RegularExpressions;

namespace iTECH.Library.Utilities
{
    public class StringUtil
    {
        public static string GetJoinedString(string separator, int[] arr) {
            string[] strArr = Array.ConvertAll(arr, new Converter<int, string>(IntToString));
            return string.Join(separator, strArr);
        }

        public static string GetCleneJoinedString(string separator, string strCommaJoined) {
            string[] arr = strCommaJoined.Split(separator.ToCharArray()[0]);
            List<string> lResult = new List<string>();
            foreach (var item in arr)
            {
                if (!string.IsNullOrEmpty(item.Trim())) {
                    lResult.Add(item);
                }
            }
            return string.Join(separator, lResult.ToArray());
        }

        public static int[] GetArrayFromJoindString(string separator, string strCommaJoined) {
            string[] arr = strCommaJoined.Split(separator.ToCharArray()[0]);
            List<int> lResult = new List<int>();
            foreach (var item in arr)
            {
                lResult.Add(StringToInt(item));
            }
            return lResult.ToArray();
        }

        public static int StringToInt(string strVal) {
            int i = 0;
            int.TryParse(strVal, out i);
            return i;
        }

        public static string IntToString(int o) {
            return Convert.ToString(o);
        }

        public static string RemoveSpecialCharacters(string str)
        {
            str = str.Replace(" ", "_");
            return Regex.Replace(str, "[^a-zA-Z0-9_.]+", "", RegexOptions.Compiled);
        }

        public static string ToTitleCase(string str) {
            var textInfo = new CultureInfo("en-US").TextInfo;
            return textInfo.ToTitleCase(str);
        }
    }
}
