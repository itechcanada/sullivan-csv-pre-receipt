﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Globalization;
using System.Text.RegularExpressions;

namespace iTECH.Library.Utilities
{
    public class BusinessUtility
    {
        private static Random _random;

        #region [Public Members]
        /// <summary>
        /// Method to get string returned from database object
        /// </summary>
        /// <param name="o"></param>
        /// <returns></returns>
        public static string GetString(object o)
        {
            return Convert.ToString(o).Trim();
        }

        /// <summary>
        /// Method to get Int32 returned from database object
        /// </summary>
        /// <param name="o"></param>
        /// <returns></returns>
        public static int GetInt(object o)
        {
            if (o != DBNull.Value)
                return Convert.ToInt32(o);
            return 0;
        }

        // <summary>
        /// Method to get Int64 returned from database object
        /// </summary>
        /// <param name="o"></param>
        /// <returns></returns>
        public static long GetLong(object o)
        {
            if (o != DBNull.Value)
                return Convert.ToInt64(o);
            return 0;
        }

        /// <summary>
        /// Method to get Int32 converted from string.
        /// Major use in Presentation Logc.
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static int GetInt(string value)
        {
            int val = 0;
            int.TryParse(value, out val);
            return val;
        }

        /// <summary>
        /// Method to get Int64 converted from string.        
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static long GetLong(string value)
        {
            long val = 0;
            long.TryParse(value, out val);
            return val;
        }

        public static byte[] GetBytes(object o) {
            if (o != DBNull.Value)
                return (byte[])o;
            return new List<byte>().ToArray();
        }


        /// <summary>
        /// Method To get boolean value returned from database
        /// </summary>
        /// <param name="o"></param>
        /// <returns></returns>
        public static bool GetBool(object o)
        {
            if (o != DBNull.Value)
                return Convert.ToBoolean(o);
            return false;
        }

        /// <summary>
        /// Method to get Boolean converted from string.
        /// Major use in Presentation Logc.
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static bool GetBool(string value)
        {
            return Convert.ToBoolean(value);
        }

        /// <summary>
        /// Method to get double returned from database.
        /// </summary>
        /// <param name="o"></param>
        /// <returns></returns>
        public static double GetDouble(object o)
        {
            if (o != DBNull.Value)
                return Convert.ToDouble(o);
            return 0;
        }

        /// <summary>
        /// Method to get double converted from string
        /// Major use in Presentation Logc.
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static double GetDouble(string value)
        {
            double val = 0;
            if (!string.IsNullOrEmpty(value)) {
                if (double.TryParse(value, out val) == false) //Try with default case
                {
                    if (double.TryParse(value.Replace('.', ','), out val) == false) { //try for inverient culture
                        double.TryParse(value.Replace(',', '.'), out val);
                    }
                }
            }           
            return val;
        }

        /// <summary>
        /// Method to get float returned from database.
        /// </summary>
        /// <param name="o"></param>
        /// <returns></returns>
        public static float GetFloat(object o)
        {
            if (o != DBNull.Value)
                return Convert.ToSingle(o);
            return 0;
        }

        /// <summary>
        /// Method to get float converted from string
        /// Major use in Presentation Logc.
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static float GetFloat(string value)
        {
            float val = 0;            
            if (!string.IsNullOrEmpty(value))
            {
                if (float.TryParse(value, out val) == false) //Try with default case
                {
                    if (float.TryParse(value.Replace('.', ','), out val) == false)
                    { //try for inverient culture
                        float.TryParse(value.Replace(',', '.'), out val);
                    }
                }
            }   
            return val;
        }

        /// <summary>
        /// Method to get decimal value returned from database.
        /// </summary>
        /// <param name="o"></param>
        /// <returns></returns>
        public static decimal GetDecimal(object o)
        {
            if (o != DBNull.Value)
                return Convert.ToDecimal(o);
            return 0;
        }

        /// <summary>
        /// Method to get decimal value converted from string
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static decimal GetDecimal(string value)
        {
            decimal val = 0;
            //decimal.TryParse(value, out val);
            if (!string.IsNullOrEmpty(value))
            {
                if (decimal.TryParse(value, out val) == false) //Try with default case
                {
                    if (decimal.TryParse(value.Replace('.', ','), out val) == false)
                    { //try for inverient culture
                        decimal.TryParse(value.Replace(',', '.'), out val);
                    }
                }
            }  
            return val;
        }

        /// <summary>
        /// Method to get datetime value returned from database.
        /// </summary>
        /// <param name="o"></param>
        /// <returns></returns>
        public static DateTime GetDateTime(object o)
        {
            if (o != DBNull.Value)
                return Convert.ToDateTime(o);
            return DateTime.MinValue;
        }

        /// <summary>
        /// Method to get returned datetime value converted from string.
        /// Major use in Presentation Logc.
        /// </summary>
        /// <param name="value"></param>
        /// <param name="format"></param>
        /// <returns></returns>
        public static DateTime GetDateTime(string value, DateFormat format)
        {
            DateTime dtVal;
            string f = "MM/dd/yyyy";
            switch (format)
            {
                case DateFormat.ddMMyyyy:
                    f = "dd/MM/yyyy";
                    break;
                case DateFormat.MMddyyyy:
                    f = "MM/dd/yyyy";
                    break;
                case DateFormat.yyyyMMdd:
                    f = "yyyy/MM/dd";
                    break;
                case DateFormat.MMddyyyyHHmmss:
                    f = "MM/dd/yyyy HH:mm:ss";
                    break;
                case DateFormat.MMddyyyyhhmmtt:
                    f = "MM/dd/yyyy hh:mm tt";
                    break;
                case DateFormat.MySqlSupported:
                    f = "yyyy-MM-dd hh:mm:ss";
                    break;
                default:
                    f = "MM/dd/yyyy";
                    break;
            }

            bool success = DateTime.TryParseExact(value, f, DateTimeFormatInfo.InvariantInfo, DateTimeStyles.None, out dtVal);
            return success ? dtVal : DateTime.MinValue;
        }

        public static DateTime GetDateTime(string value, string format) {
            DateTime dtVal;
            bool success = DateTime.TryParseExact(value, format, DateTimeFormatInfo.InvariantInfo, DateTimeStyles.None, out dtVal);
            return success ? dtVal : DateTime.MinValue;
        }

        /// <summary>
        /// Method to get parameter name prefixed with '@'.
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public static string GetParameterName(string name)
        {
            return name.Contains("@") ? name : "@" + name;
        }

        /// <summary>
        /// Method to get currency string from decimal value.
        /// </summary>
        /// <param name="val"></param>
        /// <param name="cultureName">Specified current culture name e.g. en-US</param>
        /// <returns></returns>
        public static string GetCurrencyString(decimal val, string cultureName)
        {
            return string.Format(new System.Globalization.CultureInfo(cultureName), "{0:C}", val);
        }

        /// <summary>
        /// Method to get decimal value from currency formated string.
        /// </summary>
        /// <param name="formatedValue"></param>
        /// <param name="cultureName">Specified current culture name e.g. en-US</param>
        /// <returns></returns>
        public static decimal GetDecimalValueFromCurrencyString(string formatedValue, string cultureName)
        {
            decimal moneyValue = decimal.Zero;
            decimal.TryParse(formatedValue, System.Globalization.NumberStyles.Currency, new System.Globalization.CultureInfo(cultureName), out moneyValue);
            return moneyValue;
        }

        /// <summary>
        /// Method to get currency string from double value.
        /// </summary>
        /// <param name="val"></param>
        /// <param name="cultureName">Specified current culture name e.g. en-US</param>
        /// <returns></returns>
        public static string GetCurrencyString(double val, string cultureName)
        {
            return string.Format(new System.Globalization.CultureInfo(cultureName), "{0:C}", val);
        }

        /// <summary>
        /// Method to get double value from currency formated string.
        /// </summary>
        /// <param name="formatedValue"></param>
        /// <param name="cultureName">Specified current culture name e.g. en-US</param>
        /// <returns></returns>
        public static double GetDoubleValueFromCurrencyString(string formatedValue, string cultureName)
        {
            double moneyValue = 0;
            double.TryParse(formatedValue, System.Globalization.NumberStyles.Currency, new System.Globalization.CultureInfo(cultureName), out moneyValue);
            return moneyValue;
        }

        /// <summary>
        /// Method to get formated date string.
        /// Date separator / (slash) and time sepatator : (colon).
        /// </summary>
        /// <param name="dt"></param>
        /// <param name="format"></param>
        /// <returns></returns>
        public static string GetDateTimeString(DateTime dt, DateFormat format)
        {
            if (dt == DateTime.MinValue) return string.Empty;
            StringBuilder sbDate = new StringBuilder();
            switch (format)
            {
                case DateFormat.ddMMyyyy:
                    return sbDate.Append(dt.Day.ToString("00"))
                            .Append("/")
                            .Append(dt.Month.ToString("00"))
                            .Append("/")
                            .Append(dt.Year)
                            .ToString();
                case DateFormat.MMddyyyy:
                    return sbDate.Append(dt.Month.ToString("00"))
                            .Append("/")
                            .Append(dt.Day.ToString("00"))
                            .Append("/")
                            .Append(dt.Year)
                            .ToString();
                case DateFormat.yyyyMMdd:
                    return sbDate.Append(dt.Year)
                            .Append("/")
                            .Append(dt.Month.ToString("00"))
                            .Append("/")
                            .Append(dt.Day.ToString("00"))
                            .ToString();
                case DateFormat.MMddyyyyHHmmss:
                    return sbDate.Append(dt.Month.ToString("00"))
                            .Append("/")
                            .Append(dt.Day.ToString("00"))
                            .Append("/")
                            .Append(dt.Year)
                            .Append(" ")
                            .Append(dt.Hour.ToString("00"))
                            .Append(":")
                            .Append(dt.Minute.ToString("00"))
                            .Append(":")
                            .Append(dt.Second.ToString("00"))
                            .ToString();
                case DateFormat.MMddyyyyhhmmtt:
                    sbDate.Append(dt.Month.ToString("00"))
                           .Append("/")
                           .Append(dt.Day.ToString("00"))
                           .Append("/")
                           .Append(dt.Year)
                           .Append(" ");
                    if (dt.Hour > 12)
                    {
                        sbDate.Append((dt.Hour % 12).ToString("00"));
                    }
                    else
                    {
                        sbDate.Append(dt.Hour.ToString("00"));
                    }
                    sbDate.Append(":")
                           .Append(dt.Minute.ToString("00"))
                           .Append(" ");
                    if (dt.Hour > 12)
                    {
                        sbDate.Append("PM");
                    }
                    else
                    {
                        sbDate.Append("AM");
                    }
                    return sbDate.ToString();
                case DateFormat.MySqlSupported:
                    return dt.ToString("yyyy-MM-dd hh:mm:ss");
                    //return sbDate.Append(dt.Month.ToString("00"))
                    //       .Append("-")
                    //       .Append(dt.Day.ToString("00"))
                    //       .Append("-")
                    //       .Append(dt.Year)
                    //       .Append(" ")
                    //       .Append(dt.Hour.ToString("00"))
                    //       .Append(":")
                    //       .Append(dt.Minute.ToString("00"))
                    //       .Append(":")
                    //       .Append(dt.Second.ToString("00"))
                    //       .ToString();
                default:
                    return sbDate.Append(dt.Month.ToString("00"))
                            .Append("/")
                            .Append(dt.Day.ToString("00"))
                            .Append("/")
                            .Append(dt.Year)
                            .ToString();
            }
        }

        /// <summary>
        /// Method to get custom formated date string.
        /// </summary>
        /// <param name="dt"></param>
        /// <param name="customFormat">e.g. MMM dd, yyyy</param>
        /// <returns></returns>
        public static string GetDateTimeString(DateTime dt, string customFormat)
        {
            return dt.ToString(customFormat);
        }
        
        public static DateTime? GetDateTime(DateTime date)
        {
            DateTime? dt = null;
            if (date != DateTime.MinValue)
            {
                dt = date;
            }
            return dt;
        }

        /// <summary>
        /// Generates a random string.
        /// </summary>
        /// <param name="length">The length.</param>
        /// <returns></returns>
        public static string GenerateRandomString(int length, bool includeNumbers)
        {
            string chars = includeNumbers ? "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890" : "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            string randomString = string.Empty;
            if (length > 0)
            {
                Random rnd = new Random();
                for (int i = 0; i < length; i++)
                {
                    randomString += chars[CurrentRandom.Next(chars.Length)];
                }
            }
            return randomString;
        }        

        /// <summary>
        /// Generates a random number.
        /// </summary>
        /// <param name="length">The length.</param>
        /// <returns></returns>
        public static int GenerateRandomNumber(int length)
        {
            string chars = "1234567890";
            string randomString = string.Empty;
            if (length > 0)
            {
                Random rnd = new Random();
                for (int i = 0; i < length; i++)
                {
                    randomString += chars[CurrentRandom.Next(chars.Length)];
                }
            }
            int num = 0;
            int.TryParse(randomString, out num);
            return num;
        }

        /// <summary>
        /// Generates a 4 by 4 masked string.
        /// </summary>
        /// <returns></returns>
        public static string Generate4By4MaskedString()
        {
            string chars = "1234567890";
            string randomString = string.Empty;
            Random rnd = new Random();
            for (int i = 1; i <= 16; i++)
            {
                randomString += chars[CurrentRandom.Next(chars.Length)];
                if (i < 16 && i % 4 == 0)
                {
                    randomString += "-";
                }
            }
            return randomString;
        }

        private static Random CurrentRandom
        {
            get {
                if (_random == null)
                {
                    _random = new Random();
                }
                return _random;
            }
        }

        /// <summary>
        /// Parses the camel cased string to proper.
        /// </summary>
        /// <param name="sIn">The s in.</param>
        /// <returns></returns>
        public static string ParseCamelToProper(string sIn)
        {
            char[] letters = sIn.ToCharArray();
            string sOut = "";
            foreach (char c in letters)
            {
                if (c.ToString() != c.ToString().ToLower())
                {
                    //it's uppercase, add a space
                    sOut += " " + c.ToString();
                }
                else
                {
                    sOut += c.ToString();
                }
            }
            return sOut;
        }

        /// <summary>
        /// Strips the HTML.
        /// </summary>
        /// <param name="htmlString">The HTML string.</param>
        /// <param name="htmlPlaceHolder">The HTML place holder.</param>
        /// <returns></returns>
        public static string StripHtml(string htmlString, string htmlPlaceHolder)
        {
            string pattern = @"<(.|\n)*?>";
            string sOut = Regex.Replace(htmlString, pattern, htmlPlaceHolder);
            sOut = sOut.Replace("&nbsp;", "");
            sOut = sOut.Replace("&amp;", "&");
            sOut = sOut.Replace("&gt;", ">");
            sOut = sOut.Replace("&lt;", "<");
            return sOut;
        }

        public static string StripHtml(string source)
        {
            string[] patterns = {
                                    @"<(.|\n)*?>", //general HTML tags
                                    @"<script.*?</script>" //script tags
                                };
            string stripped = source;
            foreach (string pattern in patterns)
            {
                stripped = System.Text.RegularExpressions.Regex.Replace(
                stripped, pattern, string.Empty);
            }
            return stripped;
        }

        /// <summary>
        /// Makes the HTTPS URL.
        /// </summary>
        /// <param name="url">The URL.</param>
        /// <returns></returns>
        public static string MakeHttpsUrl(string url)
        {
            UriBuilder secureUri = new UriBuilder(url);
            secureUri.Scheme = "https";
            secureUri.Port = -1;
            return secureUri.Uri.ToString();
        }

        #endregion
    }

    public enum DateFormat
    {
        /// <summary>
        /// dd/MM/yyyy
        /// </summary>
        ddMMyyyy,
        /// <summary>
        /// MM/dd/yyyy
        /// </summary>
        MMddyyyy,
        /// <summary>
        /// yyyy/MM/dd
        /// </summary>
        yyyyMMdd,
        /// <summary>
        /// MM/dd/yyyy HH:mm:ss
        /// </summary>
        MMddyyyyHHmmss,
        /// <summary>
        /// MM/dd/yyyy hh:mm tt (AM|PM)
        /// </summary>
        MMddyyyyhhmmtt,
        /// <summary>
        /// yyyy-MM-dd hh:mm:ss
        /// </summary>
        MySqlSupported
    }
}

/*****************String Format for DateTime******************
 * // create date time 2008-03-09 16:05:07.123
 * DateTime dt = new DateTime(2008, 3, 9, 16, 5, 7, 123);
 * String.Format("{0:y yy yyy yyyy}", dt);  // "8 08 008 2008"   year
 * String.Format("{0:M MM MMM MMMM}", dt);  // "3 03 Mar March"  month
 * String.Format("{0:d dd ddd dddd}", dt);  // "9 09 Sun Sunday" day
 * String.Format("{0:h hh H HH}",     dt);  // "4 04 16 16"      hour 12/24
 * String.Format("{0:m mm}",          dt);  // "5 05"            minute
 * String.Format("{0:s ss}",          dt);  // "7 07"            second
 * String.Format("{0:f ff fff ffff}", dt);  // "1 12 123 1230"   sec.fraction
 * String.Format("{0:F FF FFF FFFF}", dt);  // "1 12 123 123"    without zeroes
 * String.Format("{0:t tt}",          dt);  // "P PM"            A.M. or P.M.
 * String.Format("{0:z zz zzz}",      dt);  // "-6 -06 -06:00"   time zone
 * 
 * 
 * // date separator in german culture is "." (so "/" changes to ".")
 * String.Format("{0:d/M/yyyy HH:mm:ss}", dt); // "9/3/2008 16:05:07" - english (en-US)
 * String.Format("{0:d/M/yyyy HH:mm:ss}", dt); // "9.3.2008 16:05:07" - german (de-DE)
 * 
 * // month/day numbers without/with leading zeroes
 * String.Format("{0:M/d/yyyy}", dt);            // "3/9/2008"
 * String.Format("{0:MM/dd/yyyy}", dt);          // "03/09/2008"

 * // day/month names
 * String.Format("{0:ddd, MMM d, yyyy}", dt);    // "Sun, Mar 9, 2008"
 * String.Format("{0:dddd, MMMM d, yyyy}", dt);  // "Sunday, March 9, 2008"

 * // two/four digit year
 * String.Format("{0:MM/dd/yy}", dt);            // "03/09/08"
 * String.Format("{0:MM/dd/yyyy}", dt);          // "03/09/2008"
 * 
 * For mor help: http://www.csharp-examples.net/string-format-datetime/
**************************************************************/


/*****************String Format for Double******************
 * // just two decimal places
 * String.Format("{0:0.00}", 123.4567);      // "123.46"
 * String.Format("{0:0.00}", 123.4);         // "123.40"
 * String.Format("{0:0.00}", 123.0);         // "123.00"
 * 
 * // max. two decimal places
 * String.Format("{0:0.##}", 123.4567);      // "123.46"
 * String.Format("{0:0.##}", 123.4);         // "123.4"
 * String.Format("{0:0.##}", 123.0);         // "123"
 * 
 * // at least two digits before decimal point
 * String.Format("{0:00.0}", 123.4567);      // "123.5"
 * String.Format("{0:00.0}", 23.4567);       // "23.5"
 * String.Format("{0:00.0}", 3.4567);        // "03.5"
 * String.Format("{0:00.0}", -3.4567);       // "-03.5"
 * 
 * String.Format("{0:0,0.0}", 12345.67);     // "12,345.7"
 * String.Format("{0:0,0}", 12345.67);       // "12,346"
 * 
 * String.Format("{0:0.0}", 0.0);            // "0.0"
 * String.Format("{0:0.#}", 0.0);            // "0"
 * String.Format("{0:#.0}", 0.0);            // ".0"
 * String.Format("{0:#.#}", 0.0);            // ""
 * 
 * //Align numbers with spaces
 * String.Format("{0,10:0.0}", 123.4567);    // "     123.5"
 * String.Format("{0,-10:0.0}", 123.4567);   // "123.5     "
 * String.Format("{0,10:0.0}", -123.4567);   // "    -123.5"
 * String.Format("{0,-10:0.0}", -123.4567);  // "-123.5    "
 * For mor help: http://www.csharp-examples.net/string-format-double/
 * 
 * http://msdn.microsoft.com/en-us/library/dwhawy9k.aspx
**************************************************************/