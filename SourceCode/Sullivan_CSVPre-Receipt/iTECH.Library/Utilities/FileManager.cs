﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;

using FreeImageAPI;

namespace iTECH.Library.Utilities
{
    public class FileManager
    {
        #region Image File Methods
        private static string[] _acceptedImageFileTypes = new string[] { ".jpg", ".jpeg", ".jpe", ".gif", ".bmp", ".png" };

        /// <summary>
        /// Determines whether [is valid file type] [the specified extension].
        /// </summary>
        /// <param name="extension">The extension.</param>
        /// <returns>
        /// 	<c>true</c> if [is valid file type] [the specified extension]; otherwise, <c>false</c>.
        /// </returns>
        public static bool IsValidImageFileType(string extension)
        {
            Predicate<string> match = delegate(string extensionToMatch) { return string.Equals(extensionToMatch, extension, StringComparison.OrdinalIgnoreCase); };
            return Array.Exists(_acceptedImageFileTypes, match);
        }

        /// <summary>
        /// Determines whether [is valid file type] [the specified extension].
        /// </summary>
        /// <param name="extension">The extension.</param>
        /// <returns>
        /// 	<c>true</c> if [is valid file type] [the specified extension]; otherwise, <c>false</c>.
        /// </returns>
        public static bool IsValidImageFile(string fileName)
        {
            string extention = System.IO.Path.GetExtension(fileName);
            return IsValidImageFileType(extention);
        }

        public static bool UploadImage(HttpPostedFile postedFile, int tWidth, int tHeight, string targetAbsolutePath, string targetFileName)
        {
            return UploadImage(postedFile, targetAbsolutePath, targetFileName);
        }

        public static bool ResizeAndSave(HttpPostedFile postedImageFile, int tWidth, int tHeight, string targetAbsolutePath, string targetFileName)
        {
            NativeImageTool tool = new NativeImageTool();

            using (System.Drawing.Bitmap source = new System.Drawing.Bitmap(postedImageFile.InputStream))
            {
                bool b = false;

                int width = source.Width;
                int height = source.Height;                

                int widthDiff = (width - tWidth); //how far off maxWidth?
                int heightDiff = (height - tHeight); //how far off maxHeight?

                //figure out which dimension is further outside the max size
                bool doWidthResize = (tWidth > 0 && width > tWidth && widthDiff > -1 && (widthDiff > heightDiff || tHeight.Equals(0)));
                bool doHeightResize = (tHeight > 0 && height > tHeight && heightDiff > -1 && (heightDiff > widthDiff || tWidth.Equals(0)));

                //only resize if the image is bigger than the max or where image is square, and the diffs are the same
                if (doWidthResize || doHeightResize || (width.Equals(height) && widthDiff.Equals(heightDiff)))
                {
                    int iStart;
                    Decimal divider;
                    if (doWidthResize)
                    {
                        iStart = width;
                        divider = Math.Abs((Decimal)iStart / (Decimal)tWidth);
                        width = tWidth;
                        height = (int)Math.Round((Decimal)(height / divider));
                    }
                    else
                    {
                        iStart = height;
                        divider = Math.Abs((Decimal)iStart / (Decimal)tHeight);
                        height = tHeight;
                        width = (int)Math.Round((Decimal)(width / divider));
                    }
                }

                PixelFormat format = source.PixelFormat;
                using (Bitmap bitmap = new Bitmap(width, height, format))
                {
                    using (Graphics graphics = Graphics.FromImage(bitmap))
                    {
                        graphics.CompositingMode = System.Drawing.Drawing2D.CompositingMode.SourceOver;
                        graphics.CompositingQuality = System.Drawing.Drawing2D.CompositingQuality.GammaCorrected;
                        graphics.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
                        graphics.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
                        graphics.DrawImage(source, 0, 0, width, height);
                        //bitmap.Save(HttpContext.Current.Server.MapPath(targetAbsolutePath) + targetFileName);
                        ByteArrayToFile(HttpContext.Current.Server.MapPath(targetAbsolutePath) + targetFileName, tool.Encode(bitmap, ImageFormat.Jpeg));
                        b = true;
                    }
                }

                return b;
            }
        }

        public static bool UploadImage(HttpPostedFile postedFile, string targetAbsolutePath, string targetFileName)
        {
            postedFile.SaveAs(HttpContext.Current.Server.MapPath(targetAbsolutePath) + targetFileName);
            return true;
        }

        public static string GetImageInfoHtml(string imagePath)
        {
            try
            {
                string mapPath = HttpContext.Current.Server.MapPath(imagePath);
                using (Bitmap _bmpRepresentation = new Bitmap(mapPath, false))
                {
                    System.IO.FileInfo fInfo = new System.IO.FileInfo(mapPath);
                    decimal byteSize = Math.Round((decimal)fInfo.Length / 1024M * 100M) * .01M;
                    string suffix = "KB";
                    if (byteSize > 1000)
                    {
                        byteSize = Math.Round((decimal)byteSize * .001M * 100M) * .01M;
                        suffix = "MB";
                    }

                    ImageFormat bmpFormat = _bmpRepresentation.RawFormat;
                    string strFormat = "Unidentified Format";

                    if (bmpFormat.Equals(ImageFormat.Bmp)) strFormat = "BMP";
                    else if (bmpFormat.Equals(ImageFormat.Emf)) strFormat = "EMF";
                    else if (bmpFormat.Equals(ImageFormat.Exif)) strFormat = "EXIF";
                    else if (bmpFormat.Equals(ImageFormat.Gif)) strFormat = "GIF";
                    else if (bmpFormat.Equals(ImageFormat.Icon)) strFormat = "Icon";
                    else if (bmpFormat.Equals(ImageFormat.Jpeg)) strFormat = "JPEG";
                    else if (bmpFormat.Equals(ImageFormat.MemoryBmp)) strFormat = "MemoryBMP";
                    else if (bmpFormat.Equals(ImageFormat.Png)) strFormat = "PNG";
                    else if (bmpFormat.Equals(ImageFormat.Tiff)) strFormat = "TIFF";
                    else if (bmpFormat.Equals(ImageFormat.Wmf)) strFormat = "WMF";

                    return string.Format("Item Type: {0}<br>Dimensions: {1} x {2}<br>Size: {3}<br>Date Created: {4}", strFormat, _bmpRepresentation.Width, _bmpRepresentation.Height, string.Format("{0:0.##} {1}", byteSize, suffix), fInfo.CreationTime);
                }
            }
            catch
            {
                return "Item Type: Unidentified<br>Dimensions: Unidentified<br>Size: Unidentified";
            }
        }

        #endregion

        #region Common Methods
        /// <summary>
        /// Get Randonm FileName without na
        /// </summary>
        /// <returns></returns>
        public static string GetRandomFileName()
        {
            return BusinessUtility.GenerateRandomString(8, false);
            //return System.Web.Security.FormsAuthentication.HashPasswordForStoringInConfigFile(string.Format("{0}{1}", DateTime.Now.ToString("yyyMMddHHmmss"), System.Guid.NewGuid()), "MD5").ToLower();
        }

        /// <summary>
        /// Get Randonm FileName With File extention
        /// </summary>
        /// <returns></returns>
        public static string GetRandomFileName(string originalFileName)
        {
            string extention = System.IO.Path.GetExtension(originalFileName);
            return BusinessUtility.GenerateRandomString(8, false) + extention;
            //return System.Web.Security.FormsAuthentication.HashPasswordForStoringInConfigFile(string.Format("{0}{1}", DateTime.Now.ToString("yyyMMddHHmmss"), System.Guid.NewGuid()), "MD5").ToLower();
        }

        public static bool UploadAnyFile(HttpPostedFile postedFile, string targetAbsolutePath, string targetFileName)
        {
            postedFile.SaveAs(HttpContext.Current.Server.MapPath(targetAbsolutePath) + targetFileName);
            return true; 
        }

        public static string GetOtherFileInfo(string filePath)
        {
            System.IO.FileInfo fInfo = null;
            try
            {
                string mapPath = HttpContext.Current.Server.MapPath(filePath);
                fInfo = new System.IO.FileInfo(mapPath);
                decimal byteSize = Math.Round((decimal)fInfo.Length / 1024M * 100M) * .01M;
                string suffix = "KB";
                if (byteSize > 1000)
                {
                    byteSize = Math.Round((decimal)byteSize * .001M * 100M) * .01M;
                    suffix = "MB";
                }
                return string.Format("Item Type: {0}<br>Size: {1}<br>Date Created: {2}", fInfo.Extension.ToUpper(), string.Format("{0:0.##} {1}", byteSize, suffix), fInfo.CreationTime);
            }
            catch
            {
                return "Item Type: Unidentified<br>Size: Unidentified";
            }
            finally
            {
                fInfo = null;
            }
        }

        /// <summary>
        /// Function to save byte array to a file
        /// </summary>
        /// <param name="fileName">File name to save byte array</param>
        /// <param name="_ByteArray">Byte array to save to external file</param>
        /// <returns>Return true if byte array save successfully, if not return false</returns>
        private static bool ByteArrayToFile(string fileName, byte[] arrByte)
        {
            try
            {
                // Open file for reading
                using (System.IO.FileStream fStream = new System.IO.FileStream(fileName, System.IO.FileMode.Create, System.IO.FileAccess.Write))
                {
                    // Writes a block of bytes to this stream using data from a byte array.
                    fStream.Write(arrByte, 0, arrByte.Length);
                    return true;
                }                
            }
            catch (Exception ex)
            {
                // error occured, return false
                return false;
            }            
        }
        #endregion
    }

    public class ImageUtility
    {
        #region Image Tool Related Methods

        public static byte[] Encode(Bitmap source, ImageFormat imageFormat)
        {
            NativeImageTool tool = new NativeImageTool();

            FIBITMAP dib = FreeImageAPI.FreeImage.CreateFromBitmap(source);

            if (dib.IsNull)
            {
                return tool.Encode(source, imageFormat);
            }

            using (MemoryStream ms = new MemoryStream())
            {
                if (!FreeImageAPI.FreeImage.SaveToStream(
                    ref dib, ms,
                    ImageUtility.ConvertToFreeImageFormat(imageFormat),
                    ImageUtility.ConvertToFreeImageSaveFlags(imageFormat),
                    FREE_IMAGE_COLOR_DEPTH.FICD_AUTO,
                    true))
                {
                    return tool.Encode(source, imageFormat);
                }

                return ms.ToArray();
            }
        }


        public static FREE_IMAGE_FORMAT ConvertToFreeImageFormat(ImageFormat imageFormat)
        {
            if (imageFormat == ImageFormat.Png)
            {
                return FREE_IMAGE_FORMAT.FIF_PNG;
            }

            if (imageFormat == ImageFormat.Gif)
            {
                return FREE_IMAGE_FORMAT.FIF_GIF;
            }

            if (imageFormat == ImageFormat.Jpeg)
            {
                return FREE_IMAGE_FORMAT.FIF_JPEG;
            }

            if (imageFormat == ImageFormat.Tiff)
            {
                return FREE_IMAGE_FORMAT.FIF_TIFF;
            }

            return FREE_IMAGE_FORMAT.FIF_PNG;
        }

        public static FREE_IMAGE_SAVE_FLAGS ConvertToFreeImageSaveFlags(ImageFormat imageFormat)
        {
            if (imageFormat == ImageFormat.Png)
            {
                return FREE_IMAGE_SAVE_FLAGS.PNG_Z_BEST_COMPRESSION;
            }

            if (imageFormat == ImageFormat.Gif)
            {
                return FREE_IMAGE_SAVE_FLAGS.DEFAULT;
            }

            if (imageFormat == ImageFormat.Jpeg)
            {
                return FREE_IMAGE_SAVE_FLAGS.JPEG_QUALITYGOOD;
            }

            if (imageFormat == ImageFormat.Tiff)
            {
                return FREE_IMAGE_SAVE_FLAGS.TIFF_LZW;
            }

            return FREE_IMAGE_SAVE_FLAGS.PNG_Z_BEST_COMPRESSION;
        }
        #endregion
    }

    public class FileComparer : System.Collections.IComparer
    {
        public enum CompareBy
        {
            Name /* a-z */,
            LastWriteTime /* oldest to newest */,
            CreationTime  /* oldest to newest */,
            LastAccessTime /* oldest to newest */,
            FileSize /* smallest first */
        }
        // default comparison
        int _CompareBy = (int)CompareBy.Name;

        public FileComparer()
        {
        }

        public FileComparer(CompareBy compareBy)
        {
            _CompareBy = (int)compareBy;
        }

        int System.Collections.IComparer.Compare(object x, object y)
        {
            int output = 0;
            System.IO.FileInfo file1 = new System.IO.FileInfo(x.ToString());
            System.IO.FileInfo file2 = new System.IO.FileInfo(y.ToString());
            switch (_CompareBy)
            {
                case (int)CompareBy.LastWriteTime:
                    output = DateTime.Compare(file1.LastWriteTime, file2.LastWriteTime);
                    break;
                case (int)CompareBy.CreationTime:
                    output = DateTime.Compare(file1.CreationTime, file2.CreationTime);
                    break;
                case (int)CompareBy.LastAccessTime:
                    output = DateTime.Compare(file1.LastAccessTime, file2.LastAccessTime);
                    break;
                case (int)CompareBy.FileSize:
                    output = Convert.ToInt32(file1.Length - file2.Length);
                    break;
                case (int)CompareBy.Name:
                default:
                    output = (new System.Collections.CaseInsensitiveComparer()).Compare(file1.Name, file2.Name);
                    break;
            }
            return output;
        }
    }
}
