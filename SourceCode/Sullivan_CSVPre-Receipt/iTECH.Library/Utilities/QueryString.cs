﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Threading;
using System.Globalization;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Web.Security;
using System.IO;


namespace iTECH.Library.Utilities
{
    public  class QueryString
    {
        public static string GetModifiedUrl(string key, string val) {
            var query = HttpUtility.ParseQueryString(HttpContext.Current.Request.Url.Query);
            query[key] = val; // Add or replace param
            return HttpContext.Current.Request.Url.AbsolutePath + "?" + query;
        }
    }
}
