﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace iTECH.Library.Utilities
{
    public static class RecurserHelper
    {
        /// <summary>
        /// this method check if a control or one of its children
        /// has the type of the types given by recursivity
        /// </summary>
        /// <param name="control">control to check</param>
        /// <param name="type">type to find</param>
        /// <returns>the first occurrence of control which has one of the given types
        /// </returns>
        public static Control ContainsControlType(Control control, params Type[] types)
        {
            // may be we could loop through the controls first and then the types
            // to gain some speed of process
            // but I wanted to ensure the same process for any control, 
            // for the first one like its children 
            //(cause the first one could have a given type)
            foreach (Type type in types)
            {
                if (control.GetType().Equals(type))
                    return control;
                else
                    // begin recursivity
                    foreach (Control ctrl in control.Controls)
                    {
                        Control tmpCtrl = ContainsControlType(ctrl, type);
                        if (tmpCtrl != null)
                            return tmpCtrl;
                    }
            }
            // if no controls had the given type in the current control we return false
            return null;
        }

        /// <summary>
        /// Check if there is more than 0 links controls in a control or 
        /// if this control is a link
        /// </summary>
        /// <param name="control">control to check in</param>
        /// <returns>true if there is any links</returns>
        public static bool ContainsLink(Control control)
        {
            bool ret = false;
            // search a link in the cell
            Control ctrl = ContainsControlType(control, typeof(HyperLink),
            typeof(LinkButton), typeof(DataBoundLiteralControl));
            // if a control is returned, we have to check the case 
            // of the literal which could contain no links
            if (ctrl != null)
            {
                if (ctrl.GetType().Equals(typeof(DataBoundLiteralControl)))
                {
                    DataBoundLiteralControl dblc = (DataBoundLiteralControl)ctrl;
                    // here I check if the text contains a href or onclick attribute
                    // I assume that there this text should not be used to be displayed
                    if (dblc.Text.Contains("href") || dblc.Text.Contains("onclick"))
                        ret = true;
                }
                else ret = true;
            }
            return ret;
        }
    }
}
