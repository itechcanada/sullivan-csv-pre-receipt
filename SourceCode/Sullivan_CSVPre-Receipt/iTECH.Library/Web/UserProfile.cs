﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

using iTECH.Library.Utilities;

namespace iTECH.Library.Web
{
    public class UserProfile
    {        
        public static object UserID
        {
            get
            {
                return HttpContext.Current.Session[UserProfileSessionKeys.UserID] != null ? (object)HttpContext.Current.Session[UserProfileSessionKeys.UserID] : null;
            }
        }

        public static string LoginID
        {
            get
            {
                return HttpContext.Current.Session[UserProfileSessionKeys.LoginID] != null ? (string)HttpContext.Current.Session[UserProfileSessionKeys.LoginID] : string.Empty;
            }
        }

        
        public static string DisplayName
        {
            get
            {
                return HttpContext.Current.Session[UserProfileSessionKeys.DisplayName] != null ? (string)HttpContext.Current.Session[UserProfileSessionKeys.DisplayName] : string.Empty;
            }
        }

        
        public static string Roles
        {
            get
            {
                return HttpContext.Current.Session[UserProfileSessionKeys.Roles] != null ? (string)HttpContext.Current.Session[UserProfileSessionKeys.Roles] : string.Empty;
            }
        }

        public static bool IsAuthenticated
        {
            get
            {
                return UserProfile.UserID != null;
            }
        }

        public static SessionStoreProfile SessionStore
        {
            get {
                return (SessionStoreProfile)HttpContext.Current.Session[UserProfileSessionKeys.SessionStore];
            }
        }

        /// <summary>
        /// Method to Initialize Current User login profile.
        /// </summary>
        /// <param name="userid">Required to pass authentication</param>
        /// <param name="loginid">Optional</param>
        /// <param name="displayName">Optional</param>
        /// <param name="roles">Optional</param>
        public static void SignIn(object userid, string loginid, string displayName, string roles)
        {
            HttpContext.Current.Session[UserProfileSessionKeys.UserID] = userid;
            HttpContext.Current.Session[UserProfileSessionKeys.LoginID] = loginid;
            HttpContext.Current.Session[UserProfileSessionKeys.DisplayName] = displayName;
            HttpContext.Current.Session[UserProfileSessionKeys.Roles] = roles;
            HttpContext.Current.Session[UserProfileSessionKeys.SessionStore] = new SessionStoreProfile(UserProfileSessionKeys.SessionStoreItemCollection);
        }

        /// <summary>
        /// Method to Initialize Current User login profile.
        /// </summary>
        /// <param name="userid"></param>
        /// <param name="displayName"></param>
        /// <param name="roles"></param>
        public static void SignIn(string userid, string displayName, string roles)
        {
            UserProfile.SignIn(userid, userid, displayName, roles);
        }

        public static void SignIn(string userid, string roles)
        {
            UserProfile.SignIn(userid, userid, userid, roles);
        }

        public static void SignIn(string userid)
        {
            UserProfile.SignIn(userid, userid, userid, string.Empty);
        }

        /// <summary>
        /// Method to clear all users data from session.
        /// </summary>
        public static void SignOut()
        {
            HttpContext.Current.Session.Remove(UserProfileSessionKeys.LoginID);
            HttpContext.Current.Session.Remove(UserProfileSessionKeys.UserID);
            HttpContext.Current.Session.Remove(UserProfileSessionKeys.DisplayName);
            HttpContext.Current.Session.Remove(UserProfileSessionKeys.Roles);
            HttpContext.Current.Session.Remove(UserProfileSessionKeys.SessionStore);
            HttpContext.Current.Session.Remove(UserProfileSessionKeys.SessionStoreItemCollection);
        }

        public static bool IsInRole(string roleName)
        {
            string[] arrRoles = UserProfile.Roles.Split(',');
            foreach (string item in arrRoles)
            {
                if (item.Equals(roleName))
                {
                    return true;
                }
            }
            return false;
        }

        public static bool IsInRole(int roleID)
        {
            return IsInRole(roleID.ToString());
        }
    }

    public class SessionStoreProfile
    {
        string _sessionKey = string.Empty;

        public SessionStoreProfile(string sessionKey)
        {            
            _sessionKey = sessionKey;
            HttpContext.Current.Session[_sessionKey] = new SessionDataCollection();
        }

        public void Add(string key, object value)
        {
            SessionDataCollection dCol = (SessionDataCollection)HttpContext.Current.Session[_sessionKey];
            dCol.Add(key, value);
            HttpContext.Current.Session[_sessionKey] = dCol;
        }

        public void Remove(string key)
        {
            SessionDataCollection dCol = (SessionDataCollection)HttpContext.Current.Session[_sessionKey];
            dCol.Remove(key);
            HttpContext.Current.Session[_sessionKey] = dCol;
        }

        public void Clear()
        {
            SessionDataCollection dCol = (SessionDataCollection)HttpContext.Current.Session[_sessionKey];
            dCol.Clear();
            HttpContext.Current.Session[_sessionKey] = dCol;
        }

        public object this[string key]
        {
            get
            {
                SessionDataCollection dCol = (SessionDataCollection)HttpContext.Current.Session[_sessionKey];
                return dCol[key];
            }
            set {
                SessionDataCollection dCol = (SessionDataCollection)HttpContext.Current.Session[_sessionKey];
                dCol[key] = value;                
                HttpContext.Current.Session[_sessionKey] = dCol;
            }
        }

        public bool ContainsKey(string key)
        {
            SessionDataCollection dCol = (SessionDataCollection)HttpContext.Current.Session[_sessionKey];
            return dCol.ContainsKey(key);            
        }

        public bool ContainsValue(object value)
        {
            SessionDataCollection dCol = (SessionDataCollection)HttpContext.Current.Session[_sessionKey];
            return dCol.ContainsValue(value);     
        }

        public int Count
        {
            get {
                SessionDataCollection dCol = (SessionDataCollection)HttpContext.Current.Session[_sessionKey];
                return dCol.Count;
            }
        }
    }

   public class SessionDataCollection : Dictionary<string, object>
    {
        new internal void Add(string key, object value)
        {
            base.Add(key, value);
        }

        new internal void Remove(string key)
        {
            base.Remove(key);
        }

        new internal void Clear()
        {
            base.Clear();
        }

        new public object this[string key]
        {
            get { return base[key];}
            internal set { base[key] = value; }
        }
    }

   public struct UserProfileSessionKeys {
       public const string UserID = "_cup_UserID";
       public const string LoginID = "_cup_LoginID";
       public const string DisplayName = "User_LoginID";
       internal const string Roles = "_cup_Roles";
       internal const string SessionStore = "_cup_SessionStore";
       internal const string SessionStoreItemCollection = "_cup_SessionStore_item_collection";
   }
}
