﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace iTECH.Library
{
    //NOTE: The intent of TransactionType needs to be kept in sync with the Ids 
    //in dashCommerce_Store_TransactionTypeDescriptor - dashCommerce_Store_TransactionTypeDescriptor 
    //may hold localized values for reports, display grids, etc.
    public enum TransactionType : int
    {
        Authorization = 1,
        Charge = 2,
        Refund = 3
    }

    public enum CreditCardType
    {
        MasterCard,
        VISA,
        Amex,
        Discover,
        PayPal,
        Maestro,
        Solo
    }
}
