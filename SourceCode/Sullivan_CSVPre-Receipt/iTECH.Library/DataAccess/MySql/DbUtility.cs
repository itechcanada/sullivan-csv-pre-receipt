﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using MySql.Data.MySqlClient;
using iTECH.Library.Utilities;

namespace iTECH.Library.DataAccess.MySql
{
    public class DbUtility
    {
        public static MySqlParameter GetParameter(string name, object value, MyDbType t)
        {
            switch (t)
            {
                case MyDbType.Int:
                    return GetIntParameter(name, BusinessUtility.GetInt(value));
                case MyDbType.String:
                    return GetStringParameter(name, BusinessUtility.GetString(value));                    
                case MyDbType.Double:
                    return GetDoubleParameter(name, BusinessUtility.GetDouble(value));
                case MyDbType.Decimal:
                    return GetDecimalParameter(name, BusinessUtility.GetDecimal(value));
                case MyDbType.Float:
                    return GetFloatParameter(name, BusinessUtility.GetFloat(value));
                case MyDbType.DateTime:
                    return GetDateTimeParameter(name, BusinessUtility.GetDateTime((DateTime)value));
                default:
                    return new MySqlParameter(BusinessUtility.GetParameterName(name), value);
            }
        }

        public static MySqlParameter GetParameter(string name, object value, Type t)
        {
            if (t == typeof(string))
            {
                return GetStringParameter(name, BusinessUtility.GetString(value)); 
            }
            else if (t == typeof(int))
            {
                return GetIntParameter(name, BusinessUtility.GetInt(value));
            }
            else if (t == typeof(double))
            {
                return GetDoubleParameter(name, BusinessUtility.GetDouble(value));
            }
            else if (t == typeof(decimal))
            {
                return GetDecimalParameter(name, BusinessUtility.GetDecimal(value));
            }
            else if (t == typeof(float))
            {
                return GetFloatParameter(name, BusinessUtility.GetFloat(value));
            }
            else if (t == typeof(DateTime))
            {
                return GetDateTimeParameter(name, BusinessUtility.GetDateTime((DateTime)value));
            }
            else
            {
                return new MySqlParameter(BusinessUtility.GetParameterName(name), value);
            }
        }

        public static MySqlParameter GetStringParameter(string name, string value, bool convertEmptyStringToNull)
        {
            MySqlParameter p = new MySqlParameter(BusinessUtility.GetParameterName(name), MySqlDbType.String);
            p.Value = convertEmptyStringToNull && string.IsNullOrEmpty(value) ? DBNull.Value : (object)value;
            return p;
        }

        public static MySqlParameter GetStringParameter(string name, string value)
        {
            return GetStringParameter(name, value, true);
        }

        public static MySqlParameter GetIntParameter(string name, int? value)
        {
            MySqlParameter p = new MySqlParameter(BusinessUtility.GetParameterName(name), MySqlDbType.Int32);
            p.Value = !value.HasValue ? DBNull.Value : (object)value.Value;
            return p;
        }

        public static MySqlParameter GetDoubleParameter(string name, double? value)
        {
            MySqlParameter p = new MySqlParameter(BusinessUtility.GetParameterName(name), MySqlDbType.Double);
            p.Value = !value.HasValue ? DBNull.Value : (object)value.Value;
            return p;
        }

        public static MySqlParameter GetDecimalParameter(string name, decimal? value)
        {
            MySqlParameter p = new MySqlParameter(BusinessUtility.GetParameterName(name), MySqlDbType.Decimal);
            p.Value = !value.HasValue ? DBNull.Value : (object)value.Value;
            return p;
        }

        public static MySqlParameter GetFloatParameter(string name, float? value)
        {
            MySqlParameter p = new MySqlParameter(BusinessUtility.GetParameterName(name), MySqlDbType.Float);
            p.Value = !value.HasValue ? DBNull.Value : (object)value.Value;
            return p;
        }

        public static MySqlParameter GetDateTimeParameter(string name, DateTime? value)
        {
            MySqlParameter p = new MySqlParameter(BusinessUtility.GetParameterName(name), MySqlDbType.DateTime);
            p.Value = !value.HasValue || value.Value == DateTime.MinValue ? DBNull.Value : (object)value.Value;
            return p;
        }

        public static MySqlParameter GetObjectParameter(string name, bool value)
        {
            return new MySqlParameter(BusinessUtility.GetParameterName(name), value);
        }
    }

    public enum MyDbType { 
        Int,
        String,
        Double,
        Decimal,
        Float,
        DateTime,
        Boolean
    }
}
