﻿//  Created by Hitendra Malviya 
//  On: 26-Nov-2010
//  For: iTECH Canada Inc.
// Modified By: Hitendra
// Last Modified Date: 09-Jun-2011
using System;
using System.Xml;
using System.Collections;
using System.Collections.Generic;

using System.Data;
using MySql.Data;
using MySql.Data.MySqlClient;

namespace iTECH.Library.DataAccess.MySql
{
    /// <summary>
    /// The SqlHelper class is intended to encapsulate scalable best practices for 
    /// common uses of MySql.
    /// </summary>
    class MySqlHelper
    {
        /// <summary>
        /// Call ExecuteReader Method of SQlCommand Object.
        /// </summary>
        public static MySqlDataReader ExecuteReader(MySqlConnection con, string commandText, CommandType cmdType, MySqlParameter[] parameters)
        {  
            MySqlCommand cmd = new MySqlCommand();
            cmd.Connection = con;
            cmd.CommandType = cmdType;
            cmd.CommandText = commandText;            
            if (parameters != null)
            {
                foreach (MySqlParameter p in parameters)
                {
                    cmd.Parameters.Add(p);
                }
            }

            try
            {
                if (con.State != ConnectionState.Open) con.Open();
                SqlExecutionLogger log = new SqlExecutionLogger();
                log.SqlCommand = cmd;
                log.ExecutionStartTime = DateTime.Now;
                MySqlDataReader dr = cmd.ExecuteReader();
                log.ExecutionEndTime = DateTime.Now;
                log.Write();
                return dr;
            }
            catch
            {
                throw;
            }
        }


        /// <summary>
        /// Execute a MySqlCommand and returns no of rows affected.
        /// </summary>
        public static int ExecuteNonQuery(MySqlConnection con, string commandText, CommandType cmdType, MySqlParameter[] parameters, bool mustClose)
        {
            MySqlCommand cmd = new MySqlCommand();
            cmd.Connection = con;
            cmd.CommandType = cmdType;
            cmd.CommandText = commandText;

            if (parameters != null)
            {
                foreach (MySqlParameter p in parameters)
                {
                    cmd.Parameters.Add(p);
                }
            }

            try
            {
                if (con.State != ConnectionState.Open) con.Open();
                SqlExecutionLogger log = new SqlExecutionLogger();
                log.SqlCommand = cmd;
                log.ExecutionStartTime = DateTime.Now;
                int rowsEffected = cmd.ExecuteNonQuery();
                //return cmd.ExecuteNonQuery();
                log.ExecutionEndTime = DateTime.Now;
                log.Write();
                return rowsEffected;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (mustClose && con.State != ConnectionState.Closed)
                {
                    con.Close();
                }
            }
        }


        /// <summary>
        /// Execute a MySqlCommand (that returns a 1x1 resultset)
        /// </summary>
        public static object ExecuteScalar(MySqlConnection con, string commandText, CommandType cmdType, MySqlParameter[] parameters, bool mustClose)
        {
            MySqlCommand cmd = new MySqlCommand();
            cmd.Connection = con;
            cmd.CommandType = cmdType;
            cmd.CommandText = commandText;

            if (parameters != null)
            {
                foreach (MySqlParameter p in parameters)
                {
                    cmd.Parameters.Add(p);
                }
            }

            try
            {
                if (con.State != ConnectionState.Open) { con.Open(); }
                SqlExecutionLogger log = new SqlExecutionLogger();
                log.SqlCommand = cmd;
                log.ExecutionStartTime = DateTime.Now;                                
                object scalar = cmd.ExecuteScalar();
                log.ExecutionEndTime = DateTime.Now;
                log.Write();
                return scalar;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (mustClose && con.State != ConnectionState.Closed)
                {
                    con.Close();
                }
            }
        }

        /// <summary>
        /// To returns dataset from database.
        /// </summary>
        public static DataSet ExecuteDataset(MySqlConnection con, string commandText, CommandType cmdType, MySqlParameter[] parameters)
        {
            MySqlCommand cmd = new MySqlCommand();
            cmd.Connection = con;
            cmd.CommandType = cmdType;
            cmd.CommandText = commandText;
            MySqlScript scr = new MySqlScript();
            
            if (parameters != null)
            {
                foreach (MySqlParameter p in parameters)
                {
                    cmd.Parameters.Add(p);
                }
            }

            try
            {
                MySqlDataAdapter da = new MySqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                SqlExecutionLogger log = new SqlExecutionLogger();
                log.SqlCommand = cmd;
                log.ExecutionStartTime = DateTime.Now;  
                da.Fill(ds);
                log.ExecutionEndTime = DateTime.Now;
                log.Write();
                return ds;
            }
            catch
            {
                throw;
            }
        }



        /// <summary>
        /// Call ExecuteReader Method of SQlCommand Object.
        /// Pass transaction object with open coneection.
        /// before calling this method call 'BeginTransaction()' method of connection.
        /// </summary>
        public static MySqlDataReader ExecuteReader(MySqlTransaction trans, string commandText, CommandType cmdType, MySqlParameter[] parameters)
        {
            MySqlCommand cmd = new MySqlCommand();
            cmd.Connection = trans.Connection;
            cmd.Transaction = trans;
            cmd.CommandType = cmdType;
            cmd.CommandText = commandText;

            if (parameters != null)
            {
                foreach (MySqlParameter p in parameters)
                {
                    cmd.Parameters.Add(p);
                }
            }

            try
            {
                return cmd.ExecuteReader();
            }
            catch
            {
                throw;
            }
        }


        /// <summary>
        /// Execute a MySqlCommand and returns no of rows affected.
        /// Pass transaction object with open coneection.
        /// before calling this method call 'BeginTransaction()' method of connection.
        /// </summary>
        public static int ExecuteNonQuery(MySqlTransaction trans, string commandText, CommandType cmdType, MySqlParameter[] parameters)
        {
            MySqlCommand cmd = new MySqlCommand();
            cmd.Connection = trans.Connection;
            cmd.Transaction = trans;
            cmd.CommandType = cmdType;
            cmd.CommandText = commandText;

            if (parameters != null)
            {
                foreach (MySqlParameter p in parameters)
                {
                    cmd.Parameters.Add(p);
                }
            }

            try
            {
                return cmd.ExecuteNonQuery();
            }
            catch
            {
                throw;
            }
        }


        /// <summary>
        /// Execute a MySqlCommand (that returns a 1x1 resultset)
        /// Pass transaction object with open coneection.
        /// before calling this method call 'BeginTransaction()' method of connection.
        /// </summary>
        public static object ExecuteScalar(MySqlTransaction trans, string commandText, CommandType cmdType, MySqlParameter[] parameters)
        {
            MySqlCommand cmd = new MySqlCommand();
            cmd.Connection = trans.Connection;
            cmd.Transaction = trans;
            cmd.CommandType = cmdType;
            cmd.CommandText = commandText;

            if (parameters != null)
            {
                foreach (MySqlParameter p in parameters)
                {
                    cmd.Parameters.Add(p);
                }
            }

            try
            {
                return cmd.ExecuteScalar();
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// To returns dataset from database.
        /// Pass transaction object with open coneection.
        /// before calling this method call 'BeginTransaction()' method of connection.
        /// </summary>        
        public static DataSet ExecuteDataset(MySqlTransaction trans, string commandText, CommandType cmdType, MySqlParameter[] parameters)
        {
            MySqlCommand cmd = new MySqlCommand();
            cmd.Connection = trans.Connection;
            cmd.Transaction = trans;
            cmd.CommandType = cmdType;
            cmd.CommandText = commandText;

            if (parameters != null)
            {
                foreach (MySqlParameter p in parameters)
                {
                    cmd.Parameters.Add(p);
                }
            }

            try
            {
                MySqlDataAdapter da = new MySqlDataAdapter(cmd);
                DataSet ds = new DataSet();

                da.Fill(ds);
                return ds;
            }
            catch
            {
                throw;
            }
        }
    }   
}
