﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Data;
using System.Data.SqlClient;
using iTECH.Library.Utilities;

namespace iTECH.Library.DataAccess.MsSql
{
    public class DbUtility
    {
        public static SqlParameter GetParameter(string name, object value, Type t)
        {
            if (t == typeof(string))
            {
                return GetStringParameter(name, BusinessUtility.GetString(value));
            }
            else if (t == typeof(int))
            {
                return GetIntParameter(name, BusinessUtility.GetInt(value));
            }
            else if (t == typeof(double))
            {
                return GetDoubleParameter(name, BusinessUtility.GetDouble(value));
            }
            else if (t == typeof(decimal))
            {
                return GetDecimalParameter(name, BusinessUtility.GetDecimal(value));
            }
            else if (t == typeof(float))
            {
                return GetFloatParameter(name, BusinessUtility.GetFloat(value));
            }
            else if (t == typeof(DateTime))
            {
                return GetDateTimeParameter(name, BusinessUtility.GetDateTime((DateTime)value));
            }
            else
            {
                return new SqlParameter(BusinessUtility.GetParameterName(name), value);
            }
        }

        public static SqlParameter GetStringParameter(string name, string value, bool convertEmptyStringToNull)
        {
            object val = convertEmptyStringToNull && string.IsNullOrEmpty(value) ? DBNull.Value : (object)value;
            return new SqlParameter(BusinessUtility.GetParameterName(name), val);
        }

        public static SqlParameter GetStringParameter(string name, string value)
        {
            return GetStringParameter(name, value, true);
        }

        public static SqlParameter GetIntParameter(string name, int? value)
        {
            SqlParameter p = new SqlParameter(BusinessUtility.GetParameterName(name), SqlDbType.Int);
            p.Value = !value.HasValue ? DBNull.Value : (object)value.Value;
            return p;
        }

        public static SqlParameter GetDoubleParameter(string name, double? value)
        {
            SqlParameter p = new SqlParameter(BusinessUtility.GetParameterName(name), SqlDbType.Float);
            p.Value = !value.HasValue ? DBNull.Value : (object)value.Value;
            return p;
        }

        public static SqlParameter GetDecimalParameter(string name, decimal? value)
        {
            SqlParameter p = new SqlParameter(BusinessUtility.GetParameterName(name), SqlDbType.Decimal);
            p.Value = !value.HasValue ? DBNull.Value : (object)value.Value;
            return p;
        }

        public static SqlParameter GetFloatParameter(string name, float? value)
        {
            SqlParameter p = new SqlParameter(BusinessUtility.GetParameterName(name), SqlDbType.Float);
            p.Value = !value.HasValue ? DBNull.Value : (object)value.Value;
            return p;
        }

        public static SqlParameter GetDateTimeParameter(string name, DateTime? value)
        {
            SqlParameter p = new SqlParameter(BusinessUtility.GetParameterName(name), SqlDbType.DateTime);
            p.Value = !value.HasValue || value.Value == DateTime.MinValue ? DBNull.Value : (object)value.Value;
            return p;
        }

        public static SqlParameter GetObjectParameter(string name, bool value)
        {
            return new SqlParameter(BusinessUtility.GetParameterName(name), value);
        }
    }
}
