﻿//  Created by Hitendra Malviya 
//  On: 26-Nov-2010
//  For: iTECH Canada Inc.
// Modified By: Hitendra
// Last Modified Date: 09-Jun-2011
using System;
using System.Xml;
using System.Collections;

using System.Data;
using System.Data.Odbc;

namespace iTECH.Library.DataAccess.ODBC
{
    /// <summary>
    /// The SqlHelper class is intended to encapsulate scalable best practices for 
    /// common uses of Sql.
    /// </summary>
    class ODBCHelper
    {
        /// <summary>
        /// Call ExecuteReader Method of OdbcCommand Object.
        /// </summary>
        public static OdbcDataReader ExecuteReader(OdbcConnection con, string commandText, CommandType cmdType, OdbcParameter[] parameters)
        {  
            OdbcCommand cmd = new OdbcCommand();
            cmd.Connection = con;
            cmd.CommandType = cmdType;
            cmd.CommandText = commandText;

            if (parameters != null)
            {
                foreach (OdbcParameter p in parameters)
                {
                    cmd.Parameters.Add(p);
                }
            }

            try
            {
                if (con.State != ConnectionState.Open) con.Open();
                SqlExecutionLogger log = new SqlExecutionLogger();
                log.SqlCommand = cmd;
                log.ExecutionStartTime = DateTime.Now;
                cmd.CommandTimeout = 600;
                OdbcDataReader dr = cmd.ExecuteReader();
                log.ExecutionEndTime = DateTime.Now;
                log.Write();
                return dr;
            }
            catch
            {
                throw;
            }
        }


        /// <summary>
        /// Execute a OdbcCommand and returns no of rows affected.
        /// </summary>
        public static int ExecuteNonQuery(OdbcConnection con, string commandText, CommandType cmdType, bool mustClose)
        {
            OdbcCommand cmd = new OdbcCommand();
            cmd.Connection = con;
            cmd.CommandType = cmdType;
            cmd.CommandText = commandText;

            try
            {
                if (con.State != ConnectionState.Open) con.Open();
                SqlExecutionLogger log = new SqlExecutionLogger();
                log.SqlCommand = cmd;
                log.ExecutionStartTime = DateTime.Now;
                int rowsEffected = cmd.ExecuteNonQuery();
                log.ExecutionEndTime = DateTime.Now;
                log.Write();
                return rowsEffected;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (mustClose && con.State != ConnectionState.Closed)
                {
                    con.Close();
                }
            }
        }


        /// <summary>
        /// Execute a OdbcCommand (that returns a 1x1 resultset)
        /// </summary>
        public static object ExecuteScalar(OdbcConnection con, string commandText, CommandType cmdType, bool mustClose)
        {
            OdbcCommand cmd = new OdbcCommand();
            cmd.Connection = con;
            cmd.CommandType = cmdType;
            cmd.CommandText = commandText;

            try
            {
                if (con.State != ConnectionState.Open) { con.Open(); }
                SqlExecutionLogger log = new SqlExecutionLogger();
                log.SqlCommand = cmd;
                log.ExecutionStartTime = DateTime.Now;       
                object scalar = cmd.ExecuteScalar();
                log.ExecutionEndTime = DateTime.Now;
                log.Write();
                return scalar;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (mustClose && con.State != ConnectionState.Closed)
                {
                    con.Close();
                }
            }
        }

        /// <summary>
        /// To returns dataset from database.
        /// </summary>
        public static DataSet ExecuteDataset(OdbcConnection con, string commandText, CommandType cmdType, OdbcParameter[] parameters)
        {
            OdbcCommand cmd = new OdbcCommand();
            cmd.Connection = con;
            cmd.CommandType = cmdType;
            cmd.CommandText = commandText;

            if (parameters != null)
            {
                foreach (OdbcParameter p in parameters)
                {
                    cmd.Parameters.Add(p);
                }
            }


            try
            {
                OdbcDataAdapter da = new OdbcDataAdapter(cmd);
                DataSet ds = new DataSet();
                SqlExecutionLogger log = new SqlExecutionLogger();
                log.SqlCommand = cmd;
                log.ExecutionStartTime = DateTime.Now;
                da.Fill(ds);
                log.ExecutionEndTime = DateTime.Now;
                log.Write();
                return ds;
            }
            catch
            {
                throw;
            }
        }
    }
}
