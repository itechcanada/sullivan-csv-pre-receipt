﻿using System;
using System.Configuration;
using System.IO;
using System.Data;
using iTECH.Library.Utilities;
using System.Collections;
using SecureFTP._990;
using SecureFTP._214;
using ChoETL;

namespace SecureFTP
{
    class Program
    {
        private static Sftp sftp;
        private static string Processed_PATH = ConfigurationManager.AppSettings["Processed_PATH"];
        private static string Upload_PATH = ConfigurationManager.AppSettings["Upload_PATH"];
        private static string Unprocessed_PATH = ConfigurationManager.AppSettings["Unprocessed_PATH"];
        private static String targetFolder = ConfigurationManager.AppSettings["SFTP_TARGET_DIR"] == null ? "/home" : Convert.ToString(ConfigurationManager.AppSettings["SFTP_TARGET_DIR"]);

        private static string sFirstLineText = ConfigurationManager.AppSettings["FirstLineText"];
        private static string sLastLineText = ConfigurationManager.AppSettings["LastLineText"];

        static string m_sCusVenId = BusinessUtility.GetString(ConfigurationManager.AppSettings["CusVenId"]);
        private static string m_sSingleFilePath = ConfigurationManager.AppSettings["SingleFilePAth"];
        private static int m_iExecuteSingleFile = BusinessUtility.GetInt(ConfigurationManager.AppSettings["ExecuteSingleFile"]);

        static string m_sFileType = BusinessUtility.GetString(ConfigurationManager.AppSettings["FileType"]);
        private static string SourceFile_PATH = ConfigurationManager.AppSettings["NetworkSourceFilePath"];

        private static string DataDownloadDirectory = ConfigurationManager.AppSettings["DataDownloadDirectory"];

        static ArrayList m_lstFiles;

        private static string EdiFile = string.Empty;

        static int m_iFileId = 0;

        static Configuration oConfiguration = null;
        

        static void Main(string[] args)
        {
            string filetempname = string.Empty;
            string IsVendorFilter;      //to check vendors -- to process only selected vendorlist
            //Console.WriteLine("Processing file..");
            //TestCertificateAttachment();        //for testing - qds pdf attachment process only
            //return;
            try
            {
                ErrorLog.createLog("m_sFileType = " + m_sFileType);
                if (m_sFileType.Contains(Files.ExecutionFileType.EDI8561))
                {
                    Files oFiles1 = new Files();
                    oFiles1.UpdateReceipt();
                    InputOutput.CheckDirectoryAndCreate(Processed_PATH);
                    string[] fileEntries = Directory.GetFiles(SourceFile_PATH);
                    if (fileEntries.Length <= 0)
                    {
                        ErrorLog.createLog("No Customer own file available to process in directory : " + SourceFile_PATH);
                    }
                    foreach (string filename in fileEntries)
                    {
                        //rename file to process and keep backup of file (files is coming with same name)   2021/04/05  Sumit
                        filetempname = DataDownloadDirectory + @"\" + DateTime.Now.ToString("yyyyMMddHHmmss") + "_" + Path.GetFileName(filename);
                        File.Move(filename, filetempname);
                        m_sSingleFilePath = filetempname;

                        //m_sSingleFilePath = filename;
                        DataTable dtCustData = ExcelHelper.GetDataFromCSV(m_sSingleFilePath);       //use excel helper class instead of ChoETL library to support alphabet      2020/08/26  Sumit
                        //ChoCSVReader oChoCSVReader = new ChoCSVReader(m_sSingleFilePath);
                        //DataTable dtCustData = oChoCSVReader.AsDataTable();
                        //oChoCSVReader.Dispose();
                        if (dtCustData.Rows.Count > 0)
                        {
                            //string sDuns = Utility.GetConfigValue("CustomerOwnDunsNo");
                            string sDuns = BusinessUtility.GetString(dtCustData.Rows[0][0]);    //2020/05/14    to pick vendorduns from csv file
                            ErrorLog.createLog("Creating customer own pre rec.");
                            Configuration oConfiguration = new Configuration(sDuns, BusinessUtility.GetInt(Files.ExecutionFileType.EDI8561));
                            FTPTraking ftpObj = new FTPTraking();
                            ftpObj.ftpFileName = BusinessUtility.GetString(m_sSingleFilePath);
                            ftpObj.ftpFilePath = BusinessUtility.GetString(m_sSingleFilePath);
                            Files oFiles = new Files();
                            oFiles.VendorId = BusinessUtility.GetString(oConfiguration.VendorId);
                            oFiles.FileType = BusinessUtility.GetInt("856");
                            oFiles.VendorName = oConfiguration.VendorName;
                            oFiles.CustomerName = oConfiguration.CustomerName;
                            oFiles.Mill = oConfiguration.MillId;
                            oFiles.VendorDunsNo = oConfiguration.VendorDunsNo;
                            m_iFileId = oFiles.insertFileStatus(ftpObj);
                            ErrorLog.createLog("m_iFileId = " + m_iFileId);
                            Globalcl.SetFileId(m_iFileId);
                            Globalcl.SetFileType("856");

                            CustomerOwnReceipt oCustomerOwnReceipt = new CustomerOwnReceipt();
                            oCustomerOwnReceipt.ParseCustomerOwnReceipt(m_sSingleFilePath, ref oConfiguration);
                            //Move files one by one after processing     2020/09/22  Sumit
                            File.Copy(m_sSingleFilePath, Processed_PATH + @"\" + Path.GetFileName(m_sSingleFilePath), true);
                            //File.Delete(m_sSingleFilePath);       // comment code to use download directory for Inbiz download functionality  2021/04/05  Sumit
                        }
                        else
                        {
                            ErrorLog.createLog("No data in parsed in dtCustData after csv read");
                        }
                        System.Threading.Thread.Sleep(1000);
                    }
                    System.Threading.Thread.Sleep(10000);
                    //InputOutput.MoveDirectory(SourceFile_PATH, Processed_PATH, false);        // comment code move files with rename from network folder  2021/04/05  Sumit
                }
                else
                {
                    CleanDirectory();
                    // this code update all receipt 
                    Files oFiles1 = new Files();
                    oFiles1.UpdateReceipt();
                    //write code here to process certificate records
                    ProcessProductQdsCertificateRecord(0);
                    if (m_iExecuteSingleFile != 1)
                    {
                        try
                        {
                            oConfiguration = new Configuration();
                            DataTable dtVendorList = oConfiguration.GetActiveVendorList();
                            ErrorLog.createLog("dtVendorList count = " + dtVendorList.Rows.Count);
                            //To Select Vendor From List    2020/03/13  Sumit
                            IsVendorFilter = Utility.GetConfigValue("IsVendorFilter");
                            ErrorLog.createLog("IsVendorFilter = " + IsVendorFilter);
                            if (!string.IsNullOrEmpty(IsVendorFilter) && BusinessUtility.GetInt(IsVendorFilter) == 1)
                            {
                                if(!string.IsNullOrEmpty(Utility.GetConfigValue("VendorDunsList")))
                                {
                                    ErrorLog.createLog("Selected VendorDunsNo: " + Utility.GetConfigValue("VendorDunsList"));
                                    VendorFiltration.FilterVendor filterVendor = new VendorFiltration.FilterVendor(dtVendorList, Utility.GetConfigValue("VendorDunsList"));
                                    dtVendorList = filterVendor.FilterVendorList();
                                    ErrorLog.createLog("After Filtration - dtVendorList count = " + dtVendorList.Rows.Count);
                                }
                            }
                            //To Select Vendor From List    2020/03/13  Sumit

                            foreach (DataRow oDataRow in dtVendorList.Rows)
                            {
                               
                                oConfiguration.VendorDunsNo = BusinessUtility.GetString(oDataRow["VendorDunsNo"]);
                                oConfiguration.VendorId = BusinessUtility.GetString(oDataRow["vendorId"]);
                                oConfiguration.EdiDataSourceId = BusinessUtility.GetInt(oDataRow["EdiDataSourceId"]);
                                EdiDataSource oEdiDataSource = new EdiDataSource(oConfiguration.EdiDataSourceId);
                                ErrorLog.createLog("oEdiDataSource.EdiDataSourceTypeProperty= " + oEdiDataSource.EdiDataSourceTypeProperty);
                                if (oEdiDataSource.EdiDataSourceTypeProperty == EdiDataSource.EdiDataSourceType.SFTP)
                                {
                                    //Console.WriteLine("EdiDataSource.EdiDataSourceType.SFTP");
                                    //ErrorLog.createLog("EdiDataSource.EdiDataSourceType.SFT");
                                    //Sftpcl oSftpcl = new Sftpcl(oConfiguration.EdiDataSourceId);
                                }
                                else if (oEdiDataSource.EdiDataSourceTypeProperty == EdiDataSource.EdiDataSourceType.FTP)
                                {
                                    FTP oFTP = new FTP();
                                    oFTP.Username = oEdiDataSource.UserId;
                                    oFTP.Password = oEdiDataSource.Password;
                                    oFTP.Port = BusinessUtility.GetInt(oEdiDataSource.Port);
                                    oFTP.Server = oEdiDataSource.Host;
                                    oFTP.LocalDownloadDir = oEdiDataSource.DataDownloadDirectory;
                                    oFTP.FtpDirectory = oEdiDataSource.DataSourceDirectory;
                                    DataDownloadDirectory = oEdiDataSource.DataDownloadDirectory;
                                    ErrorLog.createLog("DatadownloadDir= " + DataDownloadDirectory);
                                    //To select only required file formats  2020/03/13  Sumit
                                    if (!string.IsNullOrEmpty(Utility.GetConfigValue("FileFormat")))
                                    {
                                        ErrorLog.createLog("Selected File Formats: " + Utility.GetConfigValue("FileFormat"));
                                        FTPFileFormat ftpFileFormat = new FTPFileFormat(oFTP);
                                        m_lstFiles = ftpFileFormat.DownloadAllFilesFromFTP(Utility.GetConfigValue("FileFormat"));
                                    }
                                    else
                                    {
                                        m_lstFiles = oFTP.DownloadAllFilesFromFTP();
                                    }
                                    //To select only required file formats  2020/03/13  Sumit
                                    oFTP.DeleteRemoteFiles(m_lstFiles);

                                    if(m_lstFiles.Count > 0)
                                    {
                                        // start
                                        FileReceived oFileReceived = new FileReceived();
                                        foreach (string s in m_lstFiles)
                                        {
                                           string ss = DataDownloadDirectory + "//" + s;
                                            InputOutput.CopyFileFromSourceToDestination(ss, oEdiDataSource.DataProcessedDirectory + "//" + s);

                                            // this will sechedule file
                                            oFileReceived.ProcessFile(s, oEdiDataSource.DataDownloadDirectory, ref oFileReceived);
                                        }

                                        // this will get file ready to be processed
                                        DataTable dtFileReadyToProcessList = oFileReceived.GetFileReadyToBeProcessed();

                                        foreach (DataRow oDataRow1 in dtFileReadyToProcessList.Rows)
                                        {
                                            int iFileReceivedId = BusinessUtility.GetInt(oDataRow1["FileReceivedId"]);
                                            string sFileReceivedDirectory = BusinessUtility.GetString(oDataRow1["FileReceivedDirectory"]);
                                            string sFileReceivedName = BusinessUtility.GetString(oDataRow1["FileName"]);
                                            InputOutput.CopyFileFromSourceToDestination(sFileReceivedDirectory + "\\" + sFileReceivedName, oEdiDataSource.DataUploadDirectory + "\\" + sFileReceivedName);


                                            m_sSingleFilePath = sFileReceivedDirectory + "//" + sFileReceivedName;
                                            ErrorLog.createLog("m_sSingleFilePath : " + m_sSingleFilePath);
                                            FileInfo o = new FileInfo(m_sSingleFilePath);
                                            if (o.Extension.ToLower().Contains(".xml"))
                                            {
                                                if (m_sFileType == Configuration.EdiFileType.EDI856863)
                                                {
                                                    ErrorLog.createLog("XML PROCESSING START FILE NAME : " + m_sSingleFilePath);
                                                    EdiXmlParser oEdiXmlParser = new EdiXmlParser(m_sSingleFilePath, iFileReceivedId);
                                                    //ErrorLog.createLog("Test 1");
                                                    oFileReceived = new FileReceived(iFileReceivedId);
                                                    //ErrorLog.createLog("Test 2");
                                                    oFiles1 = new Files(oFileReceived.UniqueFileName);
                                                    //ErrorLog.createLog("Test 3");
                                                    m_iFileId = oFiles1.FileID;
                                                    oConfiguration = new Configuration(oFiles1.VendorDunsNo, oFiles1.FileType);
                                                    //ErrorLog.createLog("Test 4");
                                                    if (m_iFileId > 0)
                                                    {
                                                        if (oFiles1.FileType.ToString() == Configuration.EdiFileType.EDI863)
                                                        {
                                                            if (oConfiguration.Activate863)
                                                            {
                                                                // Get 863 List to process
                                                                header863 oheader863 = new header863();
                                                                DataTable dtUnprocessed863 = oheader863.GetUnprocessed863(m_iFileId);
                                                                //ErrorLog.createLog("Test 5");
                                                                foreach (DataRow dr in dtUnprocessed863.Rows)
                                                                {
                                                                    int iHeaderId = BusinessUtility.GetInt(dr["ID"]);
                                                                    //ErrorLog.createLog("Header id = " + iHeaderId + " FileId = " + m_iFileId);
                                                                    Console.WriteLine("Creating qds for Header id = " + iHeaderId + " FileId = " + m_iFileId);
                                                                    //ErrorLog.createLog("Test 6");
                                                                    QdsService oQdsService = new QdsService(m_iFileId, iHeaderId);
                                                                    //ErrorLog.createLog("Test 7");
                                                                    oQdsService.CreateQDS();
                                                                    //ErrorLog.createLog("Test 8");
                                                                    oheader863.InactiveHeader(m_iFileId, iHeaderId);
                                                                    //ErrorLog.createLog("Test 9");
                                                                }
                                                            }
                                                            else
                                                            {
                                                                ErrorLog.createLog("863 is disabled");
                                                            }

                                                        }

                                                        if (oFiles1.FileType.ToString() == Configuration.EdiFileType.EDI856)
                                                        {
                                                            if (oConfiguration.Activate856)
                                                            {
                                                                // Get 856 List to process
                                                                STX856 oSTX856 = new STX856();
                                                                oSTX856.ProcessReceipt(m_iFileId);
                                                            }
                                                            else
                                                            {
                                                                ErrorLog.createLog("856 is disabled..");
                                                            }

                                                        }
                                                        //oFiles.InactiveFile(m_iFileId);
                                                        ErrorLog.createLog("XML PROCESSING END FILE ID : " + m_iFileId);
                                                        oFiles1.InactiveFile(m_iFileId);
                                                        oFileReceived.UpdateProcessedReceiveFile(iFileReceivedId);
                                                    }
                                                    //}
                                                }

                                            }
                                            else
                                            {
                                                // write code here to insert certificate pdf and metadata file
                                                #region code for Certificate Processing 2020/04/22  Sumit
                                                if (File.Exists(o.FullName))
                                                {
                                                    int certFID = ProcessExternalCertificateFile(o, iFileReceivedId);
                                                    if (o.Extension.ToLower() == ".txt" && certFID > 0)
                                                    {
                                                        ExternalCertificate.QdsExternalCertificateFile objFile = new ExternalCertificate.QdsExternalCertificateFile();
                                                        objFile = objFile.GetExternalCertificateFile(certFID);

                                                        ExternalCertificate.CertParserManager certParserManager = new ExternalCertificate.CertParserManager();
                                                        ExternalCertificate.ICertParser certParser = certParserManager.CallParser(objFile.QdsExCertFile_FileFormat);
                                                        if (certParser != null)
                                                        {
                                                            if (certParser.ParseCertificate(objFile.QdsExCertFile_FilePath, objFile.QdsExCertFile_FileName, objFile.QdsExCertFile_FileReceiveId, objFile.QdsExCertFile_Id))
                                                            {
                                                                ErrorLog.createLog("Certificate text file parsed successfully. Filename: " + objFile.QdsExCertFile_FileName);
                                                                objFile.UpdateExternalCertificateFile(objFile.QdsExCertFile_Id);
                                                                ErrorLog.createLog("Certificate text file record updated successfully. Filename: " + objFile.QdsExCertFile_FileName);

                                                                //calling function to attach certificate with qds
                                                                //ProcessProductQdsCertificateRecord(objFile.QdsExCertFile_Id);
                                                                ErrorLog.createLog("Certificate File Processing End");
                                                            }
                                                        }
                                                    }
                                                }
                                                #endregion
                                            }
                                            
                                        }

                                        // end
                                    }
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            EdiError.LogError(EdiFile, m_iFileId.ToString(), "", ex, "");
                            ErrorLog.CreateLog(ex);
                        }
                    }
                    else if (m_iExecuteSingleFile == 1)
                    { 
                        #region Local File retrive and process
                        FileInfo o = new FileInfo(m_sSingleFilePath);
                        if (o.Extension.Contains(".xml"))
                        {                            
                            if (m_sFileType == Configuration.EdiFileType.EDI856863)
                            {
                                ErrorLog.createLog("XML PROCESSING START FILE ID " + m_iFileId);
                                EdiXmlParser oEdiXmlParser = new EdiXmlParser(m_sSingleFilePath, 0);

                                // Get unprocessed file list
                                DataTable dtUnProcessedFile = oFiles1.GetUnprocessedFile();
                                dtUnProcessedFile.DefaultView.Sort = "FileId DESC";
                                dtUnProcessedFile = dtUnProcessedFile.DefaultView.ToTable();
                                foreach (DataRow drfile in dtUnProcessedFile.Rows)
                                {
                                    m_iFileId = BusinessUtility.GetInt(drfile["fileid"]);
                                    oFiles1.VendorDunsNo = BusinessUtility.GetString(drfile["VendorDunsNo"]);
                                    oFiles1.FileType = BusinessUtility.GetInt(drfile["FileType"]);
                                    oConfiguration = new Configuration(oFiles1.VendorDunsNo, oFiles1.FileType);
                                    if (m_iFileId > 0)
                                    {
                                        if (oConfiguration.Activate863 && (oFiles1.FileType.ToString() == Configuration.EdiFileType.EDI863))
                                        {
                                            // Get 863 List to process
                                            header863 oheader863 = new header863();
                                            DataTable dtUnprocessed863 = oheader863.GetUnprocessed863(m_iFileId);
                                            foreach (DataRow dr in dtUnprocessed863.Rows)
                                            {
                                                int iHeaderId = BusinessUtility.GetInt(dr["ID"]);
                                                ErrorLog.createLog("Header id = " + iHeaderId + " FileId = " + m_iFileId);
                                                Console.WriteLine("Creating qds for Header id = " + iHeaderId + " FileId = " + m_iFileId);
                                                QdsService oQdsService = new QdsService(m_iFileId, iHeaderId); 
                                                oQdsService.CreateQDS();
                                                oheader863.InactiveHeader(m_iFileId, iHeaderId);
                                            }
                                        }
                                        else
                                        {
                                            ErrorLog.createLog("863 is disabled");
                                        }

                                   
                                        if (oConfiguration.Activate856 && (oFiles1.FileType.ToString() == Configuration.EdiFileType.EDI856))
                                        {
                                            // Get 856 List to process
                                            STX856 oSTX856 = new STX856();
                                            oSTX856.ProcessReceipt(m_iFileId);
                                        }
                                        else
                                        {
                                            ErrorLog.createLog("856 is disabled..");
                                        }
                                        //oFiles.InactiveFile(m_iFileId);
                                        ErrorLog.createLog("XML PROCESSING END FILE ID : " + m_iFileId);

                                        oFiles1.InactiveFile(m_iFileId);
                                    }
                                }
                            }
                            //
                        }
                        else
                        {
                            #region code for Certificate Processing 2020/04/22  Sumit
                            if(File.Exists(o.FullName))
                            {
                                int certFID = ProcessExternalCertificateFile(o,0);
                                if (o.Extension.ToLower() == ".txt" && certFID > 0)
                                {
                                    ExternalCertificate.QdsExternalCertificateFile objFile = new ExternalCertificate.QdsExternalCertificateFile();
                                    objFile = objFile.GetExternalCertificateFile(certFID);

                                    ExternalCertificate.CertParserManager certParserManager = new ExternalCertificate.CertParserManager();
                                    ExternalCertificate.ICertParser certParser = certParserManager.CallParser(objFile.QdsExCertFile_FileFormat);
                                    if (certParser != null)
                                    {
                                        if (certParser.ParseCertificate(objFile.QdsExCertFile_FilePath, objFile.QdsExCertFile_FileName, objFile.QdsExCertFile_FileReceiveId, objFile.QdsExCertFile_Id))
                                        {
                                            ErrorLog.createLog("Certificate text file parsed successfully. Filename: " + objFile.QdsExCertFile_FileName);
                                            objFile.UpdateExternalCertificateFile(objFile.QdsExCertFile_Id);
                                            ErrorLog.createLog("Certificate text file record updated successfully. Filename: " + objFile.QdsExCertFile_FileName);

                                            //calling function to attach certificate with qds
                                            ProcessProductQdsCertificateRecord(objFile.QdsExCertFile_Id);
                                            ErrorLog.createLog("Certificate File Processing End");
                                        }
                                    }
                                }
                            }
                            #endregion
                        }
                        #endregion
                    }
                    //write code here to process certificate records
                    ProcessProductQdsCertificateRecord(0);
                }
            }
            catch (Exception ex)
            {
                ErrorLog.createLog(ex.ToString());
                ErrorLog.CreateLog(ex);
            }

            System.AppDomain.CurrentDomain.UnhandledException += UnhandledExceptionTrapper;
            // throw new Exception("Global Exception occur");
        }

        public static int ProcessExternalCertificateFile(FileInfo fInfo, Int32 receivedId)
        {
            int insertedID=0;
            try
            {
                ExternalCertificate.QdsExternalCertificateFile qdsExCertFile = new ExternalCertificate.QdsExternalCertificateFile();
                //insert Certificate file into database
                qdsExCertFile.QdsExCertFile_FileReceiveId = receivedId;
                qdsExCertFile.QdsExCertFile_FilePath = fInfo.DirectoryName.ToString();
                qdsExCertFile.QdsExCertFile_FileName = fInfo.Name;
                qdsExCertFile.QdsExCertFile_FileFormat = fInfo.Extension.ToLower().Replace(".","");

                if (fInfo.Extension.ToLower() == ".txt")
                {
                    if (qdsExCertFile.isValidCertificateFile(fInfo.DirectoryName, fInfo.Name))
                    {
                        if(!IsExcertFileAlreadyInserted(qdsExCertFile.QdsExCertFile_FileName))
                        {
                            insertedID = qdsExCertFile.InsertExternalCertificateFile(qdsExCertFile);

                            if (insertedID > 0)
                            {
                                if(receivedId>0)
                                {
                                    FileReceived ofilereceived = new FileReceived();
                                    ofilereceived.UpdateProcessedReceiveFile(receivedId);
                                }
                                
                                Console.WriteLine("File inserted as Qds Certificate File. FileName: " + fInfo.FullName);
                                ErrorLog.createLog("File inserted as Qds Certificate File. FileName: " + fInfo.FullName);
                            }
                            else
                            {
                                Console.WriteLine("File not inserted as Qds Certificate File. FileName: " + fInfo.FullName);
                                ErrorLog.createLog("File not inserted as Qds Certificate File. FileName: " + fInfo.FullName);
                            }
                        }
                        else
                        {
                            if (receivedId > 0)
                            {
                                FileReceived ofilereceived = new FileReceived();
                                ofilereceived.UpdateProcessedReceiveFile(receivedId);
                            }
                            Console.WriteLine("File Already inserted as Qds Certificate File. FileName: " + fInfo.FullName);
                            ErrorLog.createLog("File Already inserted as Qds Certificate File. FileName: " + fInfo.FullName);
                        }
                    }
                    else
                    {
                        Console.WriteLine("File is not a valid Qds Certificate File. FileName: " + fInfo.FullName);
                        ErrorLog.createLog("File is not a valid Qds Certificate File. FileName: " + fInfo.FullName);
                    }
                }
                else if(fInfo.Extension.ToLower() ==".pdf")
                {
                    if(!IsExcertFileAlreadyInserted(qdsExCertFile.QdsExCertFile_FileName))
                    {
                        insertedID = qdsExCertFile.InsertExternalCertificateFile(qdsExCertFile);

                        if (insertedID > 0)
                        {
                            if (receivedId > 0)
                            {
                                FileReceived ofilereceived = new FileReceived();
                                ofilereceived.UpdateProcessedReceiveFile(receivedId);
                            }

                            Console.WriteLine("File inserted as Qds Certificate attachment. FileName: " + fInfo.FullName);
                            ErrorLog.createLog("File inserted as Qds Certificate attachment. FileName: " + fInfo.FullName);
                        }
                        else
                        {
                            Console.WriteLine("File not inserted as Qds Certificate attachment. FileName: " + fInfo.FullName);
                            ErrorLog.createLog("File not inserted as Qds Certificate attachment. FileName: " + fInfo.FullName);
                        }
                    }
                    else
                    {
                        if (receivedId > 0)
                        {
                            FileReceived ofilereceived = new FileReceived();
                            ofilereceived.UpdateProcessedReceiveFile(receivedId);
                        }

                        Console.WriteLine("File Already inserted as Qds Certificate attachment. FileName: " + fInfo.FullName);
                        ErrorLog.createLog("File Already inserted as Qds Certificate attachment. FileName: " + fInfo.FullName);
                    }
                    
                }
            }
            catch(Exception)
            {
                throw;
            }
            return insertedID;
        }

        public static bool IsExcertFileAlreadyInserted(string filename)
        {
            bool isInserted = false;
            ExternalCertificate.QdsExternalCertificateFile qdsExCertFile = null;
            ExternalCertificate.QdsExternalCertificateFile qdsFile = new ExternalCertificate.QdsExternalCertificateFile();
            try
            {
                qdsExCertFile = qdsFile.GetExternalCertificateFile_ByFileName(filename);
                if(qdsExCertFile != null)
                {
                    if (qdsExCertFile.QdsExCertFile_Id > 0)
                        isInserted = true;
                }
            }
            catch(Exception ex)
            {
                throw ex;
            }

            return isInserted;
        }

        public static void ParseExternalCertificateFile()
        {
            DataTable dtExcertFile;
            ExternalCertificate.QdsExternalCertificateFile objFile;
            ExternalCertificate.QdsExternalCertificateFile QdsExCertFile;
            try
            {
                objFile = new ExternalCertificate.QdsExternalCertificateFile();

                dtExcertFile = objFile.GetExternalCertificateTextFile_ReadyToBeProcessed();
                Console.WriteLine("Total pending External Certificate files for parsing : " + dtExcertFile.Rows.Count.ToString());
                ErrorLog.createLog("Total pending External Certificate files for parsing : " + dtExcertFile.Rows.Count.ToString());
                if(dtExcertFile.Rows.Count >0)
                {
                    foreach(DataRow drow in dtExcertFile.Rows)
                    {
                        QdsExCertFile = objFile.GetExternalCertificateFile(BusinessUtility.GetInt(drow["qecf_id"]));
                        if(QdsExCertFile != null)
                        {
                            ExternalCertificate.CertParserManager certParserManager = new ExternalCertificate.CertParserManager();
                            ExternalCertificate.ICertParser certParser = certParserManager.CallParser(QdsExCertFile.QdsExCertFile_FileFormat);
                            if (certParser != null)
                            {
                                if (certParser.ParseCertificate(QdsExCertFile.QdsExCertFile_FilePath, QdsExCertFile.QdsExCertFile_FileName, QdsExCertFile.QdsExCertFile_FileReceiveId, QdsExCertFile.QdsExCertFile_Id))
                                {
                                    Console.WriteLine("Certificate text file parsed successfully. Filename: " + QdsExCertFile.QdsExCertFile_FileName);
                                    ErrorLog.createLog("Certificate text file parsed successfully. Filename: " + QdsExCertFile.QdsExCertFile_FileName);
                                    objFile.UpdateExternalCertificateFile(QdsExCertFile.QdsExCertFile_Id);
                                    Console.WriteLine("Certificate text file record updated successfully. Filename: " + QdsExCertFile.QdsExCertFile_FileName);
                                    ErrorLog.createLog("Certificate text file record updated successfully. Filename: " + QdsExCertFile.QdsExCertFile_FileName);
                                }
                            }
                        }
                    }
                }
                
            }
            catch (Exception)
            { throw; }
        }

        /// <summary>
        /// to attach certificate on product qds only on the basis of metadata file id
        /// </summary>
        /// <param name="qdsFID"></param>
        public static void ProcessProductQdsCertificateRecord(int qdsFID)
        {
            DataTable dtCertData =new DataTable();
            //long heatQdsNumber = 0;
            long prdQdsNumber = 0;
            QdsService qdsService;
            ExternalCertificate.QdsExternalCertificateData qdsExCertData = new ExternalCertificate.QdsExternalCertificateData();
            ExternalCertificate.QdsExternalCertificateData qdsCert;
            ExternalCertificate.QdsExternalCertificateFile qdsCertFile = new ExternalCertificate.QdsExternalCertificateFile();
            try
            {
                if (qdsFID > 0)
                    dtCertData = qdsExCertData.GetAllExternalCertificateRecord(qdsFID);
                else
                    dtCertData = qdsExCertData.GetAllExternalCertificateRecord();

                Console.WriteLine("Total Record found for Qds Certificate attachment : " + dtCertData.Rows.Count);
                ErrorLog.createLog("Total Record found for Qds Certificate attachment : " + dtCertData.Rows.Count);
                if (dtCertData.Rows.Count > 0)
                {
                    foreach (DataRow drow in dtCertData.Rows)
                    {
                        qdsCert = null;
                        qdsCert = qdsExCertData.GetSingleExternalCertificateRecord(BusinessUtility.GetInt(drow["qecd_id"]));
                        if (qdsCert != null)
                        {
                            prdQdsNumber = 0;
                            qdsService = new QdsService(0, 0);
                            //finding Product qds and attaching certificate
                            prdQdsNumber = qdsExCertData.isProductQdsExist(qdsCert.QdsExCert_PoNumber, qdsCert.QdsExCert_Heat, qdsCert.QdsExCert_VendorTagId);
                            if (prdQdsNumber > 0)
                            {
                                Console.WriteLine("Going to attach Certificate for Product Qds : " + prdQdsNumber);
                                ErrorLog.createLog("Going to attach Certificate for Product Qds : " + prdQdsNumber);
                                if(!tctipd.IsCertificateAttached_PrdQds(prdQdsNumber))
                                {
                                    if (qdsService.UpdateOnlyQdsCertificate(prdQdsNumber, Path.Combine(qdsCert.QdsExCert_FilePath, qdsCert.QdsExCert_Filename), qdsCert.QdsExCert_SenderId))
                                    {
                                        qdsExCertData.UpdateProductQdsCertificate(qdsCert.QdsExCert_Id, BusinessUtility.GetString(prdQdsNumber));
                                        Console.WriteLine("Certificate Updated For Product Qds:" + prdQdsNumber);
                                        ErrorLog.createLog("Certificate Updated For Product Qds:" + prdQdsNumber);
                                        qdsExCertData.UpdateQdsCertificate(qdsCert.QdsExCert_Id);
                                        qdsCertFile.UpdateExternalCertificateFile_ByFileName(qdsCert.QdsExCert_Filename.Trim());
                                        Console.WriteLine("Qds Certificate Record Update...");
                                        ErrorLog.createLog("Qds Certificate Record Update...");
                                    }
                                }
                                else
                                {
                                    //send email for already attached certificate
                                    Console.WriteLine("Certificate already attached To Product Qds: "+ prdQdsNumber);
                                    ErrorLog.createLog("Certificate already attached To Product Qds: " + prdQdsNumber);
                                    qdsExCertData.UpdateQdsCertificate(qdsCert.QdsExCert_Id);
                                    qdsCertFile.UpdateExternalCertificateFile_ByFileName(qdsCert.QdsExCert_Filename.Trim());
                                    Console.WriteLine("Qds Certificate Record Update...");
                                    ErrorLog.createLog("Qds Certificate Record Update...");

                                }
                            }
                            else
                            {
                                Console.WriteLine("Product Qds not found for Certificate attachment. PO Number: " + qdsCert.QdsExCert_PoNumber + "    ProductQds: " + qdsCert.QdsExCert_PrdQdsno + "     VendorTagId:" + qdsCert.QdsExCert_VendorTagId + "");
                                ErrorLog.createLog("Product Qds not found for Certificate attachment. PO Number: " + qdsCert.QdsExCert_PoNumber + "    ProductQds: " + qdsCert.QdsExCert_PrdQdsno + "     VendorTagId:" + qdsCert.QdsExCert_VendorTagId + "");
                            }
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                ErrorLog.createLog("Error In ProcessCertificateRecord.");
                ErrorLog.createLog(ex.InnerException != null ? ex.InnerException.ToString() : ex.Message);
            }
        }

        /// <summary>
        /// To attach certificate on Heat and Product qds on the basis of metadata file id 
        /// </summary>
        /// <param name="qdsFID"></param>
        public static void ProcessCertificateRecord(int qdsFID)
        {
            DataTable dtCertData;
            long heatQdsNumber =0;
            long prdQdsNumber = 0;
            QdsService qdsService;
            ExternalCertificate.QdsExternalCertificateData qdsExCertData = new ExternalCertificate.QdsExternalCertificateData();
            ExternalCertificate.QdsExternalCertificateData qdsCert;
            ExternalCertificate.QdsExternalCertificateFile qdsCertFile = new ExternalCertificate.QdsExternalCertificateFile();
            try
            {
                if (qdsFID > 0)
                    dtCertData = qdsExCertData.GetAllExternalCertificateRecord(qdsFID);
                else
                    dtCertData = qdsExCertData.GetAllExternalCertificateRecord();

                Console.WriteLine("Total Record found for Qds Certificate attachment : " + dtCertData.Rows.Count);
                ErrorLog.createLog("Total Record found for Qds Certificate attachment : " + dtCertData.Rows.Count);
                if(dtCertData.Rows.Count >0)
                {
                    foreach (DataRow drow in dtCertData.Rows)
                    {
                        qdsCert = qdsExCertData.GetSingleExternalCertificateRecord(BusinessUtility.GetInt(drow["qecd_id"]));
                        if(qdsCert != null)
                        {
                            heatQdsNumber = 0;
                            prdQdsNumber = 0;
                            //finding heat qds and attaching certificate
                            heatQdsNumber = qdsExCertData.isHeatQdsExist(qdsCert.QdsExCert_PoNumber, qdsCert.QdsExCert_Heat, qdsCert.QdsExCert_VendorTagId);
                            if (heatQdsNumber > 0)
                            {
                                Console.WriteLine("Going to attach Certificate for Heat Qds : " + heatQdsNumber);
                                ErrorLog.createLog("Going to attach Certificate for Heat Qds : " + heatQdsNumber);
                                qdsService = new QdsService(0, 0);
                                if (qdsService.UpdateOnlyQdsCertificate(heatQdsNumber, Path.Combine(qdsCert.QdsExCert_FilePath,qdsCert.QdsExCert_Filename) ,qdsCert.QdsExCert_SenderId))
                                {
                                    qdsExCertData.UpdateHeatQdsCertificate(qdsCert.QdsExCert_Id, BusinessUtility.GetString(heatQdsNumber));
                                    Console.WriteLine("Certificate Updated For Heat Qds:" + heatQdsNumber);
                                    ErrorLog.createLog("Certificate Updated For Heat Qds:" + heatQdsNumber);

                                    //finding Product qds and attaching certificate
                                    prdQdsNumber = qdsExCertData.isProductQdsExist(qdsCert.QdsExCert_PoNumber, qdsCert.QdsExCert_Heat, qdsCert.QdsExCert_VendorTagId);
                                    if (prdQdsNumber > 0)
                                    {
                                        Console.WriteLine("Going to attach Certificate for Product Qds : " + prdQdsNumber);
                                        ErrorLog.createLog("Going to attach Certificate for Product Qds : " + prdQdsNumber);
                                        if(qdsService.UpdateOnlyQdsCertificate(prdQdsNumber, Path.Combine(qdsCert.QdsExCert_FilePath, qdsCert.QdsExCert_Filename), qdsCert.QdsExCert_SenderId))
                                        {
                                            qdsExCertData.UpdateProductQdsCertificate(qdsCert.QdsExCert_Id, BusinessUtility.GetString(prdQdsNumber));
                                            Console.WriteLine("Certificate Updated For Product Qds:" + prdQdsNumber);
                                            ErrorLog.createLog("Certificate Updated For Product Qds:" + prdQdsNumber);
                                        }
                                    }
                                    else
                                    {
                                        Console.WriteLine("Product Qds not found for Certificate attachment. PO Number: " + qdsCert.QdsExCert_PoNumber + "    ProductQds: " + qdsCert.QdsExCert_PrdQdsno + "     VendorTagId:" + qdsCert.QdsExCert_VendorTagId + "");
                                        ErrorLog.createLog("Product Qds not found for Certificate attachment. PO Number: " + qdsCert.QdsExCert_PoNumber + "    ProductQds: " + qdsCert.QdsExCert_PrdQdsno + "     VendorTagId:" + qdsCert.QdsExCert_VendorTagId + "");
                                    }
                                    qdsExCertData.UpdateQdsCertificate(qdsCert.QdsExCert_Id);
                                    qdsCertFile.UpdateExternalCertificateFile_ByFileName(qdsCert.QdsExCert_Filename.Trim());
                                    Console.WriteLine("Qds Certificate Record Update...");
                                    ErrorLog.createLog("Qds Certificate Record Update...");
                                }
                            }
                            else
                            {
                                Console.WriteLine("Heat Qds not found for Certificate attachment. PO Number: " + qdsCert.QdsExCert_PoNumber + "    Heat: " + qdsCert.QdsExCert_Heat + "     VendorTagId:" + qdsCert.QdsExCert_VendorTagId + "");
                                ErrorLog.createLog("Heat Qds not found for Certificate attachment. PO Number: " + qdsCert.QdsExCert_PoNumber + "    Heat: " + qdsCert.QdsExCert_Heat + "     VendorTagId:" + qdsCert.QdsExCert_VendorTagId + "");
                            }
                        }
                    }
                }

            }
            catch(Exception ex)
            {
                ErrorLog.createLog("Error In ProcessCertificateRecord.");
                ErrorLog.createLog(ex.InnerException != null ? ex.InnerException.ToString() : ex.Message);
            }
        }

        /// <summary>
        /// to test External Certificate attachment process
        /// </summary>
        public static void TestCertificateAttachment()
        {
            try
            {
                QdsService qdsService = new QdsService(0, 0);
                long qdsnumber = BusinessUtility.GetLong(Utility.GetConfigValue("CertQDS"));
                string filename = Utility.GetConfigValue("CertFilePath");
                string senderID = Utility.GetConfigValue("CertSenderID");
                if(qdsnumber <1 || string.IsNullOrEmpty(filename) || string.IsNullOrEmpty(senderID))
                {
                    ErrorLog.createLog("Please Pass all required details from config file...");
                    Console.WriteLine("Please Pass all required details from config file...");
                    return;
                }
                if (qdsService.UpdateOnlyQdsCertificate(qdsnumber, filename, senderID))
                {
                    ErrorLog.createLog("Congrats !!!....Certificate Attached...");
                }
                else
                {
                    ErrorLog.createLog("Certificate Not Attached...");
                }
            }
            catch(Exception ex)
            {
                ErrorLog.createLog(ex.ToString());
                ErrorLog.CreateLog(ex);
            }
        }

        static void UnhandledExceptionTrapper(object sender, UnhandledExceptionEventArgs e)
        {
            ErrorLog.createLog(e.ExceptionObject.ToString());
            //Console.WriteLine(e.ExceptionObject.ToString());
            //Console.WriteLine("Press Enter to continue");
            //Console.ReadLine();
            //Environment.Exit(1);
        }

        static void AppendLineSeperator(ref string[] sArr)
        {
            string[] sLine = new string[sArr.Length]; ;
            ArrayList oArrayList = new ArrayList();
            for (int i = 0; i < sArr.Length - 1; i++)
            {
                oArrayList.Add(sArr[i] + oConfiguration.LineSeperator);
                //sLine[i].Insert(i, sArr[i] + oConfiguration.LineSeperator);
            }

            // object obj = oArrayList.ToArray();
            sArr = (string[])oArrayList.ToArray(typeof(string));
            //sArr = oArrayList.ToArray();
        }


        public static void CleanDirectory()
        {
            System.IO.DirectoryInfo di = new DirectoryInfo(Upload_PATH);

            foreach (FileInfo file in di.GetFiles())
            {
                file.Delete();
            }
            foreach (DirectoryInfo dir in di.GetDirectories())
            {
                dir.Delete(true);
            }
        }


        static void ReadAllFiles(String sourcePath, String Processed, String UnProcessed)
        {
            FTPTraking ftpObj = new FTPTraking();
            try
            {
                String[] FileInfo;
                String FileName;
                String UnprocessFile = "";
                int intI = 0;
                if (Directory.Exists(sourcePath))
                {
                    FileInfo = Directory.GetFiles(sourcePath);
                    int i;
                    if (FileInfo.Length == 0)
                    {
                        ErrorLog.createLog("No file available to process..");
                        Console.WriteLine("No file available to process..");
                    }
                    for (i = 0; i <= FileInfo.Length - 1; i++)
                    {
                        FileName = Path.GetFileName(FileInfo[i]);
                        try
                        {
                            // RedXML(sourcePath + "/" + FileName);

                            //System.IO.Directory.Move(sourcePath + "/" + FileName, Processed + "/" + FileName);
                            System.IO.File.Copy(sourcePath + "/" + FileName, Processed + "/" + FileName, true);




                            FileInfo oFileInfo = new FileInfo(sourcePath + "/" + FileName);
                            long lngFileLength = oFileInfo.Length;
                            if (lngFileLength > 0)
                            {

                                var fileLines = System.IO.File.ReadAllLines(BusinessUtility.GetString(sourcePath + "/" + FileName));

                                oConfiguration = new Configuration();
                                oConfiguration.PopulateConfigurationData(fileLines);
                                EdiFile = oConfiguration.FileType;
                                m_sCusVenId = BusinessUtility.GetString(oConfiguration.VendorId);

                                // Insert file Status
                                ftpObj.ftpFileName = BusinessUtility.GetString(FileName);
                                ftpObj.ftpFilePath = BusinessUtility.GetString(sourcePath + "/" + FileName);
                                //ftpObj.Status = BusinessUtility.GetInt(FileStatus.Processed);
                                Files oFiles = new Files();
                                oFiles.VendorId = m_sCusVenId;
                                oFiles.FileType = BusinessUtility.GetInt(EdiFile);
                                oFiles.VendorName = oConfiguration.VendorName;
                                oFiles.Mill = oConfiguration.MillId;
                                oFiles.CustomerName = oConfiguration.CustomerName;
                                oFiles.VendorDunsNo = oConfiguration.VendorDunsNo;
                                m_iFileId = oFiles.insertFileStatus(ftpObj);


                                ReadFile(sourcePath + "/" + FileName, m_iFileId);

                                // ErrorLog.createLog("File Added Successfully");
                                // End Block

                                ErrorLog.createLog("processing file name : " + ftpObj.ftpFileName + " , ID of my sqldb : " + m_iFileId);
                                System.IO.File.Delete(sourcePath + "/" + FileName);
                            }
                            intI += 1;
                        }
                        catch (Exception ex)
                        {
                            ErrorLog.createLog("Error while reading file name : " + ftpObj.ftpFileName + " , ID of my sqldb : " + m_iFileId);
                            ErrorLog.CreateLog(ex);
                            //EdiError.LogError(EdiFile, m_iFileId.ToString(), ftpObj.ftpFileName, ex, "");
                            //WriteErrorLog(ex);
                            //System.IO.Directory.Move(sourcePath + "/" + FileName, UnProcessed + "/" + FileName);
                            System.IO.File.Copy(sourcePath + "/" + FileName, UnProcessed + "/" + FileName, true);
                            System.IO.File.Delete(sourcePath + "/" + FileName);
                            UnprocessFile = FileName + "^" + UnprocessFile;
                        }
                    }
                    if (intI > 0)
                    {

                        // EmailFile(UnprocessFile, 1, Processed);
                    }
                    if (UnprocessFile != "")
                    {
                        UnprocessFile = UnprocessFile.Trim('^');
                        if (UnprocessFile != "")
                        {
                            // EmailFile(UnprocessFile, 0, UnProcessed);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorLog.createLog(ex.ToString());
                ErrorLog.CreateLog(ex);
            }

            ErrorLog.createLog("*********************** Log end for creating pre receipt **************************");
        }

      


        static void ProcessFiletoMatchSignature()
        {
            try
            {
                //LogEvent.WriteEventLog("Test Start", LogEvent.EventEntryType.INFORMATION);
                if (!Directory.Exists(Upload_PATH.ToString()))
                    //Create Destination Directory if not exist
                    Directory.CreateDirectory(Upload_PATH.ToString());

                //Copy File from SFTP server 
                sftp = new Sftp();
                ErrorLog.createLog("before connecting to SFTP");
                //ErrorLog.createLog(
                sftp.Connect();
                ErrorLog.createLog("after connecting to SFTP");
                if (sftp.IsConnected())
                {
                    // LogEvent.WriteEventLog("SFTP connected", LogEvent.EventEntryType.INFORMATION);
                    //sftp.GetFile(targetFolder + "/new.txt", PDFFILLING_PDF_PATH.ToString(), null);

                    // target folder is path of sftp home dir and Upload_PATH is path where all sftp file were uploaded

                    sftp.GetAllFiles(targetFolder, Upload_PATH.ToString(), null);
                    ReadAllFiles(Upload_PATH, Processed_PATH, Unprocessed_PATH);
                    //LogEvent.WriteEventLog("File Copied", LogEvent.EventEntryType.INFORMATION);
                    //ReadFile(Processed_PATH);
                }

                //Process Copied File for Matching signature
                //MatchSignature ms = new MatchSignature();
                //ms.ProcessFile(fileName);
                //LogEvent.WriteEventLog("Processed", LogEvent.EventEntryType.INFORMATION);
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLog(ex);
                EdiError.LogError(EdiFile, m_iFileId.ToString(), "", ex, "Error in ProcessFiletoMatchSignature");
            }
            finally
            {
                sftp.Disconnect();
            }
        }

        static void ReadFile(String Path, int iFileId)
        {

            try
            {
                //int i = 0;
                //int b = 1 / i;     

                if (!string.IsNullOrEmpty(Path))
                {

                    var fileLines = System.IO.File.ReadAllLines(BusinessUtility.GetString(Path));

                    string sFileNameWithoutExtension = System.IO.Path.GetFileNameWithoutExtension(Path);

                    // oConfiguration = new Configuration();
                    // oConfiguration.PopulateConfigurationData(fileLines);
                    // EdiFile = oConfiguration.FileType;

                    if (fileLines.Length == 1)
                    {
                        var varLinesNew = fileLines[0].Split(new string[] { oConfiguration.LineSeperator }, StringSplitOptions.None);
                        fileLines = varLinesNew;
                        AppendLineSeperator(ref fileLines);


                    }

                    if (EdiFile == BusinessUtility.GetString(863))
                    {
                        if (oConfiguration.Activate863)
                        {
                            Console.WriteLine("Please wait while system is generating QDS...");
                            ErrorLog.createLog("*********************** Log start for creating pre receipt 863 **************************");
                            System.IO.File.WriteAllLines(Processed_PATH + "\\" + sFileNameWithoutExtension + "863_split.txt", fileLines);
                            Edi863Parser oEdi863Parser = new Edi863Parser(fileLines, iFileId);
                        }
                        else
                        {
                            Console.WriteLine("863 is disable ");
                            ErrorLog.createLog("863 is disable");
                        }
                    }
                    else if (EdiFile == BusinessUtility.GetString(856))
                    {
                        if (oConfiguration.Activate856)
                        {
                            Console.WriteLine("Please wait while system is generating pre-receipt...");
                            ErrorLog.createLog("*********************** Log start for creating pre receipt 856 **************************");
                            System.IO.File.WriteAllLines(Processed_PATH + "\\" + sFileNameWithoutExtension + "856_split.txt", fileLines);
                            ErrorLog.createLog("fileLines count :" + fileLines.Length);
                            EdiParser oEdiParser1 = new EdiParser(fileLines, iFileId);
                        }
                        else
                        {
                            Console.WriteLine("856 is disable ");
                            ErrorLog.createLog("856 is disable");
                        }
                    }

                    else if (EdiFile == BusinessUtility.GetString(990))
                    {
                        Console.WriteLine("Please wait while system is generating 990...");
                        ErrorLog.createLog("*********************** Log start for creating 990 **************************");
                        Edi990Parser oEdi990Parser = new Edi990Parser(Path, m_iFileId);
                    }

                    else if (EdiFile == BusinessUtility.GetString(214))
                    {
                        Console.WriteLine("Please wait while system is generating 214...");
                        ErrorLog.createLog("*********************** Log start for creating 214 **************************");
                        Edi214Parser oEdi990Parser = new Edi214Parser(Path, m_iFileId);
                    }



                }

            }
            catch (Exception ex)
            {
                EdiError.LogError(EdiFile, m_iFileId.ToString(), "", ex, "");
                ErrorLog.createLog(ex.ToString());
                ErrorLog.CreateLog(ex);
            }
            finally { }
        }
    }
}
