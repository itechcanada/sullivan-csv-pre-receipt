﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using iTECH.Library.Utilities;
using System.IO;
using System.Collections;
using System.Data;
using SecureFTP._990;
using SecureFTP._214;

namespace SecureFTP
{
    class Sftpcl : EdiDataSource
    {
        private Sftp sftp;
        int m_iFileId = 0;
        string m_sCusVenId = "";
        string EdiFile = string.Empty;
        Configuration oConfiguration = new Configuration();
        EdiDataSource oEdiDataSource = null;
        ArrayList arlFileList = new ArrayList();

        public Sftpcl(int iDataSourceId)
        {
            oEdiDataSource = new EdiDataSource(iDataSourceId);
            this.DataSourceDirectory = oEdiDataSource.DataSourceDirectory;
            this.DataUnProcessedDirectory = oEdiDataSource.DataUnProcessedDirectory;
            this.DataProcessedDirectory = oEdiDataSource.DataProcessedDirectory;
            this.DataUploadDirectory = oEdiDataSource.DataUploadDirectory;  // Data read from this dir and cleaned. File comes from DataDownloadDirectory to this dir while processing and after processing deleted.
            this.DataDownloadDirectory = oEdiDataSource.DataDownloadDirectory;


            //InputOutput.CheckDirectoryAndCreate(DataSourceDirectory);
            InputOutput.CheckDirectoryAndCreate(DataUnProcessedDirectory);
            InputOutput.CheckDirectoryAndCreate(DataProcessedDirectory);
            InputOutput.CheckDirectoryAndCreate(DataUploadDirectory);
            InputOutput.CheckDirectoryAndCreate(DataDownloadDirectory);

            ProcessFiletoMatchSignature(iDataSourceId);
        }



        public void ProcessFiletoMatchSignature(int iDataSourceId)
        {
            try
            {
                FileReceived oFileReceived = new FileReceived();

                    sftp = new Sftp(iDataSourceId);
                    sftp.Connect();
                    if (sftp.IsConnected())
                    {
                        ErrorLog.createLog("Connection to SFTP : successful");
                        sftp.GetAllFiles(DataSourceDirectory, DataDownloadDirectory, null);
                        arlFileList = sftp.fileNames; // This holds all files dowmloaded from sftp

                        foreach (string _file in arlFileList)
                        {
                           if (_file == "." || _file == ".." || _file == ".cache" || _file == "processed" || _file == "unprocessed" || _file == "997-ACK")
                            continue;

                           oFileReceived.ProcessFile(_file, DataDownloadDirectory, ref oFileReceived);
                            
                        }

                       

                        //if (InputOutput.WaitForFile())
                        //{
                        //    ReadAllFiles(DataUploadDirectory, DataProcessedDirectory, DataUnProcessedDirectory);
                        //}
                    }

                    DataTable dtFileReadyToProcessList = oFileReceived.GetFileReadyToBeProcessed();

                    int iFileReceivedId = 0;
                    string sFileReceivedDirectory = string.Empty;
                    string sFileReceivedName = string.Empty;

                    foreach (DataRow oDataRow in dtFileReadyToProcessList.Rows)
                    {
                        iFileReceivedId = BusinessUtility.GetInt(oDataRow["FileReceivedId"]);
                        sFileReceivedDirectory = BusinessUtility.GetString(oDataRow["FileReceivedDirectory"]);
                        sFileReceivedName = BusinessUtility.GetString(oDataRow["FileName"]);
                        InputOutput.CopyFileFromSourceToDestination(sFileReceivedDirectory + "\\" + sFileReceivedName, this.DataUploadDirectory + "\\" + sFileReceivedName);
                    }
                    ErrorLog.createLog("dtFileReadyToProcessList.Rows.Count =" + dtFileReadyToProcessList.Rows.Count);
                    if (dtFileReadyToProcessList.Rows.Count > 0)
                    {
                        ReadAllFiles(DataUploadDirectory, DataProcessedDirectory, DataUnProcessedDirectory);
                    }
                    else
                    {
                        Console.WriteLine("No data available to process...");
                    }


            }
            catch (Exception ex)
            {
                ErrorLog.CreateLog(ex);
                EdiError.LogError(EdiFile, m_iFileId.ToString(), "", ex, "Error in ProcessFiletoMatchSignature");
            }
            finally
            {
                sftp.Disconnect();
            }
        }

                  

        public void ReadAllFiles(String sourcePath, String Processed, String UnProcessed)
        {
            FTPTraking ftpObj = new FTPTraking();
            try
            {
                String[] FileInfo;
                String FileName;
                String UnprocessFile = "";
                int intI = 0;
                if (Directory.Exists(sourcePath))
                {
                    FileInfo = Directory.GetFiles(sourcePath);
                    int i;
                    if (FileInfo.Length == 0)
                    {
                        ErrorLog.createLog("No file available to process..");
                        Console.WriteLine("No file available to process..");
                    }
                    for (i = 0; i <= FileInfo.Length - 1; i++)
                    {
                        FileName = Path.GetFileName(FileInfo[i]);

                        if (InputOutput.WaitForFile(sourcePath + "/" + FileName))
                        {

                            try
                            {
                                ErrorLog.createLog("sourcePath + FileName = " + sourcePath + "/" + FileName);
                                ErrorLog.createLog("Processed +  FileName = " + Processed + "/" + FileName);
                                System.IO.File.Copy(sourcePath + "/" + FileName, Processed + "/" + FileName, true);
                                FileInfo oFileInfo = new FileInfo(sourcePath + "/" + FileName);
                                long lngFileLength = oFileInfo.Length;
                                if (lngFileLength > 0)
                                {
                                    Files oFiles = new Files();
                                    FileInfo o = new FileInfo(BusinessUtility.GetString(sourcePath + "/" + FileName));
                                    if (o.Extension.Contains(".xml"))
                                    {
                                        ErrorLog.createLog("Xml inside automation start processing.");
                                        string Path = BusinessUtility.GetString(sourcePath + "/" + FileName);
                                        string sFileLine = System.IO.File.ReadAllText(Path);
                                        EdiFile = Utility.GetFileTypeFromXml(sFileLine);
                                        string sDunsno = Utility.GetDUNSNumberFromXml(sFileLine);
                                        oConfiguration = new Configuration(sDunsno, BusinessUtility.GetInt(EdiFile));
                                        m_sCusVenId = BusinessUtility.GetString(oConfiguration.VendorId);
                                        ftpObj = new FTPTraking();
                                        ftpObj.ftpFileName = BusinessUtility.GetString(Path);
                                        ftpObj.ftpFilePath = BusinessUtility.GetString(Path);
                                        oFiles.VendorId = m_sCusVenId;
                                        oFiles.FileType = BusinessUtility.GetInt(EdiFile);
                                        oFiles.VendorName = oConfiguration.VendorName;
                                        oFiles.CustomerName = oConfiguration.CustomerName;
                                        oFiles.Mill = oConfiguration.MillId;
                                        oFiles.VendorDunsNo = oConfiguration.VendorDunsNo;
                                        m_iFileId = oFiles.insertFileStatus(ftpObj);
                                        Globalcl.SetFileId(m_iFileId);
                                    }
                                    else
                                    {

                                        var fileLines = System.IO.File.ReadAllLines(BusinessUtility.GetString(sourcePath + "/" + FileName));

                                        oConfiguration = new Configuration();
                                        oConfiguration.PopulateConfigurationData(fileLines);
                                        EdiFile = oConfiguration.FileType;
                                        m_sCusVenId = BusinessUtility.GetString(oConfiguration.VendorId);

                                        // Insert file Status
                                        ftpObj.ftpFileName = BusinessUtility.GetString(FileName);
                                        ftpObj.ftpFilePath = BusinessUtility.GetString(sourcePath + "/" + FileName);
                                        //ftpObj.Status = BusinessUtility.GetInt(FileStatus.Processed);
                                        oFiles = new Files();
                                        oFiles.VendorId = m_sCusVenId;
                                        oFiles.FileType = BusinessUtility.GetInt(EdiFile);
                                        oFiles.VendorName = oConfiguration.VendorName;
                                        oFiles.Mill = oConfiguration.MillId;
                                        oFiles.CustomerName = oConfiguration.CustomerName;
                                        oFiles.VendorDunsNo = oConfiguration.VendorDunsNo;
                                    }
                                    FileReceived oFileReceived = new FileReceived(FileName);
                                    oFiles.FileReceivedId = oFileReceived.FileReceivedId;

                                    m_iFileId = oFiles.insertFileStatus(ftpObj);
                                    Globalcl.SetFileId(m_iFileId);



                                    ErrorLog.createLog("Log start for fileid m_iFileId = " + m_iFileId);
                                    ReadFile(sourcePath + "/" + FileName, m_iFileId);

                                    ErrorLog.createLog("Updating FileReceived ID :" + oFileReceived.FileReceivedId);
                                    oFileReceived.UpdateProcessedReceiveFile(oFileReceived.FileReceivedId);

                                    // ErrorLog.createLog("File Added Successfully");
                                    // End Block

                                    ErrorLog.createLog("processing file name : " + ftpObj.ftpFileName + " , ID of my sqldb : " + m_iFileId);
                                    System.IO.File.Delete(sourcePath + "/" + FileName);

                                    // wait sometime for another execution
                                    Utility.WaitForSecond(5);

                                }
                                intI += 1;
                            }


                            catch (Exception ex)
                            {
                                ErrorLog.createLog("Error while reading file name : " + ftpObj.ftpFileName + " , ID of my sqldb : " + m_iFileId);
                                ErrorLog.CreateLog(ex);
                                //EdiError.LogError(EdiFile, m_iFileId.ToString(), ftpObj.ftpFileName, ex, "");
                                //WriteErrorLog(ex);
                                //System.IO.Directory.Move(sourcePath + "/" + FileName, UnProcessed + "/" + FileName);
                                System.IO.File.Copy(sourcePath + "/" + FileName, UnProcessed + "/" + FileName, true);
                                System.IO.File.Delete(sourcePath + "/" + FileName);
                                UnprocessFile = FileName + "^" + UnprocessFile;
                            }

                        }

                    }
                    if (intI > 0)
                    {

                        // EmailFile(UnprocessFile, 1, Processed);
                    }
                    if (UnprocessFile != "")
                    {
                        UnprocessFile = UnprocessFile.Trim('^');
                        if (UnprocessFile != "")
                        {
                            // EmailFile(UnprocessFile, 0, UnProcessed);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorLog.createLog(ex.ToString());
                ErrorLog.CreateLog(ex);
            }

            ErrorLog.createLog("*********************** Log end for creating pre receipt **************************");
        }



        public void ReadFile(String Path, int iFileId)
        {
            try
            {
                if (!string.IsNullOrEmpty(Path))
                {
                    var fileLines = System.IO.File.ReadAllLines(BusinessUtility.GetString(Path));
                    string sFileNameWithoutExtension = System.IO.Path.GetFileNameWithoutExtension(Path);

                    if (fileLines.Length == 1)
                    {
                        var varLinesNew = fileLines[0].Split(new string[] { oConfiguration.LineSeperator }, StringSplitOptions.None);
                        fileLines = varLinesNew;
                        AppendLineSeperator(ref fileLines);
                    }

                    if (oConfiguration.SingleFileFor856863)
                    {
                        string m_sSingleFilePath = Path;
                        EdiSeperator oEdiSeperator = new EdiSeperator();
                        oEdiSeperator.ProcessFile(fileLines);


                        if (oEdiSeperator.lstComplete863Content.Count > 0)
                        {
                            if (oConfiguration.Activate863)
                            {
                                EdiFile = Files.ExecutionFileType.EDI863;
                                m_sCusVenId = BusinessUtility.GetString(oConfiguration.VendorId);

                                FTPTraking ftpObj = new FTPTraking();
                                ftpObj.ftpFileName = BusinessUtility.GetString(m_sSingleFilePath);
                                ftpObj.ftpFilePath = BusinessUtility.GetString(m_sSingleFilePath);
                                Files oFiles = new Files();
                                oFiles.VendorId = m_sCusVenId;
                                oFiles.FileType = BusinessUtility.GetInt(EdiFile);
                                oFiles.VendorName = oConfiguration.VendorName;
                                oFiles.CustomerName = oConfiguration.CustomerName;
                                oFiles.Mill = oConfiguration.MillId;
                                oFiles.VendorDunsNo = oConfiguration.VendorDunsNo;
                                m_iFileId = oFiles.insertFileStatus(ftpObj);

                                Console.WriteLine("Please wait while system is generating QDS...");
                                ErrorLog.createLog("*********************** Log start for creating  863 **************************");
                                try
                                {
                                    System.IO.File.WriteAllLines(DataProcessedDirectory + "\\" + ftpObj.ftpFileName + "863_split.txt", fileLines);
                                }
                                catch (Exception ex)
                                {
                                    ErrorLog.CreateLog(ex);
                                }
                                Edi863Parser oEdi863Parser = new Edi863Parser(oEdiSeperator.lstComplete863Content.ToArray(), m_iFileId);
                            }
                            else
                            {
                                Console.WriteLine("863 is disable ");
                                ErrorLog.createLog("863 is disable");
                            }


                            //Processed_PATH 856
                            if (oConfiguration.Activate856)
                            {
                                ErrorLog.createLog("m_sSingleFilePath = " + m_sSingleFilePath);
                                // ErrorLog.createLog("m_iExecuteSingleFile = " + m_iExecuteSingleFile);

                                fileLines = System.IO.File.ReadAllLines(BusinessUtility.GetString(m_sSingleFilePath));

                                oConfiguration = new Configuration();
                                oConfiguration.PopulateConfigurationData(fileLines);
                                EdiFile = oConfiguration.FileType;
                                m_sCusVenId = BusinessUtility.GetString(oConfiguration.VendorId);

                                FTPTraking ftpObj = new FTPTraking();
                                ftpObj.ftpFileName = BusinessUtility.GetString(m_sSingleFilePath);
                                ftpObj.ftpFilePath = BusinessUtility.GetString(m_sSingleFilePath);
                                Files oFiles = new Files();
                                oFiles.VendorId = m_sCusVenId;
                                oFiles.FileType = BusinessUtility.GetInt(EdiFile);
                                oFiles.VendorName = oConfiguration.VendorName;
                                oFiles.CustomerName = oConfiguration.CustomerName;
                                oFiles.Mill = oConfiguration.MillId;
                                oFiles.VendorDunsNo = oConfiguration.VendorDunsNo;
                                m_iFileId = oFiles.insertFileStatus(ftpObj);
                                Globalcl.SetFileId(m_iFileId);

                                System.IO.File.WriteAllLines(DataProcessedDirectory + "\\" + ftpObj.ftpFileName + "_split.txt", fileLines);
                                ErrorLog.createLog("fileLines count :" + fileLines.Length);
                                EdiParser oEdiParser1 = new EdiParser(fileLines, m_iFileId);

                                ErrorLog.createLog("EdiFile = " + EdiFile);
                                ErrorLog.createLog("m_iCusVenId = " + m_sCusVenId);

                            }


                        }
                        else
                        {
                            ErrorLog.createLog("863 content not found");
                        }
                    }

                    else
                    {
                        FileInfo o = new FileInfo(Path);
                        if (o.Extension.Contains(".xml"))
                        {
                            // xml 
                            ErrorLog.createLog("xml processing started..");
                            if (EdiFile == Configuration.EdiFileType.EDI856)
                            {
                                if (oConfiguration.Activate856)
                                {
                                    //Edi856XmlParser oEdi856XmlParser = new Edi856XmlParser(Path, m_iFileId);
                                }
                                ErrorLog.createLog("856 processing is off");
                                Console.WriteLine("856 processing is off");

                            }
                            else if (EdiFile == Configuration.EdiFileType.EDI863)
                            {
                                if (oConfiguration.Activate863)
                                {
                                    //Edi863XmlParser oEdi856XmlParser = new Edi863XmlParser(Path, m_iFileId);
                                }
                                else
                                {
                                    ErrorLog.createLog("863 processing is off");
                                    Console.WriteLine("863 processing is off");
                                }
                            }

                            //
                        }

                        {
                            if (EdiFile == BusinessUtility.GetString(863))
                            {
                                if (oConfiguration.Activate863)
                                {
                                    Console.WriteLine("Please wait while system is generating QDS...");
                                    ErrorLog.createLog("*********************** Log start for creating pre receipt 863 **************************");
                                    System.IO.File.WriteAllLines(DataProcessedDirectory + "\\" + sFileNameWithoutExtension + "_split.txt", fileLines);
                                    Edi863Parser oEdi863Parser = new Edi863Parser(fileLines, iFileId);
                                }
                                else
                                {
                                    ErrorLog.createLog("863 processing is off");
                                    Console.WriteLine("863 processing is off");
                                }
                            }
                            else if (EdiFile == BusinessUtility.GetString(856))
                            {
                                if (oConfiguration.Activate856)
                                {
                                    Console.WriteLine("Please wait while system is generating pre-receipt...");
                                    ErrorLog.createLog("*********************** Log start for creating pre receipt 856 **************************");
                                    System.IO.File.WriteAllLines(DataProcessedDirectory + "\\" + sFileNameWithoutExtension + "_split.txt", fileLines);
                                    ErrorLog.createLog("fileLines count :" + fileLines.Length);
                                    EdiParser oEdiParser1 = new EdiParser(fileLines, iFileId);
                                }
                                else
                                {
                                    ErrorLog.createLog("856 processing is off");
                                    Console.WriteLine("856 processing is off");
                                }
                            }
                            else if (EdiFile == BusinessUtility.GetString(990))
                            {
                                Console.WriteLine("Please wait while system is generating 990...");
                                ErrorLog.createLog("*********************** Log start for creating 990 **************************");
                                Edi990Parser oEdi990Parser = new Edi990Parser(Path, iFileId);
                            }
                            else if (EdiFile == BusinessUtility.GetString(214))
                            {
                                Console.WriteLine("Please wait while system is generating 214...");
                                ErrorLog.createLog("*********************** Log start for creating 214 **************************");
                                Edi214Parser oEdi990Parser = new Edi214Parser(Path, m_iFileId);
                            }
                        }

                    }

                }
            }
            catch (Exception ex)
            {
                EdiError.LogError(EdiFile, m_iFileId.ToString(), "", ex, "");
                ErrorLog.CreateLog(ex);
            }
            finally { }
        }


        public void AppendLineSeperator(ref string[] sArr)
        {
            string[] sLine = new string[sArr.Length]; ;
            ArrayList oArrayList = new ArrayList();
            for (int i = 0; i < sArr.Length - 1; i++)
            {
                oArrayList.Add(sArr[i] + oConfiguration.LineSeperator);
                //sLine[i].Insert(i, sArr[i] + oConfiguration.LineSeperator);
            }

            // object obj = oArrayList.ToArray();
            sArr = (string[])oArrayList.ToArray(typeof(string));
            //sArr = oArrayList.ToArray();
        }




    }
}
