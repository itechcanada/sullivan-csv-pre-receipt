﻿using iTECH.Library.Utilities;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace SecureFTP
{
    class LocalDisk : EdiDataSource
    {

        int m_iFileId = 0;
        string m_sCusVenId ="";
        string EdiFile = string.Empty;
        Configuration oConfiguration = new Configuration();

        public LocalDisk(string sSourceDir, string sUploadDir, string sProcessedDir, string sUnProcessedDir)
        {
            this.DataSourceDirectory = sSourceDir;
            this.DataUnProcessedDirectory = sUnProcessedDir;
            this.DataProcessedDirectory = sProcessedDir;
            this.DataUploadDirectory = sUploadDir;

            InputOutput.CheckDirectoryAndCreate(DataSourceDirectory);
            InputOutput.CheckDirectoryAndCreate(DataUnProcessedDirectory);
            InputOutput.CheckDirectoryAndCreate(DataProcessedDirectory);
            InputOutput.CheckDirectoryAndCreate(DataUploadDirectory);


            ProcessFiletoMatchSignature();
        }



        public void ProcessFiletoMatchSignature()
        {
            try
            {
                InputOutput.MoveDirectory(DataSourceDirectory, DataUploadDirectory);
                ReadAllFiles(DataUploadDirectory, DataProcessedDirectory, DataUnProcessedDirectory);
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLog(ex);
            }
            finally
            {
            }
        }


        public void ReadAllFiles(String sourcePath, String Processed, String UnProcessed)
        {
            FTPTraking ftpObj = new FTPTraking();
            try
            {
                String[] FileInfo;
                String FileName;
                String UnprocessFile = "";
                int intI = 0;
                if (Directory.Exists(sourcePath))
                {
                    FileInfo = Directory.GetFiles(sourcePath);
                    int i;
                    if (FileInfo.Length == 0)
                    {
                        ErrorLog.createLog("No file available to process..");
                        Console.WriteLine("No file available to process..");
                    }
                    for (i = 0; i <= FileInfo.Length - 1; i++)
                    {
                        FileName = Path.GetFileName(FileInfo[i]);
                        try
                        {
                           // RedXML(sourcePath + "/" + FileName);
                           
                            //System.IO.Directory.Move(sourcePath + "/" + FileName, Processed + "/" + FileName);
                            System.IO.File.Copy(sourcePath + "/" + FileName, Processed + "/" + FileName, true);


                            FileInfo oFileInfo = new FileInfo(sourcePath + "/" + FileName);
                            long lngFileLength = oFileInfo.Length;
                            if (lngFileLength > 0)
                            {

                                var fileLines = System.IO.File.ReadAllLines(BusinessUtility.GetString(sourcePath + "/" + FileName));
                                oConfiguration = new Configuration();
                                oConfiguration.PopulateConfigurationData(fileLines);
                                EdiFile = oConfiguration.FileType;
                                m_sCusVenId = BusinessUtility.GetString(oConfiguration.VendorId);

                                // Insert file Status
                                ftpObj.ftpFileName = BusinessUtility.GetString(FileName);
                                ftpObj.ftpFilePath = BusinessUtility.GetString(sourcePath + "/" + FileName);
                                //ftpObj.Status = BusinessUtility.GetInt(FileStatus.Processed);
                                Files oFiles = new Files();
                                oFiles.VendorId = m_sCusVenId;
                                oFiles.FileType = BusinessUtility.GetInt(EdiFile);
                                oFiles.VendorName = oConfiguration.VendorName;
                                oFiles.Mill = oConfiguration.MillId;
                                oFiles.CustomerName = oConfiguration.CustomerName;
                                oFiles.VendorDunsNo = oConfiguration.VendorDunsNo;
                                m_iFileId = oFiles.insertFileStatus(ftpObj);
                                Globalcl.SetFileId(m_iFileId);

                                ReadFile(sourcePath + "/" + FileName, m_iFileId);

                                // ErrorLog.createLog("File Added Successfully");
                                // End Block

                                ErrorLog.createLog("processing file name : " + ftpObj.ftpFileName + " , ID of my sqldb : " + m_iFileId);
                                System.IO.File.Delete(sourcePath + "/" + FileName);
                            }
                           intI += 1;
                        }
                        catch (Exception ex)
                        {
                            ErrorLog.createLog("Error while reading file name : " + ftpObj.ftpFileName + " , ID of my sqldb : " + m_iFileId);
                            ErrorLog.CreateLog(ex);
                            //EdiError.LogError(EdiFile, m_iFileId.ToString(), ftpObj.ftpFileName, ex, "");
                            //WriteErrorLog(ex);
                            //System.IO.Directory.Move(sourcePath + "/" + FileName, UnProcessed + "/" + FileName);
                            System.IO.File.Copy(sourcePath + "/" + FileName, UnProcessed + "/" + FileName, true);
                            System.IO.File.Delete(sourcePath + "/" + FileName);
                            UnprocessFile = FileName + "^" + UnprocessFile;
                        }
                    }


                }
            }
            catch (Exception ex)
            {
                ErrorLog.createLog(ex.ToString());
            }
        }



        public void ReadFile(String Path, int iFileId)
        {

            try
            {
                //int i = 0;
                //int b = 1 / i;     

                if (!string.IsNullOrEmpty(Path))
                {

                    var fileLines = System.IO.File.ReadAllLines(BusinessUtility.GetString(Path));

                    string sFileNameWithoutExtension = System.IO.Path.GetFileNameWithoutExtension(Path);

                    // oConfiguration = new Configuration();
                    // oConfiguration.PopulateConfigurationData(fileLines);
                    // EdiFile = oConfiguration.FileType;

                    if (fileLines.Length == 1)
                    {
                        var varLinesNew = fileLines[0].Split(new string[] { oConfiguration.LineSeperator }, StringSplitOptions.None);
                        fileLines = varLinesNew;
                        AppendLineSeperator(ref fileLines);


                    }

                    if (EdiFile == BusinessUtility.GetString(863))
                    {
                        if (oConfiguration.Activate863)
                        {
                            Console.WriteLine("Please wait while system is generating QDS...");
                            ErrorLog.createLog("*********************** Log start for creating pre receipt 863 **************************");
                            System.IO.File.WriteAllLines(DataProcessedDirectory + "\\" + sFileNameWithoutExtension + "_split.txt", fileLines);
                            Edi863Parser oEdi863Parser = new Edi863Parser(fileLines, iFileId);
                        }
                    }
                    else if (EdiFile == BusinessUtility.GetString(856))
                    {
                        if (oConfiguration.Activate856)
                        {
                            Console.WriteLine("Please wait while system is generating pre-receipt...");
                            ErrorLog.createLog("*********************** Log start for creating pre receipt 856 **************************");
                            System.IO.File.WriteAllLines(DataProcessedDirectory + "\\" + sFileNameWithoutExtension + "_split.txt", fileLines);
                            ErrorLog.createLog("fileLines count :" + fileLines.Length);
                            EdiParser oEdiParser1 = new EdiParser(fileLines, iFileId);
                        }
                    }



                }

            }
            catch (Exception ex)
            {
                EdiError.LogError(EdiFile, m_iFileId.ToString(), "", ex, "");
            }
            finally { }
        }


        public void AppendLineSeperator(ref string[] sArr)
        {
            string[] sLine = new string[sArr.Length]; ;
            ArrayList oArrayList = new ArrayList();
            for (int i = 0; i < sArr.Length - 1; i++)
            {
                oArrayList.Add(sArr[i] + oConfiguration.LineSeperator);
                //sLine[i].Insert(i, sArr[i] + oConfiguration.LineSeperator);
            }

            // object obj = oArrayList.ToArray();
            sArr = (string[])oArrayList.ToArray(typeof(string));
            //sArr = oArrayList.ToArray();
        }



    }
}
