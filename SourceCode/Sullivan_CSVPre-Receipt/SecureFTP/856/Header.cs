﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using iTECH.Library;
using MySql.Data.MySqlClient;
using System.Data;

namespace SecureFTP
{
    class Header
    {

        string m_sSql = string.Empty;
        StringBuilder m_sbSql = null;
        static string sStratixConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ToString();
        static string sMysqlConnectionString = ConfigurationManager.ConnectionStrings["NewConnectionString"].ToString();



        string sHeaderId = string.Empty;
        public string HeaderId
        {
            get { return sHeaderId; }
            set { sHeaderId = value; }
        }

        string sFileId = string.Empty;
        public string FileId
        {
            get { return sFileId; }
            set { sFileId = value; }
        }

        bool bReceiptCreated = false;
        public bool ReceiptCreated
        {
            get { return bReceiptCreated; }
            set { bReceiptCreated = value; }
        }

        int iProcessType = 0;
        public int ProcessType
        {
            get { return iProcessType; }
            set { iProcessType = value; }
        }
        


        public void UpdateHeader(int iHeader, int iProcessType)
        {
            m_sbSql = new StringBuilder();
            m_sbSql.Append(" UPDATE header set ReceiptCreated = 1 , ProcessType = " + iProcessType + "  WHERE  HeaderId = " + iHeader + " ");
            iTECH.Library.DataAccess.MySql.DbHelper oDbHelper = new iTECH.Library.DataAccess.MySql.DbHelper(sMysqlConnectionString, true);
            try
            {
                MySqlParameter[] oMySqlParameter = { };
                oDbHelper.ExecuteNonQuery(m_sbSql.ToString(), CommandType.Text, oMySqlParameter);
            }
            catch (Exception ex)
            {
                ErrorLog.createLog(ex.ToString());
            }
            finally
            {
                oDbHelper.CloseDatabaseConnection();
            }

        }

        public void UpdateHeader(int iHeader, string sWhs, string sShipperNo, string sCarrierName, string sCarrierVehicleDesc)
        {
            m_sbSql = new StringBuilder();
            m_sbSql.Append(" UPDATE header set RcvWhs = '" + sWhs + "', VenShpRef = '" + sShipperNo + "', CarrierName = '"+ sCarrierName + "', CarrierVehicleDesc = '"+ sCarrierVehicleDesc + "'  WHERE  HeaderId = " + iHeader);
            ErrorLog.createLog(m_sbSql.ToString());
            iTECH.Library.DataAccess.MySql.DbHelper oDbHelper = new iTECH.Library.DataAccess.MySql.DbHelper(sMysqlConnectionString, true);
            try
            {
                MySqlParameter[] oMySqlParameter = { };
                oDbHelper.ExecuteNonQuery(m_sbSql.ToString(), CommandType.Text, oMySqlParameter);
            }
            catch (Exception ex)
            {
                ErrorLog.createLog(ex.ToString());
            }
            finally
            {
                oDbHelper.CloseDatabaseConnection();
            }

        }

        public DataTable GetHeaderList(int iFileId)
        {
            DataTable dt = new DataTable();
            m_sbSql = new StringBuilder();
            m_sbSql.Append(" Select * from header  WHERE 1=1 AND  (FileId = " + iFileId + " OR " + iFileId +" = 0) AND  ReceiptCreated  = 0 AND ProcessType = 0 ");
            iTECH.Library.DataAccess.MySql.DbHelper oDbHelper = new iTECH.Library.DataAccess.MySql.DbHelper(sMysqlConnectionString, true);
            try
            {
                MySqlParameter[] oMySqlParameter = { };
                dt = oDbHelper.GetDataTable(m_sbSql.ToString(), CommandType.Text, oMySqlParameter);
            }
            catch (Exception ex)
            {
                ErrorLog.createLog(ex.ToString());
            }
            finally
            {
                oDbHelper.CloseDatabaseConnection();
            }
            return dt;
        }


        public int HeaderInsertion(int iFileId, Shipment oShipment)
        {

            //Shipment oShipment = new Shipment();
            m_sbSql = new StringBuilder();
            int returnHeaderID = 0;
            iTECH.Library.DataAccess.MySql.DbHelper oDbHelper = new iTECH.Library.DataAccess.MySql.DbHelper(sMysqlConnectionString, true);
            try
            {
                m_sbSql.Append(" INSERT INTO HEADER(FileId, CusVenId, ShipTo, VenShpRef, CarrierVehicleDesc, RcvWhs, DeliveryMethod, FreightVenId, CarrierName, CarrierRefNo, PreRcptStatus)");
                m_sbSql.Append(" VALUES ('" + iFileId + "', '" + oShipment.CusVenId + "', '" + oShipment.ShipTo + "', '" + oShipment.VenShpRef + "', '" + oShipment.CarrierVehicleDesc + "', '" + oShipment.RcvWhs + "' ");
                m_sbSql.Append(" , '" + oShipment.DeliveryMethod + "', '" + oShipment.FreightVenId + "', '" + oShipment.CarrierName + "', '" + oShipment.CarrierRefNo + "', '" + oShipment.PreRcptStatus + "'   )  ");

                MySqlParameter[] oMySqlParameter = { };

                int iStatus = oDbHelper.ExecuteNonQuery(m_sbSql.ToString(), CommandType.Text, oMySqlParameter);
                iStatus = oDbHelper.GetLastInsertID();
                returnHeaderID = iStatus;
                

            }
            catch (Exception ex)
            {
                ErrorLog.createLog(ex.ToString());
                EdiError.LogError(Globalcl.FileType, Globalcl.FileId.ToString(), "", ex, "", 0, 0);
            }
            finally
            {
                oDbHelper.CloseDatabaseConnection();
            }
            return returnHeaderID;

        }

      





    }
}
