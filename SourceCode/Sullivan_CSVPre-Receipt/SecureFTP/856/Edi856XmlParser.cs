﻿using iTECH.Library.Utilities;
using System;
using System.Configuration;
using System.IO;
using System.Xml;
using System.Xml.Linq;

namespace SecureFTP
{
    class Edi856XmlParser
    {
        public static string m_XmlData = string.Empty;
        MyXmlReader m_fullXml = null;
        public static XmlNodeList m_HeaderXmlNodeList = null;
        public static XmlNodeList m_DetailXmlNodeList = null;
        public static XmlNodeList m_ItemXmlNodeList = null;
        MyXmlReader omyXmlReader = null;
        Files oFiles = null;
        int m_iFileId = 0;
        int m_iHeaderId = 0;
        int m_iDetailId = 0;
        int m_iDtailItem = 0;
        static string sStratixConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ToString();
        static string sMysqlConnectionString = ConfigurationManager.ConnectionStrings["NewConnectionString"].ToString();

        static string m_sCompanyId = ConfigurationManager.AppSettings["CompanyId"];
        static string m_sIntchgPfx = ConfigurationManager.AppSettings["IntchgPfx"];
        static string m_sCusVenId = ConfigurationManager.AppSettings["CusVenId"];
        static string m_sRcptType = ConfigurationManager.AppSettings["RcptType"];
        string sLocation = string.Empty;
        string m_sShipFrom = string.Empty;
        int m_iWarehouse = 0;
        string m_sBol = string.Empty;
        string m_s856Content = string.Empty;

        public Edi856XmlParser(XmlNodeList HeaderXmlNodeList, XmlNodeList BodyXmlNodeList, int iFileId, string s856List)
        {
            m_HeaderXmlNodeList = HeaderXmlNodeList;
            m_DetailXmlNodeList = BodyXmlNodeList;
            m_iFileId = iFileId;
            m_s856Content = s856List;
            oFiles = new Files(m_iFileId);
            try
            {
                //m_XmlData = File.ReadAllText(sXmlPath);
                ParseData();
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLog(ex);
            }
        }

        public void ParseData()
        {
            try
            {
                foreach (XmlElement oNode in m_DetailXmlNodeList)
                {
                    if (m_ItemXmlNodeList == null)
                    {
                        XmlElement onodelist = oNode;
                        m_s856Content = onodelist.OuterXml.Replace("ch856:", string.Empty);
                        omyXmlReader = new MyXmlReader(m_s856Content);
                        #region to process multiple orders      2020/02/28  Sumit
                        m_ItemXmlNodeList = omyXmlReader.GetNodeList("Transaction/ShipNotice/Orders/Order");
                        //m_ItemXmlNodeList = omyXmlReader.GetNodeList("Transaction/ShipNotice/Orders");
                        #endregion to process multiple orders      2020/02/28  Sumit
                    }
                }
                ParseHeader();
                //ParseDeatil();
                ParseDeatil_New();
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLog(ex);
            }
        }

        Shipment oShipment = new Shipment();
        public void ParseHeader()
        {
            try
            {
                foreach (XmlElement oNode in m_HeaderXmlNodeList)
                {
                    XmlElement onodelist = oNode;
                    omyXmlReader = new MyXmlReader(onodelist.OuterXml);

                    var xDoc = XDocument.Parse(oNode.InnerXml);
                    var QdsChildList = xDoc.Descendants("Reference");
                    if (QdsChildList != null)
                    {
                        foreach (XElement element in QdsChildList)
                        {
                            m_sBol = element.ToString();
                            if(m_sBol.Contains("Reference") && m_sBol.Contains("TypeCode=\"MA\""))
                            {
                                MyXmlReader oMyXmlReader = new MyXmlReader(m_sBol);
                                m_sBol = oMyXmlReader.GetNodeText("Reference");
                                break;
                            }
                        }
                    }


                    var RoutingCarrierName = xDoc.Descendants("RoutingCarrierName");
                    foreach (XElement element in RoutingCarrierName)
                    {
                        string s = element.ToString();
                        if (s.Contains("RoutingCarrierName"))
                        {
                            MyXmlReader oMyXmlReader = new MyXmlReader(s);
                            oShipment.ShipTo = oMyXmlReader.GetNodeText("RoutingCarrierName");
                            break;
                        }
                    }
                    oShipment.VenShpRef = m_sBol;
                    oShipment.CarrierVehicleDesc = oShipment.ShipTo;
                    oShipment.DeliveryMethod = "";
                    oShipment.CarrierName = oShipment.ShipTo;
                    oShipment.CarrierRefNo = oShipment.ShipTo;
                    sLocation = oShipment.ShipTo;
                    oShipment.PreRcptStatus = BusinessUtility.GetString(ConfigurationManager.AppSettings["PreRcptStatus"]);
                    Header oHeader = new Header();
                    m_iHeaderId = oHeader.HeaderInsertion(Globalcl.FileId, oShipment);

                }
            }
            catch {
                ErrorLog.createLog("Error while parsing header data");
            }

        }
        public void ParseDeatil()
        {
            iTECH.Library.DataAccess.MySql.DbHelper oDbHelper = new iTECH.Library.DataAccess.MySql.DbHelper(sMysqlConnectionString, true);
            try
            {
                int iItm = 0;
                foreach (XmlElement oNode in m_ItemXmlNodeList)
                {
                    iItm = iItm + 1;
                    Details oDetails = new Details();
                    XmlElement onodelist = oNode;
                    omyXmlReader = new MyXmlReader(onodelist.OuterXml);
                    oDetails.PrntNo = omyXmlReader.GetNode("Order/PONumber").InnerText;
                    oDetails.PrntItm = "1";
                    string[] sar = oDetails.PrntNo.Split('-');
                    if(sar.Length > 1)
                    {
                        oDetails.PrntNo = sar[1];
                    }
                    oDetails.PrntSubItm = "1";
                    oDetails.ShipperNo = m_sBol; // bol
                    oDetails.IntchgSItm = "1";
                    oDetails.ShptNo = oDetails.PrntNo;
                        //omyXmlReader.GetNode("Item/ItemURN").InnerText;
                    oDetails.CusRtnMthd = "";
                    oDetails.ShpGrsPcs = Utility.GetConfigValue("ShpGrsPcs54");
                    oDetails.ShpGrsMsr = Utility.GetConfigValue("ShpGrsMsr54");
                    oDetails.ShpGrsWgt = omyXmlReader.GetNode("Order/Lines/Line/PhysicalProps/Dimensions/Weight").InnerText;
                    oDetails.OrigShpntPcs = omyXmlReader.GetNode("Order/Lines/Line/PhysicalProps/Dimensions/Count").InnerText;
                    var CoilLength = "";
                    if (omyXmlReader.GetNode("Order/Lines/Line/PhysicalProps/Dimensions/Length") != null)
                    {
                        CoilLength = omyXmlReader.GetNode("Order/Lines/Line/PhysicalProps/Dimensions/Length").InnerText;
                    }
                    else
                    {
                        CoilLength = "0";
                    }
                    oDetails.OrigShpntMsr = CoilLength;

                    string OrderedWeight = "";

                    OrderedWeight = oDetails.ShpGrsWgt;

                    if (!string.IsNullOrEmpty(OrderedWeight))
                    {
                        oDetails.OrigShptWgt = OrderedWeight;
                    }
                    else
                    {
                        oDetails.OrigShptWgt = "1.0";
                    }
                    oDetails.HeatNo = omyXmlReader.GetNode("Order/Lines/Line/Pack/Reference").InnerText;
                    tctipd otctipd = new tctipd();
                    //Shipment oShipment = new Shipment(); oShipment
                    otctipd.PopulateWarehouse(ref oShipment , oDetails.PrntNo, m_sCompanyId);
                    m_iWarehouse = BusinessUtility.GetInt(oShipment.RcvWhs);
                    Header oHeader = new Header();
                    oShipment.VenShpRef = m_sBol; // bol
                    oHeader.UpdateHeader(m_iHeaderId, oShipment.RcvWhs, oShipment.VenShpRef, oShipment.CarrierName, oShipment.CarrierVehicleDesc);
                    Utility.GetPoDetails(BusinessUtility.GetInt(oDetails.PrntNo), ref oDetails);
                    oDetails.MultiplePoItemExist = 1;
                    oDetails.IntchgItm = iItm.ToString();
                    m_iDetailId = oDetails.InsertDetails(m_iHeaderId, oDetails, oDbHelper);
                    if(m_iDetailId > 0)
                    {
                        XmlNodeList itemlist = omyXmlReader.GetNodeList("Order/Lines/Line/Pack");
                        if (itemlist.Count > 0)
                        {
                            this.ParseDetailItem(itemlist, BusinessUtility.GetInt(oDetails.PrntNo), BusinessUtility.GetInt(oDetails.PrntItm), iItm, oDetails.HeatNo, oDetails.Length, oDetails.OrigShptWgt);
                        }
                        else
                        {
                            ErrorLog.createLog("No item node found please check xml doc");
                        }

                    }

                }
            }
            catch (Exception ex)
            {
                ErrorLog.createLog(ex.ToString());
                EdiError.LogError(oFiles.EdiFileType, m_iFileId.ToString(), "", ex, "", m_iHeaderId, m_iDetailId);
                ErrorLog.CreateLog(ex);
            }
            finally
            {
                oDbHelper.CloseDatabaseConnection();
            }
        }

        public void ParseDeatil_New()
        {
            iTECH.Library.DataAccess.MySql.DbHelper oDbHelper = new iTECH.Library.DataAccess.MySql.DbHelper(sMysqlConnectionString, true);
            try
            {
                int iItm = 0;
                int intchgitm = 0;
                foreach (XmlElement oNode in m_ItemXmlNodeList)
                {
                    //iItm = iItm + 1;
                    Details oDetails = new Details();
                    XmlElement onodelist = oNode;
                    omyXmlReader = new MyXmlReader(onodelist.OuterXml);
                    oDetails.PrntNo = omyXmlReader.GetNode("Order/PONumber").InnerText;
                    //oDetails.PrntItm = "1";               //commented code to increase prntitm for multiple items     2020-03-04  Sumit
                    string[] sar = oDetails.PrntNo.Split('-');
                    if (sar.Length > 1)
                    {
                        oDetails.PrntNo = sar[1];
                    }
                    oDetails.PrntSubItm = "1";            //commented code to increase prnt_sitm for multiple items   2020/03/02  Sumit -- code reverted 2020/03/03
                    oDetails.ShipperNo = m_sBol; // bol
                    oDetails.IntchgSItm = "1";
                    oDetails.ShptNo = oDetails.PrntNo;
                    //omyXmlReader.GetNode("Item/ItemURN").InnerText;
                    oDetails.CusRtnMthd = "";
                    oDetails.ShpGrsPcs = Utility.GetConfigValue("ShpGrsPcs54");
                    oDetails.ShpGrsMsr = Utility.GetConfigValue("ShpGrsMsr54");

                    //iItm = 0;                                               // to reset prnt_item if new PO/Order Received in single file  2020/03/25  Sumit 
                    foreach (XmlNode prdNode in omyXmlReader.GetNodeList("//Lines//Line"))
                    {
                        intchgitm++;
                        //iItm++;                                             // to increase prnt_item for multiple items of a PO     2020/03/25  Sumit
                        MyXmlReader prdXml = new MyXmlReader(prdNode.OuterXml);

                        iItm = Convert.ToInt16(prdNode.Attributes["LineNumber"].Value);
                        //oDetails.PrntSubItm = intchgitm.ToString();       // to process multiple products for same po 2020/03/02 sumit -- code reverted 2020/03/03
                        oDetails.PrntItm = iItm.ToString();                 // assign item number to prnt_item for multiple items   2020/03/25  Sumit
                        oDetails.IntchgItm = intchgitm.ToString();          // to process multiple products for different po and different items 2020/02/28 sumit
                        oDetails.ShpGrsWgt = prdXml.GetNode("//PhysicalProps/Dimensions/Weight").InnerText;
                        oDetails.OrigShpntPcs = prdXml.GetNode("//PhysicalProps/Dimensions/Count").InnerText;
                        var CoilLength = "";
                        if (prdXml.GetNode("//PhysicalProps/Dimensions/Length") != null)
                        {
                            CoilLength = prdXml.GetNode("//PhysicalProps/Dimensions/Length").InnerText;
                        }
                        else
                        {
                            CoilLength = "0";
                        }
                        oDetails.OrigShpntMsr = CoilLength;

                        string OrderedWeight = "";

                        OrderedWeight = oDetails.ShpGrsWgt;

                        if (!string.IsNullOrEmpty(OrderedWeight))
                        {
                            oDetails.OrigShptWgt = OrderedWeight;
                        }
                        else
                        {
                            oDetails.OrigShptWgt = "1.0";
                        }
                        oDetails.HeatNo = prdXml.GetNode("//Pack/Reference").InnerText;
                        tctipd otctipd = new tctipd();
                        //Shipment oShipment = new Shipment(); oShipment
                        otctipd.PopulateWarehouse(ref oShipment, oDetails.PrntNo, m_sCompanyId);
                        m_iWarehouse = BusinessUtility.GetInt(oShipment.RcvWhs);
                        Header oHeader = new Header();
                        oShipment.VenShpRef = m_sBol; // bol
                        oHeader.UpdateHeader(m_iHeaderId, oShipment.RcvWhs, oShipment.VenShpRef, oShipment.CarrierName, oShipment.CarrierVehicleDesc);
                        Utility.GetPoDetails(BusinessUtility.GetInt(oDetails.PrntNo), ref oDetails);
                        //oDetails.MultiplePoItemExist = 1;                 //commented code to get multiple po items   2020/03/03  Sumit
                        oDetails.MultiplePoItemExist = otctipd.CheckMultiplePoItemExists(oDetails.PrntNo);

                        m_iDetailId = oDetails.InsertDetails(m_iHeaderId, oDetails, oDbHelper);
                        if (m_iDetailId > 0)
                        {
                            XmlNodeList itemlist = prdXml.GetNodeList("//Pack");
                            if (itemlist.Count > 0)
                            {
                                this.ParseDetailItem_New(itemlist, BusinessUtility.GetInt(oDetails.PrntNo), BusinessUtility.GetInt(oDetails.PrntItm), Convert.ToInt16(oDetails.IntchgItm), oDetails.HeatNo, oDetails.Length, oDetails.OrigShptWgt);
                            }
                            else
                            {
                                ErrorLog.createLog("No item node found please check xml doc");
                            }

                        }
                    }
                    

                }
            }
            catch (Exception ex)
            {
                ErrorLog.createLog(ex.ToString());
                EdiError.LogError(oFiles.EdiFileType, m_iFileId.ToString(), "", ex, "", m_iHeaderId, m_iDetailId);
                ErrorLog.CreateLog(ex);
            }
            finally
            {
                oDbHelper.CloseDatabaseConnection();
            }
        }

        public static string TruncateLongString(string str, int maxLength)
        {
            if (string.IsNullOrEmpty(str))
                return str;
            return str.Substring(0, Math.Min(str.Length, maxLength));
        }

        public void ParseDetailItem(XmlNodeList lstItem, int iPO, int iPOItem, int iIntItem, string sHeat, string sLength, string sWeight)
        {
            iTECH.Library.DataAccess.MySql.DbHelper oDbHelper = new iTECH.Library.DataAccess.MySql.DbHelper(sMysqlConnectionString, true);
            try
            {
                //sLength = "1";                            // commented on 2020/02/26 to pass Length from xml file beacuse 1 is not valid value if file has width
                ErrorLog.createLog("sLength = " + sLength);
                int iItm = 0;
                foreach (XmlElement item in lstItem)
                {
                    string sDeatilItemSEction = item.OuterXml;
                    
                    omyXmlReader = new MyXmlReader(sDeatilItemSEction);

                    sHeat = omyXmlReader.GetNode("Pack/Reference").InnerText;

                    sWeight = omyXmlReader.GetNode("Pack/BundleWeight").InnerText;

                    iItm = iItm + 1;
                    Details oDetails = new Details();
                    oDetails.PrntNo = iPO.ToString();
                    oDetails.PrntItm = iPOItem.ToString();
                    oDetails.PrntSubItm = "1";
                    oDetails.HeatNo = sHeat;
                    Utility.GetPoDetails(BusinessUtility.GetInt(oDetails.PrntNo), ref oDetails);

                    XCTI55 oXCTI55 = new XCTI55();
                    oXCTI55.CmpyId = m_sCompanyId;
                    oXCTI55.IntchgPfx = m_sIntchgPfx;
                    oXCTI55.IntchgNo = BusinessUtility.GetString(ConfigurationManager.AppSettings["IntchgNo"]);
                    oXCTI55.IntchgItm = iIntItem.ToString();
                    oXCTI55.IntchgSitm = iItm.ToString();
                    oXCTI55.Form = oDetails.Form;
                    oXCTI55.Grid = oDetails.Grade;
                    oXCTI55.Size = oDetails.Size;
                    oXCTI55.Finish = oDetails.Finish;
                    oXCTI55.EfEver = oDetails.ExtendedFinish;
                    var CoilLength = sLength;
                    if (BusinessUtility.GetDecimal(CoilLength) > 0)
                    {
                        oXCTI55.Length = CoilLength;
                    }
                    oXCTI55.InvtMsr = CoilLength;
                    oXCTI55.ShpntMsr = CoilLength;
                    tctipd otctipd = new tctipd(); // Check if live data exists for length and some prop
                    otctipd.GetPoDetails(BusinessUtility.GetInt(oDetails.PrntNo), iPOItem);
                    if (!string.IsNullOrEmpty(otctipd.CoilLengthType))
                    {
                        oXCTI55.CoilLengthTyp = otctipd.CoilLengthType;
                    }
                    else
                    {
                        oXCTI55.CoilLengthTyp = ConfigurationManager.AppSettings["CoilLengthType"].ToString();
                    }
                    //oXCTI55.CoilLengthTyp = "";                  commented for xml coil length type 2020/02/26
                    oXCTI55.DimDsgn = otctipd.DimDsgn;
                    oXCTI55.RjctRsn = BusinessUtility.GetString(ConfigurationManager.AppSettings["RjctRsn"]);
                    oXCTI55.InvtPcsTyp = BusinessUtility.GetString(ConfigurationManager.AppSettings["InvtPcsTyp"]);
                    oXCTI55.InvtMsrTyp = BusinessUtility.GetString(ConfigurationManager.AppSettings["InvtMsrTyp"]);
                    oXCTI55.InvtWgtTyp = BusinessUtility.GetString(ConfigurationManager.AppSettings["InvtWgtTyp"]);
                    //oXCTI55.ShpntMsr = BusinessUtility.GetString(ConfigurationManager.AppSettings["ShpntMsr"]);
                    oXCTI55.ShpntPcsTyp = BusinessUtility.GetString(ConfigurationManager.AppSettings["ShpntPcsTyp"]);
                    oXCTI55.ShpntMsrTyp = BusinessUtility.GetString(ConfigurationManager.AppSettings["ShpntMsrTyp"]);
                    oXCTI55.ShpntWgtTyp = BusinessUtility.GetString(ConfigurationManager.AppSettings["ShpntWgtTyp"]);
                    //string sStratixTagid = omyXmlReader.GetNode("Item/ItemURN").InnerText;
                    string sStratixTagid = string.Empty;

                    var xDoc = XDocument.Parse(item.OuterXml);
                    var QdsChildList = xDoc.Descendants("Reference");
                    if (QdsChildList != null)
                    {
                        foreach (XElement element in QdsChildList)
                        {
                            string s = element.ToString();
                            if (s.Contains("Reference") && s.Contains("TypeCode=\"LS\""))
                            {
                                MyXmlReader oMyXmlReader = new MyXmlReader(s);
                                s = oMyXmlReader.GetNodeText("Reference");
                                sStratixTagid = s;
                                break;
                            }
                        }
                    }
                    //9E0494


                    if (sStratixTagid.Length > 15)
                    {
                        sStratixTagid = sStratixTagid.Replace("18109254500", string.Empty);
                    }
                    ErrorLog.createLog("m_iWarehouse = " + m_iWarehouse + "&& sStratixTagid = " + sStratixTagid);
                    if (m_iWarehouse != 001 && m_iWarehouse != 002 && m_iWarehouse != 003 && m_iWarehouse != 0)
                    {
                        oXCTI55.TagNo = sStratixTagid;
                    }
                    else
                    {
                        oXCTI55.TagNo = string.Empty;
                    }

                    //oXCTI55.TagNo = sStratixTagid;
                    oXCTI55.InvtPcs = BusinessUtility.GetString(ConfigurationManager.AppSettings["InvtPcs"]);
                    oXCTI55.ShpntPcs = BusinessUtility.GetString(ConfigurationManager.AppSettings["ShpntPcs"]);
                    oXCTI55.InvtQlty = otctipd.InvtQlty;

                    oXCTI55.Loc = !string.IsNullOrEmpty(sLocation) ? sLocation : BusinessUtility.GetString(ConfigurationManager.AppSettings["Location"]);
                    oXCTI55.Pkg = BusinessUtility.GetString(ConfigurationManager.AppSettings["Pkg"]);
                    oXCTI55.Mill = oFiles.Mill;
                    if (!string.IsNullOrEmpty(otctipd.CutNumber))
                    {
                        oXCTI55.CutNo = otctipd.CutNumber;
                    }
                    else
                    {
                        oXCTI55.CutNo = string.Empty;
                    }

                    if ( BusinessUtility.GetDecimal(otctipd.CoilLength) > 0)
                    {
                        oXCTI55.CoilLength = CoilLength;
                    }

                    var OrderedWeight = sWeight;
                    if (!string.IsNullOrEmpty(OrderedWeight))
                    {
                        oXCTI55.InvtWgt = OrderedWeight;
                        oXCTI55.ShpntWgt = OrderedWeight;
                    }
                    else
                    {
                        oXCTI55.InvtWgt = "1.0";
                    }
                    oXCTI55.Heat = oDetails.HeatNo;
                    EdiError.ValidateHEAT(oXCTI55.Heat, "ExecuteDetailItemSection Heat missing", BusinessUtility.GetString(Globalcl.FileId));

                    // this is id which link 856 and 863

                    oXCTI55.MillId = sStratixTagid; 
                    if (!string.IsNullOrEmpty(otctipd.IDa))
                    {
                        oXCTI55.I55Id = otctipd.IDa;
                    }
                    if (!string.IsNullOrEmpty(otctipd.ODa))
                    {
                        oXCTI55.I55Od = otctipd.ODa;
                    }
                    m_sShipFrom = otctipd.ShipFrom;
                    m_iDtailItem = oXCTI55.InsertDetailItems(m_iHeaderId, m_iDetailId, oXCTI55, oDbHelper);


                }
            }

            catch (Exception ex)
            {
                ErrorLog.createLog(ex.ToString());
                EdiError.LogError(oFiles.EdiFileType, m_iFileId.ToString(), "", ex, "", m_iHeaderId, m_iDetailId);
                ErrorLog.CreateLog(ex);
            }
            finally
            {
                oDbHelper.CloseDatabaseConnection();
            }
        }

        public void ParseDetailItem_New(XmlNodeList lstItem, int iPO, int iPOItem, int iIntItem, string sHeat, string sLength, string sWeight)
        {
            iTECH.Library.DataAccess.MySql.DbHelper oDbHelper = new iTECH.Library.DataAccess.MySql.DbHelper(sMysqlConnectionString, true);
            try
            {
                //sLength = "1";                            // commented on 2020/02/26 to pass Length from xml file beacuse 1 is not valid value if file has width
                ErrorLog.createLog("sLength = " + sLength);
                int iItm = 0;
                foreach (XmlElement item in lstItem)
                {
                    string sDeatilItemSEction = item.OuterXml;

                    omyXmlReader = new MyXmlReader(sDeatilItemSEction);

                    sHeat = omyXmlReader.GetNode("//Reference").InnerText;

                    sWeight = omyXmlReader.GetNode("//BundleWeight").InnerText;

                    //iItm = iItm + 1;                                                                  //To fetch sub items according to pack number   2020/03/26  Sumit
                    iItm = Convert.ToInt16(item.Attributes["Number"].Value);
                    Details oDetails = new Details();
                    oDetails.PrntNo = iPO.ToString();
                    oDetails.PrntItm = iPOItem.ToString();
                    oDetails.PrntSubItm = "1";
                    oDetails.HeatNo = sHeat;
                    oDetails.IntchgItm = Convert.ToString(iIntItem);
                    Utility.GetPoDetails(BusinessUtility.GetInt(oDetails.PrntNo), ref oDetails);      //commented to pass itnchgItm values instead of PrntNo to get product detail from PO    2020/03/02  Sumit 2020/03/23  Code Reverted
                    //Utility.GetPoDetails(BusinessUtility.GetInt(oDetails.IntchgItm), ref oDetails);

                    XCTI55 oXCTI55 = new XCTI55();
                    oXCTI55.CmpyId = m_sCompanyId;
                    oXCTI55.IntchgPfx = m_sIntchgPfx;
                    oXCTI55.IntchgNo = BusinessUtility.GetString(ConfigurationManager.AppSettings["IntchgNo"]);
                    oXCTI55.IntchgItm = iIntItem.ToString();
                    oXCTI55.IntchgSitm = iItm.ToString();
                    oXCTI55.Form = oDetails.Form;
                    oXCTI55.Grid = oDetails.Grade;
                    oXCTI55.Size = oDetails.Size;
                    oXCTI55.Finish = oDetails.Finish;
                    oXCTI55.EfEver = oDetails.ExtendedFinish;
                    
                    var CoilLength = sLength;
                    #region commented code - Passing length insead of coil length   2020/03/30  Sumit
                    //if (BusinessUtility.GetDecimal(CoilLength) > 0)
                    //{
                    //    oXCTI55.Length = CoilLength;
                    //}
                    if (!string.IsNullOrEmpty(oDetails.Length))
                        oXCTI55.Length = oDetails.Length;
                    else
                        oXCTI55.Length = "0";
                    #endregion commented code - Passing length insead of coil length   2020/03/30  Sumit

                    oXCTI55.InvtMsr = CoilLength;
                    oXCTI55.ShpntMsr = CoilLength;
                    tctipd otctipd = new tctipd(); // Check if live data exists for length and some prop
                    otctipd.GetPoDetails(BusinessUtility.GetInt(oDetails.PrntNo), iPOItem);       //commented to pass itnchgItm values instead of iPOItem to get product detail from PO    2020/03/02  Sumit  code reverted   2020/03/25
                    //otctipd.GetPoDetails(BusinessUtility.GetInt(oDetails.PrntNo), iIntItem);
                    
                    #region commented code - to pass coillength type from db only
                    oXCTI55.CoilLengthTyp = otctipd.CoilLengthType;
                    //if (!string.IsNullOrEmpty(otctipd.CoilLengthType))
                    //{
                    //    oXCTI55.CoilLengthTyp = otctipd.CoilLengthType;
                    //}
                    //else
                    //{
                    //    oXCTI55.CoilLengthTyp = ConfigurationManager.AppSettings["CoilLengthType"].ToString();
                    //}
                    #endregion commented code - to pass coillength type from db only

                    //oXCTI55.CoilLengthTyp = "";                  commented for xml coil length type 2020/02/26 Sumit
                    oXCTI55.DimDsgn = otctipd.DimDsgn;
                    oXCTI55.RjctRsn = BusinessUtility.GetString(ConfigurationManager.AppSettings["RjctRsn"]);
                    oXCTI55.InvtPcsTyp = BusinessUtility.GetString(ConfigurationManager.AppSettings["InvtPcsTyp"]);
                    oXCTI55.InvtMsrTyp = BusinessUtility.GetString(ConfigurationManager.AppSettings["InvtMsrTyp"]);
                    oXCTI55.InvtWgtTyp = BusinessUtility.GetString(ConfigurationManager.AppSettings["InvtWgtTyp"]);
                    //oXCTI55.ShpntMsr = BusinessUtility.GetString(ConfigurationManager.AppSettings["ShpntMsr"]);
                    oXCTI55.ShpntPcsTyp = BusinessUtility.GetString(ConfigurationManager.AppSettings["ShpntPcsTyp"]);
                    oXCTI55.ShpntMsrTyp = BusinessUtility.GetString(ConfigurationManager.AppSettings["ShpntMsrTyp"]);
                    oXCTI55.ShpntWgtTyp = BusinessUtility.GetString(ConfigurationManager.AppSettings["ShpntWgtTyp"]);
                    //string sStratixTagid = omyXmlReader.GetNode("Item/ItemURN").InnerText;
                    string sStratixTagid = string.Empty;

                    var xDoc = XDocument.Parse(item.OuterXml);
                    var QdsChildList = xDoc.Descendants("Reference");
                    if (QdsChildList != null)
                    {
                        foreach (XElement element in QdsChildList)
                        {
                            string s = element.ToString();
                            if (s.Contains("Reference") && s.Contains("TypeCode=\"LS\""))
                            {
                                MyXmlReader oMyXmlReader = new MyXmlReader(s);
                                s = oMyXmlReader.GetNodeText("Reference");
                                sStratixTagid = s;
                                break;
                            }
                        }
                    }
                    //9E0494


                    if (sStratixTagid.Length > 15)
                    {
                        sStratixTagid = sStratixTagid.Replace("18109254500", string.Empty);
                    }
                    ErrorLog.createLog("m_iWarehouse = " + m_iWarehouse + "&& sStratixTagid = " + sStratixTagid);
                    if (m_iWarehouse != 001 && m_iWarehouse != 002 && m_iWarehouse != 003 && m_iWarehouse != 0)
                    {
                        oXCTI55.TagNo = sStratixTagid;
                    }
                    else
                    {
                        oXCTI55.TagNo = string.Empty;
                    }

                    //oXCTI55.TagNo = sStratixTagid;
                    oXCTI55.InvtPcs = BusinessUtility.GetString(ConfigurationManager.AppSettings["InvtPcs"]);
                    oXCTI55.ShpntPcs = BusinessUtility.GetString(ConfigurationManager.AppSettings["ShpntPcs"]);
                    oXCTI55.InvtQlty = otctipd.InvtQlty;

                    //oXCTI55.Loc = !string.IsNullOrEmpty(sLocation) ? sLocation : BusinessUtility.GetString(ConfigurationManager.AppSettings["Location"]);
                    oXCTI55.Loc = BusinessUtility.GetString(ConfigurationManager.AppSettings["Location"]);      //default location for all vendor is "-"  2020/03/25    Sumit  
                    oXCTI55.Pkg = BusinessUtility.GetString(ConfigurationManager.AppSettings["Pkg"]);
                    oXCTI55.Mill = oFiles.Mill;
                    if (!string.IsNullOrEmpty(otctipd.CutNumber))
                    {
                        oXCTI55.CutNo = otctipd.CutNumber;
                    }
                    else
                    {
                        oXCTI55.CutNo = string.Empty;
                    }

                    if (BusinessUtility.GetDecimal(otctipd.CoilLength) > 0)
                    {
                        oXCTI55.CoilLength = CoilLength;
                    }

                    var OrderedWeight = sWeight;
                    if (!string.IsNullOrEmpty(OrderedWeight))
                    {
                        oXCTI55.InvtWgt = VendorFiltration.StringMathRound.GetRoundedString(OrderedWeight);         //getting only numeric value and removing decimal points    2020/03/30  Sumit
                        oXCTI55.ShpntWgt = VendorFiltration.StringMathRound.GetRoundedString(OrderedWeight);        //getting only numeric value and removing decimal points    2020/03/30  Sumit
                    }
                    else
                    {
                        oXCTI55.InvtWgt = "1.0";
                    }
                    oXCTI55.Heat = oDetails.HeatNo;
                    EdiError.ValidateHEAT(oXCTI55.Heat, "ExecuteDetailItemSection Heat missing", BusinessUtility.GetString(Globalcl.FileId));

                    // this is id which link 856 and 863

                    oXCTI55.MillId = sStratixTagid;
                    if (!string.IsNullOrEmpty(otctipd.IDa))
                    {
                        oXCTI55.I55Id = otctipd.IDa;
                    }
                    if (!string.IsNullOrEmpty(otctipd.ODa))
                    {
                        oXCTI55.I55Od = otctipd.ODa;
                    }
                    m_sShipFrom = otctipd.ShipFrom;
                    m_iDtailItem = oXCTI55.InsertDetailItems(m_iHeaderId, m_iDetailId, oXCTI55, oDbHelper);


                }
            }

            catch (Exception ex)
            {
                ErrorLog.createLog(ex.ToString());
                EdiError.LogError(oFiles.EdiFileType, m_iFileId.ToString(), "", ex, "", m_iHeaderId, m_iDetailId);
                ErrorLog.CreateLog(ex);
            }
            finally
            {
                oDbHelper.CloseDatabaseConnection();
            }
        }

        public string GetWeightValueFromXml(string sActualDimention, int iCode)
        {
            string sReturn = string.Empty;
            try
            {
                MyXmlReader omyXmlReader = new MyXmlReader(sActualDimention);
                XmlNodeList onodelist1 = omyXmlReader.GetNodeList("ActualDimensions/Weight");
                foreach (XmlElement oNode in onodelist1)
                {
                    XmlElement onodelist = oNode;
                    omyXmlReader = new MyXmlReader(onodelist.OuterXml);
                    int sCode = BusinessUtility.GetInt(omyXmlReader.GetNode("Weight/UnitOrBasisForMeasCode/CodeValue").InnerText);
                    if (sCode == iCode)
                    {
                        string sWeight = omyXmlReader.GetNode("Weight/MeasValue").InnerText;
                        sReturn = sWeight;
                        break;
                    }
                }
            }
            catch { }
            return sReturn;
        }

        public string GetMsrValueFromXml(string sActualDimention)
        {
            string sReturn = string.Empty;
            try
            {
                MyXmlReader omyXmlReader = new MyXmlReader(sActualDimention);
                XmlNodeList onodelist1 = omyXmlReader.GetNodeList("ActualDimensions/Len");
                foreach (XmlElement oNode in onodelist1)
                {
                    XmlElement onodelist = oNode;
                    omyXmlReader = new MyXmlReader(onodelist.OuterXml);
                    string sCode = omyXmlReader.GetNode("Len/UnitOrBasisForMeasCode/CodeValue").InnerText;
                    string sWeight = omyXmlReader.GetNode("Len/MeasValue").InnerText;
                    if (sCode == "LF")
                    {
                        sReturn = BusinessUtility.GetString(BusinessUtility.GetDecimal(sWeight) * 12);  // If LF then multiply with 12 else mul by 1 suggested by sir.
                        break;
                    }
                    else
                    {
                        sReturn = BusinessUtility.GetString(BusinessUtility.GetDecimal(sWeight) * 1);
                        break;
                    }
                }
            }
            catch { }
            return sReturn;
        }


    }

}

