﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Text.RegularExpressions;
using System.Xml.Linq;
using iTECH.Library.DataAccess.ODBC;
using iTECH.Library.Utilities;
using System.Configuration;
using System.Data;
using MySql.Data.MySqlClient;
using System.Data.Odbc;
using System.Collections;

namespace SecureFTP
{
    class Edi856Parser
    {


        string[] m_sArr = null;
        int m_iFileId = 0;
        string m_sSql = string.Empty;
        StringBuilder m_sbSql = null;
        static string sStratixConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ToString();
        static string sMysqlConnectionString = ConfigurationManager.ConnectionStrings["NewConnectionString"].ToString();

        static string m_sCompanyId = ConfigurationManager.AppSettings["CompanyId"];
        static string m_sIntchgPfx = ConfigurationManager.AppSettings["IntchgPfx"];
        static string m_sCusVenId = ConfigurationManager.AppSettings["CusVenId"];
        static string m_sRcptType = ConfigurationManager.AppSettings["RcptType"];

        static int m_iCusVenId = BusinessUtility.GetInt(ConfigurationManager.AppSettings["CusVenId"]);
        private static string EdiFile = ConfigurationManager.AppSettings["EdiFile"];

        int m_iHeaderId = 0;
        int m_iDetailId = 0;

        string sPoNo = string.Empty;
        bool bShipmentCreated = false;


        List<string> lstLines = new List<string>();
        List<string> lstHeader = new List<string>();
        List<string> lstDetailShipment = new List<string>();
        List<Orders> lstDetailOrder = new List<Orders>();
        List<string> lstDetailItem = new List<string>();
        List<string> lstTotalOrderItemDetail = new List<string>();

        List<string> lstOrder = new List<string>();


        Orders oOrders = new Orders();
        Items oItems = new Items();
        Shipment oShipment = new Shipment();
        Details oDetails = new Details();
        XCTI55 oXCTI55 = null;
        Files oFiles;
        Configuration oConfiguration = null;


        Dictionary<string, string> dictIndexHolder = new Dictionary<string, string>();
        ArrayList m_arlHdrShipmentList = new ArrayList();
        List<string> m_lstHdrShipmentList = new List<string>();

        int m_iPreviousPo = 0;
        int m_iCurrentPo = 0;
        int m_iIntchgItm = 0;
        string m_sShipFrom = string.Empty;
        int m_iWarehouse = 0;


        public struct EdiKeyWords
        {
            public const string CTT = "CTT";
            public const string BSN = "BSN";
            public const string DTM = "DTM";
            public const string TD1 = "TD1";
            public const string TD5 = "TD5";
            public const string TD3 = "TD3";
            public const string REF = "REF";
            public const string N1 = "N1";
            public const string LIN = "LIN";
            public const string HL = "HL";
            public const string PRF = "PRF";
            public const string MEA = "MEA";
            public const string SN1 = "SN1";
            public const string SHIPMENT = "S";
            public const string ITEM = "I";
            public const string ORDER = "O";
            public const string HN = "HN";  // HN is for heat
            public const string SN = "SN";  // SN is for ven tag
            public const string HC = "HC";  // HC is for heat 

        }

        public struct UNITOFMEASURMENT
        {
            public const string LF = "LF";
        }


        public Edi856Parser()
        {
        }

        public Edi856Parser(Dictionary<string, string> dict, string[] sArr, int iFileID, ArrayList arlHdr)
        {
            dictIndexHolder = dict;
            m_sArr = sArr;
            m_iFileId = iFileID;
            m_arlHdrShipmentList = arlHdr;
            oFiles = new Files(m_iFileId);

            oConfiguration = new Configuration(oFiles.VendorDunsNo, BusinessUtility.GetInt(oFiles.EdiFileType));
      

            Utility.AddArrayListToList(ref arlHdr, ref m_lstHdrShipmentList);


            this.ProcessOnHL();
        }

        public string GetField(string sField)
        {
            string s = sField.Replace("~", string.Empty);
            // s = sField.Replace("...", string.Empty);
            return s;
        }
        public string RemoveLineBreaker(object obj)
        {
            return BusinessUtility.GetString(obj).Substring(0, BusinessUtility.GetString(obj).Length - 1);
        }

        public void AddElementBetweenArrayWithStartPosToList(ref List<string> oList, string[] sArr, int iStartPos, int iEndPos)
        {
            int iPos = 0;
            oList.Clear();
            foreach (var lineData in sArr)
            {
                if (iPos >= iStartPos && iPos <= iEndPos)
                {

                    oList.Add(RemoveLineBreaker(lineData));
                }
                iPos++;
            }
        }

        public void GetPoDetails(int iPoNumber, ref Details oDetails)
        {

            tctipd otctipd = new tctipd(iPoNumber, m_iFileId);
            if (otctipd != null)
            {
                oDetails.Form = otctipd.Form;
                oDetails.Grade = otctipd.Grade;
                oDetails.Size = otctipd.Size;
                oDetails.Finish = otctipd.Finish;
                oDetails.GaSize = otctipd.GaSize;
                oDetails.GaType = otctipd.GaType;
                oDetails.DimDsgn = otctipd.DimDsgn;
                oDetails.ExtendedFinish = otctipd.ExtendedFinish;

                oDetails.IDa = otctipd.IDa;
                oDetails.ODa = otctipd.ODa;

                oDetails.InvtQlty = otctipd.InvtQlty;
                if (!string.IsNullOrEmpty(otctipd.Width))
                {
                    oDetails.Width = otctipd.Width;
                }
                else
                {
                    oDetails.Width = "0";
                }

            }
        }


        public void ProcessOnHL()
        {
            string sKey = string.Empty;
            string sValue = string.Empty;
            string[] sValArr = null;
            int i = 1;
            int iDictCount = 0;
            string[] sSplitSegmnt = null;
            foreach (KeyValuePair<string, string> entry in dictIndexHolder)
            {
                sKey = entry.Key;
                sValue = entry.Value;
                sValArr = sValue.Split(',');

                sSplitSegmnt = Utility.SplitString(sKey, '*');

                if (sSplitSegmnt.Length >= 4)
                {
                    if (sSplitSegmnt[3] == EdiKeyWords.SHIPMENT)
                    {
                       
                    }
                    else if (sSplitSegmnt[3] == EdiKeyWords.ORDER)
                    {
                        lstOrder = new List<string>();
                        AddElementBetweenArrayWithStartPosToList(ref lstOrder, m_sArr, BusinessUtility.GetInt(sValArr[0]), BusinessUtility.GetInt(sValArr[1]));
                       // var po = lstOrder.ToList<string>().FirstOrDefault(stringToCheck => stringToCheck.Contains("PRF*"));

                        //if (po != null)
                        //{
                        //    sPoNo = po;
                        //    sPoNo = sPoNo.Split('*')[1];
                        //}
                    }

                    else if (sSplitSegmnt[3] == EdiKeyWords.ITEM)
                    {
                        lstDetailItem = new List<string>();
                        AddElementBetweenArrayWithStartPosToList(ref lstDetailItem, m_sArr, BusinessUtility.GetInt(sValArr[0]), BusinessUtility.GetInt(sValArr[1]));
                        if (lstDetailItem.Count > 0)
                        {
                            var result = lstDetailItem.Concat(m_lstHdrShipmentList);
                            result = result.Concat(lstOrder);

                            var po = result.ToList<string>().FirstOrDefault(stringToCheck => stringToCheck.Contains("PRF*"));

                            if (po != null)
                            {
                                sPoNo = po;
                                sPoNo = sPoNo.Split('*')[1];
                            }

                            m_iCurrentPo = BusinessUtility.GetInt(sPoNo);

                            if (m_iPreviousPo != m_iCurrentPo)
                            {
                                bShipmentCreated = false;
                                i = 1;
                                if (m_iHeaderId > 0)
                                {
                                    STX856 oSTX856 = new STX856(m_iHeaderId);
                                    oSTX856.CompanyId = m_sCompanyId;
                                    oSTX856.IntchgPfx = m_sIntchgPfx;
                                    oSTX856.InterChangeNo = 0;
                                    oSTX856.RcptType = m_sRcptType;
                                    oSTX856.CusVenId = m_sCusVenId;
                                    if (string.IsNullOrEmpty(m_sShipFrom))
                                    {
                                        m_sShipFrom = "0";       // implemented this change due to error in nlmk : Cannot insert a null into column (xcti53_rec.i53_cus_ven_shp)
                                    }
                                    oSTX856.CusVenShp = m_sShipFrom;    // poh_shp_fm
                                    oSTX856.StartInsert();
                                    m_iHeaderId = 0;
                                    ErrorLog.createLog("m_sShipFrom = " + m_sShipFrom);
                                }
                            }

                            if (!bShipmentCreated)
                            {
                                m_iPreviousPo = m_iCurrentPo;
                                PrepareHlShipment(result.ToList<string>());
                                HeaderInsertion(result.ToList<string>());
                                ExecuteDetailItemSection(result.ToList<string>(), i);
                                i++;
                            }

                            else if (bShipmentCreated && (m_iPreviousPo == m_iCurrentPo))
                            {
                                ExecuteDetailItemSection(result.ToList<string>(), i);
                                i++;
                            }


                            if (iDictCount == dictIndexHolder.Count - 1)
                            {
                                bShipmentCreated = false;
                                if (m_iHeaderId > 0)
                                {
                                    STX856 oSTX856 = new STX856(m_iHeaderId);
                                    oSTX856.CompanyId = m_sCompanyId;
                                    oSTX856.IntchgPfx = m_sIntchgPfx;
                                    oSTX856.InterChangeNo = 0;
                                    oSTX856.RcptType = m_sRcptType;
                                    oSTX856.CusVenId = m_sCusVenId;
                                    if (string.IsNullOrEmpty(m_sShipFrom))
                                    {
                                        m_sShipFrom = "0";       // implemented this change due to error in nlmk : Cannot insert a null into column (xcti53_rec.i53_cus_ven_shp)
                                    }
                                    oSTX856.CusVenShp = m_sShipFrom;    // poh_shp_fm
                                    oSTX856.StartInsert();
                                    m_iHeaderId = 0;
                                    ErrorLog.createLog("m_sShipFrom = " + m_sShipFrom);
                                }
                            }


                        }
                    }
                    
                    
                    //i++;

                }
                iDictCount++;

            }

            

        }

   


        #region   This section execute once per file and insert header and shipment related data (i54)

        public void PrepareHlShipment(List<string> oList)
        {
            DbHelper oDbHelper = new DbHelper(sStratixConnectionString, true);
            try
            {
                //lstTotalOrderItemDetail

                var po = oList.FirstOrDefault(stringToCheck => stringToCheck.Contains("PRF*"));
                if (po != null)
                {
                    sPoNo = po;
                    sPoNo = sPoNo.Split('*')[1];
                    sPoNo = Utility.GetIntFromString(sPoNo).ToString();
                    bShipmentCreated = true;
                }

                string sTD3EquipmentInitial = string.Empty;
                string sTD3EquipmentNumber = string.Empty;
                var td3 = oList.FirstOrDefault(stringToCheck => stringToCheck.Contains("TD3*"));
                if (td3 != null)
                {
                    if (td3.Split('*').Length >= 3)
                    {
                      sTD3EquipmentInitial =  td3.Split('*')[2];
                      sTD3EquipmentNumber = td3.Split('*')[3];
                    }
                }


                oShipment.CompanyId = m_sCompanyId;
                m_sSql = "Select first 1 pod_shp_to_whs,pod_frt_ven_id from potpod_rec where pod_cmpy_id =  '" + oShipment.CompanyId + "' and pod_po_pfx= 'PO' and pod_po_no = '" + sPoNo + "'";
                ErrorLog.createLog(m_sSql);
                DataTable oDataTable = new DataTable();
                try
                {
                    oDataTable = oDbHelper.GetDataTable(m_sSql, CommandType.Text, null);
                }
                catch(Exception ex)
                {
                    ErrorLog.CreateLog(ex);
                }
                finally
                {
                    oDbHelper.CloseDatabaseConnection();
                }
                if (oDataTable.Rows.Count > 0)
                {
                    
                    oShipment.RcvWhs = BusinessUtility.GetString(oDataTable.Rows[0][0]);
                    m_iWarehouse = BusinessUtility.GetInt(oShipment.RcvWhs);
                   // m_iWarehouse = 100;
                    oShipment.FreightVenId = BusinessUtility.GetString(oDataTable.Rows[0][1]);
                    ErrorLog.createLog(oShipment.RcvWhs + " < whs  ,  frtvenid > " + oShipment.FreightVenId);
                }



                for (int i = 0; i < oList.Count; i++)
                {
                    // string[] sSegment = RemoveLineBreaker(oList[i]).Split('*');
                    string[] sSegment = (oList[i]).Split('*');
                    if (sSegment.Length > 2)
                    {
                        if (sSegment[0] == EdiKeyWords.TD5)
                        {
                            if (sSegment.Length >= 5)
                            {
                                oShipment.CarrierName = GetField(sSegment[3]);
                                oShipment.CarrierRefNo = GetField(sSegment[3]);
                            }
                        }
                        else if (sSegment[0] == EdiKeyWords.N1 && sSegment[1] == "ST")
                        {
                            if (sSegment.Length >= 3)
                            {
                                oShipment.ShipTo = GetField(sSegment[2]);
                            }
                        }
                        else if (sSegment[0] == EdiKeyWords.REF)
                        {
                            if (sSegment.Length >= 3)
                            {
                                oShipment.VenShpRef = GetField(sSegment[2]);

                                if (oList[i].Contains("REF*BM*"))
                                {
                                    // this is ven shp ref field in stratix client application.  (i54_ven_shp_ref).

                                    oDetails.VenShptRef = GetField(sSegment[2]);
                                    if (!string.IsNullOrEmpty(sTD3EquipmentNumber))
                                    {
                                        string sVenShptRef = oDetails.VenShptRef + " - " + sTD3EquipmentInitial + " " + sTD3EquipmentNumber;
                                        oDetails.VenShptRef = sVenShptRef;
                                        oShipment.VenShpRef = sVenShptRef;
                                    }
                                }

                            }
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                ErrorLog.createLog("inside PrepareHlShipment");
                ErrorLog.createLog(ex.ToString());
                EdiError.LogError(EdiFile, m_iFileId.ToString(), "", ex, "inside PrepareHlShipment");
                ErrorLog.CreateLog(ex);
            }
            finally
            {
                oDbHelper.CloseDatabaseConnection();
            }
        }


        public void InsertDetail(List<string> oList)
        {
            iTECH.Library.DataAccess.MySql.DbHelper oDbHelper = new iTECH.Library.DataAccess.MySql.DbHelper(sMysqlConnectionString, true);
            if (oList.Count > 0)
            {
                var width = oList.FirstOrDefault(stringToCheck => stringToCheck.Contains("MEA*PD*WD*"));
                var weight = oList.FirstOrDefault(stringToCheck => stringToCheck.Contains("MEA*PD*WT*")) != null ? oList.FirstOrDefault(stringToCheck => stringToCheck.Contains("MEA*PD*WT*")) : oList.FirstOrDefault(stringToCheck => stringToCheck.Contains("MEA*WT*WT*"));
                var length = oList.FirstOrDefault(stringToCheck => stringToCheck.Contains("MEA*PD*LN*"));
                string sWeight = string.Empty;
                string sLength = string.Empty;

                var ShiperNo = oList.FirstOrDefault(stringToCheck => stringToCheck.Contains("BSN*"));

                if (ShiperNo != null)
                {
                    oDetails.ShipperNo = ShiperNo.Split('*')[2];
                }

                if (width != null)
                {
                    oDetails.Width = width;
                    oDetails.Width = oDetails.Width.Split('*')[3];
                }

                if (weight != null)
                {
                    sWeight = weight;
                    sWeight = sWeight.Split('*')[3];
                }
                else
                {
                    sWeight = "1.0";
                }
                if (length != null)
                {
                    sLength = length;
                    sLength = sLength.Split('*')[3];
                }


                var po = oList.FirstOrDefault(stringToCheck => stringToCheck.Contains("PRF*"));
                var heat = oList.FirstOrDefault(stringToCheck => stringToCheck.Contains("*HN*"));



                if (po != null)
                {
                    oDetails.PrntNo = Utility.GetIntFromString(po.Split('*')[1]).ToString();
                }
                if (heat != null)
                {
                    oDetails.HeatNo = heat.Split('*')[5];
                }

                oDetails.PrntItm = BusinessUtility.GetString(ConfigurationManager.AppSettings["PrntItm54"]); ;
                oDetails.PrntSubItm = BusinessUtility.GetString(ConfigurationManager.AppSettings["PrntSItm54"]); ;
                oDetails.ShptNo = BusinessUtility.GetString(ConfigurationManager.AppSettings["ShpmentNo54"]);
                oDetails.OrigShpntPcs = BusinessUtility.GetString(ConfigurationManager.AppSettings["OrigShpntPcs54"]);
                oDetails.OrigShpntMsr = BusinessUtility.GetString(ConfigurationManager.AppSettings["OrigShpntMsr54"]);
                oDetails.OrigShptWgt = sWeight;
                oDetails.ShpGrsPcs = BusinessUtility.GetString(ConfigurationManager.AppSettings["ShpGrsPcs54"]); ;
                oDetails.ShpGrsMsr = BusinessUtility.GetString(ConfigurationManager.AppSettings["ShpGrsMsr54"]); ;
                oDetails.ShpGrsWgt = sWeight;
                oDetails.Length = sLength;
                oDetails.InvtQlty = BusinessUtility.GetString(ConfigurationManager.AppSettings["InvtQuality"]);
                //oDetails.IntchgItm = BusinessUtility.GetString(BusinessUtility.GetInt(oDetails.IntchgItm) + 1);
                oDetails.IntchgItm = BusinessUtility.GetString(BusinessUtility.GetInt(ConfigurationManager.AppSettings["IntchgItm"]));
                m_iIntchgItm = BusinessUtility.GetInt(oDetails.IntchgItm);
                this.GetPoDetails(BusinessUtility.GetInt(oDetails.PrntNo), ref oDetails);

                //if (oConfiguration != null)
                //{
                //    if (!string.IsNullOrEmpty(oConfiguration.DimDesign))
                //    {
                //        oDetails.DimDsgn = oConfiguration.DimDesign;
                //    }
                //}

                m_iDetailId = oDetails.InsertDetails(m_iHeaderId, oDetails, oDbHelper);
            }
        }



        //Insert into header in mysql
        public int HeaderInsertion(List<string> oList)
        {
            string sFileId = BusinessUtility.GetString(m_iFileId);
            oShipment.CusVenId = m_sCusVenId;
            oShipment.VenShpRef = oShipment.VenShpRef != string.Empty ? oShipment.VenShpRef : BusinessUtility.GetString(ConfigurationManager.AppSettings["VenShpRef"]);
            oShipment.CarrierVehicleDesc = BusinessUtility.GetString(ConfigurationManager.AppSettings["CarrierVehicleDesc"]);
            oShipment.DeliveryMethod = BusinessUtility.GetString(ConfigurationManager.AppSettings["DeliveryMethod"]);
            oShipment.PreRcptStatus = BusinessUtility.GetString(ConfigurationManager.AppSettings["PreRcptStatus"]);

            if (oConfiguration != null)
            {
                if (!string.IsNullOrEmpty(oConfiguration.PrereceiptStatus))
                {
                    oShipment.PreRcptStatus = oConfiguration.PrereceiptStatus;
                }
                if (!string.IsNullOrEmpty(oConfiguration.VendorId))
                {
                    oShipment.CusVenId = oConfiguration.VendorId;
                }
            }

            m_sbSql = new StringBuilder();
            int returnHeaderID = 0;
            iTECH.Library.DataAccess.MySql.DbHelper oDbHelper = new iTECH.Library.DataAccess.MySql.DbHelper(sMysqlConnectionString, true);
            try
            {
                m_sbSql.Append(" INSERT INTO HEADER(FileId, CusVenId, ShipTo, VenShpRef, CarrierVehicleDesc, RcvWhs, DeliveryMethod, FreightVenId, CarrierName, CarrierRefNo, PreRcptStatus)");
                m_sbSql.Append(" VALUES ('" + sFileId + "', '" + oShipment.CusVenId + "', '" + oShipment.ShipTo + "', '" + oShipment.VenShpRef + "', '" + oShipment.CarrierVehicleDesc + "', '" + oShipment.RcvWhs + "' ");
                m_sbSql.Append(" , '" + oShipment.DeliveryMethod + "', '" + oShipment.FreightVenId + "', '" + oShipment.CarrierName + "', '" + oShipment.CarrierRefNo + "', '" + oShipment.PreRcptStatus + "'   )  ");

                MySqlParameter[] oMySqlParameter = { };

                int iStatus = oDbHelper.ExecuteNonQuery(m_sbSql.ToString(), CommandType.Text, oMySqlParameter);
                iStatus = oDbHelper.GetLastInsertID();
                m_iHeaderId = iStatus;
                returnHeaderID = m_iHeaderId;

                if (oList.Count > 0)
                {
                    var width = oList.FirstOrDefault(stringToCheck => stringToCheck.Contains("MEA*PD*WD*"));
                    var weight = oList.FirstOrDefault(stringToCheck => stringToCheck.Contains("MEA*PD*WT*")) != null ? oList.FirstOrDefault(stringToCheck => stringToCheck.Contains("MEA*PD*WT*")) : oList.FirstOrDefault(stringToCheck => stringToCheck.Contains("MEA*WT*WT*"));
                    var length = oList.FirstOrDefault(stringToCheck => stringToCheck.Contains("MEA*PD*LN*"));
                    string sWeight = string.Empty;
                    string sLength = string.Empty;


                    if (width != null)
                    {
                        oDetails.Width = width;
                        oDetails.Width = oDetails.Width.Split('*')[3];
                    }

                    if (weight != null)
                    {
                        sWeight = weight;
                        sWeight = sWeight.Split('*')[3];
                    }
                    else
                    {
                        sWeight = "1.0";
                    }
                    if (length != null)
                    {
                        sLength = length;
                        sLength = sLength.Split('*')[3];
                    }

                    var ShiperNo = oList.FirstOrDefault(stringToCheck => stringToCheck.Contains("BSN*"));

                    if (ShiperNo != null)
                    {
                        oDetails.ShipperNo = ShiperNo.Split('*')[2];
                    }


                    var po = oList.FirstOrDefault(stringToCheck => stringToCheck.Contains("PRF*"));
                   // var heat = oList.FirstOrDefault(stringToCheck => stringToCheck.Contains("*HN*"));
                    var heat = oList.FirstOrDefault(stringToCheck => stringToCheck.Contains("REF*HC*")) != null ? oList.FirstOrDefault(stringToCheck => stringToCheck.Contains("REF*HC*")) : oList.FirstOrDefault(stringToCheck => stringToCheck.Contains("*HN*"));

                    if (po != null)
                    {
                        oDetails.PrntNo = Utility.GetIntFromString(po.Split('*')[1]).ToString();
                    }
                    //if (heat != null)
                    //{
                    //    oDetails.HeatNo = heat.Split('*')[5];
                    //}

                    if (heat != null)
                    {
                        if (BusinessUtility.GetString(oFiles.VendorId) == Configuration.VENDORTYPE.NUCORBERKELEY || BusinessUtility.GetString(oFiles.VendorId) == Configuration.VENDORTYPE.NUCORDECATUR || BusinessUtility.GetString(oFiles.VendorId) == Configuration.VENDORTYPE.NUCORHICKMAN || BusinessUtility.GetString(oFiles.VendorId) == Configuration.VENDORTYPE.NUCORCRAWFORDSVILLE)
                        {
                            if (heat.Split('*').Length >= 7)
                            {
                                oDetails.HeatNo = heat.Split('*')[7];
                            }
                        }
                        else if (BusinessUtility.GetString(oFiles.VendorId) == Configuration.VENDORTYPE.NLMK || BusinessUtility.GetString(oFiles.VendorId) == Configuration.VENDORTYPE.NLMK1)
                        {
                            if (heat.Split('*').Length >= 2)
                            {
                                oDetails.HeatNo = heat.Split('*')[2];
                            }

                        }

                        ErrorLog.createLog(" oDetails.HeatNo = " + oDetails.HeatNo);
                    }

                    oDetails.PrntItm = BusinessUtility.GetString(ConfigurationManager.AppSettings["PrntItm54"]); ;
                    oDetails.PrntSubItm = BusinessUtility.GetString(ConfigurationManager.AppSettings["PrntSItm54"]); ;
                    oDetails.ShptNo = BusinessUtility.GetString(ConfigurationManager.AppSettings["ShpmentNo54"]);
                    oDetails.OrigShpntPcs = BusinessUtility.GetString(ConfigurationManager.AppSettings["OrigShpntPcs54"]);
                    oDetails.OrigShpntMsr = BusinessUtility.GetString(ConfigurationManager.AppSettings["OrigShpntMsr54"]);
                    oDetails.OrigShptWgt = sWeight;
                    oDetails.ShpGrsPcs = BusinessUtility.GetString(ConfigurationManager.AppSettings["ShpGrsPcs54"]); ;
                    oDetails.ShpGrsMsr = BusinessUtility.GetString(ConfigurationManager.AppSettings["ShpGrsMsr54"]); ;
                    oDetails.ShpGrsWgt = sWeight;
                    oDetails.Length = sLength;
                    oDetails.InvtQlty = BusinessUtility.GetString(ConfigurationManager.AppSettings["InvtQuality"]);
                    //oDetails.IntchgItm = BusinessUtility.GetString(BusinessUtility.GetInt(oDetails.IntchgItm) + 1);
                    oDetails.IntchgItm = BusinessUtility.GetString(BusinessUtility.GetInt(ConfigurationManager.AppSettings["IntchgItm"]));
                    m_iIntchgItm = BusinessUtility.GetInt(oDetails.IntchgItm);
                    this.GetPoDetails(BusinessUtility.GetInt(oDetails.PrntNo), ref oDetails);

                    //if (oConfiguration != null)
                    //{
                    //    if (!string.IsNullOrEmpty(oConfiguration.DimDesign))
                    //    {
                    //        oDetails.DimDsgn = oConfiguration.DimDesign;
                    //    }
                    //}
                     tctipd otctipd = new tctipd();
                     oDetails.MultiplePoItemExist = otctipd.CheckMultiplePoItemExists(oDetails.PrntNo);

                    m_iDetailId = oDetails.InsertDetails(m_iHeaderId, oDetails, oDbHelper);

                    if (oDetails.MultiplePoItemExist > 1)
                    {
                        ErrorLog.createLog("oDetails.MultiplePoItemExist = " + oDetails.MultiplePoItemExist);
                        EdiError.LogError("856", m_iFileId.ToString(), ConfigurationManager.AppSettings["MultiplePoItemExist"], m_iHeaderId, m_iDetailId,0,0,EdiError.ErrorLogType.ERROR,true);
                    }
                    else if (oDetails.MultiplePoItemExist == 0)
                    {
                        ErrorLog.createLog("oDetails.MultiplePoItemExist = " + oDetails.MultiplePoItemExist);
                        EdiError.LogError("856", m_iFileId.ToString(), ConfigurationManager.AppSettings["PoNotExist"], m_iHeaderId, m_iDetailId, 0, 0, EdiError.ErrorLogType.ERROR,true);
                    }

                }

            }
            catch (Exception ex)
            {
                ErrorLog.createLog(ex.ToString());
                EdiError.LogError(EdiFile, m_iFileId.ToString(), "", ex, "", m_iHeaderId, m_iDetailId);
                ErrorLog.CreateLog(ex);
            }
            finally
            {
                oDbHelper.CloseDatabaseConnection();
            }
            return returnHeaderID;

        }


        #endregion






        #region   This section is intended to insert data in detail table  i55

        public void ExecuteDetailItemSection(List<string> oList, int sbItm)
        {
            string sFileId = BusinessUtility.GetString(m_iFileId);
            string sStratixTagid = string.Empty;

            oShipment.CusVenId = m_sCusVenId;
            oShipment.VenShpRef = oShipment.VenShpRef != string.Empty ? oShipment.VenShpRef : BusinessUtility.GetString(ConfigurationManager.AppSettings["VenShpRef"]); 
            oShipment.CarrierVehicleDesc = BusinessUtility.GetString(ConfigurationManager.AppSettings["CarrierVehicleDesc"]);
            oShipment.DeliveryMethod = BusinessUtility.GetString(ConfigurationManager.AppSettings["DeliveryMethod"]);
            oShipment.PreRcptStatus = BusinessUtility.GetString(ConfigurationManager.AppSettings["PreRcptStatus"]);
            m_sbSql = new StringBuilder();
            iTECH.Library.DataAccess.MySql.DbHelper oDbHelper = new iTECH.Library.DataAccess.MySql.DbHelper(sMysqlConnectionString, true);
            try
            {
                if (m_iHeaderId > 0)
                {
   
                    if (oList.Count > 0)
                    {
                        var width = oList.FirstOrDefault(stringToCheck => stringToCheck.Contains("MEA*PD*WD*"));
                        var weight = oList.FirstOrDefault(stringToCheck => stringToCheck.Contains("MEA*PD*WT*")) != null ? oList.FirstOrDefault(stringToCheck => stringToCheck.Contains("MEA*PD*WT*")) : oList.FirstOrDefault(stringToCheck => stringToCheck.Contains("MEA*WT*WT*"));
                        var heat = oList.FirstOrDefault(stringToCheck => stringToCheck.Contains("REF*HC*")) != null ? oList.FirstOrDefault(stringToCheck => stringToCheck.Contains("REF*HC*")) : oList.FirstOrDefault(stringToCheck => stringToCheck.Contains("*HN*"));

                        var ActGa1 = oList.FirstOrDefault(stringToCheck => stringToCheck.Contains("MEA*PD*TH*"));

                        // mill id is vendor tag id in stratix application
                        var MillID = oList.FirstOrDefault(stringToCheck => stringToCheck.Contains("REF*LS*")) != null ? oList.FirstOrDefault(stringToCheck => stringToCheck.Contains("REF*LS*")) : oList.FirstOrDefault(stringToCheck => stringToCheck.Contains("*SN*"));

                        var ShiperNo = oList.FirstOrDefault(stringToCheck => stringToCheck.Contains("BSN*"));

                        if (ShiperNo != null)
                        {
                            ShiperNo = ShiperNo.Split('*')[2];
                        }

                        var CoilLength = oList.FirstOrDefault(stringToCheck => stringToCheck.Contains("MEA*PD*LN*"));
                        var Pcs = oList.FirstOrDefault(stringToCheck => stringToCheck.Contains("MEA*PD*PC*"));
                        if (Pcs != null)
                        {
                            Pcs = Pcs.Split('*')[3];
                        }
                        if (CoilLength != null)
                        {
                            if (CoilLength.Split('*').Length >= 4)
                            {
                                var UOM = CoilLength.Split('*')[4];
                                CoilLength = CoilLength.Split('*')[3];
                                if (UOM.Equals(UNITOFMEASURMENT.LF))
                                {
                                    CoilLength = (BusinessUtility.GetInt(CoilLength) * 12).ToString();
                                }
                            }
                        }

                        var po = oList.FirstOrDefault(stringToCheck => stringToCheck.Contains("PRF*"));
                        if (po != null)
                        {
                            po = Utility.GetIntFromString(po.Split('*')[1]).ToString();
                        }
                        else
                        {
                            EdiError.LogError("856", m_iFileId.ToString(), ConfigurationManager.AppSettings["PoMissing856"], m_iHeaderId, m_iDetailId, 0,0,EdiError.ErrorLogType.ERROR);
                        }

                        tctipd otctipd = new tctipd(); // Check if live data exists for length and some prop
                        otctipd.GetPoDetails(BusinessUtility.GetInt(po));

                        ErrorLog.createLog("PO = " + po);
                        ErrorLog.createLog("otctipd.Length = " + otctipd.Length);
                        ErrorLog.createLog("otctipd.CutNumber = " + otctipd.CutNumber);
                        ErrorLog.createLog("otctipd.CoilLength = " + otctipd.CoilLength);
                        ErrorLog.createLog("otctipd.IDa = " + otctipd.IDa);
                        ErrorLog.createLog("otctipd.ODa = " + otctipd.ODa);
                        ErrorLog.createLog("otctipd.CoilLengthType = " + otctipd.CoilLengthType);
                        ErrorLog.createLog("otctipd.ShipFrom = " + otctipd.ShipFrom);

                        m_sShipFrom = otctipd.ShipFrom;

                        // Insert detail item code here..
                        oXCTI55 = new XCTI55();
                        oXCTI55.CmpyId = m_sCompanyId;
                        oXCTI55.IntchgPfx = m_sIntchgPfx;
                        oXCTI55.IntchgNo = BusinessUtility.GetString(ConfigurationManager.AppSettings["IntchgNo"]);
                        oXCTI55.IntchgItm = BusinessUtility.GetString(ConfigurationManager.AppSettings["IntchgItm"]);    
                        oXCTI55.IntchgSitm = BusinessUtility.GetString(sbItm);
                        oXCTI55.Form = oDetails.Form;
                        oXCTI55.Grid = oDetails.Grade;
                        oXCTI55.Size = oDetails.Size;
                        oXCTI55.Finish = oDetails.Finish;
                        oXCTI55.EfEver = oDetails.ExtendedFinish;
                        ErrorLog.createLog("Edi856 parser oDetails.Form = " + oDetails.Form + " oDetails.Grade = " + oDetails.Grade);
                        if (BusinessUtility.GetDecimal(otctipd.Length) > 0)
                        {
                            oXCTI55.Length = CoilLength;
                        }
                        oXCTI55.InvtMsr = CoilLength;
                        oXCTI55.ShpntMsr = CoilLength;

                        ErrorLog.createLog(" oXCTI55.InvtMsr = " + oXCTI55.InvtMsr);

                        if (!string.IsNullOrEmpty(otctipd.CoilLengthType))
                        {
                            oXCTI55.CoilLengthTyp = otctipd.CoilLengthType;
                        }
                        else
                        {
                            oXCTI55.CoilLengthTyp = ConfigurationManager.AppSettings["CoilLengthType"].ToString();
                        }

                        oXCTI55.DimDsgn = oDetails.DimDsgn;
                        oXCTI55.RjctRsn = BusinessUtility.GetString(ConfigurationManager.AppSettings["RjctRsn"]);
                        oXCTI55.InvtPcsTyp = BusinessUtility.GetString(ConfigurationManager.AppSettings["InvtPcsTyp"]);
                        oXCTI55.InvtMsrTyp = BusinessUtility.GetString(ConfigurationManager.AppSettings["InvtMsrTyp"]);
                        oXCTI55.InvtWgtTyp = BusinessUtility.GetString(ConfigurationManager.AppSettings["InvtWgtTyp"]);
                        //oXCTI55.ShpntMsr = BusinessUtility.GetString(ConfigurationManager.AppSettings["ShpntMsr"]);
                        oXCTI55.ShpntPcsTyp = BusinessUtility.GetString(ConfigurationManager.AppSettings["ShpntPcsTyp"]);
                        oXCTI55.ShpntMsrTyp = BusinessUtility.GetString(ConfigurationManager.AppSettings["ShpntMsrTyp"]);
                        oXCTI55.ShpntWgtTyp = BusinessUtility.GetString(ConfigurationManager.AppSettings["ShpntWgtTyp"]);

                        if (MillID != null)
                        {
                            if (MillID.StartsWith(EdiKeyWords.LIN))
                            {
                                sStratixTagid = MillID.Split('*')[5];
                            }
                            else
                            {
                                sStratixTagid = MillID.Split('*')[2];
                            }
                        }

                        ErrorLog.createLog("m_iWarehouse = " + m_iWarehouse + "&& sStratixTagid = " + sStratixTagid);

                        if (m_iWarehouse != 001 && m_iWarehouse != 002 && m_iWarehouse != 003 && m_iWarehouse != 0)
                        {
                            oXCTI55.TagNo = sStratixTagid;
                        }
                        else
                        {
                            oXCTI55.TagNo = string.Empty;
                        }
                       // oXCTI55.TagNo = string.Empty;
                        //oXCTI55.TagNo = "ITECH1234";
                        
                        //oXCTI55.InvtPcs = BusinessUtility.GetString(ConfigurationManager.AppSettings["InvtPcs"]);
                        //oXCTI55.ShpntPcs = BusinessUtility.GetString(ConfigurationManager.AppSettings["ShpntPcs"]);
                        oXCTI55.InvtPcs = BusinessUtility.GetString(ConfigurationManager.AppSettings["InvtPcs"]);
                        oXCTI55.ShpntPcs = BusinessUtility.GetString(ConfigurationManager.AppSettings["ShpntPcs"]);
                        //oXCTI55.InvtMsr = BusinessUtility.GetString(ConfigurationManager.AppSettings["InvtMsr"]);
                        oXCTI55.InvtQlty = oDetails.InvtQlty;
                        oXCTI55.Loc = BusinessUtility.GetString(ConfigurationManager.AppSettings["Location"]);
                        oXCTI55.Pkg = BusinessUtility.GetString(ConfigurationManager.AppSettings["Pkg"]);

                        oXCTI55.Mill = oFiles.Mill;

                       // oXCTI55.Mill = "TKN";

                        if (!string.IsNullOrEmpty(otctipd.CutNumber))
                        {
                            oXCTI55.CutNo = otctipd.CutNumber;
                        }
                        else
                        {
                            oXCTI55.CutNo = string.Empty;
                        }

                        if (CoilLength != null && BusinessUtility.GetDecimal(otctipd.CoilLength) > 0)
                        {
                            oXCTI55.CoilLength = CoilLength;
                        }


                        if (weight != null)
                        {
                            if (weight.Split('*').Length >= 3)
                            {
                                oXCTI55.InvtWgt = weight;
                                oXCTI55.InvtWgt = oXCTI55.InvtWgt.Split('*')[3];
                                oXCTI55.ShpntWgt = oXCTI55.InvtWgt;
                            }
                        }
                        else
                        {
                            oXCTI55.InvtWgt = "1.0";
                        }
                        if (heat != null)
                        {
                            if (BusinessUtility.GetString(oFiles.VendorId) == Configuration.VENDORTYPE.NUCORBERKELEY || BusinessUtility.GetString(oFiles.VendorId) == Configuration.VENDORTYPE.NUCORDECATUR || BusinessUtility.GetString(oFiles.VendorId) == Configuration.VENDORTYPE.NUCORHICKMAN || BusinessUtility.GetString(oFiles.VendorId) == Configuration.VENDORTYPE.NUCORCRAWFORDSVILLE)
                            {
                                if (heat.Split('*').Length >= 7)
                                {
                                    oXCTI55.Heat = heat.Split('*')[7];
                                }
                            }
                            else if (BusinessUtility.GetString(oFiles.VendorId) == Configuration.VENDORTYPE.NLMK || BusinessUtility.GetString(oFiles.VendorId) == Configuration.VENDORTYPE.NLMK1)
                            {
                                if (heat.Split('*').Length >= 2)
                                {
                                    oXCTI55.Heat = heat.Split('*')[2];
                                }
                              
                            }
                            else
                            {
                                oXCTI55.Heat = Utility.RemoveTrailingZero(Utility.GetElementValue(heat, EdiKeyWords.HN));
                                if (string.IsNullOrEmpty(oXCTI55.Heat))
                                {
                                    oXCTI55.Heat = Utility.RemoveTrailingZero(Utility.GetElementValue(heat, EdiKeyWords.HC));
                                }

                            }

                            ErrorLog.createLog(" oXCTI55.Heat = " + oXCTI55.Heat);
                        }
                        else
                        {
                            EdiError.LogError("856", m_iFileId.ToString(), ConfigurationManager.AppSettings["HeatMissing856"], m_iHeaderId, m_iDetailId,0,0,EdiError.ErrorLogType.ERROR);
                        }
                        EdiError.ValidateHEAT(oXCTI55.Heat, "Inside ExecuteDetailItemSection", BusinessUtility.GetString(Globalcl.FileId));
                        if (ActGa1 != null)
                        {
                            if (oFiles.VendorName != Configuration.VENDORTYPE.NUCOR && oFiles.VendorName != Configuration.VENDORTYPE.NLMK)
                            {
                               // oXCTI55.ActGa1 = ActGa1;
                               // oXCTI55.ActGa1 = oXCTI55.ActGa1.Split('*')[3];
                            }
                        }

                        //if (MillID != null)
                        //{
                        //    if (MillID.Split('*').Length >= 3)
                        //    {
                        //        oXCTI55.MillId = MillID.Split('*')[3];
                        //    }
                        //}

                        if (MillID != null)
                        {
                            if (MillID.StartsWith(EdiKeyWords.LIN))
                            {
                                oXCTI55.MillId = MillID.Split('*')[5];
                            }
                            else
                            {
                                oXCTI55.MillId = MillID.Split('*')[2];
                            }
                        }


                        if (!string.IsNullOrEmpty(otctipd.IDa))
                        {
                            oXCTI55.I55Id = otctipd.IDa;
                        }
                        if (!string.IsNullOrEmpty(otctipd.ODa))
                        {
                            oXCTI55.I55Od = otctipd.ODa;
                        }


                       // oXCTI55.TagNo = "ITECH123";

                        //this.GetMeasurment(BusinessUtility.GetInt(oDetails.PrntNo), ref oXCTI55);
                        if (oConfiguration.SkipSameVendorTagID || oConfiguration.CheckQDS)
                        {
                            if (!CheckVendorTagExist(sStratixTagid))
                            {
                                if (oConfiguration.CheckQDS)
                                {
                                    int iHeat = oFiles.CheckQdsExists(oXCTI55.Mill, oXCTI55.Heat, BusinessUtility.GetString(ShiperNo));
                                    if (iHeat > 0)
                                    {
                                        oXCTI55.InsertDetailItems(m_iHeaderId, m_iDetailId, oXCTI55, oDbHelper);
                                        EdiError.LogError(Globalcl.FileType, m_iFileId.ToString(), "Heat qds exists : " + iHeat, m_iHeaderId, m_iDetailId,0,0,EdiError.ErrorLogType.INFO);
                                    }
                                    else
                                    {
                                        EdiError.LogError(Globalcl.FileType, m_iFileId.ToString(), "QDS information not received from the mill, tag : " + sStratixTagid, m_iHeaderId, m_iDetailId, 0, 0, EdiError.ErrorLogType.ERROR,true);
                                        oFiles.FailedVendorTag(m_iFileId, oConfiguration.VendorId, sStratixTagid, po, oXCTI55.Heat, oXCTI55.InvtWgt, "QDS information not received from the mill, tag : " + sStratixTagid);
                                    }
                                }
                                else if (oConfiguration.SkipSameVendorTagID)
                                {
                                    oXCTI55.InsertDetailItems(m_iHeaderId, m_iDetailId, oXCTI55, oDbHelper);
                                }
                            }
                            else
                            {
                                EdiError.LogError("856", m_iFileId.ToString(), ConfigurationManager.AppSettings["VendorTagExist"], m_iHeaderId, m_iDetailId,0,0,EdiError.ErrorLogType.ERROR,true);
                                oFiles.FailedVendorTag(m_iFileId, oConfiguration.VendorId, sStratixTagid, po, oXCTI55.Heat, oXCTI55.InvtWgt, "Vendor tag exists");
                            }
                        }
                        else
                        {
                            oXCTI55.InsertDetailItems(m_iHeaderId, m_iDetailId, oXCTI55, oDbHelper);
                        }



                    }


                }

            }
            catch (Exception ex)
            {
                ErrorLog.createLog(ex.ToString());
                EdiError.LogError(EdiFile, m_iFileId.ToString(), "", ex, "", m_iHeaderId, m_iDetailId);
                ErrorLog.CreateLog(ex);
            }
            finally
            {
                oDbHelper.CloseDatabaseConnection();
            }


        }


        #endregion

        public bool CheckVendorTagExist(string sVendorTag)
        {
            bool bExists = false;
            m_sSql = "select 1 from detailitem where MillId = '" + sVendorTag + "'";
            iTECH.Library.DataAccess.MySql.DbHelper oDbHelper = new iTECH.Library.DataAccess.MySql.DbHelper();

            try
            {
                MySqlParameter[] oMySqlParameter = { };
                bExists = oDbHelper.GetDataTable(m_sSql, CommandType.Text, oMySqlParameter).Rows.Count > 0;
            }
            catch (Exception ex)
            {
                ErrorLog.createLog(ex.ToString());
                EdiError.LogError(EdiFile, m_iFileId.ToString(), "", ex, "");
                ErrorLog.CreateLog(ex);
            }
            finally
            {
                oDbHelper.CloseDatabaseConnection();
            }
            return bExists;
        }


        public void FailedVendorTag(int iFileId, string sVendorId, string sVenTagId)
        {
            m_sSql = "INSERT INTO skippedvendortagid (FileId,VendorID,VendorTagID)VALUES(" + iFileId + ", '" + sVendorId + "', '" + sVenTagId + "')";
            iTECH.Library.DataAccess.MySql.DbHelper oDbHelper = new iTECH.Library.DataAccess.MySql.DbHelper();

            try
            {
                MySqlParameter[] oMySqlParameter = { };
                oDbHelper.ExecuteNonQuery(m_sSql, CommandType.Text, oMySqlParameter);
            }
            catch (Exception ex)
            {
                ErrorLog.createLog(ex.ToString());
                EdiError.LogError(EdiFile, m_iFileId.ToString(), "", ex, "");
                ErrorLog.CreateLog(ex);
            }
            finally
            {
                oDbHelper.CloseDatabaseConnection();
            }
        }


    }
}
