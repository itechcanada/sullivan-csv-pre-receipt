﻿using iTECH.Library.Utilities;
using System;
using System.Configuration;
using System.IO;
using System.Xml;

namespace SecureFTP
{
    /// <summary>
    /// This class created for new xml file received for skyline xml edi .
    /// file path E:\Project\WorkingCopy\EDI PROJ\SkylineEdi\SecureFTP\Docs\Files\NEW XML FORMAT RECEIVED 12-09-2019
    /// </summary>
    class Edi856XmlParserVer2
    {
        public static string m_XmlData = string.Empty;
        MyXmlReader m_fullXml = null;
        public static XmlNodeList m_HeaderXmlNodeList = null;
        public static XmlNodeList m_DetailXmlNodeList = null;
        public static XmlNodeList m_ItemXmlNodeList = null;
        MyXmlReader omyXmlReader = null;
        Files oFiles = null;
        int m_iFileId = 0;
        int m_iHeaderId = 0;
        int m_iDetailId = 0;
        int m_iDtailItem = 0;
        static string sStratixConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ToString();
        static string sMysqlConnectionString = ConfigurationManager.ConnectionStrings["NewConnectionString"].ToString();

        static string m_sCompanyId = ConfigurationManager.AppSettings["CompanyId"];
        static string m_sIntchgPfx = ConfigurationManager.AppSettings["IntchgPfx"];
        static string m_sCusVenId = ConfigurationManager.AppSettings["CusVenId"];
        static string m_sRcptType = ConfigurationManager.AppSettings["RcptType"];
        string sLocation = string.Empty;
        string m_sShipFrom = string.Empty;
        int m_iWarehouse = 0;
        string m_sBol = string.Empty;

        public Edi856XmlParserVer2(XmlNodeList HeaderXmlNodeList, XmlNodeList BodyXmlNodeList, int iFileId)
        {
            m_HeaderXmlNodeList = HeaderXmlNodeList;
            m_DetailXmlNodeList = BodyXmlNodeList;
            m_iFileId = iFileId;
            oFiles = new Files(m_iFileId);
            try
            {
                //m_XmlData = File.ReadAllText(sXmlPath);
                ParseData();
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLog(ex);
            }
        }

        public void ParseData()
        {
            try
            {
                foreach (XmlElement oNode in m_DetailXmlNodeList)
                {
                    if (m_ItemXmlNodeList == null)
                    {
                        XmlElement onodelist = oNode;
                        omyXmlReader = new MyXmlReader(onodelist.OuterXml);
                        m_ItemXmlNodeList = omyXmlReader.GetNodeList("Body/Item");
                    }
                }
                ParseHeader();
                ParseDeatil();
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLog(ex);
            }
        }

        Shipment oShipment = new Shipment();
        public void ParseHeader()
        {
            try
            {
                foreach (XmlElement oNode in m_HeaderXmlNodeList)
                {
                    XmlElement onodelist = oNode;
                    omyXmlReader = new MyXmlReader(onodelist.OuterXml);
                    m_sBol = omyXmlReader.GetNode("Header/BillOfLadingReference").InnerText;
                    //XmlNodeList oPartnerList = omyXmlReader.GetNodeList("Header/Partner");
                    //XmlNode oPartner = oPartnerList[2];
                    //omyXmlReader = new MyXmlReader(oPartner.OuterXml);
                    oShipment.ShipTo = omyXmlReader.GetNode("Header/ShipTo/CustomerID").InnerText;
                    oShipment.VenShpRef = m_sBol;
                    //oShipment.VenShpRef = "1158864";
                    oShipment.CarrierVehicleDesc = omyXmlReader.GetNode("Header/ShipmentMode").InnerText;
                    oShipment.DeliveryMethod = "";
                    oShipment.CarrierName = omyXmlReader.GetNode("Header/Carrier/CarrierID").InnerText;
                    oShipment.CarrierRefNo = omyXmlReader.GetNode("Header/Carrier/CarrierID").InnerText;
                    sLocation = omyXmlReader.GetNode("Header/VehicleID").InnerText;
                    sLocation = Utility.GetLocation(sLocation);
                    // oShipment.RcvWhs = ""; // updating whs in ParseDeatil method from po.
                    oShipment.PreRcptStatus = BusinessUtility.GetString(ConfigurationManager.AppSettings["PreRcptStatus"]);
                    Header oHeader = new Header();
                    m_iHeaderId = oHeader.HeaderInsertion(Globalcl.FileId, oShipment);


                }
                // MyXmlReader header = new MyXmlReader(m_HeaderXmlNodeList[0].ToString());
            }
            catch {
                ErrorLog.createLog("Error while parsing header data");
            }

        }
        public void ParseDeatil()
        {
            iTECH.Library.DataAccess.MySql.DbHelper oDbHelper = new iTECH.Library.DataAccess.MySql.DbHelper(sMysqlConnectionString, true);
            try
            {
                int iItm = 0;
                foreach (XmlElement oNode in m_ItemXmlNodeList)
                {
                    iItm = iItm + 1;
                    Details oDetails = new Details();
                    XmlElement onodelist = oNode;
                    omyXmlReader = new MyXmlReader(onodelist.OuterXml);
                    oDetails.PrntNo = omyXmlReader.GetNode("Item/CustomerPurchaseOrder").InnerText;
                    oDetails.PrntItm = omyXmlReader.GetNode("Item/CustomerPurchaseOrderLine").InnerText;
                    string[] sar = oDetails.PrntNo.Split('-');
                    if(sar.Length > 1)
                    {
                        oDetails.PrntNo = sar[1];
                    }
                    oDetails.PrntSubItm = "1";
                    oDetails.ShipperNo = m_sBol; // bol
                    oDetails.IntchgSItm = "1";
                    oDetails.ShptNo = m_sBol;
                        //omyXmlReader.GetNode("Item/ItemURN").InnerText;
                    oDetails.CusRtnMthd = "";
                    oDetails.ShpGrsPcs = Utility.GetConfigValue("ShpGrsPcs54");
                    oDetails.ShpGrsMsr = Utility.GetConfigValue("ShpGrsMsr54");
                    oDetails.ShpGrsWgt = omyXmlReader.GetNode("Item/Weight").InnerText;
                    oDetails.OrigShpntPcs = omyXmlReader.GetNode("Item/Qty").InnerText;

                    var CoilLength = omyXmlReader.GetNode("Item/Length").InnerText;
                    oDetails.OrigShpntMsr = CoilLength;

                    string OrderedWeight = "";
                    if (!string.IsNullOrEmpty(omyXmlReader.GetNode("Item/Weight").InnerText))
                    {
                        OrderedWeight = omyXmlReader.GetNode("Item/Weight").InnerText;
                    }
                    if (!string.IsNullOrEmpty(OrderedWeight))
                    {
                        oDetails.OrigShptWgt = OrderedWeight;
                    }
                    else
                    {
                        oDetails.OrigShptWgt = "1.0";
                    }
                    oDetails.HeatNo = omyXmlReader.GetNode("Item/Heat/HeatID").InnerText;
                    tctipd otctipd = new tctipd();
                    //Shipment oShipment = new Shipment(); oShipment
                    otctipd.PopulateWarehouse(ref oShipment , oDetails.PrntNo, m_sCompanyId);
                    m_iWarehouse = BusinessUtility.GetInt(oShipment.RcvWhs);
                    Header oHeader = new Header();
                    oShipment.VenShpRef = m_sBol; // bol
                    oHeader.UpdateHeader(m_iHeaderId, oShipment.RcvWhs, oShipment.VenShpRef, oShipment.CarrierName, oShipment.CarrierVehicleDesc);
                    Utility.GetPoDetails(BusinessUtility.GetInt(oDetails.PrntNo), ref oDetails);
                    oDetails.MultiplePoItemExist = 1;
                    oDetails.IntchgItm = iItm.ToString();
                    m_iDetailId = oDetails.InsertDetails(m_iHeaderId, oDetails, oDbHelper);
                    if(m_iDetailId > 0)
                    {
                        this.ParseDetailItem(onodelist.OuterXml,BusinessUtility.GetInt(oDetails.PrntNo), BusinessUtility.GetInt(oDetails.PrntItm), iItm, oDetails.HeatNo, oDetails.OrigShpntMsr, oDetails.OrigShptWgt);
                    }

                }
            }
            catch (Exception ex)
            {
                ErrorLog.createLog(ex.ToString());
                EdiError.LogError(oFiles.EdiFileType, m_iFileId.ToString(), "", ex, "", m_iHeaderId, m_iDetailId);
                ErrorLog.CreateLog(ex);
            }
            finally
            {
                oDbHelper.CloseDatabaseConnection();
            }
        }

        public static string TruncateLongString(string str, int maxLength)
        {
            if (string.IsNullOrEmpty(str))
                return str;
            return str.Substring(0, Math.Min(str.Length, maxLength));
        }

        public void ParseDetailItem(string sDeatilItemSEction, int iPO, int iPOItem, int iIntItem, string sHeat, string sLength, string sWeight)
        {
            iTECH.Library.DataAccess.MySql.DbHelper oDbHelper = new iTECH.Library.DataAccess.MySql.DbHelper(sMysqlConnectionString, true);
            try
            {
                int iItm = 0;
                omyXmlReader = new MyXmlReader(sDeatilItemSEction);
                iItm = iItm + 1;
                Details oDetails = new Details();
                oDetails.PrntNo = iPO.ToString();
                oDetails.PrntItm = iPOItem.ToString();
                oDetails.PrntSubItm = "1";
                oDetails.HeatNo = sHeat;
                Utility.GetPoDetails(BusinessUtility.GetInt(oDetails.PrntNo), ref oDetails);

                XCTI55 oXCTI55 = new XCTI55();
                oXCTI55.CmpyId = m_sCompanyId;
                oXCTI55.IntchgPfx = m_sIntchgPfx;
                oXCTI55.IntchgNo = BusinessUtility.GetString(ConfigurationManager.AppSettings["IntchgNo"]);
                oXCTI55.IntchgItm = iIntItem.ToString();
                oXCTI55.IntchgSitm = iItm.ToString();
                oXCTI55.Form = oDetails.Form;
                oXCTI55.Grid = oDetails.Grade;
                oXCTI55.Size = oDetails.Size;
                oXCTI55.Finish = oDetails.Finish;
                oXCTI55.EfEver = oDetails.ExtendedFinish;
                var CoilLength = sLength;
                if (BusinessUtility.GetDecimal(CoilLength) > 0)
                {
                    oXCTI55.Length = CoilLength;
                }
                oXCTI55.InvtMsr = CoilLength;
                oXCTI55.ShpntMsr = CoilLength;
                tctipd otctipd = new tctipd(); // Check if live data exists for length and some prop
                otctipd.GetPoDetails(BusinessUtility.GetInt(oDetails.PrntNo), iPOItem);
                if (!string.IsNullOrEmpty(otctipd.CoilLengthType))
                {
                    oXCTI55.CoilLengthTyp = otctipd.CoilLengthType;
                }
                else
                {
                    oXCTI55.CoilLengthTyp = ConfigurationManager.AppSettings["CoilLengthType"].ToString();
                }
                oXCTI55.CoilLengthTyp = "";
                oXCTI55.DimDsgn = otctipd.DimDsgn;
                oXCTI55.RjctRsn = BusinessUtility.GetString(ConfigurationManager.AppSettings["RjctRsn"]);
                oXCTI55.InvtPcsTyp = BusinessUtility.GetString(ConfigurationManager.AppSettings["InvtPcsTyp"]);
                oXCTI55.InvtMsrTyp = BusinessUtility.GetString(ConfigurationManager.AppSettings["InvtMsrTyp"]);
                oXCTI55.InvtWgtTyp = BusinessUtility.GetString(ConfigurationManager.AppSettings["InvtWgtTyp"]);
                //oXCTI55.ShpntMsr = BusinessUtility.GetString(ConfigurationManager.AppSettings["ShpntMsr"]);
                oXCTI55.ShpntPcsTyp = BusinessUtility.GetString(ConfigurationManager.AppSettings["ShpntPcsTyp"]);
                oXCTI55.ShpntMsrTyp = BusinessUtility.GetString(ConfigurationManager.AppSettings["ShpntMsrTyp"]);
                oXCTI55.ShpntWgtTyp = BusinessUtility.GetString(ConfigurationManager.AppSettings["ShpntWgtTyp"]);
                string sStratixTagid = omyXmlReader.GetNode("Item/ItemURN").InnerText;
                if (sStratixTagid.Length > 15)
                {
                    sStratixTagid = sStratixTagid.Replace("18109254500", string.Empty);
                }
                ErrorLog.createLog("m_iWarehouse = " + m_iWarehouse + "&& sStratixTagid = " + sStratixTagid);
                if (m_iWarehouse != 001 && m_iWarehouse != 002 && m_iWarehouse != 003 && m_iWarehouse != 0)
                {
                    oXCTI55.TagNo = sStratixTagid;
                }
                else
                {
                    oXCTI55.TagNo = string.Empty;
                }

                //oXCTI55.TagNo = sStratixTagid;
                oXCTI55.InvtPcs = BusinessUtility.GetString(ConfigurationManager.AppSettings["InvtPcs"]);
                oXCTI55.ShpntPcs = BusinessUtility.GetString(ConfigurationManager.AppSettings["ShpntPcs"]);
                oXCTI55.InvtQlty = otctipd.InvtQlty;

                oXCTI55.Loc = !string.IsNullOrEmpty(sLocation) ? sLocation: BusinessUtility.GetString(ConfigurationManager.AppSettings["Location"]);
                oXCTI55.Pkg = BusinessUtility.GetString(ConfigurationManager.AppSettings["Pkg"]);
                oXCTI55.Mill = oFiles.Mill;
                if (!string.IsNullOrEmpty(otctipd.CutNumber))
                {
                    oXCTI55.CutNo = otctipd.CutNumber;
                }
                else
                {
                    oXCTI55.CutNo = string.Empty;
                }

                if (CoilLength != null && BusinessUtility.GetDecimal(otctipd.CoilLength) > 0)
                {
                    oXCTI55.CoilLength = CoilLength;
                }

                var OrderedWeight = sWeight;
                if (!string.IsNullOrEmpty(OrderedWeight))
                {
                    oXCTI55.InvtWgt = OrderedWeight;
                    oXCTI55.ShpntWgt = OrderedWeight;
                }
                else
                {
                    oXCTI55.InvtWgt = "1.0";
                }
                oXCTI55.Heat = oDetails.HeatNo;
                EdiError.ValidateHEAT(oXCTI55.Heat, "ExecuteDetailItemSection Heat missing", BusinessUtility.GetString(Globalcl.FileId));
                oXCTI55.MillId = sStratixTagid;
                if (!string.IsNullOrEmpty(otctipd.IDa))
                {
                    oXCTI55.I55Id = otctipd.IDa;
                }
                if (!string.IsNullOrEmpty(otctipd.ODa))
                {
                    oXCTI55.I55Od = otctipd.ODa;
                }
                m_sShipFrom = otctipd.ShipFrom;
                m_iDtailItem = oXCTI55.InsertDetailItems(m_iHeaderId, m_iDetailId, oXCTI55, oDbHelper);
            }

            catch (Exception ex)
            {
                ErrorLog.createLog(ex.ToString());
                EdiError.LogError(oFiles.EdiFileType, m_iFileId.ToString(), "", ex, "", m_iHeaderId, m_iDetailId);
                ErrorLog.CreateLog(ex);
            }
            finally
            {
                oDbHelper.CloseDatabaseConnection();
            }
        }


        public string GetWeightValueFromXml(string sActualDimention, int iCode)
        {
            string sReturn = string.Empty;
            try
            {
                MyXmlReader omyXmlReader = new MyXmlReader(sActualDimention);
                XmlNodeList onodelist1 = omyXmlReader.GetNodeList("ActualDimensions/Weight");
                foreach (XmlElement oNode in onodelist1)
                {
                    XmlElement onodelist = oNode;
                    omyXmlReader = new MyXmlReader(onodelist.OuterXml);
                    int sCode = BusinessUtility.GetInt(omyXmlReader.GetNode("Weight/UnitOrBasisForMeasCode/CodeValue").InnerText);
                    if (sCode == iCode)
                    {
                        string sWeight = omyXmlReader.GetNode("Weight/MeasValue").InnerText;
                        sReturn = sWeight;
                        break;
                    }
                }
            }
            catch { }
            return sReturn;
        }

        public string GetMsrValueFromXml(string sActualDimention)
        {
            string sReturn = string.Empty;
            try
            {
                MyXmlReader omyXmlReader = new MyXmlReader(sActualDimention);
                XmlNodeList onodelist1 = omyXmlReader.GetNodeList("ActualDimensions/Len");
                foreach (XmlElement oNode in onodelist1)
                {
                    XmlElement onodelist = oNode;
                    omyXmlReader = new MyXmlReader(onodelist.OuterXml);
                    string sCode = omyXmlReader.GetNode("Len/UnitOrBasisForMeasCode/CodeValue").InnerText;
                    string sWeight = omyXmlReader.GetNode("Len/MeasValue").InnerText;
                    if (sCode == "LF")
                    {
                        sReturn = BusinessUtility.GetString(BusinessUtility.GetDecimal(sWeight) * 12);  // If LF then multiply with 12 else mul by 1 suggested by sir.
                        break;
                    }
                    else
                    {
                        sReturn = BusinessUtility.GetString(BusinessUtility.GetDecimal(sWeight) * 1);
                        break;
                    }
                }
            }
            catch { }
            return sReturn;
        }


    }

}

