﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
using System.IO;
using System.Text;

namespace SecureFTP
{

    public static class ErrorLog
    {
        static string slogFilePath = Path.Combine(Utility.GetConfigValue("LogsDiractory"), "Log");
        static string sFileName = "Log-" + DateTime.Now.ToString("yyyy-MM-dd") + ".txt";
        static string sFilePath = slogFilePath + sFileName;

        public static void createLog(string errorMessage)
        {
            try
            {
                //  string path = ConfigurationManager.AppSettings["LogsDiractory"] + "Log" + DateTime.Now.ToString("yyyy-MM-dd") + ".txt";
                string path = slogFilePath + DateTime.Now.ToString("yyyy-MM-dd") + ".txt";

                // if (BusinessUtility.GetString(ConfigurationManager.AppSettings["LogError"]).ToString().ToUpper() == "TRUE")
                // {
                if (!Directory.Exists(slogFilePath))
                {
                    Directory.CreateDirectory(slogFilePath);
                }
                if (!File.Exists(path))
                {
                    var objFile = File.Create(path);
                    objFile.Close();
                }
                if (!File.Exists(path))
                {
                    StreamWriter sw = File.CreateText(path);
                    sw.Close();
                }
                using (System.IO.StreamWriter sw = System.IO.File.AppendText(path))
                {
                    sw.WriteLine("-------- " + DateTime.Now + " --------");
                    sw.WriteLine(errorMessage);
                    sw.WriteLine("------------------------");
                    sw.Close();
                }
                // }
            }
            catch { }
        }

        public static void CreateLog(Exception ex)
        {
            try
            {
                if (!Directory.Exists(slogFilePath))
                {
                    Directory.CreateDirectory(slogFilePath);
                }
                if (!File.Exists(sFilePath))
                {
                    var objFile = File.Create(sFilePath);
                    objFile.Close();
                }



                StringBuilder sbLogText = new StringBuilder("Log starts :" + DateTime.Now);
                sbLogText.AppendLine();
                sbLogText.AppendLine();
                sbLogText.Append(ex.ToString());
                sbLogText.AppendLine();
                sbLogText.AppendLine();
                sbLogText.Append("Log ends :" + DateTime.Now);
                sbLogText.AppendLine();
                sbLogText.AppendLine();
                sbLogText.AppendLine();
                bool bAppend = true;
                StreamWriter oStreamWriter = new StreamWriter(sFilePath, bAppend);
                oStreamWriter.Write(sbLogText);
                oStreamWriter.Close();
            }
            catch (Exception ex1)
            {
                ErrorLog.createLog(ex1.ToString());
            }
            finally
            {
                EdiError.SendErrorMail(Globalcl.FileId, "ERROR IN FILE ID : " + Globalcl.FileId, ex.ToString());
            }
        }
    }

}
