ALTER TABLE `inbiz_edi`.`configuration` 
ADD COLUMN `QdsApproved` BIT(1) NULL DEFAULT 0 AFTER `SkipSameVendorTagID`;


ALTER TABLE `inbiz_edi`.`configuration` 
CHANGE COLUMN `FileProcessingType` `FileProcessingType` INT(11) NULL DEFAULT '0' COMMENT '0 is for default, 1 is for Create new pre-Receipt everytime BSN changes' ;



CREATE TABLE `inbiz_edi`.`filedetail` (
  `FileDetailId` INT NOT NULL AUTO_INCREMENT,
  `FileId` INT NULL DEFAULT 0,
  `Bsn` VARCHAR(45) NULL DEFAULT '',
  `Po` VARCHAR(45) NULL,
  `CreatedOn` DATETIME NULL DEFAULT now(),
  `Active` BIT(1) NULL DEFAULT 1,
  PRIMARY KEY (`FileDetailId`))
COMMENT = 'Holds file detail like how much bsn and po exists in particular file.';


ALTER TABLE `inbiz_edi`.`filedetail` 
ADD COLUMN `HeaderId` INT NULL DEFAULT 0 AFTER `Active`;


ALTER TABLE `inbiz_edi`.`filedetail` 
ADD COLUMN `DetailId` INT NULL DEFAULT 0 AFTER `HeaderId`;


ALTER TABLE `inbiz_edi`.`header` 
ADD COLUMN `ReceiptCreated` BIT(1) NULL DEFAULT 0 AFTER `IntchgNo`;


ALTER TABLE `inbiz_edi`.`header` 
ADD COLUMN `ProcessType` INT NULL DEFAULT 0AFTER `ReceiptCreated`;



ALTER TABLE `inbiz_edi`.`detail` 
ADD COLUMN `ShipperNo` VARCHAR(45) NULL DEFAULT '' AFTER `PrntSubItm`;


ALTER TABLE `inbiz_edi`.`detail` 
CHANGE COLUMN `IntchgNo` `IntchgNo` INT(11) NULL DEFAULT '0' AFTER `ShipperNo`;


ALTER TABLE `inbiz_edi`.`detail` 
CHANGE COLUMN `IntchgItm` `IntchgItm` INT(11) NULL AFTER `IntchgNo`;


21-04

ALTER TABLE `inbiz_edi`.`skippedvendortagid` 
ADD COLUMN `PoNo` VARCHAR(45) NULL DEFAULT '' AFTER `SkippedDateTime`,


ADD COLUMN `Heat` VARCHAR(45) NULL DEFAULT '' AFTER `PoNo`,
ADD COLUMN `Weight` VARCHAR(45) NULL DEFAULT '' AFTER `Heat`;



ALTER TABLE `inbiz_edi`.`detail` 
ADD COLUMN `MultiplePoItemExist` BIT(1) NULL DEFAULT 0 AFTER `IsReceiptUpdated`;


ALTER TABLE `inbiz_edi`.`detail` 
CHANGE COLUMN `MultiplePoItemExist` `MultiplePoItemExist` TINYINT(1) NULL DEFAULT b'0' ;



-----
03-05

ALTER TABLE `inbiz_edi`.`header863` 
ADD COLUMN `VendorTagId` VARCHAR(45) NULL DEFAULT '' AFTER `CreatedOn`;



ALTER TABLE `inbiz_edi`.`files` 
CHANGE COLUMN `FileName` `FileName` VARCHAR(500) NULL DEFAULT '' ;


ALTER TABLE `inbiz_edi`.`files` 
CHANGE COLUMN `FilePath` `FilePath` VARCHAR(500) NULL DEFAULT '' ;





-----
10-05

ALTER TABLE `inbiz_edi`.`detail863` 
ADD COLUMN `ChemistrySequence` INT NULL DEFAULT 0 AFTER `TestAlpha`;


ALTER TABLE `inbiz_edi`.`chem_mapping` 
ADD COLUMN `ChemistrySequence` INT NULL DEFAULT 0 AFTER `Description`;



UPDATE `inbiz_edi`.`chem_mapping` SET `ChemistrySequence`='1' WHERE `EdiText`='C';
UPDATE `inbiz_edi`.`chem_mapping` SET `ChemistrySequence`='2' WHERE `EdiText`='MN';
UPDATE `inbiz_edi`.`chem_mapping` SET `ChemistrySequence`='3' WHERE `EdiText`='P';
UPDATE `inbiz_edi`.`chem_mapping` SET `ChemistrySequence`='4' WHERE `EdiText`='S';
UPDATE `inbiz_edi`.`chem_mapping` SET `ChemistrySequence`='5' WHERE `EdiText`='SI';
UPDATE `inbiz_edi`.`chem_mapping` SET `ChemistrySequence`='6' WHERE `EdiText`='AL';
UPDATE `inbiz_edi`.`chem_mapping` SET `ChemistrySequence`='7' WHERE `EdiText`='CU';
UPDATE `inbiz_edi`.`chem_mapping` SET `ChemistrySequence`='8' WHERE `EdiText`='NI';
UPDATE `inbiz_edi`.`chem_mapping` SET `ChemistrySequence`='9' WHERE `EdiText`='CR';
UPDATE `inbiz_edi`.`chem_mapping` SET `ChemistrySequence`='10' WHERE `EdiText`='MO';
UPDATE `inbiz_edi`.`chem_mapping` SET `ChemistrySequence`='11' WHERE `EdiText`='V';
UPDATE `inbiz_edi`.`chem_mapping` SET `ChemistrySequence`='12' WHERE `EdiText`='NB';
UPDATE `inbiz_edi`.`chem_mapping` SET `ChemistrySequence`='13' WHERE `EdiText`='TI';
UPDATE `inbiz_edi`.`chem_mapping` SET `ChemistrySequence`='14' WHERE `EdiText`='N';
UPDATE `inbiz_edi`.`chem_mapping` SET `ChemistrySequence`='15' WHERE `EdiText`='B';
UPDATE `inbiz_edi`.`chem_mapping` SET `ChemistrySequence`='16' WHERE `EdiText`='SN';


------------------
12-05-2016

DROP TABLE IF EXISTS `edioriginzone`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `edioriginzone` (
  `DunsNumber` varchar(100) NOT NULL,
  `VendorId` varchar(45) NOT NULL,
  `StratixMillCode` varchar(10) NOT NULL,
  `HeatCode` varchar(10) NOT NULL COMMENT 'holds origin zone.',
  `VendorName` varchar(100) DEFAULT '',
  `MeltOrigin` varchar(100) DEFAULT '',
  `OriginZone` varchar(10) DEFAULT '',
  `StratixOriginZone` varchar(10) DEFAULT '',
  `CreatedOn` datetime DEFAULT CURRENT_TIMESTAMP,
  `Active` bit(1) DEFAULT b'1',
  PRIMARY KEY (`DunsNumber`,`VendorId`,`StratixMillCode`,`HeatCode`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `edioriginzone`
--

LOCK TABLES `edioriginzone` WRITE;
/*!40000 ALTER TABLE `edioriginzone` DISABLE KEYS */;
INSERT INTO `edioriginzone` VALUES ('118899371OC','696','ND','*','Nucor - Decatur','Nucor - Decatur','USA','USA','2016-05-12 17:13:07',''),('181157009','381','NU','*','Nucor - Crawfordsville','Nucor - Crawfordsville','USA','USA','2016-05-12 17:13:07',''),('609732979','2001','NP','ACO','NLMK Pennsylvania','Gerdau ','Foreign','X','2016-05-12 17:13:07',''),('609732979','2001','NP','AKR','NLMK Pennsylvania','AK Steel-Rockport','USA','USA','2016-05-12 17:13:07',''),('609732979','2001','NP','AKS','NLMK Pennsylvania','AK Steel ','USA','USA','2016-05-12 17:13:07',''),('609732979','2001','NP','AMB','NLMK Pennsylvania','Mittal - Arcelor ','Foreign','X','2016-05-12 17:13:07',''),('609732979','2001','NP','AMK','NLMK Pennsylvania','Alchevsk ','Foreign','X','2016-05-12 17:13:07',''),('609732979','2001','NP','AMR','NLMK Pennsylvania','Mittal - Riverdale ','USA','USA','2016-05-12 17:13:07',''),('609732979','2001','NP','BTA','NLMK Pennsylvania','Beta Steel ','USA','USA','2016-05-12 17:13:07',''),('609732979','2001','NP','CAR','NLMK Pennsylvania','Carsid ','Foreign','X','2016-05-12 17:13:07',''),('609732979','2001','NP','DCL','NLMK Pennsylvania','Duferco ','Foreign','X','2016-05-12 17:13:07',''),('609732979','2001','NP','DLL','NLMK Pennsylvania','Duferco ','Foreign','X','2016-05-12 17:13:07',''),('609732979','2001','NP','DOF','NLMK Pennsylvania','Dofasco - Arcelor','Foreign','X','2016-05-12 17:13:07',''),('609732979','2001','NP','JSW','NLMK Pennsylvania','Jindal','Foreign','X','2016-05-12 17:13:07',''),('609732979','2001','NP','MIT','NLMK Pennsylvania','Arcelor Mittal Quebec','Foreign','X','2016-05-12 17:13:07',''),('609732979','2001','NP','MTC','NLMK Pennsylvania','Arcelor Mittal - Cleveland ','USA','USA','2016-05-12 17:13:07',''),('609732979','2001','NP','MTH','NLMK Pennsylvania','Mittal - Indiana Harbor','USA','USA','2016-05-12 17:13:07',''),('609732979','2001','NP','NLK','NLMK Pennsylvania','NLMK Non-Domestic','Foreign','X','2016-05-12 17:13:07',''),('609732979','2001','NP','NLP','NLMK Pennsylvania','NLMK - Portage, IN','USA','USA','2016-05-12 17:13:07',''),('609732979','2001','NP','NUB','NLMK Pennsylvania','Nucor-Berkeley','USA','USA','2016-05-12 17:13:07',''),('609732979','2001','NP','NUC','NLMK Pennsylvania','Nucor Crawfordsville ','USA','USA','2016-05-12 17:13:07',''),('609732979','2001','NP','NUD','NLMK Pennsylvania','Nucor-Decatur','USA','USA','2016-05-12 17:13:07',''),('609732979','2001','NP','RG','NLMK Pennsylvania','RG Steel','USA','USA','2016-05-12 17:13:07',''),('609732979','2001','NP','SDO','NLMK Pennsylvania','Sidor ','Foreign','X','2016-05-12 17:13:07',''),('609732979','2001','NP','SEV','NLMK Pennsylvania','Severstal ','Foreign','X','2016-05-12 17:13:07',''),('609732979','2001','NP','SG','NLMK Pennsylvania','US Steel-Gary','USA','USA','2016-05-12 17:13:07',''),('609732979','2001','NP','SH','NLMK Pennsylvania','Stelco-Canada ','Foreign','X','2016-05-12 17:13:07',''),('609732979','2001','NP','SSP','NLMK Pennsylvania','Severstal - Sparrows Point ','USA','USA','2016-05-12 17:13:07',''),('609732979','2001','NP','SVR','NLMK Pennsylvania','Severstal Dearborn ','USA','USA','2016-05-12 17:13:07',''),('609732979','2001','NP','SVW','NLMK Pennsylvania','Severstal Warren ','USA','USA','2016-05-12 17:13:07',''),('609732979','2001','NP','TCP','NLMK Pennsylvania','Teeside','Foreign','X','2016-05-12 17:13:07',''),('609732979','2001','NP','TKB','NLMK Pennsylvania','NLMK - Portage, IN','Foreign','X','2016-05-12 17:13:07',''),('609732979','2001','NP','USC','NLMK Pennsylvania','US Steel-East Chicago','USA','USA','2016-05-12 17:13:07',''),('609732979','2001','NP','USE','NLMK Pennsylvania','US  Steel-Lake Erie - Canada ','Foreign','X','2016-05-12 17:13:07',''),('609732979','2001','NP','USF','NLMK Pennsylvania','US Steel - Fairfield ','USA','USA','2016-05-12 17:13:07',''),('609732979','2001','NP','USG','NLMK Pennsylvania','US Steel-Gary','USA','USA','2016-05-12 17:13:07',''),('609732979','2001','NP','USH','NLMK Pennsylvania','USS - Canada ','Foreign','X','2016-05-12 17:13:07',''),('609732979','2001','NP','USK','NLMK Pennsylvania','USS - Slovakia','Foreign','X','2016-05-12 17:13:07',''),('609732979','2001','NP','USL','NLMK Pennsylvania','USS - Great Lakes ','USA','USA','2016-05-12 17:13:07',''),('609732979','2001','NP','USS','NLMK Pennsylvania','USS-Mon Valley','USA','USA','2016-05-12 17:13:07',''),('609732979','2001','NP','UST','NLMK Pennsylvania','US Steel - Edgar Thompson ','USA','USA','2016-05-12 17:13:07',''),('609732979','2001','NP','USW','NLMK Pennsylvania','US Steel-Midwest','USA','USA','2016-05-12 17:13:07',''),('609732979','2001','NP','ZAP','NLMK Pennsylvania','Zapslb','Foreign','X','2016-05-12 17:13:07',''),('790297527OC','502','NH','*','Nucor - Hickman','Nucor - Hickman','USA','USA','2016-05-12 17:13:07',''),('884959263OC','568','NB','*','Nucor - Berkeley','Nucor - Berkeley','USA','USA','2016-05-12 17:13:07','');
/*!40000 ALTER TABLE `edioriginzone` ENABLE KEYS */;
UNLOCK TABLES;


------------------------------
13-05-2016

UPDATE `inbiz_edi`.`chem_mapping` SET `Active`=0 WHERE `EdiText`='SN';



17-05-2016

CREATE TABLE `edidatasource` (
  `EdiDataSourceId` int(11) NOT NULL AUTO_INCREMENT,
  `EdiDataSourceName` varchar(45) DEFAULT '',
  `EdiDataSourceType` int(11) DEFAULT '0',
  `Host` varchar(100) DEFAULT '',
  `UserId` varchar(100) DEFAULT '',
  `Password` varchar(100) DEFAULT '',
  `Port` varchar(45) DEFAULT '',
  `DataSourceDirectory` varchar(500) DEFAULT '',
  `Active` bit(1) DEFAULT b'1',
  `CreatedOn` datetime DEFAULT CURRENT_TIMESTAMP,
  `DataProcessedDirectory` varchar(500) DEFAULT '',
  `DataUnProcessedDirectory` varchar(500) DEFAULT '',
  `DataUploadDirectory` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`EdiDataSourceId`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;




ALTER TABLE `inbiz_edi`.`configuration` 
ADD COLUMN `EdiDataSourceId` VARCHAR(45) NULL DEFAULT 0 AFTER `FileProcessingType`;


-------------------------
25-05-2016

ALTER TABLE `inbiz_edi`.`files` 
CHANGE COLUMN `VendorID` `VendorID` INT(11) NULL DEFAULT 0 ,
CHANGE COLUMN `ReceiptNo` `ReceiptNo` INT(11) NULL DEFAULT 0 ,
CHANGE COLUMN `IntchgNo` `IntchgNo` INT(11) NULL DEFAULT 0 ,
ADD COLUMN `ProductQds` VARCHAR(45) NULL DEFAULT '' AFTER `QdsNo`;


---------

ALTER TABLE `inbiz_edi`.`files` 
ADD COLUMN `QdsReferenceNo` VARCHAR(45) NULL DEFAULT '' COMMENT 'QdsReferenceNo,  FileIdReferenceNo holds value of previous qds and file id from which current data matches.' AFTER `ProductQds`,
ADD COLUMN `FileIdReferenceNo` VARCHAR(45) NULL DEFAULT '' COMMENT 'QdsReferenceNo,  FileIdReferenceNo holds value of previous qds and file id from which current data matches.' AFTER `QdsReferenceNo`;


--
01-06-2016

ALTER TABLE `inbiz_edi`.`header863` 
ADD COLUMN `ShipingRef` VARCHAR(45) NULL DEFAULT '' AFTER `VendorTagId`;


13-06-2016

ALTER TABLE `inbiz_edi`.`arremittance` 
ADD COLUMN `ReferenceItemNumber` VARCHAR(45) NULL DEFAULT '' AFTER `RemittanceDate`;



----------------
15-06-2016

CREATE TABLE `inbiz_edi`.`filereceived` (
  `FileReceivedId` INT NOT NULL AUTO_INCREMENT,
  `FileName` VARCHAR(200) NOT NULL,
  `FileReceivedDateTime` DATETIME NULL DEFAULT now(),
  `ExecutionPeriod` INT NULL DEFAULT 0 COMMENT 'Store execution time after which file need to be read and processed for further execution.',
  `FileReceivedTime` VARCHAR(20) NULL DEFAULT '',
  `Active` BIT(1) NULL DEFAULT 1,
  PRIMARY KEY (`FileReceivedId`, `FileName`))
COMMENT = 'Store all file came from server.';



16-06

ALTER TABLE `inbiz_edi`.`filereceived` 
ADD COLUMN `FileReceivedDirectory` VARCHAR(200) NULL DEFAULT '' AFTER `FileReceivedTime`;


ALTER TABLE `inbiz_edi`.`filereceived` 
ADD COLUMN `Processed` TINYINT(1) NULL DEFAULT 0 AFTER `FileReceivedDirectory`,
ADD COLUMN `ProcessedOn` DATETIME NULL DEFAULT now() AFTER `Processed`;



ALTER TABLE `inbiz_edi`.`edidatasource` 
ADD COLUMN `DataDownloadDirectory` VARCHAR(500) NULL DEFAULT '' AFTER `DataUploadDirectory`;




----------------
20-06-2016

CREATE TABLE `inbiz_edi`.`filereceivedhistory` (
  `HistoryId` INT NOT NULL,
  `FileReceivedId` INT NULL DEFAULT 0,
  `UpdatedOn` DATETIME NULL DEFAULT now(),
  `Active` BIT(1) NULL DEFAULT 1,
  PRIMARY KEY (`HistoryId`))
COMMENT = 'Store history of File Received. store how many times file has received.';


ALTER TABLE `inbiz_edi`.`filereceivedhistory` 
CHANGE COLUMN `HistoryId` `HistoryId` INT(11) NOT NULL AUTO_INCREMENT ;



ALTER TABLE `inbiz_edi`.`configuration` 
ADD COLUMN `TimeDelayInHour` INT NULL DEFAULT 0 AFTER `EdiDataSourceId`;


ALTER TABLE `inbiz_edi`.`filereceived` 
ADD COLUMN `FileUpdatedOn` DATETIME NULL DEFAULT now() AFTER `Active`;


----------
21-06

ALTER TABLE `inbiz_edi`.`files` 
CHANGE COLUMN `FileReceivedId` `FileReceivedId` INT NULL DEFAULT '0' ;


ALTER TABLE `inbiz_edi`.`filereceived` 
ADD COLUMN `ProcessOn` DATETIME NULL DEFAULT now() AFTER `FileUpdatedOn`;


ALTER TABLE `inbiz_edi`.`filereceived` 
CHANGE COLUMN `ProcessedOn` `ProcessedOn` DATETIME NULL ;


--------
27-06

ALTER TABLE `inbiz_edi`.`skippedvendortagid` 
ADD COLUMN `Comment` VARCHAR(200) NULL DEFAULT '' AFTER `Weight`;

ALTER TABLE `inbiz_edi`.`configuration` 
ADD COLUMN `CheckQDS` BIT(1) NULL DEFAULT 0 AFTER `TimeDelayInHour`;



-----
30-06

ALTER TABLE `inbiz_edi`.`header863` 
ADD COLUMN `hdrHeatQds` INT(11) NULL DEFAULT 0 AFTER `ShipingRef`,
ADD COLUMN `hdrProductQds` INT(11) NULL DEFAULT 0 AFTER `hdrHeatQds`;


---
09-08

INSERT INTO `inbiz_edi`.`sysstatus` (`sysAppPfx`, `sysAppCode`, `sysAppCodeLang`, `sysAppLogicCode`, `sysAppDesc`, `sysAppCodeNote`, `sysAppCodeActive`, `sysAppCodeSeq`) VALUES ('PR', 'dl863', 'en', 'HEAT', 'HEAT', 'HEAT', '1', '4');
  INSERT INTO `inbiz_edi`.`sysstatus` (`sysAppPfx`, `sysAppCode`, `sysAppCodeLang`, `sysAppLogicCode`, `sysAppDesc`, `sysAppCodeNote`, `sysAppCodeActive`, `sysAppCodeSeq`) VALUES ('PR', 'dl863', 'fr', 'HEAT', 'HEAT', 'HEAT', '1', '4');



---
19-08

ALTER TABLE `inbiz_edi`.`header863` 
ADD COLUMN `QdsCreatedByStratix` TINYINT(1) NULL DEFAULT 0 AFTER `hdrProductQds`;



------
02-09



INSERT INTO `inbiz_edi`.`sysstatus` (`sysAppPfx`, `sysAppCode`, `sysAppCodeLang`, `sysAppLogicCode`, `sysAppDesc`, `sysAppCodeNote`, `sysAppCodeActive`, `sysAppCodeSeq`) VALUES ('PR', 'dl856', 'en', 'SHPNO', 'SHPNO', 'SHPNO', '1', '4');
INSERT INTO `inbiz_edi`.`sysstatus` (`sysAppPfx`, `sysAppCode`, `sysAppCodeLang`, `sysAppLogicCode`, `sysAppDesc`, `sysAppCodeNote`, `sysAppCodeActive`, `sysAppCodeSeq`) VALUES ('PR', 'dl856', 'fr', 'SHPNO', 'SHPNO', 'SHPNO', '1', '4');

--863


INSERT INTO `inbiz_edi`.`sysstatus` (`sysAppPfx`, `sysAppCode`, `sysAppCodeLang`, `sysAppLogicCode`, `sysAppDesc`, `sysAppCodeNote`, `sysAppCodeActive`, `sysAppCodeSeq`) VALUES ('PR', 'dl863', 'en', 'SHPNO', 'SHPNO', 'SHPNO', '1', '5');
INSERT INTO `inbiz_edi`.`sysstatus` (`sysAppPfx`, `sysAppCode`, `sysAppCodeLang`, `sysAppLogicCode`, `sysAppDesc`, `sysAppCodeNote`, `sysAppCodeActive`, `sysAppCodeSeq`) VALUES ('PR', 'dl863', 'fr', 'SHPNO', 'SHPNO', 'SHPNO', '1', '5');


--19-10

ALTER TABLE `inbiz_edi`.`edierror` 
ADD COLUMN `Header856ID` INT(11) NULL DEFAULT 0 AFTER `Comment`,
ADD COLUMN `Detail856ID` INT(11) NULL DEFAULT 0 AFTER `Header856ID`,
ADD COLUMN `Header863ID` INT(11) NULL DEFAULT 0 AFTER `Detail856ID`,
ADD COLUMN `Detail863ID` INT(11) NULL DEFAULT 0 AFTER `Header863ID`;


-- 21-10

ALTER TABLE `inbiz_edi`.`edierror` 
ADD COLUMN `EdiLogType` INT(1) NULL DEFAULT 0 AFTER `Detail863ID`;


-- 08-11

ALTER TABLE `inbiz_edi`.`header863` 
ADD COLUMN `QdsCreatedByEdi` TINYINT(1) NULL DEFAULT 0 AFTER `QdsCreatedByStratix`;


-- 17-11

ALTER TABLE `inbiz_edi`.`configuration` 
ADD COLUMN `Activate997` BIT(1) NULL DEFAULT 0 AFTER `Activate863`;


ALTER TABLE `inbiz_edi`.`edidatasource` 
ADD COLUMN `Ack997SftpDirectory` VARCHAR(500) NULL DEFAULT '' AFTER `DataDownloadDirectory`,
ADD COLUMN `Ack997LocalDirectory` VARCHAR(500) NULL DEFAULT '' AFTER `Ack997SftpDirectory`;



-- 12-12-16

ALTER TABLE `inbiz_edi`.`configuration` 
ADD COLUMN `ActivateMultiplePoItem` BIT(1) NULL DEFAULT 0 COMMENT 'if this is enable
 , then we will get item no from lin and will get form grd size based on po and item.' AFTER `Activate997`;



--
17-02-2017

ALTER TABLE `inbiz_edi`.`configuration` 
ADD COLUMN `VenshpRefInitials` VARCHAR(10) NULL DEFAULT '' AFTER `ActivateMultiplePoItem`;


----

28-02-2017



ALTER TABLE `inbiz_edi`.`tests` 
ADD COLUMN `TestSequence` INT(11) NULL DEFAULT 0 AFTER `UOM`;



16-03-2017

ALTER TABLE `files` 
ADD COLUMN `FileText` TEXT NULL AFTER `FileReceivedId`,
ADD COLUMN `FileSplittedText` TEXT NULL AFTER `FileText`;

20-03-2017

ALTER TABLE `inbiz_edi`.`configuration` 
ADD COLUMN `ApproveQds` INT NULL DEFAULT 0 AFTER `VenshpRefInitials`;


31-03-2017

ALTER TABLE `inbiz_edi`.`detail863` 
ADD COLUMN `mss_sdo` VARCHAR(45) NULL COMMENT 'realted to metal standard' AFTER `ChemistrySequence`,
ADD COLUMN `mss_std_id` VARCHAR(45) NULL COMMENT 'realted to metal standard' AFTER `mss_sdo`,
ADD COLUMN `mss_addnl_id` VARCHAR(45) NULL DEFAULT '' COMMENT 'realted to metal standard' AFTER `mss_std_id`;


22-05-2017

ALTER TABLE `inbiz_edi`.`configuration` 
ADD COLUMN `PrependDunsInAckFileName` BIT(1) NULL DEFAULT 0 AFTER `ApproveQds`;

ALTER TABLE `inbiz_edi`.`configuration` 
ADD COLUMN `SingleFileFor856863` BIT(1) NULL DEFAULT 0 AFTER `PrependDunsInAckFileName`;


30-05-2017
ALTER TABLE `inbiz_edi`.`configuration` 
ADD COLUMN `Exreact856863FromSingleFile` INT NULL DEFAULT 0 AFTER `SingleFileFor856863`;


31-05-2017

ALTER TABLE `inbiz_edi`.`configuration` 
ADD COLUMN `CustomTestForRange` INT(1) NULL DEFAULT 0 AFTER `Exreact856863FromSingleFile`;

ALTER TABLE `inbiz_edi`.`configuration` 
ADD COLUMN `ActivateLoopOnPiece` INT(1) NULL DEFAULT 0 AFTER `CustomTestForRange`;


08-06-2017

CREATE TABLE `elementconfig` (
  `DunsNumber` varchar(50) NOT NULL DEFAULT '',
  `FileType` varchar(10) NOT NULL DEFAULT '',
  `Key` varchar(10) NOT NULL DEFAULT '',
  `ValueCombination` varchar(500) DEFAULT '',
  `CreatedOn` datetime DEFAULT CURRENT_TIMESTAMP,
  `UpdatedOn` datetime DEFAULT CURRENT_TIMESTAMP,
  `Active` int(11) DEFAULT '1',
  PRIMARY KEY (`DunsNumber`,`FileType`,`Key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Holds element information for individual duns no. ';

-----------------

15-06-2017

CREATE TABLE `inbiz_edi`.`edi990transaction` (
  `TransId` INT NOT NULL,
  `TransactionIdentifierCode` VARCHAR(45) NULL DEFAULT '',
  `TransactionControlNo` VARCHAR(45) NULL DEFAULT '',
  `CarrierAlphaCode` VARCHAR(45) NULL DEFAULT '',
  `ShipmentIdentificationNumber` VARCHAR(45) NULL DEFAULT '',
  `CarrierDate` VARCHAR(45) NULL DEFAULT '',
  `ReservationActionCode` VARCHAR(45) NULL DEFAULT '',
  `ReferenceIdentificationQualifier` VARCHAR(45) NULL DEFAULT '' COMMENT 'N901 is used.',
  `ReferenceIdentification` VARCHAR(45) NULL DEFAULT '' COMMENT 'Reference Identification: � �Carrier Order Number�  , N902',
  `CreatedOn` DATETIME NULL DEFAULT now(),
  `Active` INT NULL DEFAULT 1,
  PRIMARY KEY (`TransId`));




------------

ALTER TABLE `inbiz_edi`.`configuration` 
ADD COLUMN `SmtpHost` VARCHAR(45) NULL DEFAULT '' AFTER `ActivateLoopOnPiece`,
ADD COLUMN `SmtpUid` VARCHAR(45) NULL DEFAULT '' AFTER `SmtpHost`,
ADD COLUMN `SmtpPass` VARCHAR(45) NULL DEFAULT '' AFTER `SmtpUid`,
ADD COLUMN `SmtpPort` VARCHAR(45) NULL DEFAULT '' AFTER `SmtpPass`,
ADD COLUMN `FromEmailAddress` VARCHAR(45) NULL DEFAULT '' AFTER `SmtpPort`,
ADD COLUMN `ToEmailAddress` VARCHAR(45) NULL DEFAULT '' AFTER `FromEmailAddress`,
ADD COLUMN `BodyMessage` VARCHAR(1000) NULL DEFAULT '' AFTER `ToEmailAddress`,
ADD COLUMN `EmailSubject` VARCHAR(100) NULL DEFAULT '' AFTER `BodyMessage`;


--------


21-06


--------------------------------

CREATE TABLE `edi214shipmentdetail` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `FileID` int(11) DEFAULT '0',
  `TransactionSetIdentifierCode` varchar(45) DEFAULT '',
  `TransactionSetControlNumber` varchar(45) DEFAULT '' COMMENT 'ST02',
  `CarrierOrderNumber` varchar(45) DEFAULT '' COMMENT 'B1001',
  `ShipmentId` varchar(45) DEFAULT '' COMMENT 'B1002',
  `CarrierAlphaCode` varchar(45) DEFAULT '' COMMENT 'B1003 , Carrier SCAC Code',
  `OrderlevelBillofLadingBM` varchar(45) DEFAULT '' COMMENT 'L1101 , L1102 is BM.. Reference qualifier for this column is BM',
  `OrderlevelConsigneeReferenceNumberCR` varchar(45) DEFAULT '' COMMENT 'L1101 , L1102 is CR.. Reference qualifier for this column is CR',
  `EntityIdentifierCode` varchar(45) DEFAULT '' COMMENT 'N101',
  `EntityName` varchar(45) DEFAULT '' COMMENT 'N102',
  `IdentificationCodeQualifier` varchar(45) DEFAULT '' COMMENT 'N103.. �94� Only if N104 is present',
  `IdentificationCode` varchar(45) DEFAULT '' COMMENT 'N104',
  `Address` varchar(100) DEFAULT '' COMMENT 'N301',
  `City` varchar(45) DEFAULT '' COMMENT 'N401',
  `State` varchar(45) DEFAULT '' COMMENT 'N402',
  `ZipCode` varchar(45) DEFAULT '' COMMENT 'N403',
  `CountryCode` varchar(45) DEFAULT '',
  `CreatedOn` varchar(45) DEFAULT 'Now()',
  `Active` int(11) DEFAULT '1',
  `MailSent` int(11) DEFAULT '0' COMMENT 'hold status ',
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='holds shipmnet details..';



----------------------

CREATE TABLE `edi214shipmentdstops` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `shipId` int(11) DEFAULT '0' COMMENT 'this is reference id of table edi214shipmentdetail.',
  `StopNumber` varchar(45) DEFAULT '' COMMENT 'LX01',
  `ShipmentStatusCodes` varchar(45) DEFAULT '' COMMENT 'AT701',
  `ShipmentStatusAppointmentReasonCodes` varchar(45) DEFAULT '' COMMENT 'AT702',
  `ShipmentAppointmentStatusCode` varchar(45) DEFAULT '' COMMENT 'AT703',
  `Date` varchar(45) DEFAULT '' COMMENT 'AT705 , YYMMDD',
  `Time` varchar(45) DEFAULT '' COMMENT 'AT706, HHMM',
  `TimeCode` varchar(45) DEFAULT '' COMMENT 'AT707 , LT is time code',
  `StopLevelReferenceNumber` varchar(45) DEFAULT '' COMMENT 'L1101',
  `StopLevelQualifier` varchar(45) DEFAULT '' COMMENT 'L1102',
  `StopLevelComment` varchar(45) DEFAULT '' COMMENT 'K101',
  `WeightQualifier` varchar(45) DEFAULT 'G' COMMENT 'AT801',
  `WeightUnitCode` varchar(45) DEFAULT 'L' COMMENT 'AT802',
  `WeightStopLevel` varchar(45) DEFAULT '' COMMENT 'AT803',
  `VolumeUnitQualifier` varchar(45) DEFAULT 'E' COMMENT 'AT806',
  `VolumeStopLevel` varchar(45) DEFAULT '' COMMENT 'AT807',
  `LadingQuantityStopLevel` varchar(45) DEFAULT '' COMMENT 'AT804',
  `StandardCarrierAlphaCodeTR` varchar(45) DEFAULT '' COMMENT 'MS201',
  `EquipmentNumberTR` varchar(45) DEFAULT '' COMMENT 'MS202',
  `EquipmentDescriptionCodeTR` varchar(45) DEFAULT '' COMMENT 'MS203',
  `StandardCarrierAlphaCodeTL` varchar(45) DEFAULT '' COMMENT 'MS201',
  `EquipmentNumberTL` varchar(45) DEFAULT '' COMMENT 'MS202',
  `EquipmentDescriptionCodeTL` varchar(45) DEFAULT '' COMMENT 'MS203',
  `EntityIdentifierCode` varchar(45) DEFAULT '' COMMENT 'N101',
  `EntityName` varchar(45) DEFAULT '' COMMENT 'N102',
  `IdentificationCodeQualifier` varchar(45) DEFAULT '' COMMENT 'N103',
  `IdentifficationCode` varchar(45) DEFAULT '' COMMENT 'N104',
  `Address` varchar(100) DEFAULT '' COMMENT 'N301',
  `City` varchar(45) DEFAULT '',
  `State` varchar(45) DEFAULT '' COMMENT 'N402',
  `ZipCode` varchar(45) DEFAULT '' COMMENT 'N403',
  `CountryCode` varchar(45) DEFAULT '' COMMENT 'N404',
  `PurchaseOrderNumber` varchar(45) DEFAULT '' COMMENT 'SPO01',
  `MeasurementCode` varchar(45) DEFAULT '' COMMENT 'SPO03',
  `Quantity` varchar(45) DEFAULT '' COMMENT 'SPO04',
  `SPOWeightUnitCode` varchar(45) DEFAULT '' COMMENT 'SPO05',
  `Weight` varchar(45) DEFAULT '' COMMENT 'SPO06',
  `CreatedOn` datetime DEFAULT CURRENT_TIMESTAMP,
  `Active` int(11) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;


CREATE TABLE `edi990transaction` (
  `TransId` int(11) NOT NULL AUTO_INCREMENT,
  `FileId` int(11) DEFAULT '0',
  `TransactionIdentifierCode` varchar(45) DEFAULT '',
  `TransactionControlNo` varchar(45) DEFAULT '',
  `CarrierAlphaCode` varchar(45) DEFAULT '',
  `ShipmentIdentificationNumber` varchar(45) DEFAULT '',
  `CarrierDate` varchar(45) DEFAULT '',
  `ReservationActionCode` varchar(45) DEFAULT '',
  `ReferenceIdentificationQualifier` varchar(45) DEFAULT '' COMMENT 'N901 is used.',
  `ReferenceIdentification` varchar(45) DEFAULT '' COMMENT 'Reference Identification: � �Carrier Order Number�  , N902',
  `CreatedOn` datetime DEFAULT CURRENT_TIMESTAMP,
  `Active` int(11) DEFAULT '1',
  `MailSent` int(11) DEFAULT '0',
  PRIMARY KEY (`TransId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



07-07-2017


ALTER TABLE `inbiz_edi`.`configuration` 
ADD COLUMN `TimeDelayInMinute` INT NULL DEFAULT 0 AFTER `TimeDelayInHour`;


12-07-2017

ALTER TABLE `inbiz_edi`.`filereceived` 
ADD COLUMN `ShipperNo` VARCHAR(45) NULL DEFAULT '' AFTER `FileName`;


ALTER TABLE `inbiz_edi`.`filereceived` 
ADD COLUMN `Comment` VARCHAR(200) NULL DEFAULT '' AFTER `ProcessOn`;

ALTER TABLE `inbiz_edi`.`filereceived` 
ADD COLUMN `FileType` VARCHAR(45) NULL DEFAULT '' AFTER `ShipperNo`;


ALTER TABLE `inbiz_edi`.`detailitem` 
ADD COLUMN `Active` BIT(1) NOT NULL DEFAULT 1 AFTER `IsReceiptUpdated`;


----
16-01-2018

ALTER TABLE `inbiz_edi_live`.`tests` 
CHANGE COLUMN `TestId` `TestId` VARCHAR(10) NOT NULL ;
