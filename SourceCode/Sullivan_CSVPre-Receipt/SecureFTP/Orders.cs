﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SecureFTP
{
    class Orders
    {

        public Orders()
        {
        }

        int iOrderSequence = 0;
        public int OrderSequence
        {
            get { return iOrderSequence; }
            set { iOrderSequence = value; }
        }

        string sVenOrderId = string.Empty;
        public string VenOrderId
        {
            get { return sVenOrderId; }
            set { sVenOrderId = value; }
        }

        string sPurchaseOrderId = string.Empty;
        public string PurchaseOrderId
        {
            get { return sPurchaseOrderId; }
            set { sPurchaseOrderId = value; }
        }

        string sPONumber = string.Empty;
        public string PONumber
        {
            get { return sPONumber; }
            set { sPONumber = value; }
        }

        string sHeatNumber = string.Empty;
        public string HeatNumber
        {
            get { return sHeatNumber; }
            set { sHeatNumber = value; }
        }


        



    }
}
