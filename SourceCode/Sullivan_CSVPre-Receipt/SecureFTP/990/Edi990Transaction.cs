﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Text;

namespace SecureFTP._990
{
    class Edi990Transaction
    {

        StringBuilder m_sbSql = null;
        string m_sSql = string.Empty;
        static string sMysqlConnectionString = ConfigurationManager.ConnectionStrings["NewConnectionString"].ToString();


        string sTransactionIdentifierCode = string.Empty;
        public string TransactionIdentifierCode
        {
            get { return sTransactionIdentifierCode; }
            set { sTransactionIdentifierCode = value; }
        }

        string sTransactionControlNo = string.Empty;
        public string TransactionControlNo
        {
            get { return sTransactionControlNo; }
            set { sTransactionControlNo = value; }
        }

        string sCarrierAlphaCode = string.Empty;
        public string CarrierAlphaCode
        {
            get { return sCarrierAlphaCode; }
            set { sCarrierAlphaCode = value; }
        }

        string sShipmentIdentificationNumber = string.Empty;
        public string ShipmentIdentificationNumber
        {
            get { return sShipmentIdentificationNumber; }
            set { sShipmentIdentificationNumber = value; }
        }

        string sCarrierDate = string.Empty;
        public string CarrierDate
        {
            get { return sCarrierDate; }
            set { sCarrierDate = value; }
        }

        string sReservationActionCode = string.Empty;
        public string ReservationActionCode
        {
            get { return sReservationActionCode; }
            set { sReservationActionCode = value; }
        }

        string sReferenceIdentificationQualifier = string.Empty;
        public string ReferenceIdentificationQualifier
        {
            get { return sReferenceIdentificationQualifier; }
            set { sReferenceIdentificationQualifier = value; }
        }

        string sReferenceIdentification = string.Empty;
        public string ReferenceIdentification
        {
            get { return sReferenceIdentification; }
            set { sReferenceIdentification = value; }
        }


        
        public int Insert990Transaction(int iFileId, Edi990Transaction oEdi990Transaction)
        {
            int iStatus = 0;
            m_sbSql = new StringBuilder();
            iTECH.Library.DataAccess.MySql.DbHelper oDbHelper = new iTECH.Library.DataAccess.MySql.DbHelper(sMysqlConnectionString, true);
            try
            {
                m_sbSql.Append(" INSERT INTO  edi990transaction ( FileId , TransactionControlNo, CarrierAlphaCode, ShipmentIdentificationNumber, CarrierDate, ReservationActionCode, ReferenceIdentificationQualifier, ReferenceIdentification) ");
                m_sbSql.Append(" VALUES ("+iFileId+" , '"+ oEdi990Transaction.TransactionControlNo + "', '"+ oEdi990Transaction.CarrierAlphaCode + "', '" + oEdi990Transaction.ShipmentIdentificationNumber + "', '" + oEdi990Transaction.CarrierDate + "', '" + oEdi990Transaction.ReservationActionCode + "', '" + oEdi990Transaction.ReferenceIdentificationQualifier + "', '" + oEdi990Transaction.ReferenceIdentification + "')");
                MySqlParameter[] oMySqlParameter = { };

                iStatus = oDbHelper.ExecuteNonQuery(m_sbSql.ToString(), CommandType.Text, oMySqlParameter);
                iStatus = oDbHelper.GetLastInsertID();

            }
            catch (Exception ex)
            {
                ErrorLog.CreateLog(ex);
            }
            finally
            {
                oDbHelper.CloseDatabaseConnection();
            }
            return iStatus;
        }


        public int UpdateStaus(int id)
        {
            int iStatus = 0;
            m_sbSql = new StringBuilder();
            iTECH.Library.DataAccess.MySql.DbHelper oDbHelper = new iTECH.Library.DataAccess.MySql.DbHelper(sMysqlConnectionString, true);
            try
            {
                m_sbSql.Append(" UPDATE edi990transaction SET MailSent = 1 WHERE TransId = " + id);

                MySqlParameter[] oMySqlParameter = { };
                iStatus = oDbHelper.ExecuteNonQuery(m_sbSql.ToString(), CommandType.Text, oMySqlParameter);
                iStatus = oDbHelper.GetLastInsertID();
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLog(ex);
            }
            finally
            {
                oDbHelper.CloseDatabaseConnection();
            }
            return iStatus;
        }

    }
}
