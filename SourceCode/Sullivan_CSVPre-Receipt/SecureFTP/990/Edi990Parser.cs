﻿using iTECH.Library.Utilities;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;

namespace SecureFTP._990
{
    public class Edi990Parser
    {

        static string sStratixConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ToString();
        string[] m_sArr = null;
        Dictionary<int, ArrayList> m_dict = new Dictionary<int, ArrayList>();
        Dictionary<int, ArrayList> m_dictItem = new Dictionary<int, ArrayList>();
        ArrayList m_arl = new ArrayList();
        Configuration oConfiguration = null;

        static string m_sStartBlock = "ST";
        static string m_sEndBlock = "SE";
        static string m_sInterchangeControlTrailer = "IEA";
        int m_iFileId = 0;
        int m_iFileType = 0;
        Files oFiles;

        public Edi990Parser(string sFilePath, int iFileID)
        {
            m_iFileId = iFileID;
            //m_sArr = m_sArr.Where(x => !string.IsNullOrEmpty(x)).ToArray(); // removing empty segment 
            try
            {

                var fileLines = System.IO.File.ReadAllLines(BusinessUtility.GetString(sFilePath));

                string sFileNameWithoutExtension = System.IO.Path.GetFileNameWithoutExtension(sFilePath);

                oConfiguration = new Configuration();
                oConfiguration.PopulateConfigurationData(fileLines);
                // EdiFile = oConfiguration.FileType;

                if (fileLines.Length == 1)
                {
                    var varLinesNew = fileLines[0].Split(new string[] { oConfiguration.LineSeperator }, StringSplitOptions.None);
                    fileLines = varLinesNew;
                    Utility.AppendLineSeperator(ref fileLines, oConfiguration.LineSeperator);
                   
                }
                m_sArr = fileLines;
                m_sArr = m_sArr.Where(x => !string.IsNullOrEmpty(x)).ToArray();

                this.StartProcessing();
                this.ExtractInfoToMysql();
            }
            catch (Exception ex)
            {
                ErrorLog.createLog("Error occur in Edi863Parser constructor : " + ex.ToString());
               // EdiError.LogError(BusinessUtility.GetString(m_iFileType), m_iFileId.ToString(), "", ex, "");
            }
        }


        public void ExtractInfoToMysql()
        {

            try
            {
                for (int i = 0; i < m_dict.Count; i++)
                {
                    if (i > 0 && i < m_dict.Count - 1)
                    {
                        m_dictItem = new Dictionary<int, ArrayList>();

                        ArrayList olst = m_dict[i];
                        List<string> lstContent = new List<string>();
                        Utility.AddArrayListToList(ref olst, ref lstContent);
                        StringBuilder sbMessage = new StringBuilder();

                        Edi990Transaction oEdi990Transaction = new Edi990Transaction();
                        oEdi990Transaction.CarrierAlphaCode = Utility.GetElementValueBasedOnElementConfig(elementconfig.Elements.CARRIERALPHACODE, lstContent);
                        oEdi990Transaction.CarrierDate = Utility.GetElementValueBasedOnElementConfig(elementconfig.Elements.CARRIERDATE, lstContent);
                        oEdi990Transaction.ReferenceIdentification = Utility.GetElementValueBasedOnElementConfig(elementconfig.Elements.REFERENCEIDENTIFICATION, lstContent);
                        oEdi990Transaction.ReferenceIdentificationQualifier = Utility.GetElementValueBasedOnElementConfig(elementconfig.Elements.REFERENCEIDENTIFICATIONQUALIFIER, lstContent);
                        oEdi990Transaction.ReservationActionCode = Utility.GetElementValueBasedOnElementConfig(elementconfig.Elements.RESERVATIONACTIONCODE, lstContent);
                        oEdi990Transaction.ShipmentIdentificationNumber = Utility.GetElementValueBasedOnElementConfig(elementconfig.Elements.SHIPMENTIDENTIFICATIONNUMBER, lstContent);

                        sbMessage.Append("CarrierAlphaCode = " + oEdi990Transaction.CarrierAlphaCode);
                        sbMessage.AppendLine();
                        sbMessage.Append("CarrierDate = " + oEdi990Transaction.CarrierDate);
                        sbMessage.AppendLine();
                        sbMessage.Append("ReferenceIdentification = " + oEdi990Transaction.ReferenceIdentification);
                        sbMessage.AppendLine();
                        sbMessage.Append("ReferenceIdentificationQualifier = " + oEdi990Transaction.ReferenceIdentificationQualifier);
                        sbMessage.AppendLine();
                        sbMessage.Append("ReservationActionCode = " + oEdi990Transaction.ReservationActionCode);
                        sbMessage.AppendLine();
                        sbMessage.Append("ShipmentIdentificationNumber = " + oEdi990Transaction.ShipmentIdentificationNumber);
                        sbMessage.AppendLine();

                        int iStatus = oEdi990Transaction.Insert990Transaction(m_iFileId, oEdi990Transaction);
                        if(iStatus > 0)
                        {
                            //oConfiguration

                            EmailManager oEmailManager = new EmailManager();

                            EmailSettings.SMTPServer = oConfiguration.SmtpHost;
                            EmailSettings.SMTPPort = BusinessUtility.GetInt(oConfiguration.SmtpPort);
                            EmailSettings.SMTPUserEmail = oConfiguration.SmtpUid;
                            EmailSettings.SMTPPassword = oConfiguration.SmtpPass;
                            EmailSettings.IsAsynchronousMail = true;
                            EmailSettings.UseDefaultCredentials = true;
                            oConfiguration.BodyMessage = oConfiguration.BodyMessage + sbMessage.ToString();
                            if ( EmailHelper.SendEmail(oConfiguration.FromEmailAddress, oConfiguration.ToEmailAddress, oConfiguration.BodyMessage, oConfiguration.EmailSubject, false))
                            {
                                oEdi990Transaction.UpdateStaus(iStatus);
                            }
                        }


                    }


                }
            }
            catch (Exception ex)
            {
                EdiError.LogError(m_iFileType.ToString(), m_iFileId.ToString(), "", ex, "Error in StartProcessing");
            }

        }


        //Seperate file st se
        public void StartProcessing()
        {
            bool bStartBlockFound = false;
            bool bEndBlockFound = false;
            bool bInterchangeControlTrailer = false;
            string sSegment = string.Empty;
            int iNoOfblock = 0;
            bool bInEndBlock = false;
            try
            {
                for (int i = 0; i < m_sArr.Length; i++)
                {
                    sSegment = BusinessUtility.GetString(m_sArr[i]);
                    bStartBlockFound = sSegment.Substring(0, 2).Equals(m_sStartBlock);
                    bEndBlockFound = sSegment.Substring(0, 2).Equals(m_sEndBlock);
                    bInterchangeControlTrailer = sSegment.Substring(0, 3).Equals(m_sInterchangeControlTrailer);

                    if (!bStartBlockFound)
                    {
                        m_arl.Add(sSegment);
                    }
                    if (bInEndBlock)
                    {
                        m_arl.Add(sSegment);
                        bInEndBlock = false;
                    }
                    if (bStartBlockFound && iNoOfblock == 0)
                    {

                        if (!m_dict.ContainsKey(iNoOfblock))
                        {
                            m_dict.Add(iNoOfblock, m_arl);
                        }
                        else
                        {
                            m_dict.Add(iNoOfblock + 1, m_arl);
                        }
                        iNoOfblock++;
                        m_arl = new ArrayList();
                        m_arl.Add(sSegment);
                    }
                    else if (bEndBlockFound)
                    {
                        bInEndBlock = true;
                        if (!m_dict.ContainsKey(iNoOfblock))
                        {
                            m_dict.Add(iNoOfblock, m_arl);
                        }
                        else
                        {
                            m_dict.Add(iNoOfblock + 1, m_arl);
                        }
                        iNoOfblock++;
                        m_arl = new ArrayList();
                        //m_arl.Add(sSegment);
                    }
                    else if (bInterchangeControlTrailer)
                    {
                        if (!m_dict.ContainsKey(iNoOfblock))
                        {
                            m_dict.Add(iNoOfblock, m_arl);
                        }
                        else
                        {
                            m_dict.Add(iNoOfblock + 1, m_arl);
                        }
                        iNoOfblock++;
                        bInEndBlock = false;
                        m_arl = new ArrayList();
                    }


                }
            }
            catch (Exception ex)
            {
                EdiError.LogError(m_iFileType.ToString(), m_iFileId.ToString(), "", ex, "Error in StartProcessing");
            }

        } // start processing end here..




    }
}
