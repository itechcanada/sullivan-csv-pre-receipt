﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using iTECH.Library.DataAccess.MySql;
using MySql.Data.MySqlClient;
using System.Data;
using iTECH.Library.Utilities;

namespace SecureFTP.ExternalCertificate
{
    public class QdsExternalCertificateData
    {
        static string sMysqlConnectionString = ConfigurationManager.ConnectionStrings["NewConnectionString"].ToString();
        StringBuilder sbSql;

        #region to check vendors -- to process only selected vendorlist without checking active flag   2020/04/14    Sumit
        string IsVendorFilter = Utility.GetConfigValue("IsVendorFilter");
        string FilteredVendorConfig = Utility.GetConfigValue("VendorDunsList");
        string[] VendorArr;
        string FilteredVendorDB = string.Empty;
        #endregion to check vendors -- to process only selected vendorlist   2020/04/14 Sumit

        public Int32 QdsExCert_Id { get; set; }
        public Int32 QdsExCert_FileReceiveId { get; set; }
        public Int32 QdsExCert_QdsExCertFileId { get; set; }
        public string QdsExCert_SenderId { get; set; }
        public string QdsExCert_Heat { get; set; }
        public string QdsExCert_PoBranch { get; set; }
        public string QdsExCert_PoNumber { get; set; }
        public string QdsExCert_VendorTagId { get; set; }
        public string QdsExCert_Filename { get; set; }
        public string QdsExCert_HeatQdsno { get; set; }
        public string QdsExCert_PrdQdsno { get; set; }
        public int QdsExCert_Processed { get; set; }
        public Nullable<DateTime> QdsExCert_CreatedOn { get; set; }
        public Nullable<DateTime> QdsExCert_ProcessedOn { get; set; }

        public string QdsExCert_FilePath { get; set; }
        

        /// <summary>
        /// To insert External Certificate record
        /// </summary>
        /// <param name="qdsExternalCertificate"></param>
        /// <returns></returns>
        public int InsertExternalCertificateRecord(QdsExternalCertificateData qdsExternalCertificate)
        {
            int iStatus = 0;
            sbSql = new StringBuilder();
            DbHelper oDbHelper = new DbHelper(sMysqlConnectionString, true);
            try
            {
                sbSql.Append(" INSERT INTO qdsexcert_data ( qecd_filereceive_id,qecd_qecf_id, qecd_sender_id,qecd_heat,qecd_pobranch, qecd_po, qecd_vendor_tag, qecd_cert_file ) ");
                sbSql.Append(" VALUES (@FileReceivedId, @qecd_qecf_id, @SenderId, @Heat, @PoBranch, @PoNumber, @VendorTagId, @FileName ) ");

                MySqlParameter[] oMySqlParameter = {
                                         DbUtility.GetParameter("@FileReceivedId", qdsExternalCertificate.QdsExCert_FileReceiveId, MyDbType.Int),
                                         DbUtility.GetParameter("@qecd_qecf_id", qdsExternalCertificate.QdsExCert_QdsExCertFileId, MyDbType.Int),
                                         DbUtility.GetParameter("@SenderId", qdsExternalCertificate.QdsExCert_SenderId, MyDbType.String),
                                         DbUtility.GetParameter("@Heat", qdsExternalCertificate.QdsExCert_Heat, MyDbType.String),
                                         DbUtility.GetParameter("@PoBranch", qdsExternalCertificate.QdsExCert_PoBranch, MyDbType.String),
                                         DbUtility.GetParameter("@PoNumber", qdsExternalCertificate.QdsExCert_PoNumber, MyDbType.String),
                                          DbUtility.GetParameter("@VendorTagId", qdsExternalCertificate.QdsExCert_VendorTagId , MyDbType.String),
                                          DbUtility.GetParameter("@FileName",qdsExternalCertificate.QdsExCert_Filename , MyDbType.String)
                                     };
                
                ErrorLog.createLog(sbSql.ToString());
                ErrorLog.createLog("InsertExternalCertificateRecord Parameters:-");
                ErrorLog.createLog("@FileReceivedId: " + qdsExternalCertificate.QdsExCert_FileReceiveId);
                ErrorLog.createLog("@qecd_qecf_id: " + qdsExternalCertificate.QdsExCert_QdsExCertFileId);
                ErrorLog.createLog("@SenderId: " + qdsExternalCertificate.QdsExCert_SenderId);
                ErrorLog.createLog("@Heat: " + qdsExternalCertificate.QdsExCert_Heat);
                ErrorLog.createLog("@PoBranch: " + qdsExternalCertificate.QdsExCert_PoBranch);
                ErrorLog.createLog("@PoNumber: " + qdsExternalCertificate.QdsExCert_PoNumber);
                ErrorLog.createLog("@VendorTagId: " + qdsExternalCertificate.QdsExCert_VendorTagId);
                ErrorLog.createLog("@FileName: " + qdsExternalCertificate.QdsExCert_Filename);
                iStatus = oDbHelper.ExecuteNonQuery(sbSql.ToString(), CommandType.Text, oMySqlParameter);
                iStatus = oDbHelper.GetLastInsertID();
            }
            catch (Exception ex)
            {
                ErrorLog.createLog("Error in Inserting Qds External Certificate Data detail." + ex.Message.ToString());
                ErrorLog.CreateLog(ex);
            }
            finally
            {
                oDbHelper.CloseDatabaseConnection();
            }
            return iStatus;
        }

        /// <summary>
        /// to select all pending certificate for process
        /// </summary>
        /// <returns></returns>
        public DataTable GetAllExternalCertificateRecord()
        {
            DataTable oDataTable = null;
            string sbSql = string.Empty;

            //sbSql= "SELECT distinct qecd.qecd_id FROM qdsexcert_data qecd inner join qdsexcert_file qecf on qecd.qecd_qecf_id=qecf.qecf_id " +
            //    "and qecd.qecd_cert_file = qecf.qecf_filename where qecd.qecd_processed = 0";
            sbSql = "SELECT distinct qecd.qecd_id FROM qdsexcert_data qecd inner join qdsexcert_file qecf on " +
                " qecd.qecd_cert_file = qecf.qecf_filename where qecd.qecd_processed = 0";

            #region to process only selected vendorlist without checking active flag    2020/04/14  Sumit
            if (!string.IsNullOrEmpty(IsVendorFilter) && BusinessUtility.GetInt(IsVendorFilter) == 1)
            {
                if (!string.IsNullOrEmpty(FilteredVendorConfig))
                {
                    VendorArr = FilteredVendorConfig.Split(',');
                    FilteredVendorDB = "";
                    for (int i = 0; i < VendorArr.Length; i++)
                    {
                        if (i == 0)
                            FilteredVendorDB = "'" + Convert.ToString(VendorArr[i]) + "'";
                        else
                            FilteredVendorDB = FilteredVendorDB + "," + "'" + Convert.ToString(VendorArr[i]) + "'";
                    }
                    sbSql = "SELECT distinct qecd.qecd_id FROM qdsexcert_data qecd inner join qdsexcert_file qecf on " +
                            " qecd.qecd_cert_file = qecf.qecf_filename WHERE qecd.qecd_sender_id in (" + FilteredVendorDB + ") and qecd.qecd_processed = 0";
                }

            }
            #endregion to process only selected vendorlist without checking active flag    2020/04/14  Sumit 

            DbHelper oDbHelper = new DbHelper(sMysqlConnectionString, true);

            try
            {
                ErrorLog.createLog(sbSql.ToString());
                oDataTable = oDbHelper.GetDataTable(sbSql.ToString(), CommandType.Text, null);
            }
            catch (Exception ex)
            {
                ErrorLog.createLog("Error in getting all Qds External Certificate Data detail.");
                ErrorLog.createLog(ex.Message.ToString());
                ErrorLog.CreateLog(ex);
            }
            finally
            {
                oDbHelper.CloseDatabaseConnection();
            }
            return oDataTable;
        }

        /// <summary>
        /// to select pending external certificates on the basis of file id
        /// </summary>
        /// <param name="qecfID"></param>
        /// <returns></returns>
        public DataTable GetAllExternalCertificateRecord(Int32 qecfID)
        {
            DataTable oDataTable = null;
            string sbSql = string.Empty;

            sbSql = "SELECT distinct qecd.qecd_id FROM qdsexcert_data qecd inner join qdsexcert_file qecf on qecd.qecd_cert_file=qecf.qecf_filename " +
                "where qecd.qecd_qecf_id=" + qecfID + " and qecd.qecd_processed = 0";

            #region to process only selected vendorlist without checking active flag    2020/04/14  Sumit
            if (!string.IsNullOrEmpty(IsVendorFilter) && BusinessUtility.GetInt(IsVendorFilter) == 1)
            {
                if (!string.IsNullOrEmpty(FilteredVendorConfig))
                {
                    VendorArr = FilteredVendorConfig.Split(',');
                    FilteredVendorDB = "";
                    for (int i = 0; i < VendorArr.Length; i++)
                    {
                        if (i == 0)
                            FilteredVendorDB = "'" + Convert.ToString(VendorArr[i]) + "'";
                        else
                            FilteredVendorDB = FilteredVendorDB + "," + "'" + Convert.ToString(VendorArr[i]) + "'";
                    }
                    sbSql = "SELECT distinct qecd.qecd_id FROM qdsexcert_data qecd inner join qdsexcert_file qecf on qecd.qecd_cert_file=qecf.qecf_filename " +
                            "WHERE qecd.qecd_sender_id in (" + FilteredVendorDB + ") and qecd.qecd_qecf_id="+ qecfID +" and qecd.qecd_processed = 0";
                }

            }
            #endregion to process only selected vendorlist without checking active flag    2020/04/14  Sumit 

            DbHelper oDbHelper = new DbHelper(sMysqlConnectionString, true);

            try
            {
                ErrorLog.createLog(sbSql.ToString());
                oDataTable = oDbHelper.GetDataTable(sbSql.ToString(), CommandType.Text, null);
            }
            catch (Exception ex)
            {
                ErrorLog.createLog("Error in getting all Qds External Certificate Data detail.");
                ErrorLog.createLog(ex.Message.ToString());
                ErrorLog.CreateLog(ex);
            }
            finally
            {
                oDbHelper.CloseDatabaseConnection();
            }
            return oDataTable;
        }

        /// <summary>
        /// to select single certificate on the basis of certificate id
        /// </summary>
        /// <param name="qdsCertID"></param>
        /// <returns></returns>
        public QdsExternalCertificateData GetSingleExternalCertificateRecord(Int32 qdsCertID)
        {
            QdsExternalCertificateData qdsExternalCertificate = null;
            sbSql = new StringBuilder();
            sbSql.Append("SELECT distinct qecf.qecf_filepath,qecd.* FROM qdsexcert_data qecd inner join qdsexcert_file qecf on qecd.qecd_cert_file=qecf.qecf_filename " +
                "WHERE qecd.qecd_id = @QecdID");
            DbHelper oDbHelper = new DbHelper(sMysqlConnectionString, true);

            try
            {
                MySqlParameter[] oMySqlParameter = {
                                                    DbUtility.GetParameter("@QecdID", qdsCertID, MyDbType.Int)
                                                    };
                
                ErrorLog.createLog(sbSql.ToString());
                ErrorLog.createLog("GetSingleExternalCertificateRecord Parameters:-");
                ErrorLog.createLog("@QecdID: " + qdsCertID);
                DataTable oDtData = new DbHelper(sMysqlConnectionString, true).GetDataTable(sbSql.ToString(), CommandType.Text, oMySqlParameter);
                if (oDtData.Rows.Count > 0)
                {
                    qdsExternalCertificate = new QdsExternalCertificateData();
                    foreach (DataRow oDataRow in oDtData.Rows)
                    {
                        qdsExternalCertificate.QdsExCert_Id = BusinessUtility.GetInt(oDataRow["qecd_id"]);
                        qdsExternalCertificate.QdsExCert_FileReceiveId = BusinessUtility.GetInt(oDataRow["qecd_filereceive_id"]);
                        qdsExternalCertificate.QdsExCert_QdsExCertFileId = BusinessUtility.GetInt(oDataRow["qecd_qecf_id"]);
                        qdsExternalCertificate.QdsExCert_SenderId = BusinessUtility.GetString(oDataRow["qecd_sender_id"]);
                        qdsExternalCertificate.QdsExCert_Heat = BusinessUtility.GetString(oDataRow["qecd_heat"]);
                        qdsExternalCertificate.QdsExCert_PoBranch = BusinessUtility.GetString(oDataRow["qecd_pobranch"]);
                        qdsExternalCertificate.QdsExCert_PoNumber = BusinessUtility.GetString(oDataRow["qecd_po"]);
                        qdsExternalCertificate.QdsExCert_VendorTagId = BusinessUtility.GetString(oDataRow["qecd_vendor_tag"]);
                        qdsExternalCertificate.QdsExCert_Filename = BusinessUtility.GetString(oDataRow["qecd_cert_file"]);
                        qdsExternalCertificate.QdsExCert_Processed = BusinessUtility.GetInt(oDataRow["qecd_processed"]);
                        qdsExternalCertificate.QdsExCert_CreatedOn = BusinessUtility.GetDateTime(oDataRow["qecd_created_on"]);
                        qdsExternalCertificate.QdsExCert_HeatQdsno = BusinessUtility.GetString(oDataRow["qecd_heatqdsno"]);
                        qdsExternalCertificate.QdsExCert_PrdQdsno = BusinessUtility.GetString(oDataRow["qecd_productqdsno"]);
                        qdsExternalCertificate.QdsExCert_ProcessedOn = BusinessUtility.GetDateTime(oDataRow["qecd_processed_on"]);
                        qdsExternalCertificate.QdsExCert_FilePath = BusinessUtility.GetString(oDataRow["qecf_filepath"]);
                    }
                }

            }
            catch (Exception ex)
            {
                ErrorLog.createLog("Error in getting single Qds External Certificate Data detail. Certificate ID: "+ qdsCertID);
                ErrorLog.createLog(ex.Message.ToString());
                ErrorLog.CreateLog(ex);
            }
            finally
            {
                oDbHelper.CloseDatabaseConnection();
            }
            return qdsExternalCertificate;
        }

        /// <summary>
        /// to get single certificate on the basis of po number, heat and vendor tag id
        /// </summary>
        /// <param name="PoNumber"></param>
        /// <param name="HeatNo"></param>
        /// <param name="VendorTagId"></param>
        /// <returns></returns>
        public QdsExternalCertificateData GetSingleExternalCertificateRecord(string PoNumber, string HeatNo, string VendorTagId)
        {
            QdsExternalCertificateData qdsExternalCertificate = null;
            sbSql = new StringBuilder();
            sbSql.Append("SELECT distinct qecf.qecf_filepath,qecd.* FROM qdsexcert_data qecd inner join qdsexcert_file qecf on qecd.qecd_cert_file = qecf.qecf_filename " +
                "WHERE qecd.qecd_po = @PoNumber and qecd.qecd_heat = @Heat and qecd.qecd_vendor_tag= @VendorTagId");
            DbHelper oDbHelper = new DbHelper(sMysqlConnectionString, true);

            try
            {
                MySqlParameter[] oMySqlParameter = {
                                                    DbUtility.GetParameter("@PoNumber", PoNumber, MyDbType.String),
                                                    DbUtility.GetParameter("@Heat", HeatNo, MyDbType.String),
                                                    DbUtility.GetParameter("@VendorTagId", VendorTagId , MyDbType.String)
                                                    };
                ErrorLog.createLog(sbSql.ToString());
                ErrorLog.createLog("GetSingleExternalCertificateRecord Parameters:-");
                ErrorLog.createLog("@PoNumber: " + PoNumber);
                ErrorLog.createLog("@Heat: " + HeatNo);
                ErrorLog.createLog("@VendorTagId: " + VendorTagId);
                DataTable oDtData = new DbHelper(sMysqlConnectionString, true).GetDataTable(sbSql.ToString(), CommandType.Text, oMySqlParameter);
                if (oDtData.Rows.Count > 0)
                {
                    qdsExternalCertificate = new QdsExternalCertificateData();
                    foreach (DataRow oDataRow in oDtData.Rows)
                    {
                        qdsExternalCertificate.QdsExCert_Id = BusinessUtility.GetInt(oDataRow["qecd_id"]);
                        qdsExternalCertificate.QdsExCert_FileReceiveId = BusinessUtility.GetInt(oDataRow["qecd_filereceive_id"]);
                        qdsExternalCertificate.QdsExCert_QdsExCertFileId = BusinessUtility.GetInt(oDataRow["qecd_qecf_id"]);
                        qdsExternalCertificate.QdsExCert_SenderId = BusinessUtility.GetString(oDataRow["qecd_sender_id"]);
                        qdsExternalCertificate.QdsExCert_Heat = BusinessUtility.GetString(oDataRow["qecd_heat"]);
                        qdsExternalCertificate.QdsExCert_PoBranch = BusinessUtility.GetString(oDataRow["qecd_pobranch"]);
                        qdsExternalCertificate.QdsExCert_PoNumber = BusinessUtility.GetString(oDataRow["qecd_po"]);
                        qdsExternalCertificate.QdsExCert_VendorTagId = BusinessUtility.GetString(oDataRow["qecd_vendor_tag"]);
                        qdsExternalCertificate.QdsExCert_Filename = BusinessUtility.GetString(oDataRow["qecd_cert_file"]);
                        qdsExternalCertificate.QdsExCert_Processed = BusinessUtility.GetInt(oDataRow["qecd_processed"]);
                        qdsExternalCertificate.QdsExCert_CreatedOn = BusinessUtility.GetDateTime(oDataRow["qecd_created_on"]);
                        qdsExternalCertificate.QdsExCert_HeatQdsno = BusinessUtility.GetString(oDataRow["qecd_heatqdsno"]);
                        qdsExternalCertificate.QdsExCert_PrdQdsno = BusinessUtility.GetString(oDataRow["qecd_productqdsno"]);
                        qdsExternalCertificate.QdsExCert_ProcessedOn = BusinessUtility.GetDateTime(oDataRow["qecd_processed_on"]);
                    }
                }

            }
            catch (Exception ex)
            {
                ErrorLog.createLog("Error in getting single Qds External Certificate Data detail. PO Number: " + PoNumber +" HeatNo: "+ HeatNo +" Vendor TagID: "+ VendorTagId);
                ErrorLog.createLog(ex.Message.ToString());
                ErrorLog.CreateLog(ex);
            }
            finally
            {
                oDbHelper.CloseDatabaseConnection();
            }
            return qdsExternalCertificate;
        }

        /// <summary>
        /// to update External Certificate on the basis of certificate id
        /// </summary>
        /// <param name="qExCertID"></param>
        /// <param name="heatQdsNo"></param>
        /// <param name="productQdsNo"></param>
        /// <returns></returns>
        public int UpdateQdsCertificate(Int32 qExCertID)
        {
            int iStatus = 0;
            sbSql = new StringBuilder();

            DbHelper oDbHelper = new DbHelper(sMysqlConnectionString, true);
            try
            {
                sbSql.Append("update qdsexcert_data set qecd_processed_on= NOW(), qecd_processed=1 WHERE qecd_id = @qecd_id");


                MySqlParameter[] oMySqlParameter = {
                                                    DbUtility.GetParameter("@qecd_id", qExCertID, MyDbType.Int)
                                                    };
                ErrorLog.createLog(sbSql.ToString());
                ErrorLog.createLog("UpdateQdsCertificate Parameters:-");
                ErrorLog.createLog("@qecd_id: " + qExCertID);
                iStatus = oDbHelper.ExecuteNonQuery(sbSql.ToString(), CommandType.Text, oMySqlParameter);
            }
            catch (Exception ex)
            {
                ErrorLog.createLog("Error in UpdateQdsCertificate. QdsExCertData_id: " + qExCertID.ToString());
                ErrorLog.CreateLog(ex);
            }
            finally
            {
                oDbHelper.CloseDatabaseConnection();
            }
            return iStatus;
        }
        
        /// <summary>
        /// To Update Heat Qds Number for External Certificate
        /// </summary>
        /// <param name="qExCertID"></param>
        /// <param name="heatQdsNo"></param>
        /// <returns></returns>
        public int UpdateHeatQdsCertificate(Int32 qExCertID, string heatQdsNo)
        {
            int iStatus = 0;
            sbSql = new StringBuilder();
            sbSql.Append("update qdsexcert_data set qecd_heatqdsno= @heatqdsno, qecd_processed_on= NOW() WHERE qecd_id = @qecd_id");
            DbHelper oDbHelper = new DbHelper(sMysqlConnectionString, true);

            try
            {
                MySqlParameter[] oMySqlParameter = {
                                                    DbUtility.GetParameter("@heatqdsno", heatQdsNo, MyDbType.String),
                                                    DbUtility.GetParameter("@qecd_id", qExCertID, MyDbType.Int)
                                                    };
                ErrorLog.createLog(sbSql.ToString());
                ErrorLog.createLog("UpdateHeatQdsCertificate Parameters:-");
                ErrorLog.createLog("@heatqdsno: " + heatQdsNo);
                ErrorLog.createLog("@qecd_id: " + qExCertID);
                iStatus = oDbHelper.ExecuteNonQuery(sbSql.ToString(), CommandType.Text, oMySqlParameter);
            }
            catch (Exception ex)
            {
                ErrorLog.createLog("Error in UpdateHeatQdsCertificate. QdsExCertData_id: " + qExCertID.ToString() + "    HeatQdsNo: " + heatQdsNo);
                ErrorLog.CreateLog(ex);
            }
            finally
            {
                oDbHelper.CloseDatabaseConnection();
            }
            return iStatus;
        }

        /// <summary>
        /// To Update Product Qds Number for External Certificate
        /// </summary>
        /// <param name="qExCertID"></param>
        /// <param name="productQdsNo"></param>
        /// <returns></returns>
        public int UpdateProductQdsCertificate(Int32 qExCertID, string productQdsNo)
        {
            int iStatus = 0;
            sbSql = new StringBuilder();
            sbSql.Append("update qdsexcert_data set qecd_productqdsno=@productqdsno, qecd_processed_on= NOW() WHERE qecd_id = @qecd_id");
            DbHelper oDbHelper = new DbHelper(sMysqlConnectionString, true);

            try
            {
                MySqlParameter[] oMySqlParameter = {
                                                    DbUtility.GetParameter("@productqdsno", productQdsNo, MyDbType.String),
                                                    DbUtility.GetParameter("@qecd_id", qExCertID, MyDbType.Int)
                                                    };
                ErrorLog.createLog(sbSql.ToString());
                ErrorLog.createLog("UpdateProductQdsCertificate Parameters:-");
                ErrorLog.createLog("@productqdsno: " + productQdsNo);
                ErrorLog.createLog("@qecd_id: " + qExCertID);
                iStatus = oDbHelper.ExecuteNonQuery(sbSql.ToString(), CommandType.Text, oMySqlParameter);
            }
            catch (Exception ex)
            {
                ErrorLog.createLog("Error in UpdateProductQdsCertificate. QdsExCertData_id: " + qExCertID.ToString() + "    ProductQds: " + productQdsNo);
                ErrorLog.CreateLog(ex);
            }
            finally
            {
                oDbHelper.CloseDatabaseConnection();
            }
            return iStatus;
        }

        /// <summary>
        /// to get Heat Qds Number from header863 on the basis of po number, heat and vendortagid
        /// </summary>
        /// <param name="PoNumber"></param>
        /// <param name="HeatNo"></param>
        /// <param name="VendorTagId"></param>
        /// <returns></returns>
        public long isHeatQdsExist(string PoNumber, string HeatNo, string VendorTagId)
        {
            long qdsNumber = 0;

            DataTable oDataTable = null;
            string sbSql = string.Empty;

            sbSql = "select distinct hdrHeatQds from header863 where origPoNo=@PoNumber and heat=@Heat and VendorTagId=@VendorTagId";

            DbHelper oDbHelper = new DbHelper(sMysqlConnectionString, true);

            try
            {
                MySqlParameter[] oMySqlParameter = {
                                                    DbUtility.GetParameter("@PoNumber", PoNumber, MyDbType.String),
                                                    DbUtility.GetParameter("@Heat", HeatNo, MyDbType.String),
                                                    DbUtility.GetParameter("@VendorTagId", VendorTagId , MyDbType.String)
                                                    };
                ErrorLog.createLog(sbSql.ToString());
                ErrorLog.createLog("isHeatQdsExist Parameters:-");
                ErrorLog.createLog("@PoNumber: " + PoNumber);
                ErrorLog.createLog("@Heat: " + HeatNo);
                ErrorLog.createLog("@VendorTagId: " + VendorTagId);
                oDataTable = oDbHelper.GetDataTable(sbSql.ToString(), CommandType.Text, oMySqlParameter);
                ErrorLog.createLog("Total Heat Qds found for certificate : " + oDataTable.Rows.Count);
                if (oDataTable.Rows.Count >0)
                {
                    foreach(DataRow drow in oDataTable.Rows)
                    {
                        qdsNumber = BusinessUtility.GetLong(drow["hdrHeatQds"]);
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorLog.createLog("Error in isHeatQdsExist.");
                ErrorLog.createLog(ex.Message.ToString());
                ErrorLog.CreateLog(ex);
            }
            finally
            {
                oDbHelper.CloseDatabaseConnection();
            }
            return qdsNumber;
        }

        /// <summary>
        /// to get Product Qds Number from header863 on the basis of Po number, heat and vendortagid
        /// </summary>
        /// <param name="PoNumber"></param>
        /// <param name="HeatNo"></param>
        /// <param name="VendorTagId"></param>
        /// <returns></returns>
        public long isProductQdsExist(string PoNumber, string HeatNo, string VendorTagId)
        {
            long qdsNumber = 0;

            DataTable oDataTable = null;
            string sbSql = string.Empty;

            sbSql = "select hdrProductQds from header863 where origPoNo=@PoNumber and heat=@Heat and VendorTagId=@VendorTagId and hdrProductQds !=0";

            DbHelper oDbHelper = new DbHelper(sMysqlConnectionString, true);

            try
            {
                MySqlParameter[] oMySqlParameter = {
                                                    DbUtility.GetParameter("@PoNumber", PoNumber, MyDbType.String),
                                                    DbUtility.GetParameter("@Heat", HeatNo, MyDbType.String),
                                                    DbUtility.GetParameter("@VendorTagId", VendorTagId , MyDbType.String)
                                                    };
                ErrorLog.createLog(sbSql.ToString());
                ErrorLog.createLog("isProductQdsExist Parameters:-");
                ErrorLog.createLog("@PoNumber: " + PoNumber);
                ErrorLog.createLog("@Heat: " + HeatNo);
                ErrorLog.createLog("@VendorTagId: " + VendorTagId);
                oDataTable = oDbHelper.GetDataTable(sbSql.ToString(), CommandType.Text, oMySqlParameter);
                ErrorLog.createLog("Total Product Qds found for certificate : " + oDataTable.Rows.Count);
                if (oDataTable.Rows.Count > 0)
                {
                    foreach (DataRow drow in oDataTable.Rows)
                    {
                        qdsNumber = BusinessUtility.GetLong(drow["hdrProductQds"]);
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorLog.createLog("Error in isProductQdsExist.");
                ErrorLog.createLog(ex.Message.ToString());
                ErrorLog.CreateLog(ex);
            }
            finally
            {
                oDbHelper.CloseDatabaseConnection();
            }
            return qdsNumber;
        }


    }


}
