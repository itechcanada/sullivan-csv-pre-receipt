﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using iTECH.Library.DataAccess.MySql;
using MySql.Data.MySqlClient;
using System.Data;
using iTECH.Library.Utilities;
using System.IO;
using System.Text.RegularExpressions;

namespace SecureFTP.ExternalCertificate
{
    public class QdsExternalCertificateFile
    {
        static string sMysqlConnectionString = ConfigurationManager.ConnectionStrings["NewConnectionString"].ToString();
        StringBuilder sbSql;

        public Int32 QdsExCertFile_Id { get; set; }                          
        public Int32 QdsExCertFile_FileReceiveId { get; set; }              
        public string QdsExCertFile_FilePath { get; set; }
        public string QdsExCertFile_FileName { get; set; }
        public Nullable<DateTime> QdsExCertFile_ReceivedOn { get; set; }
        public Nullable<DateTime> QdsExCertFile_ProcessedOn { get; set; }
        public int QdsExCertFile_Processed { get; set; }
        public string QdsExCertFile_FileFormat { get; set; }


        /// <summary>
        /// To insert Qds External Certificate Text file record in database
        /// </summary>
        /// <param name="qdsExternalCertificateFile"></param>
        /// <returns></returns>
        public int InsertExternalCertificateFile(QdsExternalCertificateFile qdsExternalCertificateFile)
        {
            int iStatus = 0;
            sbSql = new StringBuilder();
            DbHelper oDbHelper = new DbHelper(sMysqlConnectionString, true);
            try
            {
                sbSql.Append(" INSERT INTO qdsexcert_file ( qecf_filereceive_id,qecf_filepath, qecf_filename,qecf_fileformat) ");
                sbSql.Append(" VALUES (@FileReceivedId, @FilePath, @FileName, @FileFormat) ");

                MySqlParameter[] oMySqlParameter = {
                                         DbUtility.GetParameter("@FileReceivedId", qdsExternalCertificateFile.QdsExCertFile_FileReceiveId, MyDbType.Int),
                                         DbUtility.GetParameter("@FilePath", qdsExternalCertificateFile.QdsExCertFile_FilePath, MyDbType.String),
                                         DbUtility.GetParameter("@FileName", qdsExternalCertificateFile.QdsExCertFile_FileName, MyDbType.String),
                                         DbUtility.GetParameter("@FileFormat", qdsExternalCertificateFile.QdsExCertFile_FileFormat, MyDbType.String)
                                             };
                ErrorLog.createLog(sbSql.ToString());
                ErrorLog.createLog("InsertExternalCertificateFile Parameters:-");
                ErrorLog.createLog("@FileReceivedId: " + qdsExternalCertificateFile.QdsExCertFile_FileReceiveId);
                ErrorLog.createLog("@FilePath: " + qdsExternalCertificateFile.QdsExCertFile_FilePath);
                ErrorLog.createLog("@FileName: " + qdsExternalCertificateFile.QdsExCertFile_FileName);
                ErrorLog.createLog("@FileFormat: " + qdsExternalCertificateFile.QdsExCertFile_FileFormat);
                iStatus = oDbHelper.ExecuteNonQuery(sbSql.ToString(), CommandType.Text, oMySqlParameter);
                iStatus = oDbHelper.GetLastInsertID();
            }
            catch (Exception ex)
            {
                ErrorLog.createLog("Error in inserting Qds External Certificate file detail. FileName:"+ qdsExternalCertificateFile.QdsExCertFile_FileName);
                ErrorLog.CreateLog(ex);
            }
            finally
            {
                oDbHelper.CloseDatabaseConnection();
            }
            return iStatus;
        }

        /// <summary>
        /// To get single Qds External Certificate file name and other properties
        /// </summary>
        /// <param name="qdsExCert_Fileid"></param>
        /// <returns></returns>
        public QdsExternalCertificateFile GetExternalCertificateFile(Int32 qdsExCert_Fileid)
        {
            QdsExternalCertificateFile qdsExternalCertificateFile = null;
            sbSql = new StringBuilder();
            sbSql.Append("SELECT * FROM qdsexcert_file WHERE qecf_id = @Qecf_Id");
            DbHelper oDbHelper = new DbHelper(sMysqlConnectionString, true);

            try
            {
                MySqlParameter[] oMySqlParameter = {
                                                    DbUtility.GetParameter("@Qecf_Id", qdsExCert_Fileid, MyDbType.Int)
                                                    };
                ErrorLog.createLog(sbSql.ToString());
                ErrorLog.createLog("GetExternalCertificateFile Parameters:-");
                ErrorLog.createLog("@Qecf_Id: " + qdsExCert_Fileid);
                DataTable oDtData = new DbHelper(sMysqlConnectionString, true).GetDataTable(sbSql.ToString(), CommandType.Text, oMySqlParameter);
                if (oDtData.Rows.Count > 0)
                {
                    qdsExternalCertificateFile = new QdsExternalCertificateFile();
                    foreach (DataRow oDataRow in oDtData.Rows)
                    {
                        qdsExternalCertificateFile.QdsExCertFile_Id = BusinessUtility.GetInt(oDataRow["qecf_id"]);
                        qdsExternalCertificateFile.QdsExCertFile_FileReceiveId = BusinessUtility.GetInt(oDataRow["qecf_filereceive_id"]);
                        qdsExternalCertificateFile.QdsExCertFile_FilePath = BusinessUtility.GetString(oDataRow["qecf_filepath"]);
                        qdsExternalCertificateFile.QdsExCertFile_FileName = BusinessUtility.GetString(oDataRow["qecf_filename"]);
                        qdsExternalCertificateFile.QdsExCertFile_ReceivedOn = BusinessUtility.GetDateTime(oDataRow["qecf_received_on"]);
                        qdsExternalCertificateFile.QdsExCertFile_ProcessedOn = BusinessUtility.GetDateTime(oDataRow["qecf_processed_on"]);
                        qdsExternalCertificateFile.QdsExCertFile_Processed = BusinessUtility.GetInt(oDataRow["qecf_processed"]);
                        qdsExternalCertificateFile.QdsExCertFile_FileFormat = BusinessUtility.GetString(oDataRow["qecf_fileformat"]);
                    }
                }

            }
            catch (Exception ex)
            {
                ErrorLog.createLog("Error in getting Qds External Certificate file detail. File ID: " + qdsExCert_Fileid.ToString());
                ErrorLog.CreateLog(ex);
            }
            finally
            {
                oDbHelper.CloseDatabaseConnection();
            }
            return qdsExternalCertificateFile;
        }

        /// <summary>
        /// to get single qds external Certificate file properties by filename
        /// </summary>
        /// <param name="qdsExCert_FileName"></param>
        /// <returns></returns>
        public QdsExternalCertificateFile GetExternalCertificateFile_ByFileName(string qdsExCert_FileName)
        {
            QdsExternalCertificateFile qdsExternalCertificateFile = null;
            sbSql = new StringBuilder();
            sbSql.Append("SELECT * FROM qdsexcert_file WHERE qecf_filename = @Qecf_Filename");
            DbHelper oDbHelper = new DbHelper(sMysqlConnectionString, true);

            try
            {
                MySqlParameter[] oMySqlParameter = {
                                                    DbUtility.GetParameter("@Qecf_Filename", qdsExCert_FileName, MyDbType.String)
                                                    };
                ErrorLog.createLog(sbSql.ToString());
                ErrorLog.createLog("GetExternalCertificateFile_ByFileName Parameters:-");
                ErrorLog.createLog("@Qecf_Filename: " + qdsExCert_FileName);
                DataTable oDtData = new DbHelper(sMysqlConnectionString, true).GetDataTable(sbSql.ToString(), CommandType.Text, oMySqlParameter);
                if (oDtData.Rows.Count > 0)
                {
                    qdsExternalCertificateFile = new QdsExternalCertificateFile();
                    foreach (DataRow oDataRow in oDtData.Rows)
                    {
                        qdsExternalCertificateFile.QdsExCertFile_Id = BusinessUtility.GetInt(oDataRow["qecf_id"]);
                        qdsExternalCertificateFile.QdsExCertFile_FileReceiveId = BusinessUtility.GetInt(oDataRow["qecf_filereceive_id"]);
                        qdsExternalCertificateFile.QdsExCertFile_FilePath = BusinessUtility.GetString(oDataRow["qecf_filepath"]);
                        qdsExternalCertificateFile.QdsExCertFile_FileName = BusinessUtility.GetString(oDataRow["qecf_filename"]);
                        qdsExternalCertificateFile.QdsExCertFile_ReceivedOn = BusinessUtility.GetDateTime(oDataRow["qecf_received_on"]);
                        qdsExternalCertificateFile.QdsExCertFile_ProcessedOn = BusinessUtility.GetDateTime(oDataRow["qecf_processed_on"]);
                        qdsExternalCertificateFile.QdsExCertFile_Processed = BusinessUtility.GetInt(oDataRow["qecf_processed"]);
                        qdsExternalCertificateFile.QdsExCertFile_FileFormat = BusinessUtility.GetString(oDataRow["qecf_fileformat"]);
                    }
                }

            }
            catch (Exception ex)
            {
                ErrorLog.createLog("Error in getting Qds External Certificate file detail. File name: " + qdsExCert_FileName.ToString());
                ErrorLog.CreateLog(ex);
            }
            finally
            {
                oDbHelper.CloseDatabaseConnection();
            }
            return qdsExternalCertificateFile;
        }

        /// <summary>
        /// To get all ready to process Qds External Certificate text files details
        /// </summary>
        /// <returns></returns>
        public DataTable GetExternalCertificateTextFile_ReadyToBeProcessed()
        {

            DataTable oDataTable = new DataTable();
            sbSql = new StringBuilder();
            DbHelper oDbHelper = new DbHelper(sMysqlConnectionString, true);
            try
            {
                sbSql.Append("select qecf_id from qdsexcert_file where qecf_processed = 0 and qecf_fileformat='txt'");
                ErrorLog.createLog(sbSql.ToString());
                oDataTable = oDbHelper.GetDataTable(sbSql.ToString(), CommandType.Text, null);
            }
            catch (Exception ex)
            {
                ErrorLog.createLog("Error in getting GetExternalCertificateFile_ReadyToBeProcessed");
                ErrorLog.CreateLog(ex);
            }
            finally
            {
                oDbHelper.CloseDatabaseConnection();
            }
            return oDataTable;
        }

        /// <summary>
        /// To update single Qds External Certificate file after parsing content
        /// </summary>
        /// <param name="qdsExCert_Fileid"></param>
        /// <returns></returns>
        public int UpdateExternalCertificateFile(Int32 qdsExCert_Fileid)
        {
            int iStatus = 0;
            sbSql = new StringBuilder();
            sbSql.Append("update qdsexcert_file set qecf_processed_on = NOW(), qecf_processed=1 WHERE qecf_id = @Qecf_Id");
            DbHelper oDbHelper = new DbHelper(sMysqlConnectionString, true);

            try
            {
                MySqlParameter[] oMySqlParameter = {
                                                    DbUtility.GetParameter("@Qecf_Id", qdsExCert_Fileid, MyDbType.Int)
                                                    };
                ErrorLog.createLog(sbSql.ToString());
                ErrorLog.createLog("UpdateExternalCertificateFile Parameters:-");
                ErrorLog.createLog("@Qecf_Id: " + qdsExCert_Fileid);
                iStatus = oDbHelper.ExecuteNonQuery(sbSql.ToString(), CommandType.Text, oMySqlParameter);

            }
            catch (Exception ex)
            {
                ErrorLog.createLog("Error in update Qds External Certificate file record. File ID : " + qdsExCert_Fileid.ToString());
                ErrorLog.CreateLog(ex);
            }
            finally
            {
                oDbHelper.CloseDatabaseConnection();
            }
            return iStatus;
        }

        /// <summary>
        /// To update Qds External Certificate file after attachment with Qds
        /// </summary>
        /// <param name="qdsExCert_Fileid"></param>
        /// <returns></returns>
        public int UpdateExternalCertificateFile_ByFileName(string qdsExCert_FileName)
        {
            int iStatus = 0;
            sbSql = new StringBuilder();
            DataTable oDataTable = new DataTable();
            DbHelper oDbHelper = new DbHelper(sMysqlConnectionString, true);

            try
            {
                ErrorLog.createLog("select qecf_id from qdsexcert_file where qecf_filename='" + qdsExCert_FileName + "'");
                oDataTable = oDbHelper.GetDataTable("select qecf_id from qdsexcert_file where qecf_filename='" + qdsExCert_FileName + "'", CommandType.Text, null);

                if(oDataTable.Rows.Count >0)
                {
                    if(BusinessUtility.GetInt(oDataTable.Rows[0]["qecf_id"]) >0)
                    {
                        sbSql.Append("update qdsexcert_file set qecf_processed_on = NOW(), qecf_processed=1 WHERE qecf_id = @Qecf_Id");
                        MySqlParameter[] oMySqlParameter = {
                                                    DbUtility.GetParameter("@Qecf_Id", BusinessUtility.GetInt(oDataTable.Rows[0]["qecf_id"]), MyDbType.Int)
                                                    };
                        ErrorLog.createLog(sbSql.ToString());
                        ErrorLog.createLog("UpdateExternalCertificateFile_ByFileName Parameters:-");
                        ErrorLog.createLog("@Qecf_Id: " + BusinessUtility.GetInt(oDataTable.Rows[0]["qecf_id"]));
                        iStatus = oDbHelper.ExecuteNonQuery(sbSql.ToString(), CommandType.Text, oMySqlParameter);
                    }
                }
                
            }
            catch (Exception ex)
            {
                ErrorLog.createLog("Error in UpdateExternalCertificateFile_ByFileName. File Name : " + qdsExCert_FileName);
                ErrorLog.CreateLog(ex);
            }
            finally
            {
                oDbHelper.CloseDatabaseConnection();
            }
            return iStatus;
        }

        /// <summary>
        /// To identify file is valid certificate text format or not
        /// </summary>
        /// <param name="filepath"></param>
        /// <param name="filename"></param>
        /// <returns></returns>
        public bool isValidCertificateFile(string filepath, string filename)
        {
            bool isValid = false;
            FileStream fs;
            StreamReader sr;
            string line;
            string[] lineValues;
            Regex regEx = new Regex(".pdf|");
            try
            {
                fs = new FileStream(Path.Combine(filepath, filename), FileMode.Open, FileAccess.Read);
                sr = new StreamReader(fs);
                line = sr.ReadLine();

                if (line.Trim() != "")
                {
                    if (regEx.Match(line.Trim()).Success)
                    {
                        lineValues = line.Split('|');
                        if (lineValues.Length == 6)
                        {
                            isValid = true;
                        }
                    }
                }
                sr.Close();
                fs.Close();
                sr.Dispose();
                fs.Dispose();
            }
            catch (Exception ex)
            {
                ErrorLog.createLog("Error in isValidCertificateFile. Filename: " + Path.Combine(filepath, filename));
                ErrorLog.CreateLog(ex);
            }

            return isValid;
        }
    }
}
