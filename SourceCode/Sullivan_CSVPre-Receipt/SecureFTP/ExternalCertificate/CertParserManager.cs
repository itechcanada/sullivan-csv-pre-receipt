﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SecureFTP.ExternalCertificate
{
    class CertParserManager
    {
        public ICertParser CallParser(string fileFormat)
        {
            ICertParser objParser = null;
            if(!string.IsNullOrEmpty(fileFormat))
            {
                if (fileFormat.ToLower() == "txt")
                {
                    objParser = new TextCertParser();
                }
            }
            return objParser;
        }
    }
}
