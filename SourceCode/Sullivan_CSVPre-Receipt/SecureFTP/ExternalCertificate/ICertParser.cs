﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SecureFTP.ExternalCertificate
{
    interface ICertParser
    {
        bool ParseCertificate(string filePath, string fileName, Int32 fileReceivedId, Int32 qdsFileid);
    }
}
