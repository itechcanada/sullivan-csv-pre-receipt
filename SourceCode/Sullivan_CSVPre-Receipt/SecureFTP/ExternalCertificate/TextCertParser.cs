﻿using iTECH.Library.Utilities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Configuration;
using System.Text;
using System.Threading.Tasks;

namespace SecureFTP.ExternalCertificate
{
    class TextCertParser : ICertParser
    {
        QdsExternalCertificateData QdsExCertData;
        string ProcessingBranch = ConfigurationManager.AppSettings["ProcessingBranch"];                 //for PO Branch Validation

        public bool ParseCertificate(string filePath, string fileName, Int32 fileReceivedId, Int32 qdsFileid)
        {
            bool isParsed =false;
            FileStream fs = new FileStream(Path.Combine(filePath, fileName), FileMode.Open, FileAccess.Read); 
            StreamReader sr = new StreamReader(fs); 
            string line;
            string[] lineValues;
            string[] poNumber;
            try
            {
                QdsExCertData = new QdsExternalCertificateData();
                while ((line = sr.ReadLine()) != null)
                {
                    if (line.Trim() != "")
                    {
                        lineValues = line.Split('|');
                        if (lineValues.Length == 6)
                        {
                            QdsExCertData.QdsExCert_SenderId = BusinessUtility.GetString(lineValues[0]);
                            QdsExCertData.QdsExCert_Heat = BusinessUtility.GetString(lineValues[1]);

                            QdsExCertData.QdsExCert_PoNumber = BusinessUtility.GetString(lineValues[2]);
                            if (QdsExCertData.QdsExCert_PoNumber.Contains('-'))
                            {
                                poNumber = QdsExCertData.QdsExCert_PoNumber.Split('-');
                                if (poNumber.Length > 1)
                                {
                                    //QdsExCertData.QdsExCert_PoBranch = BusinessUtility.GetString(poNumber[0]);
                                    QdsExCertData.QdsExCert_PoNumber = BusinessUtility.GetString(poNumber[1]);
                                }
                            }

                            //if (string.IsNullOrEmpty(QdsExCertData.QdsExCert_PoBranch))
                            //{
                            //    QdsExCertData.QdsExCert_PoBranch = tctipd.GetBranchCode_PO(QdsExCertData.QdsExCert_PoNumber);
                            //}

                            QdsExCertData.QdsExCert_PoBranch = tctipd.GetBranchCode_PO(QdsExCertData.QdsExCert_PoNumber);

                            QdsExCertData.QdsExCert_Filename = BusinessUtility.GetString(lineValues[4]);
                            QdsExCertData.QdsExCert_VendorTagId = BusinessUtility.GetString(lineValues[5]);
                            QdsExCertData.QdsExCert_FileReceiveId = fileReceivedId;
                            QdsExCertData.QdsExCert_QdsExCertFileId = qdsFileid;

                            if (ProcessingBranch.Contains(QdsExCertData.QdsExCert_PoBranch))
                            {
                                //insert data into qdsexcert_data
                                if (QdsExCertData.InsertExternalCertificateRecord(QdsExCertData) < 1)
                                {
                                    ErrorLog.createLog("External Certificate file not parsed successfully. FileName: " + Path.Combine(filePath, fileName));
                                    return false;
                                }
                            }
                            else
                            {
                                ErrorLog.createLog("Skipping Certificate File. Branch Not Configured. PO : " + QdsExCertData.QdsExCert_PoNumber + "   Branch : " + QdsExCertData.QdsExCert_PoBranch);
                            }
                        }
                    }
                }
                isParsed = true;
            }
            catch (Exception ex)
            {
                ErrorLog.createLog("Error in Parsering Certificate Text File. FileName: " + Path.Combine(filePath, fileName));
                ErrorLog.CreateLog(ex);
            }
            finally
            {
                if (sr != null)
                {
                    sr.Close();
                    sr.Dispose();
                }
                if(fs != null)
                {
                    fs.Close();
                    fs.Dispose();
                }
            }
            return isParsed;
        }

    }
}
