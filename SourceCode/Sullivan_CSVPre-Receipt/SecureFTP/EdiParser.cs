﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Text.RegularExpressions;
using System.Xml.Linq;
using iTECH.Library.DataAccess.ODBC;
using iTECH.Library.Utilities;
using System.Configuration;
using System.Data;
using MySql.Data.MySqlClient;
using System.Data.Odbc;
using System.Collections;

namespace SecureFTP
{
    class EdiParser
    {

        string[] m_sArr = null;
        string m_sSql = string.Empty;
        StringBuilder m_sbSql = null;
        static string sStratixConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ToString();
        static string sMysqlConnectionString = ConfigurationManager.ConnectionStrings["NewConnectionString"].ToString();

        static string m_sCompanyId = ConfigurationManager.AppSettings["CompanyId"];
        static string m_sIntchgPfx = ConfigurationManager.AppSettings["IntchgPfx"];
        static string m_sCusVenId = ConfigurationManager.AppSettings["CusVenId"];
        static string m_sRcptType = ConfigurationManager.AppSettings["RcptType"];

        static int m_iCusVenId = BusinessUtility.GetInt(ConfigurationManager.AppSettings["CusVenId"]);
        private static string EdiFile = ConfigurationManager.AppSettings["EdiFile"];

        int m_iHeaderId = 0;
        int m_iDetailId = 0;

        int iTotalHlCount = 0;
        public int TotalHlCount
        {
            get { return iTotalHlCount; }
        }

        int iTotalHlOrderCount = 0;
        public int TotalHlOrderCount
        {
            get { return iTotalHlOrderCount; }
        }

        int iHeaderStartIndex = 0;
        public int HeaderStartIndex
        {
            get { return iHeaderStartIndex; }
        }

        int iHeaderEndIndex = 0;
        public int HeaderEndIndex
        {
            get { return iHeaderEndIndex; }
        }

        int iShipmentStartIndex = 0;
        public int ShipmentStartIndex
        {
            get { return iShipmentStartIndex; }
        }

        int iShipmentEndIndex = 0;
        public int ShipmentEndIndex
        {
            get { return iShipmentEndIndex; }
        }

        string sPoNo = string.Empty;

        string sHlOID = string.Empty;
        string sHlOParentID = string.Empty;
        string sHlOLevel = string.Empty;
        string sHlOChildID = string.Empty;

        string sHlIID = string.Empty;
        string sHlIParentID = string.Empty;
        string sHlILevel = string.Empty;
        string sHlIChildID = string.Empty;

        int m_iFileId = 0;

        public struct EdiKeyWords
        {
            public const string CTT = "CTT";
            public const string BSN = "BSN";
            public const string DTM = "DTM";
            public const string TD1 = "TD1";
            public const string TD5 = "TD5";
            public const string TD3 = "TD3";
            public const string REF = "REF";
            public const string N1 = "N1";
            public const string LIN = "LIN";
            public const string HL = "HL";
            public const string PRF = "PRF";
            public const string MEA = "MEA";
            public const string SN1 = "SN1";

        }

        public struct UNITOFMEASURMENT
        {
            public const string LF = "LF";
        }

        static string m_sStartBlock = "ST";
        static string m_sEndBlock = "SE";
        static string m_sInterchangeControlTrailer = "IEA";

        Orders oOrders = new Orders();
        Items oItems = new Items();
        Shipment oShipment = new Shipment();
        Details oDetails = new Details();
        XCTI55 oXCTI55 = null;
        List<Items> lstItems = new List<Items>();
        Dictionary<string, List<string>> dicOrder = new Dictionary<string, List<string>>();

        ArrayList m_arl = new ArrayList();
        Dictionary<int, ArrayList> m_dict = new Dictionary<int, ArrayList>();
        Dictionary<string, string> dictIndexHolder = new Dictionary<string, string>();
        ArrayList m_arlHdrShipmentList = new ArrayList();

        List<string> lstWholeFile = new List<string>();

        Files oFiles;
        Configuration oConfiguration;

        public EdiParser()
        { }

        public EdiParser(string[] sArr, int iFileId)
        {
            ErrorLog.createLog("m_iFileId = " + iFileId);
            oFiles = new Files(iFileId);
            m_sArr = sArr;
            m_iFileId = iFileId;
            m_sArr = m_sArr.Where(x => !string.IsNullOrEmpty(x)).ToArray(); // removing empty segment 

           
           // Utility.AddArrayToList(sArr, ref lstWholeFile);
            //this.InsertFileDetail(lstWholeFile,m_iFileId);

            

            oConfiguration = new Configuration();
            oConfiguration.PopulateConfigurationData(sArr);

            ErrorLog.createLog("Inside ediparsor constructor.. :" + m_iFileId + " : " + oFiles.EdiFileType) ;
            this.StartProcessing();

            for (int i = 0; i < m_dict.Count; i++)
            {
                //Create header
                if ((i > 0) && (i < m_dict.Count-1))
                {
                    this.StartProcess(Utility.ConvertArrayListToArray(m_dict[i]));
                }

            }

            // this code will execute only for one receipt per po.
            if (oConfiguration.FileProcessingType == BusinessUtility.GetInt(Configuration.EdiFileProcessingType.ONERECEIPTPERPO))
            {
                STX856 oSTX856 = new STX856();
                oSTX856.ProcessReceipt(iFileId);
            }

        }

        public void InsertFileDetail(List<string> lstFile, int iFileId)
        {
            var prf = lstFile.Where(x => x.Contains("PRF*")).ToList();
            var bsn = lstFile.Where(x => x.Contains("BSN*")).ToList();
            int i = 0;
            string sbsn = string.Empty;
            string sPo = string.Empty;
            foreach (string s in prf)
            {
                sPo = RemoveLineBreaker(s).Split('*')[1];
                if (i < bsn.Count)
                {
                    sbsn = bsn[i];
                    sbsn = sbsn.Split('*')[2];
                }
                //oFiles.InsertFileDetail(iFileId, sbsn, sPo);
                i++;
            }
        }


        public void StartProcessing()
        {
            bool bStartBlockFound = false;
            bool bEndBlockFound = false;
            bool bInterchangeControlTrailer = false;
            string sSegment = string.Empty;
            int iNoOfblock = 0;
            bool bInEndBlock = false;
            for (int i = 0; i < m_sArr.Length; i++)
            {
                sSegment = BusinessUtility.GetString(m_sArr[i]);
                bStartBlockFound = sSegment.Substring(0, 2).Equals(m_sStartBlock);
                bEndBlockFound = sSegment.Substring(0, 2).Equals(m_sEndBlock);
                bInterchangeControlTrailer = sSegment.Substring(0, 3).Equals(m_sInterchangeControlTrailer);

                if (!bStartBlockFound)
                {
                    m_arl.Add(sSegment);
                }
                if (bInEndBlock)
                {
                    m_arl.Add(sSegment);
                    bInEndBlock = false;
                }
                if (bStartBlockFound && iNoOfblock == 0)
                {
                    if (!m_dict.ContainsKey(iNoOfblock))
                    {
                        m_dict.Add(iNoOfblock, m_arl);
                    }
                    else
                    {
                        m_dict.Add(iNoOfblock + 1, m_arl);
                    }
                    iNoOfblock++;
                    m_arl = new ArrayList();
                    m_arl.Add(sSegment);
                }
                else if (bEndBlockFound)
                {
                    bInEndBlock = true;
                    if (!m_dict.ContainsKey(iNoOfblock))
                    {
                        m_dict.Add(iNoOfblock, m_arl);
                    }
                    else
                    {
                        m_dict.Add(iNoOfblock + 1, m_arl);
                    }
                    iNoOfblock++;
                    m_arl = new ArrayList();
                    //m_arl.Add(sSegment);
                }
                else if (bInterchangeControlTrailer)
                {
                    if (!m_dict.ContainsKey(iNoOfblock))
                    {
                        m_dict.Add(iNoOfblock, m_arl);
                    }
                    else
                    {
                        m_dict.Add(iNoOfblock + 1, m_arl);
                    }
                    iNoOfblock++;
                    bInEndBlock = false;
                    m_arl = new ArrayList();
                }


            }

        } // start processing end here..


        public void StartProcess(string[] sArr)
        {
            string sHl = string.Empty;
            string sHlNext = string.Empty;
            int iHlPos = 0;
            int iHlNextPos = 0;
            string sStartEndPos = string.Empty;
            List<string> lstArray = new List<string>();
            AddElementBetweenArrayWithStartPosToList(ref lstArray, sArr, 0, m_sArr.Length - 1);

            dictIndexHolder = new Dictionary<string, string>();

            List<string> lstHL = new List<string>();
            m_arlHdrShipmentList = new ArrayList();
            bool bHl2Found = false;  // logic to add shipment section to list
            foreach (string s in sArr)
            {
                if (s.Contains("HL*2"))
                {
                    bHl2Found = true;
                }
                if (!bHl2Found)
                {
                    m_arlHdrShipmentList.Add(s);
                }

            }


            lstHL = lstArray.Where(x => x.Contains("HL*")).ToList();
            for (int i = 0; i < lstHL.Count; i++)
            {

                sHl = lstHL.ElementAt(i);

                if (lstHL.Count > i + 1)
                {
                    sHlNext = lstHL.ElementAt(i + 1);
                }
                else
                {
                    sHlNext = string.Empty;
                }

                iHlPos = Array.FindIndex(sArr, x => x.Contains(sHl));
                if (!string.IsNullOrEmpty(sHlNext))
                {
                    iHlNextPos = Array.FindIndex(sArr, x => x.Contains(sHlNext)) - 1;
                }
                else
                {
                    iHlNextPos = Array.FindIndex(sArr, x => x.Contains("CTT*"));
                    if (iHlNextPos < 0)
                    {
                        iHlNextPos = Array.FindIndex(sArr, x => x.Contains("SE*"));
                    }
                }

                sStartEndPos = BusinessUtility.GetString(iHlPos) + ',' + BusinessUtility.GetString(iHlNextPos);
                dictIndexHolder.Add(sHl, sStartEndPos);
            }

            if (oConfiguration.FileProcessingType == BusinessUtility.GetInt(Configuration.EdiFileProcessingType.DEFAULT))
            {
                Edi856Parser oEdi856Parser = new Edi856Parser(dictIndexHolder, sArr, m_iFileId, m_arlHdrShipmentList);
            }

            if (oConfiguration.FileProcessingType == BusinessUtility.GetInt(Configuration.EdiFileProcessingType.ONERECEIPTPERPO))
            {
                ErrorLog.createLog("oConfiguration.FileProcessingType = ONERECEIPTPERPO");
                Edi856ParserSingleReceiptPerPo oEdi856ParserSingleReceiptPerPo = new Edi856ParserSingleReceiptPerPo(dictIndexHolder, sArr, m_iFileId, m_arlHdrShipmentList);
            }

            //  ProcessDictIndexHolder(dictIndexHolder);

        } // start processing end here..


        public string RemoveLineBreaker(object obj)
        {
            return BusinessUtility.GetString(obj).Substring(0, BusinessUtility.GetString(obj).Length - 1);
        }



        List<string> lstLines = new List<string>();
        List<string> lstHeader = new List<string>();
        List<string> lstDetailShipment = new List<string>();
        List<Orders> lstDetailOrder = new List<Orders>();
        List<string> lstDetailItem = new List<string>();
        List<string> lstTotalOrderItemDetail = new List<string>();


        public bool IsValidEdi(string[] sArr, string sStartLine, string sEndLine)
        {
            bool bReturn = false;
            if (sArr.Length == 1)
            {
                Configuration oConfiguration;

                oConfiguration = new Configuration();
                oConfiguration.PopulateConfigurationData(sArr);
                EdiFile = oConfiguration.FileType;


                var varLinesNew = sArr[0].Split(new string[] { oConfiguration.LineSeperator }, StringSplitOptions.None);
                sArr = varLinesNew;
                ErrorLog.createLog("oConfiguration.LineSeperator = " + oConfiguration.LineSeperator);
            }
            sArr = sArr.Where(x => !string.IsNullOrEmpty(x)).ToArray();
            if (sArr.Length > 0)
            {
                string sFirstLine = sArr[0].Substring(0, 3);
                string sLastLine = sArr[sArr.Length - 1].Substring(0, 3);
                bReturn = string.Equals(sFirstLine, sStartLine) && string.Equals(sLastLine, sEndLine);
                ErrorLog.createLog("sFirstLine = " + sFirstLine + "  &&  sLastLine = " + sLastLine );
            }

//            sFirstLine = ISA  &&  sLastLine = ISA

//sArr.Length = 1 && bReturn = False&&  sStartLine = ISA&&  sEndLine = IEA

            ErrorLog.createLog("sArr.Length = " + sArr.Length + " && bReturn = " + bReturn + "&&  sStartLine = " + sStartLine + "&&  sEndLine = " + sEndLine);
            return bReturn;
        }

    bool IsValidEdiData()
    {
        bool bReturn = false;
        //bool bHeatAvailable = false;
        try
        {
            var heat = lstTotalOrderItemDetail.FirstOrDefault(stringToCheck => stringToCheck.Contains("REF*HC*")) != null ? lstTotalOrderItemDetail.FirstOrDefault(stringToCheck => stringToCheck.Contains("REF*HC*")) : lstTotalOrderItemDetail.FirstOrDefault(stringToCheck => stringToCheck.Contains("*HN*"));
            var po = lstTotalOrderItemDetail.FirstOrDefault(stringToCheck => stringToCheck.Contains("PRF*"));

            if (heat != null)
            {
                if (oFiles.VendorName == Configuration.VENDORTYPE.NUCOR)
                {
                    heat = heat.Split('*')[7];
                }
                else
                {
                    heat = heat.Split('*')[2];
                }
            }

            if (po != null)
            {
                po = po.Split('*')[1];
            }
            bReturn = (!string.IsNullOrEmpty(heat)) && (!string.IsNullOrEmpty(po));
        }
        catch (Exception ex)
        {
            EdiError.LogError(EdiFile, m_iFileId.ToString(), oFiles.FileName, ex, "Error in IsValidEdiData");
                ErrorLog.CreateLog(ex);

            }
        return bReturn;
    }

        public void ParseOrderAndItemFromList(List<string> oList)
        {

            List<string> lstOrdItm = new List<string>(oList);
            //lstOrdItm = oList;

            List<string> lstPo = new List<string>();
            List<string> lstHeat = new List<string>();

            lstPo = oList.Where(x => x.Contains("PRF")).ToList();
            lstHeat = oList.Where(x => x.Contains("REF*HC")).ToList();
            if (lstPo.Count == lstHeat.Count)
            {
                for (int i = 0; i < lstPo.Count; i++)
                {
                    oOrders = new Orders();
                    oOrders.OrderSequence = i;
                    oOrders.PONumber = BusinessUtility.GetString(lstPo[i].Split('*')[1]);
                    oOrders.HeatNumber = BusinessUtility.GetString(lstHeat[i].Split('*')[2]);
                    lstDetailOrder.Add(oOrders);
                }
            }
            else if (lstPo.Count > lstHeat.Count)
            {
                for (int i = 0; i < lstPo.Count; i++)
                {
                    oOrders = new Orders();
                    oOrders.OrderSequence = i;
                    oOrders.PONumber = BusinessUtility.GetString(lstPo[i].Split('*')[1]);
                    //oOrders.HeatNumber = BusinessUtility.GetString(lstHeat[i].Split('*')[2]);
                    lstDetailOrder.Add(oOrders);
                }
            }


            List<string> lstOrder = new List<string>();
            lstOrder = oList.Where(x => x.EndsWith("O")).ToList();
            bool bOrderFound = false;
            int iOrderCount = 0;
            //oOrders.HeatNumber = BusinessUtility.GetString(lstHeat[i].Split('*')[2]);
            for (int i = 0; i < oList.Count; i++)
            {
                var obj = RemoveLineBreaker(oList[i]);
                bOrderFound = obj.EndsWith("O");
                if (bOrderFound)
                {
                    if (iOrderCount == 0)
                    {
                        lstOrdItm.Insert(i, "ORDER" + iOrderCount);
                    }
                    else
                    {
                        lstOrdItm.Insert(i + iOrderCount, "ORDER" + iOrderCount);
                    }

                    iOrderCount++;
                }

            }

            SeperateOrderFromList(lstOrdItm);



        }

        public void SeperateOrderFromList(List<string> olist)
        {
            int iOrderCount = 0;
            List<string> lstOrder = new List<string>();

            int iCounter = 0;
            foreach (string s in olist)
            {
                if (!s.Contains("ORDER"))
                {
                    lstOrder.Add(s);
                }

                if (s.Contains("ORDER") && iOrderCount == 0)
                {

                    iOrderCount++;
                }
                else if (s.Contains("ORDER") && iOrderCount > 0)
                {
                    dicOrder.Add(iCounter.ToString(), lstOrder);
                    lstOrder = new List<string>();
                    iCounter++;
                }
                else if (s.Contains("CTT") && iOrderCount > 0)
                {
                    dicOrder.Add(iCounter.ToString(), lstOrder);
                    lstOrder = new List<string>();
                    iCounter++;
                }


            }
        }


        public void ExecuteInsertIntoMysqlDB(int sbItm)
        {
            string sFileId = BusinessUtility.GetString(m_iFileId);
            
            oShipment.CusVenId = m_sCusVenId;
            oShipment.VenShpRef = oShipment.VenShpRef != string.Empty ? oShipment.VenShpRef : BusinessUtility.GetString(ConfigurationManager.AppSettings["VenShpRef"]); ;
            oShipment.CarrierVehicleDesc = BusinessUtility.GetString(ConfigurationManager.AppSettings["CarrierVehicleDesc"]);
            oShipment.DeliveryMethod = BusinessUtility.GetString(ConfigurationManager.AppSettings["DeliveryMethod"]);
            oShipment.PreRcptStatus = BusinessUtility.GetString(ConfigurationManager.AppSettings["PreRcptStatus"]);
            m_sbSql = new StringBuilder();
            iTECH.Library.DataAccess.MySql.DbHelper oDbHelper = new iTECH.Library.DataAccess.MySql.DbHelper(sMysqlConnectionString, true);
            try
            {
                if (m_iHeaderId > 0)
                {
                    int iStatus = 0;
                    if (lstDetailOrder.Count > 0)
                    {
                        var width = lstTotalOrderItemDetail.FirstOrDefault(stringToCheck => stringToCheck.Contains("MEA*PD*WD*"));
                        var weight = lstTotalOrderItemDetail.FirstOrDefault(stringToCheck => stringToCheck.Contains("MEA*PD*WT*")) != null ? lstTotalOrderItemDetail.FirstOrDefault(stringToCheck => stringToCheck.Contains("MEA*PD*WT*")) : lstTotalOrderItemDetail.FirstOrDefault(stringToCheck => stringToCheck.Contains("MEA*WT*WT*"));
                        var heat = lstTotalOrderItemDetail.FirstOrDefault(stringToCheck => stringToCheck.Contains("REF*HC*")) != null ? lstTotalOrderItemDetail.FirstOrDefault(stringToCheck => stringToCheck.Contains("REF*HC*")) : lstTotalOrderItemDetail.FirstOrDefault(stringToCheck => stringToCheck.Contains("*HN*"));

                        var ActGa1 = lstTotalOrderItemDetail.FirstOrDefault(stringToCheck => stringToCheck.Contains("MEA*PD*TH*"));  
                         
                        // mill id is vendor tag id in stratix application
                        var MillID = lstTotalOrderItemDetail.FirstOrDefault(stringToCheck => stringToCheck.Contains("REF*LS*")) != null ? lstTotalOrderItemDetail.FirstOrDefault(stringToCheck => stringToCheck.Contains("REF*LS*")) : lstTotalOrderItemDetail.FirstOrDefault(stringToCheck => stringToCheck.Contains("*SN*"));

                        var CoilLength = lstTotalOrderItemDetail.FirstOrDefault(stringToCheck => stringToCheck.Contains("MEA*PD*LN*"));
                        if (CoilLength != null)
                        {
                            var UOM = CoilLength.Split('*')[4];
                            CoilLength = CoilLength.Split('*')[3];
                            if (UOM.Equals(UNITOFMEASURMENT.LF))
                            {
                                CoilLength = (BusinessUtility.GetInt(CoilLength) * 12).ToString();
                            }
                        }

                        ErrorLog.createLog("CoilLength = " + CoilLength);


                        var po = lstTotalOrderItemDetail.FirstOrDefault(stringToCheck => stringToCheck.Contains("PRF*"));
                        if (po != null)
                        {
                            po = po.Split('*')[1];
                        }

                        tctipd otctipd = new tctipd(); // Check if live data exists for length and some prop
                        otctipd.GetPoDetails(BusinessUtility.GetInt(po));

                        ErrorLog.createLog("PO = " + po);
                        ErrorLog.createLog("otctipd.Length = " + otctipd.Length);
                        ErrorLog.createLog("otctipd.CutNumber = " + otctipd.CutNumber);
                        ErrorLog.createLog("otctipd.CoilLength = " + otctipd.CoilLength);
                        ErrorLog.createLog("otctipd.IDa = " + otctipd.IDa);
                        ErrorLog.createLog("otctipd.ODa = " + otctipd.ODa);
                        ErrorLog.createLog("otctipd.CoilLengthType = " + otctipd.CoilLengthType);

                            // Insert detail item code here..
                            oXCTI55 = new XCTI55();
                            oXCTI55.CmpyId = m_sCompanyId;
                            oXCTI55.IntchgPfx = m_sIntchgPfx;
                            oXCTI55.IntchgNo = BusinessUtility.GetString(ConfigurationManager.AppSettings["IntchgNo"]);
                            oXCTI55.IntchgItm = BusinessUtility.GetString(ConfigurationManager.AppSettings["IntchgItm"]);
                            oXCTI55.IntchgSitm = BusinessUtility.GetString(sbItm);
                            oXCTI55.Form = oDetails.Form;
                            oXCTI55.Grid = oDetails.Grade;
                            oXCTI55.Size = oDetails.Size;
                            oXCTI55.Finish = oDetails.Finish;
                            oXCTI55.EfEver = oDetails.ExtendedFinish;
                            if (BusinessUtility.GetDecimal(otctipd.Length) > 0)
                            {
                                oXCTI55.Length = CoilLength;
                               // oXCTI55.InvtMsr = CoilLength;
                            }
                       
                                //oXCTI55.Length = CoilLength;
                                oXCTI55.InvtMsr = CoilLength;
                    

                            if (!string.IsNullOrEmpty(otctipd.CoilLengthType))
                            {
                                oXCTI55.CoilLengthTyp = ConfigurationManager.AppSettings["CoilLengthType"].ToString();
                            }
                            else
                            {
                                oXCTI55.CoilLengthTyp = string.Empty;
                            }
                            
                            oXCTI55.DimDsgn = oDetails.DimDsgn;
                            oXCTI55.RjctRsn = BusinessUtility.GetString(ConfigurationManager.AppSettings["RjctRsn"]);
                            oXCTI55.InvtPcsTyp = BusinessUtility.GetString(ConfigurationManager.AppSettings["InvtPcsTyp"]);
                            oXCTI55.InvtMsrTyp = BusinessUtility.GetString(ConfigurationManager.AppSettings["InvtMsrTyp"]);
                            oXCTI55.InvtWgtTyp = BusinessUtility.GetString(ConfigurationManager.AppSettings["InvtWgtTyp"]);
                            oXCTI55.ShpntPcs = BusinessUtility.GetString(ConfigurationManager.AppSettings["ShpntPcs"]);
                            oXCTI55.ShpntMsr = BusinessUtility.GetString(ConfigurationManager.AppSettings["ShpntMsr"]); 
                            oXCTI55.ShpntPcsTyp = BusinessUtility.GetString(ConfigurationManager.AppSettings["ShpntPcsTyp"]); 
                            oXCTI55.ShpntMsrTyp = BusinessUtility.GetString(ConfigurationManager.AppSettings["ShpntMsrTyp"]); 
                            oXCTI55.ShpntWgtTyp = BusinessUtility.GetString(ConfigurationManager.AppSettings["ShpntWgtTyp"]); 
                            oXCTI55.TagNo = string.Empty;
                            oXCTI55.InvtPcs = BusinessUtility.GetString(ConfigurationManager.AppSettings["InvtPcs"]);
                            oXCTI55.InvtQlty = oDetails.InvtQlty;
                            oXCTI55.Loc = BusinessUtility.GetString(ConfigurationManager.AppSettings["Location"]);  
                            oXCTI55.Pkg = BusinessUtility.GetString(ConfigurationManager.AppSettings["Pkg"]);

                            oXCTI55.Mill = oFiles.Mill;
                            if (!string.IsNullOrEmpty(otctipd.CutNumber))
                            {
                                oXCTI55.CutNo = otctipd.CutNumber;
                            }
                            else
                            {
                                oXCTI55.CutNo = string.Empty;
                            }

                            //if (CoilLength != null && BusinessUtility.GetDecimal(otctipd.CoilLength) > 0)
                            //{
                            //    oXCTI55.CoilLength = CoilLength;
                            //}

                            if (CoilLength != null)
                            {
                                oXCTI55.CoilLength = CoilLength;
                            }

                            
                            if (weight != null)
                            {
                                oXCTI55.InvtWgt = weight;
                                oXCTI55.InvtWgt = oXCTI55.InvtWgt.Split('*')[3];
                                oXCTI55.ShpntWgt = oXCTI55.InvtWgt;
                            }
                            else
                            {
                                oXCTI55.InvtWgt = "1.0";
                            }
                            if (heat != null)
                            {
                                if (oFiles.VendorName == Configuration.VENDORTYPE.NUCOR)
                                {
                                    oXCTI55.Heat = heat.Split('*')[7];
                                }
                                else
                                {
                                    oXCTI55.Heat = heat.Split('*')[2];
                                }
                            }
                            if (ActGa1 != null)
                            {
                                if (oFiles.VendorName != Configuration.VENDORTYPE.NUCOR && oFiles.VendorName != Configuration.VENDORTYPE.NLMK)
                                {
                                    oXCTI55.ActGa1 = ActGa1;
                                    oXCTI55.ActGa1 = oXCTI55.ActGa1.Split('*')[3];
                                }
                            }

                            if (MillID != null)
                            {
                                if (MillID.StartsWith(EdiKeyWords.LIN))
                                {
                                    oXCTI55.MillId = MillID.Split('*')[5];
                                }
                                else
                                {
                                    oXCTI55.MillId = MillID.Split('*')[2];
                                }
                            }

                            if (!string.IsNullOrEmpty(otctipd.IDa))
                            {
                                oXCTI55.I55Id = otctipd.IDa;
                            }
                            if (!string.IsNullOrEmpty(otctipd.ODa))
                            {
                                oXCTI55.I55Od = otctipd.ODa;
                            }
                        

                            //this.GetMeasurment(BusinessUtility.GetInt(oDetails.PrntNo), ref oXCTI55);
                            oXCTI55.InsertDetailItems(m_iHeaderId, m_iDetailId, oXCTI55, oDbHelper);



                        //}

                        // }
                    }
                    if (iStatus > 0)
                    {
                        //STX856 oSTX856 = new STX856(m_iHeaderId);
                        //oSTX856.CompanyId = m_sCompanyId;
                        //oSTX856.IntchgPfx = m_sIntchgPfx;
                        //oSTX856.InterChangeNo = 0;
                        //oSTX856.RcptType = m_sRcptType;
                        //oSTX856.CusVenId = m_sCusVenId;
                        //oSTX856.CusVenShp = "0";
                        //oSTX856.StartInsert();
                    }
                }

            }
            catch (Exception ex)
            {
                ErrorLog.createLog(ex.ToString());
                EdiError.LogError(EdiFile, m_iFileId.ToString(), "", ex, "");
                ErrorLog.CreateLog(ex);
            }
            finally
            {
                oDbHelper.CloseDatabaseConnection();
            }


        }

        public void GetMeasurment(int iPoNumber, ref XCTI55 oXCTI55)
        {
            m_sSql = " select por_rcvd_pcs,por_rcvd_msr,por_rcvd_wgt from potpor_rec where por_po_no = '" + iPoNumber + "'";
            DbHelper oDbHelper = new DbHelper(sStratixConnectionString, true);
            try
            {
                DataTable oDataTable = oDbHelper.GetDataTable(m_sSql, CommandType.Text, null);
                if (oDataTable.Rows.Count > 0)
                {
                    oXCTI55.InvtPcs = BusinessUtility.GetString(oDataTable.Rows[0]["por_rcvd_pcs"]);
                    oXCTI55.InvtMsr = BusinessUtility.GetString(oDataTable.Rows[0]["por_rcvd_msr"]);
                    oXCTI55.InvtWgt = BusinessUtility.GetString(oDataTable.Rows[0]["por_rcvd_wgt"]);

                }
            }
            catch (Exception ex)
            {
                ErrorLog.createLog(ex.ToString());
                EdiError.LogError(EdiFile, m_iFileId.ToString(), "", ex, "");
                ErrorLog.CreateLog(ex);
            }
            finally
            {
                oDbHelper.CloseDatabaseConnection();
            }

        }

        public void GetPoDetails(int iPoNumber, ref Details oDetails)
        {

            tctipd otctipd = new tctipd(iPoNumber, m_iFileId);
            if (otctipd != null)
            {
                oDetails.Form = otctipd.Form;
                oDetails.Grade = otctipd.Grade;
                oDetails.Size = otctipd.Size;
                oDetails.Finish = otctipd.Finish;
                oDetails.GaSize = otctipd.GaSize;
                oDetails.GaType = otctipd.GaType;
                oDetails.DimDsgn = otctipd.DimDsgn;
                oDetails.ExtendedFinish = otctipd.ExtendedFinish;

                oDetails.IDa = otctipd.IDa;
                oDetails.ODa = otctipd.ODa;

                oDetails.InvtQlty = otctipd.InvtQlty;
                oDetails.Width = otctipd.Width;
                
            }
        }

        public void PrepareHlShipment(List<string> oList)
        {
            DbHelper oDbHelper = new DbHelper(sStratixConnectionString, true);
            try
            {
                //lstTotalOrderItemDetail

                var po = lstTotalOrderItemDetail.FirstOrDefault(stringToCheck => stringToCheck.Contains("PRF*"));
                if (po != null)
                {
                    sPoNo = po;
                    sPoNo = sPoNo.Split('*')[1];
                }

                oShipment.CompanyId = m_sCompanyId;
                m_sSql = "Select first 1 pod_shp_to_whs,pod_frt_ven_id from potpod_rec where pod_cmpy_id =  '" + oShipment.CompanyId + "' and pod_po_pfx= 'PO' and pod_po_no = '" + sPoNo + "'";
                ErrorLog.createLog(m_sSql);
                DataTable oDataTable = oDbHelper.GetDataTable(m_sSql, CommandType.Text, null);
                if (oDataTable.Rows.Count > 0)
                {
                    oShipment.RcvWhs = BusinessUtility.GetString(oDataTable.Rows[0][0]);
                    oShipment.FreightVenId = BusinessUtility.GetString(oDataTable.Rows[0][1]);
                    ErrorLog.createLog(oShipment.RcvWhs + " < whs  ,  frtvenid > " + oShipment.FreightVenId);
                }



                for (int i = 0; i < oList.Count; i++)
                {
                   // string[] sSegment = RemoveLineBreaker(oList[i]).Split('*');
                    string[] sSegment = (oList[i]).Split('*');
                    if (sSegment.Length > 2)
                    {
                        if (sSegment[0] == EdiKeyWords.TD5)
                        {
                            if (sSegment.Length >= 5)
                            {
                                oShipment.CarrierName = GetField(sSegment[3]);
                                oShipment.CarrierRefNo = GetField(sSegment[3]);
                            }
                        }
                        else if (sSegment[0] == EdiKeyWords.N1 && sSegment[1] == "ST")
                        {
                            if (sSegment.Length >= 3)
                            {
                                oShipment.ShipTo = GetField(sSegment[2]);
                            }
                        }
                        else if (sSegment[0] == EdiKeyWords.REF)
                        {
                            if (sSegment.Length >= 3)
                            {
                                oShipment.VenShpRef = GetField(sSegment[2]);
                                oDetails.VenShptRef = GetField(sSegment[2]);
                            }
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                ErrorLog.createLog("inside PrepareHlShipment");
                ErrorLog.createLog(ex.ToString());
                EdiError.LogError(EdiFile, m_iFileId.ToString(), "", ex, "inside PrepareHlShipment");
                ErrorLog.CreateLog(ex);
            }
            finally
            {
                oDbHelper.CloseDatabaseConnection();
            }
        }


        public void PrepareOrder(List<string> oList, int iStartIndex, int iEndIndex)
        {
            for (int i = 0; i <= oList.Count; i++)
            {
                if (i >= iStartIndex && i <= iEndIndex)
                {
                    string[] sSegment = RemoveLineBreaker(oList[i]).Split('*');
                    if (sSegment.Length > 0)
                    {
                        switch (sSegment[0])
                        {
                            case EdiKeyWords.LIN:
                                {
                                    oOrders.VenOrderId = GetField(sSegment[3]);
                                    break;
                                }
                            case EdiKeyWords.MEA:
                                {

                                    break;
                                }
                            case EdiKeyWords.PRF:
                                {
                                    oOrders.PurchaseOrderId = GetField(sSegment[1]);

                                    break;
                                }
                        }

                    }
                }
            }
        }

        public void PrepareItem(List<string> oList, int iStartIndex, int iEndIndex)
        {
            for (int i = 0; i <= oList.Count; i++)
            {
                if (i >= iStartIndex && i <= iEndIndex)
                {
                    string[] sSegment = RemoveLineBreaker(oList[i]).Split('*');
                    if (sSegment.Length > 0)
                    {
                        switch (sSegment[0])
                        {
                            case EdiKeyWords.LIN:
                                {

                                    break;
                                }
                            case EdiKeyWords.MEA:
                                {

                                    break;
                                }
                            case EdiKeyWords.SN1:
                                {
                                    oItems.NoOfUnitShipped = GetField(sSegment[2]);
                                    oItems.UnitOfMeasurement = GetField(sSegment[3]);
                                    break;
                                }
                        }

                    }
                }
            }
        }

        public string GetField(string sField)
        {
            string s = sField.Replace("~", string.Empty);
            // s = sField.Replace("...", string.Empty);
            return s;
        }

        public void AddElementBetweenArrayWithStartPosToList(ref List<string> oList, string[] sArr, int iStartPos, int iEndPos)
        {
            int iPos = 0;
            oList.Clear();
            foreach (var lineData in sArr)
            {
                if (iPos >= iStartPos && iPos < iEndPos)
                {
                   
                    oList.Add(RemoveLineBreaker(lineData));
                }
                iPos++;
            }
        }

        public void AddElementBetweenArrayWithStartEndPosToList(ref List<string> oList, string[] sArr, int iStartPos, int iEndPos)
        {
            int iPos = 0;
            oList.Clear();
            foreach (var lineData in sArr)
            {
                if (iPos >= iStartPos && iPos <= iEndPos)
                {
                   
                    oList.Add(RemoveLineBreaker(lineData));
                }
                iPos++;
            }
        }

        public void AddElementBetweenToList(ref List<string> oList, string[] sArr, int iStartPos, int iEndPos)
        {
            int iPos = 0;
            oList.Clear();
            foreach (var lineData in sArr)
            {
                if (iPos > iStartPos && iPos < iEndPos)
                {
                   
                    oList.Add(RemoveLineBreaker(lineData));
                }
                iPos++;
            }
        }

        public void CalculateHeaderLoop(ArrayList sArr)
        {
            if (!string.IsNullOrEmpty(BusinessUtility.GetString(sArr)))
            {
                string[] lineContent = null;
                int iPos = 0;
                foreach (string lineData in sArr)
                {
                    if (lineData.Contains(EdiKeyWords.BSN))  // BSN contains once
                    {
                        iHeaderStartIndex = iPos;
                    }

                    if (lineData.Contains(EdiKeyWords.HL)) // Hl of shipment = 1 always
                    {
                        // lineData.Substring(0, lineData.Length - 1); // substring is used here to eliminate last line breaker for each edi client
                        lineContent = RemoveLineBreaker(lineData).Split('*');
                        if (lineContent.Length > 0)
                        {
                            if (BusinessUtility.GetInt(lineContent[1]) == 1 && BusinessUtility.GetString(GetField(lineContent[3])) == "S")
                            {
                                iHeaderEndIndex = iPos;
                                iShipmentStartIndex = iPos;
                            }
                        }

                    }

                    if (lineData.Contains(EdiKeyWords.HL)) // Hl of order
                    {
                        lineContent = RemoveLineBreaker(lineData).Split('*');
                        if (lineContent.Length > 0)
                        {
                            if (BusinessUtility.GetInt(lineContent[1]) == 2 && BusinessUtility.GetString(GetField(lineContent[3])) == "O")
                            {
                                iShipmentEndIndex = iPos;
                            }
                        }

                    }

                    if (lineData.Contains(EdiKeyWords.HL)) // Hl count of order
                    {
                        lineContent = RemoveLineBreaker(lineData).Split('*');
                        if (lineContent.Length > 0)
                        {
                            if (BusinessUtility.GetString(GetField(lineContent[3])) == "O")
                            {
                                iTotalHlOrderCount++;
                            }
                        }

                    }



                    iPos++;
                }

                this.AddElementBetweenArrayWithStartPosToList(ref lstHeader, (string[])sArr.ToArray(typeof(string)), iHeaderStartIndex, iHeaderEndIndex);
                this.AddElementBetweenToList(ref lstDetailShipment, (string[])sArr.ToArray(typeof(string)), iShipmentStartIndex, iShipmentEndIndex);
                this.AddElementBetweenArrayWithStartEndPosToList(ref lstTotalOrderItemDetail, (string[])sArr.ToArray(typeof(string)), iShipmentEndIndex, m_sArr.Length); // Shipment end is start position of order

                this.PrepareHlShipment(lstDetailShipment);

                this.ParseOrderAndItemFromList(lstTotalOrderItemDetail);

                this.HeaderInsertion();


            }

        }

        public void CalculateHLoop(ArrayList sArr, int loopcount)
        {
            if (!string.IsNullOrEmpty(BusinessUtility.GetString(sArr)))
            {
                string[] lineContent = null;
                int iPos = 0;
                foreach (string lineData in sArr)
                {
                    if (lineData.Contains(EdiKeyWords.BSN))  // BSN contains once
                    {
                        iHeaderStartIndex = iPos;
                    }

                    if (lineData.Contains(EdiKeyWords.HL)) // Hl of shipment = 1 always
                    {
                        // lineData.Substring(0, lineData.Length - 1); // substring is used here to eliminate last line breaker for each edi client
                        lineContent = RemoveLineBreaker(lineData).Split('*');
                        if (lineContent.Length > 0)
                        {
                            if (BusinessUtility.GetInt(lineContent[1]) == 1 && BusinessUtility.GetString(GetField(lineContent[3])) == "S")
                            {
                                iHeaderEndIndex = iPos;
                                iShipmentStartIndex = iPos;
                            }
                        }

                    }

                    if (lineData.Contains(EdiKeyWords.HL)) // Hl of order
                    {
                        lineContent = RemoveLineBreaker(lineData).Split('*');
                        if (lineContent.Length > 0)
                        {
                            if (BusinessUtility.GetInt(lineContent[1]) == 2 && BusinessUtility.GetString(GetField(lineContent[3])) == "O")
                            {
                                iShipmentEndIndex = iPos;
                            }
                        }

                    }

                    if (lineData.Contains(EdiKeyWords.HL)) // Hl count of order
                    {
                        lineContent = RemoveLineBreaker(lineData).Split('*');
                        if (lineContent.Length > 0)
                        {
                            if (BusinessUtility.GetString(GetField(lineContent[3])) == "O")
                            {
                                iTotalHlOrderCount++;
                            }
                        }

                    }



                    iPos++;
                }

                this.AddElementBetweenArrayWithStartPosToList(ref lstHeader, (string[])sArr.ToArray(typeof(string)), iHeaderStartIndex, iHeaderEndIndex);
                this.AddElementBetweenToList(ref lstDetailShipment, (string[])sArr.ToArray(typeof(string)), iShipmentStartIndex, iShipmentEndIndex);
                this.AddElementBetweenArrayWithStartEndPosToList(ref lstTotalOrderItemDetail, (string[])sArr.ToArray(typeof(string)), iShipmentEndIndex, m_sArr.Length); // Shipment end is start position of order

                this.PrepareHlShipment(lstDetailShipment);

                this.ParseOrderAndItemFromList(lstTotalOrderItemDetail);

                this.ExecuteInsertIntoMysqlDB(loopcount);


            }

        }

        //Insert into heard in mysql
        public int HeaderInsertion()
        {
            string sFileId = BusinessUtility.GetString(m_iFileId);
            oShipment.CusVenId = m_sCusVenId;
            oShipment.VenShpRef = oShipment.VenShpRef != string.Empty ? oShipment.VenShpRef : "pre receipt";
            oShipment.CarrierVehicleDesc = BusinessUtility.GetString(ConfigurationManager.AppSettings["CarrierVehicleDesc"]);
            oShipment.DeliveryMethod = BusinessUtility.GetString(ConfigurationManager.AppSettings["DeliveryMethod"]);
            oShipment.PreRcptStatus = BusinessUtility.GetString(ConfigurationManager.AppSettings["PreRcptStatus"]);
            m_sbSql = new StringBuilder();
            int returnHeaderID = 0;
            iTECH.Library.DataAccess.MySql.DbHelper oDbHelper = new iTECH.Library.DataAccess.MySql.DbHelper(sMysqlConnectionString, true);
            try
            {
                m_sbSql.Append(" INSERT INTO HEADER(FileId, CusVenId, ShipTo, VenShpRef, CarrierVehicleDesc, RcvWhs, DeliveryMethod, FreightVenId, CarrierName, CarrierRefNo, PreRcptStatus)");
                m_sbSql.Append(" VALUES ('" + sFileId + "', '" + oShipment.CusVenId + "', '" + oShipment.ShipTo + "', '" + oShipment.VenShpRef + "', '" + oShipment.CarrierVehicleDesc + "', '" + oShipment.RcvWhs + "' ");
                m_sbSql.Append(" , '" + oShipment.DeliveryMethod + "', '" + oShipment.FreightVenId + "', '" + oShipment.CarrierName + "', '" + oShipment.CarrierRefNo + "', '" + oShipment.PreRcptStatus + "'   )  ");

                MySqlParameter[] oMySqlParameter = { };

                int iStatus = oDbHelper.ExecuteNonQuery(m_sbSql.ToString(), CommandType.Text, oMySqlParameter);
                iStatus = oDbHelper.GetLastInsertID();
                m_iHeaderId = iStatus;
                returnHeaderID = m_iHeaderId;

                if (lstDetailOrder.Count > 0)
                {
                    var width = lstTotalOrderItemDetail.FirstOrDefault(stringToCheck => stringToCheck.Contains("MEA*PD*WD*"));
                    var weight = lstTotalOrderItemDetail.FirstOrDefault(stringToCheck => stringToCheck.Contains("MEA*PD*WT*")) != null ? lstTotalOrderItemDetail.FirstOrDefault(stringToCheck => stringToCheck.Contains("MEA*PD*WT*")) : lstTotalOrderItemDetail.FirstOrDefault(stringToCheck => stringToCheck.Contains("MEA*WT*WT*"));
                    var length = lstTotalOrderItemDetail.FirstOrDefault(stringToCheck => stringToCheck.Contains("MEA*PD*LN*"));
                    string sWeight = string.Empty;
                    string sLength = string.Empty;

                    if (width != null)
                    {
                        oDetails.Width = width;
                        oDetails.Width = oDetails.Width.Split('*')[3];
                    }

                    if (weight != null)
                    {
                        sWeight = weight;
                        sWeight = sWeight.Split('*')[3];
                    }
                    else
                    {
                        sWeight = "1.0";
                    }
                    if (length != null)
                    {
                        sLength = length;
                        sLength = sLength.Split('*')[3];
                    }

                    oDetails.PrntNo = GetField(lstDetailOrder[0].PONumber);
                    oDetails.HeatNo = GetField(lstDetailOrder[0].HeatNumber);
                    oDetails.PrntItm = BusinessUtility.GetString(ConfigurationManager.AppSettings["PrntItm54"]);
                    oDetails.PrntSubItm = BusinessUtility.GetString(ConfigurationManager.AppSettings["PrntSItm54"]);
                    oDetails.ShptNo = BusinessUtility.GetString(ConfigurationManager.AppSettings["ShpmentNo54"]);
                    oDetails.OrigShpntPcs = BusinessUtility.GetString(ConfigurationManager.AppSettings["OrigShpntPcs54"]);
                    oDetails.OrigShpntMsr = BusinessUtility.GetString(ConfigurationManager.AppSettings["OrigShpntMsr54"]);
                    oDetails.OrigShptWgt = sWeight;
                    oDetails.ShpGrsPcs = BusinessUtility.GetString(ConfigurationManager.AppSettings["ShpGrsPcs54"]); ;
                    oDetails.ShpGrsMsr = BusinessUtility.GetString(ConfigurationManager.AppSettings["ShpGrsMsr54"]); ;
                    oDetails.ShpGrsWgt = sWeight;
                    oDetails.Length = sLength;
                   // oDetails.InvtQlty = BusinessUtility.GetString(ConfigurationManager.AppSettings["InvtQuality"]);             
                    oDetails.IntchgItm = BusinessUtility.GetString(BusinessUtility.GetInt(oDetails.IntchgItm) + 1);
                    this.GetPoDetails(BusinessUtility.GetInt(oDetails.PrntNo), ref oDetails);
                    
                    m_iDetailId = oDetails.InsertDetails(m_iHeaderId, oDetails, oDbHelper);
                }

            }
            catch (Exception ex)
            {
                ErrorLog.createLog(ex.ToString());
                EdiError.LogError(EdiFile, m_iFileId.ToString(), "", ex, "");
                ErrorLog.CreateLog(ex);
            }
            finally
            {
                oDbHelper.CloseDatabaseConnection();
            }
            return returnHeaderID;

        }
    }
}
