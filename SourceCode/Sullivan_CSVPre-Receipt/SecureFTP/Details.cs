﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using iTECH.Library;
using MySql.Data.MySqlClient;
using iTECH.Library.Utilities;
using System.Data;

namespace SecureFTP
{
    class Details
    {

        string m_sSql = string.Empty;
        StringBuilder m_sbSql = null;
        static string sStratixConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ToString();
        static string sMysqlConnectionString = ConfigurationManager.ConnectionStrings["NewConnectionString"].ToString();

        int m_iDetailsId = 0;

        string sPrntNo = string.Empty;
        public string PrntNo
        {
            get { return sPrntNo; }
            set { sPrntNo = value; }
        }

        string sPrntItm = string.Empty;
        public string PrntItm
        {
            get { return sPrntItm; }
            set { sPrntItm = value; }
        }

        string sPrntSubItm = string.Empty;
        public string PrntSubItm
        {
            get { return sPrntSubItm; }
            set { sPrntSubItm = value; }
        }

        string sCusRtnMthd = string.Empty;
        public string CusRtnMthd
        {
            get { return sCusRtnMthd; }
            set { sCusRtnMthd = value; }
        }

        string sShptNo = string.Empty;
        public string ShptNo
        {
            get { return sShptNo; }
            set { sShptNo = value; }
        }

        string sShptTagNo = string.Empty;
        public string ShptTagNo
        {
            get { return sShptTagNo; }
            set { sShptTagNo = value; }
        }

        string sVenShptRef = string.Empty;
        public string VenShptRef
        {
            get { return sVenShptRef; }
            set { sVenShptRef = value; }
        }

        string sOrigShpntPcs = string.Empty;
        public string OrigShpntPcs
        {
            get { return sOrigShpntPcs; }
            set { sOrigShpntPcs = value; }
        }

        string sOrigShpntMsr = string.Empty;
        public string OrigShpntMsr
        {
            get { return sOrigShpntMsr; }
            set { sOrigShpntMsr = value; }
        }

        string sOrigShptWgt = string.Empty;
        public string OrigShptWgt
        {
            get { return sOrigShptWgt; }
            set { sOrigShptWgt = value; }
        }

        string sShpGrsPcs = string.Empty;
        public string ShpGrsPcs
        {
            get { return sShpGrsPcs; }
            set { sShpGrsPcs = value; }
        }

        string sShpGrsMsr = string.Empty;
        public string ShpGrsMsr
        {
            get { return sShpGrsMsr; }
            set { sShpGrsMsr = value; }
        }

        string sShpGrsWgt = string.Empty;
        public string ShpGrsWgt
        {
            get { return sShpGrsWgt; }
            set { sShpGrsWgt = value; }
        }

        string sForm = string.Empty;
        public string Form
        {
            get { return sForm; }
            set { sForm = value; }
        }

        string sGrade = string.Empty;
        public string Grade
        {
            get { return sGrade; }
            set { sGrade = value; }
        }

        string sSize = string.Empty;
        public string Size
        {
            get { return sSize; }
            set { sSize = value; }
        }


        string sFinish = string.Empty;
        public string Finish
        {
            get { return sFinish; }
            set { sFinish = value; }
        }

        string sExtendedFinish = string.Empty;
        public string ExtendedFinish
        {
            get { return sExtendedFinish; }
            set { sExtendedFinish = value; }
        }

        string sWidth = "0.0";
        public string Width
        {
            get { return sWidth; }
            set { sWidth = value; }
        }

        string sLength = "0.0";
        public string Length
        {
            get { return sLength; }
            set { sLength = value; }
        }

        string sDimDsgn = string.Empty;
        public string DimDsgn
        {
            get { return sDimDsgn; }
            set { sDimDsgn = value; }
        }

        string sInnerDia = "0.0";
        public string InnerDia
        {
            get { return sInnerDia; }
            set { sInnerDia = value; }
        }


        string sOuterDia = "0.0";
        public string OuterDia
        {
            get { return sOuterDia; }
            set { sOuterDia = value; }
        }

        string sWallThck = "0.0";
        public string WallThck
        {
            get { return sWallThck; }
            set { sWallThck = value; }
        }

        string sGaSize = "0.0";
        public string GaSize
        {
            get { return sGaSize; }
            set { sGaSize = value; }
        }

        string sGaType = string.Empty;
        public string GaType
        {
            get { return sGaType; }
            set { sGaType = value; }
        }

        string sRndmDim1 = "0.0";
        public string RndmDim1
        {
            get { return sRndmDim1; }
            set { sRndmDim1 = value; }
        }

        string sRndmDim2 = "0.0";
        public string RndmDim2
        {
            get { return sRndmDim2; }
            set { sRndmDim2 = value; }
        }

        string sRndmDim3 = "0.0";
        public string RndmDim3
        {
            get { return sRndmDim3; }
            set { sRndmDim3 = value; }
        }

        string sRndmDim4 = "0.0";
        public string RndmDim4
        {
            get { return sRndmDim4; }
            set { sRndmDim4 = value; }
        }

        string sRndmDim5 = "0.0";
        public string RndmDim5
        {
            get { return sRndmDim5; }
            set { sRndmDim5 = value; }
        }

        string sRndmDim6 = "0.0";
        public string RndmDim6
        {
            get { return sRndmDim6; }
            set { sRndmDim6 = value; }
        }

        string sRndmDim7 = "0.0";
        public string RndmDim7
        {
            get { return sRndmDim7; }
            set { sRndmDim7 = value; }
        }

        string sRndmDim8 = "0.0";
        public string RndmDim8
        {
            get { return sRndmDim8; }
            set { sRndmDim8 = value; }
        }

        string sOfcRvw = "0";
        public string OfcRvw
        {
            get { return sOfcRvw; }
            set { sOfcRvw = value; }
        }

        string sSlsCat = string.Empty;
        public string SlsCat
        {
            get { return sSlsCat; }
            set { sSlsCat = value; }
        }

        string sHeatNo = string.Empty;
        public string HeatNo
        {
            get { return sHeatNo; }
            set { sHeatNo = value; }
        }

        string sIntchgItm = "0";
        public string IntchgItm
        {
            get { return sIntchgItm; }
            set { sIntchgItm = value; }
        }

        string sIntchgSItm = "0";
        public string IntchgSItm
        {
            get { return sIntchgSItm; }
            set { sIntchgSItm = value; }
        }

        string sInvtQlty = string.Empty;
        public string InvtQlty
        {
            get { return sInvtQlty; }
            set { sInvtQlty = value; }
        }

        string sIDa = string.Empty;
        public string IDa
        {
            get { return sIDa; }
            set { sIDa = value; }
        }

        string sODa = string.Empty;
        public string ODa
        {
            get { return sODa; }
            set { sODa = value; }
        }

        string sShipperNo = string.Empty;
        public string ShipperNo
        {
            get { return sShipperNo; }
            set { sShipperNo = value; }
        }

        int iHeaderId = 0;
        public int HeaderId
        {
            get { return iHeaderId; }
            set { iHeaderId = value; }
        }

        int iDetailId = 0;
        public int DetailId
        {
            get { return iDetailId; }
            set { iDetailId = value; }
        }

        int iMultiplePoItemExist = 0;
        public int MultiplePoItemExist
        {
            get { return iMultiplePoItemExist; }
            set { iMultiplePoItemExist = value; }
        }

        string si54_bgt_as_part = string.Empty;
        public string i54_bgt_as_part
        {
            get { return si54_bgt_as_part; }
            set { si54_bgt_as_part = value; }
        }

        public Details()
        { 

        }



        public int InsertDetails(int iHeader, Details oDetails, iTECH.Library.DataAccess.MySql.DbHelper oDbHelper)
        {
            int iReturn = 0;
            m_sbSql = new StringBuilder();
            m_sbSql.Append(" INSERT INTO detail(HeaderId, PrntNo, PrntItm, PrntSubItm, CusRtnMthd, ShptNo, ShptTagNo, VenShptRef, OrigShpntPcs, OrigShpntMsr, OrigShptWgt, ShpGrsPcs, ShpGrsMsr, ShpGrsWgt, ");
            m_sbSql.Append(" Form, Grade, Size, Finish, ExtendedFinish, Width, Length, DimDsgn, InnerDia, OuterDia, WallThck, GaSize, GaType, RndmDim1, RndmDim2, RndmDim3, RndmDim4, RndmDim5, RndmDim6, RndmDim7, RndmDim8, OfcRvw, SlsCat, IntchgItm, HeatNo, ShipperNo,MultiplePoItemExist)");
            m_sbSql.Append(" VALUES ('" + iHeader + "', '" + oDetails.PrntNo + "', '" + oDetails.PrntItm + "', '" + oDetails.PrntSubItm + "', '" + oDetails.CusRtnMthd + "', '" + oDetails.ShptNo + "' ");
            m_sbSql.Append(" , '" + oDetails.ShptTagNo + "', '" + oDetails.VenShptRef + "', '" + oDetails.OrigShpntPcs + "', '" + oDetails.OrigShpntMsr + "', '" + oDetails.OrigShptWgt + "'  ");
            m_sbSql.Append(" , '" + oDetails.ShpGrsPcs + "', '" + oDetails.ShpGrsMsr + "', '" + oDetails.ShpGrsWgt + "', '" + oDetails.Form + "', '" + oDetails.Grade + "'  ");
            m_sbSql.Append(" , '" + oDetails.Size + "', '" + oDetails.Finish + "', '" + oDetails.ExtendedFinish + "', '" + oDetails.Width + "', '" + oDetails.Length + "'  ");
            m_sbSql.Append(" , '" + oDetails.DimDsgn + "', '" + oDetails.InnerDia + "', '" + oDetails.OuterDia + "', '" + oDetails.WallThck + "', '" + oDetails.GaSize + "'  ");
            m_sbSql.Append(" , '" + oDetails.GaType + "', '" + oDetails.RndmDim1 + "', '" + oDetails.RndmDim2 + "', '" + oDetails.RndmDim3 + "', '" + oDetails.RndmDim4 + "', '" + oDetails.RndmDim5 + "'  ");
            m_sbSql.Append(" , '" + oDetails.RndmDim6 + "', '" + oDetails.RndmDim7 + "', '" + oDetails.RndmDim8 + "', '" + oDetails.OfcRvw + "', '" + oDetails.SlsCat + "' , '" + oDetails.IntchgItm + "' , '" + oDetails.HeatNo + "', '" + oDetails.ShipperNo + "', '" + oDetails.MultiplePoItemExist + "' )");

            ErrorLog.createLog("Deatil entry m_sbSql  = .." + m_sbSql.ToString());
            //iTECH.Library.DataAccess.MySql.DbHelper oDbHelper = new iTECH.Library.DataAccess.MySql.DbHelper(sMysqlConnectionString, true);
            try
            {
                MySqlParameter[] oMySqlParameter = { };
                iReturn = oDbHelper.ExecuteNonQuery(m_sbSql.ToString(), CommandType.Text, oMySqlParameter);
                iReturn = oDbHelper.GetLastInsertID();
            }
            catch (Exception ex)
            {
                ErrorLog.createLog(ex.ToString());
                EdiError.LogError("856", Globalcl.FileId.ToString(), "", ex, "InsertDetails ERROR",iHeader , 0, 0, 0, EdiError.ErrorLogType.ERROR);
                ErrorLog.CreateLog(ex);
            }
            finally
            {
                // oDbHelper.CloseDatabaseConnection();
                EdiError.ValidateFormGrdSize(oDetails.Form, oDetails.Grade, oDetails.Size, "INSIDE InsertDetails", BusinessUtility.GetString(Globalcl.FileId));
                EdiError.ValidatePO(oDetails.PrntNo, "INSIDE InsertDetails", BusinessUtility.GetString(Globalcl.FileId));
            }
            return iReturn;
        }


        public DataTable GetDetails(int iHeaderId)
        {
            DataTable dtDetails = new DataTable();
            iTECH.Library.DataAccess.MySql.DbHelper oDbHelper = new iTECH.Library.DataAccess.MySql.DbHelper(sMysqlConnectionString, true);
            try
            {
                //m_sSql = "SELECT * FROM detail WHERE HeaderId = '" + iHeaderId + "' AND MultiplePoItemExist = '1'";       // commented to get multiplePOItems (ref: whellingProject)  2020/03/04  Sumit
                m_sSql = "SELECT * FROM detail WHERE HeaderId = '" + iHeaderId + "'";
                dtDetails = oDbHelper.GetDataTable(m_sSql, CommandType.Text, null);
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLog(ex);
            }
            finally
            {
                oDbHelper.CloseDatabaseConnection();
            }
            return dtDetails;
        }

        public DataTable GetCustomerOwnDetails(int iHeaderId)
        {
            DataTable dtDetails = new DataTable();
            iTECH.Library.DataAccess.MySql.DbHelper oDbHelper = new iTECH.Library.DataAccess.MySql.DbHelper(sMysqlConnectionString, true);
            try
            {
                m_sSql = "SELECT * FROM detail WHERE HeaderId = '" + iHeaderId + "' ";
                dtDetails = oDbHelper.GetDataTable(m_sSql, CommandType.Text, null);
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLog(ex);
            }
            finally
            {
                oDbHelper.CloseDatabaseConnection();
            }
            return dtDetails;
        }


        public int GetBsnCount(int iFileId, string sBsn)
        {
            int iCnt = 0;
            iTECH.Library.DataAccess.MySql.DbHelper oDbHelper = new iTECH.Library.DataAccess.MySql.DbHelper(sMysqlConnectionString, true);
            try
            {
                m_sSql = " select  count(c.ShipperNo) as count from files a  inner join header b on a.fileid = b.fileid inner join detail c on b.HeaderId = c.headerid where b.FileId = " + iFileId + " AND c.ShipperNo = '" + sBsn + "' GROUP BY c.ShipperNo ";
                object obj = oDbHelper.GetValue(m_sSql, CommandType.Text, null);
                iCnt = BusinessUtility.GetInt(obj);
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLog(ex);
            }
            finally
            {
                oDbHelper.CloseDatabaseConnection();
            }
            return iCnt;
        }

        public int GetPoCountForCurrentBsn(int iFileId, string sBsn, string sPo)
        {
            int iCnt = 0;
            iTECH.Library.DataAccess.MySql.DbHelper oDbHelper = new iTECH.Library.DataAccess.MySql.DbHelper(sMysqlConnectionString, true);
            try
            {
                m_sSql = " select   count(c.PrntNo) as count from files a inner join header b on a.fileid = b.fileid inner join detail c on b.HeaderId = c.headerid where b.FileId = " + iFileId + " AND c.ShipperNo = '" + sBsn + "' AND c.prntNo = '" + sPo + "' GROUP BY c.ShipperNo ";
                object obj = oDbHelper.GetValue(m_sSql, CommandType.Text, null);
                iCnt = BusinessUtility.GetInt(obj);
                ErrorLog.createLog("GetPoCountForCurrentBsn = " + m_sSql);
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLog(ex);
            }
            finally
            {
                oDbHelper.CloseDatabaseConnection();
            }
            return iCnt;
        }


        public void GetDetailsForCurrentBsn(string sBsn, int iFileId , ref int iWhsId)
        {
            iTECH.Library.DataAccess.MySql.DbHelper oDbHelper = new iTECH.Library.DataAccess.MySql.DbHelper(sMysqlConnectionString, true);
            try
            {
                m_sSql = "  select  * from files a inner join header b on a.fileid = b.fileid inner join detail c on b.HeaderId = c.headerid  where b.FileId = " + iFileId + " AND c.ShipperNo = '" + sBsn + "' GROUP BY c.ShipperNo ";
                ErrorLog.createLog("GetDetailsForCurrentBsn sql = " + m_sSql);
                DataTable dt = oDbHelper.GetDataTable(m_sSql, CommandType.Text, null);
                foreach (DataRow oDataRow in dt.Rows)
                {
                    iHeaderId = BusinessUtility.GetInt(oDataRow["HeaderId"]);
                    iDetailId = BusinessUtility.GetInt(oDataRow["DetailId"]);
                    iWhsId = BusinessUtility.GetInt(oDataRow["RcvWhs"]); 
                }
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLog(ex);
            }
            finally
            {
                oDbHelper.CloseDatabaseConnection();
            }

        }

        public void GetDetailsItem(int iHeader, int iDetail)
        {
            iTECH.Library.DataAccess.MySql.DbHelper oDbHelper = new iTECH.Library.DataAccess.MySql.DbHelper(sMysqlConnectionString, true);
            try
            {
                m_sSql = "  select * from detailitem where headerid = " + iHeader + " and detailid = " + iDetail + " ";
                ErrorLog.createLog("GetDetailsForCurrentBsn sql = " + m_sSql);
                DataTable dt = oDbHelper.GetDataTable(m_sSql, CommandType.Text, null);
                foreach (DataRow oDataRow in dt.Rows)
                {
                    sIntchgItm = BusinessUtility.GetString(oDataRow["IntchgItm"]);
                    sIntchgSItm = BusinessUtility.GetString(oDataRow["IntchgSitm"]);
                }
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLog(ex);
            }
            finally
            {
                oDbHelper.CloseDatabaseConnection();
            }

        }


    }
}
