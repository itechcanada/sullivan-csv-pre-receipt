﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Tamir.SharpSsh;
using System.IO;
using System.Net;
using EnterpriseDT.Net.Ftp;
using System.Collections;

namespace SecureFTP
{
    public class FTP
    {
        public string Username { get; set; }
        public string Password { get; set; }
        public string Server { get; set; }
        public int Port { get; set; }
        public string FolderPath { get; set; }
        public string FilePathToUpload { get; set; }
        public string FilePathToDownload { get; set; }
        public string FtpDirectory { get; set; }
        public string LocalDownloadDir { get; set; }

        

        public bool FileUpdloadFTP()
        {
            try
            {
                //Working
                FileInfo fileinfo = new FileInfo(FilePathToUpload);
                FTPConnection ftpconn = new FTPConnection();
                ftpconn.ServerAddress = Server;
                ftpconn.ServerPort = Port;
                ftpconn.UserName = Username;
                ftpconn.Password = Password;
                ftpconn.Connect();
                ftpconn.UploadFile(FilePathToUpload, String.Format("{0}/{1}", FolderPath, fileinfo.Name));
                ftpconn.Close();
                ftpconn = null;
                return true;
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLog(ex);
                throw ex;
            }

        }

        public void DownloadFileFRomFTP()
        {
            FTPConnection ftpconn = new FTPConnection();
            ftpconn.ServerAddress = Server;
            ftpconn.ServerPort = Port;
            ftpconn.UserName = Username;
            ftpconn.Password = Password;
            ftpconn.Connect();
            ftpconn.DownloadFile(LocalDownloadDir, FilePathToDownload);
            ftpconn.Close();
            ftpconn = null;
            
        }

        public ArrayList DownloadAllFilesFromFTP()
        {
            FTPConnection ftpconn = new FTPConnection();
            ftpconn.ServerAddress = Server;
            ftpconn.ServerPort = Port;
            ftpconn.UserName = Username;
            ftpconn.Password = Password;
            ftpconn.Connect();

            ArrayList oList = new ArrayList();
            string[] sFilear = ftpconn.GetFiles(FtpDirectory);
              foreach(string s in sFilear)
            {
                FileInfo fileinfo = new FileInfo(s);
                ftpconn.DownloadFile(LocalDownloadDir + "\\" + fileinfo.Name, s);
                oList.Add(fileinfo.Name);
            }
            ftpconn.Close();
            ftpconn = null;
            return oList;
        }
        
        public void DeleteRemoteFiles(ArrayList olist)
        {
            try
            {
                FTPConnection ftpconn = new FTPConnection();
                ftpconn.ServerAddress = Server;
                ftpconn.ServerPort = Port;
                ftpconn.UserName = Username;
                ftpconn.Password = Password;
                ftpconn.Connect();
                foreach (string s in olist)
                {
                    ftpconn.DeleteFile(FtpDirectory + "\\" + s);
                }
                // close connection after work  2020/03/12   Sumit
                ftpconn.Close();
                ftpconn = null;
            }
            catch(Exception ex)
            {
                ErrorLog.CreateLog(ex);
            }

        }



    }
}
