﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SecureFTP.VendorFiltration
{
    static class StringMathRound 
    {
        public static string GetRoundedString(string strValue)
        {
            decimal temp;
            decimal.TryParse(strValue, out temp);
            temp = Math.Round(temp);
            return Convert.ToString(temp);
        }

        public static bool isNumericString(string strValue)
        {
            bool isValid = true;
            try
            {
                for(int i=0;i< strValue.Length;i++)
                {
                    if (!System.Text.RegularExpressions.Regex.IsMatch(strValue.Substring(i, 1), "[^0-9]"))
                        isValid = false;
                }
            }
            catch(Exception ex)
            {
                ErrorLog.createLog("Error in identify string is mumeric or not." + ex.Message.ToString());
            }
            return isValid;
        }
    }
}
