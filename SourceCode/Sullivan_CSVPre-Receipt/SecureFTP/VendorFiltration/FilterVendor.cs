﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SecureFTP.VendorFiltration
{
    public class FilterVendor : IFilterVendor
    {
        DataTable VendorList;
        string filterList;
        public FilterVendor(DataTable _vendorList, string _filterList)
        {
            this.VendorList = _vendorList;
            this.filterList = _filterList;
        }

        public DataTable VendorDunsNoList
        {
            get => VendorList;
            set => VendorList = value;
        }

        public DataTable FilterVendorList()
        {
            string[] vendorDunsList;
            string[] TobeDistinct = { "VendorDunsNo", "vendorId", "EdiDataSourceId" };
            try
            {

                VendorList = GetDistinctRecords(VendorList, TobeDistinct);

                vendorDunsList = filterList.Split(',');
                if (vendorDunsList.Length > 0)
                {
                    for (int i = 0; i < VendorList.Rows.Count; i++)
                    {
                        if (!vendorDunsList.Contains(Convert.ToString(VendorList.Rows[i]["VendorDunsNo"])))
                        {
                            VendorList.Rows.RemoveAt(i);
                            --i;
                        }
                            
                    }
                }
            }
            catch (Exception ex)
            {
                EdiError.LogError("", "", "FilterVendor", ex, "");
                ErrorLog.createLog("Error In VendorDunsNo Filtration. Error Description :-");
                ErrorLog.CreateLog(ex);
            }
            return VendorList;
        }

        //Following function will return Distinct records for Name, City and State column.
        public static DataTable GetDistinctRecords(DataTable dt, string[] Columns)
        {
            DataTable dtUniqRecords = new DataTable();
            dtUniqRecords = dt.DefaultView.ToTable(true, Columns);
            return dtUniqRecords;
        }

    }
}
