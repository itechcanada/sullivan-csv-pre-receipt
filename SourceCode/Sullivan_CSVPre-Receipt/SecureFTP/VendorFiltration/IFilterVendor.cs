﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SecureFTP.VendorFiltration
{
    interface IFilterVendor
    {
        DataTable VendorDunsNoList { get; set; }
        DataTable FilterVendorList();
    }
}
