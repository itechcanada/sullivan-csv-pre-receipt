﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SecureFTP.VendorFiltration
{
    interface IMathRound
    {
        string GetRoundedString(string strValue);
        
    }
}
