﻿using iTECH.Library.Utilities;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Xml;

namespace SecureFTP
{
    class Edi863XmlParser
    {
        public static string m_XmlData = string.Empty;
        MyXmlReader m_fullXml = null;
        public static XmlNodeList m_HeaderXmlNodeList = null;
        public static XmlNodeList m_DetailXmlNodeList = null;
        public static XmlNodeList m_ItemXmlNodeList = null;
        MyXmlReader omyXmlReader = null;
        int m_iFileId = 0;
        Files oFiles = null;
        int m_iHeaderId = 0;
        string m_s863Content = string.Empty;

        public Edi863XmlParser(XmlNodeList HeaderXmlNodeList, XmlNodeList BodyXmlNodeList, int iFileId, string s863List)
        {
            m_iFileId = iFileId;
            m_HeaderXmlNodeList = HeaderXmlNodeList;
            m_DetailXmlNodeList = BodyXmlNodeList;
            m_s863Content = s863List;
            oFiles = new Files(m_iFileId);
            try
            {
                ParseData();
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLog(ex);
            }
        }


        public void ParseData()
        {
            try
            {
                foreach (XmlElement oNode in m_DetailXmlNodeList)
                {
                    if (m_ItemXmlNodeList == null)
                    {
                        XmlElement onodelist = oNode;
                        m_s863Content = onodelist.OuterXml.Replace("ch863:", string.Empty);
                        omyXmlReader = new MyXmlReader(m_s863Content);
                        m_ItemXmlNodeList = omyXmlReader.GetNodeList("Transaction/Certification/Lines/Line");
                    }
                }
                ParseDeatil();
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLog(ex);
            }
        }

        string m_sBol = string.Empty;

        public void ParseDeatil()
        {
            header863 oheader863 = new header863();
            foreach (XmlElement oNode in m_ItemXmlNodeList)
            {
                //foreach (XmlElement oNode1 in m_HeaderXmlNodeList)
                //{
                XmlElement onodelist = oNode;
                omyXmlReader = new MyXmlReader(onodelist.OuterXml);

                m_sBol = omyXmlReader.GetNodeAttribute("Line", 0, "LoadNumber");
                oheader863.ShipingRef = m_sBol;

                // XmlElement oItemnodelist = oNode;
                //omyXmlReader = new MyXmlReader(oItemnodelist.OuterXml);
                //oheader863.MillOrdNo = omyXmlReader.GetNode("Item/CustomerPurchaseOrder").InnerText;
                string sPurchaseOrderNum = omyXmlReader.GetNodeAttribute("Line", 0, "PONumber");
                oheader863.VendorTagId = omyXmlReader.GetNodeAttribute("Line", 0, "TagNumber");
                if (oheader863.VendorTagId.Length > 15)
                {
                    oheader863.VendorTagId = oheader863.VendorTagId.Replace("18109254500", string.Empty);
                }

                oheader863.OrigPoNo = sPurchaseOrderNum.Trim();                                         //to support po number without any prefix or suffix     2020/04/02  Sumit
                string[] sPoAndItemarr = Utility.SplitString(sPurchaseOrderNum, '-');
                if (sPoAndItemarr.Length > 1)
                {
                    oheader863.OrigPoNo = sPoAndItemarr[1];
                    // oheader863.OrigPoItem = sPoAndItemarr[0];
                }

                oheader863.OrigPoItem = omyXmlReader.GetNodeAttribute("Line", 0, "LineNumber");     // to attach product and test qds with PO Items and product items   2020/03/04  Sumit
                oheader863.Heat = omyXmlReader.GetNodeAttribute("Line", 0, "HeatNumber");
                oheader863.Type = Utility.GetConfigValue("QdsType");
                oheader863.UknownHeat = Utility.GetConfigValue("UknownHeat");
                oheader863.VenId = oFiles.VendorID;
                oheader863.Mill = oFiles.Mill;
                tctipd.GetPoDetails(BusinessUtility.GetInt(oheader863.OrigPoNo), ref oheader863);
                m_iHeaderId = oheader863.InsertHeader(m_iFileId);
                // break;
                // }



                this.InsertTestResults(m_iHeaderId, oNode);
                //Detail863 oDetail863 = new Detail863();
                //int iDtId = oDetail863.InsertHeaderDetailMetalStanderd(m_iHeaderId, m_iFileId, BusinessUtility.GetInt(oheader863.OrigPoNo));
                //if (m_iHeaderId > 0)
                //{
                //     this.InvokeWebMethodForQDS(m_iFileId, m_iHeaderId, oheader863);
                //}
            }
        }


        public static string TruncateLongString(string str, int maxLength)
        {
            if (string.IsNullOrEmpty(str))
                return str;
            return str.Substring(0, Math.Min(str.Length, maxLength));
        }


        public void InvokeWebMethodForQDS(int iFileId, int iHeaderId, header863 oheader863)
        {
            try
            {
                QdsService oQdsService = new QdsService(iFileId, iHeaderId);
                oQdsService.CreateQDS();
            }
            catch (Exception ex)
            {
                EdiError.LogError(oFiles.FileType.ToString(), m_iFileId.ToString(), "", ex, "Error in InvokeWebMethodForQDS");
                ErrorLog.CreateLog(ex);
            }
        }


        public void InsertTestResults(int iHeaderId, XmlElement oDetailNode)
        {
            bool isChemistry = false;           // to check only first node insertion of chemistry  2020/03/26
            bool isTests = false;               // to check only first node insertion of tests      2020/03/26
            try
            {
                omyXmlReader = new MyXmlReader(oDetailNode.OuterXml);
                XmlNodeList oMaterialTests = omyXmlReader.GetNodeList("Line/MillTestResults/Measurements");
                XmlNodeList oActualTestResults = omyXmlReader.GetNodeList("Item/Heat/TestResults/MechanicalProperties/TensileTests/TensileTest/TestResult");
                foreach (XmlElement oElem in oMaterialTests)
                {
                    MyXmlReader oMyXmlReader = new MyXmlReader(oElem.OuterXml);
                    string sTestType = oMyXmlReader.GetNodeAttribute("Measurements", 0, "TypeCode");
                    if (sTestType.Contains("68")) // chemistry
                    {
                        if (!isChemistry)
                        {
                            ParseChemistry(iHeaderId, oElem);
                            isChemistry = true;
                        }
                    }
                    else if (sTestType.Contains("71"))
                    {
                        if (!isTests)
                        {
                            ParseTest(iHeaderId, oElem);
                            isTests = true;
                        }
                    }
                    else
                    {
                        ErrorLog.createLog("No MTA found in file" + Globalcl.FileId);
                        Console.WriteLine("No MTA found in file" + Globalcl.FileId);
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorLog.createLog(ex.Message.ToString());
                EdiError.LogError("863", m_iFileId.ToString(), oFiles.FileName, ex, ex.Message.ToString());
            }
        }


        public void ParseChemistry(int iHeaderId, XmlElement oDetailNode)
        {
            try
            {
                MyXmlReader oMyXmlReader = new MyXmlReader(oDetailNode.OuterXml);
                XmlNodeList oMaterialTests = oMyXmlReader.GetNodeList("Measurements/Result");
                Console.WriteLine("Processing chemistry");
                foreach (XmlElement oNode in oMaterialTests)
                {
                    Detail863 oDetail863 = new Detail863();
                    MyXmlReader oReader = new MyXmlReader(oNode.OuterXml);

                    int iSequence = 0;
                    int iChemDefaultVal = 0;
                    #region to fetch typecode from xml and pass it as ediText 2020/02/27 Sumit
                    //string sCodeValue = oReader.GetNodeAttribute("Result", 0, "MeasDesc");
                    string sCodeValue = oReader.GetNodeAttribute("Result", 0, "TypeCode");
                    #endregion to fetch typecode from xml and pass it as ediText 2020/02/27 Sumit
                    //string sDesc = oReader.GetNode("ActualTestResults/MeasQual/Desc").InnerText;
                    string sMeasValue = "";
                    string sUOMCodeValue = "";
                    string sUOMDesc = "";


                    sMeasValue = oReader.GetNodeChild("Result", 0, "Value");
                    XmlNode oValueNode = oReader.GetNode("Result/Value");

                    oReader = new MyXmlReader(oValueNode.OuterXml);
                    sUOMCodeValue = oReader.GetNodeAttribute("Value", 0, "UOM");
                    sUOMDesc = sUOMCodeValue;

                    string sChemElement = oDetail863.GetStratixChemicalElement(BusinessUtility.GetString(sCodeValue), ref iSequence, ref iChemDefaultVal); //lovekesh
                    //check if current elem existsin file and header
                    //ErrorLog.createLog("Chemistry block");

                    Detail863 objDetail863 = new Detail863();

                    if (!string.IsNullOrEmpty(sChemElement))
                    {
                        //ErrorLog.createLog("start insert chem" + sChemElement);
                        oDetail863.ClassID = Detail863.Characteristic863.CHEMISTRY;
                        oDetail863.ChemistrySequence = iSequence;
                        oDetail863.ChemistrylElement = sChemElement;
                        oDetail863.ChemistryElementVAlue = BusinessUtility.GetString(sMeasValue);
                        oDetail863.ChmEntryType = "V";
                        oDetail863.FileId = m_iFileId;
                        oDetail863.InsertHeaderDetailChemistry(iHeaderId, m_iFileId);
                    }
                    else
                    {
                        ErrorLog.createLog("Chemistry StratixText not found. Class ID: " + Detail863.Characteristic863.CHEMISTRY + " Header ID: " + iHeaderId + " Type Code: " + sCodeValue);
                    }

                } // chemistry block end
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLog(ex);
            }
        }


        public void ParseTest(int iHeaderId, XmlElement oDetailNode)
        {
            try
            {

                MyXmlReader oMyXmlReader = new MyXmlReader(oDetailNode.OuterXml);
                XmlNodeList oMaterialTests = oMyXmlReader.GetNodeList("Measurements/Result");
                Console.WriteLine("Processing Tests");
                foreach (XmlElement oNode in oMaterialTests)
                {
                    MyXmlReader oReader = new MyXmlReader(oNode.OuterXml);
                    Detail863 oDetail863 = new Detail863();
                    bool bTestMethodFound = false;
                    string sTestMethodDescription = string.Empty;
                    string sTestProductDescription = string.Empty;
                    string sTestCode = string.Empty;  //TestAbbr is test code
                    string sDirection = string.Empty;
                    string sTestEntityType = string.Empty;

                    string sTestabbr = string.Empty;
                    string sTestUm = string.Empty;
                    bool bUOMMissing = false;

                    string sCodeValue = string.Empty;
                    string sDesc = string.Empty;
                    string sMeasValue = string.Empty;
                    string sUOMCodeinEdiFile = "";

                    sCodeValue = oReader.GetNodeAttribute("Result", 0, "TypeCode");

                    string sUOMCodeValue = "";
                    string sUOMDesc = "";


                    sMeasValue = oReader.GetNodeChild("Result", 0, "Value");
                    XmlNode oValueNode = oReader.GetNode("Result/Value");

                    oReader = new MyXmlReader(oValueNode.OuterXml);
                    sUOMCodeValue = oReader.GetNodeAttribute("Value", 0, "UOM");
                    sUOMDesc = sUOMCodeValue;


                    sUOMCodeinEdiFile = sUOMCodeValue;
                    oDetail863.ClassID = Detail863.Characteristic863.MECHANICAL;
                    sDirection = "L";
                    DataTable dtTest = oDetail863.GetTestAbbrivation(sCodeValue, oFiles.VendorId);  //lovekesh
                    if (dtTest.Rows.Count > 0)
                    {
                        if ((BusinessUtility.GetDecimal(sMeasValue) > 0 && !string.IsNullOrEmpty(sMeasValue)))
                        {
                            foreach (DataRow oDataRow in dtTest.Rows)
                            {
                                sTestabbr = BusinessUtility.GetString(oDataRow["TestCode"]);
                                sTestEntityType = BusinessUtility.GetString(oDataRow["TestEntityType"]);
                                sTestUm = BusinessUtility.GetString(oDataRow["UOM"]);
                            }
                            ErrorLog.createLog("sUOMCodeinEdiFile = " + sUOMCodeinEdiFile);
                            Measurment oMeasurment = new Measurment(oFiles.VendorId, sUOMCodeinEdiFile);
                            if (string.IsNullOrEmpty(oMeasurment.EdiUOM) || string.IsNullOrEmpty(oMeasurment.MulVal))            //To Insert stratix UOM and multiplication number 2020/02/25 Sumit
                            {
                                ErrorLog.createLog("Stratix UOM not configured in Measurment. Vendor ID: " + oFiles.VendorId + " Class ID: " + oDetail863.ClassID + " Code: " + sCodeValue + "  File UOM: " + sUOMCodeinEdiFile);
                            }
                            else
                            {
                                oDetail863.TestAbbr = sTestabbr;
                                oDetail863.ClassID = Detail863.Characteristic863.MECHANICAL;
                                oDetail863.TestEntityType = sTestEntityType;
                                oDetail863.TestProdDesc = sTestProductDescription;  // code based on we get code of test
                                oDetail863.TestDirection = sDirection;
                                //oDetail863.TestUM = BusinessUtility.GetString(lineContent[4]);
                                #region to get stratix UOM from measurment table 2020/02/25 Sumit
                                //oDetail863.TestUM = sTestUm;
                                oDetail863.TestUM = oMeasurment.EdiUOM;
                                #endregion to get stratix UOM from measurment table 2020/02/25 Sumit
                                if (sTestEntityType.Equals("V"))
                                {
                                    string sVal = BusinessUtility.GetDouble(sMeasValue).ToString("N2");

                                    if (!String.IsNullOrEmpty(oMeasurment.MulVal))
                                    {
                                        ErrorLog.createLog("oMeasurment.MulVal = " + oMeasurment.MulVal);
                                        oDetail863.TestVal = BusinessUtility.GetDouble((BusinessUtility.GetDecimal(sVal) * BusinessUtility.GetDecimal(oMeasurment.MulVal))).ToString("N2");
                                        bUOMMissing = false;
                                    }
                                    else
                                    {
                                        bUOMMissing = true;
                                        oDetail863.TestVal = sVal;
                                    }

                                }
                                else if (sTestEntityType.Equals("A"))
                                {
                                    oDetail863.TestAlpha = sMeasValue;
                                }
                                oDetail863.InsertHeaderDetailMechanical(iHeaderId, m_iFileId);
                            }

                        }
                    }
                    else
                    {
                        ErrorLog.createLog("No test available in database. Vendor ID: " + oFiles.VendorId + " Class ID: " + oDetail863.ClassID + " Code: " + sCodeValue);
                    }

                }
                // } // // test block end
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLog(ex);
            }
        }

    }
}

public class TestFormat
{
    public String TestCode { get; set; }
    public String TestUm { get; set; }
    public String TestValue { get; set; }
}