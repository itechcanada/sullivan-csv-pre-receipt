﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using iTECH.Library.Utilities;
using System.Collections;
using System.Configuration;
using iTECH.Library.DataAccess.ODBC;
using System.Data;

namespace SecureFTP
{
    class Edi863Parser
    {
        static string m_sCusVenId = ConfigurationManager.AppSettings["CusVenId"];
        static string sStratixConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ToString();
        string[] m_sArr = null;
        Dictionary<int, ArrayList> m_dict = new Dictionary<int, ArrayList>();
        Dictionary<int, ArrayList> m_dictItem = new Dictionary<int, ArrayList>();
        ArrayList m_arl = new ArrayList();
        Configuration oConfiguration = null;

        static string m_sStartBlock = "ST";
        static string m_sEndBlock = "SE";
        static string m_sInterchangeControlTrailer = "IEA";
        static int m_iFileType = BusinessUtility.GetInt(ConfigurationManager.AppSettings["EdiFile"]);
        static string m_sCompanyId = ConfigurationManager.AppSettings["CompanyId"];

        int iChemistryStartIndex = 0;
        int iChemistryEndIndex = 0;

        int iMechanicalStartIndex = 0;
        int iMechanicalEndIndex = 0;

        string m_sSql = string.Empty;
        StringBuilder m_sbSql = null;

        int m_iFileId = 0;

        Files oFiles;

        string m_sHeat = string.Empty;
        string m_sPo = string.Empty;

        string m_sMill = string.Empty;

        public struct EdiKeyWords
        {
            public const string CTT = "CTT";
            public const string BSN = "BSN";
            public const string DTM = "DTM";
            public const string TD1 = "TD1";
            public const string TD5 = "TD5";
            public const string TD3 = "TD3";
            public const string REF = "REF";
            public const string N1 = "N1";
            public const string LIN = "LIN";
            public const string HL = "HL";
            public const string PRF = "PRF";
            public const string MEA = "MEA";
            public const string SN1 = "SN1";
            public const string CID = "CID";
            public const string TMD = "TMD";
            public const string PSD = "PSD";
            public const string HN = "HN";  // HN is for heat
            public const string SN = "SN";  // SN is for ven tag
            public const string SE = "SE";

        }


        // CID segment is Characteristic like chemistry and mechanical.
        public struct Characteristic863
        {
            public const int CHEMISTRY = 68;
            public const int MECHANICAL = 71;
        }

        public Edi863Parser()
        {
        }

        public Edi863Parser(string[] obj, int iFileID)
        {
            m_iFileId = iFileID;
            m_sArr = obj;
           // int i = Globalcl.FileId;
            m_sArr = m_sArr.Where(x => !string.IsNullOrEmpty(x)).ToArray(); // removing empty segment 
            try
            {
                oFiles = new Files( iFileID);
                oConfiguration = new Configuration();
                oConfiguration.PopulateConfigurationData(m_sArr);
                m_iFileType = BusinessUtility.GetInt(oConfiguration.FileType);

                this.StartProcessing();
                this.ExtractInfoToMysql();
            }
            catch (Exception ex)
            {
                ErrorLog.createLog("Error occur in Edi863Parser constructor : " + ex.ToString());
                EdiError.LogError(BusinessUtility.GetString(m_iFileType), m_iFileId.ToString(), "", ex, "");
                ErrorLog.CreateLog(ex);
            }
        }

        public string RemoveLineBreaker(object obj)
        {
            return BusinessUtility.GetString(obj).Substring(0, BusinessUtility.GetString(obj).Length - 1);
        }

        public string GetField(string sField)
        {
            string s = sField.Replace("~", string.Empty);
            // s = sField.Replace("...", string.Empty);
            return s;
        }

        bool IsValidEdiData()
        {
            bool bReturn = false;
            //bool bHeatAvailable = false;
            try
            {
                bReturn = (!string.IsNullOrEmpty(m_sHeat)) && (!string.IsNullOrEmpty(m_sPo));
            }
            catch (Exception ex)
            {
                EdiError.LogError(m_iFileType.ToString(), m_iFileId.ToString(), oFiles.FileName, ex, "Error in IsValidEdiData");
                ErrorLog.CreateLog(ex);
            }
            return bReturn;
        }


        public void StartProcessing()
        {
            bool bStartBlockFound = false;
            bool bEndBlockFound = false;
            bool bInterchangeControlTrailer = false;
            string sSegment = string.Empty;
            int iNoOfblock = 0;
            bool bInEndBlock = false;
            try
            {
                for (int i = 0; i < m_sArr.Length; i++)
                {
                    sSegment = BusinessUtility.GetString(m_sArr[i]);
                    bStartBlockFound = sSegment.Substring(0, 2).Equals(m_sStartBlock);
                    bEndBlockFound = sSegment.Substring(0, 2).Equals(m_sEndBlock);
                    bInterchangeControlTrailer = sSegment.Substring(0, 3).Equals(m_sInterchangeControlTrailer);

                    if (!bStartBlockFound)
                    {
                        m_arl.Add(sSegment);
                    }
                    if (bInEndBlock)
                    {
                        m_arl.Add(sSegment);
                        bInEndBlock = false;
                    }
                    if (bStartBlockFound && iNoOfblock == 0)
                    {

                        if (!m_dict.ContainsKey(iNoOfblock))
                        {
                            m_dict.Add(iNoOfblock, m_arl);
                        }
                        else
                        {
                            m_dict.Add(iNoOfblock + 1, m_arl);
                        }
                        iNoOfblock++;
                        m_arl = new ArrayList();
                        m_arl.Add(sSegment);
                    }
                    else if (bEndBlockFound)
                    {
                        bInEndBlock = true;
                        if (!m_dict.ContainsKey(iNoOfblock))
                        {
                            m_dict.Add(iNoOfblock, m_arl);
                        }
                        else
                        {
                            m_dict.Add(iNoOfblock + 1, m_arl);
                        }
                        iNoOfblock++;
                        m_arl = new ArrayList();
                        //m_arl.Add(sSegment);
                    }
                    else if (bInterchangeControlTrailer)
                    {
                        if (!m_dict.ContainsKey(iNoOfblock))
                        {
                            m_dict.Add(iNoOfblock, m_arl);
                        }
                        else
                        {
                            m_dict.Add(iNoOfblock + 1, m_arl);
                        }
                        iNoOfblock++;
                        bInEndBlock = false;
                        m_arl = new ArrayList();
                    }


                }
            }
            catch (Exception ex)
            {
                EdiError.LogError(m_iFileType.ToString(), m_iFileId.ToString(), "", ex, "Error in StartProcessing");
                ErrorLog.CreateLog(ex);
            }
            
        } // start processing end here..


        public void ExtractInfoToMysql()
        {

            try
            {
                for (int i = 0; i < m_dict.Count; i++)
                {
                    if (i > 0 && i < m_dict.Count - 1)
                    {
                        m_dictItem = new Dictionary<int, ArrayList>();
                        CreateItemDictionary(m_dict[i]);
                        FetchDataFromItemDictionary();
                        //this.InitializeHeader(m_dict[i], ref oheader863);
                        // iHeaderId = oheader863.InsertHeader(m_iFileId);
                        //this.ExtractChemistry(m_dict[i], ref oDetail863, iHeaderId, iChemistryStartIndex, iChemistryEndIndex);
                        //this.ExtractMechanical(m_dict[i], ref oDetail863, iHeaderId, iMechanicalStartIndex, iMechanicalEndIndex);

                    }


                }
            }
            catch (Exception ex)
            {
                EdiError.LogError(m_iFileType.ToString(), m_iFileId.ToString(), "", ex, "Error in StartProcessing");
                ErrorLog.CreateLog(ex);
            }

        }



        public void FetchDataFromItemDictionary()
        {
            bool bMeaFound = false;
            header863 oheader863 = null;
            Detail863 oDetail863 = null;
            //string sVenTagId = string.Empty;
            int iHeaderId = 0;
            oheader863 = new header863();
            try
            {
                for (int i = 0; i < m_dictItem.Count; i++)
                {
                    ArrayList alsItem = new ArrayList();
                    alsItem = m_dictItem[i];
                    for (int j = 0; j < alsItem.Count; j++)
                    {
                        if (alsItem[j].ToString().Contains(EdiKeyWords.LIN))
                        {
                            bMeaFound = true;
                            //sVenTagId = alsItem.ToString().Split('*')[5];
                        }
                        if (alsItem[j].ToString().Contains("REF*SI*"))
                        {
                            if(string.IsNullOrEmpty(oheader863.ShipingRef))
                            {
                                oheader863.ShipingRef = RemoveLineBreaker(alsItem[j].ToString()).Split('*')[2];
                            }
                        }
                        if (bMeaFound)
                        {
                          //  oheader863 = new header863();
                            oheader863.Mill = oFiles.Mill;
                            m_sMill = oFiles.Mill;
                            oheader863.VenId = oFiles.VendorID;
                            oheader863.Type = BusinessUtility.GetString(ConfigurationManager.AppSettings["QdsType"]);
                            oheader863.UknownHeat = BusinessUtility.GetString(ConfigurationManager.AppSettings["UknownHeat"]);
                            //oheader863.ApvdFlag = BusinessUtility.GetString(ConfigurationManager.AppSettings["ApvdFlag"]);
                            oheader863.ApvdFlag = BusinessUtility.GetString(oConfiguration.QdsApproved);
                            this.InitializeHeader(alsItem, ref oheader863);
                            iHeaderId = oheader863.InsertHeader(m_iFileId);
                            this.ExtractChemistry(alsItem, ref oDetail863, iHeaderId, iChemistryStartIndex, iChemistryEndIndex);
                            this.ExtractMechanical(alsItem, ref oDetail863, iHeaderId, iMechanicalStartIndex, iMechanicalEndIndex);

                            int iDtId = oDetail863.InsertHeaderDetailMetalStanderd(iHeaderId, m_iFileId, BusinessUtility.GetInt(oheader863.OrigPoNo));
                            

                            if (IsValidEdiData())
                            {
                                if (iHeaderId > 0)
                                {
                                    this.InvokeWebMethodForQDS(m_iFileId, iHeaderId, oheader863);
                                }
                            }
                            else
                            {
                                ErrorLog.createLog("Edi data missing like po and heat..!!");
                                Console.WriteLine("Edi data missing like po and heat..!!");
                                Exception ex = null;
                                EdiError.LogError(m_iFileType.ToString(), m_iFileId.ToString(), ConfigurationManager.AppSettings["Edidatamissing"]);
                            }
                            bMeaFound = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                EdiError.LogError(m_iFileType.ToString(), m_iFileId.ToString(), "", ex, "Error in FetchDataFromItemDictionary");
                ErrorLog.CreateLog(ex);
            }
        }





        public void InvokeWebMethodForQDS(int iFileId, int iHeaderId, header863 oheader863)
        {
            try
            {
                QdsService oQdsService = new QdsService(iFileId, iHeaderId);
                oQdsService.CreateQDS();
            }
            catch (Exception ex)
            {
                EdiError.LogError(m_iFileType.ToString(), m_iFileId.ToString(), "", ex, "Error in InvokeWebMethodForQDS");
                ErrorLog.CreateLog(ex);
            }
        }

        public void CreateItemDictionary(ArrayList sArr)
        {
            ArrayList arlItem = new ArrayList();
            string[] lineContent = null;
            bool bLinFound = false;
            int iLinCount = 0;
            try
            {
                for (int i = 0; i < sArr.Count; i++)
                {
                    lineContent = BusinessUtility.GetString(sArr[i]).Split('*');
                    if (BusinessUtility.GetString(lineContent[0]) != EdiKeyWords.LIN)
                    {
                        arlItem.Add(sArr[i]);
                    }
                    if (BusinessUtility.GetString(lineContent[0]) == EdiKeyWords.LIN || BusinessUtility.GetString(lineContent[0]) == EdiKeyWords.CTT || RemoveLineBreaker(lineContent[0]) == EdiKeyWords.CTT || BusinessUtility.GetString(lineContent[0]) == EdiKeyWords.SE)
                    {
                        //bLinFound = true;  // SE need to added here..
                        m_dictItem.Add(iLinCount, arlItem);
                        arlItem = new ArrayList();
                        arlItem.Add(sArr[i]);
                        iLinCount++;
                    }
                }
            }
            catch (Exception ex)
            {
                EdiError.LogError(m_iFileType.ToString(), m_iFileId.ToString(), "", ex, "Error in CreateItemDictionary");
                ErrorLog.CreateLog(ex);
            }
        }

        
        // Extract each chemistry and save in mysql..
        public void ExtractChemistry(ArrayList sArr, ref Detail863 oDetail863, int iHeaderId, int iStartIndex, int iEndIndex)
        {
            int iSequence = 0;
            string[] lineContent = null;
            string sChemElement = string.Empty;
            try
            {
                for (int i = 0; i < sArr.Count; i++)
                {
                    if (i >= iStartIndex && i <= iEndIndex)
                    {
                        lineContent = RemoveLineBreaker(sArr[i]).Split('*');
                        if (lineContent.Length > 1)
                        {
                            if (BusinessUtility.GetString(lineContent[0]) == EdiKeyWords.MEA)
                            {
                                oDetail863 = new Detail863();

                                string sVal = lineContent[2];
                                if (sVal.ToUpper().StartsWith("Z"))
                                {
                                    if (sVal.Length >= 2)
                                    {
                                        sVal = sVal.Substring(1, sVal.Length - 1);
                                    }
                                }

                                int iChemDefaultVal = 0;
                                sChemElement = oDetail863.GetStratixChemicalElement(BusinessUtility.GetString(sVal), ref iSequence , ref iChemDefaultVal);

                                 // default change start

                                // check if current elem existsin file and header


                                Detail863 objDetail863 = new Detail863();
                                bool bDonotInsertDuplicateElement = false;
                                DataTable dtChemExistInHEader = objDetail863.GetChemistryList(m_iFileId, iHeaderId);
                                if (dtChemExistInHEader.Rows.Count > 0)
                                {
                                    DataRow[] result = dtChemExistInHEader.Select("ChemistrylElement = '" + sChemElement + "'");  // check if element in record exist
                                    if (result.Length > 0)
                                    {
                                        objDetail863.GetStratixChemicalElementBasedOnStratixElem(sChemElement);
                                        if (objDetail863.ChemDefault == 1)
                                        {
                                            bDonotInsertDuplicateElement = true;
                                            EdiError.LogError(m_iFileType.ToString(), m_iFileId.ToString(), "Duplicate chemistry detected. -> " + sChemElement, 0, 0, iHeaderId, 0);
                                        }
                                        else
                                        {
                                            bDonotInsertDuplicateElement = false;
                                            objDetail863.UpdateHeaderDetailChemistry(iHeaderId, m_iFileId, sChemElement);
                                            //delete current chem element
                                        }
                                    }
                                }

                                




                                // default change end


                                if (!string.IsNullOrEmpty(sChemElement) && !bDonotInsertDuplicateElement)
                                {
                                    string sChemElemVal = BusinessUtility.GetString(lineContent[3]);
                                    decimal dChemVal = BusinessUtility.GetDecimal(sChemElemVal);
                                    decimal dDefaultChemVal = .0001M;
                                    oDetail863.ClassID = Characteristic863.CHEMISTRY;
                                    oDetail863.ChemistrySequence = iSequence;
                                    oDetail863.ChemistrylElement = sChemElement;
                                    //if (dChemVal < dDefaultChemVal)
                                    //{
                                    //    oDetail863.ChemistryElementVAlue = "< " + BusinessUtility.GetString(lineContent[3]);
                                    //    oDetail863.ChmEntryType = "A";
                                    //}
                                    //else
                                    //{
                                    //    oDetail863.ChemistryElementVAlue = BusinessUtility.GetString(lineContent[3]);
                                    //    oDetail863.ChmEntryType = "V";
                                    //}
                                    oDetail863.ChemistryElementVAlue = BusinessUtility.GetString(lineContent[3]);
                                    oDetail863.ChmEntryType = "V";

                                    oDetail863.FileId = m_iFileId;
                                    oDetail863.InsertHeaderDetailChemistry(iHeaderId, m_iFileId);
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                EdiError.LogError(m_iFileType.ToString(), m_iFileId.ToString(), "", ex, "Error in ExtractChemistry", 0, 0, iHeaderId, 0);
                ErrorLog.CreateLog(ex);
            }
        }


        // Extract each Mechanical and save in mysql..
        public void ExtractMechanical(ArrayList sArr, ref Detail863 oDetail863, int iHeaderId, int iStartIndex, int iEndIndex)
        {

            //944 Sample Direction Code
            //01 Longitudinal
            //02 Transverse
            //05 Forty-Five Degree

            //L - Longitudinal
            //N - Not Applicable
            //T - Transverse


            string[] lineContent = null;
            bool bTestMethodFound = false;
            string sTestMethodDescription = string.Empty;
            string sTestProductDescription = string.Empty;
            string sTestCode = string.Empty;  //TestAbbr is test code
            string sDirection = string.Empty;
            string sTestEntityType = string.Empty;

            string sTestabbr = string.Empty;
            string sTestUm = string.Empty;
            bool bUOMMissing = false;
            try
            {
                for (int i = 0; i < sArr.Count; i++)
                {
                    if (i >= iStartIndex && i <= iEndIndex)
                    {
                        lineContent = RemoveLineBreaker(sArr[i]).Split('*');
                        if (lineContent.Length > 1)
                        {
                            if (BusinessUtility.GetString(lineContent[0]) == EdiKeyWords.PSD)
                            {
                                sDirection = BusinessUtility.GetString(lineContent[6]);
                                if (sDirection.Equals("01"))
                                {
                                    sDirection = "L";
                                }
                                else if (sDirection.Equals("02"))
                                {
                                    sDirection = "T";
                                }
                                else
                                {
                                    sDirection = "N";
                                }
                            }
                            else if (BusinessUtility.GetString(lineContent[0]) == EdiKeyWords.TMD)
                            {
                                bTestMethodFound = true;
                                if (lineContent.Length == 6)
                                {
                                    sTestMethodDescription = BusinessUtility.GetString(lineContent[6]);
                                }
                                sTestProductDescription = BusinessUtility.GetString(lineContent[3]);
                                // sTestEntityType = BusinessUtility.GetString(lineContent[3]);
                                if (sTestProductDescription.Contains("177"))
                                {
                                    sDirection = "N";
                                }
                            }

                              //  TMD*32*ST*090~            90 is sTestProductDescription
                              //  MEA*TR*TF*47.4*84~	  84 is uom  .. mea04 is uom  lineContent[4] for measurment

                            else if (BusinessUtility.GetString(lineContent[0]) == EdiKeyWords.MEA && bTestMethodFound)
                            {
                                bTestMethodFound = false;
                                oDetail863 = new Detail863();
                                DataTable dtTest = oDetail863.GetTestAbbrivation(sTestProductDescription, oFiles.VendorId);

                                foreach (DataRow oDataRow in dtTest.Rows)
                                {
                                    sTestabbr = BusinessUtility.GetString(oDataRow["TestCode"]);
                                    sTestEntityType = BusinessUtility.GetString(oDataRow["TestEntityType"]);
                                    sTestUm = BusinessUtility.GetString(oDataRow["UOM"]);
                                }

                                Measurment oMeasurment = new Measurment(oFiles.VendorId, lineContent[4]);


                                oDetail863.TestAbbr = sTestabbr;
                                oDetail863.ClassID = Characteristic863.MECHANICAL;
                                oDetail863.TestEntityType = sTestEntityType;
                                oDetail863.TestProdDesc = sTestProductDescription;  // code based on we get code of test
                                oDetail863.TestDirection = sDirection;
                                //oDetail863.TestUM = BusinessUtility.GetString(lineContent[4]);
                                oDetail863.TestUM = sTestUm;

                                if (sTestEntityType.Equals("V"))
                                {
                                    string sVal = BusinessUtility.GetDouble(lineContent[3]).ToString("N2");


                                    //string[] s = sVal.Split('.');   // Mrinal : commenting this code and applying round logic above up to 2 decimal place.
                                    //if ( s.Length == 2 )
                                    //{
                                    //    sVal = s[0];

                                    //    string s2 = s[1];
                                    //    if(s2.Length >= 2)
                                    //    {
                                    //     s2 = s2.Substring(0, 2);
                                    //    }
                                    //    sVal = sVal + "." + s2;
                                    //}
                                    if (!String.IsNullOrEmpty(oMeasurment.MulVal))
                                    {
                                        oDetail863.TestVal = BusinessUtility.GetDouble((BusinessUtility.GetDecimal(sVal) * BusinessUtility.GetDecimal(oMeasurment.MulVal))).ToString("N2");
                                        //ErrorLog.createLog("Converting test val");
                                        //ErrorLog.createLog("oDetail863.TestVal = " + oDetail863.TestVal);
                                        bUOMMissing = false;
                                    }
                                    else
                                    {
                                        bUOMMissing = true;
                                        oDetail863.TestVal = sVal;
                                    }

                                }
                                else if (sTestEntityType.Equals("A"))
                                {
                                    oDetail863.TestAlpha = BusinessUtility.GetString(lineContent[3]);
                                }
                                else if (sTestEntityType.Equals("R"))
                                {
                                    string[] sRange = BusinessUtility.GetString(lineContent[3]).Split(',');
                                    if (sRange.Length > 0)
                                    {
                                        if (!String.IsNullOrEmpty(oMeasurment.MulVal))
                                        {
                                            oDetail863.TestMinVal = BusinessUtility.GetDouble((BusinessUtility.GetDecimal(sRange[0]) * BusinessUtility.GetDecimal(oMeasurment.MulVal))).ToString("N2");
                                            oDetail863.TestMaxVal = BusinessUtility.GetDouble((BusinessUtility.GetDecimal(sRange[1]) * BusinessUtility.GetDecimal(oMeasurment.MulVal))).ToString("N2");
                                        }
                                        else
                                        {
                                            oDetail863.TestMinVal = sRange[0];
                                            oDetail863.TestMaxVal = sRange[1];
                                        }
                                    }
                                }
                                oDetail863.FileId = m_iFileId;
                                if (dtTest.Rows.Count > 0)
                                {
                                    if (!bUOMMissing)
                                    {
                                        oDetail863.InsertHeaderDetailMechanical(iHeaderId, m_iFileId);
                                    }
                                    else
                                    {
                                        //Utility.WaitForSecond(2);
                                        // send mail for skipped test.

                                        Console.WriteLine("Omiting test as UM missing");
                                        ErrorLog.createLog("Omiting test as UM missing");
                                        EdiError.LogError("863", Globalcl.FileId.ToString(), "Omiting test as UM missing", 0, 0, iHeaderId, 0);
                                        
                                        string sMessage = "Unable to process test for following Mill-Heat("+ m_sMill +" , " + m_sHeat+") due missing UM  : "+ BusinessUtility.GetString(lineContent[4]) + " code.";
                                        EdiError.SendErrorMail(Globalcl.FileId, "Unable to process test", sMessage);
                                        //Utility.WaitForSecond(10);
                                    }
                                }
                                else
                                {
                                    string stest = string.Empty;
                                    try
                                    {
                                        if(lineContent.Length > 2)
                                        {
                                            stest = lineContent[0] + "*" + lineContent[1] + "*" + lineContent[2] + "*" + lineContent[3];
                                        }
                                    }
                                    catch { }
                                    Console.WriteLine("Omiting test as Tests entry not available in db");
                                    ErrorLog.createLog("Omiting test as Tests entry not available in db");
                                    EdiError.LogError("863", Globalcl.FileId.ToString(), "Tests entry not available in db for mill heat ( " + m_sMill + " , " + m_sHeat + " )", 0, 0, iHeaderId, 0);
                                    EdiError.SendErrorMail(Globalcl.FileId, "Tests entry missing", "Tests entry not available in db for mill heat ( " + m_sMill + " , " + m_sHeat + " test = "+ stest + ")");
                                    // string sMessage = "Unable to process test for following Mill-Heat(" + m_sMill + " , " + m_sHeat + ") due missing UM  : " + BusinessUtility.GetDouble(lineContent[4]) + " code.";
                                    //EdiError.SendErrorMail(Globalcl.FileId, "Unable to process test", sMessage);
                                    //Utility.WaitForSecond(10);
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                EdiError.LogError(m_iFileType.ToString(), m_iFileId.ToString(), "", ex, "Error in ExtractMechanical", 0, 0, iHeaderId, 0);
                ErrorLog.CreateLog(ex);
            }
        }



        



        public void InitializeHeader(ArrayList sArr, ref header863 oheader863)
        {
            string[] lineContent = null;
            
            iMechanicalEndIndex = 0;
            iMechanicalStartIndex = 0;
            iChemistryStartIndex = 0;
            iChemistryEndIndex = 0;

            bool bMechanicalFound = false;
            try
            {
                for (int i = 0; i < sArr.Count; i++)
                {
                    lineContent = RemoveLineBreaker(sArr[i]).Split('*');
                    if (lineContent.Length > 1 || lineContent[0] == EdiKeyWords.CTT)
                    {
                        if (BusinessUtility.GetString(lineContent[0]) == EdiKeyWords.LIN)
                        {
                            EdiError.LogError(Files.ExecutionFileType.EDI863, Globalcl.FileId.ToString(), "PO STRING : " + BusinessUtility.GetString(lineContent[3]));
                            string[] sPoAndItem = BusinessUtility.GetString(lineContent[3]).Split('-');
                            if (sPoAndItem.Length > 1)
                            {
                               oheader863.OrigPoNo =  Utility.GetIntFromString(BusinessUtility.GetString(lineContent[3])).ToString();
                               if (string.IsNullOrEmpty(oheader863.OrigPoNo))
                               {
                                   oheader863.OrigPoNo = sPoAndItem[0];
                               }
                                //oheader863.OrigPoItem = sPoAndItem[1];
                            }
                            else
                            {
                                oheader863.OrigPoNo = BusinessUtility.GetString(lineContent[3]);
                                oheader863.OrigPoItem = "";
                            }

                            //oheader863.Heat = BusinessUtility.GetString(lineContent[7]);
                            oheader863.Heat = Utility.RemoveTrailingZero(Utility.GetElementValue(RemoveLineBreaker(sArr[i]), EdiKeyWords.HN));
                            m_sHeat = oheader863.Heat;
                            m_sPo = oheader863.OrigPoNo;
                            //oheader863.VendorTagId = BusinessUtility.GetString(lineContent[5]);
                            oheader863.VendorTagId = Utility.GetElementValue(RemoveLineBreaker(sArr[i]), EdiKeyWords.SN);
                            this.GetPoDetails(BusinessUtility.GetInt(oheader863.OrigPoNo), oheader863);
                        }

                        else if (BusinessUtility.GetString(lineContent[0]) == EdiKeyWords.CID && BusinessUtility.GetInt(lineContent[2]) == Characteristic863.CHEMISTRY)
                        {
                            iChemistryStartIndex = i;
                        }

                        else if (BusinessUtility.GetString(lineContent[0]) == EdiKeyWords.CID && BusinessUtility.GetInt(lineContent[2]) == Characteristic863.MECHANICAL && !bMechanicalFound)
                        {
                            iChemistryEndIndex = i - 1;
                            iMechanicalStartIndex = i;
                            bMechanicalFound = true;
                        }
                        else if (BusinessUtility.GetString(lineContent[0]) == EdiKeyWords.CTT)
                        {
                            iMechanicalEndIndex = i;
                            bMechanicalFound = false;
                        }
                        else if (iMechanicalStartIndex > 0 && iMechanicalEndIndex == 0)
                        {
                            iMechanicalEndIndex = sArr.Count - 1;
                            //bMechanicalFound = false;
                        }

                        if (iChemistryEndIndex == 0 && BusinessUtility.GetString(lineContent[0]) == EdiKeyWords.CTT)
                        {
                            iChemistryEndIndex = i - 1;
                        }
                        else if (iChemistryEndIndex == 0 && BusinessUtility.GetString(lineContent[0]) == EdiKeyWords.MEA && sArr.Count - 1 == i)  // adding as chem will be blank if ctt not found
                        {
                            iChemistryEndIndex = i - 1;
                        }


                    }

                }
            }
            catch (Exception ex)
            {
                EdiError.LogError(m_iFileType.ToString(), m_iFileId.ToString(), "", ex, "Error in InitializeHeader");
                ErrorLog.CreateLog(ex);
            }
        }



        public void GetPoDetails(int iPoNumber,  header863 oheader863)
        {
            DbHelper oDbHelper = new DbHelper(sStratixConnectionString, true);
            try
            {
                if (BusinessUtility.GetInt(oheader863.OrigPoItem) > 0)
                {
                    m_sSql = "Select first 1 pod_shp_to_whs,pod_frt_ven_id from potpod_rec where pod_cmpy_id =  '" + m_sCompanyId + "' and pod_po_pfx= 'PO' and pod_po_no = '" + iPoNumber + "' AND pod_po_itm = '" + BusinessUtility.GetInt(oheader863.OrigPoItem) + "'";
                }
                else
                {
                    m_sSql = "Select first 1 pod_shp_to_whs,pod_frt_ven_id from potpod_rec where pod_cmpy_id =  '" + m_sCompanyId + "' and pod_po_pfx= 'PO' and pod_po_no = '" + iPoNumber + "'";
                }
                //ErrorLog.createLog("Sql for whs = " + m_sSql);
                DataTable oDataTable = oDbHelper.GetDataTable(m_sSql, CommandType.Text, null);
                ErrorLog.createLog("whs count = " + oDataTable.Rows.Count);
                if (oDataTable.Rows.Count > 0)
                {
                    ErrorLog.createLog("Warehouse for qds is : " + BusinessUtility.GetString(oDataTable.Rows[0][0]));
                    oheader863.Whs = BusinessUtility.GetString(oDataTable.Rows[0][0]);
                }
            }
            catch (Exception ex)
            {
                ErrorLog.createLog(ex.ToString());
                EdiError.LogError(BusinessUtility.GetString(m_iFileType), m_iFileId.ToString(), "", ex, "Error in GetPoDetails");
                ErrorLog.CreateLog(ex);
            }
            finally
            {
                oDbHelper.CloseDatabaseConnection();
            }
        }

        



    }
}
