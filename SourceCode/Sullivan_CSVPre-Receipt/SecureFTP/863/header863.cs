﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using iTECH.Library.DataAccess.ODBC;
using iTECH.Library.Utilities;
using System.Data;
using MySql.Data.MySqlClient;
using System.Configuration;

namespace SecureFTP
{
    class header863
    {
        StringBuilder m_sbSql = null;
        string m_sSql = string.Empty;
        static string sStratixConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ToString();
        static string sMysqlConnectionString = ConfigurationManager.ConnectionStrings["NewConnectionString"].ToString();
        Configuration oConfiguration = null;

        int m_iFileId = 0;
        int m_iHeaderId = 0;

        string sMill = string.Empty;
        public string Mill
        {
            get { return sMill; }
            set { sMill = value; }
        }


        string sHeat = string.Empty;
        public string Heat
        {
            get { return sHeat; }
            set { sHeat = value; }
        }

        string sType = string.Empty;
        public string Type
        {
            get { return sType; }
            set { sType = value; }
        }

        string sWhs = string.Empty;
        public string Whs
        {
            get { return sWhs; }
            set { sWhs = value; }
        }

        string sOrigPoPrefix = string.Empty;
        public string OrigPoPrefix
        {
            get { return sOrigPoPrefix; }
            set { sOrigPoPrefix = value; }
        }

        string sOrigPoNo = string.Empty;
        public string OrigPoNo
        {
            get { return sOrigPoNo; }
            set { sOrigPoNo = value; }
        }

        string sOrigPoItem = string.Empty;
        public string OrigPoItem
        {
            get { return sOrigPoItem; }
            set { sOrigPoItem = value; }
        }

        string sOrigPoDistribution = string.Empty;
        public string OrigPoDistribution
        {
            get { return sOrigPoDistribution; }
            set { sOrigPoDistribution = value; }
        }

        string sMillOrdNo = string.Empty;
        public string MillOrdNo
        {
            get { return sMillOrdNo; }
            set { sMillOrdNo = value; }
        }

        string sVenId = string.Empty;
        public string VenId
        {
            get { return sVenId; }
            set { sVenId = value; }
        }

        string sStatus = string.Empty;
        public string Status
        {
            get { return sStatus; }
            set { sStatus = value; }
        }

        string sApvdFlag = string.Empty;
        public string ApvdFlag
        {
            get { return sApvdFlag; }
            set { sApvdFlag = value; }
        }

        string sOrigZone = string.Empty;
        public string OrigZone
        {
            get { return sOrigZone; }
            set { sOrigZone = value; }
        }

        string sUknownHeat = string.Empty;
        public string UknownHeat
        {
            get { return sUknownHeat; }
            set { sUknownHeat = value; }
        }

        string sVendorTagId = string.Empty;
        public string VendorTagId
        {
            get { return sVendorTagId; }
            set { sVendorTagId = value; }
        }

        string sShipingRef = string.Empty;
        public string ShipingRef
        {
            get { return sShipingRef; }
            set { sShipingRef = value; }
        }
        



        public int InsertHeader(int iFileId)
        {
            int iStatus = 0;
            m_iFileId = iFileId;
            m_sbSql = new StringBuilder();
            iTECH.Library.DataAccess.MySql.DbHelper oDbHelper = new iTECH.Library.DataAccess.MySql.DbHelper(sMysqlConnectionString, true);
            try
            {
                m_sbSql.Append(" INSERT INTO header863(FileId,mill, heat, type, whs, origPoPrefix, origPoNo, origPoItem, origPoDistribution, millOrdNo, venId, status, apvdFlag, origZone, uknownHeat, VendorTagId,ShipingRef)");
                m_sbSql.Append(" VALUES ('" + iFileId + "','" + sMill + "', '" + sHeat + "', '" + sType + "', '" + sWhs + "', '" + sOrigPoPrefix + "', '" + sOrigPoNo + "' ");
                m_sbSql.Append(" , '" + BusinessUtility.GetInt(sOrigPoItem) + "', '" + BusinessUtility.GetInt(sOrigPoDistribution) + "', '" + sMillOrdNo + "', '" + sVenId + "', '" + sStatus + "' , '" + BusinessUtility.GetInt(sApvdFlag) + "', '" + sOrigZone + "', '" + sUknownHeat + "' , '" + sVendorTagId + "' , '" + sShipingRef + "')  ");
                MySqlParameter[] oMySqlParameter = { };

                iStatus = oDbHelper.ExecuteNonQuery(m_sbSql.ToString(), CommandType.Text, oMySqlParameter);
                iStatus = oDbHelper.GetLastInsertID();
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLog(ex);
            }
            finally
            {
                oDbHelper.CloseDatabaseConnection();
            }
            return iStatus;
        }


        public header863()
        {

        }


        public header863(int iFileId, int iHeaderId)
        {
            m_iHeaderId = iHeaderId;
            m_iFileId = iFileId;
            m_sbSql = new StringBuilder();
            iTECH.Library.DataAccess.MySql.DbHelper oDbHelper = new iTECH.Library.DataAccess.MySql.DbHelper(sMysqlConnectionString, true);

            m_sbSql.Append(" SELECT ID AS HeaderID, FileId,mill, heat, type, whs, origPoPrefix, origPoNo, origPoItem, origPoDistribution, millOrdNo, venId, status, apvdFlag, origZone, uknownHeat, VendorTagId,ShipingRef FROM header863 WHERE FileId = '" + iFileId + "' AND ID = '" + iHeaderId + "'");
            ErrorLog.createLog("header sql = " + m_sbSql.ToString());
            MySqlParameter[] oMySqlParameter = { };
            try
            {
                DataTable dtHeaderList = oDbHelper.GetDataTable(m_sbSql.ToString(), CommandType.Text, oMySqlParameter);
                for (int i = 0; i < dtHeaderList.Rows.Count; i++)
                {
                    sMill = BusinessUtility.GetString(dtHeaderList.Rows[i]["mill"]);
                    sHeat = BusinessUtility.GetString(dtHeaderList.Rows[i]["heat"]);
                    sType = BusinessUtility.GetString(dtHeaderList.Rows[i]["type"]);
                    sWhs = BusinessUtility.GetString(dtHeaderList.Rows[i]["whs"]);
                    sOrigPoPrefix = BusinessUtility.GetString(dtHeaderList.Rows[i]["origPoPrefix"]);
                    sOrigPoNo = BusinessUtility.GetString(dtHeaderList.Rows[i]["origPoNo"]);
                    sOrigPoItem = BusinessUtility.GetString(dtHeaderList.Rows[i]["origPoItem"]);
                    sOrigPoDistribution = BusinessUtility.GetString(dtHeaderList.Rows[i]["origPoDistribution"]);
                    sMillOrdNo = BusinessUtility.GetString(dtHeaderList.Rows[i]["millOrdNo"]);
                    sVenId = BusinessUtility.GetString(dtHeaderList.Rows[i]["venId"]);
                    sStatus = BusinessUtility.GetString(dtHeaderList.Rows[i]["status"]);
                    sApvdFlag = BusinessUtility.GetString(dtHeaderList.Rows[i]["apvdFlag"]);
                    sOrigZone = BusinessUtility.GetString(dtHeaderList.Rows[i]["origZone"]);
                    sUknownHeat = BusinessUtility.GetString(dtHeaderList.Rows[i]["uknownHeat"]);
                    VendorTagId = BusinessUtility.GetString(dtHeaderList.Rows[i]["VendorTagId"]);
                    ShipingRef = BusinessUtility.GetString(dtHeaderList.Rows[i]["ShipingRef"]);
                }

            }

            catch (Exception ex)
            {
                ErrorLog.CreateLog(ex);
            }


        }



        public void CheckPoExistanceForCurrentFile(int iFileId, int iPo, string sHeat, string sShipingRef, ref int iProductQds)
        {
            m_sSql = " select FileId,hdrProductQds from header863 where heat = '" + sHeat + "' and origPoNo = '" + iPo + "' and ShipingRef = '" + sShipingRef + "' and fileid = '" + iFileId + "' ";
            try
            {
                iTECH.Library.DataAccess.MySql.DbHelper oDbHelper = new iTECH.Library.DataAccess.MySql.DbHelper(sMysqlConnectionString, true);
                MySqlParameter[] oMySqlParameter = { };

                DataTable oDtAddress = oDbHelper.GetDataTable(m_sSql, CommandType.Text, oMySqlParameter);
                foreach (DataRow oDataRow in oDtAddress.Rows)
                {
                    
                    iProductQds = BusinessUtility.GetInt(oDataRow["hdrProductQds"]);
                    if (iProductQds > 0)
                    {
                        break;
                    }

                }
            }
            catch(Exception ex)
            {}
        }

        public DataTable GetUnprocessed863(int iFileId)
        {
            DataTable dt = null;
            iTECH.Library.DataAccess.MySql.DbHelper oDbHelper = new iTECH.Library.DataAccess.MySql.DbHelper(sMysqlConnectionString, true);
            try
            {
                m_sSql = "select * from header863 where processed = 0 AND FileId = " + iFileId;
                ErrorLog.createLog("GetUnprocessed863 = " + m_sSql);
                MySqlParameter[] oMySqlParameter = { };
                dt = oDbHelper.GetDataTable(m_sSql, CommandType.Text, oMySqlParameter);
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLog(ex);
                EdiError.LogError(Globalcl.FileType, Globalcl.FileId.ToString(), "", ex, "Error in GetUnprocessed856 of files class");
            }
            finally
            {
                oDbHelper.CloseDatabaseConnection();
            }
            return dt;
        }


        public void InactiveHeader(int iFileid, int iHeaderId)
        {
            iTECH.Library.DataAccess.MySql.DbHelper oDbHelper = new iTECH.Library.DataAccess.MySql.DbHelper(sMysqlConnectionString, true);
            //int iFileId = 0;  //comment code due to this header863 records not updated    2020/03/14  Sumit
            try
            {
                MySqlParameter[] oMySqlParameter = { };
                m_sSql = "update header863 set processed = 1 where ID =  " + iHeaderId + " AND FileId = " + iFileid;
                oDbHelper.ExecuteNonQuery(m_sSql, CommandType.Text, oMySqlParameter);
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLog(ex);
                //EdiError.LogError(sEdiFileType, iFileId.ToString(), sFileName, ex, "Error in UpdateReceipt of files class");
            }
            finally
            {
                oDbHelper.CloseDatabaseConnection();
            }
        }

    }
}
