﻿using SecureFTP.QdsCreationService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using iTECH.Library.Utilities;
using System.Data;
using System.Configuration;
using MySql.Data.MySqlClient;
using iTECH.Library.DataAccess.ODBC;
using System.IO;
using IBM.Data.Informix;

namespace SecureFTP
{
    class QdsService
    {
        private int m_iFileId = 0;
        private int m_iHeaderId = 0;
        long lngQdsNo = 0;       // this is heat qds no
        long lngProductQdsNo = 0;   // this is product qds no
        Configuration oConfiguration = null;
        header863 oheader863 = null;
        private static string EdiFile = string.Empty;
        Files oFiles;
        string m_sSql = string.Empty;
        static string sMysqlConnectionString = ConfigurationManager.ConnectionStrings["NewConnectionString"].ToString();
        static string sStratixConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ToString();
        long lngOldQds = 0;
        int iOldFileId = 0;
        string sOldRefSi = string.Empty;
        int iOldPo = 0;

        long lngOldHeatQds = 0;

        long lngExistingQds = 0;
        long lngRelaredReceipt = 0;
        bool bTestMatchFromPreviousQds = false;
        bool bShipRefNumberMatch = false;
        bool bPOMatch = false;
        public QdsService(int iFileId, int iHeaderId)
        {
            m_iFileId = iFileId;
            if (m_iFileId > 0)
            {
                m_iHeaderId = iHeaderId;
                oFiles = new Files(m_iFileId);
                EdiFile = oFiles.EdiFileType;
            }
        }

        public bool CheckServiceConnection(string sCompId, string sConnectedAccType, string sEnvClass, string sEnvNAme, string sUserNAme, string sPass)
        {
            bool bReturn = false;
            try
            {
                GatewayAuthService.AuthenticationToken oAuthenticationToken = new GatewayAuthService.AuthenticationToken();
                oAuthenticationToken = UserAuthenticationTest(sCompId, sConnectedAccType, sEnvClass, sEnvNAme, sUserNAme, sPass);
                if (oAuthenticationToken.value.Length > 10)
                {
                    bReturn = true;
                    Console.WriteLine("Login success to service");
                    UserAuthenticationLogOut(oAuthenticationToken);
                }
            }
            catch(Exception ex)
            {
                ErrorLog.createLog("Error in CheckServiceConnection" + ex.ToString());
                Console.WriteLine("Error in CheckServiceConnection");
                ErrorLog.CreateLog(ex);
            }

            return bReturn;
        }

        public static DataTable GetInfomixRecord(string constr, string sqlQuery)
        {
            //ErrorLog.createLog("Connection string:" + constr);
            DataTable dt = new DataTable();
            using (IfxConnection conn = new IfxConnection(constr))
            {

                //string strSQL = string.Empty;
                //strSQL = ""; // Write Your sql query here 
                try
                {
                    IfxCommand query = new IfxCommand(sqlQuery);
                    IBM.Data.Informix.IfxDataAdapter adapter = new IfxDataAdapter();
                    query.Connection = conn;
                    query.CommandTimeout = 200000; //you can define your command timeout time
                    conn.Open();
                    adapter.SelectCommand = new IfxCommand("SET ISOLATION TO DIRTY READ", conn);
                    adapter.SelectCommand.ExecuteNonQuery(); //Tells the program to wait in case of a lock.     
                    adapter.SelectCommand = query;
                    adapter.Fill(dt);
                    conn.Close();
                    adapter.Dispose();
                }

                catch (Exception ex)
                {
                    ErrorLog.createLog(ex.Message);
                    throw;
                }
                finally
                {
                    conn.Close();

                }
            }
            return dt;

        }

        public bool CheckMillHeatExistance(string sMill, string sHeat, ref long lngHeatQds, ref long lngReceiptNo)
        {
            EdiError.LogError(EdiFile, m_iFileId.ToString(), "CheckMillHeatExistance started");
            bool bReturn = false;
            DbHelper oDbHelper = new DbHelper(sStratixConnectionString, true);
            try
            {
                m_sSql = "select qds_qds_ctl_no as qdsno, qds_orig_no as prereceiptno  from mchqds_rec where qds_heat = '" + sHeat + "' and qds_mill = '" + sMill + "' and qds_qds_typ = 'H' ";
                ErrorLog.createLog(m_sSql);
                //DataTable oDataTable = oDbHelper.GetDataTable(m_sSql, CommandType.Text, null);
                DataTable oDataTable = GetInfomixRecord(Utility.GetConfigValue("IBMNewConnection"), m_sSql);
                if (oDataTable.Rows.Count > 0)
                {
                    lngHeatQds = BusinessUtility.GetInt(oDataTable.Rows[0]["qdsno"]);
                    lngReceiptNo = BusinessUtility.GetInt(oDataTable.Rows[0]["prereceiptno"]);
                }

                EdiError.LogError(EdiFile, m_iFileId.ToString(), "lngHeatQds = " + lngHeatQds + "lngReceiptNo = " + lngReceiptNo);
                bReturn = lngHeatQds > 0;
                ErrorLog.createLog("lngHeatQds = " + lngHeatQds + "lngReceiptNo = " + lngReceiptNo);
                ErrorLog.createLog("bReturn = " + bReturn);
            }
            catch (Exception ex)
            {
                EdiError.LogError(EdiFile, m_iFileId.ToString(), "CheckMillHeatExistance error occured. in catch block");
                if (BusinessUtility.GetString(ex).Contains("informix.mchqds_rec"))
                {
                    bReturn = false;
                    ErrorLog.createLog("Locking issue found . bReturn =" + bReturn);
                    EdiError.LogError(EdiFile, m_iFileId.ToString(), "Table is locked . CheckMillHeatExistance bReturn =" + bReturn);
                }
                else
                {
                    ErrorLog.CreateLog(ex);
                    ErrorLog.createLog("CheckMillHeatExistance catch else block");
                    EdiError.LogError(EdiFile, m_iFileId.ToString(), "CheckMillHeatExistance catch else block");
                    EdiError.LogError(EdiFile, m_iFileId.ToString(), "", ex, "");
                }
                ErrorLog.CreateLog(ex);
            }
            EdiError.LogError(EdiFile, m_iFileId.ToString(), "CheckMillHeatExistance completed bReturn = " + bReturn);
            return bReturn;
        }


        public int GetPoCount(int iPo)
        {
            bool bReturn = false;
            int iCount = 0;
            DbHelper oDbHelper = new DbHelper(sStratixConnectionString, true);
            try
            {
                m_sSql = " select count(*) as count from potpod_rec where pod_po_no = " + iPo;
                ErrorLog.createLog(m_sSql);
                DataTable oDataTable = oDbHelper.GetDataTable(m_sSql, CommandType.Text, null);
                if (oDataTable.Rows.Count > 0)
                {
                    iCount = BusinessUtility.GetInt(oDataTable.Rows[0]["count"]);
                }
                ErrorLog.createLog("iCount = " + iCount);
            }
            catch (Exception ex)
            {
                if (BusinessUtility.GetString(ex).Contains("informix.mchqds_rec"))
                {
                    iCount = 1;
                    ErrorLog.createLog("Locking issue found . iCount = " + iCount);
                }
                else
                {
                    ErrorLog.CreateLog(ex);
                    EdiError.LogError(EdiFile, m_iFileId.ToString(), "", ex, "");
                }
                ErrorLog.CreateLog(ex);
            }
            return iCount;
        }


        public string  GetStratixOriginZone(string sHeat)
        {
            string sOriginZone = string.Empty;
            string sHeatCode = string.Empty;
            try
            {
                if (oFiles.VendorName.Contains("Nucor") || oFiles.VendorName.Contains("AK STEEL"))
                {
                    sHeatCode = "*";
                }
                else
                {
                    sHeatCode = new String(sHeat.Where(c => Char.IsLetter(c) && Char.IsUpper(c)).ToArray());  // commenting code.. taking only 3 digit from heat.
                    if(!string.IsNullOrEmpty(sHeat) )
                    {
                        if (sHeat.Length >= 3)
                        {
                            sHeatCode = sHeat.Substring(0, 3);
                            EdiOriginZone oEdiOriginZone = new EdiOriginZone(oFiles.VendorDunsNo, oFiles.VendorID, oFiles.Mill, sHeatCode);
                            sOriginZone = oEdiOriginZone.StratixOriginZone;
                        }
                    }
                }

                if (string.IsNullOrEmpty(sOriginZone))
                {
                    bool isNumeric = int.TryParse(sHeat, out int n);
                    if (isNumeric)
                    {
                        sOriginZone = "USA";
                    }
                }
                if(string.IsNullOrEmpty(sOriginZone))
                {
                   EdiOriginZone oEdiOriginZone = new EdiOriginZone(oFiles.VendorDunsNo, oFiles.VendorID, oFiles.Mill, sHeatCode);
                   sOriginZone = oEdiOriginZone.StratixOriginZone;
                }

                if (string.IsNullOrEmpty(sOriginZone))
                {
                    sOriginZone = "USA";
                }
            }
            catch (Exception ex)
            {
                EdiError.LogError("863", m_iFileId.ToString(), "", ex, "");
                ErrorLog.CreateLog(ex);
            }
            EdiError.ValidateOriginZone(sOriginZone, "GetStratixOriginZone", BusinessUtility.GetString(Globalcl.FileId));
            ErrorLog.createLog("sOriginZone = " + sOriginZone);
            return sOriginZone;
        }

        public bool CheckHistory(string sMill, string sHeat, string ShipingRef, int ipono, ref long lngOldQdsNo , ref int iOldFileId)
        {
            try
            {
                //---- new logic
                EdiError.LogError(EdiFile, m_iFileId.ToString(), "CheckHistory start of QdsService class");
                Detail863 oDetail863 = new Detail863();
                DataTable dtTotalTestWithShipperNo = oDetail863.GetPreviousTestDetail(sMill, sHeat, ShipingRef, 0, ipono);
                int iOldMaxFileID = 0;
                if(dtTotalTestWithShipperNo.Rows.Count > 0)
                {
                    iOldMaxFileID = BusinessUtility.GetInt(dtTotalTestWithShipperNo.Rows[0]["FileId"]);
                }
                ErrorLog.createLog("iOldMaxFileID = " + iOldMaxFileID);

                DataTable dtOldTestDetailForCurrentMillHeat = oDetail863.GetPreviousTestDetail(sMill, sHeat, ShipingRef, iOldMaxFileID, ipono);
                DataTable dtCurrentTestDetailForCurrentMillHeat = oDetail863.GetTestDetailWithOrWithoutQds(sMill, sHeat, ShipingRef, Globalcl.FileId, m_iHeaderId);
                string sTestAbbr, sTestVal, sTestDirection, sTestUM, sRefSi;
                sTestAbbr = sTestVal = sTestDirection = sTestUM = sRefSi = string.Empty;

                string sOldTestAbbr, sOldTestVal, sOldTestDirection, sOldTestUM;
                sOldTestAbbr = sOldTestVal = sOldTestDirection = sOldTestUM = string.Empty;
                                                      
                EdiError.LogError(EdiFile, m_iFileId.ToString(), "dtOldTestDetailForCurrentMillHeat.Rows.Count ==  " + dtOldTestDetailForCurrentMillHeat.Rows.Count);
                EdiError.LogError(EdiFile, m_iFileId.ToString(), " dtCurrentTestDetailForCurrentMillHeat.Rows.Count ==  " + dtCurrentTestDetailForCurrentMillHeat.Rows.Count);
                ErrorLog.createLog("dtOldTestDetailForCurrentMillHeat.Rows.Count ==  " + dtOldTestDetailForCurrentMillHeat.Rows.Count);
                ErrorLog.createLog(" dtCurrentTestDetailForCurrentMillHeat.Rows.Count ==  " + dtCurrentTestDetailForCurrentMillHeat.Rows.Count);
                if (dtOldTestDetailForCurrentMillHeat.Rows.Count != dtCurrentTestDetailForCurrentMillHeat.Rows.Count)
                {
                    ErrorLog.createLog("bTestMatchFromPreviousQds = FALSE");
                    EdiError.LogError(EdiFile, m_iFileId.ToString(), "bTestMatchFromPreviousQds = FALSE");
                    bTestMatchFromPreviousQds = false;
                    lngOldQdsNo = 0;
                    iOldFileId = 0;
                    sOldRefSi = string.Empty;
                    return bTestMatchFromPreviousQds;
                }
                EdiError.LogError(EdiFile, m_iFileId.ToString(), "started comparing test");
                foreach (DataRow oDataRow in dtCurrentTestDetailForCurrentMillHeat.Rows)
                {
                    sTestAbbr = BusinessUtility.GetString(oDataRow["TestAbbr"]);
                    sTestVal = BusinessUtility.GetString(oDataRow["TestVal"]);
                    sTestDirection = BusinessUtility.GetString(oDataRow["TestDirection"]);
                    sTestUM = BusinessUtility.GetString(oDataRow["TestUM"]);
                    sRefSi = BusinessUtility.GetString(oDataRow["ShipingRef"]);

                    DataRow[] result = dtOldTestDetailForCurrentMillHeat.Select("TestAbbr = '" + sTestAbbr + "'");
                    foreach (DataRow row in result)
                    {
                        sOldTestVal = BusinessUtility.GetString(row["TestVal"]);
                        sOldTestDirection = BusinessUtility.GetString(row["TestDirection"]);
                        sOldTestUM = BusinessUtility.GetString(row["TestUM"]);
                        lngOldQdsNo = BusinessUtility.GetInt(row["hdrProductQds"]);
                        iOldFileId = BusinessUtility.GetInt(row["FileId"]);
                        sOldRefSi = BusinessUtility.GetString(row["ShipingRef"]);
                    }
                    ErrorLog.createLog("lngOldQdsNo = '" + lngOldQdsNo + "', iOldFileId = " + iOldFileId);
                    EdiError.LogError(EdiFile, m_iFileId.ToString(), "CheckHistory lngOldQdsNo = '" + lngOldQdsNo + "', iOldFileId = " + iOldFileId);
                    if ((sTestVal == sOldTestVal) && (sTestDirection == sOldTestDirection) && (sTestUM == sOldTestUM) && (sRefSi == sOldRefSi))
                    {
                        bTestMatchFromPreviousQds = true;
                    }
                    else
                    {
                        ErrorLog.createLog("bTestMatchFromPreviousQds = FALSE");
                        EdiError.LogError(EdiFile, m_iFileId.ToString(), "bTestMatchFromPreviousQds = FALSE");
                        bTestMatchFromPreviousQds = false;
                        lngOldQdsNo = 0;
                        iOldFileId = 0;
                        sOldRefSi = string.Empty;
                        break;
                    }


                }
            }
            catch (Exception ex)
            { ErrorLog.CreateLog(ex); }

            return bTestMatchFromPreviousQds;
          //  }

            //---- new logic end
        }


        public bool CheckMetalStandardHistory(string sCurentShipRefno, string sMill, string sHeat, int iPO, ref long lngOldQdsNo, ref int iOldFileId)
        {
            bool bMatched = false;
            try
            {
                long iOldQDSFromTestCompare = lngOldQdsNo;
                lngOldQdsNo = 0;
                iOldFileId = 0;
                //---- new logic
                EdiError.LogError(EdiFile, m_iFileId.ToString(), "CheckMetalStandardHistory start of QdsService class");
                Detail863 oDetail863 = new Detail863();
                DataTable dtOldMetalStdCurrentMillHeat = oDetail863.GetPreviousMaterialStanderdDetail(sMill, sHeat, iPO, sCurentShipRefno);
                DataTable dtCurrentMetalStdCurrentMillHeat = oDetail863.GetCurrentMaterialStanderdDetail(sMill, sHeat, iPO, Globalcl.FileId, m_iHeaderId);
                
                ErrorLog.createLog("dtOldMetalStdCurrentMillHeat count = " + dtOldMetalStdCurrentMillHeat.Rows.Count);
                ErrorLog.createLog("dtCurrentMetalStdCurrentMillHeat count = " + dtCurrentMetalStdCurrentMillHeat.Rows.Count);

                string sold_mss_sdo = string.Empty;
                string sold_mss_std_id = string.Empty;
                string sold_mss_addnl_id = string.Empty;


                string snew_mss_sdo = string.Empty;
                string snew_mss_std_id = string.Empty;
                string ssnew_mss_addnl_id = string.Empty;

                foreach (DataRow oDataRow in dtCurrentMetalStdCurrentMillHeat.Rows)
                {  
                    snew_mss_sdo = BusinessUtility.GetString(oDataRow["mss_sdo"]);
                    snew_mss_std_id = BusinessUtility.GetString(oDataRow["mss_std_id"]);
                    ssnew_mss_addnl_id = BusinessUtility.GetString(oDataRow["mss_addnl_id"]);

                    foreach (DataRow oDataRow1 in dtOldMetalStdCurrentMillHeat.Rows)
                    {
                        sold_mss_sdo = BusinessUtility.GetString(oDataRow1["mss_sdo"]);
                        sold_mss_std_id = BusinessUtility.GetString(oDataRow1["mss_std_id"]);
                        sold_mss_addnl_id = BusinessUtility.GetString(oDataRow1["mss_addnl_id"]);
                        lngOldQdsNo = BusinessUtility.GetLong(oDataRow1["hdrProductQds"]);
                        iOldFileId = BusinessUtility.GetInt(oDataRow1["FileId"]);
                        //sOldRefSi = BusinessUtility.GetString(dtOldMetalStdCurrentMillHeat.Rows[0]["ShipingRef"]);
                        iOldPo = BusinessUtility.GetInt(dtOldMetalStdCurrentMillHeat.Rows[0]["origPoNo"]);
                    }

                    if ((sold_mss_sdo == snew_mss_sdo) && (sold_mss_std_id == snew_mss_std_id) && (sold_mss_addnl_id == ssnew_mss_addnl_id))
                    {
                        bMatched = true;
                    }
                    else
                    {
                        bMatched = false;
                        break;
                    }
                }


                ErrorLog.createLog("Metal standard match = " + bMatched);
                EdiError.LogError(EdiFile, m_iFileId.ToString(), "CHECKHIST lngOldQdsNo = '" + iOldQDSFromTestCompare + "', CheckMetalStandardHistory lngOldQdsNo = " + lngOldQdsNo);
                EdiError.LogError(EdiFile, m_iFileId.ToString(), "CheckMetalStandardHistory lngOldQdsNo = '" + lngOldQdsNo + "', iOldFileId = " + iOldFileId);
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLog(ex);
            }
            return bMatched;
        }



        // Note : if object reference error comes then plzzz check conection already exists or not.
        // There is one qds based on one heat and mill
        //select count(*) from mchqds_rec where qds_heat = 'NF0236' and qds_mill = 'ND'
        public void CreateQDS()
        {
            ErrorLog.createLog("m_iFileId in QdsService = " + m_iFileId);
            ErrorLog.createLog("Creating qds is in progress..");
            try
            {
                oConfiguration = new Configuration(oFiles.VendorDunsNo, BusinessUtility.GetInt(oFiles.EdiFileType));
                GatewayAuthService.AuthenticationToken oAuthenticationToken = new GatewayAuthService.AuthenticationToken();
                //oAuthenticationToken = UserAuthenticationTest("MSU", "I", "LIV", "livmaj", "itechedi", "+~%5qcKm");
                oAuthenticationToken = UserAuthentication();
                if (oAuthenticationToken != null)
                {
                    if (oAuthenticationToken.value.Length > 10)
                    {
                        oheader863 = new header863(m_iFileId, m_iHeaderId);
                        QdsCreationService.ServiceMessages oServiceMessages = new QdsCreationService.ServiceMessages();
                        QdsCreationService.QdsServiceClient oQdsServiceClient = new QdsServiceClient();
                        // long qdsreturn =0L;
                        QdsCreationService.AuthenticationToken oQDSAuthenticationToken = new QdsCreationService.AuthenticationToken();
                        oQDSAuthenticationToken.username = oAuthenticationToken.username;
                        oQDSAuthenticationToken.value = oAuthenticationToken.value;
                        //QdsCreationService.CreateQdsResponse objResponse = new CreateQdsResponse(oQDSAuthenticationToken, oServiceMessages, qdsreturn);
                        //for generating random value
                        Random r = new Random();
                        int value = r.Next(1000);
                        QdsCreationService.CreateQdsInput oCreateQdsInput = new CreateQdsInput();
                        oCreateQdsInput.type = oheader863.Type;//
                        if (Configuration.ApplicationMode == Configuration.ApplicationType.LOCAL)
                        {
                            oCreateQdsInput.heat = oheader863.Heat + "-" + value.ToString("000");//
                        }
                        else
                        {
                            oCreateQdsInput.heat = oheader863.Heat;
                        }
                        //NLKI1559092 669007016
                        //NLKI1559092

                        oCreateQdsInput.origPoItem = BusinessUtility.GetInt(oheader863.OrigPoItem);         //pass origPOItem to map with multiple items of PO items    2020/03/05  Sumit
                        oCreateQdsInput.origPoItemSpecified = true;                                         //pass origPoItemSpecified as true to map with multiple items of PO items    2020/03/05  Sumit
                        oCreateQdsInput.apvdFlag = BusinessUtility.GetInt(oheader863.ApvdFlag);//
                        oCreateQdsInput.mill = oheader863.Mill; //
                        oCreateQdsInput.millOrdNo = oheader863.MillOrdNo;
                        //oCreateQdsInput.origPoDistribution = BusinessUtility.GetInt(oheader863.OrigPoDistribution);
                        oCreateQdsInput.origPoDistribution = 1;                                             //To pass origin PO Distribution as 1 by default 2020/03/19
                        oCreateQdsInput.origPoDistributionSpecified = true;
                        oCreateQdsInput.venId = oheader863.VenId;
                        oCreateQdsInput.origZone = this.GetStratixOriginZone(oCreateQdsInput.heat);
                        //To pass PO Number 2020/03/19  Sumit
                        ErrorLog.createLog("Adding Po Number in CreateQdsInput...");
                        oCreateQdsInput.origPoNo = Convert.ToInt32(oheader863.OrigPoNo);
                        oCreateQdsInput.origPoNoSpecified = true;
                        //To pass PO Number 2020/03/19  Sumit

                        if (oConfiguration.ApproveQds.Equals("1"))
                        {
                            oCreateQdsInput.apvdFlag = 1;
                        }

                        EdiError.LogError(EdiFile, m_iFileId.ToString(), "HEATQDS oCreateQdsInput.origZone = " + oCreateQdsInput.origZone, 0, 0, m_iHeaderId, 0, EdiError.ErrorLogType.INFO);

                        if (Configuration.ApplicationMode == Configuration.ApplicationType.LOCAL)
                        {
                            oCreateQdsInput.whs = "CHI";
                        }
                        else
                        {
                            oCreateQdsInput.whs = oheader863.Whs;
                        }
                        EdiError.ValidateWAREHOUSE(oheader863.Whs, "Inside qds creation", BusinessUtility.GetString(Globalcl.FileId));
                        oCreateQdsInput.uknownHeat = BusinessUtility.GetInt(oheader863.UknownHeat);//
                                                                                                   //QdsCreationService.CreateQdsRequest oCreateQdsRequest = new CreateQdsRequest(oQDSAuthenticationToken, oCreateQdsInput);
                        ErrorLog.createLog("Before Calling heat QDS service");
                        ErrorLog.createLog("Type=" + oCreateQdsInput.type);
                        ErrorLog.createLog("Heat=" + oCreateQdsInput.heat);
                        ErrorLog.createLog("apvdFlag=" + oCreateQdsInput.apvdFlag);
                        ErrorLog.createLog("mill=" + oCreateQdsInput.mill);
                        ErrorLog.createLog("millOrdNo=" + oCreateQdsInput.millOrdNo);
                        ErrorLog.createLog("origPoDistribution=" + oCreateQdsInput.origPoDistribution);
                        ErrorLog.createLog("venId=" + oCreateQdsInput.venId);
                        ErrorLog.createLog("whs=" + oCreateQdsInput.whs);
                        ErrorLog.createLog("oCreateQdsInput.apvdFlag=" + oCreateQdsInput.apvdFlag);
                        ErrorLog.createLog("oCreateQdsInput.origPoItem=" + oCreateQdsInput.origPoItem);
                        ErrorLog.createLog("oCreateQdsInput.origPoNo=" + oCreateQdsInput.origPoNo);
                        ErrorLog.createLog("oCreateQdsInput.origPoNoSpecified=" + oCreateQdsInput.origPoNoSpecified);

                        // TEST CHECK FOR OLD AND CURRENT FILE FOR CREATING PRODUCT QDS. WE CREATE PRD QDS ONLY IF MEATL STD CHANGE OR SHP REF CHNAGE OR TEST DIFF.
                        bool bTestMatched = this.CheckHistory(oCreateQdsInput.mill, oCreateQdsInput.heat, oheader863.ShipingRef, BusinessUtility.GetInt(oheader863.OrigPoNo), ref lngOldQds, ref iOldFileId);
                        Detail863 oDetail863 = new Detail863();

                        DataTable dtOldChemistry = oDetail863.GetCurrentPreviousChemistryDetail(oCreateQdsInput.mill, oCreateQdsInput.heat, m_iFileId, oheader863.ShipingRef);
                        DataTable dtCurrentChemistry = oDetail863.GetCurrentPreviousChemistryDetail(oCreateQdsInput.mill, oCreateQdsInput.heat, m_iFileId);

                        bool bChemMatch = true;
                        //oDetail863.CompareTwoChemistry(dtOldChemistry, dtCurrentChemistry, ref bChemMatch);



                        bool bTestFound = oDetail863.GetTestList(m_iFileId, m_iHeaderId, oConfiguration.VendorId).Rows.Count > 0;

                        bool bMetalStandardMatched = false;

                        // METAL STD AND SHIPPING REF NUMBER MATCH CHECK FOR OLD AND CURRENT FILE FOR CREATING PRODUCT QDS. WE CREATE PRD QDS ONLY IF MEATL STD CHANGE OR SHP REF CHNAGE OR TEST DIFF.
                        bMetalStandardMatched = this.CheckMetalStandardHistory(oheader863.ShipingRef, oCreateQdsInput.mill, oCreateQdsInput.heat, BusinessUtility.GetInt(oheader863.OrigPoNo), ref lngOldQds, ref iOldFileId);
                        ErrorLog.createLog("sOldRefSi = " + sOldRefSi + " oheader863.ShipingRef = " + oheader863.ShipingRef);
                        bShipRefNumberMatch = sOldRefSi == oheader863.ShipingRef;
                        bPOMatch = iOldPo == BusinessUtility.GetInt(oheader863.OrigPoNo);

                        ErrorLog.createLog("bTestMatched = " + bTestMatched);
                        EdiError.LogError(EdiFile, m_iFileId.ToString(), "bTestMatched = " + bTestMatched, 0, 0, m_iHeaderId, 0, EdiError.ErrorLogType.INFO);
                        ErrorLog.createLog("bShipRefNumberMatch = " + bShipRefNumberMatch);
                        EdiError.LogError(EdiFile, m_iFileId.ToString(), "bShipRefNumberMatch = " + bShipRefNumberMatch, 0, 0, m_iHeaderId, 0, EdiError.ErrorLogType.INFO);
                        ErrorLog.createLog("bMetalStandardMatched = " + bMetalStandardMatched);
                        EdiError.LogError(EdiFile, m_iFileId.ToString(), "bMetalStandardMatched = " + bMetalStandardMatched, 0, 0, m_iHeaderId, 0, EdiError.ErrorLogType.INFO);
                        ErrorLog.createLog("bPOMatch = " + bPOMatch);
                        EdiError.LogError(EdiFile, m_iFileId.ToString(), "bPOMatch = " + bPOMatch, 0, 0, m_iHeaderId, 0, EdiError.ErrorLogType.INFO);
                        if (bMetalStandardMatched)
                        {
                            EdiError.LogError(EdiFile, m_iFileId.ToString(), "Metal Standard Matched", 0, 0, m_iHeaderId, 0, EdiError.ErrorLogType.INFO, false);
                        }
                        else
                        {
                            EdiError.LogError(EdiFile, m_iFileId.ToString(), "Metal Standard did not Matched", 0, 0, m_iHeaderId, 0, EdiError.ErrorLogType.INFO, false);
                        }

                        EdiError.LogError(EdiFile, m_iFileId.ToString(), "HEAT :" + oCreateQdsInput.heat, 0, 0, m_iHeaderId, 0, EdiError.ErrorLogType.INFO);
                        EdiError.LogError(EdiFile, m_iFileId.ToString(), "MILL :" + oCreateQdsInput.mill, 0, 0, m_iHeaderId, 0, EdiError.ErrorLogType.INFO);
                        ErrorLog.createLog("oDetail863.GetTestList(m_iFileId, m_iHeaderId).Rows.Count = " + bTestFound);
                        if (!bTestFound)
                        {
                            EdiError.LogError(EdiFile, m_iFileId.ToString(), "Test not found in heat :" + oCreateQdsInput.heat, 0, 0, m_iHeaderId, 0, EdiError.ErrorLogType.INFO, false);
                        }
                        if (!string.IsNullOrEmpty(oCreateQdsInput.whs))
                        {

                            if (!this.CheckMillHeatExistance(BusinessUtility.GetString(oCreateQdsInput.mill), BusinessUtility.GetString(oCreateQdsInput.heat), ref lngExistingQds, ref lngRelaredReceipt))
                            {
                                EdiError.LogError(EdiFile, m_iFileId.ToString(), "Going to hit service to create heat qds in CreateQDS method", 0, 0, m_iHeaderId, 0, EdiError.ErrorLogType.INFO);
                                if (lngExistingQds == 0)   // it means heat qds not exists
                                {
                                    if (bChemMatch)
                                    {
                                        oServiceMessages = oQdsServiceClient.CreateQds(ref oQDSAuthenticationToken, oCreateQdsInput, out lngQdsNo);
                                    }
                                    else
                                    {
                                        EdiError.LogError(EdiFile, m_iFileId.ToString(), "Chemistry did not matched so skiping heat qds..", 0, 0, m_iHeaderId, 0, EdiError.ErrorLogType.ERROR, true);
                                        ErrorLog.createLog("Chemistry did not matched so skiping heat qds..");
                                    }
                                }
                                EdiError.LogError(EdiFile, m_iFileId.ToString(), "Heat qds success.. QDS NO : " + lngQdsNo, 0, 0, m_iHeaderId, 0, EdiError.ErrorLogType.INFO);
                                ErrorLog.createLog("Heat qds lngQdsNo = " + lngQdsNo);

                                if (lngQdsNo > 0)
                                {
                                    ErrorLog.createLog("lngQdsNo = " + lngQdsNo);
                                    this.UpdateStatusInMySqlDb(lngQdsNo, 0);
                                    this.UpdateQDSInHeader(lngQdsNo, 0);
                                    this.UpdateHeatQDSStatusHeader(lngQdsNo);
                                    if (oCreateQdsInput.type == Globalcl.ProductQDS)
                                    {
                                        this.UpdateTests(lngQdsNo, ref oQDSAuthenticationToken);
                                    }
                                    this.UpdateChemistry(lngQdsNo, ref oQDSAuthenticationToken);
                                    this.UpdateMetalStandard(oServiceMessages, oQdsServiceClient, ref oQDSAuthenticationToken, BusinessUtility.GetInt(oheader863.OrigPoNo), lngQdsNo, oCreateQdsInput.type);

                                    #region to update QdsExternalCertificate    2020/05/18  Sumit
                                    //bool bSuccess = false;
                                    //StringBuilder sberr = new StringBuilder();
                                    ////string spathh = Utility.GetConfigValue("Processed_PATH") + "\\" + oFiles.CertFileToUpload;
                                    ////ErrorLog.createLog("spathh = " + spathh);
                                    ////this.UpdateQdsExternalCertificate(ref oQDSAuthenticationToken, lngQdsNo, spathh, ref bSuccess, ref sberr);
                                    //ErrorLog.createLog("Getting Heat Qds External Certificate record detail for PO:" + oheader863.OrigPoNo + "     Heat:" + oheader863.Heat + "      VendorTagId:" + oheader863.VendorTagId);
                                    //ExternalCertificate.QdsExternalCertificateData qdsExCertData = new ExternalCertificate.QdsExternalCertificateData();
                                    //ExternalCertificate.QdsExternalCertificateData qdsCert;
                                    //qdsCert = qdsExCertData.GetSingleExternalCertificateRecord(oheader863.OrigPoNo, oheader863.Heat, oheader863.VendorTagId);
                                    //if(qdsCert != null)
                                    //{
                                    //    ErrorLog.createLog("External Certificate record found. Certificate File:" + Path.Combine(qdsCert.QdsExCert_FilePath, qdsCert.QdsExCert_Filename));
                                    //    this.UpdateQdsExternalCertificate(ref oQDSAuthenticationToken, lngQdsNo, Path.Combine(qdsCert.QdsExCert_FilePath, qdsCert.QdsExCert_Filename), ref bSuccess, ref sberr);
                                    //}
                                    //ErrorLog.createLog("cert update = " + sberr.ToString());
                                    #endregion to update QdsExternalCertificate    2020/05/18  Sumit
                                    lngQdsNo = 0;
                                }
                                // create product qds also if heat qds  exists..

                                #region Product Qds after heat creation

                                ErrorLog.createLog("lngOldQds = " + lngOldQds + " , iOldFileId = " + iOldFileId);
                                if (!bShipRefNumberMatch || !bTestMatched || !bMetalStandardMatched || !bPOMatch)
                                {

                                    int iQdsForPoIncurrentFile = 0;
                                    header863 oheader8631 = new header863();
                                    oheader8631.CheckPoExistanceForCurrentFile(m_iFileId, BusinessUtility.GetInt(oheader863.OrigPoNo), oCreateQdsInput.heat, oheader863.ShipingRef, ref iQdsForPoIncurrentFile);

                                    //if (iQdsForPoIncurrentFile == 0)
                                    //{

                                    int iPocount = GetPoCount(BusinessUtility.GetInt(oheader863.OrigPoNo));

                                    if (iPocount == 1 || iPocount > 0)
                                    {

                                        //if (!bMetalStandardMatched)
                                        if (bTestFound)
                                        {
                                            ErrorLog.createLog("Creating product qds after heat qds creation");

                                            oCreateQdsInput.type = Globalcl.ProductQDS;
                                            oCreateQdsInput.origPoNo = BusinessUtility.GetInt(oheader863.OrigPoNo);
                                            oCreateQdsInput.origPoNoSpecified = true;
                                            //oCreateQdsInput.origPoItem = 1;                                               //commented code and passing header863 poitem number for multiple item qds  2020/03/05  Sumit
                                            oCreateQdsInput.origPoItem = BusinessUtility.GetInt(oheader863.OrigPoItem);     
                                            oCreateQdsInput.origPoItemSpecified = true;
                                            oCreateQdsInput.origPoDistribution = 1;
                                            oCreateQdsInput.origPoDistributionSpecified = true;
                                            ErrorLog.createLog("origPoNo = " + oCreateQdsInput.origPoNo);
                                            oCreateQdsInput.origZone = this.GetStratixOriginZone(oCreateQdsInput.heat);
                                            if (oConfiguration.ApproveQds.Equals("1"))
                                            {
                                                oCreateQdsInput.apvdFlag = 1;
                                            }

                                            ErrorLog.createLog("Before Calling product QDS service");
                                            ErrorLog.createLog("Type=" + oCreateQdsInput.type);
                                            ErrorLog.createLog("Heat=" + oCreateQdsInput.heat);
                                            ErrorLog.createLog("oCreateQdsInput.origPoNo=" + oCreateQdsInput.origPoNo);
                                            ErrorLog.createLog("apvdFlag=" + oCreateQdsInput.apvdFlag);
                                            ErrorLog.createLog("mill=" + oCreateQdsInput.mill);
                                            ErrorLog.createLog("millOrdNo=" + oCreateQdsInput.millOrdNo);
                                            ErrorLog.createLog("origPoDistribution=" + oCreateQdsInput.origPoDistribution);
                                            ErrorLog.createLog("venId=" + oCreateQdsInput.venId);
                                            ErrorLog.createLog("whs=" + oCreateQdsInput.whs);
                                            ErrorLog.createLog("oCreateQdsInput.apvdFlag=" + oCreateQdsInput.apvdFlag);



                                            ErrorLog.createLog("oCreateQdsInput.apvdFlag=" + oCreateQdsInput.apvdFlag);

                                            EdiError.LogError(EdiFile, m_iFileId.ToString(), "PRODUCTQDS oCreateQdsInput.origZone = " + oCreateQdsInput.origZone, 0, 0, m_iHeaderId, 0, EdiError.ErrorLogType.INFO);

                                            ErrorLog.createLog("oCreateQdsInput.origZone = " + oCreateQdsInput.origZone);
                                            EdiError.LogError(EdiFile, m_iFileId.ToString(), "Product qds service hit after heat qds creation", 0, 0, m_iHeaderId, 0, EdiError.ErrorLogType.INFO);

                                            long tempqds = 0;
                                            if (this.CheckMillHeatExistance(BusinessUtility.GetString(oCreateQdsInput.mill), BusinessUtility.GetString(oCreateQdsInput.heat), ref tempqds, ref lngRelaredReceipt))
                                            {
                                                oServiceMessages = oQdsServiceClient.CreateQds(ref oQDSAuthenticationToken, oCreateQdsInput, out lngProductQdsNo);
                                            }
                                            else
                                            {
                                                EdiError.LogError(EdiFile, m_iFileId.ToString(), "Heat qds not exist so skiping product qds", 0, 0, m_iHeaderId, 0, EdiError.ErrorLogType.ERROR, true);
                                            }

                                            ErrorLog.createLog("oServiceMessages.returnStatus for product qds = " + oServiceMessages.returnStatus);
                                            ErrorLog.createLog("product qds lngProductQdsNo = " + lngProductQdsNo);
                                            EdiError.LogError(EdiFile, m_iFileId.ToString(), "product qds after heat = " + lngProductQdsNo, 0, 0, m_iHeaderId, 0, EdiError.ErrorLogType.INFO);



                                            if (lngProductQdsNo > 0)
                                            {
                                                UpdateQdsInput oUpdateQdsInput = new UpdateQdsInput();
                                                oUpdateQdsInput.qdsNumber = lngProductQdsNo;
                                                oUpdateQdsInput.qdsNumberSpecified = true;
                                                oUpdateQdsInput.linkType = 3;
                                                oUpdateQdsInput.itmCtlNo = 0;
                                                oUpdateQdsInput.itmCtlNoSpecified = true;
                                                oUpdateQdsInput.tagNo = "";
                                                oServiceMessages = oQdsServiceClient.UpdateQds(ref oQDSAuthenticationToken, oUpdateQdsInput, out lngProductQdsNo);

                                                ErrorLog.createLog("oQdsServiceClient.UpdateQds for product qds = " + oServiceMessages.returnStatus);
                                                ErrorLog.createLog("UpdateQds product qds lngProductQdsNo = " + lngProductQdsNo);

                                                this.UpdateVendorTagId(ref oQDSAuthenticationToken, ref oServiceMessages, ref oQdsServiceClient, lngProductQdsNo);
                                         
                                                ErrorLog.createLog("lngProductQdsNo = " + lngProductQdsNo);
                                                this.UpdateStatusInMySqlDb(0, lngProductQdsNo);
                                                this.UpdateQDSInHeader(0, lngProductQdsNo);
                                                if (oCreateQdsInput.type == Globalcl.ProductQDS)
                                                {
                                                    this.UpdateTests(lngProductQdsNo, ref oQDSAuthenticationToken);
                                                }
                                                this.UpdateChemistry(lngProductQdsNo, ref oQDSAuthenticationToken);
                                                this.UpdateMetalStandard(oServiceMessages, oQdsServiceClient, ref oQDSAuthenticationToken, BusinessUtility.GetInt(oheader863.OrigPoNo), lngProductQdsNo, oCreateQdsInput.type);

                                                // Update cert..
                                                #region to update QdsExternalCertificate    2020/05/18  Sumit
                                                //bool bSuccess = false;
                                                //StringBuilder sberr = new StringBuilder();
                                                ////string spathh = Utility.GetConfigValue("Processed_PATH") + "\\" + oFiles.CertFileToUpload;
                                                ////ErrorLog.createLog("spathh = " + spathh);
                                                ////this.UpdateQdsExternalCertificate(ref oQDSAuthenticationToken, lngProductQdsNo, spathh, ref bSuccess, ref sberr);
                                                //ErrorLog.createLog("Getting Product Qds External Certificate record detail for PO:" + oheader863.OrigPoNo + "     Heat:" + oheader863.Heat + "      VendorTagId:" + oheader863.VendorTagId);
                                                //ExternalCertificate.QdsExternalCertificateData qdsExCertData = new ExternalCertificate.QdsExternalCertificateData();
                                                //ExternalCertificate.QdsExternalCertificateData qdsCert;
                                                //qdsCert = qdsExCertData.GetSingleExternalCertificateRecord(oheader863.OrigPoNo, oheader863.Heat, oheader863.VendorTagId);
                                                //if (qdsCert != null)
                                                //{
                                                //    ErrorLog.createLog("External Certificate record found. Certificate File:" + Path.Combine(qdsCert.QdsExCert_FilePath, qdsCert.QdsExCert_Filename));
                                                //    this.UpdateQdsExternalCertificate(ref oQDSAuthenticationToken, lngProductQdsNo, Path.Combine(qdsCert.QdsExCert_FilePath, qdsCert.QdsExCert_Filename), ref bSuccess, ref sberr);
                                                //}
                                                //ErrorLog.createLog("cert update = " + sberr.ToString());
                                                #endregion to update QdsExternalCertificate    2020/05/18  Sumit

                                                lngProductQdsNo = 0;
                                            }

                                            ErrorLog.createLog("Creating product qds after heat qds creation end here");
                                        }
                                    }
                                    else if (iPocount > 1)
                                    {
                                        EdiError.LogError("863", m_iFileId.ToString(), ConfigurationManager.AppSettings["MulPoError"], 0, 0, m_iHeaderId, 0, EdiError.ErrorLogType.ERROR, true);
                                        ErrorLog.createLog("mul po error > 1");
                                    }
                                    else if (iPocount == 0)
                                    {
                                        EdiError.LogError("863", m_iFileId.ToString(), ConfigurationManager.AppSettings["PoNotExist"], 0, 0, m_iHeaderId, 0, EdiError.ErrorLogType.ERROR, true);
                                        ErrorLog.createLog("mul po error == 0");
                                    }


                                }
                                else
                                {
                                    // update vendor tag and qds reference here here.
                                    this.UpdateQdsReferenceNo(iOldFileId, lngOldQds);
                                    if (lngOldQds > 0)
                                    {
                                        this.UpdateVendorTagId(ref oQDSAuthenticationToken, ref oServiceMessages, ref oQdsServiceClient, lngOldQds);
                                    }

                                    EdiError.LogError("863", m_iFileId.ToString(), "Test entry match with previous fileId = " + iOldFileId + " OldQDS = " + lngOldQds, 0, 0, m_iHeaderId, 0, EdiError.ErrorLogType.WARNING, true);
                                    EdiError.LogError("863", m_iFileId.ToString(), "VENDOR TAG ID : " + oheader863.VendorTagId + " UPDATED IN OLD QDS NO = " + lngOldQds + "AND FILE ID :" + iOldFileId, 0, 0, m_iHeaderId, 0, EdiError.ErrorLogType.WARNING, true);
                                }

                                #endregion
                            } // heat qds found check block end here
                            else
                            {
                                //Control comes here if heat qds exists in stratix system.
                                EdiError.LogError("863", m_iFileId.ToString(), "Heat exists in stratix system", 0, 0, m_iHeaderId, 0, EdiError.ErrorLogType.INFO, false);


                                #region updating chemistry block in heat qds
                                //update chemistry detail to heat qds found earlier.
                                lngQdsNo = lngExistingQds;
                                //EdiError.LogError(EdiFile, m_iFileId.ToString(), "Updating chemistry detail to heat qds found earlier, heat qds no :" + lngQdsNo);

                                if (lngQdsNo > 0)
                                {
                                    ErrorLog.createLog("Existing Heat qds of STRATIX. lngQdsNo = " + lngQdsNo);
                                    this.UpdateStatusInMySqlDb(lngQdsNo, 0);
                                    this.UpdateQDSInHeader(lngQdsNo, 0, true);

                                    if (this.CheckHeatQdsExistForMillHeat(BusinessUtility.GetString(oCreateQdsInput.mill), BusinessUtility.GetString(oCreateQdsInput.heat)))
                                    {
                                        UpdateHeatQDSStatusHeader(lngQdsNo);
                                    }

                                    lngQdsNo = 0;
                                    lngExistingQds = 0;
                                }

                                #endregion

                                #region Product qds if heat exist in system

                                EdiError.LogError("863", m_iFileId.ToString(), "Heat exists... lngOldQds = " + lngOldQds + " iOldFileId = " + iOldFileId + " sOldRefSi = " + sOldRefSi, 0, 0, m_iHeaderId, 0, EdiError.ErrorLogType.INFO);
                                ErrorLog.createLog("bMetalStandardMatched = " + bMetalStandardMatched);
                                // lngOldQds is old product qds exists in our db and  iOldFileId is file id of that old product qds.
                                if (!bShipRefNumberMatch || !bTestMatched || !bMetalStandardMatched || !bPOMatch)
                                {
                                    ErrorLog.createLog("Heat exists... lngOldQds = " + lngOldQds + " iOldFileId = " + iOldFileId + " sOldRefSi = " + sOldRefSi);

                                    int iPocount = GetPoCount(BusinessUtility.GetInt(oheader863.OrigPoNo));

                                    int iQdsForPoIncurrentFile = 0;
                                    header863 oheader8631 = new header863();
                                    oheader8631.CheckPoExistanceForCurrentFile(m_iFileId, BusinessUtility.GetInt(oheader863.OrigPoNo), oCreateQdsInput.heat, oheader863.ShipingRef, ref iQdsForPoIncurrentFile);

                                    //if (iQdsForPoIncurrentFile == 0)
                                    //{                                                         


                                    if (iPocount == 1 || iPocount > 0)
                                    {
                                        // create product qds if heat exists...
                                        // if (!bMetalStandardMatched)
                                        if (bTestFound)
                                        {
                                            ErrorLog.createLog("Create product qds");

                                            oCreateQdsInput.type = Globalcl.ProductQDS;
                                            oCreateQdsInput.origPoNo = BusinessUtility.GetInt(oheader863.OrigPoNo);
                                            oCreateQdsInput.origPoNoSpecified = true;
                                            //oCreateQdsInput.origPoItem = 1;                                               //commented code and passing header863 poitem number for multiple item qds  2020/03/05  Sumit
                                            oCreateQdsInput.origPoItem = BusinessUtility.GetInt(oheader863.OrigPoItem);
                                            oCreateQdsInput.origPoItemSpecified = true;
                                            oCreateQdsInput.origPoDistribution = 1;
                                            oCreateQdsInput.origPoDistributionSpecified = true;
                                            if (oConfiguration.ApproveQds.Equals("1"))
                                            {
                                                oCreateQdsInput.apvdFlag = 1;
                                            }
                                            ErrorLog.createLog("oCreateQdsInput.apvdFlag=" + oCreateQdsInput.apvdFlag);
                                            ErrorLog.createLog("origPoNo = " + oCreateQdsInput.origPoNo);

                                            oCreateQdsInput.origZone = this.GetStratixOriginZone(oCreateQdsInput.heat);
                                            EdiError.LogError(EdiFile, m_iFileId.ToString(), "PRODUCTQDS oCreateQdsInput.origZone = " + oCreateQdsInput.origZone, 0, 0, m_iHeaderId, 0, EdiError.ErrorLogType.INFO);
                                            ErrorLog.createLog("oCreateQdsInput.origZone = " + oCreateQdsInput.origZone);
                                            EdiError.LogError(EdiFile, m_iFileId.ToString(), "Product qds service hit when heat exist in stratix system. so creation product qds only", 0, 0, m_iHeaderId, 0, EdiError.ErrorLogType.INFO);

                                            long tempqds = 0;
                                            if (this.CheckMillHeatExistance(BusinessUtility.GetString(oCreateQdsInput.mill), BusinessUtility.GetString(oCreateQdsInput.heat), ref tempqds, ref lngRelaredReceipt))
                                            {
                                                oServiceMessages = oQdsServiceClient.CreateQds(ref oQDSAuthenticationToken, oCreateQdsInput, out lngProductQdsNo);
                                            }
                                            else
                                            {
                                                EdiError.LogError(EdiFile, m_iFileId.ToString(), "Heat qds not exist so skiping product qds", 0, 0, m_iHeaderId, 0, EdiError.ErrorLogType.ERROR, true);
                                            }


                                            ErrorLog.createLog("oServiceMessages.returnStatus for product qds = " + oServiceMessages.returnStatus);
                                            ErrorLog.createLog("product qds lngProductQdsNo = " + lngProductQdsNo);
                                            EdiError.LogError(EdiFile, m_iFileId.ToString(), "product qds only = " + lngProductQdsNo, 0, 0, m_iHeaderId, 0, EdiError.ErrorLogType.INFO);



                                            if (lngProductQdsNo > 0)
                                            {
                                                //UpdateVendorTagIdInput oUpdateVendorTagIdInput = new UpdateVendorTagIdInput();
                                                //oUpdateVendorTagIdInput.vendorTagId = oheader863.VendorTagId;
                                                //oServiceMessages = oQdsServiceClient.UpdateVendorTagId(ref oQDSAuthenticationToken, lngProductQdsNo, oUpdateVendorTagIdInput);

                                                //ErrorLog.createLog("oQdsServiceClient.UpdateVendorTagId for product qds = " + oServiceMessages.returnStatus);
                                                //ErrorLog.createLog("UpdateVendorTagId product qds lngProductQdsNo = " + lngProductQdsNo);


                                                UpdateQdsInput oUpdateQdsInput = new UpdateQdsInput();
                                                oUpdateQdsInput.qdsNumber = lngProductQdsNo;
                                                oUpdateQdsInput.qdsNumberSpecified = true;
                                                oUpdateQdsInput.linkType = 3;
                                                oUpdateQdsInput.itmCtlNo = 0;
                                                oUpdateQdsInput.itmCtlNoSpecified = true;
                                                oUpdateQdsInput.tagNo = "";
                                                oServiceMessages = oQdsServiceClient.UpdateQds(ref oQDSAuthenticationToken, oUpdateQdsInput, out lngProductQdsNo);

                                                ErrorLog.createLog("oQdsServiceClient.UpdateQds for product qds = " + oServiceMessages.returnStatus);
                                                ErrorLog.createLog("UpdateQds product qds lngProductQdsNo = " + lngProductQdsNo);


                                                this.UpdateVendorTagId(ref oQDSAuthenticationToken, ref oServiceMessages, ref oQdsServiceClient, lngProductQdsNo);
                                            }

                                            if (lngProductQdsNo > 0)
                                            {
                                                ErrorLog.createLog("lngProductQdsNo = " + lngProductQdsNo);
                                                this.UpdateStatusInMySqlDb(0, lngProductQdsNo);
                                                this.UpdateQDSInHeader(0, lngProductQdsNo);
                                                if (oCreateQdsInput.type == Globalcl.ProductQDS)
                                                {
                                                    this.UpdateTests(lngProductQdsNo, ref oQDSAuthenticationToken);
                                                }
                                                this.UpdateChemistry(lngProductQdsNo, ref oQDSAuthenticationToken);
                                                this.UpdateMetalStandard(oServiceMessages, oQdsServiceClient, ref oQDSAuthenticationToken, BusinessUtility.GetInt(oheader863.OrigPoNo), lngProductQdsNo, oCreateQdsInput.type);
                                                //bool bSuccess = false;
                                                //StringBuilder sberr = new StringBuilder();
                                                //string spathh = Utility.GetConfigValue("Processed_PATH") + "\\" + oFiles.CertFileToUpload;
                                                //ErrorLog.createLog("spathh = " + spathh);
                                                //this.UpdateQdsExternalCertificate(ref oQDSAuthenticationToken, lngProductQdsNo, spathh, ref bSuccess, ref sberr);
                                                //ErrorLog.createLog("cert update = " + sberr.ToString());
                                                lngProductQdsNo = 0;
                                            }
                                            ErrorLog.createLog("QDS EXISTS FOR GIVEN MILL AND HEAT");
                                        }
                                        else
                                        {
                                            EdiError.LogError(EdiFile, m_iFileId.ToString(), "Product qds not created as test are not available in edi file.", 0, 0, m_iHeaderId, 0, EdiError.ErrorLogType.WARNING, false);
                                        }
                                    }
                                    else
                                    {
                                        EdiError.LogError("863", m_iFileId.ToString(), ConfigurationManager.AppSettings["MulPoError"], 0, 0, m_iHeaderId, 0, EdiError.ErrorLogType.ERROR, true);
                                    }


                                }
                                else
                                {
                                    // update vendor tag here. if test match
                                    this.UpdateQdsReferenceNo(iOldFileId, lngOldQds);

                                    if (lngOldQds > 0)
                                    {
                                        this.UpdateVendorTagId(ref oQDSAuthenticationToken, ref oServiceMessages, ref oQdsServiceClient, lngOldQds);
                                    }

                                    EdiError.LogError("863", m_iFileId.ToString(), "Test entry or metal standerd match with previous fileId = " + iOldFileId + " OldQDS = " + lngOldQds, 0, 0, m_iHeaderId, 0, EdiError.ErrorLogType.WARNING, true);
                                    EdiError.LogError("863", m_iFileId.ToString(), "VENDOR TAG ID : " + oheader863.VendorTagId + " UPDATED IN OLD QDS NO = " + lngOldQds + "AND FILE ID :" + iOldFileId, 0, 0, m_iHeaderId, 0, EdiError.ErrorLogType.WARNING, true);
                                }

                                #endregion

                            }
                        }
                        else
                        {
                            ErrorLog.createLog("Po not found");
                            EdiError.LogError("863", m_iFileId.ToString(), "", null, "Po not found", 0, 0, m_iHeaderId, 0, EdiError.ErrorLogType.ERROR);
                            EdiError.SendErrorMail(Globalcl.FileId, "Po not found in QDS", "Po not found in QDS");
                        }

                        ErrorLog.createLog("after Calling QDS service");
                        UserAuthenticationLogOut(oAuthenticationToken);
                    }
                }
                else
                {
                    ErrorLog.createLog("Authention object is null check authentication param");
                }

            }

            catch (Exception ex)
            {
                ErrorLog.createLog("Error in webservice qds creation..");
                EdiError.LogError(EdiFile, m_iFileId.ToString(), "Error occured in creating qds or methods after qds creation..", 0, 0, m_iHeaderId, 0, EdiError.ErrorLogType.ERROR,true);
                ErrorLog.CreateLog(ex);
                EdiError.LogError(EdiFile, m_iFileId.ToString(), "", ex, "", 0, 0, m_iHeaderId, 0, EdiError.ErrorLogType.ERROR);
                UserAuthenticationLogOut(oAuthenticationToken);
                EdiError.LogLatestSctmsgError(m_iFileId, 863, m_iHeaderId);
            }
        }


        public void UpdateVendorTagId(ref AuthenticationToken AuthenticationHeader, ref QdsCreationService.ServiceMessages oServiceMessages, ref QdsCreationService.QdsServiceClient oQdsServiceClient, long qdsNumber)
        {
            try
            {
                if (!string.IsNullOrEmpty(oheader863.VendorTagId))
                {
                    if (qdsNumber > 0)
                    {
                        if (!tctipd.VenTagExistsForQds(qdsNumber, oheader863.VendorTagId) && oheader863.VendorTagId.Length <= 15)
                        {
                            UpdateVendorTagIdInput oUpdateVendorTagIdInput = new UpdateVendorTagIdInput();
                            oUpdateVendorTagIdInput.vendorTagId = oheader863.VendorTagId;
                            oServiceMessages = oQdsServiceClient.UpdateVendorTagId(ref AuthenticationHeader, qdsNumber, oUpdateVendorTagIdInput);

                            ErrorLog.createLog("oQdsServiceClient.UpdateVendorTagId status for product qds: " + qdsNumber + " = " + oServiceMessages.returnStatus);
                            ErrorLog.createLog("UpdateVendorTagId product qds  = " + qdsNumber);
                        }
                        else
                        {
                            ErrorLog.createLog("Vendor tag already exists for product qds or length is greater then 15 = " + qdsNumber);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLog(ex);
                EdiError.LogLatestSctmsgError(m_iFileId, 863, m_iHeaderId);
            }
        }

        
        public void UpdateQdsExternalCertificate(ref QdsCreationService.AuthenticationToken oQDSAuthenticationToken, long lQds, string sFullPath, ref bool bSuccess, ref StringBuilder sbErrorMsg)
        {
            try
            {
                if (File.Exists(sFullPath))
                {
                    byte[] image = System.IO.File.ReadAllBytes(sFullPath);
                    //image = FileToByteArray(sFullPath);
                    //var temp = System.IO.File.ReadAllBytes(sFullPath);
                    //string encodedFile = Convert.ToBase64String(temp);
                    //var decodedFileBytes = Convert.FromBase64String(encodedFile);

                    UpdateQdsExternalCertificatesInput oUpdateQdsExternalCertificatesInput = new UpdateQdsExternalCertificatesInput();
                    oUpdateQdsExternalCertificatesInput.certificateType = "MTC";
                    oUpdateQdsExternalCertificatesInput.sdo = "ASTM";
                    oUpdateQdsExternalCertificatesInput.externalSystem = 0;
                    oUpdateQdsExternalCertificatesInput.image = image;
                    oUpdateQdsExternalCertificatesInput.imgFormat = "PDF";
                    oUpdateQdsExternalCertificatesInput.docType = "ETC";
                    oUpdateQdsExternalCertificatesInput.docMgmtGroup = "MTR";

                    #region 2020/05/18  For Testing Purpose Only    Sumit
                    //oUpdateQdsExternalCertificatesInput.certificateType = Utility.GetConfigValue("CertType");
                    //oUpdateQdsExternalCertificatesInput.sdo = Utility.GetConfigValue("CertSdo");
                    //oUpdateQdsExternalCertificatesInput.externalSystem = BusinessUtility.GetInt(Utility.GetConfigValue("CertExternalSystem"));
                    //oUpdateQdsExternalCertificatesInput.image = image;
                    //oUpdateQdsExternalCertificatesInput.imgFormat = Utility.GetConfigValue("CertImageFormat");
                    //oUpdateQdsExternalCertificatesInput.docType = Utility.GetConfigValue("CertDocType");
                    //oUpdateQdsExternalCertificatesInput.docMgmtGroup = Utility.GetConfigValue("CertDocMgmtGrp");
                    //oUpdateQdsExternalCertificatesInput.imageNumber = 2;
                    //oUpdateQdsExternalCertificatesInput.imageNumberSpecified = true;
                    //oUpdateQdsExternalCertificatesInput.vendorCertifcateReference = "iTech";
                    #endregion 2020/05/18  For Testing Purpose Only    Sumit

                    ErrorLog.createLog("Before Attaching Certificate Number...");
                    ErrorLog.createLog("File Name:" + sFullPath);
                    ErrorLog.createLog("Qds Number:" + lQds.ToString());
                    ErrorLog.createLog("oUpdateQdsExternalCertificatesInput.certificateType:" + oUpdateQdsExternalCertificatesInput.certificateType);
                    ErrorLog.createLog("oUpdateQdsExternalCertificatesInput.sdo:" + oUpdateQdsExternalCertificatesInput.sdo);
                    ErrorLog.createLog("oUpdateQdsExternalCertificatesInput.externalSystem:" + oUpdateQdsExternalCertificatesInput.externalSystem);
                    ErrorLog.createLog("oUpdateQdsExternalCertificatesInput.imgFormat:" + oUpdateQdsExternalCertificatesInput.imgFormat);
                    ErrorLog.createLog("oUpdateQdsExternalCertificatesInput.docType:" + oUpdateQdsExternalCertificatesInput.docType);
                    ErrorLog.createLog("oUpdateQdsExternalCertificatesInput.docMgmtGroup:" + oUpdateQdsExternalCertificatesInput.docMgmtGroup);
                    //ErrorLog.createLog("oUpdateQdsExternalCertificatesInput.vendorCertifcateReference:" + oUpdateQdsExternalCertificatesInput.vendorCertifcateReference);

                    QdsServiceClient oQdsServiceClient = new QdsServiceClient();
                    ServiceMessages oServiceMessages = oQdsServiceClient.UpdateQdsExternalCertificates(ref oQDSAuthenticationToken, lQds, oUpdateQdsExternalCertificatesInput);
                    ErrorLog.createLog("update cert status :" + oServiceMessages.returnStatus);
                    bSuccess = true;
                }
                else
                {
                    sbErrorMsg.Append("File not exists : " + sFullPath);
                    ErrorLog.createLog("File not exists : " + sFullPath);
                    bSuccess = false;
                }
            }
            catch (Exception ex)
            {
                if (sbErrorMsg.Length > 4)
                {

                }

                sbErrorMsg.AppendLine();
                sbErrorMsg.Append(ex.ToString());
                ErrorLog.createLog("Error in UpdateQdsExternalCertificate..");
                ErrorLog.CreateLog(ex);
                bSuccess = false;
            }
        }


        public void UpdateMetalStandard(QdsCreationService.ServiceMessages oServiceMessages, QdsCreationService.QdsServiceClient oQdsServiceClient, ref QdsCreationService.AuthenticationToken oQDSAuthenticationToken, int iPo, long lQds, string sQdsType)
        {
            try
            {
                EdiError.LogError(EdiFile, m_iFileId.ToString(), "UpdateMetalStandard execution start", 0, 0, m_iHeaderId, 0, EdiError.ErrorLogType.INFO);
                tctipd otctipd = new tctipd();
                otctipd.PopulateMaterialStanderd(iPo);

                if (!string.IsNullOrEmpty(otctipd.Sdo))
                {

                    UpdateQdsMetalStandardInput oUpdateQdsMetalStandardInput = new UpdateQdsMetalStandardInput();
                    oUpdateQdsMetalStandardInput.sdo = otctipd.Sdo;
                    oUpdateQdsMetalStandardInput.stdId = otctipd.StdId;
                    oUpdateQdsMetalStandardInput.addnlId = otctipd.AddnlId;

                    oUpdateQdsMetalStandardInput.matchingChemistry = "NA";
                    oUpdateQdsMetalStandardInput.matchingJominy = "NA";
                    oUpdateQdsMetalStandardInput.matchingTest = "NA";

                    //if (sQdsType == Globalcl.HeatQDS)
                    //{
                    //    oUpdateQdsMetalStandardInput.matchingChemistry = "Y";
                    //    oUpdateQdsMetalStandardInput.matchingJominy = "NA";
                    //    oUpdateQdsMetalStandardInput.matchingTest = "NA";
                    //}
                    //else if (sQdsType == Globalcl.ProductQDS)
                    //{
                    //    oUpdateQdsMetalStandardInput.matchingChemistry = "Y";
                    //    oUpdateQdsMetalStandardInput.matchingJominy = "NA";
                    //    oUpdateQdsMetalStandardInput.matchingTest = "Y";
                    //}

                    oServiceMessages = oQdsServiceClient.UpdateQdsMetalStandard(ref oQDSAuthenticationToken, lQds, oUpdateQdsMetalStandardInput);


                    ErrorLog.createLog("oQdsServiceClient.UpdateQdsMetalStandard for  qds = " + oServiceMessages.returnStatus);
                    ErrorLog.createLog("UpdateQdsMetalStandard  qds lngQdsNo = " + lQds);
                    ErrorLog.createLog("UpdateQdsMetalStandard  po = " + iPo);
                    EdiError.LogError(EdiFile, m_iFileId.ToString(), "UpdateMetalStandard status = " + oServiceMessages.returnStatus, 0, 0, m_iHeaderId, 0, EdiError.ErrorLogType.INFO);
                }
                else
                {
                    ErrorLog.createLog("Metal std not exist");
                    EdiError.SendErrorMail(m_iFileId, "Metal std not exist", "Metal std not exist for qds : " + lQds);
                }

            }
            catch (Exception ex)
            {
                ErrorLog.createLog("Error in UpdateMetalStandard..");
                EdiError.LogError(EdiFile, m_iFileId.ToString(), "Error in UpdateMetalStandard..", 0, 0, m_iHeaderId, 0, EdiError.ErrorLogType.INFO);
                ErrorLog.CreateLog(ex);
                EdiError.LogLatestSctmsgError(m_iFileId, 863, m_iHeaderId);
            }
        }


        public void UpdateStatusInMySqlDb(long lHeatQds, long lProductQds)
        {
            if (lHeatQds > 0)
            {
                m_sSql = "update  files SET QdsNo = '" + lHeatQds + "' where FileId ='" + m_iFileId + "'";
            }
            else if (lProductQds > 0)
            {
                m_sSql = "update  files SET ProductQds = '" + lProductQds + "' where FileId ='" + m_iFileId + "'";
            }

            iTECH.Library.DataAccess.MySql.DbHelper oDbHelper = new iTECH.Library.DataAccess.MySql.DbHelper(sMysqlConnectionString, true);
            try
            {
                MySqlParameter[] oMySqlParameter = { };
                int iStatus = oDbHelper.ExecuteNonQuery(m_sSql, CommandType.Text, oMySqlParameter);
            }
            catch (Exception ex)
            {
                EdiError.LogError("863", Globalcl.FileId.ToString(), "", ex, "UpdateStatusInMySqlDb");
                ErrorLog.CreateLog(ex);
            }
            finally
            {
                oDbHelper.CloseDatabaseConnection();
            }
        }

        public void UpdateQDSInHeader(long lHeatQds, long lProductQds, bool bExistsInStratix = false)
        {
            if (lHeatQds > 0)
            {
                if (bExistsInStratix)
                {
                    m_sSql = "update header863 set QdsCreatedByStratix = 1, hdrHeatQds = '" + lHeatQds + "' WHERE FileId = '" + m_iFileId + "' and ID = " + m_iHeaderId + " ";
                }
                else
                {
                    m_sSql = "update header863 set hdrHeatQds = '" + lHeatQds + "' WHERE FileId = '" + m_iFileId + "' and ID = " + m_iHeaderId + " ";
                }
               
            }
            else if (lProductQds > 0)
            {
                if (bExistsInStratix)
                {
                    m_sSql = "update header863 set QdsCreatedByStratix = 1, hdrProductQds = '" + lProductQds + "' WHERE FileId = '" + m_iFileId + "' and ID = " + m_iHeaderId + " ";
                }
                else
                {
                    m_sSql = "update header863 set hdrProductQds = '" + lProductQds + "' WHERE FileId = '" + m_iFileId + "' and ID = " + m_iHeaderId + " ";
                }
            }

            iTECH.Library.DataAccess.MySql.DbHelper oDbHelper = new iTECH.Library.DataAccess.MySql.DbHelper(sMysqlConnectionString, true);
            try
            {
                MySqlParameter[] oMySqlParameter = { };
                int iStatus = oDbHelper.ExecuteNonQuery(m_sSql, CommandType.Text, oMySqlParameter);
            }
            catch (Exception ex)
            {
                EdiError.LogError("863", Globalcl.FileId.ToString(), "", ex, "UpdateQDSInHeader");
                ErrorLog.CreateLog(ex);
            }
            finally
            {
                oDbHelper.CloseDatabaseConnection();
            }
        }


        public void UpdateHeatQDSStatusHeader(long lHeatQds)
        {
            if (lHeatQds > 0)
            {
                m_sSql = "update header863 set QdsCreatedByEdi = 1, hdrHeatQds = '" + lHeatQds + "' WHERE FileId = '" + m_iFileId + "' and ID = " + m_iHeaderId + " ";
            }
            iTECH.Library.DataAccess.MySql.DbHelper oDbHelper = new iTECH.Library.DataAccess.MySql.DbHelper(sMysqlConnectionString, true);
            try
            {
                MySqlParameter[] oMySqlParameter = { };
                int iStatus = oDbHelper.ExecuteNonQuery(m_sSql, CommandType.Text, oMySqlParameter);
            }
            catch (Exception ex)
            {
                EdiError.LogError("863", Globalcl.FileId.ToString(), "", ex, "UpdateHeatQDSStatusHeader error");
                ErrorLog.CreateLog(ex);
            }
            finally
            {
                oDbHelper.CloseDatabaseConnection();
            }
        }

        public bool CheckHeatQdsExistForMillHeat(string sMill, string sHeat)
        {
            bool bReturn = false;
            m_sSql = "select QdsCreatedByEdi from header863 where mill = '" + sMill + "' AND heat = '"+sHeat+"' and QdsCreatedByEdi > 0";
            iTECH.Library.DataAccess.MySql.DbHelper oDbHelper = new iTECH.Library.DataAccess.MySql.DbHelper(sMysqlConnectionString, true);
            try
            {
                MySqlParameter[] oMySqlParameter = { };
                DataTable dt = oDbHelper.GetDataTable(m_sSql, CommandType.Text, oMySqlParameter);
                bReturn = dt.Rows.Count > 0;
            }
            catch (Exception ex)
            {
                EdiError.LogError("863", Globalcl.FileId.ToString(), "", ex, "CheckHeatQdsExistForMillHeat error");
                ErrorLog.CreateLog(ex);
            }
            finally
            {
                oDbHelper.CloseDatabaseConnection();
            }
            return bReturn;
        }


        public void UpdateQdsReferenceNo(int iOldFileId, long lngOldQdsNumber)
        {
            m_sSql = "update  files SET FileIdReferenceNo = '" + iOldFileId + "', QdsReferenceNo = '" + lngOldQdsNumber + "' where FileId ='" + m_iFileId + "'";
            iTECH.Library.DataAccess.MySql.DbHelper oDbHelper = new iTECH.Library.DataAccess.MySql.DbHelper(sMysqlConnectionString, true);
            try
            {
                MySqlParameter[] oMySqlParameter = { };
                int iStatus = oDbHelper.ExecuteNonQuery(m_sSql, CommandType.Text, oMySqlParameter);
            }
            catch (Exception ex)
            {
                EdiError.LogError("863", Globalcl.FileId.ToString(), "", ex, "UpdateQdsReferenceNo");
                ErrorLog.CreateLog(ex);
            }
            finally
            {
                oDbHelper.CloseDatabaseConnection();
            }
        }



        // update chemistry in db
        public void UpdateChemistry(long lngQdsno, ref QdsCreationService.AuthenticationToken oQDSAuthenticationToken)
        {
            ErrorLog.createLog("Creating Chemistry is in progress..");
            EdiError.LogError(EdiFile, m_iFileId.ToString(), "Creating Chemistry start.");
            try
            {
                Detail863 oDetail863 = new Detail863();
                DataTable dtChemistry = oDetail863.GetChemistryList(m_iFileId, m_iHeaderId);
                EdiError.LogError(EdiFile, m_iFileId.ToString(), "chemistry count from mysql db = " + dtChemistry.Rows.Count, 0, 0, m_iHeaderId, 0, EdiError.ErrorLogType.INFO);
                ErrorLog.createLog("Create chem start : chemistry count : " + dtChemistry.Rows.Count);
                UpdateQdsChemistryInput[] oUpdateQdsChemistryInputArr = new UpdateQdsChemistryInput[dtChemistry.Rows.Count];
                 
                for (int i = 0; i < dtChemistry.Rows.Count; i++)
                {
                    UpdateQdsChemistryInput oUpdateQdsChemistryInput = new UpdateQdsChemistryInput();
                      
                    oUpdateQdsChemistryInput.chmElement = BusinessUtility.GetString(dtChemistry.Rows[i]["ChemistrylElement"]);
                    if (tctipd.CheckChemistryEmlementExistInStratix(oUpdateQdsChemistryInput.chmElement))
                    {
                        oUpdateQdsChemistryInput.chmEntryType = BusinessUtility.GetString(dtChemistry.Rows[i]["ChemistrylEntryType"]);
                        if (oUpdateQdsChemistryInput.chmEntryType.Equals("A"))
                        {
                            oUpdateQdsChemistryInput.chmAlpha = BusinessUtility.GetString(dtChemistry.Rows[i]["ChemistryElementVAlue"]);
                        }
                        else
                        {
                            oUpdateQdsChemistryInput.chmVal = decimal.Parse(BusinessUtility.GetString(dtChemistry.Rows[i]["ChemistryElementVAlue"]));
                        }
                        oUpdateQdsChemistryInput.chmValSpecified = true;
                        oUpdateQdsChemistryInputArr[i] = oUpdateQdsChemistryInput;
                    }
                    else
                    {
                        ErrorLog.createLog("Chemistry element no available in stratix : " + oUpdateQdsChemistryInput.chmElement);
                        EdiError.LogError(EdiFile, m_iFileId.ToString(), "Chemistry element no available in stratix " + oUpdateQdsChemistryInput.chmElement, 0, 0, m_iHeaderId, 0, EdiError.ErrorLogType.INFO);
                    }
                }

                ErrorLog.createLog("oUpdateQdsChemistryInputArr.Length = " + oUpdateQdsChemistryInputArr.Length);
                if (dtChemistry.Rows.Count > 0 && oUpdateQdsChemistryInputArr.Length > 0)
                {
                    QdsCreationService.ServiceMessages oServiceMessages = new QdsCreationService.ServiceMessages();
                    QdsCreationService.QdsServiceClient oQdsServiceClient = new QdsServiceClient();

                    oServiceMessages = oQdsServiceClient.UpdateQdsChemistry(ref oQDSAuthenticationToken, lngQdsno, oUpdateQdsChemistryInputArr);
                    ErrorLog.createLog("Chemistry Update message : " + oServiceMessages.returnStatus);
                    EdiError.LogError(EdiFile, m_iFileId.ToString(), "Chemistry Update message : " + oServiceMessages.returnStatus, 0, 0, m_iHeaderId, 0, EdiError.ErrorLogType.INFO);
                }
                else
                {
                    EdiError.LogError(EdiFile, m_iFileId.ToString(), "No chemistry found to update. Count = " + dtChemistry.Rows.Count, 0, 0, m_iHeaderId, 0, EdiError.ErrorLogType.WARNING,true);
                }

            }
            catch (Exception ex)
            {
                EdiError.LogError(EdiFile, m_iFileId.ToString(), "Error in Chemistry Update method", 0, 0, m_iHeaderId, 0, EdiError.ErrorLogType.ERROR);
                ErrorLog.createLog("Error in Chemistry Update method : ");
                ErrorLog.CreateLog(ex);
                EdiError.LogError(EdiFile, m_iFileId.ToString(), "", ex, "", 0, 0, m_iHeaderId, 0, EdiError.ErrorLogType.ERROR);
                EdiError.LogLatestSctmsgError(m_iFileId, 863, m_iHeaderId);
            }

        }


         // update test in db
        public void UpdateTests(long lngQdsno, ref QdsCreationService.AuthenticationToken oQDSAuthenticationToken)
        {
            ErrorLog.createLog("Creating Tests is in progress..");
            EdiError.LogError(EdiFile, m_iFileId.ToString(), "Creating Tests start", 0, 0, m_iHeaderId, 0);
            try
            {
                Detail863 oDetail863 = new Detail863();
                bool bRange = oDetail863.IsTestRange(m_iFileId, m_iHeaderId);
                ErrorLog.createLog(" bRange = " + bRange);
                DataTable dtTest;
                if (bRange)
                {
                    dtTest = oDetail863.GetTestListRange(m_iFileId, m_iHeaderId, oConfiguration.VendorId);
                }
                else
                {
                    dtTest = oDetail863.GetTestList(m_iFileId, m_iHeaderId, oConfiguration.VendorId);
                }
                string sTestEntityType = string.Empty;
                ErrorLog.createLog("Create Test start : Test count : " + dtTest.Rows.Count);
                EdiError.LogError(EdiFile, m_iFileId.ToString(), "Test count in mysql db = " + dtTest.Rows.Count, 0, 0, m_iHeaderId, 0, EdiError.ErrorLogType.INFO);
                if (dtTest.Rows.Count > 0)
                {
                    UpdateQdsTestInput[] oUpdateQdsTestInputArr = new UpdateQdsTestInput[dtTest.Rows.Count];

                    for (int i = 0; i < dtTest.Rows.Count; i++)
                    {
                        // UpdateQdsChemistryInput oUpdateQdsChemistryInput = new UpdateQdsChemistryInput();
                        UpdateQdsTestInput oUpdateQdsTestInput = new UpdateQdsTestInput();
                        sTestEntityType = BusinessUtility.GetString(dtTest.Rows[i]["TestEntityType"]);

                        oUpdateQdsTestInput.tstDirection = BusinessUtility.GetString(dtTest.Rows[i]["TestDirection"]);
                        oUpdateQdsTestInput.tstAbbr = BusinessUtility.GetString(dtTest.Rows[i]["TestAbbr"]);
                        oUpdateQdsTestInput.tstEntTyp = BusinessUtility.GetString(dtTest.Rows[i]["TestEntityType"]);

                        if (tctipd.CheckTestEmlementExistInStratix(oUpdateQdsTestInput.tstAbbr))
                        {

                            if (sTestEntityType.Equals("V"))
                            {
                                oUpdateQdsTestInput.tstVal = decimal.Parse(BusinessUtility.GetString(dtTest.Rows[i]["TestVal"]));
                                oUpdateQdsTestInput.tstUm = BusinessUtility.GetString(dtTest.Rows[i]["TestUM"]);
                                oUpdateQdsTestInput.tstValSpecified = true;
                            }
                            else if (sTestEntityType.Equals("A"))
                            {
                                oUpdateQdsTestInput.tstAlpha = BusinessUtility.GetString(dtTest.Rows[i]["TestAlpha"]);
                            }
                            else if (sTestEntityType.Equals("R"))
                            {
                                oUpdateQdsTestInput.tstMinVal = decimal.Parse(BusinessUtility.GetString(dtTest.Rows[i]["TestMinVal"]));
                                oUpdateQdsTestInput.tstMaxVal = decimal.Parse(BusinessUtility.GetString(dtTest.Rows[i]["TestMaxVal"]));
                                oUpdateQdsTestInput.tstUm = BusinessUtility.GetString(dtTest.Rows[i]["TestUM"]);
                                oUpdateQdsTestInput.tstMinValSpecified = true;
                                oUpdateQdsTestInput.tstMaxValSpecified = true;
                                
                                ErrorLog.createLog(" oUpdateQdsTestInput.tstMinVal =" + oUpdateQdsTestInput.tstMinVal);
                                ErrorLog.createLog(" oUpdateQdsTestInput.tstMaxVal =" + oUpdateQdsTestInput.tstMaxVal);
                            }


                            oUpdateQdsTestInputArr[i] = oUpdateQdsTestInput;
                        }
                        else
                        {
                            ErrorLog.createLog("Test element not available in stratix : " + oUpdateQdsTestInput.tstAbbr);
                            EdiError.LogError(EdiFile, m_iFileId.ToString(), "Test element no available in stratix " + oUpdateQdsTestInput.tstAbbr, 0, 0, m_iHeaderId, 0, EdiError.ErrorLogType.INFO);
                        }
                    }

                    if (oUpdateQdsTestInputArr.Length > 0)
                    {
                        QdsCreationService.ServiceMessages oServiceMessages = new QdsCreationService.ServiceMessages();
                        QdsCreationService.QdsServiceClient oQdsServiceClient = new QdsServiceClient();

                        oServiceMessages = oQdsServiceClient.UpdateQdsTest(ref oQDSAuthenticationToken, lngQdsno, oUpdateQdsTestInputArr);
                        ErrorLog.createLog("Test Update message : " + oServiceMessages.returnStatus);
                        EdiError.LogError(EdiFile, m_iFileId.ToString(), "Test Update message = " + oServiceMessages.returnStatus, 0, 0, m_iHeaderId, 0, EdiError.ErrorLogType.INFO);
                    }
                    else
                    {
                        ErrorLog.createLog("No test added in array oUpdateQdsTestInputArr len   : " + oUpdateQdsTestInputArr.Length);
                    }

                }
                else
                {
                    ErrorLog.createLog("No test found.");
                    EdiError.LogError(EdiFile, m_iFileId.ToString(), "No test found.", 0, 0, m_iHeaderId, 0, EdiError.ErrorLogType.ERROR, true);
                }


            }

            catch (Exception ex)
            {
                ErrorLog.createLog("Error in Test update method : ");
                EdiError.LogError(EdiFile, m_iFileId.ToString(), "Error in Test update method", 0, 0, m_iHeaderId, 0, EdiError.ErrorLogType.INFO,true);
                UserAuthenticationLogOut(oAuthenticationToken);
                ErrorLog.CreateLog(ex);
                EdiError.LogError(EdiFile, m_iFileId.ToString(), "", ex, "", 0, 0, m_iHeaderId, 0, EdiError.ErrorLogType.INFO);
                EdiError.LogLatestSctmsgError(m_iFileId, 863, m_iHeaderId);
            }

        }


        // Login to stratix client
        GatewayAuthService.AuthenticationToken oAuthenticationToken;
        public GatewayAuthService.AuthenticationToken UserAuthentication()
        {
            GatewayAuthService.GatewayLoginResponse oGatewayLoginResponse = new GatewayAuthService.GatewayLoginResponse();
            try
            {
                oConfiguration = new Configuration(oFiles.VendorDunsNo, BusinessUtility.GetInt(oFiles.EdiFileType));
                oAuthenticationToken = new GatewayAuthService.AuthenticationToken();

                string sTokenValue = string.Empty;
                //ErrorLog.createLog("Edi Auth start");

                //ErrorLog.createLog("oConfiguration.CompanyId = " + oConfiguration.CompanyId);
                //ErrorLog.createLog("oConfiguration.ConnectedAccessType = " + oConfiguration.ConnectedAccessType);
                //ErrorLog.createLog("oConfiguration.EnvironmentClass = " + oConfiguration.EnvironmentClass);
                //ErrorLog.createLog("oConfiguration.EnvironmentName = " + oConfiguration.EnvironmentName);
                //ErrorLog.createLog("oConfiguration.Username = " + oConfiguration.Username);
                //ErrorLog.createLog("oConfiguration.Password = " + oConfiguration.Password);


                GatewayAuthService.GatewayLoginRequestType oGatewayLoginRequestType = new GatewayAuthService.GatewayLoginRequestType();
                oGatewayLoginRequestType.companyId = oConfiguration.CompanyId;
                oGatewayLoginRequestType.connectedAccessType = oConfiguration.ConnectedAccessType;
                oGatewayLoginRequestType.environmentClass = oConfiguration.EnvironmentClass;
                oGatewayLoginRequestType.environmentName = oConfiguration.EnvironmentName;
                oGatewayLoginRequestType.forceDisconnect = true;
                oGatewayLoginRequestType.forceDisconnectSpecified = true;

                oGatewayLoginRequestType.username = oConfiguration.Username;
                oGatewayLoginRequestType.password = oConfiguration.Password;
                //ErrorLog.createLog("Edi Auth end");
                GatewayAuthService.GatewayLoginRequest oGatewayLoginRequest = new GatewayAuthService.GatewayLoginRequest(oGatewayLoginRequestType);
                GatewayAuthService.AuthenticationService oAuthenticationService = new GatewayAuthService.AuthenticationServiceClient();
                oGatewayLoginResponse = oAuthenticationService.GatewayLogin(oGatewayLoginRequest);

                String sTokenUser = oGatewayLoginResponse.response.authenticationToken.username;
                sTokenValue = oGatewayLoginResponse.response.authenticationToken.value;

                oAuthenticationToken = oGatewayLoginResponse.response.authenticationToken;
                //Write log for success
        
                ErrorLog.createLog("User authentication success " + "\n sTokenUser: " + sTokenUser + "\n sTokenValue: " + sTokenValue + " ");
            }
            catch (Exception MyError)
            {
                ErrorLog.createLog("User authentication error: " + MyError.ToString());
                ErrorLog.createLog("User authentication error: " + oGatewayLoginResponse.ServiceMessagesHeader.messageList[0].ToString() + "\n" + MyError.Message);

                //throw;
                EdiError.LogError("863", m_iFileId.ToString(), ConfigurationManager.AppSettings["AuthenticationFail"], 0, 0, m_iHeaderId, 0, EdiError.ErrorLogType.ERROR,true);
                EdiError.LogError(EdiFile, m_iFileId.ToString(), "", MyError, oGatewayLoginResponse.ServiceMessagesHeader.messageList[0].ToString() + "\n" + MyError.Message, 0, 0, m_iHeaderId, 0, EdiError.ErrorLogType.ERROR);
                ErrorLog.CreateLog(MyError);
                EdiError.LogLatestSctmsgError(m_iFileId, 863, m_iHeaderId);
            }
            return oAuthenticationToken;
        }

        /// <summary>
        /// Parameterized UserAuthentication Token Generation   2020/04/15  Sumit
        /// </summary>
        /// <param name="VendorDunsNo"></param>
        /// <param name="fileType"></param>
        /// <returns></returns>
        public GatewayAuthService.AuthenticationToken UserAuthentication(string VendorDunsNo, int fileType)
        {
            GatewayAuthService.GatewayLoginResponse oGatewayLoginResponse = new GatewayAuthService.GatewayLoginResponse();
            try
            {
                oConfiguration = new Configuration(VendorDunsNo, fileType);
                oAuthenticationToken = new GatewayAuthService.AuthenticationToken();

                string sTokenValue = string.Empty;

                GatewayAuthService.GatewayLoginRequestType oGatewayLoginRequestType = new GatewayAuthService.GatewayLoginRequestType();
                oGatewayLoginRequestType.companyId = oConfiguration.CompanyId;
                oGatewayLoginRequestType.connectedAccessType = oConfiguration.ConnectedAccessType;
                oGatewayLoginRequestType.environmentClass = oConfiguration.EnvironmentClass;
                oGatewayLoginRequestType.environmentName = oConfiguration.EnvironmentName;
                oGatewayLoginRequestType.forceDisconnect = true;
                oGatewayLoginRequestType.forceDisconnectSpecified = true;

                oGatewayLoginRequestType.username = oConfiguration.Username;
                oGatewayLoginRequestType.password = oConfiguration.Password;
                //ErrorLog.createLog("Edi Auth end");
                GatewayAuthService.GatewayLoginRequest oGatewayLoginRequest = new GatewayAuthService.GatewayLoginRequest(oGatewayLoginRequestType);
                GatewayAuthService.AuthenticationService oAuthenticationService = new GatewayAuthService.AuthenticationServiceClient();
                oGatewayLoginResponse = oAuthenticationService.GatewayLogin(oGatewayLoginRequest);

                String sTokenUser = oGatewayLoginResponse.response.authenticationToken.username;
                sTokenValue = oGatewayLoginResponse.response.authenticationToken.value;

                oAuthenticationToken = oGatewayLoginResponse.response.authenticationToken;
                //Write log for success

                ErrorLog.createLog("User authentication success " + "\n sTokenUser: " + sTokenUser + "\n sTokenValue: " + sTokenValue + " ");
            }
            catch (Exception MyError)
            {
                ErrorLog.createLog("User authentication error: " + MyError.ToString());
                ErrorLog.createLog("User authentication error: " + oGatewayLoginResponse.ServiceMessagesHeader.messageList[0].ToString() + "\n" + MyError.Message);

                //throw;
                EdiError.LogError("863", m_iFileId.ToString(), ConfigurationManager.AppSettings["AuthenticationFail"], 0, 0, m_iHeaderId, 0, EdiError.ErrorLogType.ERROR, true);
                EdiError.LogError(EdiFile, m_iFileId.ToString(), "", MyError, oGatewayLoginResponse.ServiceMessagesHeader.messageList[0].ToString() + "\n" + MyError.Message, 0, 0, m_iHeaderId, 0, EdiError.ErrorLogType.ERROR);
                ErrorLog.CreateLog(MyError);
                EdiError.LogLatestSctmsgError(m_iFileId, 863, m_iHeaderId);
            }
            return oAuthenticationToken;
        }

        public GatewayAuthService.AuthenticationToken UserAuthenticationTest( string sCompId, string sConnectedAccType, string sEnvClass,string sEnvNAme, string sUserNAme, string sPass)
        {
            GatewayAuthService.GatewayLoginResponse oGatewayLoginResponse = new GatewayAuthService.GatewayLoginResponse();
            try
            {
               // oConfiguration = new Configuration(oFiles.VendorDunsNo, BusinessUtility.GetInt(oFiles.EdiFileType));
                oAuthenticationToken = new GatewayAuthService.AuthenticationToken();

                string sTokenValue = string.Empty;

                GatewayAuthService.GatewayLoginRequestType oGatewayLoginRequestType = new GatewayAuthService.GatewayLoginRequestType();
                oGatewayLoginRequestType.companyId = sCompId;
                oGatewayLoginRequestType.connectedAccessType = sConnectedAccType;
                oGatewayLoginRequestType.environmentClass = sEnvClass;
                oGatewayLoginRequestType.environmentName = sEnvNAme;
                oGatewayLoginRequestType.forceDisconnect = true;
                oGatewayLoginRequestType.forceDisconnectSpecified = true;

                oGatewayLoginRequestType.username = sUserNAme;
                oGatewayLoginRequestType.password = sPass;

                GatewayAuthService.GatewayLoginRequest oGatewayLoginRequest = new GatewayAuthService.GatewayLoginRequest(oGatewayLoginRequestType);
                GatewayAuthService.AuthenticationService oAuthenticationService = new GatewayAuthService.AuthenticationServiceClient();
                oGatewayLoginResponse = oAuthenticationService.GatewayLogin(oGatewayLoginRequest);

                String sTokenUser = oGatewayLoginResponse.response.authenticationToken.username;
                sTokenValue = oGatewayLoginResponse.response.authenticationToken.value;

                oAuthenticationToken = oGatewayLoginResponse.response.authenticationToken;
                //Write log for success
                ErrorLog.createLog("User Login to use QDS service ");
                Console.WriteLine("Login to service is successful");
                ErrorLog.createLog("User authentication success " + "\n sTokenUser: " + sTokenUser + "\n sTokenValue: " + sTokenValue + " ");
            }
            catch (Exception MyError)
            {
                ErrorLog.createLog("User authentication error: " + oGatewayLoginResponse.ServiceMessagesHeader.messageList[0].ToString() + "\n" + MyError.Message);

                //throw;
                EdiError.LogError("863", m_iFileId.ToString(), ConfigurationManager.AppSettings["AuthenticationFail"], 0, 0, m_iHeaderId, 0, EdiError.ErrorLogType.ERROR,true);

                EdiError.LogError(EdiFile, m_iFileId.ToString(), "", MyError, oGatewayLoginResponse.ServiceMessagesHeader.messageList[0].ToString() + "\n" + MyError.Message, 0, 0, m_iHeaderId, 0, EdiError.ErrorLogType.ERROR);
                ErrorLog.CreateLog(MyError);
                EdiError.LogLatestSctmsgError(m_iFileId, 863, m_iHeaderId);
            }
            return oAuthenticationToken;

        }


        public void UserAuthenticationLogOut(GatewayAuthService.AuthenticationToken oAuthenticationToken)
        {
            try
            {
                GatewayAuthService.AuthenticationService oAuthenticationService = new GatewayAuthService.AuthenticationServiceClient();
                GatewayAuthService.LogoutRequest oLogoutRequest = new GatewayAuthService.LogoutRequest();
                oLogoutRequest.authenticationToken = oAuthenticationToken;
                GatewayAuthService.LogoutResponse oLogoutResponse = new GatewayAuthService.LogoutResponse();
                oLogoutResponse = oAuthenticationService.Logout(oLogoutRequest);
                //Log write for success logout
                ErrorLog.createLog("User LogOut from QDS service ");
                ErrorLog.createLog("User authentication success LogOut ");
            }
            catch (Exception MyError)
            {
                ErrorLog.createLog("User authentication LogOut error: " + MyError.Message);
                ErrorLog.CreateLog(MyError);
                EdiError.LogLatestSctmsgError(m_iFileId, 863, m_iHeaderId);
            }
        }

        /// <summary>
        /// To Update Qds Certificate through UpdateExternal Qds process
        /// </summary>
        /// <param name="lQDS"></param>
        /// <param name="filename"></param>
        /// <param name="VendorDuns"></param>
        /// <returns></returns>
        public bool UpdateOnlyQdsCertificate(long lQDS, string filename, string VendorDuns)
        {
            bool bSuccess = false;
            StringBuilder sberr = new StringBuilder();
            GatewayAuthService.AuthenticationToken oAuthToken = new GatewayAuthService.AuthenticationToken();
            try
            {
                oAuthToken = UserAuthentication(VendorDuns, 863);
                if (oAuthToken != null)
                {
                    if (oAuthToken.value.Length > 10)
                    {
                        //QdsCreationService.ServiceMessages oServiceMessages = new QdsCreationService.ServiceMessages();
                        //QdsCreationService.QdsServiceClient oQdsServiceClient = new QdsServiceClient();
                        
                        QdsCreationService.AuthenticationToken oQDSAuthenticationToken = new QdsCreationService.AuthenticationToken();
                        oQDSAuthenticationToken.username = oAuthToken.username;
                        oQDSAuthenticationToken.value = oAuthToken.value;

                        
                        this.UpdateQdsExternalCertificate(ref oQDSAuthenticationToken, lQDS, filename, ref bSuccess, ref sberr);
                        ErrorLog.createLog("Certificate update = " + sberr.ToString());
                    }
                }
            }
            catch(Exception ex)
            {
                ErrorLog.createLog("Error in UpdateOnlyQdsCertificate");
                ErrorLog.CreateLog(ex);
            }
            finally
            {
                UserAuthenticationLogOut(oAuthToken);
            }
            return bSuccess;
        }

        public byte[] FileToByteArray(string fileName)
        {
            byte[] fileContent = null;
            
            System.IO.FileStream fs = new System.IO.FileStream(fileName, System.IO.FileMode.Open, System.IO.FileAccess.Read);
            System.IO.BinaryReader binaryReader = new System.IO.BinaryReader(fs);

            long byteLength = new System.IO.FileInfo(fileName).Length;
            fileContent = binaryReader.ReadBytes((Int32)byteLength);
            
            fs.Close();
            fs.Dispose();
            binaryReader.Close();
            return fileContent;
        }

        public CreateQdsResponse CreateQds(CreateQdsRequest request)
        {
            EdiError.LogError(EdiFile, m_iFileId.ToString(), "CreateQdsResponse CreateQds executed");
            throw new NotImplementedException();
        }

        public UpdateQdsHeaderResponse UpdateQdsHeader(UpdateQdsHeaderRequest request)
        {
            throw new NotImplementedException();
        }

        public DeleteQdsResponse DeleteQds(DeleteQdsRequest request)
        {
            throw new NotImplementedException();
        }

        public UpdateQdsResponse UpdateQds(UpdateQdsRequest request)
        {
            throw new NotImplementedException();
        }

        public UpdateVendorTagIdResponse UpdateVendorTagId(UpdateVendorTagIdRequest request)
        {
            throw new NotImplementedException();
        }

        public DeleteVendorTagIDResponse DeleteVendorTagID(DeleteVendorTagIDRequest request)
        {
            throw new NotImplementedException();
        }

        public UpdateQdsChemistryResponse UpdateQdsChemistry(UpdateQdsChemistryRequest request)
        {
            throw new NotImplementedException();
        }

        public DeleteQdsChemistryResponse DeleteQdsChemistry(DeleteQdsChemistryRequest request)
        {
            throw new NotImplementedException();
        }

        public UpdateQdsTestResponse UpdateQdsTest(UpdateQdsTestRequest request)
        {
            throw new NotImplementedException();
        }

        public DeleteQdsTestResponse DeleteQdsTest(DeleteQdsTestRequest request)
        {
            throw new NotImplementedException();
        }

        public UpdateQdsMetalStandardResponse UpdateQdsMetalStandard(UpdateQdsMetalStandardRequest request)
        {
            throw new NotImplementedException();
        }

        public DeleteQdsMetalStandardResponse DeleteQdsMetalStandard(DeleteQdsMetalStandardRequest request)
        {
            throw new NotImplementedException();
        }

        public UpdateQdsExternalCertificatesResponse UpdateQdsExternalCertificates(UpdateQdsExternalCertificatesRequest request)
        {
            throw new NotImplementedException();
        }

        public DeleteQdsExternalCertificatesResponse DeleteQdsExternalCertificates(DeleteQdsExternalCertificatesRequest request)
        {
            throw new NotImplementedException();
        }

        public UpdateQdsInstructionsResponse UpdateQdsInstructions(UpdateQdsInstructionsRequest request)
        {
            throw new NotImplementedException();
        }

        public DeleteQdsInstructionsResponse DeleteQdsInstructions(DeleteQdsInstructionsRequest request)
        {
            throw new NotImplementedException();
        }

        public UpdateQdsJominyResponse UpdateQdsJominy(UpdateQdsJominyRequest request)
        {
            throw new NotImplementedException();
        }

        public DeleteQdsJominyResponse DeleteQdsJominy(DeleteQdsJominyRequest request)
        {
            throw new NotImplementedException();
        }

        public UpdateQdsMicroinclusionResponse UpdateQdsMicroinclusion(UpdateQdsMicroinclusionRequest request)
        {
            throw new NotImplementedException();
        }

        public DeleteQdsMicroinclusionResponse DeleteQdsMicroinclusion(DeleteQdsMicroinclusionRequest request)
        {
            throw new NotImplementedException();
        }

        public UpdateQdsImpactTestResponse UpdateQdsImpactTest(UpdateQdsImpactTestRequest request)
        {
            throw new NotImplementedException();
        }

        public DeleteQdsImpactTestResponse DeleteQdsImpactTest(DeleteQdsImpactTestRequest request)
        {
            throw new NotImplementedException();
        }

        public UpdateQdsHeatTreatmentResponse UpdateQdsHeatTreatment(UpdateQdsHeatTreatmentRequest request)
        {
            throw new NotImplementedException();
        }

        public DeleteQdsHeatTreatmentResponse DeleteQdsHeatTreatment(DeleteQdsHeatTreatmentRequest request)
        {
            throw new NotImplementedException();
        }
    }

    


}
