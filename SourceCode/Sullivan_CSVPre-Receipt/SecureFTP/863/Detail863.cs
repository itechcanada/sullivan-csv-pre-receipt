﻿using iTECH.Library.Utilities;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Text;

namespace SecureFTP
{
    class Detail863
    {

        StringBuilder m_sbSql = null;
        string m_sSql = string.Empty;
        static string sMysqlConnectionString = ConfigurationManager.ConnectionStrings["NewConnectionString"].ToString();

        int iClassID = 0;
        public int ClassID
        {
            get { return iClassID; }
            set { iClassID = value; }
        }

        string sClassDescription = string.Empty;
        public string ClassDescription
        {
            get { return sClassDescription; }
            set { sClassDescription = value; }
        }

        string sChemistrylElement = string.Empty;
        public string ChemistrylElement
        {
            get { return sChemistrylElement; }
            set { sChemistrylElement = value; }
        }

        string sChemistryElementVAlue = string.Empty;
        public string ChemistryElementVAlue
        {
            get { return sChemistryElementVAlue; }
            set { sChemistryElementVAlue = value; }
        }

        string sChmEntryType = string.Empty;
        public string ChmEntryType
        {
            get { return sChmEntryType; }
            set { sChmEntryType = value; }
        }
         

        int iFileId = 0;
        public int FileId
        {
            get { return iFileId; }
            set { iFileId = value; }
        }

        int iHeaderId = 0;
        public int HeaderId
        {
            get { return iHeaderId; }
            set { iHeaderId = value; }
        }

        string sTestAbbr = string.Empty;
        public string TestAbbr
        {
            get { return sTestAbbr; }
            set { sTestAbbr = value; }
        }

        string sTestDirection = string.Empty;
        public string TestDirection
        {
            get { return sTestDirection; }
            set { sTestDirection = value; }
        }

        string sTestEntityType = string.Empty;
        public string TestEntityType
        {
            get { return sTestEntityType; }
            set { sTestEntityType = value; }
        }

        string sTestVal = string.Empty;
        public string TestVal
        {
            get { return sTestVal; }
            set { sTestVal = value; }
        }

        string sTestUM = string.Empty;
        public string TestUM
        {
            get { return sTestUM; }
            set { sTestUM = value; }
        }

        string sTestMinVal = string.Empty;
        public string TestMinVal
        {
            get { return sTestMinVal; }
            set { sTestMinVal = value; }
        }

        string sTestMaxVal = string.Empty;
        public string TestMaxVal
        {
            get { return sTestMaxVal; }
            set { sTestMaxVal = value; }
        }

        string sTestProdDesc = string.Empty;
        public string TestProdDesc
        {
            get { return sTestProdDesc; }
            set { sTestProdDesc = value; }
        }

        string sTestAlpha = string.Empty;
        public string TestAlpha
        {
            get { return sTestAlpha; }
            set { sTestAlpha = value; }
        }

        string sStratixText = string.Empty;
        public string StratixText
        {
            get { return sStratixText; }
            set { sStratixText = value; }
        }

        string sEdiText = string.Empty;
        public string EdiText
        {
            get { return sEdiText; }
            set { sEdiText = value; }
        }

        int iChemDefault = 0;
        public int ChemDefault
        {
            get { return iChemDefault; }
            set { iChemDefault = value; }
        }

        int iChemistrySequence = 0;
          public int ChemistrySequence
        {
            get { return iChemistrySequence; }
            set { iChemistrySequence = value; }
        }


        public struct Characteristic863
        {
            public const int CHEMISTRY = 68;
            public const int MECHANICAL = 71;
            public const int METALSTANDARD = 100;
        }

        public int InsertHeaderDetailChemistry(int iHeaderId, int iFileId)
        {
            int iStatus = 0;
            m_sbSql = new StringBuilder();
            iTECH.Library.DataAccess.MySql.DbHelper oDbHelper = new iTECH.Library.DataAccess.MySql.DbHelper(sMysqlConnectionString, true);
            try
            {
                m_sbSql.Append(" INSERT INTO detail863(HeaderId, ClassID, ClassDescription, ChemistrylElement, ChemistryElementVAlue, FileId, ChemistrylEntryType, ChemistrySequence)");
                m_sbSql.Append(" VALUES ('" + iHeaderId + "','" + iClassID + "', '" + sClassDescription + "', '" + sChemistrylElement + "', '" + sChemistryElementVAlue + "', '" + iFileId + "', '" + sChmEntryType + "', '" + iChemistrySequence + "' ) ");
                MySqlParameter[] oMySqlParameter = { };

                iStatus = oDbHelper.ExecuteNonQuery(m_sbSql.ToString(), CommandType.Text, oMySqlParameter);
                iStatus = oDbHelper.GetLastInsertID();

               // ErrorLog.createLog("sChemistrylElement = " + sChemistrylElement + " For File id = " + iFileId);
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLog(ex);
            }
            finally
            {
                oDbHelper.CloseDatabaseConnection();
            }
            return iStatus;
        }


        public void UpdateHeaderDetailChemistry(int iHeaderId, int iFileId, string sElem)
        {
            int iStatus = 0;
            m_sbSql = new StringBuilder();
            iTECH.Library.DataAccess.MySql.DbHelper oDbHelper = new iTECH.Library.DataAccess.MySql.DbHelper(sMysqlConnectionString, true);
            try
            {
                m_sbSql.Append(" Update detail863 SET Active = 0 where HeaderId = " + iHeaderId + " AND FileId = " + iFileId + " AND ChemistrylElement = '"+sElem+"'");
                MySqlParameter[] oMySqlParameter = { };

                iStatus = oDbHelper.ExecuteNonQuery(m_sbSql.ToString(), CommandType.Text, oMySqlParameter);

            }
            catch (Exception ex)
            {
                ErrorLog.CreateLog(ex);
            }
            finally
            {
                oDbHelper.CloseDatabaseConnection();
            }

        }




        public int InsertHeaderDetailMechanical(int iHeaderId, int iFileId)
        {
            int iStatus = 0;
            m_sbSql = new StringBuilder();
            iTECH.Library.DataAccess.MySql.DbHelper oDbHelper = new iTECH.Library.DataAccess.MySql.DbHelper(sMysqlConnectionString, true);
            try
            {

                //sTestAbbr = this.GetTestAbbrivation(sTestProdDesc, oDbHelper);

                m_sbSql.Append(" INSERT INTO detail863 ( HeaderId, ClassID, FileId, TestAbbr, TestDirection, TestEntityType, TestVal, TestUM, TestMinVal, TestMaxVal, TestAlpha )");
                m_sbSql.Append(" VALUES ('" + iHeaderId + "','" + iClassID + "', '" + iFileId + "', '" + sTestAbbr + "', '" + sTestDirection + "', '" + sTestEntityType + "' , '" + sTestVal + "', '" + sTestUM + "', '" + sTestMinVal + "' , '" + sTestMaxVal + "', '" + sTestAlpha + "') ");
                MySqlParameter[] oMySqlParameter = { };

                iStatus = oDbHelper.ExecuteNonQuery(m_sbSql.ToString(), CommandType.Text, oMySqlParameter);
                iStatus = oDbHelper.GetLastInsertID();
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLog(ex);
                EdiError.LogError("863", iFileId.ToString(), "", ex, "InsertHeaderDetailMechanical");
            }
            finally
            {
                oDbHelper.CloseDatabaseConnection();
            }
            return iStatus;
        }

        public int InsertHeaderDetailMetalStanderd(int iHeaderId, int iFileId, int iPo)
        {
            tctipd otctipd = new tctipd();
            otctipd.PopulateMaterialStanderd(iPo);
            string Sdo = otctipd.Sdo;
            string StdId = otctipd.StdId;
            string AddnlId = otctipd.AddnlId;

            int iStatus = 0;
            m_sbSql = new StringBuilder();
            iTECH.Library.DataAccess.MySql.DbHelper oDbHelper = new iTECH.Library.DataAccess.MySql.DbHelper(sMysqlConnectionString, true);
            try
            {
                m_sbSql.Append(" INSERT INTO detail863 ( HeaderId, ClassID,ClassDescription, FileId, mss_sdo, mss_std_id, mss_addnl_id )");
                m_sbSql.Append(" VALUES ('" + iHeaderId + "','" + Characteristic863.METALSTANDARD + "', 'METAL STANDARD', '" + iFileId + "', '" + Sdo + "', '" + StdId + "', '" + AddnlId + "' ) ");
                MySqlParameter[] oMySqlParameter = { };

                iStatus = oDbHelper.ExecuteNonQuery(m_sbSql.ToString(), CommandType.Text, oMySqlParameter);
                iStatus = oDbHelper.GetLastInsertID();
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLog(ex);
                EdiError.LogError("863", iFileId.ToString(), "", ex, "InsertHeaderDetailMetalStanderd");
            }
            finally
            {
                oDbHelper.CloseDatabaseConnection();
            }
            return iStatus;
        }


        public DataTable GetTestAbbrivation(string sTestId,string iVendorId)
        {
            string sReturn = string.Empty;
            iTECH.Library.DataAccess.MySql.DbHelper oDbHelper = new iTECH.Library.DataAccess.MySql.DbHelper(sMysqlConnectionString, true);
            m_sSql = "select * from tests where TestId = '" + sTestId + "' AND VendorId = '" + iVendorId + "' AND Active = 1";
            //ErrorLog.createLog(m_sSql);
            MySqlParameter[] oMySqlParameter = { iTECH.Library.DataAccess.MySql.DbUtility.GetParameter("TestId", sTestId, iTECH.Library.DataAccess.MySql.MyDbType.String), };
            DataTable dtTestDetail = oDbHelper.GetDataTable(m_sSql, CommandType.Text, oMySqlParameter);
            return dtTestDetail;
        }

        public string GetStratixChemicalElement(string sEdiChemElement, ref int iSequence, ref int iChemDefaultVal)
        {
            string sReturn = string.Empty;
            iTECH.Library.DataAccess.MySql.DbHelper oDbHelper = new iTECH.Library.DataAccess.MySql.DbHelper(sMysqlConnectionString, true);
            try
            {
                m_sSql = "select StratixText,ChemistrySequence, UseDefault from chem_mapping where EdiText = '" + sEdiChemElement + "' AND Active = 1";
                MySqlParameter[] oMySqlParameter = { iTECH.Library.DataAccess.MySql.DbUtility.GetParameter("EdiText", sEdiChemElement, iTECH.Library.DataAccess.MySql.MyDbType.String), };

                DataTable oDtChemical = oDbHelper.GetDataTable(m_sSql, CommandType.Text, oMySqlParameter);
                foreach (DataRow oDataRow in oDtChemical.Rows)
                {
                    sReturn = BusinessUtility.GetString(oDataRow["StratixText"]);
                    iSequence = BusinessUtility.GetInt(oDataRow["ChemistrySequence"]);
                    iChemDefaultVal = BusinessUtility.GetInt(oDataRow["UseDefault"]);
                }
                #region comment code to not run same query for same result again 2020/02/27 Sumit
                //object objIntChgNo = oDbHelper.GetValue(m_sSql, CommandType.Text, oMySqlParameter);
                //sReturn = BusinessUtility.GetString(objIntChgNo);
                #endregion comment code to not run same query for same result again 2020/02/27 Sumit

            }
            catch (Exception ex)
            {
                ErrorLog.CreateLog(ex);
                EdiError.LogError("863", iFileId.ToString(), "", ex, "GetStratixChemicalElement");
            }
            finally
            {
                oDbHelper.CloseDatabaseConnection();
            }
            return sReturn;
        }


        public void GetStratixChemicalElementBasedOnStratixElem(string sstratixChemElement)
        {
            string sReturn = string.Empty;
            iTECH.Library.DataAccess.MySql.DbHelper oDbHelper = new iTECH.Library.DataAccess.MySql.DbHelper(sMysqlConnectionString, true);
            try
            {
                m_sSql = "select EdiText,StratixText,ChemistrySequence, UseDefault from chem_mapping where StratixText = '" + sstratixChemElement + "' AND Active = 1";
                MySqlParameter[] oMySqlParameter = { iTECH.Library.DataAccess.MySql.DbUtility.GetParameter("StratixText", sstratixChemElement, iTECH.Library.DataAccess.MySql.MyDbType.String), };

                DataTable oDtChemical = oDbHelper.GetDataTable(m_sSql, CommandType.Text, oMySqlParameter);
                foreach (DataRow oDataRow in oDtChemical.Rows)
                {
                    this.EdiText = BusinessUtility.GetString(oDataRow["EdiText"]);
                    this.StratixText = BusinessUtility.GetString(oDataRow["StratixText"]);
                    this.ChemistrySequence = BusinessUtility.GetInt(oDataRow["ChemistrySequence"]);
                    this.ChemDefault = BusinessUtility.GetInt(oDataRow["UseDefault"]);
                }
                object objIntChgNo = oDbHelper.GetValue(m_sSql, CommandType.Text, oMySqlParameter);
                sReturn = BusinessUtility.GetString(objIntChgNo);


            }
            catch (Exception ex)
            {
                ErrorLog.CreateLog(ex);
                EdiError.LogError("863", iFileId.ToString(), "", ex, "GetStratixChemicalElement");
            }
            finally
            {
                oDbHelper.CloseDatabaseConnection();
            }
        }






        public DataTable GetChemistryList(int iFileId, int iHeaderId)
        {
            m_sbSql = new StringBuilder();
            DataTable dtChemistryList = new DataTable();
            iTECH.Library.DataAccess.MySql.DbHelper oDbHelper = new iTECH.Library.DataAccess.MySql.DbHelper(sMysqlConnectionString, true);
            try
            {
                m_sbSql.Append(" SELECT DetailId, HeaderId, ClassID, ClassDescription, ChemistrylElement, ChemistryElementVAlue,ChemistrylEntryType, CreatedOn, Active, FileId, TestAbbr, TestDirection, TestEntityType, TestVal, TestUM, TestMinVal, TestMaxVal FROM detail863 WHERE Active = 1 AND FileId = '" + iFileId + "' AND HeaderId = '" + iHeaderId + "' AND ClassID = '" + Characteristic863.CHEMISTRY + "' order by ChemistrySequence ");
                MySqlParameter[] oMySqlParameter = { };
                //ErrorLog.createLog(m_sbSql.ToString());
                dtChemistryList = oDbHelper.GetDataTable(m_sbSql.ToString(), CommandType.Text, oMySqlParameter);
            }
            catch (Exception ex)
            {
                EdiError.LogError("863", iFileId.ToString(), "", ex, "GetChemistryList");
            }
            finally
            {
                oDbHelper.CloseDatabaseConnection();
            }
            return dtChemistryList;
        }

        public DataTable GetTestList(int iFileId, int iHeaderId, string iVendorId)
        {
            m_sbSql = new StringBuilder();
            iTECH.Library.DataAccess.MySql.DbHelper oDbHelper = new iTECH.Library.DataAccess.MySql.DbHelper(sMysqlConnectionString, true);

            m_sbSql.Append(" SELECT distinct a.DetailId, a.HeaderId, a.ClassID, a.ClassDescription, a.ChemistrylElement, a.ChemistryElementVAlue,a.ChemistrylEntryType, a.CreatedOn, a.Active, a.FileId, a.TestAbbr, a.TestDirection, a.TestEntityType, a.TestVal, a.TestUM, a.TestMinVal, a.TestMaxVal,a.TestAlpha FROM detail863 a inner join tests  b on a.TestAbbr = b.TestCode"
                + " WHERE FileId = '" + iFileId + "' AND HeaderId = '" + iHeaderId + "' AND ClassID = '" + Characteristic863.MECHANICAL + "' AND b.vendorid = '" + iVendorId + "'  order by TestSequence asc");
            MySqlParameter[] oMySqlParameter = { };
            ErrorLog.createLog(m_sbSql.ToString());
            DataTable dtTestList = oDbHelper.GetDataTable(m_sbSql.ToString(), CommandType.Text, oMySqlParameter);
            return dtTestList;
        }

        public DataTable GetTestListRange(int iFileId, int iHeaderId, string iVendorId)
        {
            m_sbSql = new StringBuilder();
            iTECH.Library.DataAccess.MySql.DbHelper oDbHelper = new iTECH.Library.DataAccess.MySql.DbHelper(sMysqlConnectionString, true);

            m_sbSql.Append(" SELECT distinct TestAbbr,  min(testVal) as 'TestMinVal', max(testVal) as 'TestMaxVal', 'R' as 'TestEntityType', TestUM,TestDirection, TestSequence FROM detail863 a inner join tests  b on a.TestAbbr = b.TestCode "
                + " WHERE FileId = '" + iFileId + "' AND HeaderId = '" + iHeaderId + "' AND ClassID = '" + Characteristic863.MECHANICAL + "'  GROUP BY  TestAbbr order by TestSequence asc");
            MySqlParameter[] oMySqlParameter = { };
            ErrorLog.createLog(m_sbSql.ToString());
            DataTable dtTestList = oDbHelper.GetDataTable(m_sbSql.ToString(), CommandType.Text, oMySqlParameter);
            return dtTestList;
        }

        public bool IsTestRange(int iFileId, int iHeaderId)
        {
            m_sbSql = new StringBuilder();
            iTECH.Library.DataAccess.MySql.DbHelper oDbHelper = new iTECH.Library.DataAccess.MySql.DbHelper(sMysqlConnectionString, true);

            m_sbSql.Append("  select  count(TestAbbr) as num from detail863   WHERE FileId = '" + iFileId + "' AND HeaderId = '" + iHeaderId + "'  AND ClassID = '" + Characteristic863.MECHANICAL + "'    GROUP BY  TestAbbr limit 1");
            MySqlParameter[] oMySqlParameter = { };
            ErrorLog.createLog(m_sbSql.ToString());
            object num = oDbHelper.GetValue(m_sbSql.ToString(), CommandType.Text, oMySqlParameter);
            bool bRet = BusinessUtility.GetInt(num) > 1;
           // DataTable dtTestList = oDbHelper.GetDataTable(m_sbSql.ToString(), CommandType.Text, oMySqlParameter);
            return bRet;
        }

        public DataTable GetMetalStdList(int iFileId, int iHeaderId, string iVendorId)
        {
            m_sbSql = new StringBuilder();
            iTECH.Library.DataAccess.MySql.DbHelper oDbHelper = new iTECH.Library.DataAccess.MySql.DbHelper(sMysqlConnectionString, true);

            m_sbSql.Append(" SELECT a.DetailId, a.HeaderId, a.ClassID, a.ClassDescription,a.FileId, a.mss_sdo, a.mss_std_id, a.mss_addnl_id FROM detail863 WHERE Active = 1 AND FileId = '" + iFileId + "' AND HeaderId = '" + iHeaderId + "' AND ClassID = '" + Characteristic863.METALSTANDARD + "'  ");

            MySqlParameter[] oMySqlParameter = { };
            DataTable dtTestList = oDbHelper.GetDataTable(m_sbSql.ToString(), CommandType.Text, oMySqlParameter);
            return dtTestList;
        }

        public DataTable GetTestDetailWithOrWithoutQds(string sMill, string sHeat, string sShipingRef, int iFileId = 0, int iHeaderId =0)
        {
            m_sbSql = new StringBuilder();
            DataTable dtTestDetail = new DataTable();
            iTECH.Library.DataAccess.MySql.DbHelper oDbHelper = new iTECH.Library.DataAccess.MySql.DbHelper(sMysqlConnectionString, true);
            try
            {
                m_sbSql.Append(" select  c.TestDirection,c.TestAbbr,c.TestEntityType,c.TestVal,c.TestUM ,b.heat,b.mill,a.FileId,b.hdrProductQds,b.ShipingRef ");
                m_sbSql.Append(" from Files a ");
                m_sbSql.Append(" join header863 b  on a.FileId = b.FileId ");
                m_sbSql.Append(" join detail863 c on b.ID = c.HeaderId AND b.FileId = c.FileId ");
                m_sbSql.Append(" WHERE 1=1 AND c.ClassID =71 ");
                if (iHeaderId > 0 && iFileId > 0)
                {
                    m_sbSql.Append(" AND a.FileId = " + iFileId + " AND b.ID = " + iHeaderId);
                }
                else
                {
                    m_sbSql.Append("AND a.FileId = " + iFileId + " AND b.heat = '" + sHeat + "' AND b.mill = '" + sMill + "'  AND b.hdrProductQds <> '' AND b.ShipingRef = '" + sShipingRef + "' ");
                }

                MySqlParameter[] oMySqlParameter = { };
                ErrorLog.createLog("Cuerrent test sql." + m_sbSql.ToString());
                dtTestDetail = oDbHelper.GetDataTable(m_sbSql.ToString(), CommandType.Text, oMySqlParameter);

            }
            catch (Exception ex)
            {
                EdiError.LogError("863", Globalcl.FileId.ToString(), "", ex, "GetTestDetailWithOrWithoutQds");
            }
            finally
            {
                oDbHelper.CloseDatabaseConnection();
            }
            return dtTestDetail;
            
        }

        public DataTable GetPreviousTestDetail(string sMill, string sHeat, string sShipingRef, int iOldFileID, int iPono)
        {
            m_sbSql = new StringBuilder();
            DataTable dtTestDetail = new DataTable();
            iTECH.Library.DataAccess.MySql.DbHelper oDbHelper = new iTECH.Library.DataAccess.MySql.DbHelper(sMysqlConnectionString, true);
            try
            {
                m_sbSql.Append(" select  c.TestDirection,c.TestAbbr,c.TestEntityType,c.TestVal,c.TestUM ,b.heat,b.mill,a.FileId,b.hdrProductQds,b.ShipingRef ");
                m_sbSql.Append(" from Files a ");
                m_sbSql.Append(" join header863 b  on a.FileId = b.FileId ");
                m_sbSql.Append(" join detail863 c on b.ID = c.HeaderId AND b.FileId = c.FileId ");
                m_sbSql.Append(" WHERE 1=1 AND c.ClassID =71 ");
                m_sbSql.Append(" AND b.heat = '" + sHeat + "' AND b.mill = '" + sMill + "'  AND b.hdrProductQds <> '' AND b.ShipingRef = '" + sShipingRef + "' and b.origPoNo = '"+ iPono + "' ");
                if (iOldFileID > 0)
                {
                    m_sbSql.Append("  and a.fileid = " + iOldFileID);
                }
                m_sbSql.Append(" order by a.fileid desc ");
                MySqlParameter[] oMySqlParameter = { };

                ErrorLog.createLog("Previous test sql :" + m_sbSql.ToString());

                dtTestDetail = oDbHelper.GetDataTable(m_sbSql.ToString(), CommandType.Text, oMySqlParameter);

            }
            catch (Exception ex)
            {
                EdiError.LogError("863", Globalcl.FileId.ToString(), "", ex, "GetPreviousTestDetail");
            }
            finally
            {
                oDbHelper.CloseDatabaseConnection();
            }
            return dtTestDetail;

        }

        public DataTable GetPreviousMaterialStanderdDetail(string sMill, string sHeat, int iPo, string sCurentShipRefno)
        {
            m_sbSql = new StringBuilder();
            DataTable dtTestDetail = new DataTable();
            iTECH.Library.DataAccess.MySql.DbHelper oDbHelper = new iTECH.Library.DataAccess.MySql.DbHelper(sMysqlConnectionString, true);
            try
            {
                m_sbSql.Append(" select  c.mss_sdo,c.mss_std_id,c.mss_addnl_id,b.hdrProductQds,b.ShipingRef, b.FileId , b.origPoNo");
                m_sbSql.Append(" from Files a ");
                m_sbSql.Append(" join header863 b  on a.FileId = b.FileId ");
                m_sbSql.Append(" join detail863 c on b.ID = c.HeaderId AND b.FileId = c.FileId ");
                m_sbSql.Append(" WHERE 1=1 AND c.ClassID ='"+Characteristic863.METALSTANDARD+"' ");
                m_sbSql.Append(" AND b.heat = '" + sHeat + "' AND b.mill = '" + sMill + "'  AND b.hdrProductQds <> ''  and b.origPoNo = "+iPo+ " AND b.ShipingRef = '"+ sCurentShipRefno + "' ");

                MySqlParameter[] oMySqlParameter = { };

                ErrorLog.createLog("Previous metal std : " + m_sbSql.ToString());

                dtTestDetail = oDbHelper.GetDataTable(m_sbSql.ToString(), CommandType.Text, oMySqlParameter);

            }
            catch (Exception ex)
            {
                EdiError.LogError("863", Globalcl.FileId.ToString(), "", ex, "GetPreviousMaterialStanderdDetail");
            }
            finally
            {
                oDbHelper.CloseDatabaseConnection();
            }
            return dtTestDetail;

        }


        public DataTable GetCurrentMaterialStanderdDetail(string sMill, string sHeat, int iPo, int iFileId, int iHeaderId)
        {
            m_sbSql = new StringBuilder();
            DataTable dtTestDetail = new DataTable();
            iTECH.Library.DataAccess.MySql.DbHelper oDbHelper = new iTECH.Library.DataAccess.MySql.DbHelper(sMysqlConnectionString, true);
            try
            {
                m_sbSql.Append(" select  c.mss_sdo,c.mss_std_id,c.mss_addnl_id,b.hdrProductQds,b.ShipingRef ");
                m_sbSql.Append(" from Files a ");
                m_sbSql.Append(" join header863 b  on a.FileId = b.FileId ");
                m_sbSql.Append(" join detail863 c on b.ID = c.HeaderId AND b.FileId = c.FileId ");
                m_sbSql.Append(" WHERE 1=1 AND c.ClassID ='" + Characteristic863.METALSTANDARD + "' ");
                m_sbSql.Append(" AND b.heat = '" + sHeat + "' AND b.mill = '" + sMill + "'  and b.origPoNo = " + iPo + " AND a.FileId = " + iFileId + " AND b.ID = " + iHeaderId );

                MySqlParameter[] oMySqlParameter = { };

                ErrorLog.createLog("Current metal std his : " + m_sbSql.ToString());

                dtTestDetail = oDbHelper.GetDataTable(m_sbSql.ToString(), CommandType.Text, oMySqlParameter);

            }
            catch (Exception ex)
            {
                EdiError.LogError("863", Globalcl.FileId.ToString(), "", ex, "GetPreviousMaterialStanderdDetail");
            }
            finally
            {
                oDbHelper.CloseDatabaseConnection();
            }
            return dtTestDetail;

        }



        public DataTable GetChemistryDetailWithOrWithoutQds(string sMill, string sHeat, int iFileId = 0)
        {
            m_sbSql = new StringBuilder();
            DataTable dtTestDetail = new DataTable();
            iTECH.Library.DataAccess.MySql.DbHelper oDbHelper = new iTECH.Library.DataAccess.MySql.DbHelper(sMysqlConnectionString, true);
            try
            {
                m_sbSql.Append(" SELECT HeaderId, ClassID, ClassDescription, ChemistrylElement, ChemistryElementVAlue, ChemistrySequence, ChemistrylEntryType,b.heat,b.mill,a.FileId,a.QdsNo ");
                m_sbSql.Append(" from Files a ");
                m_sbSql.Append(" join header863 b  on a.FileId = b.FileId ");
                m_sbSql.Append(" join detail863 c on b.ID = c.HeaderId AND b.FileId = c.FileId ");
                m_sbSql.Append(" WHERE 1=1 AND c.ClassID =68 ");
                if (iFileId > 0)
                {
                    m_sbSql.Append(" AND a.FileId = " + iFileId);
                }
                else
                {
                    m_sbSql.Append(" AND b.heat = '" + sHeat + "' AND b.mill = '" + sMill + "'  AND a.QdsNo <> '' ");
                }

                MySqlParameter[] oMySqlParameter = { };
                dtTestDetail = oDbHelper.GetDataTable(m_sbSql.ToString(), CommandType.Text, oMySqlParameter);

            }
            catch (Exception ex)
            {
                EdiError.LogError("863", Globalcl.FileId.ToString(), "", ex, "GetChemistryDetailWithOrWithoutQds");
            }
            finally
            {
                oDbHelper.CloseDatabaseConnection();
            }
            return dtTestDetail;

        }


        public void CompareTwoTest(DataTable dtCurrentTest, DataTable dtPreviousTest)
        {

        }


        public DataTable GetCurrentPreviousChemistryDetail(string sMill, string sHeat, int iFileId = 0, string sRefNo = "")
        {
            m_sbSql = new StringBuilder();
            DataTable dtTestDetail = new DataTable();
            iTECH.Library.DataAccess.MySql.DbHelper oDbHelper = new iTECH.Library.DataAccess.MySql.DbHelper(sMysqlConnectionString, true);
            try
            {
                m_sbSql.Append(" SELECT HeaderId, ClassID, ClassDescription, ChemistrylElement, ChemistryElementVAlue, ChemistrySequence, ChemistrylEntryType,b.heat,b.mill,a.FileId,b.hdrHeatQds ");
                m_sbSql.Append(" from Files a ");
                m_sbSql.Append(" join header863 b  on a.FileId = b.FileId ");
                m_sbSql.Append(" join detail863 c on b.ID = c.HeaderId AND b.FileId = c.FileId ");
                m_sbSql.Append(" WHERE 1=1 AND c.ClassID =68 ");
                if (iFileId > 0 && sRefNo == "") // if  sRefNo == "" then we are finding current file chemistry
                {
                    m_sbSql.Append(" AND a.FileId = " + iFileId);
                }
                else if(!string.IsNullOrEmpty(sRefNo)) // if  sRefNo is not empty then we are finding previous file chemistry
                {
                    m_sbSql.Append(" AND b.heat = '" + sHeat + "' AND b.mill = '" + sMill + "'  AND b.hdrHeatQds <> ''   AND a.FileId <> " + iFileId);
                }

                //ErrorLog.createLog("HIS  sql: " + m_sbSql.ToString());

                MySqlParameter[] oMySqlParameter = { };
                dtTestDetail = oDbHelper.GetDataTable(m_sbSql.ToString(), CommandType.Text, oMySqlParameter);

            }
            catch (Exception ex)
            {
                EdiError.LogError("863", Globalcl.FileId.ToString(), "", ex, "GetCurrentPreviousChemistryDetail");
            }
            finally
            {
                oDbHelper.CloseDatabaseConnection();
            }
            return dtTestDetail;

        }


        public void CompareTwoChemistry(DataTable dtOldChemistry, DataTable dtCurrentChemistry, ref bool ChemMatch)
        {
            string sCurrentChemistrylElement, sCurrentChemistryElementVAlue;
            sCurrentChemistrylElement = sCurrentChemistryElementVAlue = string.Empty;

            string sOldChemistrylElement, sOldChemistryElementVAlue;
            sOldChemistrylElement = sOldChemistryElementVAlue = string.Empty;

            ErrorLog.createLog("dtOldChemistry.Rows.Count" + dtOldChemistry.Rows.Count);
            ErrorLog.createLog("dtCurrentChemistry.Rows.Count" + dtCurrentChemistry.Rows.Count);
            if (dtOldChemistry.Rows.Count == dtCurrentChemistry.Rows.Count)
            {
                foreach (DataRow oDataRow in dtCurrentChemistry.Rows)
                {
                    sCurrentChemistrylElement = BusinessUtility.GetString(oDataRow["ChemistrylElement"]);
                    sCurrentChemistryElementVAlue = BusinessUtility.GetString(oDataRow["ChemistryElementVAlue"]);

                    DataRow[] result = dtOldChemistry.Select("ChemistrylElement = '" + sTestAbbr + "'");
                    foreach (DataRow row in result)
                    {
                        sOldChemistrylElement = BusinessUtility.GetString(row["ChemistrylElement"]);
                        sOldChemistryElementVAlue = BusinessUtility.GetString(row["ChemistryElementVAlue"]);
                    }

                    if ((sCurrentChemistrylElement == sOldChemistrylElement) && (sCurrentChemistryElementVAlue == sOldChemistryElementVAlue))
                    {
                        ChemMatch = true;
                    }
                    else
                    {
                        ChemMatch = false;
                        ErrorLog.createLog("Chemistry did not matched matched..!!");
                        break;
                    }

                }
                if (ChemMatch)
                {
                    ErrorLog.createLog("Chemistry matched..!!");
                }
                else
                {
                    ErrorLog.createLog("Chemistry did not matched..!!");
                }

            }
            else if(dtOldChemistry.Rows.Count == 0)
            {
                ChemMatch = true;
            }
            else
            {
                ErrorLog.createLog("Chemistry did not matched..!!");
            }
            
                

            }





    }
}
