﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.Configuration;
using System.Data;


using iTECH.Library.Utilities;



namespace SecureFTP
{
    
    class Edi210Parser
    {
        static string CusVenId = BusinessUtility.GetString(ConfigurationManager.AppSettings["CusVenId"]);
        CreateBaucher createB = new CreateBaucher();
        DataTable dt = new DataTable();
        string InvoiceStatus = "0";
        string InvoiceActive = "1";
        Decimal discamout = 0.0M;
        string GlacCode = string.Empty;
        string Branch = "CRP";  // CHI
        string subac = string.Empty;
        string shprefno = string.Empty;
        string IsActive = "0";
        string CreatedDate = DateTime.Now.ToString("yyyy-MM-dd");        
        #region Keyword
        public struct EdiKeyWords
        {
            public const string ISA = "ISA";
            public const string GS = "GS";
            public const string ST = "ST";
            public const string B3 = "B3";
            public const string G62 = "G62";
            public const string R3 = "R3";
            public const string N9 = "N9";
            public const string SPO = "SPO";
            public const string L3 = "L3";            

        }
        #endregion

        string[] m_sArr = null;

        Files oFiles;
        Configuration oConfiguration;
        int m_iFileId = 0;

        public struct VOUCHARSTSTUS
        {
            public const string PENDING = "N";
            public const string SUCCESS = "Y";
            public const string ERROR = "E";
        }

        public Edi210Parser(string[] sArr, int iFileId)
        {
            try
            {
                DataTable dtVoucharstatus = new DataTable();
            ErrorLog.createLog("m_iFileId = " + iFileId);
            oFiles = new Files(iFileId);
            m_sArr = sArr;
            m_iFileId = iFileId;
            m_sArr = m_sArr.Where(x => !string.IsNullOrEmpty(x)).ToArray(); // removing empty segment 

            oConfiguration = new Configuration();
            oConfiguration.PopulateConfigurationData(sArr);
            CusVenId = oConfiguration.VendorId;
          
            this.StartProcessing(m_sArr);

            string sVendorandBranch = createB.GetVendorandBranch(BusinessUtility.GetString(createB.PoNo));
            if (sVendorandBranch.Length > 0)
            {
                string[] strlist = sVendorandBranch.Split('-');
                if (strlist.Length > 0)
                {
                    CusVenId = strlist[0];
                    Branch = strlist[1];
                }
            }

            int ID = createB.SaveDetails(CusVenId, createB.InvoiceNumber, InvoiceStatus, BusinessUtility.GetString(createB.PoNo), Branch, BusinessUtility.GetString(createB.VoucherAmount), createB.sDueDate, BusinessUtility.GetString(discamout), InvoiceActive, createB.sInvoiceDate, GlacCode, subac, shprefno, IsActive, CreatedDate);
            dt = createB.GetDeatils(ID);
            int stratixID = createB.GetStratixID();
            createB.SetDataTitanium(stratixID, dt);
            //Testing
            createB.CreateVoucher(createB);


                dtVoucharstatus = createB.GetVoucharStatus(createB.LastGateWayId);
                if(dtVoucharstatus.Rows.Count >0)
                {
                    InvoiceStatus = BusinessUtility.GetString(FnVoucharStatus(BusinessUtility.GetString(dtVoucharstatus.Rows[0]["itn_sys_cd"])));
                    IsActive = "1";
                    createB.UpdateDetails(InvoiceStatus, IsActive, ID);
                }
            }
            catch(Exception ex)
            {
                ErrorLog.createLog("Class : Edi210Parser.cs ; Function : Edi210Parser ; Error: " + ex.Message);
            }
        }

        private int FnVoucharStatus(string input)
        {
            int n = 0;
            if (input == VOUCHARSTSTUS.SUCCESS)
            { n = 1; }
            else if (input == VOUCHARSTSTUS.PENDING)
            { n = 2; }
            else if (input == VOUCHARSTSTUS.ERROR)
            { n = 3; }                                 
            return n;
        }
        public void StartProcessing(string[] Arry)
        {
            for(int i=0; i<Arry.Length; i++)
            {
                Getdata(Arry[i]);
            }  
                                 
        }
        public void Getdata(string str)
        {            
          
            string[] substr = str.Split('*');
            if(str.Contains(EdiKeyWords.ISA))
            {
                
            }
            else if (str.Contains(EdiKeyWords.B3))
            {
                createB.InvoiceNumber = BusinessUtility.GetString(str.Split('*')[1]);
                createB.sDueDate = DateTime.ParseExact(str.Split('*')[6], "yyyyMMdd", null).ToString("yyyy-MM-dd"); // BusinessUtility.GetDateTime(str.Split('*')[6]).ToString("yyyy-MM-dd");
                createB.sInvoiceDate = DateTime.ParseExact(str.Split('*')[4], "yyyyMMdd", null).ToString("yyyy-MM-dd"); // BusinessUtility.GetDateTime(str.Split('*')[4]).ToString("yyyy-MM-dd");
                //Sir, aap nahi ho to koi dhyan hi nahi deta.
            }
            else if(str.Contains(EdiKeyWords.GS))
            { }
            else if (str.Contains(EdiKeyWords.ST))
            { }            
            else if (str.Contains(EdiKeyWords.G62))
            { }
            else if (str.Contains(EdiKeyWords.R3))
            { }
            else if (str.Contains(EdiKeyWords.N9))
            {
                if(BusinessUtility.GetString(str.Split('*')[1]) =="PO")
                { createB.PoNo = BusinessUtility.GetInt(str.Split('*')[2].Substring(3)); }
            }
            else if (str.Contains(EdiKeyWords.SPO))
            {
               // createB.PoNo = BusinessUtility.GetInt(str.Split('*')[1]);
            }
            else if (str.Contains(EdiKeyWords.L3))
            { createB.VoucherAmount = BusinessUtility.GetDecimal(str.Split('*')[5]);}          
        }
    }
}
