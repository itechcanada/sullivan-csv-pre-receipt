﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Configuration;
using System.Data;

using MySql.Data.MySqlClient;
using iTECH.Library.Utilities;

namespace SecureFTP
{
    class CreateBaucher
    {
        static string sMysqlConnectionString = ConfigurationManager.ConnectionStrings["NewConnectionString"].ToString();
        static string dateformat = ConfigurationManager.AppSettings["DateFormat"];
        public string SourceCompanyId { get; set; }
        public int LastGateWayId { get; set; }
        public string VoucherPrefix { get; set; }
        public string sEntryDate { get; set; }
        public string stxVendorId { get; set; }
        public string InvoiceNumber { get; set; }
        public string ExternalRef { get; set; }
        public string sInvoiceDate { get; set; }
        public int PoNo { get; set; }
        public string PoPrefix { get; set; }

        public int PoItem { get; set; }
        public string Branch { get; set; }
        public decimal PreTaxVoucherAmount { get; set; }
        public decimal VoucherAmount { get; set; }
        public decimal DiscountableDiscount { get; set; }
        public string Description { get; set; }
        public string Currency { get; set; }
        public decimal ExchangeRate { get; set; }
        public int PaymentTerm { get; set; }
        public string sDueDate { get; set; }
        public string sDiscountDate { get; set; }
        public decimal DiscAmount { get; set; }
        public string ItemRemarks { get; set; }
        public string PaymentType { get; set; }
        public int VoucherCrossRefNo { get; set; }
        public string AuthenticateRef { get; set; }
        public string VoucherCategory { get; set; }
        public string sServiceFulmintDate { get; set; }
        public int PrepaymentEli { get; set; }
        public string TransactionStatusAction { get; set; }
        public string TransactionReason { get; set; }
        public string TransactionStatus { get; set; }
        public string TransactionStatusRemarks { get; set; }
        public string PaymentStatusAction { get; set; }
        public string PaymentReason { get; set; }
        public string PaymentStatus { get; set; }
        public string PyamentStatusRemarks { get; set; }
        public string CompanyId { get; set; }
        public string GlCode { get; set; }
        public string SubAc { get; set; }
        public string Remarks { get; set; }
        public int SeqNo { get; set; }
        public int CreditAmount { get; set; }
        public string PrePay { get; set; }
        public string lst { get; set; }

        public string DiscountTerm { get; set; }


        
        public void SetDataTitanium(int id, DataTable dt)
        {
            this.SourceCompanyId = ConfigurationManager.AppSettings["SourceCompanyId"];
            this.LastGateWayId = BusinessUtility.GetInt(id + 1);
            this.VoucherPrefix = BusinessUtility.GetString(ConfigurationManager.AppSettings["VoucherPrefix"]);
            this.sEntryDate = BusinessUtility.GetDateTime(dt.Rows[0]["createddate"]).ToString(dateformat); //titanium
            this.stxVendorId = BusinessUtility.GetString(dt.Rows[0]["vendorId"].ToString());   //for titanium
            this.InvoiceNumber = BusinessUtility.GetString(dt.Rows[0]["invoicenumber"].ToString()).Replace("'", "''").ToUpper();
            this.ExternalRef = BusinessUtility.GetString(dt.Rows[0]["shprefno"]).Replace("'", "''");
            this.sInvoiceDate = BusinessUtility.GetDateTime(dt.Rows[0]["invoicedate"]).ToString(dateformat); // yyyy-MM-dd   //for titanium
            this.PoNo = BusinessUtility.GetInt(dt.Rows[0]["pono"]);
            if (BusinessUtility.GetInt(dt.Rows[0]["pono"]) > 0)
            {
                this.PoPrefix = BusinessUtility.GetString(ConfigurationManager.AppSettings["PoPrefix"]);
            }
            else
            {
                this.PoPrefix = "";
            }
            this.PoItem = BusinessUtility.GetInt(ConfigurationManager.AppSettings["PoItem"]);
            this.Branch = BusinessUtility.GetString(dt.Rows[0]["branch"].ToString());
            this.PreTaxVoucherAmount = BusinessUtility.GetDecimal(ConfigurationManager.AppSettings["PreTaxVoucherAmount"]);
            this.VoucherAmount = BusinessUtility.GetDecimal(dt.Rows[0]["totalamount"]);
            this.DiscountableDiscount = BusinessUtility.GetDecimal(ConfigurationManager.AppSettings["DiscountableDiscount"]);
            this.Description = BusinessUtility.GetString(ConfigurationManager.AppSettings["Description"]);
            this.Currency = BusinessUtility.GetString(ConfigurationManager.AppSettings["Currency"]);
            this.ExchangeRate = BusinessUtility.GetDecimal(ConfigurationManager.AppSettings["ExchangeRate"]);
            this.PaymentTerm = BusinessUtility.GetInt(ConfigurationManager.AppSettings["PaymentTerm"]);            
            this.sDueDate = BusinessUtility.GetDateTime(dt.Rows[0]["duedate"]).ToString(dateformat);  //for titanium            

            //for titanium
            if (this.sDiscountDate == "0001-01-01")
            {
                this.sDiscountDate = "";
            }
            
            this.DiscAmount = BusinessUtility.GetDecimal(dt.Rows[0]["discamount"]);

            if (BusinessUtility.GetDecimal(dt.Rows[0]["discamount"]) != 0)
            {
                this.sDiscountDate = BusinessUtility.GetDateTime(dt.Rows[0]["discdate"]).ToString(dateformat);
            }
            else
            {
                this.sDiscountDate = "";
            }
            this.ItemRemarks = BusinessUtility.GetString(ConfigurationManager.AppSettings["ItemRemarks"]);

            this.PaymentType = BusinessUtility.GetString(ConfigurationManager.AppSettings["PaymentType"]);
            this.VoucherCrossRefNo = BusinessUtility.GetInt(ConfigurationManager.AppSettings["VoucherCrossRefNo"]);
            this.AuthenticateRef = BusinessUtility.GetString(ConfigurationManager.AppSettings["AuthenticateRef"]);
            this.VoucherCategory = ""; // BusinessUtility.GetString(dt.Rows[0]["vouchercategory"]); //"MAT";

            this.sServiceFulmintDate = BusinessUtility.GetDateTime(dt.Rows[0]["createddate"]).ToString(dateformat); //for titanium
            this.PrepaymentEli = BusinessUtility.GetInt(ConfigurationManager.AppSettings["PrepaymentEli"]);
            this.TransactionStatusAction = ""; // BusinessUtility.GetString(dt.Rows[0]["TransactionStatusAction"].ToString());// BusinessUtility.GetString(ConfigurationManager.AppSettings["TransactionStatusAction"]);
            this.TransactionReason = ""; // BusinessUtility.GetString(dt.Rows[0]["TransactionReason"].ToString()); // BusinessUtility.GetString(ConfigurationManager.AppSettings["TransactionReason"]);
            this.TransactionStatus = ""; // BusinessUtility.GetString(dt.Rows[0]["TransactionStatus"].ToString()); //BusinessUtility.GetString(ConfigurationManager.AppSettings["TransactionStatus"]);
            this.TransactionStatusRemarks = BusinessUtility.GetString(ConfigurationManager.AppSettings["TransactionStatusRemarks"]);
            this.PaymentStatusAction = BusinessUtility.GetString(ConfigurationManager.AppSettings["PaymentStatusAction"]);
            this.PaymentReason = BusinessUtility.GetString(ConfigurationManager.AppSettings["PaymentReason"]);
            this.PaymentStatus = BusinessUtility.GetString(ConfigurationManager.AppSettings["PaymentStatus"]);
            this.PyamentStatusRemarks = BusinessUtility.GetString(ConfigurationManager.AppSettings["Description"]);

            this.CompanyId = BusinessUtility.GetString(ConfigurationManager.AppSettings["CompanyId"]);

            //for TCIGGD_rec
            this.GlCode = BusinessUtility.GetString(dt.Rows[0]["glacCode"].ToString());
            this.SubAc = BusinessUtility.GetString(dt.Rows[0]["subac"].ToString());
            this.Remarks = ""; // BusinessUtility.GetString(dt.Rows[0]["Remarks"].ToString());
            this.SeqNo = 0; // BusinessUtility.GetInt(ConfigurationManager.AppSettings["SequenceNo"]);            
            this.CreditAmount = BusinessUtility.GetInt(ConfigurationManager.AppSettings["CreditAmount"]);
            this.PrePay = "";// BusinessUtility.GetString(dt.Rows[0]["PreApprove"].ToString());
            //if (this.PrePay == "N")
            //{
            //    this.lst = SetSubAccountDetail();
            //}
        }

        public bool CreateVoucher(CreateBaucher objStxInvoice)
        {            
            iTECH.Library.DataAccess.ODBC.DbHelper dbHelp1 = new iTECH.Library.DataAccess.ODBC.DbHelper(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);
            string strSQL = "INSERT INTO APIGVC_rec(gvc_src_co_id,gvc_gat_ctl_no,gvc_vchr_pfx,gvc_ent_dt,gvc_ven_id,gvc_ven_inv_no,gvc_extl_ref,gvc_ven_inv_dt,gvc_po_pfx,gvc_po_no,";
            strSQL += "gvc_po_itm,gvc_vchr_brh,gvc_ptx_vchr_amt,gvc_vchr_amt,gvc_dscb_amt,gvc_desc30,gvc_cry,gvc_exrt,gvc_pttrm,gvc_disc_trm,gvc_due_dt,gvc_disc_dt,gvc_disc_amt,";
            strSQL += "gvc_chk_itm_rmk,gvc_pmt_typ,gvc_vchr_xref,gvc_auth_ref,gvc_vchr_cat,gvc_svc_ffm_dt,gvc_ppmt_elgbl,gvc_trs_sts_actn,gvc_trs_rsn,gvc_trs_sts,gvc_trs_sts_rmk,";
            strSQL += "gvc_pmt_sts_actn,gvc_pmt_rsn,gvc_pmt_sts,gvc_pmt_sts_rmk)Values ( ";
            strSQL += "'" + objStxInvoice.SourceCompanyId + "'," + objStxInvoice.LastGateWayId + ",'" + objStxInvoice.VoucherPrefix + "','" + objStxInvoice.sEntryDate + "','" + objStxInvoice.stxVendorId + "','" + objStxInvoice.InvoiceNumber + "','" + objStxInvoice.ExternalRef + "','" + objStxInvoice.sInvoiceDate + "','" + objStxInvoice.PoPrefix + "'," + objStxInvoice.PoNo + "," + objStxInvoice.PoItem + ",'" + objStxInvoice.Branch + "'," + objStxInvoice.PreTaxVoucherAmount.ToString().Replace(",", ".") + "," + objStxInvoice.VoucherAmount.ToString().Replace(",", ".") + "," + objStxInvoice.DiscountableDiscount.ToString().Replace(",", ".") + ",'" + objStxInvoice.Description + "','" + objStxInvoice.Currency + "'," + objStxInvoice.ExchangeRate.ToString().Replace(",", ".") + "," + objStxInvoice.PaymentTerm + ",'" + objStxInvoice.DiscountTerm + "','" + objStxInvoice.sDueDate + "',";
            strSQL += "'" + objStxInvoice.sDiscountDate + "'," + objStxInvoice.DiscAmount.ToString().Replace(",", ".") + ",'" + objStxInvoice.ItemRemarks + "','" + objStxInvoice.PaymentType + "'," + objStxInvoice.VoucherCrossRefNo + ",'" + objStxInvoice.AuthenticateRef + "','" + objStxInvoice.VoucherCategory + "','" + objStxInvoice.sServiceFulmintDate + "'," + objStxInvoice.PrepaymentEli + ",'" + objStxInvoice.TransactionStatusAction + "','" + objStxInvoice.TransactionReason + "','" + objStxInvoice.TransactionStatus + "','" + TransactionStatusRemarks + "','" + objStxInvoice.PaymentStatusAction + "','" + objStxInvoice.PaymentReason + "','" + objStxInvoice.PaymentStatus + "','" + objStxInvoice.PyamentStatusRemarks + "')";
            ErrorLog.createLog("FirstQuery :" + strSQL);
            
            //StringBuilder strSQL1 = new StringBuilder();
            //if (lst != null)
            //{
            //    strSQL1.Append("insert into TCIGGD_rec select * from ( ");                
            //    for (int i = 0; i < lst.Count; i++)
            //    {                    
            //        strSQL1.Append("select '" + objStxInvoice.SourceCompanyId + "'," + objStxInvoice.LastGateWayId + "," + (i + 1) + ",'" + BusinessUtility.GetString(lst[i].GlCode) + "','" + BusinessUtility.GetString(lst[i].SubAccountCode) + "'," + BusinessUtility.GetDecimal(lst[i].Amount) + "," + objStxInvoice.CreditAmount + ",'" + objStxInvoice.Remarks + "' from table(set{1})");
            //        if (i != lst.Count - 1)
            //        {
            //            strSQL1.Append(" union all ");
            //        }
            //    }
            //    strSQL1.Append(")");
            //    ErrorLog.createLog("SecondQuery :" + strSQL1.ToString());
            //}            
            string strSQL2 = "insert into SCTITN_rec values('" + objStxInvoice.SourceCompanyId + "', " + objStxInvoice.LastGateWayId + ", '" + objStxInvoice.CompanyId + "', CURRENT, null, null , null, 'N',0, 'VCH')";
            ErrorLog.createLog("ThirdQuery :" + strSQL2);
            try
            {                
                if (dbHelp1.ExecuteNonQuery(strSQL, System.Data.CommandType.Text) > 0)
                {
                    //if (objStxInvoice.PrePay != BusinessUtility.GetString(PreApprove.Yes) && lst.Count > 0)
                    //{

                    //    if (dbHelp1.ExecuteNonQuery(strSQL1.ToString(), System.Data.CommandType.Text) > 0)
                    //    {
                    //        dbHelp1.ExecuteNonQuery(strSQL2, System.Data.CommandType.Text);
                    //    }
                    //}
                    //else
                    //{
                        dbHelp1.ExecuteNonQuery(strSQL2, System.Data.CommandType.Text);
                   // }
                }                
                return true;
            }

            catch (Exception ex)
            {
                ErrorLog.CreateLog(ex);                
                throw ex;
            }
            finally
            {

                dbHelp1.CloseDatabaseConnection();
            }
        }

        public int SaveDetails( string venderId, string InvoiceNumber, string InvoiceStatus, string PONo, string Branch, string TotalAmount, string DueDate,  string discAmount, string InvoiceActive, string invoicedate, string glAcCode, string subac,string shprefno, string IsActive,  string CreatedDate)
        {
            iTECH.Library.DataAccess.MySql.DbHelper oDbHelper = new iTECH.Library.DataAccess.MySql.DbHelper(sMysqlConnectionString, true);
            int ID = 0;
            try
            {
                string sQuery = " INSERT INTO `stxinvoice`(`vendorId`,`invoiceNumber`,`invoiceStatus`,`poNo`,`branch`,`totalAmount`,`dueDate`,`discAmount`,`InvoiceActive`,`invoicedate`, " +
                           " `glAcCode`,`subac`,`shprefno`,`IsActive`,`CreatedDate`) VALUES " +
                           " ('"+ venderId + "', '"+ InvoiceNumber + "','"+ InvoiceStatus + "','"+ PONo + "','"+ Branch + "','"+ TotalAmount + "', '"+ DueDate + "','"+ discAmount + "', '"+ InvoiceActive + "', '"+ invoicedate + "', '"+ glAcCode + "','"+ subac + "', '"+ shprefno + "','"+ IsActive + "', '"+ CreatedDate + "');";
                MySqlParameter[] oMySqlParameter = { };

                oDbHelper.ExecuteNonQuery(sQuery, CommandType.Text, oMySqlParameter);
              ID = oDbHelper.GetLastInsertID();
            }
            catch
            {
                throw;
            }
            return ID;                   
        }

        public void UpdateDetails(string InvoiceStatus, string IsActive , int ID)
        {
            iTECH.Library.DataAccess.MySql.DbHelper oDbHelper = new iTECH.Library.DataAccess.MySql.DbHelper(sMysqlConnectionString, true);            
            try
            {
                string sQuery = " Update `stxinvoice` set `invoiceStatus`= '" + InvoiceStatus + "' , `IsActive`='" + IsActive + "' where id='" + ID + "'";
                MySqlParameter[] oMySqlParameter = { };
                oDbHelper.ExecuteNonQuery(sQuery, CommandType.Text, oMySqlParameter);                
            }
            catch
            {
                throw;
            }            
        }


        public DataTable GetDeatils(int ID)
        {
            iTECH.Library.DataAccess.MySql.DbHelper oDbHelper = new iTECH.Library.DataAccess.MySql.DbHelper(sMysqlConnectionString, true);
            DataTable dt = new DataTable();
            try {
                string sQuery = " select * from stxinvoice where id ='" + ID + "' ";
                dt = oDbHelper.GetDataTable(sQuery, CommandType.Text, new MySqlParameter[] { });
            }
            catch { throw; }
            return dt;
        }

        public int GetStratixID()
        {
            int n= 0;
            try
            {
                iTECH.Library.DataAccess.ODBC.DbHelper dbHelp1 = new iTECH.Library.DataAccess.ODBC.DbHelper(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);
                string strquery = "select first 1 gvc_gat_ctl_no from APIGVC_rec order by gvc_gat_ctl_no desc;";
                ErrorLog.createLog("Query For Stratix ID :" + strquery);
                object obj = dbHelp1.GetValue(strquery, CommandType.Text);
                n = BusinessUtility.GetInt(obj);
            }
            catch { throw; }
            return n;
        }

        public string GetVendorandBranch(string PO)
        {
            string sVendorandBranch = string.Empty;
            try {
                iTECH.Library.DataAccess.ODBC.DbHelper dbHelp1 = new iTECH.Library.DataAccess.ODBC.DbHelper(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);
                string strQuery = " select concat(concat(poh_ven_id,'-'),poh_po_brh) as ven_brh from potpoh_rec where poh_po_no='" + PO + "';";
                ErrorLog.createLog("Query For Vendor and branch :" + strQuery);
                object obj = dbHelp1.GetValue(strQuery, CommandType.Text);
                sVendorandBranch = BusinessUtility.GetString(obj);
            }
            catch { }
            return sVendorandBranch;
        }

        public DataTable  GetVoucharStatus( int controlNo)
        {
            DataTable dt = new DataTable();
            try {
                iTECH.Library.DataAccess.ODBC.DbHelper dbHelp1 = new iTECH.Library.DataAccess.ODBC.DbHelper(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);
                string strquery = " select * from SCTITN_rec  where itn_gat_ctl_no='" + controlNo + "';";
               dt = dbHelp1.GetDataTable(strquery, CommandType.Text, null);
            }
            catch
            { throw; }
            return dt;
        }
    }
}
