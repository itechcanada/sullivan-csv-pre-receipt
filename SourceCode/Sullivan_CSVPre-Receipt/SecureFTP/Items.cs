﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SecureFTP
{
    class Items
    {
        public Items()
        {

        }


        string sNoOfUnitShipped = string.Empty;
        public string NoOfUnitShipped
        {
            get { return sNoOfUnitShipped; }
            set { sNoOfUnitShipped = value; }
        }

        string sUnitOfMeasurement = string.Empty;
        public string UnitOfMeasurement
        {
            get { return sUnitOfMeasurement; }
            set { sUnitOfMeasurement = value; }
        }

    }
}
