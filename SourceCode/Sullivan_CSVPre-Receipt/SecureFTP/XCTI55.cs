﻿using iTECH.Library.Utilities;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Text;

namespace SecureFTP
{
    class XCTI55
    {

        StringBuilder m_sbSql = new StringBuilder();
        string m_sSql = string.Empty;
        int m_iDetailID = 0;

        static string sMysqlConnectionString = ConfigurationManager.ConnectionStrings["NewConnectionString"].ToString();

                
        string sCmpyId = string.Empty;
        public string CmpyId
        {
            get { return sCmpyId; }
            set { sCmpyId = value; }
        }

        string sIntchgPfx = string.Empty;
        public string IntchgPfx
        {
            get { return sIntchgPfx; }
            set { sIntchgPfx = value; }
        }

        string sIntchgNo = "0";
        public string IntchgNo
        {
            get { return sIntchgNo; }
            set { sIntchgNo = value; }
        }

        string sIntchgItm = "0";
        public string IntchgItm
        {
            get { return sIntchgItm; }
            set { sIntchgItm = value; }
        }

        string sIntchgSitm = "0";
        public string IntchgSitm
        {
            get { return sIntchgSitm; }
            set { sIntchgSitm = value; }
        }

        string sForm = "0";
        public string Form
        {
            get { return sForm; }
            set { sForm = value; }
        }

        string sGrid ="0";
        public string Grid
        {
            get { return sGrid; }
            set { sGrid = value; }
        }

        string sSize = "0";
        public string Size
        {
            get { return sSize; }
            set { sSize = value; }
        }

        string sFinish = "0";
        public string Finish
        {
            get { return sFinish; }
            set { sFinish = value; }
        }

        string sEfEver = "0";
        public string EfEver
        {
            get { return sEfEver; }
            set { sEfEver = value; }
        }

        string sWidth = "0";
        public string Width
        {
            get { return sWidth; }
            set { sWidth = value; }
        }

        string sLength = "0";
        public string Length
        {
            get { return sLength; }
            set { sLength = value; }
        }

        string sDimDsgn = "0";
        public string DimDsgn
        {
            get { return sDimDsgn; }
            set { sDimDsgn = value; }
        }

        string sIdia = "0";
        public string Idia
        {
            get { return sIdia; }
            set { sIdia = value; }
        }

        string sGaSize = "0";
        public string GaSize
        {
            get { return sGaSize; }
            set { sGaSize = value; }
        }

        string sGaType = "0";
        public string GaType
        {
            get { return sGaType; }
            set { sGaType = value; }
        }

        string sRdmDim1 = "0";
        public string RdmDim1
        {
            get { return sRdmDim1; }
            set { sRdmDim1 = value; }
        }

        string sRdmDim2 = "0";
        public string RdmDim2
        {
            get { return sRdmDim2; }
            set { sRdmDim2 = value; }
        }

        string sRdmDim3 = "0";
        public string RdmDim3
        {
            get { return sRdmDim3; }
            set { sRdmDim3 = value; }
        }

        string sRdmDim4 = "0";
        public string RdmDim4
        {
            get { return sRdmDim4; }
            set { sRdmDim4 = value; }
        }

        string sRdmDim5 = "0";
        public string RdmDim5
        {
            get { return sRdmDim5; }
            set { sRdmDim5 = value; }
        }

        string sRdmDim6 = "0";
        public string RdmDim6
        {
            get { return sRdmDim6; }
            set { sRdmDim6 = value; }
        }

        string sRdmDim7 = "0";
        public string RdmDim7
        {
            get { return sRdmDim7; }
            set { sRdmDim7 = value; }
        }

        string sRdmDim8 = "0";
        public string RdmDim8
        {
            get { return sRdmDim8; }
            set { sRdmDim8 = value; }
        }

        string sImgDocFmt = string.Empty;
        public string ImgDocFmt
        {
            get { return sImgDocFmt; }
            set { sImgDocFmt = value; }
        }

        string sRdmImgDocId = "0";
        public string RdmImgDocId
        {
            get { return sRdmImgDocId; }
            set { sRdmImgDocId = value; }
        }

        string sUsbWdth1 = "0";
        public string UsbWdth1
        {
            get { return sUsbWdth1; }
            set { sUsbWdth1 = value; }
        }

        string sUsbLength1 = "0";
        public string UsbLength1
        {
            get { return sUsbLength1; }
            set { sUsbLength1 = value; }
        }

        string sUsbArea1 = "0";
        public string UsbArea1
        {
            get { return sUsbArea1; }
            set { sUsbArea1 = value; }
        }

        string sUsbWdth2 = "0";
        public string UsbWdth2
        {
            get { return sUsbWdth2; }
            set { sUsbWdth2 = value; }
        }

        string sUsbLength2 = "0";
        public string UsbLength2
        {
            get { return sUsbLength2; }
            set { sUsbLength2 = value; }
        }

        string sUsbArea2 = "0";
        public string UsbArea2
        {
            get { return sUsbArea2; }
            set { sUsbArea2 = value; }
        }

        string sUsbWdth3 = "0";
        public string UsbWdth3
        {
            get { return sUsbWdth3; }
            set { sUsbWdth3 = value; }
        }

        string sUsbLength3 = "0";
        public string UsbLength3
        {
            get { return sUsbLength3; }
            set { sUsbLength3 = value; }
        }

        string sUsbArea3 = "0";
        public string UsbArea3
        {
            get { return sUsbArea3; }
            set { sUsbArea3 = value; }
        }

        string sUsbWdth4 = "0";
        public string UsbWdth4
        {
            get { return sUsbWdth4; }
            set { sUsbWdth4 = value; }
        }

        string sUsbLength4 = "0";
        public string UsbLength4
        {
            get { return sUsbLength4; }
            set { sUsbLength4 = value; }
        }

        string sUsbArea4 = "0";
        public string UsbArea4
        {
            get { return sUsbArea4; }
            set { sUsbArea4 = value; }
        }

        string sUsbWdth5 = "0";
        public string UsbWdth5
        {
            get { return sUsbWdth5; }
            set { sUsbWdth5 = value; }
        }

        string sUsbLength5 = "0";
        public string UsbLength5
        {
            get { return sUsbLength5; }
            set { sUsbLength5 = value; }
        }

        string sUsbArea6 = "0";
        public string UsbArea6
        {
            get { return sUsbArea6; }
            set { sUsbArea6 = value; }
        }


        string sUsbWdth7 = "0";
        public string UsbWdth7
        {
            get { return sUsbWdth7; }
            set { sUsbWdth7 = value; }
        }

        string sUsbLength7 = "0";
        public string UsbLength7
        {
            get { return sUsbLength7; }
            set { sUsbLength7 = value; }
        }

        string sUsbArea7 = "0";
        public string UsbArea7
        {
            get { return sUsbArea7; }
            set { sUsbArea7 = value; }
        }

        string sUsbWdth8 = "0";
        public string UsbWdth8
        {
            get { return sUsbWdth8; }
            set { sUsbWdth8 = value; }
        }

        string sUsbLength8 = "0";
        public string UsbLength8
        {
            get { return sUsbLength8; }
            set { sUsbLength8 = value; }
        }

        string sUsbArea8 = "0";
        public string UsbArea8
        {
            get { return sUsbArea8; }
            set { sUsbArea8 = value; }
        }

        string sUsbWdth9 = "0";
        public string UsbWdth9
        {
            get { return sUsbWdth9; }
            set { sUsbWdth9 = value; }
        }

        string sUsbLength9 = "0";
        public string UsbLength9
        {
            get { return sUsbLength9; }
            set { sUsbLength9 = value; }
        }

        string sUsbArea9 = "0";
        public string UsbArea9
        {
            get { return sUsbArea9; }
            set { sUsbArea9 = value; }
        }

        string sUsbWdth10 = "0";
        public string UsbWdth10
        {
            get { return sUsbWdth10; }
            set { sUsbWdth10 = value; }
        }

        string sUsbLength10 = "0";
        public string UsbLength10
        {
            get { return sUsbLength10; }
            set { sUsbLength10 = value; }
        }

        string sUsbArea10 = "0";
        public string UsbArea10
        {
            get { return sUsbArea10; }
            set { sUsbArea10 = value; }
        }

        string sTagNo = string.Empty;
        public string TagNo
        {
            get { return sTagNo; }
            set { sTagNo = value; }
        }

        string sMill = "0";
        public string Mill
        {
            get { return sMill; }
            set { sMill = value; }
        }

        string sHeat = string.Empty;
        public string Heat
        {
            get { return sHeat; }
            set { sHeat = value; }
        }

        string sMillId = "0";
        public string MillId
        {
            get { return sMillId; }
            set { sMillId = value; }
        }

        string sInvtType = "0";
        public string InvtType
        {
            get { return sInvtType; }
            set { sInvtType = value; }
        }

        string sRjctRsn = string.Empty;
        public string RjctRsn
        {
            get { return sRjctRsn; }
            set { sRjctRsn = value; }
        }

        //string sRjctDate = "01-01-1900";
        string sRjctDate = "null";
        public string RjctDate
        {
            get { return sRjctDate; }
            set { sRjctDate = value; }
        }

        string sRjctLgnId = "0";
        public string RjctLgnId
        {
            get { return sRjctLgnId; }
            set { sRjctLgnId = value; }
        }

        string sRjctRmk = string.Empty;
        public string RjctRmk
        {
            get { return sRjctRmk; }
            set { sRjctRmk = value; }
        }

        string sInvtQlty = "0";
        public string InvtQlty
        {
            get { return sInvtQlty; }
            set { sInvtQlty = value; }
        }

        string sLoc = string.Empty;
        public string Loc
        {
            get { return sLoc; }
            set { sLoc = value; }
        }

        string sInvtPcs = "0";
        public string InvtPcs
        {
            get { return sInvtPcs; }
            set { sInvtPcs = value; }
        }

        string sInvtMsr = "0";
        public string InvtMsr
        {
            get { return sInvtMsr; }
            set { sInvtMsr = value; }
        }

        string sInvtWgt = "0";
        public string InvtWgt
        {
            get { return sInvtWgt; }
            set { sInvtWgt = value; }
        }

        string sInvtPcsTyp = string.Empty;
        public string InvtPcsTyp
        {
            get { return sInvtPcsTyp; }
            set { sInvtPcsTyp = value; }
        }

        string sInvtMsrTyp = string.Empty;
        public string InvtMsrTyp
        {
            get { return sInvtMsrTyp; }
            set { sInvtMsrTyp = value; }
        }

        string sInvtWgtTyp = string.Empty;
        public string InvtWgtTyp
        {
            get { return sInvtWgtTyp; }
            set { sInvtWgtTyp = value; }
        }

        string sShpntPcs = "0";
        public string ShpntPcs
        {
            get { return sShpntPcs; }
            set { sShpntPcs = value; }
        }

        string sShpntMsr = "0";
        public string ShpntMsr
        {
            get { return sShpntMsr; }
            set { sShpntMsr = value; }
        }

        string sShpntWgt = "0";
        public string ShpntWgt
        {
            get { return sShpntWgt; }
            set { sShpntWgt = value; }
        }

        string sShpntPcsTyp = string.Empty;
        public string ShpntPcsTyp
        {
            get { return sShpntPcsTyp; }
            set { sShpntPcsTyp = value; }
        }

        string sShpntMsrTyp = string.Empty;
        public string ShpntMsrTyp
        {
            get { return sShpntMsrTyp; }
            set { sShpntMsrTyp = value; }
        }

        string sShpntWgtTyp = string.Empty;
        public string ShpntWgtTyp
        {
            get { return sShpntWgtTyp; }
            set { sShpntWgtTyp = value; }
        }

        string sPkg = "0";
        public string Pkg
        {
            get { return sPkg; }
            set { sPkg = value; }
        }

        string sRcvdTrWgt = "0";
        public string RcvdTrWgt
        {
            get { return sRcvdTrWgt; }
            set { sRcvdTrWgt = value; }
        }

        string sBlgWdth = "0";
        public string BlgWdth
        {
            get { return sBlgWdth; }
            set { sBlgWdth = value; }
        }

        string sActWdth1 = "0";
        public string ActWdth1
        {
            get { return sActWdth1; }
            set { sActWdth1 = value; }
        }

        string sActWdth2 = "0";
        public string ActWdth2
        {
            get { return sActWdth2; }
            set { sActWdth2 = value; }
        }

        string sActLgth1 = "0";
        public string ActLgth1
        {
            get { return sActLgth1; }
            set { sActLgth1 = value; }
        }

        string sActLgth2 = "0";
        public string ActLgth2
        {
            get { return sActLgth2; }
            set { sActLgth2 = value; }
        }

        string sActIdia1 = "0";
        public string ActIdia1
        {
            get { return sActIdia1; }
            set { sActIdia1 = value; }
        }

        string sActIdia2 = "0";
        public string ActIdia2
        {
            get { return sActIdia2; }
            set { sActIdia2 = value; }
        }

        string sActOdia1 = "0";
        public string ActOdia1
        {
            get { return sActOdia1; }
            set { sActOdia1 = value; }
        }

        string sActOdia2 = "0";
        public string ActOdia2
        {
            get { return sActOdia2; }
            set { sActOdia2 = value; }
        }

        string sActGa1 = "0";
        public string ActGa1
        {
            get { return sActGa1; }
            set { sActGa1 = value; }
        }

        string sActGa2 = "0";
        public string ActGa2
        {
            get { return sActGa2; }
            set { sActGa2 = value; }
        }

        string sActDiag1 = "0";
        public string ActDiag1
        {
            get { return sActDiag1; }
            set { sActDiag1 = value; }
        }

        string sActDiag2 = "0";
        public string ActDiag2
        {
            get { return sActDiag2; }
            set { sActDiag2 = value; }
        }

        string sActSq = "0";
        public string ActSq
        {
            get { return sActSq; }
            set { sActSq = value; }
        }

        string sActFltns1 = "0";
        public string ActFltns1
        {
            get { return sActFltns1; }
            set { sActFltns1 = value; }
        }

        string sActFltns2 = "0";
        public string ActFltns2
        {
            get { return sActFltns2; }
            set { sActFltns2 = value; }
        }

        string sCutNo = "0";
        public string CutNo
        {
            get { return sCutNo; }
            set { sCutNo = value; }
        }

        string sI55Id = "0";
        public string I55Id
        {
            get { return sI55Id; }
            set { sI55Id = value; }
        }

        string sI55Od = "0";
        public string I55Od
        {
            get { return sI55Od; }
            set { sI55Od = value; }
        }

        string sCoilLength = "0";
        public string CoilLength
        {
            get { return sCoilLength; }
            set { sCoilLength = value; }
        }

        string sCoilLengthTyp = string.Empty;
        public string CoilLengthTyp
        {
            get { return sCoilLengthTyp; }
            set { sCoilLengthTyp = value; }
        }

        string sPartCoilIndc = "0";
        public string PartCoilIndc
        {
            get { return sPartCoilIndc; }
            set { sPartCoilIndc = value; }
        }

        string sProdFor = "0";
        public string ProdFor
        {
            get { return sProdFor; }
            set { sProdFor = value; }
        }

        string sPartCusId = "0";
        public string PartCusId
        {
            get { return sPartCusId; }
            set { sPartCusId = value; }
        }

        string sPart = "0";
        public string Part
        {
            get { return sPart; }
            set { sPart = value; }
        }

        string sPartRevNo = "0";
        public string PartRevNo
        {
            get { return sPartRevNo; }
            set { sPartRevNo = value; }
        }

        string sPartAcs = "0";
        public string PartAcs
        {
            get { return sPartAcs; }
            set { sPartAcs = value; }
        }

        string sCertRcvd = "0";
        public string CertRcvd
        {
            get { return sCertRcvd; }
            set { sCertRcvd = value; }
        }

        string sCusTagNo = "";
        public string CusTagNo
        {
            get { return sCusTagNo; }
            set { sCusTagNo = value; }
        }

        string sPrdCst = "0";
        public string PrdCst
        {
            get { return sPrdCst; }
            set { sPrdCst = value; }
        }

        string sClaimNo = "0";
        public string ClaimNo
        {
            get { return sClaimNo; }
            set { sClaimNo = value; }
        }

        string sNcrPfx = string.Empty;
        public string NcrPfx
        {
            get { return sNcrPfx; }
            set { sNcrPfx = value; }
        }

        string sNcrNo = "0";
        public string NcrNo
        {
            get { return sNcrNo; }
            set { sNcrNo = value; }
        }

        string sNcrItm = "0";
        public string NcrItm
        {
            get { return sNcrItm; }
            set { sNcrItm = value; }
        }

        string sNcrSbItm = "0";
        public string NcrSbItm
        {
            get { return sNcrSbItm; }
            set { sNcrSbItm = value; }
        }

        string sNcrLnNo = "0";
        public string NcrLnNo
        {
            get { return sNcrLnNo; }
            set { sNcrLnNo = value; }
        }

        string sShpTagNo = "";
        public string ShpTagNo
        {
            get { return sShpTagNo; }
            set { sShpTagNo = value; }
        }

        string sNewRcptPfx = "";
        public string NewRcptPfx
        {
            get { return sNewRcptPfx; }
            set { sNewRcptPfx = value; }
        }

        string sNewRcptNo = "0";
        public string NewRcptNo
        {
            get { return sNewRcptNo; }
            set { sNewRcptNo = value; }
        }

        string sNewRcptItm = "0";
        public string NewRcptItm
        {
            get { return sNewRcptItm; }
            set { sNewRcptItm = value; }
        }

        string sNewItmCtlNo = "0";
        public string NewItmCtlNo
        {
            get { return sNewItmCtlNo; }
            set { sNewItmCtlNo = value; }
        }

        string sUsbArea5 = "0";
        public string UsbArea5
        {
            get { return sUsbArea5; }
            set { sUsbArea5 = value; }
        }

        string sUsbWdth6 = "0";
        public string UsbWdth6
        {
            get { return sUsbWdth6; }
            set { sUsbWdth6 = value; }
        }

        string sUsbLength6 = "0";
        public string UsbLength6
        {
            get { return sUsbLength6; }
            set { sUsbLength6 = value; }
        }

        string sThpTyID = "ITECH";
        public string ThpTyID
        {
            get { return sThpTyID; }
            set { sThpTyID = value; }
        }

      
        public XCTI55()
        {}


        public DataTable GetDetailItem(int iHeaderId)
        {
            DataTable dtDetails = new DataTable();
            iTECH.Library.DataAccess.MySql.DbHelper oDbHelper = new iTECH.Library.DataAccess.MySql.DbHelper(sMysqlConnectionString, true);
            try
            {
                m_sSql = "SELECT * FROM detailitem WHERE HeaderId = '" + iHeaderId + "'";
                dtDetails = oDbHelper.GetDataTable(m_sSql, CommandType.Text, null);
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLog(ex);
            }
            finally
            {
                oDbHelper.CloseDatabaseConnection();
            }
            return dtDetails;
        }


         public int InsertDetailItems(int iHeaderId, int iDetailId, XCTI55 oXCTI55, iTECH.Library.DataAccess.MySql.DbHelper oDbHelper)
        {
            int iReturn = 0;
            
            m_sbSql = new StringBuilder();
            m_sbSql.Append(" INSERT INTO detailitem ");
            m_sbSql.Append(" (DetailId,HeaderId,CmpyId,IntchgPfx,IntchgNo,IntchgItm,IntchgSitm,Form,Grid,Size,Finish,EfEver, ");
            m_sbSql.Append(" Length,DimDsgn,Idia,GaSize,GaType,RdmDim1,RdmDim2,RdmDim3,RdmDim4,RdmDim5,RdmDim6,RdmDim7,RdmDim8,ImgDocFmt, ");
            m_sbSql.Append(" UsbWdth1,UsbLength1,UsbArea1,UsbWdth2,UsbLength2,UsbArea2,UsbWdth3,UsbLength3,UsbArea3,UsbWdth4, ");
            m_sbSql.Append(" UsbArea4,UsbWdth5,UsbLength5,UsbArea5,UsbWdth6,UsbLength6,UsbArea6,UsbWdth7,UsbLength7,UsbArea7,UsbWdth8, " );
            m_sbSql.Append(" UsbLength8,UsbArea8,UsbWdth9,UsbLength9,UsbArea9,UsbWdth10,UsbLength10,UsbArea10,TagNo,Mill,Heat,MillId,InvtType, ");
            m_sbSql.Append(" RjctRsn,RjctLgnId,RjctRmk,InvtQlty,Loc,InvtPcs,InvtMsr,InvtWgt,InvtPcsTyp,InvtMsrTyp,InvtWgtTyp,ShpntPcs, " );
            m_sbSql.Append(" ShpntMsr,ShpntWgt,ShpntPcsTyp,ShpntMsrTyp,ShpntWgtTyp,Pkg,RcvdTrWgt,BlgWdth,ActWdth1,ActWdth2,ActLgth1,ActLgth2, ");
             m_sbSql.Append(" ActIdia1,ActIdia2,ActOdia1,ActOdia2,ActGa1,ActGa2,ActDiag1,ActDiag2,ActSq,ActFltns1,ActFltns2,CutNo,I55Id,I55Od, ");
             m_sbSql.Append(" CoilLength,CoilLengthTyp,PartCoilIndc,ProdFor,PartCusId,Part,PartRevNo,PartAcs,CertRcvd,CusTagNo,PrdCst,ClaimNo, ");
            m_sbSql.Append(" NcrPfx,NcrNo,NcrItm,NcrSbItm,NcrLnNo,ShpTagNo,NewRcptPfx,NewRcptNo,NewRcptItm,NewItmCtlNo) " );        
            m_sbSql.Append(" VALUES " );
          
            m_sbSql.Append( " ( ");
            m_sbSql.Append("'" + iDetailId + "','" + iHeaderId + "', '" + oXCTI55.CmpyId + "', '" + oXCTI55.IntchgPfx + "','" + oXCTI55.IntchgNo + "','" + oXCTI55.IntchgItm + "','" + oXCTI55.IntchgSitm + "', ");
              m_sbSql.Append("'" + oXCTI55.Form + "', '" + oXCTI55.Grid + "', '" + oXCTI55.Size + "','" + oXCTI55.Finish + "','" + oXCTI55.EfEver + "','" + oXCTI55.Length + "', ");
              m_sbSql.Append("'" + oXCTI55.DimDsgn + "', '" + oXCTI55.Idia + "', '" + oXCTI55.GaSize + "','" + oXCTI55.GaType + "','" + oXCTI55.RdmDim1 + "','" + oXCTI55.RdmDim2 + "', ");
              m_sbSql.Append("'" + oXCTI55.RdmDim3 + "', '" + oXCTI55.RdmDim4 + "', '" + oXCTI55.RdmDim5 + "','" + oXCTI55.RdmDim6 + "','" + oXCTI55.RdmDim7 + "','" + oXCTI55.RdmDim8 + "', ");
              m_sbSql.Append("'" + oXCTI55.ImgDocFmt + "', '" + oXCTI55.UsbWdth1 + "', '" + oXCTI55.UsbLength1 + "','" + oXCTI55.UsbArea1 + "','" + oXCTI55.UsbWdth2 + "','" + oXCTI55.UsbLength2 + "', ");
              m_sbSql.Append("'" + oXCTI55.UsbArea2 + "', '" + oXCTI55.UsbWdth3 + "', '" + oXCTI55.UsbLength3 + "','" + oXCTI55.UsbArea3 + "','" + oXCTI55.UsbWdth4 + "','" + oXCTI55.UsbArea4 + "', ");
              m_sbSql.Append("'" + oXCTI55.UsbWdth5 + "', '" + oXCTI55.UsbLength5 + "', '" + oXCTI55.UsbArea5 + "','" + oXCTI55.UsbWdth6 + "','" + oXCTI55.UsbLength6 + "','" + oXCTI55.UsbArea6 + "', ");
              m_sbSql.Append("'" + oXCTI55.UsbWdth7 + "', '" + oXCTI55.UsbLength7 + "', '" + oXCTI55.UsbArea7 + "','" + oXCTI55.UsbWdth8 + "','" + oXCTI55.UsbLength8 + "','" + oXCTI55.UsbArea8 + "', ");
              m_sbSql.Append("'" + oXCTI55.UsbWdth9 + "', '" + oXCTI55.UsbLength9 + "', '" + oXCTI55.UsbArea9 + "','" + oXCTI55.UsbWdth10 + "','" + oXCTI55.UsbLength10 + "','" + oXCTI55.UsbArea10 + "', ");
              m_sbSql.Append("'" + oXCTI55.TagNo + "', '" + oXCTI55.Mill + "', '" + oXCTI55.Heat + "','" + oXCTI55.MillId + "','" + oXCTI55.InvtType + "','" + oXCTI55.RjctRsn + "', ");
              m_sbSql.Append(" '" + oXCTI55.RjctLgnId + "', '" + oXCTI55.RjctRmk + "','" + oXCTI55.InvtQlty + "','" + oXCTI55.Loc + "','" + oXCTI55.InvtPcs + "', ");
              m_sbSql.Append("'" + oXCTI55.InvtMsr + "', '" + oXCTI55.InvtWgt + "', '" + oXCTI55.InvtPcsTyp + "','" + oXCTI55.InvtMsrTyp + "','" + oXCTI55.InvtWgtTyp + "','" + oXCTI55.ShpntPcs + "', ");
              m_sbSql.Append("'" + oXCTI55.ShpntMsr  + "', '" + oXCTI55.ShpntWgt + "', '" + oXCTI55.ShpntPcsTyp + "','" + oXCTI55.ShpntMsrTyp + "','" + oXCTI55.ShpntWgtTyp + "','" + oXCTI55.Pkg + "', ");
              m_sbSql.Append("'" + oXCTI55.RcvdTrWgt + "', '" + oXCTI55.BlgWdth + "', '" + oXCTI55.ActWdth1 + "','" + oXCTI55.ActWdth2 + "','" + oXCTI55.ActLgth1 + "','" + oXCTI55.ActLgth2 + "', ");
              m_sbSql.Append("'" + oXCTI55.ActIdia1 + "', '" + oXCTI55.ActIdia2 + "', '" + oXCTI55.ActOdia1 + "','" + oXCTI55.ActOdia2 + "','" + oXCTI55.ActGa1 + "','" + oXCTI55.ActGa2 + "', ");
              m_sbSql.Append("'" + oXCTI55.ActDiag1 + "', '" + oXCTI55.ActDiag2 + "', '" + oXCTI55.ActSq + "','" + oXCTI55.ActFltns1 + "','" + oXCTI55.ActFltns2 + "','" + oXCTI55.CutNo + "', ");
              m_sbSql.Append("'" + oXCTI55.I55Id + "', '" + oXCTI55.I55Od + "', '" + oXCTI55.CoilLength + "','" + oXCTI55.CoilLengthTyp + "','" + oXCTI55.PartCoilIndc + "','" + oXCTI55.ProdFor + "', ");
              m_sbSql.Append("'" + oXCTI55.PartCusId + "', '" + oXCTI55.Part + "', '" + oXCTI55.PartRevNo + "','" + oXCTI55.PartAcs + "','" + oXCTI55.CertRcvd + "','" + oXCTI55.CusTagNo + "', ");
              m_sbSql.Append("'" + oXCTI55.PrdCst + "', '" + oXCTI55.ClaimNo + "','" + oXCTI55.NcrPfx + "','" + oXCTI55.NcrNo + "','" + oXCTI55.NcrItm + "', ");
              m_sbSql.Append("'" + oXCTI55.NcrSbItm + "', '" + oXCTI55.NcrLnNo + "','" + oXCTI55.ShpTagNo + "','" + oXCTI55.NewRcptPfx + "','" + oXCTI55.NewRcptNo + "', ");
              m_sbSql.Append("'" + oXCTI55.NewRcptItm + "', '" + oXCTI55.NewItmCtlNo + "'");
            
            m_sbSql.Append(" ) " );

            //iTECH.Library.DataAccess.MySql.DbHelper oDbHelper = new iTECH.Library.DataAccess.MySql.DbHelper(sMysqlConnectionString, true);
            ErrorLog.createLog(m_sbSql.ToString());
            try
            {
                MySqlParameter[] oMySqlParameter = { };
                iReturn = oDbHelper.ExecuteNonQuery(m_sbSql.ToString(), CommandType.Text, oMySqlParameter);
                iReturn = oDbHelper.GetLastInsertID();
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLog(ex);
            }
            finally
            {
                // oDbHelper.CloseDatabaseConnection();
                EdiError.ValidateFormGrdSize(oXCTI55.Form, oXCTI55.Grid, oXCTI55.Size, "INSIDE InsertDetailItems", BusinessUtility.GetString(Globalcl.FileId));
            }
            return iReturn;
        }

         public int InsertDetailItemsInInfomix(XCTI55 oXCTI55, iTECH.Library.DataAccess.ODBC.DbHelper oDbHelper)
        {

             int iStatus = 0;
             m_sbSql = new StringBuilder();
            /// Leave column i55_odia
            // Informix to postgres - replace single quotes ' from oXCTI55.RjctDate to pass null    2021/09/06

            m_sbSql.Append(" insert into XCTI55_rec(i55_cmpy_id,i55_intchg_pfx,i55_intchg_no,i55_intchg_itm,i55_intchg_sbitm,i55_frm,i55_grd,i55_size,i55_fnsh, ");
            m_sbSql.Append(" i55_ef_evar,i55_wdth,i55_lgth,i55_dim_dsgn,i55_idia,i55_odia,i55_ga_size,i55_ga_typ,i55_rdm_dim_1,i55_rdm_dim_2,i55_rdm_dim_3, ");
            m_sbSql.Append(" i55_rdm_dim_4,i55_rdm_dim_5,i55_rdm_dim_6,i55_rdm_dim_7,i55_rdm_dim_8,i55_img_doc_fmt,i55_rdm_img_doc_id,i55_usb_wdth_1, " );
            m_sbSql.Append(" i55_usb_lgth_1,i55_usb_area_1,i55_usb_wdth_2,i55_usb_lgth_2,i55_usb_area_2,i55_usb_wdth_3,i55_usb_lgth_3,i55_usb_area_3,i55_usb_wdth_4, ");
            m_sbSql.Append(" i55_usb_lgth_4,i55_usb_area_4,i55_usb_wdth_5,i55_usb_lgth_5,i55_usb_area_5,i55_usb_wdth_6,i55_usb_lgth_6,i55_usb_area_6,i55_usb_wdth_7, ");
            m_sbSql.Append(" i55_usb_lgth_7,i55_usb_area_7,i55_usb_wdth_8,i55_usb_lgth_8,i55_usb_area_8,i55_usb_wdth_9,i55_usb_lgth_9,i55_usb_area_9,i55_usb_wdth_10, ");
            m_sbSql.Append(" i55_usb_lgth_10,i55_usb_area_10,i55_tag_no,i55_mill,i55_heat,i55_mill_id,i55_invt_typ,i55_rjct_rsn,i55_rjct_dt,i55_rjct_lgn_id,i55_rjct_rmk, ");
            m_sbSql.Append(" i55_invt_qlty,i55_loc,i55_invt_pcs,i55_invt_msr,i55_invt_wgt,i55_invt_pcs_typ,i55_invt_msr_typ,i55_invt_wgt_typ,i55_shpnt_pcs,i55_shpnt_msr, ");
            m_sbSql.Append(" i55_shpnt_wgt,i55_shpnt_pcs_typ,i55_shpnt_msr_typ,i55_shpnt_wgt_typ,i55_pkg,i55_rcvd_tr_wgt,i55_blg_wdth,i55_act_wdth_1,i55_act_wdth_2, ");
            m_sbSql.Append(" i55_act_lgth_1,i55_act_lgth_2,i55_act_idia_1,i55_act_idia_2,i55_act_odia_1,i55_act_odia_2,i55_act_ga_1,i55_act_ga_2,i55_act_diag_1, ");
            m_sbSql.Append(" i55_act_diag_2,i55_act_sq,i55_act_fltns_1,i55_act_fltns_2,i55_cut_no,i55_id,i55_od,i55_coil_lgth,i55_coil_lgth_typ,i55_part_coil_indc, ");
            m_sbSql.Append(" i55_cert_rcvd,i55_cus_tag_no,i55_prd_cst,i55_claim_no,i55_ncr_pfx,i55_ncr_no, ");
            m_sbSql.Append(" i55_ncr_itm,i55_ncr_sbitm,i55_ncr_ln_no,i55_shp_tag_no,i55_new_rcpt_pfx,i55_new_rcpt_no,i55_new_rcpt_itm,i55_new_itm_ctl_no, i55_thpty_id) ");
             
            m_sbSql.Append(" values( ");

            m_sbSql.Append("'" + oXCTI55.CmpyId + "', '" + oXCTI55.IntchgPfx + "','" + oXCTI55.IntchgNo + "','" + oXCTI55.IntchgItm + "','" + oXCTI55.IntchgSitm + "', ");
            m_sbSql.Append("'" + oXCTI55.Form + "', '" + oXCTI55.Grid + "', '" + oXCTI55.Size + "','" + oXCTI55.Finish + "','" + oXCTI55.EfEver + "' ,'" + oXCTI55.Width + "','" + oXCTI55.Length + "', ");
            m_sbSql.Append("'" + oXCTI55.DimDsgn + "', '" + oXCTI55.Idia + "','" + oXCTI55.ActOdia1 + "', '" + oXCTI55.GaSize + "','" + oXCTI55.GaType + "','" + oXCTI55.RdmDim1 + "','" + oXCTI55.RdmDim2 + "', ");
            m_sbSql.Append("'" + oXCTI55.RdmDim3 + "', '" + oXCTI55.RdmDim4 + "', '" + oXCTI55.RdmDim5 + "','" + oXCTI55.RdmDim6 + "','" + oXCTI55.RdmDim7 + "','" + oXCTI55.RdmDim8 + "', ");
            m_sbSql.Append("'" + oXCTI55.ImgDocFmt + "','" + oXCTI55.RdmImgDocId + "',  '" + oXCTI55.UsbWdth1 + "', '" + oXCTI55.UsbLength1 + "','" + oXCTI55.UsbArea1 + "','" + oXCTI55.UsbWdth2 + "','" + oXCTI55.UsbLength2 + "', ");
            m_sbSql.Append("'" + oXCTI55.UsbArea2 + "', '" + oXCTI55.UsbWdth3 + "', '" + oXCTI55.UsbLength3 + "','" + oXCTI55.UsbArea3 + "','" + oXCTI55.UsbWdth4 + "','" + oXCTI55.UsbLength4 + "','" + oXCTI55.UsbArea4 + "', ");
            m_sbSql.Append("'" + oXCTI55.UsbWdth5 + "', '" + oXCTI55.UsbLength5 + "', '" + oXCTI55.UsbArea5 + "','" + oXCTI55.UsbWdth6 + "','" + oXCTI55.UsbLength6 + "','" + oXCTI55.UsbArea6 + "', ");
            m_sbSql.Append("'" + oXCTI55.UsbWdth7 + "', '" + oXCTI55.UsbLength7 + "', '" + oXCTI55.UsbArea7 + "','" + oXCTI55.UsbWdth8 + "','" + oXCTI55.UsbLength8 + "','" + oXCTI55.UsbArea8 + "', ");
            m_sbSql.Append("'" + oXCTI55.UsbWdth9 + "', '" + oXCTI55.UsbLength9 + "', '" + oXCTI55.UsbArea9 + "','" + oXCTI55.UsbWdth10 + "','" + oXCTI55.UsbLength10 + "','" + oXCTI55.UsbArea10 + "', ");
            m_sbSql.Append("'" + oXCTI55.TagNo + "', '" + oXCTI55.Mill + "', '" + oXCTI55.Heat + "','" + oXCTI55.MillId + "','" + oXCTI55.InvtType + "','" + oXCTI55.RjctRsn + "', ");
            m_sbSql.Append("" + oXCTI55.RjctDate + ", '" + oXCTI55.RjctLgnId + "', '" + oXCTI55.RjctRmk + "','" + oXCTI55.InvtQlty + "','" + oXCTI55.Loc + "','" + oXCTI55.InvtPcs + "', ");
            m_sbSql.Append("'" + oXCTI55.InvtMsr + "', '" + oXCTI55.InvtWgt + "', '" + oXCTI55.InvtPcsTyp + "','" + oXCTI55.InvtMsrTyp + "','" + oXCTI55.InvtWgtTyp + "','" + oXCTI55.ShpntPcs + "', ");
            m_sbSql.Append("'" + oXCTI55.ShpntMsr  + "', '" + oXCTI55.ShpntWgt + "', '" + oXCTI55.ShpntPcsTyp + "','" + oXCTI55.ShpntMsrTyp + "','" + oXCTI55.ShpntWgtTyp + "','" + oXCTI55.Pkg + "', ");
            m_sbSql.Append("'" + oXCTI55.RcvdTrWgt + "', '" + oXCTI55.BlgWdth + "', '" + oXCTI55.ActWdth1 + "','" + oXCTI55.ActWdth2 + "','" + oXCTI55.ActLgth1 + "','" + oXCTI55.ActLgth2 + "', ");
            m_sbSql.Append("'" + oXCTI55.ActIdia1 + "', '" + oXCTI55.ActIdia2 + "', '" + oXCTI55.ActOdia1 + "','" + oXCTI55.ActOdia2 + "','" + oXCTI55.ActGa1 + "','" + oXCTI55.ActGa2 + "', ");
            m_sbSql.Append("'" + oXCTI55.ActDiag1 + "', '" + oXCTI55.ActDiag2 + "', '" + oXCTI55.ActSq + "','" + oXCTI55.ActFltns1 + "','" + oXCTI55.ActFltns2 + "','" + oXCTI55.CutNo + "', ");
            m_sbSql.Append("'" + oXCTI55.I55Id + "', '" + oXCTI55.I55Od + "', '" + oXCTI55.CoilLength + "','" + oXCTI55.CoilLengthTyp + "','" + oXCTI55.PartCoilIndc + "', ");
            m_sbSql.Append(" '" + oXCTI55.CertRcvd + "','" + oXCTI55.CusTagNo + "', ");
            m_sbSql.Append("'" + oXCTI55.PrdCst + "', '" + oXCTI55.ClaimNo + "','" + oXCTI55.NcrPfx + "','" + oXCTI55.NcrNo + "','" + oXCTI55.NcrItm + "', ");
            m_sbSql.Append("'" + oXCTI55.NcrSbItm + "', '" + oXCTI55.NcrLnNo + "','" + oXCTI55.ShpTagNo + "','" + oXCTI55.NewRcptPfx + "','" + oXCTI55.NewRcptNo + "', ");
            m_sbSql.Append("'" + oXCTI55.NewRcptItm + "', '" + oXCTI55.NewItmCtlNo + "', '' ");
            
            m_sbSql.Append(" ) " );

            ErrorLog.createLog(BusinessUtility.GetString(m_sbSql.ToString()));

             try
             {
                 iStatus = oDbHelper.ExecuteNonQuery(BusinessUtility.GetString(m_sbSql), CommandType.Text);
             }
             catch(Exception ex)
             {
                 ErrorLog.createLog(ex.ToString());
                ErrorLog.CreateLog(ex);
            }
            finally
            {
                EdiError.ValidateFormGrdSize(oXCTI55.Form, oXCTI55.Grid, oXCTI55.Size, "INSIDE InsertDetailItems", BusinessUtility.GetString(Globalcl.FileId));
            }

            return iStatus;


         }




         public int InsertDetailItemsInInfomixForBailySO(XCTI55 oXCTI55, iTECH.Library.DataAccess.ODBC.DbHelper oDbHelper)
         {

             int iStatus = 0;
             m_sbSql = new StringBuilder();
             /// Leave column i55_odia

             m_sbSql.Append(" insert into XCTI55_rec(i55_cmpy_id,i55_intchg_pfx,i55_intchg_no,i55_intchg_itm,i55_intchg_sbitm,i55_frm,i55_grd,i55_size,i55_fnsh, ");
             m_sbSql.Append(" i55_ef_evar,i55_wdth,i55_lgth,i55_dim_dsgn,i55_idia,i55_odia,i55_ga_size,i55_ga_typ,i55_rdm_dim_1,i55_rdm_dim_2,i55_rdm_dim_3, ");
             m_sbSql.Append(" i55_rdm_dim_4,i55_rdm_dim_5,i55_rdm_dim_6,i55_rdm_dim_7,i55_rdm_dim_8,i55_img_doc_fmt,i55_rdm_img_doc_id,i55_usb_wdth_1, ");
             m_sbSql.Append(" i55_usb_lgth_1,i55_usb_area_1,i55_usb_wdth_2,i55_usb_lgth_2,i55_usb_area_2,i55_usb_wdth_3,i55_usb_lgth_3,i55_usb_area_3,i55_usb_wdth_4, ");
             m_sbSql.Append(" i55_usb_lgth_4,i55_usb_area_4,i55_usb_wdth_5,i55_usb_lgth_5,i55_usb_area_5,i55_usb_wdth_6,i55_usb_lgth_6,i55_usb_area_6,i55_usb_wdth_7, ");
             m_sbSql.Append(" i55_usb_lgth_7,i55_usb_area_7,i55_usb_wdth_8,i55_usb_lgth_8,i55_usb_area_8,i55_usb_wdth_9,i55_usb_lgth_9,i55_usb_area_9,i55_usb_wdth_10, ");
             m_sbSql.Append(" i55_usb_lgth_10,i55_usb_area_10,i55_tag_no,i55_mill,i55_heat,i55_mill_id,i55_invt_typ,i55_rjct_rsn,i55_rjct_dt,i55_rjct_lgn_id,i55_rjct_rmk, ");
             m_sbSql.Append(" i55_invt_qlty,i55_loc,i55_invt_pcs,i55_invt_msr,i55_invt_wgt,i55_invt_pcs_typ,i55_invt_msr_typ,i55_invt_wgt_typ,i55_shpnt_pcs,i55_shpnt_msr, ");
             m_sbSql.Append(" i55_shpnt_wgt,i55_shpnt_pcs_typ,i55_shpnt_msr_typ,i55_shpnt_wgt_typ,i55_pkg,i55_rcvd_tr_wgt,i55_blg_wdth,i55_act_wdth_1,i55_act_wdth_2, ");
             m_sbSql.Append(" i55_act_lgth_1,i55_act_lgth_2,i55_act_idia_1,i55_act_idia_2,i55_act_odia_1,i55_act_odia_2,i55_act_ga_1,i55_act_ga_2,i55_act_diag_1, ");
             m_sbSql.Append(" i55_act_diag_2,i55_act_sq,i55_act_fltns_1,i55_act_fltns_2,i55_cut_no,i55_id,i55_od,i55_coil_lgth,i55_coil_lgth_typ,i55_part_coil_indc,i55_prod_for, ");
             m_sbSql.Append(" i55_part_cus_id,i55_part,i55_part_revno,i55_part_acs,i55_cert_rcvd,i55_cus_tag_no,i55_prd_cst,i55_claim_no,i55_ncr_pfx,i55_ncr_no, ");
             m_sbSql.Append(" i55_ncr_itm,i55_ncr_sbitm,i55_ncr_ln_no,i55_shp_tag_no,i55_new_rcpt_pfx,i55_new_rcpt_no,i55_new_rcpt_itm,i55_new_itm_ctl_no, i55_thpty_id) ");

             m_sbSql.Append(" values( ");

             m_sbSql.Append("'" + oXCTI55.CmpyId + "', '" + oXCTI55.IntchgPfx + "','" + oXCTI55.IntchgNo + "','" + oXCTI55.IntchgItm + "','" + oXCTI55.IntchgSitm + "', ");
             m_sbSql.Append("'" + oXCTI55.Form + "', '" + oXCTI55.Grid + "', '" + oXCTI55.Size + "','" + oXCTI55.Finish + "','" + oXCTI55.EfEver + "' ,'" + oXCTI55.Width + "','" + oXCTI55.Length + "', ");
             m_sbSql.Append("'" + oXCTI55.DimDsgn + "', '" + oXCTI55.Idia + "','" + oXCTI55.ActOdia1 + "', '" + oXCTI55.GaSize + "','" + oXCTI55.GaType + "','" + oXCTI55.RdmDim1 + "','" + oXCTI55.RdmDim2 + "', ");
             m_sbSql.Append("'" + oXCTI55.RdmDim3 + "', '" + oXCTI55.RdmDim4 + "', '" + oXCTI55.RdmDim5 + "','" + oXCTI55.RdmDim6 + "','" + oXCTI55.RdmDim7 + "','" + oXCTI55.RdmDim8 + "', ");
             m_sbSql.Append("'" + oXCTI55.ImgDocFmt + "','" + oXCTI55.RdmImgDocId + "',  '" + oXCTI55.UsbWdth1 + "', '" + oXCTI55.UsbLength1 + "','" + oXCTI55.UsbArea1 + "','" + oXCTI55.UsbWdth2 + "','" + oXCTI55.UsbLength2 + "', ");
             m_sbSql.Append("'" + oXCTI55.UsbArea2 + "', '" + oXCTI55.UsbWdth3 + "', '" + oXCTI55.UsbLength3 + "','" + oXCTI55.UsbArea3 + "','" + oXCTI55.UsbWdth4 + "','" + oXCTI55.UsbLength4 + "','" + oXCTI55.UsbArea4 + "', ");
             m_sbSql.Append("'" + oXCTI55.UsbWdth5 + "', '" + oXCTI55.UsbLength5 + "', '" + oXCTI55.UsbArea5 + "','" + oXCTI55.UsbWdth6 + "','" + oXCTI55.UsbLength6 + "','" + oXCTI55.UsbArea6 + "', ");
             m_sbSql.Append("'" + oXCTI55.UsbWdth7 + "', '" + oXCTI55.UsbLength7 + "', '" + oXCTI55.UsbArea7 + "','" + oXCTI55.UsbWdth8 + "','" + oXCTI55.UsbLength8 + "','" + oXCTI55.UsbArea8 + "', ");
             m_sbSql.Append("'" + oXCTI55.UsbWdth9 + "', '" + oXCTI55.UsbLength9 + "', '" + oXCTI55.UsbArea9 + "','" + oXCTI55.UsbWdth10 + "','" + oXCTI55.UsbLength10 + "','" + oXCTI55.UsbArea10 + "', ");
             m_sbSql.Append("'" + oXCTI55.TagNo + "', '" + oXCTI55.Mill + "', '" + oXCTI55.Heat + "','" + oXCTI55.MillId + "','" + oXCTI55.InvtType + "','" + oXCTI55.RjctRsn + "', ");
             m_sbSql.Append("'" + oXCTI55.RjctDate + "', '" + oXCTI55.RjctLgnId + "', '" + oXCTI55.RjctRmk + "','" + oXCTI55.InvtQlty + "','" + oXCTI55.Loc + "','" + oXCTI55.InvtPcs + "', ");
             m_sbSql.Append("'" + oXCTI55.InvtMsr + "', '" + oXCTI55.InvtWgt + "', '" + oXCTI55.InvtPcsTyp + "','" + oXCTI55.InvtMsrTyp + "','" + oXCTI55.InvtWgtTyp + "','" + oXCTI55.ShpntPcs + "', ");
             m_sbSql.Append("'" + oXCTI55.ShpntMsr + "', '" + oXCTI55.ShpntWgt + "', '" + oXCTI55.ShpntPcsTyp + "','" + oXCTI55.ShpntMsrTyp + "','" + oXCTI55.ShpntWgtTyp + "','" + oXCTI55.Pkg + "', ");
             m_sbSql.Append("'" + oXCTI55.RcvdTrWgt + "', '" + oXCTI55.BlgWdth + "', '" + oXCTI55.ActWdth1 + "','" + oXCTI55.ActWdth2 + "','" + oXCTI55.ActLgth1 + "','" + oXCTI55.ActLgth2 + "', ");
             m_sbSql.Append("'" + oXCTI55.ActIdia1 + "', '" + oXCTI55.ActIdia2 + "', '" + oXCTI55.ActOdia1 + "','" + oXCTI55.ActOdia2 + "','" + oXCTI55.ActGa1 + "','" + oXCTI55.ActGa2 + "', ");
             m_sbSql.Append("'" + oXCTI55.ActDiag1 + "', '" + oXCTI55.ActDiag2 + "', '" + oXCTI55.ActSq + "','" + oXCTI55.ActFltns1 + "','" + oXCTI55.ActFltns2 + "','" + oXCTI55.CutNo + "', ");
             m_sbSql.Append("'" + oXCTI55.I55Id + "', '" + oXCTI55.I55Od + "', '" + oXCTI55.CoilLength + "','" + oXCTI55.CoilLengthTyp + "','" + oXCTI55.PartCoilIndc + "','" + oXCTI55.ProdFor + "', ");
             m_sbSql.Append("'" + oXCTI55.PartCusId + "', '" + oXCTI55.Part + "', '" + oXCTI55.PartRevNo + "','" + oXCTI55.PartAcs + "','" + oXCTI55.CertRcvd + "','" + oXCTI55.CusTagNo + "', ");
             m_sbSql.Append("'" + oXCTI55.PrdCst + "', '" + oXCTI55.ClaimNo + "','" + oXCTI55.NcrPfx + "','" + oXCTI55.NcrNo + "','" + oXCTI55.NcrItm + "', ");
             m_sbSql.Append("'" + oXCTI55.NcrSbItm + "', '" + oXCTI55.NcrLnNo + "','" + oXCTI55.ShpTagNo + "','" + oXCTI55.NewRcptPfx + "','" + oXCTI55.NewRcptNo + "', ");
             m_sbSql.Append("'" + oXCTI55.NewRcptItm + "', '" + oXCTI55.NewItmCtlNo + "', '" + oXCTI55.ThpTyID + "'");

             m_sbSql.Append(" ) ");

             ErrorLog.createLog(BusinessUtility.GetString(m_sbSql.ToString()));

             try
             {
                 iStatus = oDbHelper.ExecuteNonQuery(BusinessUtility.GetString(m_sbSql), CommandType.Text);
             }
             catch (Exception ex)
             {
                 ErrorLog.createLog(ex.ToString());
                ErrorLog.CreateLog(ex);
            }
            finally
            {
                EdiError.ValidateFormGrdSize(oXCTI55.Form, oXCTI55.Grid, oXCTI55.Size, "INSIDE InsertDetailItemsInInfomixForBailySO", BusinessUtility.GetString(Globalcl.FileId));
            }

            return iStatus;


         }


        public DataTable GetDetailItem(int iHeaderId, int iDetailid = 0)
        {
            DataTable dtDetails = new DataTable();
            iTECH.Library.DataAccess.MySql.DbHelper oDbHelper = new iTECH.Library.DataAccess.MySql.DbHelper(sMysqlConnectionString, true);
            try
            {
                if (iDetailid > 0)
                {
                    m_sSql = "SELECT * FROM detailitem WHERE HeaderId = '" + iHeaderId + "' and DetailId = '" + iDetailid + "' AND Active = 1";
                }
                else
                {
                    m_sSql = "SELECT * FROM detailitem WHERE HeaderId = '" + iHeaderId + "' AND Active = 1";
                }
                ErrorLog.createLog(m_sSql);
                dtDetails = oDbHelper.GetDataTable(m_sSql, CommandType.Text, null);
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLog(ex);
            }
            finally
            {
                oDbHelper.CloseDatabaseConnection();
            }
            return dtDetails;
        }







    }
}
