﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using iTECH.Library.DataAccess.ODBC;
using iTECH.Library.Utilities;
using System.Data;
using MySql.Data.MySqlClient;
using ChoETL;

namespace SecureFTP
{
    class CustomerOwnReceipt
    {

        StringBuilder sbSql = null;
        string m_sSql = string.Empty;
        static string sStratixConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ToString();
        static string sMysqlConnectionString = ConfigurationManager.ConnectionStrings["NewConnectionString"].ToString();

        private static string sXctiUserId = ConfigurationManager.AppSettings["xctiUserId"];
        private static string EdiFile = ConfigurationManager.AppSettings["EdiFile"];
        private static int m_iMaxInerchangeNo = BusinessUtility.GetInt(ConfigurationManager.AppSettings["MaxInerchangeNo"]);


        static string m_sCompanyId = ConfigurationManager.AppSettings["CompanyId"];
        static string m_sIntchgPfx = ConfigurationManager.AppSettings["IntchgPfx"];
        static string m_sCusVenId = ConfigurationManager.AppSettings["CusVenId"];
        static string m_sRcptType = ConfigurationManager.AppSettings["RcptType"];

        Configuration m_oConfiguration = new Configuration();

        int m_iHeaderId = 0;
        int m_iFileId = 0;
        int m_iDetailId = 0;

        string sCusVenId = string.Empty;
        public string CusVenId
        {
            get { return sCusVenId; }
            set { sCusVenId = value; }
        }


        string sShipTo = string.Empty;
        public string ShipTo
        {
            get { return sShipTo; }
            set { sShipTo = value; }
        }

        string sReceiptDate = string.Empty;
        public string ReceiptDate
        {
            get { return sReceiptDate; }
            set { sReceiptDate = value; }
        }

        string sVenShpRef = string.Empty;
        public string VenShpRef
        {
            get { return sVenShpRef; }
            set { sVenShpRef = value; }
        }

        string sCarrierVehicleDesc = string.Empty;
        public string CarrierVehicleDesc
        {
            get { return sCarrierVehicleDesc; }
            set { sCarrierVehicleDesc = value; }
        }

        string sRcvWhs = string.Empty;
        public string RcvWhs
        {
            get { return sRcvWhs; }
            set { sRcvWhs = value; }
        }

        string sDeliveryMethod = string.Empty;
        public string DeliveryMethod
        {
            get { return sDeliveryMethod; }
            set { sDeliveryMethod = value; }
        }

        string sFreightVenId = string.Empty;
        public string FreightVenId
        {
            get { return sFreightVenId; }
            set { sFreightVenId = value; }
        }

        string sCarrierName = string.Empty;
        public string CarrierName
        {
            get { return sCarrierName; }
            set { sCarrierName = value; }
        }

        string sCarrierRefNo = string.Empty;
        public string CarrierRefNo
        {
            get { return sCarrierRefNo; }
            set { sCarrierRefNo = value; }
        }

        string sPreRcptStatus = "I";
        public string PreRcptStatus
        {
            get { return sPreRcptStatus; }
            set { sPreRcptStatus = value; }
        }

        int iInterChangeNo = 0;
        public int InterChangeNo
        {
            get { return iInterChangeNo; }
            set { iInterChangeNo = value; }
        }

        string sCompanyId = string.Empty;
        public string CompanyId
        {
            get { return sCompanyId; }
            set { sCompanyId = value; }
        }

        string sIntchgPfx = string.Empty;
        public string IntchgPfx
        {
            get { return sIntchgPfx; }
            set { sIntchgPfx = value; }
        }

        string sRcptType = string.Empty;
        public string RcptType
        {
            get { return sRcptType; }
            set { sRcptType = value; }
        }

        string sNewRcptPfx = string.Empty;
        public string NewRcptPfx
        {
            get { return sNewRcptPfx; }
            set { sNewRcptPfx = value; }
        }

        string sNewRcptNo = "0";
        public string NewRcptNo
        {
            get { return sNewRcptNo; }
            set { sNewRcptNo = value; }
        }

        string sNcrPfx = string.Empty;
        public string NcrPfx
        {
            get { return sNcrPfx; }
            set { sNcrPfx = value; }
        }

        string sNcrNo = "0";
        public string NcrNo
        {
            get { return sNcrNo; }
            set { sNcrNo = value; }
        }

        string sFileNo = string.Empty;
        public string FileNo
        {
            get { return sFileNo; }
            set { sFileNo = value; }
        }

        string sCusVenShp = string.Empty;
        public string CusVenShp
        {
            get { return sCusVenShp; }
            set { sCusVenShp = value; }
        }

        string sIntchgItm = "0";
        public string IntchgItm
        {
            get { return sIntchgItm; }
            set { sIntchgItm = value; }
        }

        string sPrntPfx = "SO";
        public string PrntPfx
        {
            get { return sPrntPfx; }
            set { sPrntPfx = value; }
        }

        string sPO = string.Empty;
        public string PO
        {
            get { return sPO; }
            set { sPO = value; }
        }

        string sForm = string.Empty;
        public string Form
        {
            get { return sForm; }
            set { sForm = value; }
        }

        string sGrid = string.Empty;
        public string Grid
        {
            get { return sGrid; }
            set { sGrid = value; }
        }

        string sSize = string.Empty;
        public string Size
        {
            get { return sSize; }
            set { sSize = value; }
        }

        string sFinish = string.Empty;
        public string Finish
        {
            get { return sFinish; }
            set { sFinish = value; }
        }

        string sFileType = string.Empty;
        public string FileType
        {
            get { return sFileType; }
            set { sFileType = value; }
        }



        public CustomerOwnReceipt()
        {
            ErrorLog.createLog("CustomerOwnReceipt constructor");
        }

        public bool CheckConnection()
        {
            bool bReturn = false;
            DbHelper oDbHelper = new DbHelper(sStratixConnectionString, true);
            m_sSql = "SELECT (MAX(i53_intchg_no) + 1) AS i00_intchg_no   FROM xcti53_rec";

            try
            {
                object objIntChgNo = oDbHelper.GetValue(m_sSql, CommandType.Text);
                iInterChangeNo = BusinessUtility.GetInt(objIntChgNo);
                bReturn = iInterChangeNo > 0;
                ErrorLog.createLog("Connection successful");
            }
            catch (Exception ex)
            {
                ErrorLog.createLog("Error in Check connection : " + ex.ToString());
                // EdiError.LogError(EdiFile, m_iFileId.ToString(), "", ex, "");
            }

            return bReturn;
        }

        public CustomerOwnReceipt(int iHeaderId)
        {
            m_iHeaderId = iHeaderId;
            iTECH.Library.DataAccess.MySql.DbHelper oDbHelper = new iTECH.Library.DataAccess.MySql.DbHelper(sMysqlConnectionString, true);
            sbSql = new StringBuilder();
            sbSql.Append("select * from header where HeaderId = '" + iHeaderId + "'");
            MySqlParameter[] oMySqlParameter = { iTECH.Library.DataAccess.MySql.DbUtility.GetParameter("HeaderId", iHeaderId, iTECH.Library.DataAccess.MySql.MyDbType.Int), };

            DataTable oDtAddress = new iTECH.Library.DataAccess.MySql.DbHelper(sMysqlConnectionString, true).GetDataTable(sbSql.ToString(), CommandType.Text, oMySqlParameter);
            foreach (DataRow oDataRow in oDtAddress.Rows)
            {
                // HeaderId, FileId, CusVenId, ShipTo, ReceiptDate, VenShpRef, CarrierVehicleDesc, RcvWhs, DeliveryMethod, FreightVenId, CarrierName, CarrierRefNo, PreRcptStatus
                sFileNo = oDataRow["FileId"].ToString();
                m_iFileId = BusinessUtility.GetInt(sFileNo);
                sCusVenId = oDataRow["CusVenId"].ToString();
                sShipTo = oDataRow["ShipTo"].ToString();
                sReceiptDate = oDataRow["ReceiptDate"].ToString();
                sVenShpRef = oDataRow["VenShpRef"].ToString();
                sCarrierVehicleDesc = oDataRow["CarrierVehicleDesc"].ToString();
                sRcvWhs = oDataRow["RcvWhs"].ToString();
                sDeliveryMethod = oDataRow["DeliveryMethod"].ToString();
                sFreightVenId = oDataRow["FreightVenId"].ToString();
                sCarrierName = oDataRow["CarrierName"].ToString();
                sCarrierRefNo = oDataRow["CarrierRefNo"].ToString();
                sPreRcptStatus = oDataRow["PreRcptStatus"].ToString();
            }

        }

        public void ProcessReceipt(int iFileId)
        {
            try
            {
                Header oHeader = new Header();
                DataTable dtHeader = oHeader.GetHeaderList(iFileId);
                for (int i = 0; i < dtHeader.Rows.Count; i++)
                {
                    oHeader.ReceiptCreated = BusinessUtility.GetBool(dtHeader.Rows[i]["ReceiptCreated"]);
                    oHeader.HeaderId = BusinessUtility.GetString(dtHeader.Rows[i]["HeaderId"]);
                    oHeader.FileId = BusinessUtility.GetString(dtHeader.Rows[i]["FileId"]);

                    m_iHeaderId = BusinessUtility.GetInt(oHeader.HeaderId);

                    if (m_iHeaderId > 0)
                    {
                        STX856 oSTX856 = new STX856(m_iHeaderId);
                        //oSTX856.CompanyId = m_sCompanyId;
                        //oSTX856.IntchgPfx = m_sIntchgPfx;
                        //oSTX856.InterChangeNo = 0;
                        //oSTX856.RcptType = m_sRcptType;
                        //oSTX856.CusVenId = m_sCusVenId;
                        //oSTX856.CusVenShp = "0";    // poh_shp_fm


                        //this.StartInsert(ref oConfiguration);

                        // ErrorLog.createLog("m_sShipFrom = " + m_sShipFrom);

                        // oHeader.UpdateHeader(m_iHeaderId, BusinessUtility.GetInt(Configuration.EdiFileProcessingType.ONERECEIPTPERPO));

                        m_iHeaderId = 0;
                    }
                }
            }
            catch (Exception ex)
            {
            }
            finally
            {

            }


        }


        /// <summary>
        /// only for customerown receipt
        /// </summary>
        /// <param name="oConfiguration"></param>
        /// <param name="oShipment"></param>
        public void StartInsert(ref Configuration oConfiguration, Shipment oShipment)
        {
            Header oheader = new Header();
            Details oDetails = new Details();
            DataTable dtHeader = oheader.GetHeaderList(m_iFileId);
            DataTable dtDetails = oDetails.GetCustomerOwnDetails(m_iHeaderId);
            XCTI55 oXCTI55 = new XCTI55();
            DataTable dtDetailItem = oXCTI55.GetDetailItem(m_iHeaderId);
            ErrorLog.createLog("Detail  count : " + dtDetails.Rows.Count);
            ErrorLog.createLog("Detail Item count : " + dtDetailItem.Rows.Count);

            if (dtDetails.Rows.Count > 0)
            {

                Files oFiles = new Files();
                //oFiles.UpdatePoBsnForCurrentFile(m_iFileId);

                DbHelper oDbHelper = new DbHelper(sStratixConnectionString, true);
                //m_sSql = "SELECT (MAX(i53_intchg_no) + 1) AS i00_intchg_no   FROM xcti53_rec";

                //try
                //{
                //    object objIntChgNo = oDbHelper.GetValue(m_sSql, CommandType.Text);
                //    iInterChangeNo = BusinessUtility.GetInt(objIntChgNo);
                //}
                //catch (Exception ex)
                //{
                //    ErrorLog.createLog(ex.ToString());
                //    EdiError.LogError(EdiFile, m_iFileId.ToString(), "", ex, "");
                //}

                iInterChangeNo = m_iMaxInerchangeNo + m_iFileId + m_iHeaderId;  // new logic applied due to error "Could not do a physical-order read to fetch next row"
                ErrorLog.createLog("Interchange no created for file id " + m_iFileId + ", HeaderId = '" + m_iHeaderId + "'   :    " + iInterChangeNo);

                ErrorLog.createLog("Stratix Interchange created..");
                Console.WriteLine("Stratix Interchange created..");

                sCompanyId = Utility.GetConfigValue("CompanyId");
                m_sCompanyId = sCompanyId;
                m_sIntchgPfx = Utility.GetConfigValue("IntchgPfx");
                sRcvWhs = Utility.GetConfigValue("RcvWhs");
                sVenShpRef = oShipment.CusVenShp;
                sCusVenShp = "0";

                //sCusVenId = Utility.GetConfigValue("CustomerOwnCusVenID");    2020/05/15  picking from header table
                sCusVenId = Utility.GetConfigValue("CustomerOwnCusVenID");

                sbSql = new StringBuilder();

                sbSql.Append(" insert into xcti53_rec (i53_cmpy_id, i53_intchg_pfx, i53_intchg_no, i53_rcpt_typ, i53_cus_ven_id, i53_cus_ven_shp, i53_rcpt_dt,");
                sbSql.Append(" i53_ven_shp_ref, i53_crr_vcl_desc, i53_rcvg_whs, i53_dlvy_mthd, i53_frt_ven_id, i53_crr_nm, i53_crr_ref_no, i53_prcpt_sts, ");
                sbSql.Append(" i53_new_rcpt_pfx, i53_new_rcpt_no, i53_ncr_pfx, i53_ncr_no) ");
                sbSql.Append(" VALUES ('" + sCompanyId + "', '" + m_sIntchgPfx + "', '" + iInterChangeNo + "', '" + m_sRcptType + "', '" + sCusVenId + "',");
                sbSql.Append(" '" + sCusVenShp + "', current, '" + sVenShpRef + "', '" + sCarrierVehicleDesc + "', '" + sRcvWhs + "', '" + sDeliveryMethod + "',");
                sbSql.Append(" '" + sFreightVenId + "', '" + sCarrierName + "', '" + sCarrierRefNo + "','" + sPreRcptStatus + "', '" + sNewRcptPfx + "', '" + sNewRcptNo + "', '" + sNcrPfx + "','" + sNcrNo + "')");

                ErrorLog.createLog(BusinessUtility.GetString(sbSql));
                ErrorLog.createLog("sPreRcptStatus = " + sPreRcptStatus);

                int iStatus = 0;
                try
                {

                    iStatus = oDbHelper.ExecuteNonQuery(BusinessUtility.GetString(sbSql), CommandType.Text);
                    if (iStatus > 0)
                    {
                        ErrorLog.createLog("Stratix Header created..");
                        Console.WriteLine("Stratix Header created..");
                        iStatus = 0;

                        if (dtDetails.Rows.Count > 0)
                        {
                            for (int i = 0; i < dtDetails.Rows.Count; i++)
                            {
                                oDetails.PrntNo = BusinessUtility.GetString(dtDetails.Rows[i]["PrntNo"]);
                                oDetails.PrntItm = BusinessUtility.GetString(dtDetails.Rows[i]["PrntItm"]);
                                oDetails.PrntSubItm = BusinessUtility.GetString(dtDetails.Rows[i]["PrntSubItm"]);
                                oDetails.CusRtnMthd = BusinessUtility.GetString(dtDetails.Rows[i]["CusRtnMthd"]);
                                oDetails.ShptNo = BusinessUtility.GetString(dtDetails.Rows[i]["ShptNo"]);
                                oDetails.ShptTagNo = BusinessUtility.GetString(dtDetails.Rows[i]["ShptTagNo"]);
                                oDetails.VenShptRef = BusinessUtility.GetString(dtDetails.Rows[i]["VenShptRef"]);
                                oDetails.OrigShpntPcs = BusinessUtility.GetString(dtDetails.Rows[i]["OrigShpntPcs"]);
                                oDetails.OrigShpntMsr = BusinessUtility.GetString(dtDetails.Rows[i]["OrigShpntMsr"]);
                                oDetails.OrigShptWgt = BusinessUtility.GetString(dtDetails.Rows[i]["OrigShptWgt"]);
                                oDetails.ShpGrsPcs = BusinessUtility.GetString(dtDetails.Rows[i]["ShpGrsPcs"]);
                                oDetails.ShpGrsMsr = BusinessUtility.GetString(dtDetails.Rows[i]["ShpGrsMsr"]);
                                oDetails.ShpGrsWgt = BusinessUtility.GetString(dtDetails.Rows[i]["ShpGrsWgt"]);
                                oDetails.Form = BusinessUtility.GetString(dtDetails.Rows[i]["Form"]);
                                oDetails.Grade = BusinessUtility.GetString(dtDetails.Rows[i]["Grade"]);
                                oDetails.Size = BusinessUtility.GetString(dtDetails.Rows[i]["Size"]);
                                oDetails.Finish = BusinessUtility.GetString(dtDetails.Rows[i]["Finish"]);
                                oDetails.ExtendedFinish = BusinessUtility.GetString(dtDetails.Rows[i]["ExtendedFinish"]);
                                oDetails.Width = BusinessUtility.GetString(dtDetails.Rows[i]["Width"]);
                                oDetails.Length = BusinessUtility.GetString(dtDetails.Rows[i]["Length"]);
                                oDetails.DimDsgn = BusinessUtility.GetString(dtDetails.Rows[i]["DimDsgn"]);
                                oDetails.InnerDia = BusinessUtility.GetString(dtDetails.Rows[i]["InnerDia"]);
                                oDetails.OuterDia = BusinessUtility.GetString(dtDetails.Rows[i]["OuterDia"]);
                                oDetails.WallThck = BusinessUtility.GetString(dtDetails.Rows[i]["WallThck"]);
                                oDetails.GaSize = BusinessUtility.GetString(dtDetails.Rows[i]["GaSize"]);
                                oDetails.GaType = BusinessUtility.GetString(dtDetails.Rows[i]["GaType"]);
                                oDetails.RndmDim1 = BusinessUtility.GetString(dtDetails.Rows[i]["RndmDim1"]);
                                oDetails.RndmDim2 = BusinessUtility.GetString(dtDetails.Rows[i]["RndmDim2"]);
                                oDetails.RndmDim3 = BusinessUtility.GetString(dtDetails.Rows[i]["RndmDim3"]);
                                oDetails.RndmDim4 = BusinessUtility.GetString(dtDetails.Rows[i]["RndmDim4"]);
                                oDetails.RndmDim5 = BusinessUtility.GetString(dtDetails.Rows[i]["RndmDim5"]);
                                oDetails.RndmDim6 = BusinessUtility.GetString(dtDetails.Rows[i]["RndmDim6"]);
                                oDetails.RndmDim7 = BusinessUtility.GetString(dtDetails.Rows[i]["RndmDim7"]);
                                oDetails.RndmDim8 = BusinessUtility.GetString(dtDetails.Rows[i]["RndmDim8"]);
                                oDetails.OfcRvw = BusinessUtility.GetString(dtDetails.Rows[i]["OfcRvw"]);
                                oDetails.SlsCat = BusinessUtility.GetString(dtDetails.Rows[i]["SlsCat"]);
                                oDetails.IntchgItm = BusinessUtility.GetString(dtDetails.Rows[i]["IntchgItm"]);
                                oDetails.i54_bgt_as_part = BusinessUtility.GetString(dtDetails.Rows[i]["i54_bgt_as_part"]);

                                sbSql = new StringBuilder();
                                sbSql.Append(" insert into XCTI54_rec (i54_cmpy_id, i54_intchg_pfx, i54_intchg_no, i54_intchg_itm,i54_prnt_pfx, i54_prnt_no,i54_prnt_itm, ");
                                sbSql.Append(" i54_prnt_sitm, i54_cus_rtn_mthd, i54_shpt_no, i54_shp_tag_no, i54_ven_shp_ref, i54_orig_shpnt_pcs,i54_orig_shpnt_msr, ");
                                sbSql.Append(" i54_orig_shpnt_wgt, i54_shp_grs_pcs, i54_shp_grs_msr, i54_shp_grs_wgt,  i54_frm , i54_grd, i54_size, ");
                                sbSql.Append(" i54_fnsh, i54_ef_evar , i54_wdth,  i54_lgth , i54_dim_dsgn ,  i54_idia , i54_odia , i54_wthck, i54_ga_size, i54_ga_typ,  i54_rdm_dim_1 , i54_rdm_dim_2, ");
                                sbSql.Append(" i54_rdm_dim_3, i54_rdm_dim_4 , i54_rdm_dim_5 ,  i54_rdm_dim_6 , i54_rdm_dim_7 , i54_rdm_dim_8 , i54_ofc_rvw , i54_new_rcpt_pfx, i54_new_rcpt_no, ");
                                sbSql.Append(" i54_new_rcpt_itm, i54_sls_cat , i54_ncr_pfx, i54_ncr_no, i54_ncr_itm, i54_bgt_as_part, i54_bgt_as)  ");
                                sbSql.Append(" values('" + m_sCompanyId + "', '" + m_sIntchgPfx + "', '" + iInterChangeNo + "', '" + oDetails.IntchgItm + "','" + sPrntPfx + "', '" + oDetails.PrntNo + "','" + oDetails.PrntItm + "', ");
                                sbSql.Append(" " + oDetails.PrntSubItm + ", '" + oDetails.CusRtnMthd + "', '" + oDetails.ShptNo + "', '', '" + oDetails.VenShptRef + "', '" + oDetails.OrigShpntPcs + "','" + oDetails.OrigShpntMsr + "', ");
                                sbSql.Append(" '" + oDetails.OrigShptWgt + "', '" + oDetails.ShpGrsPcs + "', '" + oDetails.ShpGrsMsr + "', '" + oDetails.ShpGrsWgt + "',  '" + oDetails.Form + "' , '" + oDetails.Grade + "', '" + oDetails.Size + "', ");
                                sbSql.Append(" '" + oDetails.Finish + "', '" + oDetails.ExtendedFinish + "' , '" + oDetails.Width + "',  '" + oDetails.Length + "' , '" + oDetails.DimDsgn + "' ,  '" + oDetails.InnerDia + "' , '" + oDetails.OuterDia + "' , '" + oDetails.WallThck + "', '" + oDetails.GaSize + "', '" + oDetails.GaType + "',  '" + oDetails.RndmDim1 + "' , '" + oDetails.RndmDim2 + "',");
                                sbSql.Append(" '" + oDetails.RndmDim3 + "', '" + oDetails.RndmDim4 + "' , '" + oDetails.RndmDim5 + "' ,  '" + oDetails.RndmDim6 + "' , '" + oDetails.RndmDim7 + "' , '" + oDetails.RndmDim8 + "' , '" + oDetails.OfcRvw + "' , '0', '0',  ");
                                sbSql.Append(" '0', '0' , '0', '0', '0', '" + oDetails.i54_bgt_as_part + "', '1') ");

                                ErrorLog.createLog(BusinessUtility.GetString(sbSql));
                                iStatus = oDbHelper.ExecuteNonQuery(BusinessUtility.GetString(sbSql), CommandType.Text);

                                if (iStatus > 0)
                                {
                                    ErrorLog.createLog("Stratix Detais created..");
                                    Console.WriteLine("Stratix Detais created..");
                                }

                            }
                            if (dtDetailItem.Rows.Count > 0)
                            {
                                for (int i = 0; i < dtDetailItem.Rows.Count; i++)
                                {
                                    oXCTI55.InvtWgt = BusinessUtility.GetString(dtDetailItem.Rows[i]["InvtWgt"]);
                                    oXCTI55.InvtPcs = BusinessUtility.GetString(dtDetailItem.Rows[i]["InvtPcs"]);
                                    oXCTI55.InvtMsr = BusinessUtility.GetString(dtDetailItem.Rows[i]["InvtMsr"]);
                                    oXCTI55.CmpyId = m_sCompanyId;
                                    oXCTI55.IntchgPfx = m_sIntchgPfx;
                                    oXCTI55.IntchgNo = iInterChangeNo.ToString();
                                    oXCTI55.IntchgItm = BusinessUtility.GetString(dtDetailItem.Rows[i]["IntchgItm"]);
                                    oXCTI55.IntchgSitm = BusinessUtility.GetString(dtDetailItem.Rows[i]["IntchgSitm"]);
                                    oXCTI55.Form = BusinessUtility.GetString(dtDetailItem.Rows[i]["Form"]);
                                    oXCTI55.Grid = BusinessUtility.GetString(dtDetailItem.Rows[i]["Grid"]);
                                    oXCTI55.Size = BusinessUtility.GetString(dtDetailItem.Rows[i]["Size"]);
                                    oXCTI55.Finish = BusinessUtility.GetString(dtDetailItem.Rows[i]["Finish"]);
                                    oXCTI55.EfEver = oDetails.ExtendedFinish;
                                    oXCTI55.Length = BusinessUtility.GetString(dtDetailItem.Rows[i]["Length"]);
                                    oXCTI55.DimDsgn = oDetails.DimDsgn;
                                    oXCTI55.RjctRsn = BusinessUtility.GetString(ConfigurationManager.AppSettings["RjctRsn"]);
                                    oXCTI55.InvtPcsTyp = BusinessUtility.GetString(ConfigurationManager.AppSettings["InvtPcsTyp"]);
                                    oXCTI55.InvtMsrTyp = BusinessUtility.GetString(ConfigurationManager.AppSettings["InvtMsrTyp"]);
                                    oXCTI55.InvtWgtTyp = BusinessUtility.GetString(ConfigurationManager.AppSettings["InvtWgtTyp"]);
                                    oXCTI55.ShpntPcs = BusinessUtility.GetString(ConfigurationManager.AppSettings["ShpntPcs"]);
                                    // oXCTI55.ShpntMsr = BusinessUtility.GetString(ConfigurationManager.AppSettings["ShpntMsr"]);

                                    oXCTI55.ShpntMsr = BusinessUtility.GetString(dtDetailItem.Rows[i]["ShpntMsr"]);

                                    // oXCTI55.ShpntWgt = oDetails.OrigShptWgt;
                                    oXCTI55.ShpntWgt = BusinessUtility.GetString(dtDetailItem.Rows[i]["ShpntWgt"]);

                                    oXCTI55.ShpntPcsTyp = BusinessUtility.GetString(ConfigurationManager.AppSettings["ShpntPcsTyp"]);
                                    oXCTI55.ShpntMsrTyp = BusinessUtility.GetString(ConfigurationManager.AppSettings["ShpntMsrTyp"]);
                                    oXCTI55.ShpntWgtTyp = BusinessUtility.GetString(ConfigurationManager.AppSettings["ShpntWgtTyp"]);
                                    oXCTI55.Heat = BusinessUtility.GetString(dtDetailItem.Rows[i]["Heat"]);
                                    oXCTI55.GaType = oDetails.GaType;
                                    oXCTI55.GaSize = BusinessUtility.GetString(dtDetailItem.Rows[i]["GaSize"]);
                                    oXCTI55.DimDsgn = oDetails.DimDsgn;
                                    oXCTI55.Loc = BusinessUtility.GetString(dtDetailItem.Rows[i]["Loc"]);
                                    oXCTI55.Mill = BusinessUtility.GetString(dtDetailItem.Rows[i]["Mill"]);

                                    oXCTI55.Width = oDetails.Width;
                                    oXCTI55.Pkg = BusinessUtility.GetString(dtDetailItem.Rows[i]["Pkg"]);

                                    oXCTI55.InvtQlty = BusinessUtility.GetString(dtDetailItem.Rows[i]["InvtQlty"]);
                                    oXCTI55.InvtType = BusinessUtility.GetString(ConfigurationManager.AppSettings["InvtType"]);


                                    oXCTI55.CoilLengthTyp = BusinessUtility.GetString(dtDetailItem.Rows[i]["CoilLengthTyp"]); ;
                                    oXCTI55.CoilLength = BusinessUtility.GetString(dtDetailItem.Rows[i]["CoilLength"]);
                                    oXCTI55.ActGa1 = BusinessUtility.GetString(dtDetailItem.Rows[i]["ActGa1"]);

                                    oXCTI55.I55Id = BusinessUtility.GetString(dtDetailItem.Rows[i]["I55Id"]);
                                    oXCTI55.I55Od = BusinessUtility.GetString(dtDetailItem.Rows[i]["I55Od"]);

                                    oXCTI55.MillId = BusinessUtility.GetString(dtDetailItem.Rows[i]["MillId"]);

                                    oXCTI55.CutNo = BusinessUtility.GetString(dtDetailItem.Rows[i]["CutNo"]);
                                    oXCTI55.CusTagNo = BusinessUtility.GetString(dtDetailItem.Rows[i]["CusTagNo"]);
                                    oXCTI55.TagNo = BusinessUtility.GetString(dtDetailItem.Rows[i]["TagNo"]);
                                    oXCTI55.ThpTyID = BusinessUtility.GetString(dtDetailItem.Rows[i]["OpTagID"]);
                                    oXCTI55.InsertDetailItemsInInfomixForBailySO(oXCTI55, oDbHelper);

                                    ErrorLog.createLog("Stratix Detais item created..");
                                    Console.WriteLine("Stratix Detais item created..");
                                }
                            }


                            if (iStatus > 0)
                            {
                                iStatus = 0;
                                sbSql = new StringBuilder();
                                sbSql.Append(" insert into XCTI52_rec (i52_cmpy_id, i52_intchg_pfx, i52_intchg_no, i52_prs_md, i52_rcpt_pfx, i52_rcpt_no, i52_rcpt_itm ");
                                sbSql.Append(" , i52_itm_ctl_no, i52_tag_no, i52_prd_chg, i52_cond_chg, i52_hld_chg, i52_instr_chg, i52_whs_ofc ");
                                sbSql.Append(" , i52_cnv_rcpt_no, i52_crtd_dtts, i52_crtd_dtms, i52_upd_dtts,i52_upd_dtms ");
                                sbSql.Append(" ) ");
                                sbSql.Append(" values ('" + m_sCompanyId + "', '" + m_sIntchgPfx + "', '" + iInterChangeNo + "', 'A', 'PR', 0, 0, '0', '', 0, 0, 0, 0, 'W', 0, current, '1', current,'1') ");

                                ErrorLog.createLog(BusinessUtility.GetString(sbSql));

                                iStatus = oDbHelper.ExecuteNonQuery(BusinessUtility.GetString(sbSql), CommandType.Text);

                                if (iStatus > 0)
                                {
                                    iStatus = 0;
                                    sbSql = new StringBuilder();
                                    sbSql.Append(" INSERT INTO xcti00_rec (i00_cmpy_id,i00_intchg_pfx,i00_intchg_no,i00_evnt,i00_usr_id,i00_crtd_dtts,i00_crtd_dtms,i00_upd_dtts,i00_upd_dtms, ");
                                    sbSql.Append(" i00_sts_cd,i00_ssn_log_ctl_no,i00_intrf_cl) VALUES ('" + m_sCompanyId + "','" + m_sIntchgPfx + "','" + iInterChangeNo + "','PRC','" + sXctiUserId + "',CURRENT,0,CURRENT,0,'N',0,'E') ");

                                    ErrorLog.createLog(BusinessUtility.GetString(sbSql));
                                    ErrorLog.createLog("Stratix xcti00 triggered..");
                                    Console.WriteLine("Stratix xcti00 triggered..");

                                    iStatus = oDbHelper.ExecuteNonQuery(BusinessUtility.GetString(sbSql), CommandType.Text);



                                    if (iStatus > 0)
                                    {
                                        //   sbSql = new StringBuilder();
                                        //sbSql.Append("update  files SET IntchgNo = '" + iInterChangeNo + "' where FileId ='" + m_iFileId + "'");
                                        //iStatus = oMysql.ExecuteNonQuery(BusinessUtility.GetString(sbSql), CommandType.Text, null);


                                        int iReceiptNo = 0;
                                        //sbSql = new StringBuilder();
                                        //sbSql.Append("select i53_new_rcpt_no from xcti53_rec where i53_intchg_no = '" + iInterChangeNo + "' ");
                                        //object oRcptNo = oDbHelper.GetValue(sbSql.ToString(), CommandType.Text);
                                        //iReceiptNo = BusinessUtility.GetInt(oRcptNo);

                                        ErrorLog.createLog("iReceiptNo =" + iReceiptNo);

                                        //ALTER TABLE `header` ADD COLUMN `IntchgNo` INT NULL;
                                        sbSql = new StringBuilder();
                                        sbSql.Append(" UPDATE header SET  IntchgNo = '" + iInterChangeNo + "'  WHERE HeaderId = '" + m_iHeaderId + "' ");
                                        iTECH.Library.DataAccess.MySql.DbHelper oDbHelper1 = new iTECH.Library.DataAccess.MySql.DbHelper(sMysqlConnectionString, true);
                                        iStatus = oDbHelper1.ExecuteNonQuery(BusinessUtility.GetString(sbSql), CommandType.Text, null);


                                        sbSql = new StringBuilder();
                                        sbSql.Append(" UPDATE detail SET  IntchgNo = '" + iInterChangeNo + "'  WHERE HeaderId = '" + m_iHeaderId + "' ");
                                        iStatus = oDbHelper1.ExecuteNonQuery(BusinessUtility.GetString(sbSql), CommandType.Text, null);



                                        //Update File status 
                                        sbSql = new StringBuilder();
                                        sbSql.Append(" UPDATE files SET  Status = 1 , IntchgNo = '" + iInterChangeNo + "' , ReceiptNo = '" + iReceiptNo + "'  WHERE FileId = '" + m_iFileId + "' ");
                                        iStatus = oDbHelper1.ExecuteNonQuery(BusinessUtility.GetString(sbSql), CommandType.Text, null);

                                        ErrorLog.createLog("sbSql =" + sbSql.ToString());


                                        oDbHelper1.CloseDatabaseConnection();

                                        ErrorLog.createLog("Successfully created pre receipt ");
                                        Console.WriteLine("Successfully created pre receipt");
                                    }
                                }
                            }



                        }
                    }
                }
                catch (Exception ex)
                {
                    ErrorLog.createLog("Error while creating pre receipt.");
                    ErrorLog.createLog(ex.ToString());
                    EdiError.LogError(EdiFile, m_iFileId.ToString(), "", ex, "");
                    EdiError.LogError(EdiFile, m_iFileId.ToString(), ConfigurationManager.AppSettings["StratixError856"]);
                }
                finally
                {
                    oDbHelper.CloseDatabaseConnection();
                }

            }
            else
            {
                ErrorLog.createLog("Detail data is empty so omiting creating receipt.");
            }

        } // start insert end here..


        public void TestCustomerOwnReceipt()
        {

            DbHelper oDbHelper = new DbHelper(sStratixConnectionString, true);
            m_sSql = "SELECT (MAX(i00_intchg_no) + 2) AS i00_intchg_no   FROM xcti00_rec";
            try
            {
                object objIntChgNo = oDbHelper.GetValue(m_sSql, CommandType.Text);
                iInterChangeNo = BusinessUtility.GetInt(objIntChgNo);

                ErrorLog.createLog("iInterChangeNo = " + iInterChangeNo);
            }
            catch (Exception ex)
            { ErrorLog.CreateLog(ex); }


            sbSql = new StringBuilder();
            sbSql.Append(" insert into xcti53_rec (i53_cmpy_id, i53_intchg_pfx, i53_intchg_no, i53_rcpt_typ, i53_cus_ven_id, i53_cus_ven_shp, i53_rcpt_dt, i53_ven_shp_ref, ");
            sbSql.Append(" i53_crr_vcl_desc, i53_rcvg_whs, i53_dlvy_mthd, i53_frt_ven_id, i53_crr_nm, i53_crr_ref_no, i53_prcpt_sts,  i53_new_rcpt_pfx, i53_new_rcpt_no, ");
            sbSql.Append(" i53_ncr_pfx, i53_ncr_no) ");
            sbSql.Append(" VALUES ('STX', 'XI', '" + iInterChangeNo + "', 'O', '1010', '0', current, '10166710 -  1923', '', 'CHI', '', '', '', '','I', '', '0', '','0') ");

            ErrorLog.createLog(BusinessUtility.GetString(sbSql));
            ErrorLog.createLog("sPreRcptStatus = " + sPreRcptStatus);

            int iStatus = 0;
            try
            {

                iStatus = oDbHelper.ExecuteNonQuery(BusinessUtility.GetString(sbSql), CommandType.Text);
                ErrorLog.createLog("i53 iStatus = " + iStatus);
            }
            catch (Exception ex)
            { ErrorLog.CreateLog(ex); }
            if (iStatus > 0)
            {
                sbSql = new StringBuilder();
                sbSql.Append(" insert into XCTI54_rec (i54_cmpy_id, i54_intchg_pfx, i54_intchg_no, i54_intchg_itm,i54_prnt_pfx, i54_prnt_no,i54_prnt_itm,  i54_prnt_sitm, ");
                sbSql.Append(" i54_cus_rtn_mthd, i54_shpt_no, i54_shp_tag_no, i54_ven_shp_ref, i54_orig_shpnt_pcs,i54_orig_shpnt_msr,  i54_orig_shpnt_wgt, i54_shp_grs_pcs, ");
                sbSql.Append(" i54_shp_grs_msr, i54_shp_grs_wgt,  i54_frm , i54_grd, i54_size,  i54_fnsh, ");
                sbSql.Append(" i54_ef_evar , i54_wdth,  i54_lgth , i54_dim_dsgn ,  i54_idia , i54_odia,  ");
                sbSql.Append("  i54_wthck, i54_ga_size, i54_ga_typ,  i54_rdm_dim_1 , i54_rdm_dim_2,  i54_rdm_dim_3,  ");
                sbSql.Append("  i54_rdm_dim_4 , i54_rdm_dim_5 ,  i54_rdm_dim_6 , i54_rdm_dim_7 , ");
                sbSql.Append(" i54_rdm_dim_8 , i54_ofc_rvw , i54_new_rcpt_pfx, i54_new_rcpt_no,  i54_new_rcpt_itm, i54_sls_cat , i54_ncr_pfx, i54_ncr_no, i54_ncr_itm)  ");
                sbSql.Append(" values('STX', 'XI', '" + iInterChangeNo + "', '1','', '5744','1',  0, ");
                sbSql.Append(" '', '0', '', '', '1','0.0000',  '0.00', '1', ");
                sbSql.Append(" '0.0000', '0.00',  'AC' , '1100', '032',  'H14', ");
                sbSql.Append(" '' , '0.0000',  '0' , 'F' ,  '0.00000' , '0.00000' , ");
                sbSql.Append(" '0.00000', '0.031', '',  '0.0000' , '0.0000', '0.0000',  ");
                sbSql.Append(" '0.0000' , '0.0000' ,  '0.0000' , '0.0000' ,  ");
                sbSql.Append(" '0.0000' , '0' , '0', '0',   '0', '0' , '0', '0', '0')  ");
                try
                {

                    iStatus = oDbHelper.ExecuteNonQuery(BusinessUtility.GetString(sbSql), CommandType.Text);
                    ErrorLog.createLog("i54 iStatus = " + iStatus);
                }
                catch (Exception ex)
                { ErrorLog.CreateLog(ex); }
            }

            if (iStatus > 0)
            {
                sbSql = new StringBuilder();

                sbSql.Append(" insert into XCTI52_rec (i52_cmpy_id, i52_intchg_pfx, i52_intchg_no, i52_prs_md, i52_rcpt_pfx, i52_rcpt_no, ");
                sbSql.Append(" i52_rcpt_itm  , i52_itm_ctl_no, i52_tag_no, i52_prd_chg, i52_cond_chg, i52_hld_chg, ");
                sbSql.Append(" i52_instr_chg, i52_whs_ofc  , i52_cnv_rcpt_no, i52_crtd_dtts, i52_crtd_dtms, i52_upd_dtts,  ");
                sbSql.Append(" i52_upd_dtms  ) ");
                sbSql.Append(" values ('STX', 'XI', '" + iInterChangeNo + "', 'A', 'PR', 0, ");
                sbSql.Append(" 0, '0', '', 0, 0, 0, ");
                sbSql.Append(" 0, 'W', 0, current, '1', current, ");
                sbSql.Append(" '1') ");
                try
                {

                    iStatus = oDbHelper.ExecuteNonQuery(BusinessUtility.GetString(sbSql), CommandType.Text);
                    ErrorLog.createLog("i52 iStatus = " + iStatus);
                }
                catch (Exception ex)
                { ErrorLog.CreateLog(ex); }
            }

            if (iStatus > 0)
            {
                sbSql = new StringBuilder();
                sbSql.Append(" INSERT INTO xcti00_rec (i00_cmpy_id,i00_intchg_pfx,i00_intchg_no,i00_evnt,i00_usr_id,i00_crtd_dtts, ");
                sbSql.Append(" i00_crtd_dtms,i00_upd_dtts,i00_upd_dtms,  i00_sts_cd,i00_ssn_log_ctl_no,i00_intrf_cl) ");
                sbSql.Append(" VALUES ('STX','XI','" + iInterChangeNo + "','PRC','administ',CURRENT,  ");
                sbSql.Append(" 0,CURRENT,0,'N',0,'E') ");

                try
                {

                    iStatus = oDbHelper.ExecuteNonQuery(BusinessUtility.GetString(sbSql), CommandType.Text);
                    ErrorLog.createLog("i00 iStatus = " + iStatus);
                }
                catch (Exception ex)
                { ErrorLog.CreateLog(ex); }

            }

            //select * from sctmsg_rec order by 4 desc
            //--i53_new_rcpt_no
            //select * from XCTI53_rec where i53_intchg_no =300015
            //select * from xcti00_rec where i00_intchg_no =300015


        }



        public void ParseCustomerOwnReceipt(string sPath, ref Configuration oConfiguration)
        {
            ErrorLog.createLog("ParseCustomerOwnReceipt in");
            m_oConfiguration = oConfiguration;
            DataTable dtCustData = ExcelHelper.GetDataFromCSV(sPath);

            //ChoCSVReader oChoCSVReader = new ChoCSVReader(sPath);
            //DataTable dtCustData = oChoCSVReader.AsDataTable();

            //if (dtCustData.Rows.Count > 0)
            //{
            //    dtCustData.Rows[0].Delete();
            //    dtCustData.AcceptChanges();
            //}

            m_iFileId = Globalcl.FileId;
            iTECH.Library.DataAccess.MySql.DbHelper oDbHelper = new iTECH.Library.DataAccess.MySql.DbHelper(sMysqlConnectionString, true);
            tctipd otctipd1 = new tctipd();
            ErrorLog.createLog(" dtCustData count = " + dtCustData.Rows.Count);
            string sLocation = string.Empty;
            string sShipperNo = string.Empty;
            string sHeat = string.Empty;
            string sWarehouse = string.Empty;
            string sVendorTag = string.Empty;
            string sOpTag = string.Empty;
            string sGaSiZe = string.Empty;
            string sWidth = string.Empty;
            string sCustId = string.Empty;
            string sPartNo = string.Empty;
            string sRevNo = string.Empty;
            if (dtCustData.Rows.Count > 0)
            {
                //FileType = Files.ExecutionFileType.EDI8561;
                //string sCustId = BusinessUtility.GetString(dtCustData.Rows[0][1]).TrimStart('0');
                //string sPartNo = BusinessUtility.GetString(dtCustData.Rows[0][6]).TrimStart('0');
                //string sRevNo = BusinessUtility.GetString(dtCustData.Rows[0][7]).TrimStart('0');
                //sLocation = BusinessUtility.GetString(dtCustData.Rows[0][9]).TrimStart('0');
                //sHeat = BusinessUtility.GetString(dtCustData.Rows[0][15]).TrimStart('0');
                //sWarehouse = BusinessUtility.GetString(dtCustData.Rows[0][8]).TrimStart('0');
                //sVendorTag = BusinessUtility.GetString(dtCustData.Rows[0][10]).TrimStart('0');
                //sOpTag = BusinessUtility.GetString(dtCustData.Rows[0][11]).TrimStart('0');
                //ErrorLog.createLog("sCustId = " + sCustId + " sPartNo = " + sPartNo + " sRevNo = " + sRevNo);
                //otctipd1.PopulateProductInfo(BusinessUtility.GetInt(sCustId), sPartNo, sRevNo);
                //ErrorLog.createLog("optag = " + sOpTag);
                //Form = otctipd1.Form;
                //Grid = otctipd1.Grade;
                //Size = otctipd1.Size;
                //Finish = otctipd1.Finish;
                //string sGaSiZe = otctipd1.GaSize;
                //string sWidth = otctipd1.Width;
                //if (string.IsNullOrEmpty(Form))
                //{
                //    EdiError.LogError(EdiFile, m_iFileId.ToString(), "Form , Grade or Size not available for Partno : " + sPartNo);
                //}
                //CusVenShp = BusinessUtility.GetString(dtCustData.Rows[0][3]);
                //PO = "0";


                Shipment oShipment = new Shipment();
                Files oFiles = new Files(m_iFileId);

                try
                {
                    #region HeaderInsertStart

                    Header oHeader = new Header();
                    Console.WriteLine("parsing header");
                    ErrorLog.createLog("parsing header");

                    //getting warehouse detail


                    oShipment.CusVenId = oFiles.VendorId.ToString();
                    oShipment.ShipTo = Utility.GetConfigValue("ShipTo");
                    oShipment.VenShpRef = Utility.GetConfigValue("VenShpRef");
                    oShipment.CarrierVehicleDesc = Utility.GetConfigValue("CarrierVehicleDesc");
                    //oShipment.RcvWhs = Utility.GetConfigValue("RcvWhs");
                    oShipment.DeliveryMethod = Utility.GetConfigValue("DeliveryMethod");
                    //oShipment.FreightVenId = Utility.GetConfigValue("FreightVenId");
                    oShipment.CarrierName = Utility.GetConfigValue("CarrierName");
                    oShipment.CarrierRefNo = Utility.GetConfigValue("CarrierRefNo");
                    oShipment.PreRcptStatus = Utility.GetConfigValue("PreRcptStatus");

                    //populate RchWhs and FreightVenId through PO detail    22/06/2020
                    string PONo = BusinessUtility.GetString(dtCustData.Rows[0][1]);
                    if (PONo.Contains('-'))
                    {
                        string[] tempprntno = PONo.Split("-");
                        if (tempprntno.Length > 1)
                        {
                            PONo = tempprntno[1].Trim();
                        }
                    }
                    otctipd1.PopulateWarehouse(ref oShipment, BusinessUtility.GetString(PONo), m_oConfiguration.CompanyId);

                    m_iHeaderId = oHeader.HeaderInsertion(Globalcl.FileId, oShipment);
                    ErrorLog.createLog("Globalcl.FileId = " + Globalcl.FileId);
                    ErrorLog.createLog("m_iHeaderId = " + m_iHeaderId);

                    #endregion HeaderInsertEnd

                    #region DetailInsertStart

                    if (m_iHeaderId > 0)
                    {
                        int counter = 1;
                        foreach (DataRow oDataRow in dtCustData.Rows)
                        {
                            if (!string.IsNullOrEmpty(BusinessUtility.GetString(oDataRow[1])))
                            {
                                Details oDetails = new Details();
                                Console.WriteLine("parsing detail");
                                ErrorLog.createLog("parsing detail");
                                //string sCustId1 = BusinessUtility.GetString(oDataRow[1]).TrimStart('0');
                                //string sPartNo1 = BusinessUtility.GetString(oDataRow[6]).TrimStart('0');
                                //string sRevNo1 = BusinessUtility.GetString(oDataRow[7]).TrimStart('0');
                                //sLocation = BusinessUtility.GetString(oDataRow[9]).TrimStart('0');
                                //sHeat = BusinessUtility.GetString(oDataRow[15]).TrimStart('0');
                                //sWarehouse = BusinessUtility.GetString(oDataRow[8]).TrimStart('0');
                                //sVendorTag = BusinessUtility.GetString(oDataRow[10]).TrimStart('0');
                                //sOpTag = BusinessUtility.GetString(oDataRow[11]).TrimStart('0');
                                //ErrorLog.createLog("sCustId1 = " + sCustId1 + " sPartNo1 = " + sPartNo1 + " sRevNo1 = " + sRevNo1);
                                //otctipd1.PopulateProductInfo(BusinessUtility.GetInt(sCustId1), sPartNo1, sRevNo1);

                                oDetails.PrntNo = BusinessUtility.GetString(oDataRow[1]);
                                if (oDetails.PrntNo.Contains('-'))
                                {
                                    string[] prntno = oDetails.PrntNo.Split("-");
                                    if (prntno.Length > 1)
                                    {
                                        oDetails.PrntNo = prntno[1].Trim();
                                    }
                                }

                                oDetails.PrntItm = "1";     // default item and sub item as 1
                                oDetails.PrntSubItm = "1";
                                oDetails.ShipperNo = Utility.GetConfigValue("ShipperNo");
                                oDetails.IntchgItm = counter.ToString();
                                oDetails.CusRtnMthd = Utility.GetConfigValue("CusRtnMthd");
                                oDetails.ShptNo = oDetails.PrntNo;
                                oDetails.ShptTagNo = Utility.GetConfigValue("ShptTagNo");
                                oDetails.VenShptRef = Utility.GetConfigValue("VenShpRef");
                                oDetails.OrigShpntPcs = Utility.GetConfigValue("OrigShpntPcs");
                                oDetails.OrigShpntMsr = Utility.GetConfigValue("OrigShpntMsr");
                                oDetails.OrigShptWgt = Utility.GetConfigValue("OrigShptWgt");
                                oDetails.ShpGrsPcs = Utility.GetConfigValue("ShpGrsPcs");
                                oDetails.ShpGrsMsr = Utility.GetConfigValue("ShpGrsMsr");
                                oDetails.ShpGrsWgt = Utility.GetConfigValue("ShpGrsWgt");

                                //get po item detail
                                otctipd1.GetPoDetails(Convert.ToInt16(oDetails.PrntNo), Convert.ToInt16(oDetails.PrntItm));

                                oDetails.Form = otctipd1.Form;
                                oDetails.Grade = otctipd1.Grade;
                                oDetails.Size = otctipd1.Size;
                                oDetails.Finish = otctipd1.Finish;
                                oDetails.ExtendedFinish = otctipd1.ExtendedFinish;
                                //oDetails.Length = otctipd1.Length;
                                oDetails.Width = otctipd1.Width;
                                //oDetails.i54_bgt_as_part = sPartNo1;
                                //if (string.IsNullOrEmpty(oDetails.Width))
                                //{
                                //    oDetails.Width = "0.0";
                                //}

                                #region pass length value from csv file     2020/10/23  Sumit
                                //if (Convert.ToDouble(otctipd1.Length) > 0)
                                //    oDetails.Length = otctipd1.Length;
                                //else
                                //    oDetails.Length = BusinessUtility.GetString(oDataRow[9]);
                                #endregion pass length value from csv file     2020/10/23  Sumit
                                oDetails.Length = BusinessUtility.GetString(oDataRow[9]);

                                oDetails.DimDsgn = otctipd1.DimDsgn;
                                oDetails.IDa = otctipd1.IDa;
                                oDetails.ODa = otctipd1.ODa;
                                oDetails.GaSize = otctipd1.GaSize;
                                oDetails.GaType = otctipd1.GaType;
                                oDetails.HeatNo = BusinessUtility.GetString(oDataRow[8]);

                                m_iDetailId = oDetails.InsertDetails(m_iHeaderId, oDetails, oDbHelper);
                                ErrorLog.createLog("m_iDetailId = " + m_iDetailId);


                                #region to insert record into detail item
                                if (m_iHeaderId > 0 && m_iDetailId > 0)
                                {
                                    Console.WriteLine("Parsing Detail Items");
                                    ErrorLog.createLog("Parsing Detail Items");

                                    XCTI55 oXCTI55 = new XCTI55();
                                    oXCTI55.CmpyId = m_oConfiguration.CompanyId;
                                    oXCTI55.IntchgPfx = Utility.GetConfigValue("IntchgPfx");
                                    oXCTI55.IntchgItm = counter.ToString();
                                    oXCTI55.IntchgSitm = "1";

                                    oXCTI55.Form = oDetails.Form;
                                    oXCTI55.Grid = oDetails.Grade;
                                    oXCTI55.Size = oDetails.Size;
                                    oXCTI55.Finish = oDetails.Finish;
                                    oXCTI55.EfEver = oDetails.ExtendedFinish;
                                    oXCTI55.Width = oDetails.Width;

                                    oXCTI55.Length = oDetails.Length;
                                    oXCTI55.DimDsgn = oDetails.DimDsgn;
                                    oXCTI55.Idia = oDetails.IDa;

                                    //oXCTI55.GaSize = BusinessUtility.GetString(oDataRow["Col8"]);
                                    oXCTI55.GaSize = oDetails.GaSize;
                                    oXCTI55.GaType = oDetails.GaType;

                                    //oXCTI55.TagNo = BusinessUtility.GetString(oDataRow["Col10"]); // tag
                                    oXCTI55.TagNo = Utility.GetConfigValue("TagNo");
                                    oXCTI55.Mill = oConfiguration.MillId; // mill
                                    oXCTI55.Heat = oDetails.HeatNo; // heat
                                    oXCTI55.MillId = BusinessUtility.GetString(oDataRow[5]);
                                    oXCTI55.InvtType = Utility.GetConfigValue("InvtType");
                                    oXCTI55.RjctRsn = Utility.GetConfigValue("RjctRsn");
                                    oXCTI55.RjctLgnId = Utility.GetConfigValue("RjctLgnId");
                                    oXCTI55.RjctRmk = Utility.GetConfigValue("RjctRmk");

                                    //oXCTI55.InvtQlty = oDetails.InvtQlty;
                                    oXCTI55.InvtQlty = otctipd1.InvtQlty;
                                    if (string.IsNullOrEmpty(oXCTI55.InvtQlty))
                                        oXCTI55.InvtQlty = Utility.GetConfigValue("InvtQlty");

                                    oXCTI55.Loc = Utility.GetConfigValue("Loc");

                                    //calculate Pcs and Msr on the basis of csv file width and twf (theoretical foot weight)    2020/06/03
                                    //oXCTI55.InvtPcs = Utility.GetConfigValue("InvtPcs");
                                    //oXCTI55.InvtMsr = Utility.GetConfigValue("InvtMsr");
                                    oXCTI55.InvtPcs = Convert.ToString(tctipd.GetInvtPcs(BusinessUtility.GetString(oDataRow[7]), oXCTI55.Form, oXCTI55.Grid, oXCTI55.Size, oXCTI55.Finish));
                                    oXCTI55.InvtMsr = Convert.ToString(tctipd.GetInvtMsr(BusinessUtility.GetString(oDataRow[7]), oXCTI55.Form, oXCTI55.Grid, oXCTI55.Size, oXCTI55.Finish));
                                    oXCTI55.InvtWgt = BusinessUtility.GetString(oDataRow[7]);
                                    oXCTI55.InvtPcsTyp = Utility.GetConfigValue("InvtPcsTyp");
                                    oXCTI55.InvtMsrTyp = Utility.GetConfigValue("InvtMsrTyp");
                                    oXCTI55.InvtWgtTyp = Utility.GetConfigValue("InvtWgtTyp");

                                    //oXCTI55.ShpntPcs = Utility.GetConfigValue("ShpntPcs");
                                    //oXCTI55.ShpntMsr = Utility.GetConfigValue("ShpntMsr");
                                    oXCTI55.ShpntPcs = oXCTI55.InvtPcs;
                                    oXCTI55.ShpntMsr = oXCTI55.InvtMsr;
                                    oXCTI55.ShpntWgt = BusinessUtility.GetString(oDataRow[7]);
                                    oXCTI55.ShpntPcsTyp = Utility.GetConfigValue("ShpntPcsTyp");
                                    oXCTI55.ShpntMsrTyp = Utility.GetConfigValue("ShpntMsrTyp");
                                    oXCTI55.ShpntWgtTyp = Utility.GetConfigValue("ShpntWgtTyp");

                                    oXCTI55.Pkg = Utility.GetConfigValue("Pkg"); // packaging
                                    oXCTI55.CutNo = otctipd1.CutNumber;
                                    oXCTI55.I55Id = oDetails.IDa;
                                    oXCTI55.I55Od = oDetails.ODa;

                                    #region pass 0 as coil length and pass blank as coil length type if not available   2020/10/23  Sumit
                                    //oXCTI55.CoilLength = otctipd1.CoilLength;
                                    //oXCTI55.CoilLengthTyp = otctipd1.CoilLengthType;
                                    if (!string.IsNullOrEmpty(otctipd1.CoilLength))
                                        oXCTI55.CoilLength = otctipd1.CoilLength;
                                    else
                                        oXCTI55.CoilLength = "0";

                                    if (!string.IsNullOrEmpty(otctipd1.CoilLengthType))
                                        oXCTI55.CoilLengthTyp = otctipd1.CoilLengthType;
                                    else
                                        oXCTI55.CoilLengthTyp = string.Empty;
                                    #endregion pass 0 as coil length and pass blank as coil length type if not available   2020/10/23  Sumit

                                    oXCTI55.CusTagNo = Utility.GetConfigValue("CusTagNo");

                                    Console.Write("parsing detail item");
                                    ErrorLog.createLog("parsing detail item");
                                    oXCTI55.InsertDetailItems(m_iHeaderId, m_iDetailId, oXCTI55, oDbHelper);

                                }
                                else
                                {
                                    ErrorLog.createLog("Header or detail is missing");
                                }
                                #endregion to insert record into detail item
                            }

                            counter++;
                        }

                    }
                    else
                    { ErrorLog.createLog("Header is missing"); }

                    #endregion DetailInsertEnd

                    if (m_iFileId > 0 && m_iHeaderId > 0 && m_iDetailId > 0)
                    {
                        ErrorLog.createLog("Connecting stratix");
                        Console.WriteLine("Connecting stratix");
                        //this.StartInsert(ref oConfiguration, oShipment);
                        STX856 oSTX856 = new STX856();
                        oSTX856.ProcessReceipt(m_iFileId);
                    }
                }
                catch (Exception ex)
                { ErrorLog.CreateLog(ex); }

            }
        }

        
    }
}
