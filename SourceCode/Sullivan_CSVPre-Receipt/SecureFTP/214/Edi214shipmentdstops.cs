﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Text;

namespace SecureFTP._214
{
   public class Edi214shipmentdstops
    {
        StringBuilder m_sbSql = null;
        string m_sSql = string.Empty;
        static string sMysqlConnectionString = ConfigurationManager.ConnectionStrings["NewConnectionString"].ToString();
        

        public string            shipId                                  { get; set; }
    public string         StopNumber                                     { get; set; }
    public string         ShipmentStatusCodes                            { get; set; }
    public string         ShipmentStatusAppointmentReasonCodes           { get; set; }
    public string         ShipmentAppointmentStatusCode                  { get; set; }
    public string         Date                                           { get; set; }
    public string         Time                                           { get; set; }
    public string         TimeCode                                       { get; set; }
    public string         StopLevelReferenceNumber                       { get; set; }
    public string         StopLevelQualifier                             { get; set; }
    public string         StopLevelComment                               { get; set; }
    public string         WeightQualifier                                { get; set; }
    public string         WeightUnitCode                                 { get; set; }
    public string         WeightStopLevel                                { get; set; }
    public string         VolumeUnitQualifier                            { get; set; }
    public string         VolumeStopLevel                                { get; set; }
    public string         LadingQuantityStopLevel                        { get; set; }
    public string         StandardCarrierAlphaCodeTR                     { get; set; }
    public string         EquipmentNumberTR                              { get; set; }
    public string         EquipmentDescriptionCodeTR                     { get; set; }
    public string         StandardCarrierAlphaCodeTL                     { get; set; }
    public string         EquipmentNumberTL                              { get; set; }
    public string         EquipmentDescriptionCodeTL                     { get; set; }
    public string         EntityIdentifierCode                           { get; set; }
    public string         EntityName                                     { get; set; }
    public string         IdentificationCodeQualifier                    { get; set; }
    public string         IdentifficationCode                            { get; set; }
    public string         Address                                        { get; set; }
    public string         City                                           { get; set; }
    public string         State                                          { get; set; }
    public string         ZipCode                                        { get; set; }
    public string         CountryCode                                    { get; set; }
    public string         PurchaseOrderNumber                            { get; set; }
    public string         MeasurementCode                                { get; set; }
    public string         Quantity                                       { get; set; }
    public string         SPOWeightUnitCode                              { get; set; }
    public string Weight                                                { get; set; }



        public int Insert214ShipmentStops(int iShipID, Edi214shipmentdstops oEdi214ShipmentDetail)
        {
            int iStatus = 0;
            m_sbSql = new StringBuilder();
            iTECH.Library.DataAccess.MySql.DbHelper oDbHelper = new iTECH.Library.DataAccess.MySql.DbHelper(sMysqlConnectionString, true);
            try
            {
                m_sbSql.Append(" INSERT INTO edi214shipmentdstops (shipId, StopNumber, ShipmentStatusCodes, ShipmentStatusAppointmentReasonCodes, ShipmentAppointmentStatusCode , Date ,Time , TimeCode  ");
                m_sbSql.Append(" , StopLevelReferenceNumber, StopLevelQualifier, StopLevelComment, WeightQualifier, WeightUnitCode , WeightStopLevel, VolumeUnitQualifier , VolumeStopLevel ,LadingQuantityStopLevel ");
                m_sbSql.Append(" , StandardCarrierAlphaCodeTR,  EquipmentNumberTR, EquipmentDescriptionCodeTR ,StandardCarrierAlphaCodeTL , EquipmentNumberTL , EquipmentDescriptionCodeTL , EntityIdentifierCode ");
                m_sbSql.Append(" , EntityName, IdentificationCodeQualifier, IdentifficationCode, Address, City , State, ZipCode , CountryCode , PurchaseOrderNumber , MeasurementCode , Quantity , SPOWeightUnitCode ,Weight ) ");
                m_sbSql.Append(" VALUES  ("+ iShipID + " , '"+ oEdi214ShipmentDetail .StopNumber+ "' , '" + oEdi214ShipmentDetail.ShipmentStatusCodes + "' , '" + oEdi214ShipmentDetail.ShipmentStatusAppointmentReasonCodes + "'  ");

                m_sbSql.Append(" , '" + oEdi214ShipmentDetail.ShipmentAppointmentStatusCode + "' , '" + oEdi214ShipmentDetail.Date + "' , '" + oEdi214ShipmentDetail.Time + "' , '" + oEdi214ShipmentDetail.TimeCode + "'  ");
                m_sbSql.Append(" , '" + oEdi214ShipmentDetail.StopLevelReferenceNumber + "' , '" + oEdi214ShipmentDetail.StopLevelQualifier + "' , '" + oEdi214ShipmentDetail.StopLevelComment + "' , '" + oEdi214ShipmentDetail.WeightQualifier + "'  ");

                m_sbSql.Append(" , '" + oEdi214ShipmentDetail.WeightUnitCode + "' , '" + oEdi214ShipmentDetail.WeightStopLevel + "' , '" + oEdi214ShipmentDetail.VolumeUnitQualifier + "' , '" + oEdi214ShipmentDetail.VolumeStopLevel + "'  ");
                m_sbSql.Append(" , '" + oEdi214ShipmentDetail.LadingQuantityStopLevel + "' , '" + oEdi214ShipmentDetail.StandardCarrierAlphaCodeTR + "' , '" + oEdi214ShipmentDetail.EquipmentNumberTR + "' , '" + oEdi214ShipmentDetail.EquipmentDescriptionCodeTR + "'  ");

                m_sbSql.Append(" , '" + oEdi214ShipmentDetail.StandardCarrierAlphaCodeTL + "' , '" + oEdi214ShipmentDetail.EquipmentNumberTL + "' , '" + oEdi214ShipmentDetail.EquipmentDescriptionCodeTL + "' , '" + oEdi214ShipmentDetail.EntityIdentifierCode + "'  ");
                m_sbSql.Append(" , '" + oEdi214ShipmentDetail.EntityName + "' , '" + oEdi214ShipmentDetail.IdentificationCodeQualifier + "' , '" + oEdi214ShipmentDetail.IdentifficationCode + "' , '" + oEdi214ShipmentDetail.Address + "' ");

                m_sbSql.Append(" , '" + oEdi214ShipmentDetail.City + "' , '" + oEdi214ShipmentDetail.State + "' , '" + oEdi214ShipmentDetail.ZipCode + "' , '" + oEdi214ShipmentDetail.CountryCode + "' ");
                m_sbSql.Append(" , '" + oEdi214ShipmentDetail.PurchaseOrderNumber + "' , '" + oEdi214ShipmentDetail.MeasurementCode + "' , '" + oEdi214ShipmentDetail.Quantity + "' , '" + oEdi214ShipmentDetail.SPOWeightUnitCode + "'  ");

                m_sbSql.Append(" , '" + oEdi214ShipmentDetail.Weight + "'  ) ");

                MySqlParameter[] oMySqlParameter = { };
                iStatus = oDbHelper.ExecuteNonQuery(m_sbSql.ToString(), CommandType.Text, oMySqlParameter);
                iStatus = oDbHelper.GetLastInsertID();
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLog(ex);
            }
            finally
            {
                oDbHelper.CloseDatabaseConnection();
            }
            return iStatus;
        }



    }
}
