﻿using iTECH.Library.Utilities;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;

namespace SecureFTP._214
{
    public class Edi214Parser
    {
        static string sStratixConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ToString();
        string[] m_sArr = null;
        Dictionary<int, ArrayList> m_dict = new Dictionary<int, ArrayList>();
        Dictionary<int, List<string>> m_dictItem = new Dictionary<int, List<string>>();
        ArrayList m_arl = new ArrayList();
        Configuration oConfiguration = null;

        static string m_sStartBlock = "ST";
        static string m_sEndBlock = "SE";
        static string m_sInterchangeControlTrailer = "IEA";
        int m_iFileId = 0;
        int m_iFileType = 0;
        Files oFiles;

        List<string> lstHeader = new List<string>();
        List<string> lstStops = new List<string>();

        public Edi214Parser(string sFilePath, int iFileID)
        {
            m_iFileId = iFileID;
            //m_sArr = m_sArr.Where(x => !string.IsNullOrEmpty(x)).ToArray(); // removing empty segment 
            try
            {

                var fileLines = System.IO.File.ReadAllLines(BusinessUtility.GetString(sFilePath));

                string sFileNameWithoutExtension = System.IO.Path.GetFileNameWithoutExtension(sFilePath);

                oConfiguration = new Configuration();
                oConfiguration.PopulateConfigurationData(fileLines);
                // EdiFile = oConfiguration.FileType;

                if (fileLines.Length == 1)
                {
                    var varLinesNew = fileLines[0].Split(new string[] { oConfiguration.LineSeperator }, StringSplitOptions.None);
                    fileLines = varLinesNew;
                    Utility.AppendLineSeperator(ref fileLines, oConfiguration.LineSeperator);

                }
                Utility.AppendLineSeperator(ref fileLines, oConfiguration.LineSeperator);

                m_sArr = fileLines;
                m_sArr = m_sArr.Where(x => !string.IsNullOrEmpty(x)).ToArray();

                this.StartProcessing();
                this.ExtractInfoToMysql();
            }
            catch (Exception ex)
            {
                ErrorLog.createLog("Error occur in Edi863Parser constructor : " + ex.ToString());
                // EdiError.LogError(BusinessUtility.GetString(m_iFileType), m_iFileId.ToString(), "", ex, "");
            }

        }




        public void ExtractInfoToMysql()
        {
            StringBuilder sbMessage = new StringBuilder();
            try
            {
                for (int i = 0; i < m_dict.Count; i++)
                {
                    if (i > 0 && i < m_dict.Count - 1)
                    {
                        m_dictItem = new Dictionary<int, List<string>>();

                        ArrayList olst = m_dict[i];
                        List<string> lstContent = new List<string>();
                        Utility.AddArrayListToList(ref olst, ref lstContent);

                        this.FilterShipmentStops(lstContent);

                        if (lstHeader.Count > 0)
                        {
                            Edi214ShipmentDetail oEdi214ShipmentDetail = new Edi214ShipmentDetail();
                            oEdi214ShipmentDetail.Address = Utility.GetElementValueBasedOnElementConfig(elementconfig.Elements.Address214, lstHeader);
                            oEdi214ShipmentDetail.City = Utility.GetElementValueBasedOnElementConfig(elementconfig.Elements.City214, lstHeader);
                            oEdi214ShipmentDetail.CountryCode = Utility.GetElementValueBasedOnElementConfig(elementconfig.Elements.CountryCode214, lstHeader);
                            oEdi214ShipmentDetail.ZipCode = Utility.GetElementValueBasedOnElementConfig(elementconfig.Elements.ZipCode214, lstHeader);
                            oEdi214ShipmentDetail.CarrierOrderNumber = Utility.GetElementValueBasedOnElementConfig(elementconfig.Elements.CarrierOrderNumber214, lstHeader);
                            oEdi214ShipmentDetail.State = Utility.GetElementValueBasedOnElementConfig(elementconfig.Elements.State214, lstHeader);
                            oEdi214ShipmentDetail.TransactionSetControlNumber = Utility.GetElementValueBasedOnElementConfig(elementconfig.Elements.TransactionSetControlNumber214, lstHeader);
                            oEdi214ShipmentDetail.TransactionSetIdentifierCode = Utility.GetElementValueBasedOnElementConfig(elementconfig.Elements.TransactionSetIdentifierCode214, lstHeader);
                            int iShpno = oEdi214ShipmentDetail.Insert214ShipmentDEtail(m_iFileId, oEdi214ShipmentDetail);

                            sbMessage.Append("Address = " + oEdi214ShipmentDetail.Address);
                            sbMessage.AppendLine();
                            sbMessage.Append("City = " + oEdi214ShipmentDetail.City);
                            sbMessage.AppendLine();
                            sbMessage.Append("CountryCode = " + oEdi214ShipmentDetail.CountryCode);
                            sbMessage.AppendLine();
                            sbMessage.Append("ZipCode = " + oEdi214ShipmentDetail.ZipCode);
                            sbMessage.AppendLine();
                            sbMessage.Append("CarrierOrderNumber = " + oEdi214ShipmentDetail.CarrierOrderNumber);
                            sbMessage.AppendLine();

                            if (m_dictItem.Count > 0 && iShpno > 0)   // all stops are inside dict in form of list
                            {
                                for (int j = 0; j < m_dictItem.Count; j++)
                                {
                                    List<string> lstStop = new List<string>();
                                    lstStop = m_dictItem[j];

                                    if (lstStop.Count > 3)
                                    {
                                        Edi214shipmentdstops oEdi214shipmentdstops = new Edi214shipmentdstops();
                                        oEdi214shipmentdstops.StopNumber = Utility.GetElementValueBasedOnElementConfig(elementconfig.Elements.StopNumber214, lstStop);
                                        oEdi214shipmentdstops.Time = Utility.GetElementValueBasedOnElementConfig(elementconfig.Elements.Time214, lstStop);
                                        oEdi214shipmentdstops.Date = Utility.GetElementValueBasedOnElementConfig(elementconfig.Elements.Date214, lstStop);
                                        oEdi214shipmentdstops.Address = Utility.GetElementValueBasedOnElementConfig(elementconfig.Elements.Address214, lstStop);
                                        oEdi214shipmentdstops.City = Utility.GetElementValueBasedOnElementConfig(elementconfig.Elements.City214, lstStop);
                                        oEdi214shipmentdstops.State = Utility.GetElementValueBasedOnElementConfig(elementconfig.Elements.State214, lstStop);
                                        oEdi214shipmentdstops.ZipCode = Utility.GetElementValueBasedOnElementConfig(elementconfig.Elements.ZipCode214, lstStop);
                                        oEdi214shipmentdstops.CountryCode = Utility.GetElementValueBasedOnElementConfig(elementconfig.Elements.CountryCode214, lstStop);
                                        oEdi214shipmentdstops.PurchaseOrderNumber = Utility.GetElementValueBasedOnElementConfig(elementconfig.Elements.PurchaseOrderNumber214, lstStop);

                                        sbMessage.Append("StopNumber = " + oEdi214shipmentdstops.StopNumber);
                                        sbMessage.AppendLine();
                                        sbMessage.Append("PurchaseOrderNumber = " + oEdi214shipmentdstops.PurchaseOrderNumber);
                                        sbMessage.AppendLine();
                                        

                                        oEdi214shipmentdstops.Insert214ShipmentStops(iShpno, oEdi214shipmentdstops);

                                    }

                                }

                                EmailManager oEmailManager = new EmailManager();

                                EmailSettings.SMTPServer = oConfiguration.SmtpHost;
                                EmailSettings.SMTPPort = BusinessUtility.GetInt(oConfiguration.SmtpPort);
                                EmailSettings.SMTPUserEmail = oConfiguration.SmtpUid;
                                EmailSettings.SMTPPassword = oConfiguration.SmtpPass;
                                EmailSettings.IsAsynchronousMail = true;
                                EmailSettings.UseDefaultCredentials = true;


                                oConfiguration.BodyMessage = oConfiguration.BodyMessage + sbMessage.ToString();
                                if (EmailHelper.SendEmail(oConfiguration.FromEmailAddress, oConfiguration.ToEmailAddress, oConfiguration.BodyMessage, oConfiguration.EmailSubject, false))
                                {
                                    oEdi214ShipmentDetail.UpdateStaus(iShpno);
                                }
                            }



                        }


                    }
                }
            }
            catch (Exception ex)
            {
                EdiError.LogError(m_iFileType.ToString(), m_iFileId.ToString(), "", ex, "Error in StartProcessing");
            }

        }


        public void FilterShipmentStops(List<string> olst)
        {
         
            bool bHeaderFound = false;
            bool bStopsFound = false;
            int i = 0;
            foreach (string s in olst)
            {
                string[] sSplitedElement = s.Split('*');
               if(!bHeaderFound)
                {
                    lstHeader.Add(s);
                }
                if (sSplitedElement[0] == "LX")
                {
                    bHeaderFound = true;
                    bStopsFound = true;
                    lstStops = new List<string>();

                }
                if (sSplitedElement[0] == "SPO")
                {
                    lstStops.Add(s);
                    bHeaderFound = true;
                    bStopsFound = false;

                    m_dictItem.Add(i, lstStops);
                    lstStops = new List<string>();
                    i++;

                }
                if (bStopsFound== true)
                {
                    lstStops.Add(s);
                }

            }
        }







        //Seperate file st se
        public void StartProcessing()
        {
            bool bStartBlockFound = false;
            bool bEndBlockFound = false;
            bool bInterchangeControlTrailer = false;
            string sSegment = string.Empty;
            int iNoOfblock = 0;
            bool bInEndBlock = false;
            try
            {
                for (int i = 0; i < m_sArr.Length; i++)
                {
                    sSegment = BusinessUtility.GetString(m_sArr[i]);
                    bStartBlockFound = sSegment.Substring(0, 2).Equals(m_sStartBlock);
                    bEndBlockFound = sSegment.Substring(0, 2).Equals(m_sEndBlock);
                    bInterchangeControlTrailer = sSegment.Substring(0, 3).Equals(m_sInterchangeControlTrailer);

                    if (!bStartBlockFound)
                    {
                        m_arl.Add(sSegment);
                    }
                    if (bInEndBlock)
                    {
                        m_arl.Add(sSegment);
                        bInEndBlock = false;
                    }
                    if (bStartBlockFound && iNoOfblock == 0)
                    {

                        if (!m_dict.ContainsKey(iNoOfblock))
                        {
                            m_dict.Add(iNoOfblock, m_arl);
                        }
                        else
                        {
                            m_dict.Add(iNoOfblock + 1, m_arl);
                        }
                        iNoOfblock++;
                        m_arl = new ArrayList();
                        m_arl.Add(sSegment);
                    }
                    else if (bEndBlockFound)
                    {
                        bInEndBlock = true;
                        if (!m_dict.ContainsKey(iNoOfblock))
                        {
                            m_dict.Add(iNoOfblock, m_arl);
                        }
                        else
                        {
                            m_dict.Add(iNoOfblock + 1, m_arl);
                        }
                        iNoOfblock++;
                        m_arl = new ArrayList();
                        //m_arl.Add(sSegment);
                    }
                    else if (bInterchangeControlTrailer)
                    {
                        if (!m_dict.ContainsKey(iNoOfblock))
                        {
                            m_dict.Add(iNoOfblock, m_arl);
                        }
                        else
                        {
                            m_dict.Add(iNoOfblock + 1, m_arl);
                        }
                        iNoOfblock++;
                        bInEndBlock = false;
                        m_arl = new ArrayList();
                    }


                }
            }
            catch (Exception ex)
            {
                EdiError.LogError(m_iFileType.ToString(), m_iFileId.ToString(), "", ex, "Error in StartProcessing");
            }

        } // start processing end here..







    }
}
