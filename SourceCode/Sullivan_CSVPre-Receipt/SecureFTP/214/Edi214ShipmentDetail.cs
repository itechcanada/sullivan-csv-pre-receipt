﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Text;

namespace SecureFTP._214
{
   public class Edi214ShipmentDetail
    {

        StringBuilder m_sbSql = null;
        string m_sSql = string.Empty;
        static string sMysqlConnectionString = ConfigurationManager.ConnectionStrings["NewConnectionString"].ToString();

       public string    TransactionSetIdentifierCode            { get; set; }
       public string    TransactionSetControlNumber             { get; set; }
       public string    CarrierOrderNumber                      { get; set; }
       public string    ShipmentId                              { get; set; }
       public string    CarrierAlphaCode                        { get; set; }
       public string    OrderlevelBillofLadingBM                { get; set; }
       public string    OrderlevelConsigneeReferenceNumberCR    { get; set; }
       public string    EntityIdentifierCode                    { get; set; }
       public string    EntityName                              { get; set; }
       public string    IdentificationCodeQualifier             { get; set; }
       public string    IdentificationCode                      { get; set; }
       public string    Address                                 { get; set; }
       public string    City                                    { get; set; }
       public string    State                                   { get; set; }
       public string    ZipCode                                 { get; set; }
       public string    CountryCode                             { get; set; }


        public int Insert214ShipmentDEtail(int iFileId, Edi214ShipmentDetail oEdi214ShipmentDetail)
        {
            int iStatus = 0;
            m_sbSql = new StringBuilder();
            iTECH.Library.DataAccess.MySql.DbHelper oDbHelper = new iTECH.Library.DataAccess.MySql.DbHelper(sMysqlConnectionString, true);
            try
            {
                m_sbSql.Append(" INSERT INTO edi214shipmentdetail (FileID, TransactionSetIdentifierCode, TransactionSetControlNumber, CarrierOrderNumber, ShipmentId, CarrierAlphaCode, OrderlevelBillofLadingBM, ");
                m_sbSql.Append(" OrderlevelConsigneeReferenceNumberCR, EntityIdentifierCode, EntityName, IdentificationCodeQualifier , IdentificationCode, Address, City, State , ZipCode , CountryCode ) ");
                m_sbSql.Append(" VALUES ( " +iFileId+ " , '"+oEdi214ShipmentDetail.TransactionSetIdentifierCode + "' , '" + oEdi214ShipmentDetail.TransactionSetControlNumber + "'  ");
                m_sbSql.Append(" , '" + oEdi214ShipmentDetail.CarrierOrderNumber + "' , '" + oEdi214ShipmentDetail.ShipmentId + "' , '" + oEdi214ShipmentDetail.CarrierAlphaCode + "' , '" + oEdi214ShipmentDetail.OrderlevelBillofLadingBM + "' ");
                m_sbSql.Append(" , '" + oEdi214ShipmentDetail.OrderlevelConsigneeReferenceNumberCR + "' , '" + oEdi214ShipmentDetail.EntityIdentifierCode + "' , '" + oEdi214ShipmentDetail.EntityName + "' , '" + oEdi214ShipmentDetail.IdentificationCodeQualifier + "' ");
                m_sbSql.Append(" , '" + oEdi214ShipmentDetail.IdentificationCode + "' , '" + oEdi214ShipmentDetail.Address + "' , '" + oEdi214ShipmentDetail.City + "' ");
                m_sbSql.Append(" , '" + oEdi214ShipmentDetail.State + "' , '" + oEdi214ShipmentDetail.ZipCode + "' , '" + oEdi214ShipmentDetail.CountryCode + "' ) ");

                MySqlParameter[] oMySqlParameter = { };
                iStatus = oDbHelper.ExecuteNonQuery(m_sbSql.ToString(), CommandType.Text, oMySqlParameter);
                iStatus = oDbHelper.GetLastInsertID();
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLog(ex);
            }
            finally
            {
                oDbHelper.CloseDatabaseConnection();
            }
            return iStatus;
        }

        public int UpdateStaus(int iShipmentID)
        {
            int iStatus = 0;
            m_sbSql = new StringBuilder();
            iTECH.Library.DataAccess.MySql.DbHelper oDbHelper = new iTECH.Library.DataAccess.MySql.DbHelper(sMysqlConnectionString, true);
            try
            {
                m_sbSql.Append(" UPDATE edi214shipmentdetail SET MailSent = 1 WHERE Id = " + iShipmentID);

                MySqlParameter[] oMySqlParameter = { };
                iStatus = oDbHelper.ExecuteNonQuery(m_sbSql.ToString(), CommandType.Text, oMySqlParameter);
                iStatus = oDbHelper.GetLastInsertID();
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLog(ex);
            }
            finally
            {
                oDbHelper.CloseDatabaseConnection();
            }
            return iStatus;
        }



    }
}
