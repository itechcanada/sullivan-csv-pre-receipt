﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Text;
using iTECH.Library.Utilities;

namespace SecureFTP
{
    class EdiDataSource
    {

        string m_sSql = string.Empty;
        static string sMysqlConnectionString = ConfigurationManager.ConnectionStrings["NewConnectionString"].ToString();
        private string EdiFile = string.Empty;


        #region properties

        string sVendorId = string.Empty;
        public string VendorId
        {
            get { return sVendorId; }
            set { sVendorId = value; }
        }


        string sEdiFileType = string.Empty;
        public string EdiFileType
        {
            get { return sEdiFileType; }
            set { sEdiFileType = value; }
        }

        int iEdiDataSourceId = 0;
        int EdiDataSourceId
        {
            get { return iEdiDataSourceId; }
            set { iEdiDataSourceId = value; }
        }

        string sEdiDataSourceName = string.Empty;
        public string EdiDataSourceName
        {
            get { return sEdiDataSourceName; }
            set { sEdiDataSourceName = value; }
        }

        string sHost = string.Empty;
        public string Host
        {
            get { return sHost; }
            set { sHost = value; }
        }

        string sUserId = string.Empty;
        public string UserId
        {
            get { return sUserId; }
            set { sUserId = value; }
        }

        string sPassword = string.Empty;
        public string Password
        {
            get { return sPassword; }
            set { sPassword = value; }
        }

        string sPort = string.Empty;
        public string Port
        {
            get { return sPort; }
            set { sPort = value; }
        }

        string sDataSourceDirectory = string.Empty;
        public string DataSourceDirectory
        {
            get { return sDataSourceDirectory; }
            set { sDataSourceDirectory = value; }
        }

        string sDataProcessedDirectory = string.Empty;
        public string DataProcessedDirectory
        {
            get { return sDataProcessedDirectory; }
            set { sDataProcessedDirectory = value; }
        }

        string sDataUnProcessedDirectory = string.Empty;
        public string DataUnProcessedDirectory
        {
            get { return sDataUnProcessedDirectory; }
            set { sDataUnProcessedDirectory = value; }
        }

        string sDataUploadDirectory = string.Empty;
        public string DataUploadDirectory
        {
            get { return sDataUploadDirectory; }
            set { sDataUploadDirectory = value; }
        }

        string sDataDownloadDirectory = string.Empty;
        public string DataDownloadDirectory
        {
            get { return sDataDownloadDirectory; }
            set { sDataDownloadDirectory = value; }
        }

        string sEdiDataSourceTypeProperty = string.Empty;
        public string EdiDataSourceTypeProperty
        {
            get { return sEdiDataSourceTypeProperty; }
            set { sEdiDataSourceTypeProperty = value; }
        }

        string sAck997SftpDirectory = string.Empty;
        public string Ack997SftpDirectory
        {
            get { return sAck997SftpDirectory; }
            set { sAck997SftpDirectory = value; }
        }

        string sAck997LocalDirectory = string.Empty;
        public string Ack997LocalDirectory
        {
            get { return sAck997LocalDirectory; }
            set { sAck997LocalDirectory = value; }
        }
        


        bool bActive = false;
        public bool Active
        {
            get { return bActive; }
            set { bActive = value; }
        }

        #endregion

        public struct EdiDataSourceType
        {
            public const string LocalDisk = "1";
            public const string SFTP = "2";
            public const string Email = "3";
            public const string NetworkPC = "4";
            public const string FTP = "5";
        }


        public EdiDataSource()
        {

        }

        // parametrised constructor to get edi origin zone
        public EdiDataSource(int iEdiDataSourceId)
        {
            EdiDataSourceId = iEdiDataSourceId;
            m_sSql = "SELECT * FROM edidatasource WHERE EdiDataSourceId = '" + EdiDataSourceId + "' AND Active = 1";

            iTECH.Library.DataAccess.MySql.DbHelper oDbHelper = new iTECH.Library.DataAccess.MySql.DbHelper(sMysqlConnectionString, true);

            try
            {
                MySqlParameter[] oMySqlParameter = {};
                DataTable oDtAddress = new iTECH.Library.DataAccess.MySql.DbHelper(sMysqlConnectionString, true).GetDataTable(m_sSql, CommandType.Text, oMySqlParameter);
                foreach (DataRow oDataRow in oDtAddress.Rows)
                {
                    sEdiDataSourceName = BusinessUtility.GetString(oDataRow["EdiDataSourceName"]);
                    bActive = BusinessUtility.GetBool(oDataRow["Active"]);
                    sHost = BusinessUtility.GetString(oDataRow["Host"]);
                    sUserId = BusinessUtility.GetString(oDataRow["UserId"]);
                    sPassword = BusinessUtility.GetString(oDataRow["Password"]);
                    sPort = BusinessUtility.GetString(oDataRow["Port"]);
                    sDataSourceDirectory = BusinessUtility.GetString(oDataRow["DataSourceDirectory"]);
                    sDataProcessedDirectory = BusinessUtility.GetString(oDataRow["DataProcessedDirectory"]);
                    sDataUnProcessedDirectory = BusinessUtility.GetString(oDataRow["DataUnProcessedDirectory"]);
                    sDataUploadDirectory = BusinessUtility.GetString(oDataRow["DataUploadDirectory"]);
                    sEdiDataSourceTypeProperty = BusinessUtility.GetString(oDataRow["EdiDataSourceType"]);
                    sDataDownloadDirectory = BusinessUtility.GetString(oDataRow["DataDownloadDirectory"]);

                    sAck997SftpDirectory = BusinessUtility.GetString(oDataRow["Ack997SftpDirectory"]);
                    sAck997LocalDirectory = BusinessUtility.GetString(oDataRow["Ack997LocalDirectory"]);
                }
            }
            catch (Exception ex)
            {
                ErrorLog.createLog("Error in Configuration : " + ex.ToString());
                ErrorLog.CreateLog(ex);
            }
            finally
            {
                oDbHelper.CloseDatabaseConnection();
            }
        }



    }
}