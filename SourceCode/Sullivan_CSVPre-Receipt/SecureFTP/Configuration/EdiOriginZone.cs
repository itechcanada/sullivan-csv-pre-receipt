﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Text;
using iTECH.Library.Utilities;


namespace SecureFTP
{
    class EdiOriginZone
    {

        string m_sSql = string.Empty;
        static string sMysqlConnectionString = ConfigurationManager.ConnectionStrings["NewConnectionString"].ToString();
        private string EdiFile = string.Empty;


        #region properties

        string sVendorId = string.Empty;
        public string VendorId
        {
            get { return sVendorId; }
            set { sVendorId = value; }
        }


        string sFileType = string.Empty;
        public string FileType
        {
            get { return sFileType; }
            set { sFileType = value; }
        }

        string sVendorName = string.Empty;
        public string VendorName
        {
            get { return sVendorName; }
            set { sVendorName = value; }
        }

        bool bActive = false;
        public bool Active
        {
            get { return bActive; }
            set { bActive = value; }
        }

        string sDunsNumber = string.Empty;
        public string DunsNumber
        {
            get { return sDunsNumber; }
            set { sDunsNumber = value; }
        }

        string sStratixMillCode = string.Empty;
        public string StratixMillCode
        {
            get { return sStratixMillCode; }
            set { sStratixMillCode = value; }
        }

        string sHeatCode = string.Empty;
        public string HeatCode
        {
            get { return sHeatCode; }
            set { sHeatCode = value; }
        }

        string sMeltOrigin = string.Empty;
        public string MeltOrigin
        {
            get { return sMeltOrigin; }
            set { sMeltOrigin = value; }
        }

        string sOriginZone = string.Empty;
        public string OriginZone
        {
            get { return sOriginZone; }
            set { sOriginZone = value; }
        }

        string sStratixOriginZone = string.Empty;
        public string StratixOriginZone
        {
            get { return sStratixOriginZone; }
            set { sStratixOriginZone = value; }
        }

        #endregion


        public EdiOriginZone()
        {

        }


        // parametrised constructor to get edi origin zone
        public EdiOriginZone(string sVendorDunsNo, string sVendorId, string sStratixMillCode, string sHeatCode)
        {
            m_sSql = "SELECT * FROM edioriginzone WHERE DunsNumber = '" + sVendorDunsNo + "' AND VendorId = '" + sVendorId + "' AND StratixMillCode = '" + sStratixMillCode + "' AND HeatCode = '" + sHeatCode + "' AND Active = 1";
            ErrorLog.createLog(m_sSql);
            iTECH.Library.DataAccess.MySql.DbHelper oDbHelper = new iTECH.Library.DataAccess.MySql.DbHelper(sMysqlConnectionString, true);

            try
            {
                MySqlParameter[] oMySqlParameter = {};
                DataTable oDtAddress = new iTECH.Library.DataAccess.MySql.DbHelper(sMysqlConnectionString, true).GetDataTable(m_sSql, CommandType.Text, oMySqlParameter);
                foreach (DataRow oDataRow in oDtAddress.Rows)
                {
                    sVendorName = BusinessUtility.GetString(oDataRow["VendorName"]);
                    bActive = BusinessUtility.GetBool(oDataRow["Active"]);
                    sMeltOrigin = BusinessUtility.GetString(oDataRow["MeltOrigin"]);
                    sOriginZone = BusinessUtility.GetString(oDataRow["OriginZone"]);
                    sStratixOriginZone = BusinessUtility.GetString(oDataRow["StratixOriginZone"]);
                }
            }
            catch (Exception ex)
            {
                ErrorLog.createLog("Error in Configuration : " + ex.ToString());
                ErrorLog.CreateLog(ex);
            }
            finally
            {
                oDbHelper.CloseDatabaseConnection();
            }
        }






    }
}
