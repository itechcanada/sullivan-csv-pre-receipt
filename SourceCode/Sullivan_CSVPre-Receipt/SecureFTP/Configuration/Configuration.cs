﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Text;
using iTECH.Library.Utilities;

namespace SecureFTP
{
    class Configuration
    {
         string m_sSql = string.Empty;
        static string sMysqlConnectionString = ConfigurationManager.ConnectionStrings["NewConnectionString"].ToString();
        private string EdiFile = string.Empty;

        private static string SearchVendorName = ConfigurationManager.AppSettings["SearchVendorName"];
        private static string SearchFileType = ConfigurationManager.AppSettings["SearchFileType"];

        #region to check vendors -- to process only selected vendorlist without checking active flag   2020/03/16    Sumit
        string IsVendorFilter = Utility.GetConfigValue("IsVendorFilter");      
        string FilteredVendorConfig = Utility.GetConfigValue("VendorDunsList");   
        string[] VendorArr;
        string FilteredVendorDB = string.Empty;
        #endregion to check vendors -- to process only selected vendorlist   2020/03/16 Sumit

        #region properties

        string sVendorId = string.Empty;
        public string VendorId
        {
            get { return sVendorId; }
            set { sVendorId = value; }
        }


        string sFileType = string.Empty;
        public string FileType
        {
            get { return sFileType; }
            set { sFileType = value; }
        }

        string sVendorName = string.Empty;
        public string VendorName
        {
            get { return sVendorName; }
            set { sVendorName = value; }
        }

        string sMillId = string.Empty;
        public string MillId
        {
            get { return sMillId; }
            set { sMillId = value; }
        }

        bool bActive = false;
        public bool Active
        {
            get { return bActive; }
            set { bActive = value; }
        }


        private static string sCompanyId = string.Empty;
        public string CompanyId
        {
            get { return sCompanyId; }
        }

        private static string sConnectedAccessType = string.Empty;
        public string ConnectedAccessType
        {
            get { return sConnectedAccessType; }
        }

        private static string sEnvironmentClass = string.Empty;
        public string EnvironmentClass
        {
            get { return sEnvironmentClass; }
        }

        private static string sEnvironmentName = string.Empty;
        public string EnvironmentName
        {
            get { return sEnvironmentName; }
        }

        private static string sUsername = string.Empty;
        public string Username
        {
            get { return sUsername; }
        }

        private static string sPassword = string.Empty;
        public string Password
        {
            get { return sPassword; }
        }

        private static string sLineSeperator = string.Empty;
        public string LineSeperator
        {
            get { return sLineSeperator; }
        }

        ///<summary>
        ///This Property hold type of current vendor edi file.
        ///</summary>
        private string sEDIFileType = string.Empty;
        public string EDIFileType
        {
            get { return sEDIFileType; }
        }

        string sCustomerName = string.Empty;
        public string CustomerName
        {
            get { return sCustomerName; }
            set { sCustomerName = value; }
        }

        string sPrereceiptStatus = string.Empty;
        public string PrereceiptStatus
        {
            get { return sPrereceiptStatus; }
            set { sPrereceiptStatus = value; }
        }

        string sDimDesign = string.Empty;
        public string DimDesign
        {
            get { return sDimDesign; }
            set { sDimDesign = value; }
        }

        string sVendorDunsNo = string.Empty;
        public string VendorDunsNo
        {
            get { return sVendorDunsNo; }
            set { sVendorDunsNo = value; }
        }

        bool bSkipSameVendorTagID = false;
        public bool SkipSameVendorTagID
        {
            get { return bSkipSameVendorTagID; }
            set { bSkipSameVendorTagID = value; }
        }

        int iFileProcessingType = 0;
        public int FileProcessingType
        {
            get { return iFileProcessingType; }
            set { iFileProcessingType = value; }
        }

        string bQdsApproved = string.Empty;
        public string QdsApproved
        {
            get { return bQdsApproved; }
            set { bQdsApproved = value; }
        }

        int iEdiDataSourceId = 0;
        public int EdiDataSourceId
        {
            get { return iEdiDataSourceId; }
            set { iEdiDataSourceId = value; }
        }

        int iTimeDelayInHour = 0;
        public int TimeDelayInHour
        {
            get { return iTimeDelayInHour; }
            set { iTimeDelayInHour = value; }
        }

        bool bCheckQDS = false;
        public bool CheckQDS
        {
            get { return bCheckQDS; }
        }

        bool bActivate856 = false;
        public bool Activate856
        {
            get { return bActivate856; }
        }

        bool bActivate863 = false;
        public bool Activate863
        {
            get { return bActivate863; }
        }

        string sAckText = string.Empty;
        public string AckText
        {
            get { return sAckText; }
            set { sAckText = value; }
        }

        bool bActive997 = false;
         public bool Active997
        {
            get { return bActive997; }
        }

        bool bActivateMultiplePoItem = false;
        public bool ActivateMultiplePoItem
        {
            get { return bActivateMultiplePoItem; }
        }

        string sApproveQds = string.Empty;
        public string ApproveQds
        {
            get { return sApproveQds; }
            set { sApproveQds = value; }
        }

        bool bPrependDunsInAckFileName = false;
        public bool PrependDunsInAckFileName
        {
            get { return bPrependDunsInAckFileName; }
        }

        bool bSingleFileFor856863 = false;
        public bool SingleFileFor856863
        {
            get { return bSingleFileFor856863; }
        }

        string sSmtpHost = string.Empty;
        public string SmtpHost
        {
            get { return sSmtpHost; }
            set { sSmtpHost = value; }
        }


        string sSmtpUid = string.Empty;
        public string SmtpUid
        {
            get { return sSmtpUid; }
            set { sSmtpUid = value; }
        }

        string sSmtpPass = string.Empty;
        public string SmtpPass
        {
            get { return sSmtpPass; }
            set { sSmtpPass = value; }
        }

        string sSmtpPort = string.Empty;
        public string SmtpPort
        {
            get { return sSmtpPort; }
            set { sSmtpPort = value; }
        }

        string sFromEmailAddress = string.Empty;
        public string FromEmailAddress
        {
            get { return sFromEmailAddress; }
            set { sFromEmailAddress = value; }
        }

        string sToEmailAddress = string.Empty;
        public string ToEmailAddress
        {
            get { return sToEmailAddress; }
            set { sToEmailAddress = value; }
        }

        string sBodyMessage = string.Empty;
        public string BodyMessage
        {
            get { return sBodyMessage; }
            set { sBodyMessage = value; }
        }

        string sEmailSubject = string.Empty;
        public string EmailSubject
        {
            get { return sEmailSubject; }
            set { sEmailSubject = value; }
        }









        #endregion properties



        // Sets application environment
        public static string ApplicationMode = ApplicationType.PRODUCTION;

        public struct ApplicationType
        {
            public const string PRODUCTION = "PRODUCTION";
            public const string QC = "QC";
            public const string LOCAL = "LOCAL";
        }

        public struct EdiFileType
        {
            public const string EDI856 = "856";
            public const string EDI863 = "863";
            public const string EDI990 = "990";
            public const string EDI214 = "214";
            public const string EDI210 = "210";
            public const string EDI856863 = "856863";
        }

        public struct EdiFileProcessingType
        {
            public const string DEFAULT = "0";
            public const string ONERECEIPTPERPO = "1";
            public const string ONERECEIPTPERFILE = "2";
        }


        //static string m_sFilteType = string.Empty;
        //static string m_sVendorName = string.Empty;

        public Configuration()
        {
            this.SetStratixUser();
        }

        ///<summary>
        ///This Function populate vendor detail like duns no.
        ///</summary>
        public Configuration(string sDunsNo, int iFileType)
        {
            elementconfig.PopulateElementConfig(sDunsNo, iFileType);
            sFileType = iFileType.ToString();
            sVendorDunsNo = sDunsNo;
            m_sSql = "SELECT * FROM configuration WHERE VendorDunsNo = '" + sVendorDunsNo + "' AND FileType = '" + iFileType + "' AND Active = 1";

            #region to process only selected vendorlist without checking active flag    2020/03/16  Sumit
            if (!string.IsNullOrEmpty(IsVendorFilter) && BusinessUtility.GetInt(IsVendorFilter) == 1)
            {
                if (!string.IsNullOrEmpty(FilteredVendorConfig))
                {
                    VendorArr = FilteredVendorConfig.Split(',');
                    FilteredVendorDB = "";
                    for (int i = 0; i < VendorArr.Length; i++)
                    {
                        if (i == 0)
                            FilteredVendorDB = "'" + Convert.ToString(VendorArr[i]) + "'";
                        else
                            FilteredVendorDB = FilteredVendorDB + "," + "'" + Convert.ToString(VendorArr[i]) + "'";
                    }
                    m_sSql = "SELECT * FROM configuration WHERE VendorDunsNo in (" + FilteredVendorDB + ") and VendorDunsNo = '" + sVendorDunsNo + "' AND FileType = '" + iFileType + "'";
                }

            }
            #endregion to process only selected vendorlist without checking active flag    2020/03/16  Sumit    

            iTECH.Library.DataAccess.MySql.DbHelper oDbHelper = new iTECH.Library.DataAccess.MySql.DbHelper(sMysqlConnectionString, true);

            try
            {
                MySqlParameter[] oMySqlParameter = { iTECH.Library.DataAccess.MySql.DbUtility.GetParameter("VendorDunsNo", sVendorDunsNo, iTECH.Library.DataAccess.MySql.MyDbType.String),
                                                 iTECH.Library.DataAccess.MySql.DbUtility.GetParameter("FileType", iFileType, iTECH.Library.DataAccess.MySql.MyDbType.Int)
                                               };

                DataTable oDtAddress = oDbHelper.GetDataTable(m_sSql, CommandType.Text, oMySqlParameter);
                foreach (DataRow oDataRow in oDtAddress.Rows)
                {
                    sVendorName = BusinessUtility.GetString(oDataRow["VendorName"]);
                    bActive = BusinessUtility.GetBool(oDataRow["Active"]);
                    sMillId = BusinessUtility.GetString(oDataRow["MillId"]);
                    sLineSeperator = BusinessUtility.GetString(oDataRow["LineSeperator"]);
                    bActive = BusinessUtility.GetBool(oDataRow["Active"]);
                    sVendorId = BusinessUtility.GetString(oDataRow["vendorId"]);
                    sCustomerName = BusinessUtility.GetString(oDataRow["CustomerName"]);
                    sPrereceiptStatus = BusinessUtility.GetString(oDataRow["PrereceiptStatus"]);
                    sDimDesign = BusinessUtility.GetString(oDataRow["DimDesign"]);
                    sEDIFileType = BusinessUtility.GetString(oDataRow["FileType"]);

                    // Edi 863 properties
                    sCompanyId = BusinessUtility.GetString(oDataRow["CompanyId"]);
                    sConnectedAccessType = BusinessUtility.GetString(oDataRow["ConnectedAccessType"]);
                    sEnvironmentClass = BusinessUtility.GetString(oDataRow["EnvironmentClass"]);
                    sEnvironmentName = BusinessUtility.GetString(oDataRow["EnvironmentName"]);
                    sUsername = BusinessUtility.GetString(oDataRow["Username"]);
                    sPassword = BusinessUtility.GetString(oDataRow["Password"]);
                    bSkipSameVendorTagID = BusinessUtility.GetBool(oDataRow["SkipSameVendorTagID"]);
                    bQdsApproved = BusinessUtility.GetString(oDataRow["QdsApproved"]);
                    iFileProcessingType = BusinessUtility.GetInt(oDataRow["FileProcessingType"]);
                    iEdiDataSourceId = BusinessUtility.GetInt(oDataRow["EdiDataSourceId"]);
                    iTimeDelayInHour = BusinessUtility.GetInt(oDataRow["TimeDelayInHour"]);
                    bCheckQDS = BusinessUtility.GetBool(oDataRow["CheckQDS"]);
                    bActivate856 = BusinessUtility.GetBool(oDataRow["Activate856"]);
                    bActivate863 = BusinessUtility.GetBool(oDataRow["Activate863"]);
                    bActivateMultiplePoItem = BusinessUtility.GetBool(oDataRow["ActivateMultiplePoItem"]);
                    ApproveQds = BusinessUtility.GetString(oDataRow["ApproveQds"]);
                    bPrependDunsInAckFileName = BusinessUtility.GetBool(oDataRow["PrependDunsInAckFileName"]);
                    bSingleFileFor856863  = BusinessUtility.GetBool(oDataRow["SingleFileFor856863"]);

                    sSmtpHost = BusinessUtility.GetString(oDataRow["SmtpHost"]);
                    sSmtpUid = BusinessUtility.GetString(oDataRow["SmtpUid"]);
                    sSmtpPass = BusinessUtility.GetString(oDataRow["SmtpPass"]);
                    sSmtpPort = BusinessUtility.GetString(oDataRow["SmtpPort"]);
                    sFromEmailAddress = BusinessUtility.GetString(oDataRow["FromEmailAddress"]);
                    sToEmailAddress = BusinessUtility.GetString(oDataRow["ToEmailAddress"]);
                    sBodyMessage = BusinessUtility.GetString(oDataRow["BodyMessage"]);
                    sEmailSubject = BusinessUtility.GetString(oDataRow["EmailSubject"]);
                }
            }
            catch (Exception ex)
            {
                ErrorLog.createLog("Error in Configuration : " + ex.ToString());
                ErrorLog.CreateLog(ex);
            }
            finally
            {
                oDbHelper.CloseDatabaseConnection();
            }
        }

        public struct VENDORSEARCHTEXT
        {
            public const string NLMK = "N1*SF*NLMK";
            public const string NLMK1 = "N1*SU*NLMK";
            public const string NUCOR = "N1*SF*NUCOR";
            public const string NUCOR863 = "N1*MF*NUCOR";
            public const string TIMKON = "N1*SF*TIMKEN";
            public const string TIMKON1 = "N1*BY*SULLIVAN";
        }

        public struct VENDORTYPE
        {
            public const string NLMK = "NLMK";
            public const string NUCOR = "NUCOR";
            public const string TIMKON = "TIMKEN";
            public const string NUCORDECATUR  = "696";
            public const string NUCORHICKMAN = "502";
            public const string NUCORBERKELEY = "568";
            public const string NLMK1 = "2001";
            public const string NUCORCRAWFORDSVILLE = "381";
        }

        public struct FILESEARCHTEXT
        {
            public const string EDI856 = "ST*856*";
            public const string EDI863 = "ST*863*";
            public const string EDI990 = "ST*990*";
            public const string EDI214 = "ST*214*";
            public const string EDI210 = "ST*210*";
        }

        private void SetFileAndVendor(string[] sArr)
        {
            string[] sSearchVendorNameArr = SearchVendorName.Split(',');
            string[] sSearchFileTypeArr = SearchFileType.Split(',');
            string sDunsNo = string.Empty;
            bool bFound = false;
            if (sArr.Length == 1)
            {
                string sContent = sArr[0];
                if (sSearchFileTypeArr.Length > 0)
                {
                    foreach (string s in sSearchFileTypeArr)
                    {
                        bFound = sContent.Contains(s);
                        if (bFound)
                        {
                            if (s.Equals(FILESEARCHTEXT.EDI856))
                            {
                                sFileType = EdiFileType.EDI856;
                            }
                            else if (s.Equals(FILESEARCHTEXT.EDI863))
                            {
                                sFileType = EdiFileType.EDI863;
                            }
                            else if (s.Equals(FILESEARCHTEXT.EDI990))
                            {
                                sFileType = EdiFileType.EDI990;
                            }
                            else if (s.Equals(FILESEARCHTEXT.EDI214))
                            {
                                sFileType = EdiFileType.EDI214;
                            }
                            else if (s.Equals(FILESEARCHTEXT.EDI210))
                            {
                                sFileType = EdiFileType.EDI210;
                            }
                        }

                    }

                    EdiFile = sFileType;
                    sEDIFileType = sFileType;

                }

                if (sSearchVendorNameArr.Length > 0)
                {

                    //var DunsNo = sArr.ToList<string>().FirstOrDefault(stringToCheck => stringToCheck.Contains("ISA*"));
                    //if (DunsNo != null)
                    //{
                    //    sDunsNo = DunsNo.Split('*')[6];
                    //}
                    //sVendorDunsNo = sDunsNo;

                    //foreach (string s in sSearchVendorNameArr)
                    //{
                    //    bFound = sContent.Contains(s);
                    //    if (bFound)
                    //    {
                    //        if (s.Equals(VENDORSEARCHTEXT.NLMK))
                    //        {
                    //            sVendorName = VENDORTYPE.NLMK;
                    //        }
                    //        else if (s.Equals(VENDORSEARCHTEXT.NUCOR))
                    //        {
                    //            sVendorName = VENDORTYPE.NUCOR;
                    //        }
                    //        else if (s.Equals(VENDORSEARCHTEXT.NUCOR863))
                    //        {
                    //            sVendorName = VENDORTYPE.NUCOR;
                    //        }
                    //        else if (s.Equals(VENDORSEARCHTEXT.TIMKON))
                    //        {
                    //            sVendorName = VENDORTYPE.TIMKON;
                    //        }
                    //        else if (s.Equals(VENDORSEARCHTEXT.NLMK1))
                    //        {
                    //            sVendorName = VENDORTYPE.NLMK;
                    //        }
                    //        else if (s.Equals(VENDORSEARCHTEXT.TIMKON1))
                    //        {
                    //            sVendorName = VENDORTYPE.TIMKON;
                    //        }
                            
                    //    }
                    //}
                }
            }
            else
            {
                // If file is already splited handle here..
                var results = (String[])null;

                //var DunsNo = sArr.ToList<string>().FirstOrDefault(stringToCheck => stringToCheck.Contains("ISA*"));
                //if (DunsNo != null)
                //{
                //    sDunsNo = DunsNo.Split('*')[6];
                //}
                //sVendorDunsNo = sDunsNo;
                //foreach (string ss in sSearchVendorNameArr)
                //{
                //    results = Array.FindAll(sArr, s => s.StartsWith(ss));
                //    if (results.Length > 0)
                //    {
                //        if (ss.Equals(VENDORSEARCHTEXT.NLMK))
                //        {
                //            sVendorName = VENDORTYPE.NLMK;
                //        }
                //        else if (ss.Equals(VENDORSEARCHTEXT.NUCOR))
                //        {
                //            sVendorName = VENDORTYPE.NUCOR;
                //        }
                //        else if (ss.Equals(VENDORSEARCHTEXT.NUCOR863))
                //        {
                //            sVendorName = VENDORTYPE.NUCOR;
                //        }
                //        else if (ss.Equals(VENDORSEARCHTEXT.TIMKON))
                //        {
                //            sVendorName = VENDORTYPE.TIMKON;
                //        }
                //        else if (ss.Equals(VENDORSEARCHTEXT.NLMK1))
                //        {
                //            sVendorName = VENDORTYPE.NLMK;
                //        }
                //        else if (ss.Equals(VENDORSEARCHTEXT.TIMKON1))
                //        {
                //            sVendorName = VENDORTYPE.TIMKON;
                //        }
                //    }
                //}


                foreach (string ss in sSearchFileTypeArr)
                {
                    results = Array.FindAll(sArr, s => s.StartsWith(ss));
                    if (results.Length > 0)
                    {
                        if (ss.Equals(FILESEARCHTEXT.EDI856))
                        {
                            sFileType = EdiFileType.EDI856;
                        }
                        else if (ss.Equals(FILESEARCHTEXT.EDI863))
                        {
                            sFileType = EdiFileType.EDI863;
                        }
                        else if (ss.Equals(FILESEARCHTEXT.EDI990))
                        {
                            sFileType = EdiFileType.EDI990;
                        }
                        else if (ss.Equals(FILESEARCHTEXT.EDI214))
                        {
                            sFileType = EdiFileType.EDI214;
                        }
                        else if (ss.Equals(FILESEARCHTEXT.EDI210))
                        {
                            sFileType = EdiFileType.EDI210;
                        }
                    }

                }

                EdiFile = sFileType;
                sEDIFileType = sFileType;

             
            }
        }

        public void PopulateConfigurationData(string[] sArr)
        {
            sArr = sArr.Select(s => s.ToUpperInvariant()).ToArray();  // this code convert all array element to uppercase.
            this.GetDunsNo(sArr);
            this.SetFileAndVendor(sArr);
            this.PopulateConfigurationData(sVendorDunsNo, sFileType);  
        }

        public string GetDunsNo(string[] sArr)
        {
            string sReturn = string.Empty;
            try
            {
                string sDuns = string.Empty;
                if (sArr.Length == 1)
                {
                    var DunsNo = sArr.ToList<string>().FirstOrDefault(stringToCheck => stringToCheck.Contains("ISA*"));
                    if (DunsNo != null)
                    {
                        sDuns = DunsNo.Split('*')[6].Trim();
                    }
                    
                }
                else
                {
                    var DunsNo = sArr.ToList<string>().FirstOrDefault(stringToCheck => stringToCheck.Contains("ISA*"));
                    if (DunsNo != null)
                    {
                        sDuns = DunsNo.Split('*')[6].Trim();
                    }
                }

                sReturn = sVendorDunsNo = sDuns;
            }
            catch (Exception ex)
            {

            }
            return sReturn;
        }


        ///<summary>
        ///This Function populate vendor detail like duns no.
        ///</summary>
        public void PopulateConfigurationData(string sDunsNo, string sFileType)
        {
            elementconfig.PopulateElementConfig(sDunsNo, BusinessUtility.GetInt(sFileType));
            sVendorDunsNo = sDunsNo;
            m_sSql = "SELECT * FROM configuration WHERE VendorDunsNo = '" + sDunsNo.Trim() + "' AND FileType = '" + sFileType.Trim() + "' AND Active = 1";

            #region to process only selected vendorlist without checking active flag    2020/03/16  Sumit
            if (!string.IsNullOrEmpty(IsVendorFilter) && BusinessUtility.GetInt(IsVendorFilter) == 1)
            {
                if (!string.IsNullOrEmpty(FilteredVendorConfig))
                {
                    VendorArr = FilteredVendorConfig.Split(',');
                    FilteredVendorDB = "";
                    for (int i = 0; i < VendorArr.Length; i++)
                    {
                        if (i == 0)
                            FilteredVendorDB = "'" + Convert.ToString(VendorArr[i]) + "'";
                        else
                            FilteredVendorDB = FilteredVendorDB + "," + "'" + Convert.ToString(VendorArr[i]) + "'";
                    }
                    m_sSql = "SELECT * FROM configuration WHERE VendorDunsNo in (" + FilteredVendorDB + ") and VendorDunsNo = '" + sDunsNo.Trim() + "' AND FileType = '" + sFileType.Trim() + "'";
                }

            }
            #endregion to process only selected vendorlist without checking active flag    2020/03/16  Sumit  

            iTECH.Library.DataAccess.MySql.DbHelper oDbHelper = new iTECH.Library.DataAccess.MySql.DbHelper(sMysqlConnectionString, true);

            try
            {
                MySqlParameter[] oMySqlParameter = { iTECH.Library.DataAccess.MySql.DbUtility.GetParameter("VendorDunsNo", sDunsNo, iTECH.Library.DataAccess.MySql.MyDbType.String),
                                                 iTECH.Library.DataAccess.MySql.DbUtility.GetParameter("FileType", sFileType, iTECH.Library.DataAccess.MySql.MyDbType.String)
                                               };

                DataTable oDtAddress = oDbHelper.GetDataTable(m_sSql, CommandType.Text, oMySqlParameter);
                foreach (DataRow oDataRow in oDtAddress.Rows)
                {
                    sVendorName = BusinessUtility.GetString(oDataRow["VendorName"]);
                    bActive = BusinessUtility.GetBool(oDataRow["Active"]);
                    sMillId = BusinessUtility.GetString(oDataRow["MillId"]);
                    sLineSeperator = BusinessUtility.GetString(oDataRow["LineSeperator"]);
                    sVendorId = BusinessUtility.GetString(oDataRow["vendorId"]);
                    sCustomerName = BusinessUtility.GetString(oDataRow["CustomerName"]);
                    sPrereceiptStatus = BusinessUtility.GetString(oDataRow["PrereceiptStatus"]);
                    sDimDesign = BusinessUtility.GetString(oDataRow["DimDesign"]);
                    sEDIFileType = BusinessUtility.GetString(oDataRow["FileType"]);

                    sAckText = BusinessUtility.GetString(oDataRow["AckText"]);

                    // Edi 863 properties
                    sCompanyId = BusinessUtility.GetString(oDataRow["CompanyId"]);
                    sConnectedAccessType = BusinessUtility.GetString(oDataRow["ConnectedAccessType"]);
                    sEnvironmentClass = BusinessUtility.GetString(oDataRow["EnvironmentClass"]);
                    sEnvironmentName = BusinessUtility.GetString(oDataRow["EnvironmentName"]);
                    sUsername = BusinessUtility.GetString(oDataRow["Username"]);
                    sPassword = BusinessUtility.GetString(oDataRow["Password"]);
                    bSkipSameVendorTagID = BusinessUtility.GetBool(oDataRow["SkipSameVendorTagID"]);
                    bQdsApproved = BusinessUtility.GetString(oDataRow["QdsApproved"]);
                    iFileProcessingType = BusinessUtility.GetInt(oDataRow["FileProcessingType"]);
                    iEdiDataSourceId = BusinessUtility.GetInt(oDataRow["EdiDataSourceId"]);
                    iTimeDelayInHour = BusinessUtility.GetInt(oDataRow["TimeDelayInHour"]);
                    bCheckQDS = BusinessUtility.GetBool(oDataRow["CheckQDS"]);
                    bActivate856 = BusinessUtility.GetBool(oDataRow["Activate856"]);
                    bActivate863 = BusinessUtility.GetBool(oDataRow["Activate863"]);
                    bActive997 = BusinessUtility.GetBool(oDataRow["Activate997"]);
                    bActivateMultiplePoItem = BusinessUtility.GetBool(oDataRow["ActivateMultiplePoItem"]);
                    ApproveQds = BusinessUtility.GetString(oDataRow["ApproveQds"]);
                    bPrependDunsInAckFileName = BusinessUtility.GetBool(oDataRow["PrependDunsInAckFileName"]);
                    bSingleFileFor856863 = BusinessUtility.GetBool(oDataRow["SingleFileFor856863"]);

                    sSmtpHost = BusinessUtility.GetString(oDataRow["SmtpHost"]);
                    sSmtpUid = BusinessUtility.GetString(oDataRow["SmtpUid"]);
                    sSmtpPass = BusinessUtility.GetString(oDataRow["SmtpPass"]);
                    sSmtpPort = BusinessUtility.GetString(oDataRow["SmtpPort"]);
                    sFromEmailAddress = BusinessUtility.GetString(oDataRow["FromEmailAddress"]);
                    sToEmailAddress = BusinessUtility.GetString(oDataRow["ToEmailAddress"]);
                    sBodyMessage = BusinessUtility.GetString(oDataRow["BodyMessage"]);
                    sEmailSubject = BusinessUtility.GetString(oDataRow["EmailSubject"]);
                }

                //this.SetStratixUser();
            }
            catch (Exception ex)
            {
                ErrorLog.createLog("Error in Configuration : " + ex.ToString());
                ErrorLog.CreateLog(ex);
            }
            finally
            {
                oDbHelper.CloseDatabaseConnection();
            }
        }


        public DataTable GetActiveVendorList()
        {
            m_sSql = "SELECT * FROM configuration WHERE Active = 1";

            #region to process only selected vendorlist without checking active flag    2020/03/16  Sumit
            if (!string.IsNullOrEmpty(IsVendorFilter) && BusinessUtility.GetInt(IsVendorFilter) == 1)
            {
                if (!string.IsNullOrEmpty(FilteredVendorConfig))
                {
                    VendorArr = FilteredVendorConfig.Split(',');
                    FilteredVendorDB = "";
                    for (int i = 0; i < VendorArr.Length; i++)
                    {
                        if (i == 0)
                            FilteredVendorDB = "'" + Convert.ToString(VendorArr[i]) + "'";
                        else
                            FilteredVendorDB = FilteredVendorDB + "," + "'" + Convert.ToString(VendorArr[i]) + "'";
                    }
                    m_sSql = "SELECT * FROM configuration WHERE VendorDunsNo in (" + FilteredVendorDB + ")";
                }

            }
            #endregion to process only selected vendorlist without checking active flag    2020/03/16  Sumit  


            iTECH.Library.DataAccess.MySql.DbHelper oDbHelper = new iTECH.Library.DataAccess.MySql.DbHelper(sMysqlConnectionString, true);
            DataTable oDtAddress = null;
            try
            {
                MySqlParameter[] oMySqlParameter = {};
                oDtAddress = oDbHelper.GetDataTable(m_sSql, CommandType.Text, oMySqlParameter);
            }
            catch (Exception ex)
            {
                ErrorLog.createLog("Error in Configuration : " + ex.ToString());
                ErrorLog.CreateLog(ex);
            }
            finally
            {
                oDbHelper.CloseDatabaseConnection();
            }
            return oDtAddress;
        }

        
        public void SetStratixUser()
        {
            if (ApplicationMode == ApplicationType.LOCAL && EdiFile == EdiFileType.EDI863)
            {
                sCompanyId = "STX";
                sConnectedAccessType = "I";
                sEnvironmentClass = "LIV";
                sEnvironmentName = "livitec";
                //forceDisconnect = true;
                sUsername = "prabh";
                sPassword = "password";
            }
            else if (ApplicationMode == ApplicationType.QC && EdiFile == EdiFileType.EDI863)
            {
                sCompanyId = "MSU";
                sConnectedAccessType = "I";
                sEnvironmentClass = "TST";
                sEnvironmentName = "tstmaj";
                //forceDisconnect = true;
                sUsername = "itechedi";
                sPassword = "+~%5qcKm";
            }
            else if (ApplicationMode == ApplicationType.PRODUCTION && EdiFile == EdiFileType.EDI863)
            {
                sCompanyId = "MSU";
                sConnectedAccessType = "I";
                sEnvironmentClass = "LIV";
                sEnvironmentName = "livitec";
                //forceDisconnect = true;
                sUsername = "prabh";
                sPassword = "password";
            }
        }



        // #transactionsetcontrolno# is ST02 section from incoming edi file.
        // the ISA15 should be T for test data and P for production. 
        // #transstatus# is A if accepted and R if rejected
        // AK102 will be value of GS06 of edi original file
        public string CreateAck(string sAck, object obj)
        {
            string sReturn = string.Empty;
            string[] sArr = Utility.GetSplitedArray(obj);
            string sPoNo = string.Empty;
            string sBsn = string.Empty;

            string sDunsTo = string.Empty;
            string sDunsFrom = string.Empty;
            string sDunsQualifierTo = string.Empty;
            string sDunsQualifierFrom = string.Empty;

            string sDateTimeYYYYMMDD = DateTime.Now.ToString("yyyyMMdd");
            string sDateTimeYYMMDD = DateTime.Now.ToString("yyMMdd");
            string sHHMM = DateTime.Now.Hour.ToString().PadLeft(2, '0') + "" + DateTime.Now.Second.ToString().PadLeft(2, '0');

            string sTransControlNo = string.Empty;
            string sTotalnumberofsegmentsincluded = string.Empty;
            int iMaxFileId = this.GetMaxFileID();
            string sISAID = string.Empty;
            string sGs6 = string.Empty;
            string sDunsToIsa = string.Empty;
            string sDunsFromIsa = string.Empty;

            try
            {
                if (sArr.Length > 1)
                {
                    var po = sArr.ToList<string>().FirstOrDefault(stringToCheck => stringToCheck.Contains("PRF*"));
                    var bsn = sArr.ToList<string>().FirstOrDefault(stringToCheck => stringToCheck.Contains("BSN*"));

                    var ISA = sArr.ToList<string>().FirstOrDefault(stringToCheck => stringToCheck.Contains("ISA*"));
                    var ST = sArr.ToList<string>().FirstOrDefault(stringToCheck => stringToCheck.Contains("ST*"));
                    var SE = sArr.ToList<string>().FirstOrDefault(stringToCheck => stringToCheck.Contains("SE*"));

                    var GS = sArr.ToList<string>().FirstOrDefault(stringToCheck => stringToCheck.Contains("GS*"));


                    if (po != null)
                    {
                        sPoNo = po;
                        sPoNo = sPoNo.Split('*')[1];
                    }
                    if (po == null && string.IsNullOrEmpty(sPoNo))
                    {
                        po = sArr.ToList<string>().FirstOrDefault(stringToCheck => stringToCheck.Contains("LIN*"));
                        sPoNo = Utility.GetIntFromString(Utility.GetElementValue(po, "PO")).ToString();
                    }

                    if (GS != null)
                    {
                        sGs6 = GS.Split('*')[6];
                    }


                    if (bsn != null)
                    {
                        sBsn = bsn.Split('*')[2];
                    }

                    if (ISA != null)
                    {
                        sDunsFrom = ISA.Split('*')[6].Trim();
                        sDunsTo = ISA.Split('*')[8].Trim();
                        sDunsQualifierTo = ISA.Split('*')[7].Trim();
                        sDunsQualifierFrom = ISA.Split('*')[5].Trim();
                    }
                    if (ST != null)
                    {
                        sTransControlNo = ST.Split('*')[2].Trim();
                    }
                    if (SE != null)
                    {
                        sTotalnumberofsegmentsincluded = SE.Split('*')[1].Trim();
                    }

                    sISAID = iMaxFileId.ToString().PadLeft(9, '0');
                    sDunsToIsa = sDunsTo.PadRight(15, ' ');
                    sDunsFromIsa = sDunsFrom.PadRight(15, ' ');

                }

                sReturn = sAck.Replace("#pono#", sPoNo);
                sReturn = sReturn.Replace("#bsn02#", sBsn);
                sReturn = sReturn.Replace("#yymmdd#", sDateTimeYYMMDD);
                sReturn = sReturn.Replace("#hhmm#", sHHMM);
                sReturn = sReturn.Replace("#yyyymmdd#", sDateTimeYYYYMMDD);
                sReturn = sReturn.Replace("#groupversion#", BusinessUtility.GetString(ConfigurationManager.AppSettings["AckGroupVersion"]));      // IBM ASKED FOR THIS GROUP VERSION NO         AckGroupVersion
                sReturn = sReturn.Replace("#dunsqualifierto#", sDunsQualifierTo);
                sReturn = sReturn.Replace("#dunsto#", sDunsTo);
                sReturn = sReturn.Replace("#dunstoisa#", sDunsToIsa);
                sReturn = sReturn.Replace("#dunsfromisa#", sDunsFromIsa);
                sReturn = sReturn.Replace("#dunsfrom#", sDunsFrom);
                sReturn = sReturn.Replace("#dunsqualifierfrom#", sDunsQualifierFrom);
                //sReturn = sReturn.Replace("#dunsfrom#", "TIMKENSTEELT");
                //sReturn = sReturn.Replace("#dunsqualifierfrom#", "ZZ");
                if (Configuration.ApplicationMode == ApplicationType.PRODUCTION)
                {
                    sReturn = sReturn.Replace("#appenvironment#", "P");
                }
                else
                {
                    sReturn = sReturn.Replace("#appenvironment#", "T");
                }
                if (!string.IsNullOrEmpty(sPoNo))
                {
                    sReturn = sReturn.Replace("#transstatus#", "A");
                }
                else
                {
                    sReturn = sReturn.Replace("#transstatus#", "R");
                }
                sReturn = sReturn.Replace("#numoftranssetincluded#", "1");
                sReturn = sReturn.Replace("#numoftranssetreceived#", "1");
                sReturn = sReturn.Replace("#numoftranssetaccepted#", "1");

                sReturn = sReturn.Replace("#transactionsetcontrolno#", sTransControlNo);
                sReturn = sReturn.Replace("#Totalnumberofsegmentsincluded#", "6");
                sReturn = sReturn.Replace("#gsmaxid#", iMaxFileId.ToString());
                sReturn = sReturn.Replace("#isamaxid#", sISAID);
                sReturn = sReturn.Replace("#gs6#", sGs6);

                ErrorLog.createLog("Ack creation complete text is :  " + sReturn);
            }
            catch (Exception ex)
            {
                ErrorLog.createLog(ex.ToString());
                ErrorLog.CreateLog(ex);
            }


            return sReturn;
        }



        public int GetMaxFileID()
        {
            m_sSql = "select MAX(FileId) AS id from files";
            object iId = 0;
            iTECH.Library.DataAccess.MySql.DbHelper oDbHelper = new iTECH.Library.DataAccess.MySql.DbHelper(sMysqlConnectionString, true);
            try
            {
                iId = oDbHelper.GetValue(m_sSql, CommandType.Text, null);
            }
            catch (Exception ex)
            {
                ErrorLog.createLog("Error in GetMaxFileID : " + ex.ToString());
                ErrorLog.CreateLog(ex);
            }

            return BusinessUtility.GetInt(iId);

        }





    }
}
