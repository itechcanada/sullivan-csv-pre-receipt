﻿using EnterpriseDT.Net.Ftp;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SecureFTP
{
    class FTPFileFormat : FTP
    {
        public FTPFileFormat(FTP ftp)
        {
            Username = ftp.Username;
            Password = ftp.Password;
            Port = ftp.Port;
            Server = ftp.Server;
            LocalDownloadDir = ftp.LocalDownloadDir;
            FtpDirectory = ftp.FtpDirectory;
        }

        public ArrayList DownloadAllFilesFromFTP(string FileFormat)
        {            
            FTPConnection ftpconn = new FTPConnection();
            ftpconn.ServerAddress = Server;
            ftpconn.ServerPort = Port;
            ftpconn.UserName = Username;
            ftpconn.Password = Password;
            ftpconn.Connect();

            ArrayList oList = new ArrayList();
            string[] sFilear = ftpconn.GetFiles(FtpDirectory).Where(s => FileFormat.Contains(Path.GetExtension(s).ToLower())).ToArray();
            foreach (string s in sFilear)
            {
                FileInfo fileinfo = new FileInfo(s);
                ftpconn.DownloadFile(LocalDownloadDir + "\\" + fileinfo.Name, s);
                oList.Add(fileinfo.Name);
            }
            ftpconn.Close();
            ftpconn = null;
            return oList;
        }
    }
}
