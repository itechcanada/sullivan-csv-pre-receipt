﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SecureFTP.MailNotify.NotifyStatus
{
    interface INotifyStatus
    {
        bool IsNotify { get; set; }
        string NotifyLevel { get; set; }
        string MessageSubject { get; set; }
        string MessageBody { get; set; }

        void GetStatus();
    }
}
