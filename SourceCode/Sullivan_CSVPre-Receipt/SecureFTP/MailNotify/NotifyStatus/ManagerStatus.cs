﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SecureFTP.MailNotify.NotifyStatus
{
    class ManagerStatus
    {
        public INotifyStatus NotificationStatus(string filepath, string status)
        {
            INotifyStatus _iNotifyStatus = null;

            if (status.Trim().ToLower() == "onsuccess")
                _iNotifyStatus = new OnSuccessStatus(filepath);
            else if (status.Trim().ToLower() == "onfail")
                _iNotifyStatus = new OnFailStatus(filepath);

            if (_iNotifyStatus != null)
                _iNotifyStatus.GetStatus();

            return _iNotifyStatus;
        }
    }
}
