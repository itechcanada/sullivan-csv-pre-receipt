﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace SecureFTP.MailNotify.NotifyStatus
{
    class OnFailStatus : INotifyStatus
    {
        bool _isNotify = false;
        string _notifylevel;
        string _messageSubject;
        string _messageBody;
        string _FilePath { get; set; }

        public bool IsNotify { get => _isNotify; set => _isNotify = value; }
        public string NotifyLevel { get => _notifylevel; set => _notifylevel = value; }
        public string MessageSubject { get => _messageSubject; set => _messageSubject = value; }
        public string MessageBody { get => _messageBody; set => _messageBody = value; }

        public OnFailStatus(string FilePath)
        {
            _FilePath = FilePath;
        }

        public void GetStatus()
        {
            try
            {
                XmlDocument xDoc = new XmlDocument();
                xDoc.Load(_FilePath);
                XmlNode xStatus = xDoc.SelectSingleNode("//Status");
                if (xStatus != null)
                {
                    XmlNode xOnSuccess = xStatus.SelectSingleNode("OnFail");
                    if (xOnSuccess != null)
                    {
                        if (Convert.ToString(xOnSuccess.Attributes["Notify"].Value) == "1")
                            _isNotify = true;
                        if (!string.IsNullOrEmpty(xOnSuccess.Attributes["Level"].Value))
                            _notifylevel = Convert.ToString(xOnSuccess.Attributes["Level"].Value);
                        if (!string.IsNullOrEmpty(xOnSuccess.SelectSingleNode("MessageSubject").InnerText))
                            _messageSubject = Convert.ToString(xOnSuccess.SelectSingleNode("MessageSubject").InnerText).Trim();
                        if (!string.IsNullOrEmpty(xOnSuccess.SelectSingleNode("MessageBody").InnerText))
                            _messageBody = Convert.ToString(xOnSuccess.SelectSingleNode("MessageBody").InnerText).Trim();
                    }
                }

            }
            catch (Exception ex)
            {
                ErrorLog.createLog("Error In OnFailStatus-GetStatus");
                ErrorLog.CreateLog(ex);
            }
        }
    }
}
