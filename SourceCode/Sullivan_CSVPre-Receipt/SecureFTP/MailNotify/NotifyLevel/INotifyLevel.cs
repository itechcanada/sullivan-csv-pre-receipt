﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SecureFTP.MailNotify.NotifyLevel
{
    interface INotifyLevel
    {
        string LevelName { get;}
        string FilterName { get; set; }
        List<string> MailIDList { get; set; }
        string PONumber { get; set; }
        int PoItem { get; set; }
        string BranchName { get; set; }

        void GetLevelMailList();
    }
}
