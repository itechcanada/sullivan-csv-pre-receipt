﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace SecureFTP.MailNotify.NotifyLevel
{
    class SupportLevel : INotifyLevel
    {
        string _levelName;
        string _filterName;
        List<string> _mailList;
        string _FilePath;
        string _poNumber;
        int _poItem;
        String _branchName { get; set; }

        public string LevelName => "Support";
        public string FilterName { get => _filterName; set => _filterName = value; }
        public List<string> MailIDList { get => _mailList; set => _mailList = value; }
        public string PONumber { get => _poNumber; set => _poNumber = value; }
        public int PoItem { get => _poItem; set => _poItem = value; }
        public string BranchName { get => _branchName; set => _branchName = value; }

        public SupportLevel(string FilePath)
        {
            _FilePath = FilePath;
            _filterName = "Itech";
            _mailList = new List<string>();
        }

        public void GetLevelMailList()
        {
            try
            {
                XmlDocument xDoc = new XmlDocument();
                xDoc.Load(_FilePath);
                XmlNode xLevel = xDoc.SelectSingleNode("//Level");
                if (xLevel != null)
                {
                    XmlNode xSupport = xLevel.SelectSingleNode("Support");
                    if (xSupport != null)
                    {
                        foreach (XmlNode filternode in xSupport.ChildNodes)
                        {
                            foreach (XmlNode xNode in filternode.ChildNodes)
                            {
                                _mailList.Add(xNode.InnerText.ToString().Trim());
                            }
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                ErrorLog.createLog("Error in SupportLevel-GetLevelMailList");
                ErrorLog.CreateLog(ex);
            }
        }
    }
}
