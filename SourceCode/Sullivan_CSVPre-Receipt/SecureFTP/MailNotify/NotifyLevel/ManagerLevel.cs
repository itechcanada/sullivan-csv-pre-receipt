﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SecureFTP.MailNotify.NotifyLevel
{
    class ManagerLevel
    {
        public INotifyLevel NotificationLevelMailList(string filepath, string status, string level, string PoNo, int PoItem)
        {
            INotifyLevel _iNotifylevel = null;

            if (status.Trim().ToLower() == "onfail")
                _iNotifylevel = new SupportLevel(filepath);
            else if(status.Trim().ToLower() =="onsuccess")
            {
                switch(level.Trim().ToLower())
                {
                    case "branch":
                        _iNotifylevel = new BranchLevel(filepath, PoNo, PoItem);
                        break;
                    case "vendorid":
                        _iNotifylevel = new VendorLevel(filepath, PoNo, PoItem);
                        break;
                    case "mill":
                        _iNotifylevel = new MillLevel(filepath, PoNo, PoItem);
                        break;
                }
            }

            if (_iNotifylevel != null)
                _iNotifylevel.GetLevelMailList();


            return _iNotifylevel;
        }
    }
}
