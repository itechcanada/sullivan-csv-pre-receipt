﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using iTECH.Library.DataAccess.MsSql;
using iTECH.Library.Utilities;

namespace SecureFTP.MailNotify.NotifyLevel
{
    public class VendorLevel : INotifyLevel
    {
        string sMysqlConnectionString = ConfigurationManager.ConnectionStrings["NewConnectionString"].ToString();
        string _filterName;
        List<string> _mailList;
        string _FilePath;
        string _poNumber;
        int _poItem;
        string _branchName;

        public string LevelName => "VendorID";
        public string FilterName { get => _filterName; set => _filterName = value; }
        public List<string> MailIDList { get => _mailList; set => _mailList = value; }
        public string PONumber { get => _poNumber; set => _poNumber = value; }
        public int PoItem { get => _poItem; set => _poItem = value; }
        public string BranchName { get => _branchName; set => _branchName = value; }

        public VendorLevel(string FilePath, string PoNo, int PoItem)
        {
            _FilePath = FilePath;
            _poNumber = PoNo;
            _poItem = PoItem;
            _mailList = new List<string>();
            
        }

        public void GetLevelMailList()
        {
            try
            {
                XmlDocument xDoc = new XmlDocument();
                xDoc.Load(_FilePath);
                XmlNode xLevel = xDoc.SelectSingleNode("//Level");
                if (xLevel != null)
                {
                    XmlNode xSupport = xLevel.SelectSingleNode("VendorID");
                    if (xSupport != null)
                    {
                        //_tableName = xSupport.Attributes["TableName"].Value.Trim();
                        //_columnName = xSupport.Attributes["ColumnName"].Value.Trim();
                        //_where = xSupport.Attributes["where"].Value.Trim();

                        _filterName = LevelName + "_" + GetVendorID();
                        XmlNode filterNode = xSupport.SelectSingleNode(_filterName);
                        if (filterNode != null)
                        {
                            foreach (XmlNode xNode in filterNode.ChildNodes)
                            {
                                _mailList.Add(xNode.InnerText.ToString().Trim());
                            }
                        }

                    }
                }

            }
            catch (Exception ex)
            {
                ErrorLog.createLog("Error in VendorLevel-GetLevelMailList");
                ErrorLog.CreateLog(ex);
            }
        }

        public string GetVendorID()
        {
            string _vendorid = string.Empty;
            DataTable oDataTable = null;
            string sbSql = string.Empty;

            sbSql = "select distinct f.VendorID from files f inner join header h on f.FileId = h.FileId inner join detail d on h.HeaderId= d.HeaderId and d.PrntNo='"+ _poNumber + "' and d.PrntItm = "+ _poItem +"";
            DbHelper oDbHelper = new DbHelper(sMysqlConnectionString, true);
            try
            {
                ErrorLog.createLog(sbSql.ToString());
                oDataTable = oDbHelper.GetDataTable(sbSql.ToString(), CommandType.Text, null);
                if (oDataTable.Rows.Count > 0)
                    _vendorid = BusinessUtility.GetString(oDataTable.Rows[0]["f.VendorID"]);

            }
            catch (Exception ex)
            {
                ErrorLog.createLog("Error in VendorLevel-GetVendorID. PO No: " + _poNumber + " PO Item: " + _poItem);
                ErrorLog.CreateLog(ex);
            }
            finally
            {
                oDbHelper.CloseDatabaseConnection();
            }

            return _vendorid;
        }
    }
}
