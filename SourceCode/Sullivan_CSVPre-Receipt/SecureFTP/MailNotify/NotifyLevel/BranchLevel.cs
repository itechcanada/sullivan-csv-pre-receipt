﻿
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using iTECH.Library.DataAccess.MySql;
using MySql.Data.MySqlClient;
using iTECH.Library.Utilities;

namespace SecureFTP.MailNotify.NotifyLevel
{
    public class BranchLevel : INotifyLevel
    {
        static string sMysqlConnectionString = ConfigurationManager.ConnectionStrings["NewConnectionString"].ToString();
        string _filterName;
        List<string> _mailList;
        string _FilePath;
        //Int32 _headerID;
        //string _tableName;
        //string _columnName;
        //string _where;
        string _poNumber;
        int _poItem;
        string _branchName;

        public string LevelName => "Branch";
        public string FilterName { get => _filterName; set => _filterName = value; }
        public List<string> MailIDList { get => _mailList; set => _mailList = value; }
        public string PONumber { get => _poNumber; set => _poNumber= value; }
        public int PoItem { get => _poItem; set => _poItem = value; }
        public string BranchName { get => _branchName; set => _branchName= value; }

        public BranchLevel(string FilePath, string PoNo, int PoItem)
        {
            _FilePath = FilePath;
            _poNumber = PoNo;
            _poItem = PoItem; 
            _mailList = new List<string>();
        }

        public void GetLevelMailList()
        {
            try
            {
                XmlDocument xDoc = new XmlDocument();
                xDoc.Load(_FilePath);
                XmlNode xLevel = xDoc.SelectSingleNode("//Level");
                if (xLevel != null)
                {
                    XmlNode xSupport = xLevel.SelectSingleNode("Branch");
                    if (xSupport != null)
                    {
                        //_tableName = xSupport.Attributes["TableName"].Value.Trim();
                        //_columnName = xSupport.Attributes["ColumnName"].Value.Trim();
                        //_where = xSupport.Attributes["where"].Value.Trim();

                        _filterName = LevelName +"_" + GetBranch();
                        XmlNode filterNode = xSupport.SelectSingleNode(_filterName);
                        if (filterNode != null)
                        {
                            foreach (XmlNode xNode in filterNode.ChildNodes)
                            {
                                _mailList.Add(xNode.InnerText.ToString().Trim());
                            }
                        }

                    }
                }

            }
            catch (Exception ex)
            {
                ErrorLog.createLog("Error in BranchLevel-GetLevelMailList");
                ErrorLog.CreateLog(ex);
            }
        }

        /// <summary>
        /// get branch code from stratix database on the basis of headerid
        /// </summary>
        /// <returns></returns>
        public string GetBranch()
        {
            try
            {
                if(!string.IsNullOrEmpty(_poNumber))
                {
                    _branchName = tctipd.GetBranchCode_PO(_poNumber);
                    ErrorLog.createLog("Branch Code for Branch Level: " + _branchName);
                }
            }
            catch (Exception ex)
            {
                ErrorLog.createLog("Error in BranchLevel-GetBranch. Po No: "+ _poNumber);
                ErrorLog.CreateLog(ex);
            }

            return _branchName;
        }
    }
}
