﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using iTECH.Library.Utilities;
using System.Net.Mail;

namespace SecureFTP.MailNotify
{
    class MailNotifyManager
    {
        string NotificationConfigFile;
        //public Int32 HeaderID { get; set; }       commented header id, use Po number and item in all level    2020/05/04  Sumit
        public string prNumber { get; set; }
        public string poNumber { get; set; }
        public int poItem { get; set; }
        public string NotificationStatus { get; set; }
        public string NotificationMessage { get; set; }
        public string PoWhs { get; set; }

        public MailNotifyManager()
        {
            string filepath = System.IO.Path.GetDirectoryName(Environment.GetCommandLineArgs()[0]);
            NotificationConfigFile = System.IO.Path.Combine(filepath, "MailNotification_Config.xml");
            ErrorLog.createLog("Notification Config: " + NotificationConfigFile);
        }

        public void SendNotification()
        {
            StringBuilder sbMsg = new StringBuilder();
            string _emailRecepient = string.Empty;
            List<MailAddress> _recepientList = new List<MailAddress>();
            MailAddress _recepientMail;
            try
            {
                ErrorLog.createLog("Po Number: " + poNumber + "  Po Item: "+ poItem);
                //ErrorLog.createLog("PR Number:" + prNumber);
                ErrorLog.createLog("Status: " + NotificationStatus);
                ErrorLog.createLog("Message: " + NotificationStatus);

                //collect smtp details
                SmtpManager smtpManager = new SmtpManager(NotificationConfigFile);
                smtpManager.GetSMTPDetail();


                //Collect Status message and level
                NotifyStatus.ManagerStatus managerStatus = new NotifyStatus.ManagerStatus();
                NotifyStatus.INotifyStatus iNotifyStatus = managerStatus.NotificationStatus(NotificationConfigFile, NotificationStatus.Trim());

                if (iNotifyStatus == null)
                {
                    ErrorLog.createLog("Notification Status message or level not configured. Cann't send email notification.");
                    return;
                }

                if (string.IsNullOrEmpty(iNotifyStatus.MessageSubject))
                {
                    ErrorLog.createLog("Notification message subject empty.Cann't send email notification.");
                    return;
                }

                //Configuring Message Body
                sbMsg.AppendLine("Hello,");
                sbMsg.AppendLine();
                sbMsg.AppendLine(iNotifyStatus.MessageBody);
                sbMsg.AppendLine();
                sbMsg.AppendLine(NotificationMessage);
                


                //collect level mail id list
                NotifyLevel.ManagerLevel managerLevel = new NotifyLevel.ManagerLevel();
                NotifyLevel.INotifyLevel iNotifyLevel = managerLevel.NotificationLevelMailList(NotificationConfigFile, NotificationStatus.Trim(), iNotifyStatus.NotifyLevel.Trim(), poNumber, poItem);

                if (iNotifyLevel == null)
                {
                    ErrorLog.createLog("Notification level detail not configured. Cann't send email notification.");
                    return;
                }


                if (iNotifyLevel.MailIDList.Count < 1)
                {
                    ErrorLog.createLog("Notification level mail id not configured. Cann't send email notification.");
                    return;
                }
                else
                {
                    foreach (string email in iNotifyLevel.MailIDList)
                    {
                        if (string.IsNullOrEmpty(_emailRecepient))
                            _emailRecepient = email;
                        else
                            _emailRecepient = _emailRecepient + "," + email;

                        //add in mail address list
                        _recepientMail = new MailAddress(email);
                        _recepientList.Add(_recepientMail);
                    }
                }

                //configure msg body
                if(!string.IsNullOrEmpty(iNotifyLevel.BranchName))
                    sbMsg.AppendLine("Branch: "+ iNotifyLevel.BranchName);
                if (!string.IsNullOrEmpty(iNotifyLevel.PONumber))
                {
                    sbMsg.AppendLine("PO Number: " + iNotifyLevel.PONumber);
                    //get Po whs from stratix db    2020/05/04  Sumit
                    PoWhs = GetPOWhs(poNumber, poItem);
                    sbMsg.AppendLine("Ship To Warehouse: " + PoWhs);
                }
                sbMsg.AppendLine();
                sbMsg.AppendLine();
                sbMsg.AppendLine("Thank You");


                //setting email configuration for email notification
                MailAddress fromAddress = new MailAddress(smtpManager.SMTPUid);
                EmailSettings.SMTPServer = smtpManager.SMTPHost;
                EmailSettings.SMTPPort = BusinessUtility.GetInt(smtpManager.SMTPPort);
                EmailSettings.SMTPUserEmail = smtpManager.SMTPUid;
                EmailSettings.SMTPPassword = smtpManager.SMTPPwd;
                EmailSettings.IsAsynchronousMail = true;
                EmailSettings.UseDefaultCredentials = true;

                //before sending notification
                ErrorLog.createLog("EmailSettings.SMTPServer: " + EmailSettings.SMTPServer);
                ErrorLog.createLog("EmailSettings.SMTPPort: " + EmailSettings.SMTPPort);
                ErrorLog.createLog("EmailSettings.SMTPUserEmail: " + EmailSettings.SMTPUserEmail);
                ErrorLog.createLog("EmailSettings.SMTPPassword: " + EmailSettings.SMTPPassword);
                ErrorLog.createLog("EmailSettings.IsAsynchronousMail: " + EmailSettings.IsAsynchronousMail);
                ErrorLog.createLog("EmailSettings.UseDefaultCredentials: " + EmailSettings.UseDefaultCredentials);
                ErrorLog.createLog("Email Subject: " + iNotifyStatus.MessageSubject + " - "+ NotificationMessage);
                ErrorLog.createLog("smtpManager.SMTPUid: " + smtpManager.SMTPUid);
                ErrorLog.createLog("Email Recepient: " + _emailRecepient);
                ErrorLog.createLog("Email Message: " + sbMsg.ToString());


                try
                {
                    //calling email send
                    if (EmailHelper.SendEmail(fromAddress, _recepientList, sbMsg.ToString(), iNotifyStatus.MessageSubject +" - "+ NotificationMessage, false))
                    {
                        ErrorLog.createLog("Email Notification Sent For :" + NotificationMessage);
                    }
                }
                catch(Exception ex)
                {
                    ErrorLog.createLog("Error in Sending Email Notification");
                    ErrorLog.CreateLog(ex);
                }
                #region To test the mail code
                //MailMessage Mail = new MailMessage();
                //Mail.From = new MailAddress(smtpManager.SMTPUid);
                //foreach (string email in iNotifyLevel.MailIDList)
                //{
                //    Mail.To.Add(new MailAddress(email));
                //}
                //Mail.Subject = iNotifyStatus.MessageSubject;
                //Mail.Body = sbMsg.ToString();

                ////
                //SmtpClient emailClient = new SmtpClient(smtpManager.SMTPHost);

                //emailClient.Host = smtpManager.SMTPHost;
                //emailClient.UseDefaultCredentials = true;
                //emailClient.Port = Convert.ToInt16(smtpManager.SMTPPort);
                //emailClient.Credentials = new System.Net.NetworkCredential(smtpManager.SMTPUid, smtpManager.SMTPPwd);
                //emailClient.EnableSsl = smtpManager.SMTPEnableSSL;
                //try
                //{
                //    emailClient.Send(Mail);
                //}
                //catch (Exception ex)
                //{
                //    ErrorLog.createLog("Error in EmailFile method of EdiError class : " + ex.ToString());
                //}
                #endregion To test the mail code
            }
            catch (Exception ex)
            {
                ErrorLog.createLog("Error in MailNotifyManager-SendNotification");
                ErrorLog.CreateLog(ex);
            }
        }

        public string GetPOWhs(string PONumber,int POItem)
        {
            string whs = string.Empty;
            string freightVenId = string.Empty;
            try
            {
                tctipd tctipd = new tctipd();
                tctipd.PopulateWarehouse(PONumber,  POItem, ref whs, ref freightVenId);
            }
            catch { throw; }
            return whs;
        }
    }
}
