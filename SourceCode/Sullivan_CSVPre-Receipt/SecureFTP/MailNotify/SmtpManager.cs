﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace SecureFTP.MailNotify
{
    class SmtpManager
    {
        string _filePath;
        public string SMTPHost { get; set; }
        public string SMTPUid { get; set; }
        public string SMTPPwd { get; set; }
        public string SMTPPort { get; set; }
        public bool SMTPEnableSSL { get; set; }

        public SmtpManager(string filepath)
        {
            _filePath = filepath;
        }

        public void GetSMTPDetail()
        {
            try
            {
                XmlDocument xDoc = new XmlDocument();
                xDoc.Load(_filePath);
                XmlNode xSmtp = xDoc.SelectSingleNode("//SMTP");
                if (xSmtp != null)
                {
                    if (!string.IsNullOrEmpty(xSmtp.SelectSingleNode("Host").InnerText.Trim()))
                        this.SMTPHost = xSmtp.SelectSingleNode("Host").InnerText.Trim();
                    if (!string.IsNullOrEmpty(xSmtp.SelectSingleNode("UID").InnerText.Trim()))
                        this.SMTPUid = xSmtp.SelectSingleNode("UID").InnerText.Trim();
                    if (!string.IsNullOrEmpty(xSmtp.SelectSingleNode("Pwd").InnerText.Trim()))
                        this.SMTPPwd = xSmtp.SelectSingleNode("Pwd").InnerText.Trim();
                    if (!string.IsNullOrEmpty(xSmtp.SelectSingleNode("Port").InnerText.Trim()))
                        this.SMTPPort = xSmtp.SelectSingleNode("Port").InnerText.Trim();
                    if (!string.IsNullOrEmpty(xSmtp.SelectSingleNode("EnableSsl").InnerText.Trim()))
                    {
                        if(xSmtp.SelectSingleNode("EnableSsl").InnerText.Trim().ToLower() =="true")
                            this.SMTPEnableSSL = true;
                        else
                            this.SMTPEnableSSL = false;
                    }
                    
                }

            }
            catch (Exception ex)
            {
                ErrorLog.createLog("Error in SmtpManager-GetSMTPDetail");
                ErrorLog.CreateLog(ex);
            }
        }
    }
}
