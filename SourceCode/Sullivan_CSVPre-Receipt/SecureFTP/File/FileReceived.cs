﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Text;
using iTECH.Library.Utilities;
using System.IO;
using iTECH.Library.DataAccess.MySql;

namespace SecureFTP
{
    class FileReceived
    {
        string m_sSql = string.Empty;
        StringBuilder m_sbSql = new StringBuilder();
        static string sMysqlConnectionString = ConfigurationManager.ConnectionStrings["NewConnectionString"].ToString();
        Configuration oConfiguration = new Configuration();
        int iFileReceivedId = 0;
        public int FileReceivedId
        {
            get { return iFileReceivedId; }
            set { iFileReceivedId = value; }
        }

        string sFileName = string.Empty;
        public string FileName
        {
            get { return sFileName; }
            set { sFileName = value; }
        }

        int iExecutionPeriod = 0;
        public int ExecutionPeriod
        {
            get { return iExecutionPeriod; }
            set { iExecutionPeriod = value; }
        }

        string sFileReceivedTime = string.Empty;
        public string FileReceivedTime
        {
            get { return sFileReceivedTime; }
            set { sFileReceivedTime = value; }
        }

        string sFileReceivedDirectory = string.Empty;
        public string FileReceivedDirectory
        {
            get { return sFileReceivedDirectory; }
            set { sFileReceivedDirectory = value; }
        }

        int iProcessed = 0;
        public int Processed
        {
            get { return iProcessed; }
            set { iProcessed = value; }
        }

        DateTime dtFileReceivedDateTime = DateTime.Now;
        public DateTime FileReceivedDateTime
        {
            get { return dtFileReceivedDateTime; }
            set { dtFileReceivedDateTime = value; }
        }

        DateTime dtProcessedOn = DateTime.Now;
        public DateTime ProcessedOn
        {
            get { return dtProcessedOn; }
            set { dtProcessedOn = value; }
        }

        DateTime dtProcessOn = DateTime.Now;
        public DateTime ProcessOn
        {
            get { return dtProcessOn; }
            set { dtProcessOn = value; }
        }

        bool bActive = true;
        public bool Active
        {
            get { return bActive; }
            set { bActive = value; }
        }


        string sEdiFile = string.Empty;
        public string EdiFile
        {
            get { return sEdiFile; }
            set { sEdiFile = value; }
        }

        string sDunsno = string.Empty;
        public string Dunsno
        {
            get { return sDunsno; }
            set { sDunsno = value; }
        }

        string sUniqueFileName = string.Empty;
        public string UniqueFileName
        {
            get { return sUniqueFileName; }
            set { sUniqueFileName = value; }
        }

        public FileReceived()
        {
        }

          // parametrised constructor to get edi origin zone
        public FileReceived(int iFileReceivedId)
        {
            FileReceivedId = iFileReceivedId;
            m_sSql = "SELECT * FROM filereceived WHERE FileReceivedId = '" + iFileReceivedId + "' AND Active = 1";

            iTECH.Library.DataAccess.MySql.DbHelper oDbHelper = new iTECH.Library.DataAccess.MySql.DbHelper(sMysqlConnectionString, true);

            try
            {
                MySqlParameter[] oMySqlParameter = {};
                DataTable oDtAddress = new iTECH.Library.DataAccess.MySql.DbHelper(sMysqlConnectionString, true).GetDataTable(m_sSql, CommandType.Text, oMySqlParameter);
                foreach (DataRow oDataRow in oDtAddress.Rows)
                {
                    FileName = BusinessUtility.GetString(oDataRow["FileName"]);
                    Active = BusinessUtility.GetBool(oDataRow["Active"]);
                    Processed = BusinessUtility.GetInt(oDataRow["Processed"]);
                    FileReceivedDirectory = BusinessUtility.GetString(oDataRow["FileReceivedDirectory"]);
                    FileReceivedTime = BusinessUtility.GetString(oDataRow["FileReceivedTime"]);

                    ExecutionPeriod = BusinessUtility.GetInt(oDataRow["ExecutionPeriod"]);
                    FileReceivedDateTime = BusinessUtility.GetDateTime(oDataRow["FileReceivedDateTime"]);
                    ProcessedOn = BusinessUtility.GetDateTime(oDataRow["ProcessedOn"]);
                    ProcessOn = BusinessUtility.GetDateTime(oDataRow["ProcessOn"]);
                    UniqueFileName = BusinessUtility.GetString(oDataRow["UniqueFileName"]);
                }
            }
            catch (Exception ex)
            {
                ErrorLog.createLog("Error in FileReceived : " + ex.ToString());
                ErrorLog.CreateLog(ex);
            }
            finally
            {
                oDbHelper.CloseDatabaseConnection();
            }
        }

        public FileReceived(string sFileName)
        {
            FileName = sFileName;
            m_sSql = "SELECT * FROM filereceived WHERE FileName = '" + sFileName + "' AND Active = 1";

            iTECH.Library.DataAccess.MySql.DbHelper oDbHelper = new iTECH.Library.DataAccess.MySql.DbHelper(sMysqlConnectionString, true);

            try
            {
                MySqlParameter[] oMySqlParameter = { };
                DataTable oDtAddress = new iTECH.Library.DataAccess.MySql.DbHelper(sMysqlConnectionString, true).GetDataTable(m_sSql, CommandType.Text, oMySqlParameter);
                foreach (DataRow oDataRow in oDtAddress.Rows)
                {

                    FileName = BusinessUtility.GetString(oDataRow["FileName"]);
                    Active = BusinessUtility.GetBool(oDataRow["Active"]);
                    Processed = BusinessUtility.GetInt(oDataRow["Processed"]);
                    FileReceivedDirectory = BusinessUtility.GetString(oDataRow["FileReceivedDirectory"]);
                    FileReceivedTime = BusinessUtility.GetString(oDataRow["FileReceivedTime"]);

                    ExecutionPeriod = BusinessUtility.GetInt(oDataRow["ExecutionPeriod"]);
                    FileReceivedDateTime = BusinessUtility.GetDateTime(oDataRow["FileReceivedDateTime"]);
                    ProcessedOn = BusinessUtility.GetDateTime(oDataRow["ProcessedOn"]);
                    ProcessOn = BusinessUtility.GetDateTime(oDataRow["ProcessOn"]);
                    FileReceivedId = BusinessUtility.GetInt(oDataRow["FileReceivedId"]);
                    UniqueFileName = BusinessUtility.GetString(oDataRow["UniqueFileName"]);
                }
            }
            catch (Exception ex)
            {
                ErrorLog.createLog("Error in FileReceived : " + ex.ToString());
                ErrorLog.CreateLog(ex);
            }
            finally
            {
                oDbHelper.CloseDatabaseConnection();
            }
        }


        public int InsertReceiveFile(FileReceived oFileReceived)
        {
            int iStatus = 0;
            m_sbSql = new StringBuilder();
            iTECH.Library.DataAccess.MySql.DbHelper oDbHelper = new iTECH.Library.DataAccess.MySql.DbHelper(sMysqlConnectionString, true);
            try
            {
                m_sbSql.Append(" INSERT INTO filereceived ( FileName,ExecutionPeriod,FileReceivedTime,FileReceivedDirectory, ShipperNo, FileType, UniqueFileName ) ");
                m_sbSql.Append(" VALUES (@FileName, @ExecutionPeriod, @FileReceivedTime, @FileReceivedDirectory, @ShipperNo, @FileType, @UniqueFileName ) ");

                MySqlParameter[] oMySqlParameter = {  
                                         DbUtility.GetParameter("@FileName", oFileReceived.FileName, MyDbType.String),
                                         DbUtility.GetParameter("@ExecutionPeriod", oFileReceived.ExecutionPeriod, MyDbType.Int),
                                         DbUtility.GetParameter("@FileReceivedTime", oFileReceived.FileReceivedTime, MyDbType.String),
                                         DbUtility.GetParameter("@FileReceivedDirectory", oFileReceived.FileReceivedDirectory, MyDbType.String),
                                         DbUtility.GetParameter("@FileType", this.EdiFile, MyDbType.String),
                                          DbUtility.GetParameter("@UniqueFileName", Files.GetUniqueFileName() , MyDbType.String)
                                     };
                iStatus = oDbHelper.ExecuteNonQuery(m_sbSql.ToString(), CommandType.Text, oMySqlParameter);
                iStatus = oDbHelper.GetLastInsertID();
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLog(ex);
            }
            finally
            {
                oDbHelper.CloseDatabaseConnection();
            }
            return iStatus;
        }


        public int InsertReceiveFileHistory(int iFileReceivedId)
        {
            int iStatus = 0;
            m_sbSql = new StringBuilder();
            iTECH.Library.DataAccess.MySql.DbHelper oDbHelper = new iTECH.Library.DataAccess.MySql.DbHelper(sMysqlConnectionString, true);
            try
            {
                m_sbSql.Append(" INSERT INTO filereceivedhistory ( FileReceivedId ) ");
                m_sbSql.Append(" VALUES ( " + iFileReceivedId + " ) ");

                iStatus = oDbHelper.ExecuteNonQuery(m_sbSql.ToString(), CommandType.Text, null);
                iStatus = oDbHelper.GetLastInsertID();
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLog(ex);
            }
            finally
            {
                oDbHelper.CloseDatabaseConnection();
            }
            return iStatus;
        }


        public void ProcessFile(string sFileNAme,string sPath ,ref FileReceived oFileReceived)
        {
            this.PopulateFileInfo(sFileNAme, sPath, ref oFileReceived);

        }

        public void PopulateFileInfo(string sFile, string sFilePath, ref FileReceived oFileReceived)
        {
            try
            {
                int iFileReceivedId = 0;
                string sFile1 = sFilePath + "\\" + sFile;
                if (InputOutput.DirectoryExists(sFilePath))
                {
                    if (InputOutput.FileExistsInDirectory(sFile1))
                    {
                        //FileInfo info = new FileInfo(sFile1);
                        oFileReceived.FileName = sFile;
                        oFileReceived.FileReceivedDirectory = sFilePath;

                        FileReceived oFileReceived1 = new FileReceived(oFileReceived.FileName);
                        if (oFileReceived1.FileReceivedId == 0)
                        {
                           this.GetConfigurationDetailForFile(sFile1);
                           iFileReceivedId = this.InsertReceiveFile(oFileReceived);
                           if (iFileReceivedId > 0)
                           {
                               //Get delay time and update
                               //this.GetConfigurationDetailForFile(sFile1);
                                //if (oConfiguration.TimeDelayInHour > 0)
                                //{
                                ErrorLog.createLog("time delay : " + oConfiguration.TimeDelayInHour);
                                this.UpdateReceiveFile(iFileReceivedId, oConfiguration.TimeDelayInHour);
                               //}

                           }
                        }
                        else
                        {
                            // File exists at this point so need to update time execution period again
                            if (oFileReceived1.FileReceivedId > 0)
                            {
                                this.InsertReceiveFileHistory(oFileReceived1.FileReceivedId);
                                this.GetConfigurationDetailForFile(sFile1);
                                //if (oConfiguration.TimeDelayInHour > 0)
                                //{
                                this.UpdateReceiveFile(oFileReceived1.FileReceivedId, oConfiguration.TimeDelayInHour);
                                //}
                                
                            }

                            ErrorLog.createLog("File exists so updating file");
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLog(ex);
            }
        }


        public void GetConfigurationDetailForFile(string sFilePath)
        {
            FileInfo o = new FileInfo(sFilePath);
            if (o.Extension.Contains(".xml"))
            {
                string sFileLine = System.IO.File.ReadAllText(sFilePath);
                this.EdiFile = Utility.GetFileTypeFromXml(sFileLine);
                this.Dunsno = Utility.GetDUNSNumberFromXml(sFileLine);
                oConfiguration = new Configuration(this.Dunsno, BusinessUtility.GetInt(this.EdiFile));
            }
            // to set process time only for xml files   2020/05/19  Sumit
            //else
            //{
            //    var fileLines = System.IO.File.ReadAllLines(BusinessUtility.GetString(sFilePath));

            //    oConfiguration = new Configuration();
            //    oConfiguration.PopulateConfigurationData(fileLines);
            //}
        }

        public void UpdateReceiveFile(int iId, int iDelayTimeInHour)
        {
            m_sbSql = new StringBuilder();
            iTECH.Library.DataAccess.MySql.DbHelper oDbHelper = new iTECH.Library.DataAccess.MySql.DbHelper(sMysqlConnectionString, true);
            try
            {
                if (iDelayTimeInHour == 0)
                {
                    iDelayTimeInHour = -1;
                    
                    m_sbSql.Append(" update filereceived SET ExecutionPeriod = " + iDelayTimeInHour + " ,Processed = 0,  ProcessOn = NOW() + INTERVAL " + iDelayTimeInHour + " HOUR, FileUpdatedOn = now() WHERE FileReceivedId = " + iId);
                }
                else
                {
                    m_sbSql.Append(" update filereceived SET ExecutionPeriod = " + iDelayTimeInHour + " ,Processed = 0,  ProcessOn = NOW() + INTERVAL " + iDelayTimeInHour + " HOUR, FileUpdatedOn = now() WHERE FileReceivedId = " + iId);
                }

                ErrorLog.createLog("iDelayTimeInHour = " + iDelayTimeInHour);
               
                oDbHelper.ExecuteNonQuery(m_sbSql.ToString(), CommandType.Text, null);
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLog(ex);
            }
            finally
            {
                oDbHelper.CloseDatabaseConnection();
            }
        }

        public void UpdateProcessedReceiveFile(int iId)
        {
            m_sbSql = new StringBuilder();
            iTECH.Library.DataAccess.MySql.DbHelper oDbHelper = new iTECH.Library.DataAccess.MySql.DbHelper(sMysqlConnectionString, true);
            try
            {
                m_sbSql.Append(" update filereceived SET Processed = 1 , ProcessedOn = now(), FileUpdatedOn = now() WHERE FileReceivedId = " + iId);

                oDbHelper.ExecuteNonQuery(m_sbSql.ToString(), CommandType.Text, null);
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLog(ex);
            }
            finally
            {
                oDbHelper.CloseDatabaseConnection();
            }
        }

        public DataTable GetFileReadyToBeProcessed()
        {

            DataTable oDataTable = new DataTable();
            m_sbSql = new StringBuilder();
            iTECH.Library.DataAccess.MySql.DbHelper oDbHelper = new iTECH.Library.DataAccess.MySql.DbHelper(sMysqlConnectionString, true);
            try
            {
                m_sbSql.Append(" select * from FileReceived where Processed = 0 AND ProcessOn < now() ");

                oDataTable = oDbHelper.GetDataTable(m_sbSql.ToString(), CommandType.Text, null);
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLog(ex);
            }
            finally
            {
                oDbHelper.CloseDatabaseConnection();
            }
            return oDataTable;
        }




    }
}
