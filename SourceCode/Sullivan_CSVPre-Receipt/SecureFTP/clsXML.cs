﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
//using System.Web.Security;
//using System.Web.UI;
//using System.Web.UI.HtmlControls;
//using System.Web.UI.WebControls;
//using System.Web.UI.WebControls.WebParts;
using System.Xml;

namespace Source_InventoryDataEntry
{
    public class clsXML
    {
        //Get Node Text
        public static string GetNodeText(string XML, string Node)
        {
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.LoadXml(XML);

            XmlNode xmlNode = null;
            xmlNode = xmlDoc.SelectSingleNode(Node);            

            string Value = xmlNode.InnerText;
            return Value;
        }

        //Get Node XML
        public static string GetNodeXML(string XML, string Node)
        {
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.LoadXml(XML);

            XmlNode xmlNode = null;
            xmlNode = xmlDoc.SelectSingleNode(Node);

            string Value = xmlNode.InnerXml;
            return Value;
        }

        //Set Node 
        public static string SetNode(string XML, string Node,string childNode)
        {
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.LoadXml(XML);

            XmlNode xmlNode = null;
            xmlNode = xmlDoc.SelectSingleNode(Node);

            xmlNode.InnerXml = childNode;
            return xmlDoc.InnerXml;
        }

        //Get Node List
        public static string[] GetNodeList(string XML, string Node)
        {
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.LoadXml(XML);

            XmlNodeList xmlNodeList = null;
            xmlNodeList = xmlDoc.SelectNodes(Node);
            XmlNode xmlNode = null;
            int i = 0;

            string[] Value = new string[xmlNodeList.Count];
            foreach (XmlNode xmlNode_loopVariable in xmlNodeList)
            {
                xmlNode = xmlNode_loopVariable;
                Value[i] = xmlNode.ChildNodes.Item(0).InnerText;
                i += 1;
            }
            return Value;
        }

        //Get Node Count
        public static int GetNodeCount(string XML, string Node)
        {
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.LoadXml(XML);

            XmlNodeList xmlNodeList = null;
            xmlNodeList = xmlDoc.SelectNodes(Node);

            int Count = xmlNodeList.Count;
            return Count;
        }

        //Get Node Count
        public static int GetNodeCount(string XML, string Node,int Item)
        {
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.LoadXml(XML);

            XmlNodeList xmlNodeList = null;
            xmlNodeList =  xmlDoc.SelectNodes(Node);
            
            int Count = xmlNodeList.Count;
            return Count;
        }



        //Get Node Attribute
        public static string GetNodeAttribute(string XML, string Node, int Item, int Attribute,string Text )
        {
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.LoadXml(XML);

            XmlNodeList xmlNodeList = null;
            xmlNodeList = xmlDoc.SelectNodes(Node);

            XmlNode xmlNode = xmlNodeList.Item(Item);

            if (xmlNode.Attributes[Attribute].Value == Text)
            {
                string Value = xmlNode.InnerText;
                return Value;
            }

            //if ((xmlNode.Attributes[Attribute] != null))
            //{
            //    string Value = xmlNode.Attributes[Attribute].Value;
            //    return Value;
            //}
            return "";
        }


        //Get Node Attribute
        public static string GetNodeAttributeAndName(string XML, string Node, int Item, int Attribute1, int Attribute2, string Text1, string Text2)
        {
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.LoadXml(XML);

            XmlNodeList xmlNodeList = null;
            xmlNodeList = xmlDoc.SelectNodes(Node);

            XmlNode xmlNode = xmlNodeList.Item(Item);

            if (xmlNode.Attributes[Attribute1].Value == Text1 && xmlNode.Attributes[Attribute2].Value == Text2 )
            {
                string Value = xmlNode.InnerText;
                return Value;
            }

            //if ((xmlNode.Attributes[Attribute] != null))
            //{
            //    string Value = xmlNode.Attributes[Attribute].Value;
            //    return Value;
            //}
            return "";
        }


        //Get Node Child
        public static string GetNodeChild(string XML, string Node, int Item, string Child)
        {
            XmlDocument xmlDoc = new XmlDocument();            
            xmlDoc.LoadXml(XML);

            XmlNodeList xmlNodeList = null;
            xmlNodeList = xmlDoc.SelectNodes(Node);

            XmlNode xmlNode = xmlNodeList.Item(Item);
            if ((xmlNode[Child] != null))
            {
                string Value = xmlNode[Child].InnerText;
                return Value;
            }
            return "";
        }
    }
}
