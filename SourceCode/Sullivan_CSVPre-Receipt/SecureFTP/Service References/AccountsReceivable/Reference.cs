﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SecureFTP.AccountsReceivable {
    
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.ServiceContractAttribute(Namespace="http://stratix.invera.com/services", ConfigurationName="AccountsReceivable.AccountsReceivableDistributionService")]
    public interface AccountsReceivableDistributionService {
        
        // CODEGEN: Generating message contract since message CreateARDistRequest has headers
        [System.ServiceModel.OperationContractAttribute(Action="http://stratix.invera.com/services/CreateARDist", ReplyAction="*")]
        [System.ServiceModel.FaultContractAttribute(typeof(string), Action="http://stratix.invera.com/services/CreateARDist", Name="StratixFault")]
        [System.ServiceModel.XmlSerializerFormatAttribute(SupportFaults=true)]
        SecureFTP.AccountsReceivable.CreateARDistResponse CreateARDist(SecureFTP.AccountsReceivable.CreateARDistRequest request);
        
        // CODEGEN: Generating message contract since message ModifyARDistRequest has headers
        [System.ServiceModel.OperationContractAttribute(Action="http://stratix.invera.com/services/ModifyARDist", ReplyAction="*")]
        [System.ServiceModel.FaultContractAttribute(typeof(string), Action="http://stratix.invera.com/services/ModifyARDist", Name="StratixFault")]
        [System.ServiceModel.XmlSerializerFormatAttribute(SupportFaults=true)]
        SecureFTP.AccountsReceivable.ModifyARDistResponse ModifyARDist(SecureFTP.AccountsReceivable.ModifyARDistRequest request);
        
        // CODEGEN: Generating message contract since message DeleteARDistRequest has headers
        [System.ServiceModel.OperationContractAttribute(Action="http://stratix.invera.com/services/DeleteARDist", ReplyAction="*")]
        [System.ServiceModel.FaultContractAttribute(typeof(string), Action="http://stratix.invera.com/services/DeleteARDist", Name="StratixFault")]
        [System.ServiceModel.XmlSerializerFormatAttribute(SupportFaults=true)]
        SecureFTP.AccountsReceivable.DeleteARDistResponse DeleteARDist(SecureFTP.AccountsReceivable.DeleteARDistRequest request);
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.7.2558.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://stratix.invera.com/schema/common/datatypes")]
    public partial class AuthenticationToken : object, System.ComponentModel.INotifyPropertyChanged {
        
        private string usernameField;
        
        private string valueField;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, Order=0)]
        public string username {
            get {
                return this.usernameField;
            }
            set {
                this.usernameField = value;
                this.RaisePropertyChanged("username");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, Order=1)]
        public string value {
            get {
                return this.valueField;
            }
            set {
                this.valueField = value;
                this.RaisePropertyChanged("value");
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.7.2558.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://stratix.invera.com/schema/common/datatypes")]
    public partial class DataElementMessageList : object, System.ComponentModel.INotifyPropertyChanged {
        
        private string dataElementField;
        
        private string[] messageListField;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, Order=0)]
        public string dataElement {
            get {
                return this.dataElementField;
            }
            set {
                this.dataElementField = value;
                this.RaisePropertyChanged("dataElement");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("messageList", Form=System.Xml.Schema.XmlSchemaForm.Unqualified, IsNullable=true, Order=1)]
        public string[] messageList {
            get {
                return this.messageListField;
            }
            set {
                this.messageListField = value;
                this.RaisePropertyChanged("messageList");
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.7.2558.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://stratix.invera.com/schema/common/datatypes")]
    public partial class ServiceMessages : object, System.ComponentModel.INotifyPropertyChanged {
        
        private string returnStatusField;
        
        private string[] messageListField;
        
        private DataElementMessageList[] dataElementMessageListField;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, Order=0)]
        public string returnStatus {
            get {
                return this.returnStatusField;
            }
            set {
                this.returnStatusField = value;
                this.RaisePropertyChanged("returnStatus");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("messageList", Form=System.Xml.Schema.XmlSchemaForm.Unqualified, IsNullable=true, Order=1)]
        public string[] messageList {
            get {
                return this.messageListField;
            }
            set {
                this.messageListField = value;
                this.RaisePropertyChanged("messageList");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("dataElementMessageList", Form=System.Xml.Schema.XmlSchemaForm.Unqualified, IsNullable=true, Order=2)]
        public DataElementMessageList[] dataElementMessageList {
            get {
                return this.dataElementMessageListField;
            }
            set {
                this.dataElementMessageListField = value;
                this.RaisePropertyChanged("dataElementMessageList");
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.7.2558.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://stratix.invera.com/services")]
    public partial class ARDist : object, System.ComponentModel.INotifyPropertyChanged {
        
        private int refItemNumberField;
        
        private bool refItemNumberFieldSpecified;
        
        private string invoiceNumberField;
        
        private decimal writeOffAmountField;
        
        private bool writeOffAmountFieldSpecified;
        
        private decimal discountTakenAmountField;
        
        private bool discountTakenAmountFieldSpecified;
        
        private decimal paymentAmountField;
        
        private bool paymentAmountFieldSpecified;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, Order=0)]
        public int refItemNumber {
            get {
                return this.refItemNumberField;
            }
            set {
                this.refItemNumberField = value;
                this.RaisePropertyChanged("refItemNumber");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool refItemNumberSpecified {
            get {
                return this.refItemNumberFieldSpecified;
            }
            set {
                this.refItemNumberFieldSpecified = value;
                this.RaisePropertyChanged("refItemNumberSpecified");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, Order=1)]
        public string invoiceNumber {
            get {
                return this.invoiceNumberField;
            }
            set {
                this.invoiceNumberField = value;
                this.RaisePropertyChanged("invoiceNumber");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, Order=2)]
        public decimal writeOffAmount {
            get {
                return this.writeOffAmountField;
            }
            set {
                this.writeOffAmountField = value;
                this.RaisePropertyChanged("writeOffAmount");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool writeOffAmountSpecified {
            get {
                return this.writeOffAmountFieldSpecified;
            }
            set {
                this.writeOffAmountFieldSpecified = value;
                this.RaisePropertyChanged("writeOffAmountSpecified");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, Order=3)]
        public decimal discountTakenAmount {
            get {
                return this.discountTakenAmountField;
            }
            set {
                this.discountTakenAmountField = value;
                this.RaisePropertyChanged("discountTakenAmount");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool discountTakenAmountSpecified {
            get {
                return this.discountTakenAmountFieldSpecified;
            }
            set {
                this.discountTakenAmountFieldSpecified = value;
                this.RaisePropertyChanged("discountTakenAmountSpecified");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, Order=4)]
        public decimal paymentAmount {
            get {
                return this.paymentAmountField;
            }
            set {
                this.paymentAmountField = value;
                this.RaisePropertyChanged("paymentAmount");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool paymentAmountSpecified {
            get {
                return this.paymentAmountFieldSpecified;
            }
            set {
                this.paymentAmountFieldSpecified = value;
                this.RaisePropertyChanged("paymentAmountSpecified");
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(WrapperName="CreateARDist", WrapperNamespace="http://stratix.invera.com/services", IsWrapped=true)]
    public partial class CreateARDistRequest {
        
        [System.ServiceModel.MessageHeaderAttribute(Namespace="http://stratix.invera.com/services")]
        public SecureFTP.AccountsReceivable.AuthenticationToken AuthenticationHeader;
        
        [System.ServiceModel.MessageBodyMemberAttribute(Namespace="http://stratix.invera.com/services", Order=0)]
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string cashRctIdentity;
        
        [System.ServiceModel.MessageBodyMemberAttribute(Namespace="http://stratix.invera.com/services", Order=1)]
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public SecureFTP.AccountsReceivable.ARDist arDist;
        
        public CreateARDistRequest() {
        }
        
        public CreateARDistRequest(SecureFTP.AccountsReceivable.AuthenticationToken AuthenticationHeader, string cashRctIdentity, SecureFTP.AccountsReceivable.ARDist arDist) {
            this.AuthenticationHeader = AuthenticationHeader;
            this.cashRctIdentity = cashRctIdentity;
            this.arDist = arDist;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(WrapperName="CreateARDistResponse", WrapperNamespace="http://stratix.invera.com/services", IsWrapped=true)]
    public partial class CreateARDistResponse {
        
        [System.ServiceModel.MessageHeaderAttribute(Namespace="http://stratix.invera.com/services")]
        public SecureFTP.AccountsReceivable.AuthenticationToken AuthenticationHeader;
        
        [System.ServiceModel.MessageHeaderAttribute(Namespace="http://stratix.invera.com/services")]
        public SecureFTP.AccountsReceivable.ServiceMessages ServiceMessagesHeader;
        
        [System.ServiceModel.MessageBodyMemberAttribute(Namespace="http://stratix.invera.com/services", Order=0)]
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public int refItemNumber;
        
        public CreateARDistResponse() {
        }
        
        public CreateARDistResponse(SecureFTP.AccountsReceivable.AuthenticationToken AuthenticationHeader, SecureFTP.AccountsReceivable.ServiceMessages ServiceMessagesHeader, int refItemNumber) {
            this.AuthenticationHeader = AuthenticationHeader;
            this.ServiceMessagesHeader = ServiceMessagesHeader;
            this.refItemNumber = refItemNumber;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(WrapperName="ModifyARDist", WrapperNamespace="http://stratix.invera.com/services", IsWrapped=true)]
    public partial class ModifyARDistRequest {
        
        [System.ServiceModel.MessageHeaderAttribute(Namespace="http://stratix.invera.com/services")]
        public SecureFTP.AccountsReceivable.AuthenticationToken AuthenticationHeader;
        
        [System.ServiceModel.MessageBodyMemberAttribute(Namespace="http://stratix.invera.com/services", Order=0)]
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string cashRctIdentity;
        
        [System.ServiceModel.MessageBodyMemberAttribute(Namespace="http://stratix.invera.com/services", Order=1)]
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public SecureFTP.AccountsReceivable.ARDist arDist;
        
        public ModifyARDistRequest() {
        }
        
        public ModifyARDistRequest(SecureFTP.AccountsReceivable.AuthenticationToken AuthenticationHeader, string cashRctIdentity, SecureFTP.AccountsReceivable.ARDist arDist) {
            this.AuthenticationHeader = AuthenticationHeader;
            this.cashRctIdentity = cashRctIdentity;
            this.arDist = arDist;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(WrapperName="ModifyARDistResponse", WrapperNamespace="http://stratix.invera.com/services", IsWrapped=true)]
    public partial class ModifyARDistResponse {
        
        [System.ServiceModel.MessageHeaderAttribute(Namespace="http://stratix.invera.com/services")]
        public SecureFTP.AccountsReceivable.AuthenticationToken AuthenticationHeader;
        
        [System.ServiceModel.MessageHeaderAttribute(Namespace="http://stratix.invera.com/services")]
        public SecureFTP.AccountsReceivable.ServiceMessages ServiceMessagesHeader;
        
        public ModifyARDistResponse() {
        }
        
        public ModifyARDistResponse(SecureFTP.AccountsReceivable.AuthenticationToken AuthenticationHeader, SecureFTP.AccountsReceivable.ServiceMessages ServiceMessagesHeader) {
            this.AuthenticationHeader = AuthenticationHeader;
            this.ServiceMessagesHeader = ServiceMessagesHeader;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(WrapperName="DeleteARDist", WrapperNamespace="http://stratix.invera.com/services", IsWrapped=true)]
    public partial class DeleteARDistRequest {
        
        [System.ServiceModel.MessageHeaderAttribute(Namespace="http://stratix.invera.com/services")]
        public SecureFTP.AccountsReceivable.AuthenticationToken AuthenticationHeader;
        
        [System.ServiceModel.MessageBodyMemberAttribute(Namespace="http://stratix.invera.com/services", Order=0)]
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string cashRctIdentity;
        
        [System.ServiceModel.MessageBodyMemberAttribute(Namespace="http://stratix.invera.com/services", Order=1)]
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public int refItemNumber;
        
        public DeleteARDistRequest() {
        }
        
        public DeleteARDistRequest(SecureFTP.AccountsReceivable.AuthenticationToken AuthenticationHeader, string cashRctIdentity, int refItemNumber) {
            this.AuthenticationHeader = AuthenticationHeader;
            this.cashRctIdentity = cashRctIdentity;
            this.refItemNumber = refItemNumber;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(WrapperName="DeleteARDistResponse", WrapperNamespace="http://stratix.invera.com/services", IsWrapped=true)]
    public partial class DeleteARDistResponse {
        
        [System.ServiceModel.MessageHeaderAttribute(Namespace="http://stratix.invera.com/services")]
        public SecureFTP.AccountsReceivable.AuthenticationToken AuthenticationHeader;
        
        [System.ServiceModel.MessageHeaderAttribute(Namespace="http://stratix.invera.com/services")]
        public SecureFTP.AccountsReceivable.ServiceMessages ServiceMessagesHeader;
        
        public DeleteARDistResponse() {
        }
        
        public DeleteARDistResponse(SecureFTP.AccountsReceivable.AuthenticationToken AuthenticationHeader, SecureFTP.AccountsReceivable.ServiceMessages ServiceMessagesHeader) {
            this.AuthenticationHeader = AuthenticationHeader;
            this.ServiceMessagesHeader = ServiceMessagesHeader;
        }
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public interface AccountsReceivableDistributionServiceChannel : SecureFTP.AccountsReceivable.AccountsReceivableDistributionService, System.ServiceModel.IClientChannel {
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public partial class AccountsReceivableDistributionServiceClient : System.ServiceModel.ClientBase<SecureFTP.AccountsReceivable.AccountsReceivableDistributionService>, SecureFTP.AccountsReceivable.AccountsReceivableDistributionService {
        
        public AccountsReceivableDistributionServiceClient() {
        }
        
        public AccountsReceivableDistributionServiceClient(string endpointConfigurationName) : 
                base(endpointConfigurationName) {
        }
        
        public AccountsReceivableDistributionServiceClient(string endpointConfigurationName, string remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public AccountsReceivableDistributionServiceClient(string endpointConfigurationName, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public AccountsReceivableDistributionServiceClient(System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(binding, remoteAddress) {
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        SecureFTP.AccountsReceivable.CreateARDistResponse SecureFTP.AccountsReceivable.AccountsReceivableDistributionService.CreateARDist(SecureFTP.AccountsReceivable.CreateARDistRequest request) {
            return base.Channel.CreateARDist(request);
        }
        
        public SecureFTP.AccountsReceivable.ServiceMessages CreateARDist(ref SecureFTP.AccountsReceivable.AuthenticationToken AuthenticationHeader, string cashRctIdentity, SecureFTP.AccountsReceivable.ARDist arDist, out int refItemNumber) {
            SecureFTP.AccountsReceivable.CreateARDistRequest inValue = new SecureFTP.AccountsReceivable.CreateARDistRequest();
            inValue.AuthenticationHeader = AuthenticationHeader;
            inValue.cashRctIdentity = cashRctIdentity;
            inValue.arDist = arDist;
            SecureFTP.AccountsReceivable.CreateARDistResponse retVal = ((SecureFTP.AccountsReceivable.AccountsReceivableDistributionService)(this)).CreateARDist(inValue);
            AuthenticationHeader = retVal.AuthenticationHeader;
            refItemNumber = retVal.refItemNumber;
            return retVal.ServiceMessagesHeader;
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        SecureFTP.AccountsReceivable.ModifyARDistResponse SecureFTP.AccountsReceivable.AccountsReceivableDistributionService.ModifyARDist(SecureFTP.AccountsReceivable.ModifyARDistRequest request) {
            return base.Channel.ModifyARDist(request);
        }
        
        public SecureFTP.AccountsReceivable.ServiceMessages ModifyARDist(ref SecureFTP.AccountsReceivable.AuthenticationToken AuthenticationHeader, string cashRctIdentity, SecureFTP.AccountsReceivable.ARDist arDist) {
            SecureFTP.AccountsReceivable.ModifyARDistRequest inValue = new SecureFTP.AccountsReceivable.ModifyARDistRequest();
            inValue.AuthenticationHeader = AuthenticationHeader;
            inValue.cashRctIdentity = cashRctIdentity;
            inValue.arDist = arDist;
            SecureFTP.AccountsReceivable.ModifyARDistResponse retVal = ((SecureFTP.AccountsReceivable.AccountsReceivableDistributionService)(this)).ModifyARDist(inValue);
            AuthenticationHeader = retVal.AuthenticationHeader;
            return retVal.ServiceMessagesHeader;
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        SecureFTP.AccountsReceivable.DeleteARDistResponse SecureFTP.AccountsReceivable.AccountsReceivableDistributionService.DeleteARDist(SecureFTP.AccountsReceivable.DeleteARDistRequest request) {
            return base.Channel.DeleteARDist(request);
        }
        
        public SecureFTP.AccountsReceivable.ServiceMessages DeleteARDist(ref SecureFTP.AccountsReceivable.AuthenticationToken AuthenticationHeader, string cashRctIdentity, int refItemNumber) {
            SecureFTP.AccountsReceivable.DeleteARDistRequest inValue = new SecureFTP.AccountsReceivable.DeleteARDistRequest();
            inValue.AuthenticationHeader = AuthenticationHeader;
            inValue.cashRctIdentity = cashRctIdentity;
            inValue.refItemNumber = refItemNumber;
            SecureFTP.AccountsReceivable.DeleteARDistResponse retVal = ((SecureFTP.AccountsReceivable.AccountsReceivableDistributionService)(this)).DeleteARDist(inValue);
            AuthenticationHeader = retVal.AuthenticationHeader;
            return retVal.ServiceMessagesHeader;
        }
    }
}
