﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Net.Mail;
using System.Configuration;
using System.Threading;
using System.Data;
using System.Text;
using iTECH.Library.Utilities;

namespace SecureFTP
{

    public class EmailManager
    {
        private MailMessage _emailMessage;
        private bool _isHtml = false;

        public EmailManager()
        {

        }

        public EmailManager(bool isHtml)
        {
            _isHtml = isHtml;
        }

        public bool SendEmail(string subject, string body, string fromAddress, string toAddress)
        {
            try
            {
                _emailMessage = new MailMessage(fromAddress, toAddress, subject, body);
                if (EmailSettings.IsAsynchronousMail)
                {
                    Thread thread = new Thread(new ThreadStart(AsyncMail));
                    thread.Start();
                }
                else
                {
                    AsyncMail();
                }

                return true;
            }
            catch
            {

                throw;
            }
        }

        public bool SendEmail(string subject, string body, MailAddress fromAddress, MailAddress toAddress)
        {
            try
            {
                _emailMessage = new MailMessage(fromAddress, toAddress);
                _emailMessage.Subject = subject;
                _emailMessage.Body = body;
                if (EmailSettings.IsAsynchronousMail)
                {
                    Thread thread = new Thread(new ThreadStart(AsyncMail));
                    thread.Start();
                }
                else
                {
                    AsyncMail();
                }

                return true;
            }
            catch
            {

                throw;
            }
        }

        public bool SendEmail(string subject, string body, MailAddress fromAddress, List<MailAddress> toAddresses)
        {
            try
            {
                _emailMessage = new MailMessage();
                _emailMessage.From = fromAddress;
                foreach (MailAddress item in toAddresses)
                {
                    _emailMessage.To.Add(item);
                }
                _emailMessage.Subject = subject;
                _emailMessage.Body = body;
                if (EmailSettings.IsAsynchronousMail)
                {
                    Thread thread = new Thread(new ThreadStart(AsyncMail));
                    thread.Start();
                }
                else
                {
                    AsyncMail();
                }

                return true;
            }
            catch
            {

                throw;
            }
        }

        public bool SendEmail(string subject, string body, string fromAddress, string toAddress, string attahcedFilePath)
        {
            try
            {
                _emailMessage = new MailMessage(fromAddress, toAddress, subject, body);
                _emailMessage.Attachments.Add(new Attachment(attahcedFilePath));
                if (EmailSettings.IsAsynchronousMail)
                {
                    Thread thread = new Thread(new ThreadStart(AsyncMail));
                    thread.Start();
                }
                else
                {
                    AsyncMail();
                }

                return true;
            }
            catch
            {

                throw;
            }
        }

        private void AsyncMail()
        {
            try
            {
                _emailMessage.IsBodyHtml = _isHtml;

                SmtpClient emailClient = new SmtpClient(EmailSettings.SMTPServer);
                emailClient.Host = EmailSettings.SMTPServer;
                emailClient.UseDefaultCredentials = EmailSettings.UseDefaultCredentials;
                emailClient.Port = EmailSettings.SMTPPort;
                if (EmailSettings.UseDefaultCredentials)
                {
                    emailClient.Credentials = new System.Net.NetworkCredential(EmailSettings.SMTPUserEmail, EmailSettings.SMTPPassword);
                }
                emailClient.Send(_emailMessage);
            }
            catch(Exception ex)
            {
                ErrorLog.createLog("Error during Generated File submit");
                ErrorLog.CreateLog(ex);
                //throw;
            }
        }

    }

    public class EmailSettings
    {
        public static string SMTPServer
        {
            get;set;
           
        }
        public static int SMTPPort
        {
            get; set;
        }
        public static string SMTPUserEmail
        {
            get; set;
        }
        public static string SMTPPassword
        {
            get; set;
        }

        public static bool IsAsynchronousMail
        {
            get; set;
        }

        public static bool UseDefaultCredentials
        {
            get; set;
        }
        public static string ErrorEmailFrom
        {
            get
            {
                if (ConfigurationManager.AppSettings["ErrorEmailFrom"] != null)
                {
                    return ConfigurationManager.AppSettings["ErrorEmailFrom"];
                }
                return string.Empty;
            }
            set { }
        }
        public static string ErrorEmailTo
        {
            get
            {
                if (ConfigurationManager.AppSettings["ErrorEmailTo"] != null)
                {
                    return ConfigurationManager.AppSettings["ErrorEmailTo"];
                }
                return string.Empty;
            }
            set { }
        }
        public static string ErrorSubject
        {
            get
            {
                if (ConfigurationManager.AppSettings["ErrorSubject"] != null)
                {
                    return ConfigurationManager.AppSettings["ErrorSubject"];
                }
                return "Error Email";
            }
            set { }
        }
    }

    public class EmailHelper
    {
        public static bool SendEmail(string fromAddress, string toAddress, string message, string sub, string attachedFilePath)
        {
            try
            {
                EmailManager manager = new EmailManager(true);
                return manager.SendEmail(sub, message, fromAddress, toAddress, attachedFilePath);
            }
            catch
            {
                throw;
            }
        }

        public static bool SendEmail(string fromAddress, string toAddress, string message, string sub, bool isHtml)
        {
            try
            {
                EmailManager manager = new EmailManager(isHtml);
                return manager.SendEmail(sub, message, fromAddress, toAddress);
            }
            catch
            {

                throw;
            }
        }

        public static bool SendEmail(string fromAddress, string fromName, string toAddress, string toName, string message, string sub, bool isHtml)
        {
            try
            {
                EmailManager manager = new EmailManager(isHtml);
                return manager.SendEmail(sub, message, new MailAddress(fromAddress, fromName), new MailAddress(toAddress, toName));
            }
            catch
            {

                throw;
            }
        }

        public static bool SendEmail(MailAddress from, List<MailAddress> toList, string message, string sub, bool isHtml)
        {
            try
            {
                EmailManager manager = new EmailManager(isHtml);
                return manager.SendEmail(sub, message, from, toList);
            }
            catch
            {
                throw;
            }
        }

        public static bool SendErrorMail(string message)
        {
            try
            {
                EmailManager manager = new EmailManager(true);
                return manager.SendEmail(EmailSettings.ErrorSubject, message, EmailSettings.ErrorEmailFrom, EmailSettings.ErrorEmailTo);
            }
            catch
            {

                throw;
            }
        }

        /// <summary>
        /// Source of help: http://www.ahmedblog.com/Blogs/ShowBlog.aspx?Id=2dc7e9e7-92b4-48cc-a7d1-d6535e5554ab
        /// 
        /// </summary>
        /// <param name="from"></param>
        /// <param name="to"></param>
        /// <param name="sub">Character lenght limit is 65</param>
        /// <param name="body"></param>
        /// <param name="location"></param>
        /// <param name="starUtctDate"></param>
        /// <param name="endUtcDate"></param>
        /// <returns></returns>
        public static bool SendAppointment(string from, string to, string body, string sub, string location, DateTime starUtctDate, DateTime endUtcDate)
        {
            try
            {
                SmtpClient sc = new SmtpClient(EmailSettings.SMTPServer);
                sc.Host = EmailSettings.SMTPServer;
                sc.UseDefaultCredentials = EmailSettings.UseDefaultCredentials;
                if (sc.UseDefaultCredentials)
                {
                    sc.Credentials = new System.Net.NetworkCredential(EmailSettings.SMTPUserEmail, EmailSettings.SMTPPassword);
                }
                sc.Port = EmailSettings.SMTPPort;

                MailMessage msg = new MailMessage();
                msg.From = new MailAddress(from);
                msg.To.Add(new MailAddress(to));
                msg.Subject = sub;
                msg.Body = body;

                StringBuilder str = new StringBuilder();
                str.AppendLine("BEGIN:VCALENDAR");
                str.AppendLine("PRODID:-//Appointment");
                str.AppendLine("VERSION:2.0");
                str.AppendLine("METHOD:REQUEST");
                str.AppendLine("BEGIN:VEVENT");
                str.AppendLine(string.Format("DTSTART:{0:yyyyMMddTHHmmssZ}", starUtctDate));
                str.AppendLine(string.Format("DTSTAMP:{0:yyyyMMddTHHmmssZ}", DateTime.UtcNow));
                str.AppendLine(string.Format("DTEND:{0:yyyyMMddTHHmmssZ}", endUtcDate));
                str.AppendLine(string.Format("LOCATION: {0}", location));
                str.AppendLine(string.Format("UID:{0}", Guid.NewGuid()));
                str.AppendLine(string.Format("DESCRIPTION:{0}", BusinessUtility.StripHtml(msg.Body)));
                str.AppendLine(string.Format("X-ALT-DESC;FMTTYPE=text/html:{0}", msg.Body));
                str.AppendLine(string.Format("SUMMARY:{0}", msg.Subject));
                str.AppendLine(string.Format("ORGANIZER:MAILTO:{0}", msg.From.Address));

                str.AppendLine(string.Format("ATTENDEE;CN=\"{0}\";RSVP=TRUE:mailto:{1}", msg.To[0].DisplayName, msg.To[0].Address));

                str.AppendLine("BEGIN:VALARM");
                str.AppendLine("TRIGGER:-PT15M");
                str.AppendLine("ACTION:DISPLAY");
                str.AppendLine("DESCRIPTION:Reminder");
                str.AppendLine("END:VALARM");
                str.AppendLine("END:VEVENT");
                str.AppendLine("END:VCALENDAR");
                System.Net.Mime.ContentType ct = new System.Net.Mime.ContentType("text/calendar");
                ct.Parameters.Add("method", "REQUEST");
                AlternateView avCal = AlternateView.CreateAlternateViewFromString(str.ToString(), ct);
                msg.AlternateViews.Add(avCal);
                AlternateView avBody = AlternateView.CreateAlternateViewFromString(msg.Body, new System.Net.Mime.ContentType("text/html"));
                msg.AlternateViews.Add(avBody);


                sc.Send(msg);
                return true;
            }
            catch
            {

                throw;
            }
        }
    }

    
}
