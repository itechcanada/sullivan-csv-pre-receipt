﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace SecureFTP
{
    class InputOutput
    {


        public static void CheckDirectoryAndCreate (string sDirectory)
        {
            try
            {
                if (!Directory.Exists(sDirectory))
                {
                    Directory.CreateDirectory(sDirectory);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static bool DirectoryExists(string sDir)
        {
            return Directory.Exists(sDir);
        }

        public static bool FileExistsInDirectory(string sDir)
        {
            return File.Exists(sDir);
        }

        public static void MoveDirectory(string source, string target, bool bMoveDir = true)
        {
            var stack = new Stack<Folders>();
            stack.Push(new Folders(source, target));

            while (stack.Count > 0)
            {
                var folders = stack.Pop();
                Directory.CreateDirectory(folders.Target);
                foreach (var file in Directory.GetFiles(folders.Source, "*.*"))
                {
                    string targetFile = Path.Combine(folders.Target, Path.GetFileName(file));
                    if (File.Exists(targetFile)) File.Delete(targetFile);
                    File.Move(file, targetFile);
                }
                if (bMoveDir)
                {
                    foreach (var folder in Directory.GetDirectories(folders.Source))
                    {
                        stack.Push(new Folders(folder, Path.Combine(folders.Target, Path.GetFileName(folder))));
                    }
                }
            }
            Cleandirectory(source);
        }


        public static void CopyFileFromSourceToDestination(string sSource, string sDestination)
        {
            try
            {
                // http://www.dotnetfunda.com/articles/show/1312/detect-file-copy-completion-in-filesystemwatcher
                // http://stackoverflow.com/questions/10982104/wait-until-file-is-completely-written
                // http://stackoverflow.com/questions/1406808/wait-for-file-to-be-freed-by-process

                //http://stackoverflow.com/questions/50744/wait-until-file-is-unlocked-in-net


                File.Copy(sSource, sDestination, true);
            }
            catch (Exception ex)
            {
                ErrorLog.createLog("Error in CopyFileFromSourceToDestination");
            }

        }


        /// <summary>
        /// Blocks until the file is not locked any more.
        /// </summary>
        /// <param name="fullPath"></param>
        public static bool WaitForFile(string fullPath)
        {
            int numTries = 0;
            while (true)
            {
                ++numTries;
                try
                {
                    // Attempt to open the file exclusively.
                    using (FileStream fs = new FileStream(fullPath,
                        FileMode.Open, FileAccess.ReadWrite,
                        FileShare.None, 100))
                    {
                        fs.ReadByte();

                        // If we got this far the file is ready
                        break;
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine("WaitForFile {0} failed to get an exclusive lock: {1}", fullPath, ex.ToString());

                    if (numTries > 10)
                    {
                        Console.WriteLine("WaitForFile {0} giving up after 10 tries", fullPath);
                        return false;
                    }

                    // Wait for the lock to be released
                    System.Threading.Thread.Sleep(500);
                }
            }

            Console.WriteLine("WaitForFile {0} returning true after {1} tries", fullPath, numTries);
            return true;
        }



        public static void Cleandirectory(string sSource)
        {
            Array.ForEach(Directory.GetFiles(sSource), File.Delete);
        }

        public static string GetFileNameFromPath(string sPath)
        {
            string sName = string.Empty;
            try
            {
               sName = Path.GetFileName(sPath);
            }
            catch (Exception ex)
            {

            }
            return sName;
        }




    }


    public class Folders
    {
        public string Source { get; private set; }
        public string Target { get; private set; }

        public Folders(string source, string target)
        {
            Source = source;
            Target = target;
        }
    }


}
