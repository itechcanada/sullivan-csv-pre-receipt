﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Text;
using iTECH.Library.Utilities;

namespace SecureFTP
{
    class Measurment
    {

        string m_sSql = string.Empty;

        static string sMysqlConnectionString = ConfigurationManager.ConnectionStrings["NewConnectionString"].ToString();

        string sVendorId = string.Empty;
        public string VendorId
        {
            get { return sVendorId; }
        }

        string sUOM = string.Empty;
        public string UOM
        {
            get { return sUOM; }
        }

        string sEdiUOM = string.Empty;
        public string EdiUOM
        {
            get { return sEdiUOM; }
        }

        string sMulVal = string.Empty;
        public string MulVal
        {
            get { return sMulVal; }
        }


        public Measurment()
        {
           
        }

        public Measurment(string iVendorId, string sUom)
        {
            sVendorId = iVendorId.ToString();
            sUOM = sUom;
            m_sSql = "select * from measurment where VendorId = '"+ iVendorId +"' and uom = '" + sUom + "'";
            ErrorLog.createLog(m_sSql);
            iTECH.Library.DataAccess.MySql.DbHelper oDbHelper = new iTECH.Library.DataAccess.MySql.DbHelper(sMysqlConnectionString, true);

            try
            {
                MySqlParameter[] oMySqlParameter = { iTECH.Library.DataAccess.MySql.DbUtility.GetParameter("VendorId", sVendorId, iTECH.Library.DataAccess.MySql.MyDbType.String),
                                                 iTECH.Library.DataAccess.MySql.DbUtility.GetParameter("uom", sUOM, iTECH.Library.DataAccess.MySql.MyDbType.String)
                                               };

                DataTable oDtAddress = new iTECH.Library.DataAccess.MySql.DbHelper(sMysqlConnectionString, true).GetDataTable(m_sSql, CommandType.Text, oMySqlParameter);
                foreach (DataRow oDataRow in oDtAddress.Rows)
                {
                    sEdiUOM = BusinessUtility.GetString(oDataRow["EdiUOM"]);
                    sMulVal = BusinessUtility.GetString(oDataRow["MulVal"]);
                }
            }
            catch (Exception ex)
            {
                ErrorLog.createLog("Error in Measurment : " + ex.ToString());
                ErrorLog.CreateLog(ex);
            }
            finally
            {
                oDbHelper.CloseDatabaseConnection();
            }
        }


    }
}
