﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using iTECH.Library.DataAccess.ODBC;
using iTECH.Library.Utilities;
using System.Data;
using MySql.Data.MySqlClient;

namespace SecureFTP.Utility
{
    class StratixConnection
    {
        static string sStratixConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ToString();
        public static DbHelper CreateStratixConnection(bool bConnectionOpen = true)
        {
            DbHelper oDbHelper = null;
            try
            {
                oDbHelper = new DbHelper(sStratixConnectionString, bConnectionOpen);
            }
            catch (Exception ex)
            {

            }
            finally
            {
                if (!bConnectionOpen)
                {
                    oDbHelper.CloseDatabaseConnection(); 
                }
            }
            return oDbHelper;
        }


        public static void CloseStratixDbConnection(DbHelper odbHelpr)
        {
            odbHelpr.CloseDatabaseConnection();
        }


    }
}
