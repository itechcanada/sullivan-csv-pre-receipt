﻿
using iTECH.Library.Utilities;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SecureFTP
{
    // this extract all ST and SE segments.
    class TransactionSeperator
    {

        //string[] m_sArr = null;
        public Dictionary<int, ArrayList> m_dict = new Dictionary<int, ArrayList>();
        ArrayList m_arl = new ArrayList();
        static string m_sStartBlock = "ST";
        static string m_sEndBlock = "SE";
        static string m_sInterchangeControlTrailer = "IEA";

        // m_dict holds total st se sections.
        public void GetStSeSegment(string[] m_sArr)
        {
            bool bStartBlockFound = false;
            bool bEndBlockFound = false;
            bool bInterchangeControlTrailer = false;
            string sSegment = string.Empty;
            int iNoOfblock = 0;
            bool bInEndBlock = false;
            for (int i = 0; i < m_sArr.Length; i++)
            {
                sSegment = BusinessUtility.GetString(m_sArr[i]);
                bStartBlockFound = sSegment.Substring(0, 2).Equals(m_sStartBlock);
                bEndBlockFound = sSegment.Substring(0, 2).Equals(m_sEndBlock);
                bInterchangeControlTrailer = sSegment.Substring(0, 3).Equals(m_sInterchangeControlTrailer);

                if (!bStartBlockFound)
                {
                    m_arl.Add(sSegment);
                }
                if (bInEndBlock)
                {
                    m_arl.Add(sSegment);
                    bInEndBlock = false;
                }
                if (bStartBlockFound && iNoOfblock == 0)
                {
                    if (!m_dict.ContainsKey(iNoOfblock))
                    {
                        m_dict.Add(iNoOfblock, m_arl);
                    }
                    else
                    {
                        m_dict.Add(iNoOfblock + 1, m_arl);
                    }
                    iNoOfblock++;
                    m_arl = new ArrayList();
                    m_arl.Add(sSegment);
                }
                else if (bEndBlockFound)
                {
                    bInEndBlock = true;
                    if (!m_dict.ContainsKey(iNoOfblock))
                    {
                        m_dict.Add(iNoOfblock, m_arl);
                    }
                    else
                    {
                        m_dict.Add(iNoOfblock + 1, m_arl);
                    }
                    iNoOfblock++;
                    m_arl = new ArrayList();
                    //m_arl.Add(sSegment);
                }
                else if (bInterchangeControlTrailer)
                {
                    if (!m_dict.ContainsKey(iNoOfblock))
                    {
                        m_dict.Add(iNoOfblock, m_arl);
                    }
                    else
                    {
                        m_dict.Add(iNoOfblock + 1, m_arl);
                    }
                    iNoOfblock++;
                    bInEndBlock = false;
                    m_arl = new ArrayList();
                }


            }

        } // start processing end here..



    }
}
