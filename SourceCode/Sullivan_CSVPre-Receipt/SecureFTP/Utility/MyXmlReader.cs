﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

    public class MyXmlReader
    {
        
            XmlDocument xmlDoc = new XmlDocument();

            public MyXmlReader(string xmlString)
            {
                xmlDoc.LoadXml(xmlString);
            }

            //Get Node Text
            public string GetNodeText(string node)
            {
                XmlNode xmlNode = xmlDoc.SelectSingleNode(node);
                return xmlNode.InnerText;
            }

        //Get Node Text
        public string GetNodeXml(string node)
        {
            XmlNode xmlNode = xmlDoc.SelectSingleNode(node);
            return xmlNode.InnerXml;
        }

        public XmlNode GetNode(string node)
            {
                return xmlDoc.SelectSingleNode(node);
            }

            //Get Node List
            public string[] GetNodeListArray(string node)
            {
                XmlNodeList xmlNodeList = xmlDoc.SelectNodes(node);
                List<string> lstNodes = new List<string>();
                foreach (XmlNode xmlNode in xmlNodeList)
                {
                    if (xmlNode.ChildNodes.Count > 0)
                    {
                        lstNodes.Add(xmlNode.ChildNodes[0].InnerText);
                    }
                }
                return lstNodes.ToArray();
            }

            public XmlNodeList GetNodeList(string node)
            {
                return xmlDoc.SelectNodes(node);
            }

            //Get Node Count
            public int GetNodeCount(string node)
            {
                XmlNodeList xmlNodeList = default(XmlNodeList);
                xmlNodeList = xmlDoc.SelectNodes(node);

                int Count = xmlNodeList.Count;
                return Count;
            }

            //Get Node Attribute
            public string GetNodeAttribute(string node, int item, string attribute)
            {
                XmlNodeList xmlNodeList = xmlDoc.SelectNodes(node);

                XmlNode xmlNode = xmlNodeList.Item(item);
                if ((xmlNode.Attributes[attribute] != null))
                {
                    string Value = xmlNode.Attributes[attribute].Value;
                    return Value;
                }
                return "";
            }

            //Get Node Child
            public string GetNodeChild(string node, int item, string child)
            {
                XmlNodeList xmlNodeList = xmlDoc.SelectNodes(node);

                XmlNode xmlNode = xmlNodeList.Item(item);
                if ((xmlNode[child] != null))
                {
                    string Value = xmlNode[child].InnerText;
                    return Value;
                }
                return "";
            }




    public string GetDunsNO()
    {
        string sReturn = string.Empty;
        try
        {
            XmlElement root = xmlDoc.DocumentElement;
            sReturn = root.Attributes["SenderID"].Value;      //Picking ReceiverID as VendorDunsNo    2020/03/17  Sumit     Code reverted   2020/04/03
            //sReturn = root.Attributes["ReceiverID"].Value;
        }
        catch(Exception ex)
        { }
        return sReturn;
    }

    public string GetEdiType(XmlNodeList oLst)
    {
        string sReturn = string.Empty;
        try
        {
            foreach (XmlElement oNode in oLst)
            {
                XmlElement root = oNode;
                sReturn = root.Attributes["TypeCode"].Value;
            }  
        }
        catch (Exception ex)
        { }
        return sReturn;
    } 




}
