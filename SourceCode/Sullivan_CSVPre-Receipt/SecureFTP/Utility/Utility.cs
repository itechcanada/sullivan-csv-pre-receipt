﻿using iTECH.Library.Utilities;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml;

namespace SecureFTP
{
     class Utility
    {

        private static string EdiFile = System.Configuration.ConfigurationManager.AppSettings["EdiFile"];
        public static string[] ConvertArrayListToArray(ArrayList oArrayList)
        {
            object[] sArr = null;
            sArr = oArrayList.ToArray();

            string[] arr = ((IEnumerable)sArr).Cast<object>()
                                 .Select(x => x.ToString())
                                 .ToArray();
            return arr;

        }


        public static void AddArrayListToList(ref ArrayList arLst, ref List<string> lst)
        {
            foreach (string s in arLst)
            {
                lst.Add(RemoveLineBreaker(s));
            }
        }

        public static void AddArrayToList(string[] strArray, ref List<string> lst)
        {
            lst = new List<string>(strArray);
           // List<string> lstArray = strArray.toList<string>;
        }


        public static string RemoveLineBreaker(object obj)
        {
            return BusinessUtility.GetString(obj).Substring(0, BusinessUtility.GetString(obj).Length - 1);
        }


        public static void AddElementBetweenArrayWithStartPosToList(ref List<string> oList, string[] sArr, int iStartPos, int iEndPos)
        {
            int iPos = 0;
            oList.Clear();
            foreach (var lineData in sArr)
            {
                if (iPos >= iStartPos && iPos < iEndPos)
                {

                    oList.Add(RemoveLineBreaker(lineData));
                }
                iPos++;
            }
        }


        public static string[] SplitString(string sString, char cSpliter)
        {
            string[] sArr = new string[] { };
            if (!string.IsNullOrEmpty(sString))
            {
                sArr = sString.Split(cSpliter);
            }
            return sArr;
        }

        public static bool IsNumber(string s)
        {
            int n;
            bool isNumeric = int.TryParse(s, out n);
            return isNumeric;
        }

        public static int GetIntFromString(string str)
        {
            string sNum = string.Empty;
            try
            {
                string[] sPoAndItem = str.Split('-');
                if (sPoAndItem.Length > 1)
                {
                    if (IsNumber(sPoAndItem[0]))
                    {
                        sNum = sPoAndItem[0];
                    }
                    else if (IsNumber(sPoAndItem[1]))
                    {
                        sNum = sPoAndItem[1];
                    }
                    //oheader863.OrigPoItem = sPoAndItem[1];
                }
                else
                {
                    sNum = Regex.Replace(str, "[^0-9]+", string.Empty);
                }
            }
            catch (Exception ex)
            { ErrorLog.CreateLog(ex); }
            return int.Parse(sNum);
        }

        public static decimal PutDecimalAtSpecificPlace(int iNum)
        {
           decimal dNum = (decimal)iNum / 100;
           return dNum;
        }


        public static void WaitForSecond(int iSecond)
        {
            // Thread.Sleep() wants a number of milliseconds to wait
            // 1 sec = 1000 ms, 60 sec = 60000 ms
            try
            {
                Console.WriteLine("Control is waiting for " + iSecond  + " Second for next event");
                System.Threading.Thread.Sleep(iSecond * 1000);
                Console.WriteLine("Resuming system...");
            }
            catch (Exception ex)
            {
                ErrorLog.createLog(ex.ToString());
            }
        }


        public static string GetConfigValue(string sKey)
        {
            string sValue = string.Empty;
            try
            {
                if (!string.IsNullOrEmpty(System.Configuration.ConfigurationManager.AppSettings[sKey]))
                {
                    sValue = System.Configuration.ConfigurationManager.AppSettings[sKey];
                }
            }
            catch (Exception ex)
            {

            }

            return sValue;
        }


        public static void CheckDirectoryAndCreate(string sPath)
        {
            try
            {
                if (!Directory.Exists(sPath))
                {
                    Directory.CreateDirectory(sPath);
                }
            }
            catch (Exception ex)
            {
                ErrorLog.createLog(ex.ToString());
            }
        }


        public static void CreateAndWriteFile(string sPath, string sText, bool bAppend)
        {
            try
            {
                if (!File.Exists(sPath))
                {
                    var objFile = File.Create(sPath);
                    objFile.Close();
                    TextWriter tw = new StreamWriter(sPath);
                    tw.WriteLine(sText);
                    tw.Close();
                }
                else if (File.Exists(sPath))
                {
                    TextWriter tw = new StreamWriter(sPath, bAppend);
                    tw.WriteLine(sText);
                    tw.Close();
                }

                //using (System.IO.StreamWriter sw = System.IO.File.AppendText(sPath))
                //{
                //    sw.WriteLine(sText);
                //    sw.Close();
                //}
            }
            catch (Exception ex)
            {
                ErrorLog.createLog(ex.ToString());
            }

        }

        public static string[] GetSplitedArray(object obj)
        {
            // convert object to array
            string[] sArr = ((IEnumerable)obj).Cast<object>()
                                 .Select(x => x.ToString())
                                 .ToArray();


            if (sArr.Length == 1)
            {
                Configuration oConfiguration;

                oConfiguration = new Configuration();
                oConfiguration.PopulateConfigurationData(sArr);
                EdiFile = oConfiguration.FileType;


                var varLinesNew = sArr[0].Split(new string[] { oConfiguration.LineSeperator }, StringSplitOptions.None);
                sArr = varLinesNew;
            }
            sArr = sArr.Where(x => !string.IsNullOrEmpty(x)).ToArray();
            return sArr;
        }


        public static string GetElementValue(string sElementLine, string sElementToFetch)
        {
            string sReturn = string.Empty;
            try
            {
                int iIndex = 0;
                string[] sElemArr = sElementLine.Split(Globalcl.ElementSeperator);
                if (sElemArr.Length > 0)
                {
                    foreach (string s in sElemArr)
                    {
                        if (s == sElementToFetch)
                        {
                            if (iIndex < sElemArr.Length)
                            {
                                sReturn = sElemArr[iIndex + 1];
                                break;
                            }
                        }
                        iIndex++;
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorLog.createLog(ex.ToString());
            }
            return sReturn;
        }


        public static int GetLinItem(string s)
        {
            int iRet = 1;
            try
            {
                string lin = s.Split('*')[5].ToString();
                string[] sar = lin.Split('-');
                if (sar.Length == 3)
                {
                    lin = sar[2];
                    iRet = BusinessUtility.GetInt( lin.Substring(lin.Length - 3, 3)); 
                }
            }
            catch
            { }
            return iRet;
        }


        public static string RemoveTrailingZero(string s)
        {
            string sRet = string.Empty;
            try
            {
                sRet = s.TrimStart('0');
            }
            catch { }
            return sRet;
        }



        public static List<string> GetChemistryContent(string[] olist)
        {
            List<string> oChemistryElement = new List<string>();
            bool bChemistryFound = false;
            bool bAdded = false;
            oChemistryElement.Add("CID**68~");
            foreach (string s in olist)
            {
                bChemistryFound = s.ToString().Contains("MEA*CH*");
                if (bChemistryFound)
                {
                    oChemistryElement.Add(s.ToString());
                    bAdded = true;
                }
                else
                {
                    if (bAdded)
                        break;
                }
            }
            return oChemistryElement;
        }

        public static List<string> GetTestContent(string[] olist)
        {
            List<string> oTestElement = new List<string>();
            bool bTestyFound = false;
            bool bAdded = false;
            oTestElement.Add("CID**71~");
            oTestElement.Add("PSD*02*****01*11~");
            foreach (string s in olist)
            {
                bTestyFound = s.ToString().Contains("MEA*TR*");
                if (bTestyFound)
                {
                    if (s.ToString().Contains("*YB*"))
                    {
                        oTestElement.Add("TMD*32*ST*093***YIELD STRENGTH~");
                    }
                    else if (s.ToString().Contains("*RK*"))
                    {
                        oTestElement.Add("TMD*32*ST*177***HARDNESS~");
                    }
                    else if (s.ToString().Contains("*TF*"))
                    {
                        oTestElement.Add("TMD*32*ST*090***TENSILE~");
                    }
                    else if (s.ToString().Contains("*EA*"))
                    {
                        oTestElement.Add("TMD*32*ST*094***ELONGATION~");
                    }
                    oTestElement.Add(s.ToString());
                    bAdded = true;
                }
                else
                {
                    if (bAdded)
                        break;
                }
            }
            return oTestElement;
        }

        public static string GetElementLine(string sElementToFind, string[] sArray)
        {
           string sReturn = string.Empty;
           bool bElementFound = false;
            foreach (string s in sArray)
            {
                bElementFound = s.ToString().Contains(sElementToFind);
                if (bElementFound)
                {
                    sReturn = s.ToString();
                }
                
            }
            return sReturn;
        }


       public static void AppendLineSeperator(ref string[] sArr, string sSeperator)
        {
            try
            {
                string[] sLine = new string[sArr.Length]; ;
                ArrayList oArrayList = new ArrayList();
                for (int i = 0; i < sArr.Length; i++)
                {
                    oArrayList.Add(sArr[i] + sSeperator);
                }

                sArr = (string[])oArrayList.ToArray(typeof(string));
            }
            catch(Exception ex)
            {
                ErrorLog.createLog(ex.ToString());
            }
        }



        public static string GetElementFromDatatable(DataTable dtSource, string sColumnToFetch, string sCondition)
        {
            string sRet = string.Empty;
            try
            {
                DataRow[] result = dtSource.Select(sCondition);
                if (result.Length > 0)
                {
                    sRet = BusinessUtility.GetString(result[0][sColumnToFetch]);
                }
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLog(ex);
            }
            return sRet;
        }



        public static string GetElementValueBasedOnElementConfig(string sElementToFind, List<string> objSource)
        {
            string sReturn = string.Empty;
            try
            {
                List<string> lstSource = new List<string>();
                string sValueCombination = Utility.GetElementFromDatatable(elementconfig.ElementDetails, "ValueCombination", "Key='" + sElementToFind + "'  ");
                string[] sValueCombinationArray = sValueCombination.Split(';');
                foreach (string s in sValueCombinationArray)
                {

                    if (string.IsNullOrEmpty(sReturn) && !string.IsNullOrEmpty(s))
                    {
                        string[] sarr = s.Split('*');
                        if (sarr.Length > 0)
                        {
                            var element = objSource.FirstOrDefault(stringToCheck => stringToCheck.Contains(sarr[0]));
                            if (element != null)
                            {
                                if (sElementToFind == elementconfig.Elements.PO)
                                {
                                    sReturn = Utility.GetIntFromString(element.Split('*')[BusinessUtility.GetInt(sarr[1])]).ToString();
                                }
                                else if (sElementToFind == elementconfig.Elements.POITEM)
                                {
                                    //sReturn = element.Split('*')[BusinessUtility.GetInt(sarr[1])].ToString();
                                    sReturn = Utility.GetLinItem(element).ToString();
                                    if (sReturn == "0")
                                    {
                                        sReturn = string.Empty;
                                    }
                                }
                                else
                                {
                                    sReturn = element.Split('*')[BusinessUtility.GetInt(sarr[1])].ToString();
                                }
                            }
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLog(ex);
            }
            return sReturn;
        }

        public static string GetFileTypeFromXml(string sXml)
        {
            string sType = string.Empty;
            try
            {
                MyXmlReader omyXmlReader = new MyXmlReader(sXml);
                XmlNodeList m_BodyXmlNodeList = omyXmlReader.GetNodeList("TransSetRoot/FuncGroup/Transaction");
                sType = omyXmlReader.GetEdiType(m_BodyXmlNodeList);
            }
            catch(Exception ex)
            {
                ErrorLog.CreateLog(ex);
            }
            return sType;
        }

        public static string GetDUNSNumberFromXml(string sXml)
        {
            string sType = string.Empty;
            try
            {
                MyXmlReader m_fullXml = new MyXmlReader(sXml);
                XmlNodeList m_HeaderXmlNodeList = m_fullXml.GetNodeList("TransSetRoot");
                // Get duns
                sType = m_fullXml.GetDunsNO();
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLog(ex);
            }
            return sType;
        }

        public static void GetPoDetails(int iPoNumber, ref Details oDetails)
        {
            tctipd otctipd = null;
            if (BusinessUtility.GetInt(oDetails.PrntItm.Length) > 0)
            {
                otctipd = new tctipd(iPoNumber, BusinessUtility.GetInt(oDetails.PrntItm));
            }
            else
            {
                otctipd = new tctipd(iPoNumber, 0);
            }
            

            if (otctipd != null)
            {
                oDetails.Form = otctipd.Form;
                oDetails.Grade = otctipd.Grade;
                oDetails.Size = otctipd.Size;
                oDetails.Finish = otctipd.Finish;
                oDetails.GaSize = otctipd.GaSize;
                oDetails.GaType = otctipd.GaType;
                oDetails.DimDsgn = otctipd.DimDsgn;
                oDetails.ExtendedFinish = otctipd.ExtendedFinish;
                #region commented code - Passing item length intead of coil length   2020/03/30  Sumit
                //if (!string.IsNullOrEmpty(otctipd.CoilLength))
                //{
                //    oDetails.Length = otctipd.CoilLength;
                //}
                //else
                //{
                //    oDetails.Length = "0.0";
                //}
                if (!string.IsNullOrEmpty(otctipd.Length))
                    oDetails.Length = otctipd.Length;
                else
                    oDetails.Length = "0";
                #endregion commented code - Passing item length intead of coil length   2020/03/30  Sumit

                oDetails.IDa = otctipd.IDa;
                oDetails.ODa = otctipd.ODa;

                oDetails.InvtQlty = otctipd.InvtQlty;
                if (!string.IsNullOrEmpty(otctipd.Width))
                {
                    oDetails.Width = otctipd.Width;
                }
                else
                {
                    oDetails.Width = "0";
                }

            }
        }

        public static int GetRandomNo()
        {
            Random rnd = new Random();
           // int month = rnd.Next(1, 13); // creates a number between 1 and 12
            int dice = rnd.Next(1, 9);   // creates a number between 1 and 6
            //int card = rnd.Next(52);
            return dice;
        }

        public static string GetLocation(string sLocation)
        {
            //NYS  007485
            string sRet = string.Empty;
            try
            {
                sRet = sLocation.Replace("NYS", string.Empty);
                sRet = sRet.Replace(" ", string.Empty);
                sRet = sRet.TrimStart(new Char[] { '0' });
            }
            catch { }
            return sRet;
        }


        public static string GetGUID()
        {
            string sReturn = string.Empty;
            try
            {
                Guid obj = Guid.NewGuid();
                sReturn = obj.ToString();
            }
            catch(Exception ex)
            {
                ErrorLog.CreateLog(ex);
            }
            return sReturn;
        }


    }
}
