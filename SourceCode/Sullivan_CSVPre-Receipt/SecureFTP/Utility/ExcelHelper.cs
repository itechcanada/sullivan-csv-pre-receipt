﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.OleDb;
using System.Data;
using iTECH.Library.Utilities;
using System.Data.Odbc;

using System.Text;
using System.Xml;

//using Microsoft.Office.Core;
using Excel = Microsoft.Office.Interop.Excel;
using System.Reflection;
using System.IO;

/// <summary>
/// Summary description for ExcelHelper
/// </summary>
/// 
namespace SecureFTP
{
    public class ExcelHelper
    {
        public ExcelHelper()
        {
            //
            // TODO: Add constructor logic here
            //
        }


        private static readonly ExcelHelper _instance = new ExcelHelper();

        public static ExcelHelper Instance
        {
            get { return _instance; }
        }

        public static DataTable exceldata(string filePath)
        {
            DataTable dtexcel = new DataTable();
            bool hasHeaders = false;
            string HDR = hasHeaders ? "Yes" : "No";
            string strConn;
            if (filePath.Substring(filePath.LastIndexOf('.')).ToLower() == ".xlsx")
                strConn = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + filePath + ";Extended Properties=\"Excel 12.0;HDR=" + HDR + ";IMEX=0\"";
            else
                strConn = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + filePath + ";Extended Properties=\"Excel 8.0;HDR=" + HDR + ";IMEX=0\"";
            OleDbConnection conn = new OleDbConnection(strConn);
            conn.Open();
            DataTable schemaTable = conn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, new object[] { null, null, null, "TABLE" });
            //Looping Total Sheet of Xl File
            /*foreach (DataRow schemaRow in schemaTable.Rows)
            {
            }*/
            //Looping a first Sheet of Xl File
            DataRow schemaRow = schemaTable.Rows[0];
            string sheet = schemaRow["TABLE_NAME"].ToString();
            if (!sheet.EndsWith("_"))
            {
                //string sheet3 = "InventoryLoad";
                string query = "SELECT  * FROM [" + sheet + "]";
                OleDbDataAdapter daexcel = new OleDbDataAdapter(query, conn);
                //dtexcel.Locale = CultureInfo.CurrentCulture;
                daexcel.Fill(dtexcel);
            }

            conn.Close();
            return dtexcel;

        }

        public static DataSet exceldataToDataset(string filePath)
        {
            DataSet DS = new DataSet();
            DataTable dtexcel = new DataTable();
            bool hasHeaders = false;
            string HDR = hasHeaders ? "Yes" : "No";
            string strConn;
            if (filePath.Substring(filePath.LastIndexOf('.')).ToLower() == ".xlsx")
                strConn = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + filePath + ";Extended Properties=\"Excel 12.0;HDR=" + HDR + ";IMEX=0\"";
            else
                strConn = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + filePath + ";Extended Properties=\"Excel 8.0;HDR=" + HDR + ";IMEX=0\"";
            OleDbConnection conn = new OleDbConnection(strConn);
            conn.Open();
            DataTable schemaTable = conn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, new object[] { null, null, null, "TABLE" });
            //Looping Total Sheet of Xl File
            /*foreach (DataRow schemaRow in schemaTable.Rows)
            {
            }*/
            //Looping a first Sheet of Xl File
            foreach (DataRow schemaRow in schemaTable.Rows)
            {
                string sheet = schemaRow["TABLE_NAME"].ToString();
                if (!sheet.EndsWith("_"))
                {
                    dtexcel = new DataTable(sheet);
                    //string sheet3 = "InventoryLoad";
                    string query = "SELECT  * FROM [" + sheet + "]";
                    OleDbDataAdapter daexcel = new OleDbDataAdapter(query, conn);
                    //dtexcel.Locale = CultureInfo.CurrentCulture;
                    daexcel.Fill(dtexcel);
                    if (dtexcel.Rows.Count > 0)
                    {
                        DS.Tables.Add(dtexcel);
                    }
                }
            }
            conn.Close();
            return DS; ;

        }

        


        public void ExportToExcel(System.Data.DataTable Tbl, string ExcelFilePath)
        {
            try
            {
                DataSet ds = new DataSet();
                if (ds.Tables.Count > 0)
                {
                    foreach (DataTable dt in ds.Tables)
                    {
                        ds.Tables.Remove(dt);
                    }
                }
                ds.Tables.Add(Tbl.Copy());
                DataSetsToExcel(ds, ExcelFilePath);

                return;
                //Excel.Application excelApplication = new Excel.Application();
                ////excelApplication.Visible = true;
                ////dynamic excelWorkBook = excelApplication.Workbooks.Add();
                //Object Template = Type.Missing;
                //Excel.Workbook excelWorkBook = excelApplication.Workbooks.Add(Template);
                //Excel.Worksheet wkSheetData = (Excel.Worksheet)excelWorkBook.ActiveSheet;

                //// column headings
                //for (int i = 0; i < Tbl.Columns.Count; i++)
                //{
                //    excelApplication.Cells[1, (i + 1)] = Tbl.Columns[i].ColumnName;
                //}

                //// rows
                //for (int i = 0; i < Tbl.Rows.Count; i++)
                //{
                //    // to do: format datetime values before printing
                //    for (int j = 0; j < Tbl.Columns.Count; j++)
                //    {
                //        excelApplication.Cells[(i + 2), (j + 1)] = Tbl.Rows[i][j];
                //    }
                //}

                //// This works.
                //excelWorkBook.SaveAs(ExcelFilePath, Excel.XlFileFormat.xlWorkbookNormal, System.Reflection.Missing.Value, System.Reflection.Missing.Value, false, false, Excel.XlSaveAsAccessMode.xlShared, false, false, System.Reflection.Missing.Value, System.Reflection.Missing.Value, System.Reflection.Missing.Value);
                //excelWorkBook.Close(Missing.Value, Missing.Value, Missing.Value);

            }
            catch (Exception ex)
            {
                throw new Exception("ExportToExcel: \n" + ex.Message);
            }
        }

        public static void DataSetsToExcel(DataSet dataSet, string filepath)
        {
            try
            {
                string connString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + filepath + ";Extended Properties=Excel 12.0 Xml;";
                string tablename = "";
                DataTable dt = new DataTable();
                foreach (System.Data.DataTable dataTable in dataSet.Tables)
                {
                    dt = dataTable;
                    tablename = dataTable.TableName;
                    using (OleDbConnection con = new OleDbConnection(connString))
                    {
                        con.Open();
                        StringBuilder strSQL = new StringBuilder();
                        strSQL.Append("CREATE TABLE ").Append("[" + tablename + "]");
                        strSQL.Append("(");
                        for (int i = 0; i < dt.Columns.Count; i++)
                        {
                            strSQL.Append("[" + dt.Columns[i].ColumnName + "] text,");
                        }
                        strSQL = strSQL.Remove(strSQL.Length - 1, 1);
                        strSQL.Append(")");

                        OleDbCommand cmd = new OleDbCommand(strSQL.ToString(), con);
                        cmd.ExecuteNonQuery();

                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            //strSQL.Clear();
                            strSQL = new StringBuilder();
                            StringBuilder strfield = new StringBuilder();
                            StringBuilder strvalue = new StringBuilder();
                            for (int j = 0; j < dt.Columns.Count; j++)
                            {
                                strfield.Append("[" + dt.Columns[j].ColumnName + "]");
                                strvalue.Append("'" + dt.Rows[i][j].ToString().Replace("'", "''") + "'");
                                if (j != dt.Columns.Count - 1)
                                {
                                    strfield.Append(",");
                                    strvalue.Append(",");
                                }
                                else
                                {
                                }
                            }
                            if (strvalue.ToString().Contains("<br/>"))
                            {
                                strvalue = strvalue.Replace("<br/>", Environment.NewLine);
                            }
                            cmd.CommandText = strSQL.Append(" insert into [" + tablename + "]( ")
                                .Append(strfield.ToString())
                                .Append(") values (").Append(strvalue).Append(")").ToString();
                            cmd.ExecuteNonQuery();
                        }
                        con.Close();
                    }
                }
            }
            catch (Exception ex)
            {

            }
        }

        public static void ConvertCSVToXLSX(string csvFilePath, string fileName)
        {
            try
            {
                DataTable dtexcel = new DataTable();
                string connectionString = "Driver={Microsoft Text Driver (*.txt; *.csv)};Dbq=" + csvFilePath + ";";
                OdbcConnection conn = new OdbcConnection(connectionString);
                conn.Open();
                string query = "SELECT  * FROM [" + fileName + "]";
                OdbcDataAdapter daexcel = new OdbcDataAdapter(query, conn);
                daexcel.Fill(dtexcel);
                string sFileNme = DateTime.Now.ToString("yyyymmddhhmmss");
                // ExcelHelper.Instance.ExportToExcel(dtexcel, HttpContext.Current.Request.PhysicalApplicationPath + "Upload\\CSVFiles\\Customers" + sFileNme + ".xlsx");
                // HttpContext.Current.Response.Redirect("~/Upload/CSVFiles/Customers" + sFileNme + ".xlsx");
            }
            catch
            {
            }
        }

        public static DataTable GetDataFromCSV(string path)
        {
            DataTable oDataTable = new DataTable();
            try
            {
                StreamReader oStreamReader = new StreamReader(path);
                int RowCount = 0;
                string[] ColumnNames = null;
                string[] oStreamDataValues = null;
                int TotalCoulmn = 0;
                //using while loop read the stream data till end
                while (!oStreamReader.EndOfStream)
                {
                    String oStreamRowData = oStreamReader.ReadLine().Trim();
                    if (oStreamRowData.Length > 0)
                    {
                        oStreamDataValues = oStreamRowData.Split(',');
                        //Bcoz the first row contains column names, we will poluate 
                        //the column name by
                        //reading the first row and RowCount-0 will be true only once
                        if (RowCount == 0)
                        {
                            RowCount = 1;
                            ColumnNames = oStreamRowData.Split(',');
                            oDataTable = new DataTable();
                            TotalCoulmn = ColumnNames.Length;
                            //using foreach looping through all the column names

                            int i = 0;
                            foreach (string csvcolumn in ColumnNames)
                            {
                                //DataColumn oDataColumn = new DataColumn(csvcolumn.ToUpper(), typeof(string));
                                DataColumn oDataColumn = new DataColumn("Col" + i, typeof(string));
                                i += 1;
                                //setting the default value of empty.string to newly created column
                                oDataColumn.DefaultValue = string.Empty;

                                //adding the newly created column to the table
                                oDataTable.Columns.Add(oDataColumn);
                            }

                            //creates a new DataRow with the same schema as of the oDataTable            
                            DataRow oDataRow = oDataTable.NewRow();

                            //using foreach looping through all the column names
                            int columnlength = ColumnNames.Length;
                            //TotalCoulmn
                            for (i = 0; i < ColumnNames.Length; i++)
                            {
                                //oDataRow[ColumnNames[i]] = oStreamDataValues[i] == null ? string.Empty : oStreamDataValues[i].ToString();
                                string sCo = "Col" + i;
                                if (i >= oStreamDataValues.Length)
                                {
                                    oDataRow[sCo] = "";
                                }
                                else
                                {
                                    oDataRow[sCo] = oStreamDataValues[i] == null ? string.Empty : oStreamDataValues[i].ToString();
                                }
                            }
                            //adding the newly created row with data to the oDataTable       
                            oDataTable.Rows.Add(oDataRow);
                        }
                        else
                        {
                            //creates a new DataRow with the same schema as of the oDataTable            
                            DataRow oDataRow = oDataTable.NewRow();

                            //using foreach looping through all the column names
                            int columnlength = ColumnNames.Length;
                            //TotalCoulmn
                            for (int i = 0; i < ColumnNames.Length; i++)
                            {
                                //oDataRow[ColumnNames[i]] = oStreamDataValues[i] == null ? string.Empty : oStreamDataValues[i].ToString();
                                string sCo = "Col" + i;
                                if (i >= oStreamDataValues.Length)
                                {
                                    oDataRow[sCo] = "";
                                }
                                else
                                {
                                    oDataRow[sCo] = oStreamDataValues[i] == null ? string.Empty : oStreamDataValues[i].ToString();
                                }
                            }
                            //adding the newly created row with data to the oDataTable       
                            oDataTable.Rows.Add(oDataRow);
                        }
                    }
                }
                //close the oStreamReader object
                oStreamReader.Close();
                //release all the resources used by the oStreamReader object
                oStreamReader.Dispose();

                //Looping through all the rows in the Datatable
                //foreach (DataRow oDataRow in oDataTable.Rows)
                //{

                //    string RowValues = string.Empty;

                //    //Looping through all the columns in a row

                //    foreach (string csvcolumn in ColumnNames)
                //    {
                //        //concatenating the values for display purpose
                //        RowValues += csvcolumn + "=" + oDataRow[csvcolumn].ToString() + ";  ";
                //    }
                //    //Displaying the result on the console window
                //    Console.WriteLine(RowValues);
                //}

            }
            catch (System.Exception)
            {
                throw;
            }

            return oDataTable;
        }


        public static DataTable GetFiledataForCashReceipt(string path, char chSpliter)
        {
            DataTable oDataTable = new DataTable();
            try
            {
                StreamReader oStreamReader = new StreamReader(path);
                int RowCount = 0;
                string[] ColumnNames = null;
                string[] oStreamDataValues = null;
                int TotalCoulmn = 0;
                //using while loop read the stream data till end
                while (!oStreamReader.EndOfStream)
                {
                    String oStreamRowData = oStreamReader.ReadLine().Trim();
                    if (oStreamRowData.Length > 0)
                    {
                        oStreamDataValues = oStreamRowData.Split(chSpliter);
                        //Bcoz the first row contains column names, we will poluate 
                        //the column name by
                        //reading the first row and RowCount-0 will be true only once
                        if (RowCount == 0)
                        {
                            RowCount = 1;
                            ColumnNames = oStreamRowData.Split(chSpliter);
                            oDataTable = new DataTable();
                            TotalCoulmn = ColumnNames.Length;
                            //using foreach looping through all the column names

                            int i = 0;
                            foreach (string csvcolumn in ColumnNames)
                            {
                                //DataColumn oDataColumn = new DataColumn(csvcolumn.ToUpper(), typeof(string));
                                DataColumn oDataColumn = new DataColumn("Col" + i, typeof(string));
                                i += 1;
                                //setting the default value of empty.string to newly created column
                                oDataColumn.DefaultValue = string.Empty;

                                //adding the newly created column to the table
                                oDataTable.Columns.Add(oDataColumn);
                            }
                        }
                        else
                        {
                            //creates a new DataRow with the same schema as of the oDataTable            
                            DataRow oDataRow = oDataTable.NewRow();

                            //using foreach looping through all the column names
                            int columnlength = ColumnNames.Length;
                            //TotalCoulmn
                            for (int i = 0; i < ColumnNames.Length; i++)
                            {
                                //oDataRow[ColumnNames[i]] = oStreamDataValues[i] == null ? string.Empty : oStreamDataValues[i].ToString();
                                string sCo = "Col" + i;
                                if (i >= oStreamDataValues.Length)
                                {
                                    oDataRow[sCo] = "";
                                }
                                else
                                {
                                    oDataRow[sCo] = oStreamDataValues[i] == null ? string.Empty : oStreamDataValues[i].ToString();
                                }
                            }
                            //adding the newly created row with data to the oDataTable       
                            oDataTable.Rows.Add(oDataRow);
                        }
                    }
                }
                //close the oStreamReader object
                oStreamReader.Close();
                //release all the resources used by the oStreamReader object
                oStreamReader.Dispose();

                //Looping through all the rows in the Datatable
                //foreach (DataRow oDataRow in oDataTable.Rows)
                //{

                //    string RowValues = string.Empty;

                //    //Looping through all the columns in a row

                //    foreach (string csvcolumn in ColumnNames)
                //    {
                //        //concatenating the values for display purpose
                //        RowValues += csvcolumn + "=" + oDataRow[csvcolumn].ToString() + ";  ";
                //    }
                //    //Displaying the result on the console window
                //    Console.WriteLine(RowValues);
                //}

            }
            catch (System.Exception)
            {

                throw;
            }

            return oDataTable;
        }


    }
}