﻿using iTECH.Library.Utilities;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;

namespace SecureFTP
{
    class EdiXmlParser
    {
        public static string m_XmlData = string.Empty;
        MyXmlReader m_fullXml = null;
        public static XmlNodeList m_HeaderXmlNodeList = null;
        public static XmlNodeList m_BodyXmlNodeList = null;
        MyXmlReader omyXmlReader = null;
        Files oFiles = null;
        int m_iFileId = 0;
        int m_iHeaderId = 0;
        int m_iDetailId = 0;
        int m_iDtailItem = 0;
        static string sStratixConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ToString();
        static string sMysqlConnectionString = ConfigurationManager.ConnectionStrings["NewConnectionString"].ToString();

        static string m_sCompanyId = ConfigurationManager.AppSettings["CompanyId"];
        static string m_sIntchgPfx = ConfigurationManager.AppSettings["IntchgPfx"];
        static string m_sCusVenId = ConfigurationManager.AppSettings["CusVenId"];
        static string m_sRcptType = ConfigurationManager.AppSettings["RcptType"];

        string ProcessingBranch = ConfigurationManager.AppSettings["ProcessingBranch"];

        string m_sShipFrom = string.Empty;
        int m_iWarehouse = 0;
        int m_iFileReceivedId = 0;
        Configuration oConfiguration = null;
        string m_sSingleFilePath = string.Empty;
        public EdiXmlParser(string sXmlPath, int iFileReceivedId)
        {
            //oConfiguration = oConfig;
            m_sSingleFilePath = sXmlPath;
            m_iFileReceivedId = iFileReceivedId;
            oFiles = new Files(m_iFileId);
            try
            {
                m_XmlData = File.ReadAllText(sXmlPath);
                m_XmlData = m_XmlData.Replace("o;?", string.Empty);
                ParseData();
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLog(ex);
            }
        }



        public void ParseData()
        {
            try

            {
                if (!string.IsNullOrEmpty(m_XmlData))
                {
                    m_fullXml = new MyXmlReader(m_XmlData);
                    m_HeaderXmlNodeList = m_fullXml.GetNodeList("TransSetRoot");
                    // Get duns
                    string sDuns = m_fullXml.GetDunsNO();
                    m_BodyXmlNodeList = m_fullXml.GetNodeList("TransSetRoot/FuncGroup/Transaction");
                    string sxml = m_BodyXmlNodeList.ToString();
                    sxml = sxml.Replace("ch856:", string.Empty);
                    sxml = sxml.Replace("ch863:", string.Empty);
                    // Get edi type
                    string sType = m_fullXml.GetEdiType(m_BodyXmlNodeList);
                    XmlNode node8631;
                    string s863List = string.Empty;
                    string s856List = string.Empty;
                    //foreach (XmlNode node863 in m_BodyXmlNodeList)
                    //{
                    //    if (sType == Configuration.EdiFileType.EDI863)
                    //    {
                    //        node8631 = node863;
                    //        var xDoc = XDocument.Parse(node8631.InnerXml);
                    //        var QdsChildList = xDoc.Descendants("Lines");
                    //        if (QdsChildList != null)
                    //        {
                    //            foreach (XElement element in QdsChildList)
                    //            {
                    //                s863List = element.ToString();
                    //                break;
                    //            }
                    //        }
                    //    }
                    //    else if (sType == Configuration.EdiFileType.EDI856)
                    //    {
                    //        node8631 = node863;
                    //        var xDoc = XDocument.Parse(node8631.InnerXml);
                    //        var QdsChildList = xDoc.Descendants("Orders");
                    //        if (QdsChildList != null)
                    //        {
                    //            foreach (XElement element in QdsChildList)
                    //            {
                    //                s856List = element.ToString();
                    //                break;
                    //            }
                    //        }
                    //    }
                    //}


                    FileInfo o = new FileInfo(m_sSingleFilePath);
                    FTPTraking ftpObj = new FTPTraking();
                    Files oFiles = new Files();
                    #region to check branch PO should process or not    2020/04/03  Sumit
                    if (!IsProcessBranch(Convert.ToInt16(sType), m_BodyXmlNodeList))
                    {
                        ErrorLog.createLog("Branch is not configured. Skipping File : " + o.Name.ToString());
                        if (m_iFileReceivedId > 0)
                        {
                            FileReceived ofileReceived = new FileReceived();
                            ofileReceived.UpdateProcessedReceiveFile(m_iFileReceivedId);
                        }
                        return;
                    }
                    #endregion to check branch PO should process or not    2020/04/03  Sumit

                    if (o.Extension.ToLower().Contains(".xml"))
                    {
                        // xml 
                        FileReceived oFileReceived = new FileReceived(m_iFileReceivedId);
                        string sFileLine = System.IO.File.ReadAllText(m_sSingleFilePath);
                        sFileLine = sFileLine.Replace("o;?", string.Empty);
                        //string EdiFile = Configuration.EdiFileType.EDI856863;
                        string EdiFile = sType;
                        //string sDunsno = Utility.GetDUNSNumberFromXml(sFileLine);
                        string sDunsno = sDuns;
                        oConfiguration = new Configuration(sDunsno, BusinessUtility.GetInt(EdiFile));
                        m_sCusVenId = BusinessUtility.GetString(oConfiguration.VendorId);
                        ftpObj.ftpFileName = BusinessUtility.GetString(o.Name);
                        ftpObj.ftpFilePath = BusinessUtility.GetString(m_sSingleFilePath);

                        oFiles.VendorId = m_sCusVenId;
                        //oFiles.FileText = sFileLine;
                        oFiles.FileType = BusinessUtility.GetInt(EdiFile);
                        oFiles.VendorName = oConfiguration.VendorName;
                        oFiles.CustomerName = oConfiguration.CustomerName;
                        oFiles.Mill = oConfiguration.MillId;
                        oFiles.VendorDunsNo = oConfiguration.VendorDunsNo;
                        oFiles.FileReceivedId = m_iFileReceivedId;
                        oFiles.UniqueFileName = oFileReceived.UniqueFileName;

                        string sCertFile = ftpObj.ftpFileName.Replace("ASN", string.Empty);
                        sCertFile = sCertFile.Replace("_", string.Empty);
                        sCertFile = sCertFile.Replace(".xml", string.Empty);
                        sCertFile = sCertFile.Replace(".XML", string.Empty);
                        sCertFile = sCertFile + ".pdf";

                        oFiles.CertFileToUpload = sCertFile;
                    }

                    if (oConfiguration.Activate856 && sType == Configuration.EdiFileType.EDI856)
                    {
                        oFiles.FileType = BusinessUtility.GetInt(Configuration.EdiFileType.EDI856);
                        m_iFileId = oFiles.insertFileStatus(ftpObj);

                        Globalcl.SetFileId(m_iFileId);
                        Edi856XmlParser oEdi856XmlParser = new Edi856XmlParser(m_HeaderXmlNodeList, m_BodyXmlNodeList, m_iFileId, s856List);
                    }
                    else if (oConfiguration.Activate863 && sType == Configuration.EdiFileType.EDI863)
                    {
                        oFiles.FileType = BusinessUtility.GetInt(Configuration.EdiFileType.EDI863);
                        m_iFileId = oFiles.insertFileStatus(ftpObj);
                        Globalcl.SetFileId(m_iFileId);
                        Edi863XmlParser oEdi863XmlParser = new Edi863XmlParser(m_HeaderXmlNodeList, m_BodyXmlNodeList, m_iFileId, s863List);
                    }
                    else
                    {
                        ErrorLog.createLog("Either 856 or 863 not active or issue with file parsing");
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLog(ex);
            }
        }

        public bool IsProcessBranch(int fileType, XmlNodeList objxml)
        {
            MyXmlReader objMyXml;
            bool isProcess = false;
            string PONumber = string.Empty;
            string[] branchArr;
            string branchCode = string.Empty;
            try
            {
                if (fileType == 856)
                {
                    foreach (XmlElement onode in objxml)
                    {
                        objMyXml = new MyXmlReader(onode.OuterXml);
                        PONumber = objMyXml.GetNode("//Order//PONumber").InnerText;
                        branchArr = PONumber.Split('-');
                        if (branchArr.Length > 1)
                        {
                            PONumber = branchArr[1].ToString();
                        }
                        if (!string.IsNullOrEmpty(PONumber))
                            break;
                    }
                }
                else if (fileType == 863)
                {
                    foreach (XmlElement onode in objxml)
                    {
                        objMyXml = new MyXmlReader(onode.OuterXml);
                        
                        PONumber = objMyXml.GetNodeAttribute("//Lines//Line", 0, "PONumber");
                        branchArr = PONumber.Split('-');
                        if (branchArr.Length > 1)
                        {
                            PONumber = branchArr[1].ToString();
                        }
                        if (!string.IsNullOrEmpty(PONumber))
                            break;
                    }
                }
                // to check this branch PO should be process or not
                branchCode = tctipd.GetBranchCode_PO(PONumber);
                if (ProcessingBranch.Contains(branchCode))
                {
                    isProcess = true;
                }
            }
            catch (Exception ex)
            {
                ErrorLog.createLog("Error in getting branch for PO Number.");
            }
            return isProcess;
        }
    }
}
