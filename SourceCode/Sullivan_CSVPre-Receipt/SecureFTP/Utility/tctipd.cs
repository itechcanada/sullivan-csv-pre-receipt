﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using iTECH.Library.DataAccess.ODBC;
using iTECH.Library.Utilities;
using System.Data;
using MySql.Data.MySqlClient;
using IBM.Data.Informix;

namespace SecureFTP
{
    class tctipd
    {
        StringBuilder sbSql = null;
        string m_sSql = string.Empty;
        static string sStratixConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ToString();
        static string sMysqlConnectionString = ConfigurationManager.ConnectionStrings["NewConnectionString"].ToString();
        static string m_sCompanyId = ConfigurationManager.AppSettings["CompanyId"];
        private string EdiFile = string.Empty;

        int m_iFileId = 0;
        int m_iPoNo = 0;

        public tctipd()
        { }


        public tctipd(int iPoNo, int iFileId)
        {
            m_iPoNo = iPoNo;
            m_iFileId = iFileId; // here file id is treating as item
            this.GetPoDetails(m_iPoNo, m_iFileId);
        }

        string sForm = string.Empty;
        public string Form
        {
            get { return sForm; }
        }

        string sGrade = string.Empty;
        public string Grade
        {
            get { return sGrade; }
        }

        string sSize = string.Empty;
        public string Size
        {
            get { return sSize; }
        }


        string sFinish = string.Empty;
        public string Finish
        {
            get { return sFinish; }
        }

        string sGaSize = "0.0";
        public string GaSize
        {
            get { return sGaSize; }
        }

        string sGaType = string.Empty;
        public string GaType
        {
            get { return sGaType; }
        }

        string sDimDsgn = string.Empty;
        public string DimDsgn
        {
            get { return sDimDsgn; }
        }

        string sExtendedFinish = string.Empty;
        public string ExtendedFinish
        {
            get { return sExtendedFinish; }
        }

        string sIDa = string.Empty;
        public string IDa
        {
            get { return sIDa; }
        }

        string sODa = string.Empty;
        public string ODa
        {
            get { return sODa; }     
        }

        string sInvtQlty = string.Empty;
        public string InvtQlty
        {
            get { return sInvtQlty; }
        }

        string sCoilLength = string.Empty;
        public string CoilLength
        {
            get { return sCoilLength; }
        }

        string sCoilLengthType = string.Empty;
        public string CoilLengthType
        {
            get { return sCoilLengthType; }
        }

        string sCutNumber = string.Empty;
        public string CutNumber
        {
            get { return sCutNumber; }
        }

        string sLength = "0.0";
        public string Length
        {
            get { return sLength; }
        }

        string sWidth = string.Empty;
        public string Width
        {
            get { return sWidth; }
        }

        string sShipFrom = string.Empty;
        public string ShipFrom
        {
            get { return sShipFrom; }
        }

        string sSdo = string.Empty;
        public string Sdo
        {
            get { return sSdo; }
        }

        string sStdId = string.Empty;
        public string StdId
        {
            get { return sStdId; }
        }

        string sAddnlId = string.Empty;
        public string AddnlId
        {
            get { return sAddnlId; }
        }


        public void GetPoDetails(int iPoNumber, int iItem = 1)
        {
            if (iItem == 0)
            {
                iItem = 1;
            }
            DbHelper oDbHelper = new DbHelper(sStratixConnectionString, true);
            sbSql = new StringBuilder();
            sbSql.Append("select ipd_frm,ipd_grd,ipd_size,ipd_fnsh,ipd_ga_size,ipd_ga_typ,ipd_dim_dsgn, ipd_ef_evar,ipd_idia, ipd_odia, prd_coil_lgth, prd_coil_lgth_typ, prd_cut_no, ipd_lgth, ipd_wdth");
            sbSql.Append(" from  TCTIPD_REC ");
            sbSql.Append(" join potpod_rec on ipd_cmpy_id = pod_cmpy_id and ipd_ref_pfx = pod_po_pfx and ipd_ref_no = pod_po_no and ipd_ref_itm = pod_po_itm ");
            sbSql.Append(" left join intprd_rec on prd_cmpy_id = pod_cmpy_id and prd_itm_ctl_no = pod_itm_ctl_no ");
            sbSql.Append(" where ipd_ref_pfx = 'PO' and ipd_ref_itm =" + iItem + " and ipd_ref_no ='" + iPoNumber + "' and ipd_cmpy_id = '" + m_sCompanyId + "'");
            try
            {
                DataTable oDataTable = new DataTable();
                if (BusinessUtility.GetInt(Utility.GetConfigValue("InformixRead")) == 1)        // use informix read function   2021/03/23  Sumit
                    oDataTable = GetInfomixRecord(sbSql.ToString());
                else
                    oDataTable = oDbHelper.GetDataTable(sbSql.ToString(), CommandType.Text, null);

                ErrorLog.createLog("oDataTable.Rows.Count for form grd size = :" + oDataTable.Rows.Count);
                if (oDataTable.Rows.Count > 0)
                {
                    sForm = BusinessUtility.GetString(oDataTable.Rows[0]["ipd_frm"]);
                    sGrade = BusinessUtility.GetString(oDataTable.Rows[0]["ipd_grd"]);
                    sSize = BusinessUtility.GetString(oDataTable.Rows[0]["ipd_size"]);
                    sFinish = BusinessUtility.GetString(oDataTable.Rows[0]["ipd_fnsh"]);
                    sGaSize = BusinessUtility.GetString(oDataTable.Rows[0]["ipd_ga_size"]);
                    sGaType = BusinessUtility.GetString(oDataTable.Rows[0]["ipd_ga_typ"]);
                    sDimDsgn = BusinessUtility.GetString(oDataTable.Rows[0]["ipd_dim_dsgn"]);
                    sExtendedFinish = BusinessUtility.GetString(oDataTable.Rows[0]["ipd_ef_evar"]);
                    sIDa = BusinessUtility.GetString(oDataTable.Rows[0]["ipd_idia"]); ;
                    sODa = BusinessUtility.GetString(oDataTable.Rows[0]["ipd_odia"]);
                    sCoilLength = BusinessUtility.GetString(oDataTable.Rows[0]["prd_coil_lgth"]);
                    sCoilLengthType = BusinessUtility.GetString(oDataTable.Rows[0]["prd_coil_lgth_typ"]);
                    sCutNumber = BusinessUtility.GetString(oDataTable.Rows[0]["prd_cut_no"]);
                    sLength = BusinessUtility.GetString(oDataTable.Rows[0]["ipd_lgth"]);
                    sWidth = BusinessUtility.GetString(oDataTable.Rows[0]["ipd_wdth"]);
                }

                ErrorLog.createLog("sForm = " + sForm);
                ErrorLog.createLog("sGrade = " + sGrade);
                ErrorLog.createLog("sSize = " + sSize);
                ErrorLog.createLog("sFinish = " + sFinish);

                ErrorLog.createLog(sbSql.ToString());
                //m_sSql = "select poi_invt_qlty from potpoi_rec WHERE poi_cmpy_id = '" + m_sCompanyId + "' and poi_po_pfx = 'PO' and poi_po_itm =1 and poi_po_no = '" + iPoNumber + "'";       //comment code to get value for multiple PO item    2020/03/03  Sumit
                m_sSql = "select poi_invt_qlty from potpoi_rec WHERE poi_cmpy_id = '" + m_sCompanyId + "' and poi_po_pfx = 'PO' and poi_po_itm =" + iItem + " and poi_po_no = '" + iPoNumber + "'";
                ErrorLog.createLog(m_sSql);
                DataTable dtInvTQlty = new DataTable();
                if (BusinessUtility.GetInt(Utility.GetConfigValue("InformixRead")) == 1)        // use informix read function   2021/03/23  Sumit
                    dtInvTQlty = GetInfomixRecord(m_sSql.ToString());
                else
                    dtInvTQlty = oDbHelper.GetDataTable(m_sSql, CommandType.Text, null);

                if (dtInvTQlty.Rows.Count > 0)
                {
                    sInvtQlty = BusinessUtility.GetString(dtInvTQlty.Rows[0]["poi_invt_qlty"]);
                }


                m_sSql = "select cpk_id from pntcpk_rec where cpk_ref_pfx = 'PO' and cpk_ref_no = '" + iPoNumber + "'";
                DataTable dtID = new DataTable();
                ErrorLog.createLog(m_sSql.ToString());
                if (BusinessUtility.GetInt(Utility.GetConfigValue("InformixRead")) == 1)        // use informix read function   2021/03/23  Sumit
                    dtID = GetInfomixRecord(m_sSql.ToString());
                else
                    dtID = oDbHelper.GetDataTable(m_sSql, CommandType.Text, null);

                if (dtID.Rows.Count > 0)
                {
                    sIDa = BusinessUtility.GetString(dtID.Rows[0]["cpk_id"]);
                }

                m_sSql = "select poh_shp_fm from potpoh_rec where poh_po_no = '" + iPoNumber + "'";
                ErrorLog.createLog(m_sSql.ToString());
                DataTable dtShipInfo = new DataTable();
                if (BusinessUtility.GetInt(Utility.GetConfigValue("InformixRead")) == 1)        // use informix read function   2021/03/23  Sumit
                    dtShipInfo = GetInfomixRecord(m_sSql.ToString());
                else
                    dtShipInfo = oDbHelper.GetDataTable(m_sSql, CommandType.Text, null);

                if (dtShipInfo.Rows.Count > 0)
                {
                    sShipFrom = BusinessUtility.GetString(dtShipInfo.Rows[0]["poh_shp_fm"]);
                }


            }
            catch (Exception ex)
            {
                ErrorLog.createLog("GetPoDetails error occured");
                ErrorLog.createLog(ex.ToString());
                Files oFiles = new Files(m_iFileId);
                EdiError.LogError(oFiles.EdiFileType, m_iFileId.ToString(), oFiles.FileName, ex, "");
                ErrorLog.CreateLog(ex);
            }
            finally
            {
                oDbHelper.CloseDatabaseConnection();
            }
        }


        public int CheckMultiplePoItemExists(string sPono)
        {
           int iMultiplePoExists = 0;
           DbHelper oDbHelper = new DbHelper(sStratixConnectionString, true);
            try
            {
                m_sSql = "select * from potpod_rec where pod_po_no = '" + sPono + "' AND pod_cmpy_id = '" + m_sCompanyId + "' and pod_po_pfx = 'PO'";
                ErrorLog.createLog(m_sSql.ToString());
                DataTable oDataTable = new DataTable();
                if (BusinessUtility.GetInt(Utility.GetConfigValue("InformixRead")) == 1)        // use informix read function   2021/03/23  Sumit
                    oDataTable = GetInfomixRecord(m_sSql.ToString());
                else
                    oDataTable = oDbHelper.GetDataTable(m_sSql, CommandType.Text, null);

                iMultiplePoExists = oDataTable.Rows.Count;
                ErrorLog.createLog("  iMultiplePoExists = " + iMultiplePoExists + "  " + m_sSql);
            }
            catch (Exception ex)
            {
                ErrorLog.createLog(ex.ToString());
                Files oFiles = new Files(m_iFileId);
                EdiError.LogError(oFiles.EdiFileType, m_iFileId.ToString(), oFiles.FileName, ex, "");
                ErrorLog.CreateLog(ex);
            }
            finally
            {
                oDbHelper.CloseDatabaseConnection();
            }
           return iMultiplePoExists;
        }


        public string GetCustomerId(string sInvoiceNo)
        {
            string sCustomerId = string.Empty;
            DbHelper oDbHelper = new DbHelper(sStratixConnectionString, true);
            try
            {
                m_sSql = "select ivh_sld_cus_id from ivtivh_rec where ivh_upd_ref ='" + sInvoiceNo + "'";
                ErrorLog.createLog(m_sSql);
                object obj = oDbHelper.GetValue(m_sSql, CommandType.Text);
                sCustomerId = BusinessUtility.GetString(obj);
            }

            catch (Exception ex)
            {
                ErrorLog.createLog(ex.ToString());
                Files oFiles = new Files(m_iFileId);
                EdiError.LogError(oFiles.EdiFileType, m_iFileId.ToString(), oFiles.FileName, ex, "");
                ErrorLog.CreateLog(ex);
            }
            finally
            {
                oDbHelper.CloseDatabaseConnection();
            }
            return sCustomerId;
        }


        public void PopulateProductInfo(int iCusId, string sPartNo, string sRevNo)
        {
            
            string sCustomerId = string.Empty;
            DbHelper oDbHelper = new DbHelper(sStratixConnectionString, true);
            try
            {
                m_sSql = "select ppd_frm, ppd_grd, ppd_size, ppd_fnsh, ppd_ef_evar, ppd_wdth, ppd_dim_dsgn, ppd_idia, ppd_odia, ppd_ga_size from cprclg_rec join cprppd_rec"
                 + " on  clg_cmpy_id = ppd_cmpy_id  and clg_part_sts = ppd_part_sts and clg_part_ctl_no = ppd_part_ctl_no"
                 + " where clg_cus_ven_id=" + iCusId + " and clg_part = '" + sPartNo + "' and clg_part_sts = 'P' and clg_part_revno = '"+sRevNo+"' ";
                ErrorLog.createLog(m_sSql);
                DataTable oDataTable = new DataTable();
                if (BusinessUtility.GetInt(Utility.GetConfigValue("InformixRead")) == 1)        // use informix read function   2021/03/23  Sumit
                    oDataTable = GetInfomixRecord(m_sSql.ToString());
                else
                    oDataTable = oDbHelper.GetDataTable(m_sSql, CommandType.Text, null);

                ErrorLog.createLog("oDataTable count = " + oDataTable.Rows.Count);
                if (oDataTable.Rows.Count > 0)
                {
                    sForm = BusinessUtility.GetString(oDataTable.Rows[0]["ppd_frm"]);
                    sGrade = BusinessUtility.GetString(oDataTable.Rows[0]["ppd_grd"]);
                    sSize = BusinessUtility.GetString(oDataTable.Rows[0]["ppd_size"]);
                    sFinish = BusinessUtility.GetString(oDataTable.Rows[0]["ppd_fnsh"]);
                    sGaSize = BusinessUtility.GetString(oDataTable.Rows[0]["ppd_ga_size"]);
                    sDimDsgn = BusinessUtility.GetString(oDataTable.Rows[0]["ppd_dim_dsgn"]);
                    sExtendedFinish = BusinessUtility.GetString(oDataTable.Rows[0]["ppd_ef_evar"]);
                    sIDa = BusinessUtility.GetString(oDataTable.Rows[0]["ppd_idia"]); ;
                    sODa = BusinessUtility.GetString(oDataTable.Rows[0]["ppd_odia"]);
                    sWidth = BusinessUtility.GetString(oDataTable.Rows[0]["ppd_wdth"]);
                }
            }

            catch (Exception ex)
            {
                ErrorLog.createLog(ex.ToString());
                Files oFiles = new Files(m_iFileId);
                ErrorLog.CreateLog(ex);
                // EdiError.LogError(oFiles.EdiFileType, m_iFileId.ToString(), oFiles.FileName, ex, "");
            }
            finally
            {
                oDbHelper.CloseDatabaseConnection();
            }
        }


        public bool CheckCHQExists(string sCheckNo)
        {
            int iCount = 0;
            bool bReturm = false;
            DbHelper oDbHelper = new DbHelper(sStratixConnectionString, true);
            try
            {
                m_sSql = "  select count(*) as count from artcsh_rec  where csh_cmpy_id = 'USS' AND csh_crcp_pfx = 'CH' AND  csh_ssn_id = '"+sCheckNo+"'";
                ErrorLog.createLog(m_sSql);
                object obj = oDbHelper.GetValue(m_sSql, CommandType.Text);
                iCount = BusinessUtility.GetInt(obj);
                ErrorLog.createLog("iCount = " + iCount);
                bReturm = iCount > 0;
                ErrorLog.createLog("bReturm = " + bReturm);
            }
            catch (Exception ex)
            {
                ErrorLog.createLog(ex.ToString());
                Files oFiles = new Files(m_iFileId);
                EdiError.LogError(oFiles.EdiFileType, m_iFileId.ToString(), oFiles.FileName, ex, "");
                ErrorLog.CreateLog(ex);
            }
            finally
            {
                oDbHelper.CloseDatabaseConnection();
            }
            return bReturm;
        }

        public void PopulateMaterialStanderd(int iPoNo)
        {
            DbHelper oDbHelper = new DbHelper(sStratixConnectionString, true);
            try
            {
                m_sSql = "select mss_sdo, mss_std_id, mss_addnl_id from pntssp_rec  join mcrmss_rec on mss_mss_ctl_no = ssp_mss_ctl_no";
                m_sSql = m_sSql + " where ssp_ref_pfx = 'PO' and ssp_ref_no = "+iPoNo+" and ssp_ref_itm = 1  ";
                ErrorLog.createLog(m_sSql);
                DataTable dtInvTQlty = new DataTable();
                if (BusinessUtility.GetInt(Utility.GetConfigValue("InformixRead")) == 1)        // use informix read function   2021/03/23  Sumit
                    dtInvTQlty = GetInfomixRecord(m_sSql.ToString());
                else
                    dtInvTQlty = oDbHelper.GetDataTable(m_sSql, CommandType.Text, null);

                if (dtInvTQlty.Rows.Count > 0)
                {
                    sSdo = BusinessUtility.GetString(dtInvTQlty.Rows[0]["mss_sdo"]);
                    sStdId = BusinessUtility.GetString(dtInvTQlty.Rows[0]["mss_std_id"]);
                    sAddnlId = BusinessUtility.GetString(dtInvTQlty.Rows[0]["mss_addnl_id"]);
                }
            }
            catch (Exception ex)
            {
                ErrorLog.createLog(ex.ToString());
                Files oFiles = new Files(m_iFileId);
                EdiError.LogError(oFiles.EdiFileType, m_iFileId.ToString(), oFiles.FileName, ex, "");
                ErrorLog.CreateLog(ex);
            }
            finally
            {
                oDbHelper.CloseDatabaseConnection();
            }

        }

        public void PopulateWarehouse(ref Shipment oShipment, string sPoNo, string sCompanyId)
        {
            try
            {
                //m_sSql = "Select first 1 pod_shp_to_whs,pod_frt_ven_id from potpod_rec where pod_cmpy_id =  '" + sCompanyId + "' and pod_po_pfx= 'PO' and pod_po_no = '" + sPoNo + "'";       // 2021/11/30   modify Query to support Postgres
                m_sSql = "Select pod_shp_to_whs,pod_frt_ven_id from potpod_rec where pod_cmpy_id =  '" + sCompanyId + "' and pod_po_pfx= 'PO' and pod_po_no = '" + sPoNo + "' limit 1";
                ErrorLog.createLog(m_sSql);
                DbHelper oDbHelper = new DbHelper(sStratixConnectionString, true);
                DataTable oDataTable = new DataTable();
                if (BusinessUtility.GetInt(Utility.GetConfigValue("InformixRead")) == 1)        // use informix read function   2021/03/23  Sumit
                    oDataTable = GetInfomixRecord(m_sSql.ToString());
                else
                    oDataTable = oDbHelper.GetDataTable(m_sSql, CommandType.Text, null);

                if (oDataTable.Rows.Count > 0)
                {
                    oShipment.RcvWhs = BusinessUtility.GetString(oDataTable.Rows[0][0]);
                    oShipment.FreightVenId = BusinessUtility.GetString(oDataTable.Rows[0][1]);
                    ErrorLog.createLog(oShipment.RcvWhs + " < whs  ,  frtvenid > " + oShipment.FreightVenId);
                }
            }
            catch (Exception ex)
            { ErrorLog.CreateLog(ex); }
        }

        /// <summary>
        /// to get Warehouse and FreightVenID detail of a PO Item    2020/05/04  Sumit
        /// </summary>
        /// <param name="oShipment"></param>
        /// <param name="sPoNo"></param>
        /// <param name="POitem"></param>
        public void PopulateWarehouse(string sPoNo, int POitem, ref string poWhs, ref string freightVenID)
        {
            string m_sCompanyId = ConfigurationManager.AppSettings["CompanyId"];
            try
            {
                m_sSql = "Select first 1 pod_shp_to_whs,pod_frt_ven_id from potpod_rec where pod_cmpy_id =  '" + m_sCompanyId + "' and pod_po_pfx= 'PO' and pod_po_no = '" + sPoNo + "' and pod_po_itm='"+ POitem +"'";
                ErrorLog.createLog(m_sSql);
                DbHelper oDbHelper = new DbHelper(sStratixConnectionString, true);
                DataTable oDataTable = new DataTable();
                if (BusinessUtility.GetInt(Utility.GetConfigValue("InformixRead")) == 1)        // use informix read function   2021/03/23  Sumit
                    oDataTable = GetInfomixRecord(m_sSql.ToString());
                else
                    oDataTable = oDbHelper.GetDataTable(m_sSql, CommandType.Text, null);

                if (oDataTable.Rows.Count > 0)
                {
                    poWhs = BusinessUtility.GetString(oDataTable.Rows[0][0]);
                    freightVenID = BusinessUtility.GetString(oDataTable.Rows[0][1]);
                    ErrorLog.createLog(poWhs + " < whs  ,  frtvenid > " + freightVenID);
                }
            }
            catch (Exception ex)
            { ErrorLog.CreateLog(ex); }
        }

        public static bool VenTagExistsForQds(long lngQdsNo, string sVenTag)
        {
            bool bAvailable = false;
            DbHelper oDbHelper = new DbHelper(sStratixConnectionString, true);
            try
            {
                string sSQL = "select * from mchvti_rec where vti_qds_ctl_no = " + lngQdsNo + " and vti_mill_id = '" + sVenTag + "'";
                ErrorLog.createLog(sSQL);
                DataTable oDataTable = new DataTable();
                if (BusinessUtility.GetInt(Utility.GetConfigValue("InformixRead")) == 1)        // use informix read function   2021/03/23  Sumit
                    oDataTable = GetInfomixRecord(sSQL.ToString());
                else
                    oDataTable = oDbHelper.GetDataTable(sSQL, CommandType.Text, null);

                bAvailable = oDataTable.Rows.Count > 0;
            }
            catch (Exception ex)
            { ErrorLog.CreateLog(ex); }
            finally
            {
                oDbHelper.CloseDatabaseConnection();
            }
            return bAvailable;

        }

        public static void GetPoDetails(int iPoNumber, ref header863 oheader863)
        {
            string m_sSql = string.Empty;
            DbHelper oDbHelper = new DbHelper(sStratixConnectionString, true);
            try
            {
                if (BusinessUtility.GetInt(oheader863.OrigPoItem) > 0)
                {
                    m_sSql = "Select first 1 pod_shp_to_whs,pod_frt_ven_id from potpod_rec where pod_cmpy_id =  '" + m_sCompanyId + "' and pod_po_pfx= 'PO' and pod_po_no = '" + iPoNumber + "' AND pod_po_itm = '" + BusinessUtility.GetInt(oheader863.OrigPoItem) + "'";
                }
                else
                {
                    m_sSql = "Select first 1 pod_shp_to_whs,pod_frt_ven_id from potpod_rec where pod_cmpy_id =  '" + m_sCompanyId + "' and pod_po_pfx= 'PO' and pod_po_no = '" + iPoNumber + "'";
                }
                ErrorLog.createLog("Sql for whs = " + m_sSql);
                DataTable oDataTable = new DataTable();
                if (BusinessUtility.GetInt(Utility.GetConfigValue("InformixRead")) == 1)        // use informix read function   2021/03/23  Sumit
                    oDataTable = GetInfomixRecord(m_sSql.ToString());
                else
                    oDataTable = oDbHelper.GetDataTable(m_sSql, CommandType.Text, null);

                ErrorLog.createLog("whs count = " + oDataTable.Rows.Count);
                if (oDataTable.Rows.Count > 0)
                {
                    ErrorLog.createLog("Warehouse for qds is : " + BusinessUtility.GetString(oDataTable.Rows[0][0]));
                    oheader863.Whs = BusinessUtility.GetString(oDataTable.Rows[0][0]);
                }
            }
            catch (Exception ex)
            {
                ErrorLog.createLog(ex.ToString());
                //EdiError.LogError(BusinessUtility.GetString(m_iFileType), m_iFileId.ToString(), "", ex, "Error in GetPoDetails");
                ErrorLog.CreateLog(ex);
            }
            finally
            {
                oDbHelper.CloseDatabaseConnection();
            }
        }

        public static bool CheckChemistryEmlementExistInStratix(string sChemElement)
        {
            bool bAvailable = false;
            DbHelper oDbHelper = new DbHelper(sStratixConnectionString, true);
            try
            {
                string sSQL = "select * from scrche_rec where che_chmel = '"+ sChemElement +"'";
                ErrorLog.createLog(sSQL.ToString());
                DataTable oDataTable = new DataTable();
                if (BusinessUtility.GetInt(Utility.GetConfigValue("InformixRead")) == 1)        // use informix read function   2021/03/23  Sumit
                    oDataTable = GetInfomixRecord(sSQL.ToString());
                else
                    oDataTable = oDbHelper.GetDataTable(sSQL, CommandType.Text, null);

                bAvailable = oDataTable.Rows.Count > 0;
            }
            catch (Exception ex)
            { ErrorLog.CreateLog(ex); }
            finally
            {
                oDbHelper.CloseDatabaseConnection();
            }
            return bAvailable;

        }

        public static bool CheckTestEmlementExistInStratix(string sTestElem)
        {
            bool bAvailable = false;
            DbHelper oDbHelper = new DbHelper(sStratixConnectionString, true);
            try
            {
                string sSQL = "select * from inrtst_rec where tst_tst_abbr = '" + sTestElem + "' and tst_actv = 1";
                ErrorLog.createLog(sSQL.ToString());
                DataTable oDataTable = new DataTable();
                if (BusinessUtility.GetInt(Utility.GetConfigValue("InformixRead")) == 1)        // use informix read function   2021/03/23  Sumit
                    oDataTable = GetInfomixRecord(sSQL.ToString());
                else
                    oDataTable = oDbHelper.GetDataTable(sSQL, CommandType.Text, null);

                bAvailable = oDataTable.Rows.Count > 0;
            }
            catch (Exception ex)
            { ErrorLog.CreateLog(ex); }
            finally
            {
                oDbHelper.CloseDatabaseConnection();
            }
            return bAvailable;

        }

        public static string GetBranchCode_PO(string sPoNo)
        {
            string branchCode = string.Empty;
            string sSQL = string.Empty;

            DbHelper oDbHelper = new DbHelper(sStratixConnectionString, true);
            try
            {
                DataTable oDataTable = new DataTable();
                sSQL = "select poh_po_brh from potpoh_rec where poh_cmpy_id='" + m_sCompanyId + "' and poh_po_no='" + sPoNo + "'";
                ErrorLog.createLog(sSQL.ToString());

                if (BusinessUtility.GetInt(Utility.GetConfigValue("InformixRead")) == 1)        // use informix read function   2021/03/23  Sumit
                    oDataTable = GetInfomixRecord(sSQL.ToString());
                else
                    oDataTable = oDbHelper.GetDataTable(sSQL.ToString(), CommandType.Text, null);

                if(oDataTable.Rows.Count >0)
                {
                    branchCode = BusinessUtility.GetString(oDataTable.Rows[0]["poh_po_brh"]);
                    
                }
                ErrorLog.createLog("PO Number: " + sPoNo + " Branch: " + branchCode);
            }
            catch (Exception ex)
            {
                ErrorLog.createLog("GetBranchCode_PO error occured");
                ErrorLog.createLog(ex.ToString());
                ErrorLog.CreateLog(ex);
            }
            finally
            {
                oDbHelper.CloseDatabaseConnection();
            }
            return branchCode;
        }

        public static bool IsCertificateAttached_PrdQds(long prdQds)
        {
            string m_sSql = string.Empty;
            bool isAttached = false;
            DbHelper oDbHelper = new DbHelper(sStratixConnectionString, true);
            try
            {
                m_sSql = "select qec_qds_ctl_no,qec_cert_typ, qec_sdo, qec_img_no, qec_ctlg_rmk, qec_ven_cert_ref, qec_extl_sys from mchqec_rec inner join mchqds_rec " +
                " on qds_cmpy_id = qec_cmpy_id and qds_qds_ctl_no = qec_qds_ctl_no and qds_qds_typ = 'P' where qds_qds_ctl_no ='"+ prdQds +"'";

                ErrorLog.createLog("Sql for Certificate Search = " + m_sSql);
                DataTable oDataTable = new DataTable();
                if (BusinessUtility.GetInt(Utility.GetConfigValue("InformixRead")) == 1)        // use informix read function   2021/03/23  Sumit
                    oDataTable = GetInfomixRecord(m_sSql.ToString());
                else
                    oDataTable = oDbHelper.GetDataTable(m_sSql, CommandType.Text, null);

                ErrorLog.createLog("Certificate count = " + oDataTable.Rows.Count);
                if (oDataTable.Rows.Count > 0)
                {
                    if (BusinessUtility.GetLong(oDataTable.Rows[0]["qec_qds_ctl_no"]) == prdQds)
                    {
                        isAttached = true;
                        ErrorLog.createLog("Certificate Attached : True");
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorLog.createLog(ex.ToString());
                ErrorLog.CreateLog(ex);
            }
            finally
            {
                oDbHelper.CloseDatabaseConnection();
            }
            return isAttached;
        }

        public static int GetInvtPcs(string Weight, string Form, string Grade, string Size, string Finish)
        {
            int _totalPcs = 1;
            double _PcsInSqFeet = 0;
            string m_sSql = string.Empty;
            DbHelper oDbHelper = new DbHelper(sStratixConnectionString, true);
            try
            {
                m_sSql = "select prm_e_twf from inrprm_rec where prm_frm='"+ Form + "' and prm_grd = '"+ Grade + "' and prm_size='"+ Size + "' and prm_fnsh='"+ Finish + "'";

                ErrorLog.createLog("Sql for Total Pcs = " + m_sSql);
                DataTable oDataTable = new DataTable();
                if (BusinessUtility.GetInt(Utility.GetConfigValue("InformixRead")) == 1)        // use informix read function   2021/03/23  Sumit
                    oDataTable = GetInfomixRecord(m_sSql.ToString());
                else
                    oDataTable = oDbHelper.GetDataTable(m_sSql, CommandType.Text, null);

                ErrorLog.createLog("Piece Records count = " + oDataTable.Rows.Count);
                if (oDataTable.Rows.Count > 0)
                {
                    _PcsInSqFeet = Convert.ToDouble(Weight) / BusinessUtility.GetDouble(oDataTable.Rows[0]["prm_e_twf"]);
                    _totalPcs = Convert.ToInt16(_PcsInSqFeet / 12);
                }
            }
            catch (Exception ex)
            {
                ErrorLog.createLog(ex.ToString());
                ErrorLog.CreateLog(ex);
            }
            finally
            {
                oDbHelper.CloseDatabaseConnection();
            }

            return _totalPcs;
        }

        public static double GetInvtMsr(string Weight, string Form, string Grade, string Size, string Finish)
        {
            double _totalLength = 1;
            double _PcsInSqFeet = 0;
            string m_sSql = string.Empty;
            DbHelper oDbHelper = new DbHelper(sStratixConnectionString, true);
            try
            {
                m_sSql = "select prm_e_twf from inrprm_rec where prm_frm='" + Form + "' and prm_grd = '" + Grade + "' and prm_size='" + Size + "' and prm_fnsh='" + Finish + "'";

                ErrorLog.createLog("Sql for Invt Msr = " + m_sSql);
                DataTable oDataTable = new DataTable();
                if (BusinessUtility.GetInt(Utility.GetConfigValue("InformixRead")) == 1)        // use informix read function   2021/03/23  Sumit
                    oDataTable = GetInfomixRecord(m_sSql.ToString());
                else
                    oDataTable = oDbHelper.GetDataTable(m_sSql, CommandType.Text, null);

                ErrorLog.createLog("Piece Records count = " + oDataTable.Rows.Count);
                if (oDataTable.Rows.Count > 0)
                {
                    _PcsInSqFeet = Convert.ToDouble(Weight) / BusinessUtility.GetDouble(oDataTable.Rows[0]["prm_e_twf"]);
                    _totalLength = _PcsInSqFeet * 12;
                }
            }
            catch (Exception ex)
            {
                ErrorLog.createLog(ex.ToString());
                ErrorLog.CreateLog(ex);
            }
            finally
            {
                oDbHelper.CloseDatabaseConnection();
            }

            return _totalLength;
        }

        /// <summary>
        /// getting PR error description from stratix database   2021/03/23  Sumit
        /// </summary>
        /// <param name="IntchgNo"></param>
        /// <returns></returns>
        public string GetPRErrorCodeDescription(string IntchgNo)
        {
            DbHelper oDbHelper = new DbHelper(sStratixConnectionString, true);
            string errorDesc = string.Empty;
            try
            {
                m_sSql = "select i00_intchg_no, msg_msg_var from sctmsg_rec join sctslg_rec ON slg_clnt_pid = msg_clnt_pid AND slg_clnt_dtts = msg_clnt_dtts "
                    + " AND slg_clnt_dtms = msg_clnt_dtms AND slg_cmpy_id = msg_cmpy_id JOIN xcti00_rec on i00_ssn_log_ctl_no = slg_ssn_log_ctl_no "
                    + " where i00_intchg_no = '" + IntchgNo + "'";
                ErrorLog.createLog(m_sSql);
                DataTable dtError = new DataTable();
                if (BusinessUtility.GetInt(Utility.GetConfigValue("InformixRead")) == 1)
                    dtError = GetInfomixRecord(m_sSql);
                else
                    dtError = oDbHelper.GetDataTable(m_sSql, CommandType.Text, null);

                if (dtError.Rows.Count > 0)
                {
                    for (int i = 0; i < dtError.Rows.Count; i++)
                    {
                        errorDesc = errorDesc + Convert.ToString(dtError.Rows[i]["msg_msg_var"]) + "\n";
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLog(ex);
            }
            finally
            {
                oDbHelper.CloseDatabaseConnection();
            }
            return errorDesc;
        }

        /// <summary>
        /// Select records through informix library     2021/03/23  Sumit
        /// </summary>
        /// <param name="sqlQuery"></param>
        /// <returns></returns>
        public static DataTable GetInfomixRecord(string sqlQuery)
        {
            string constr = Utility.GetConfigValue("IBMNewConnection").ToString();
            //ErrorLog.createLog("Connection string:" + constr);
            DataTable dt = new DataTable();
            using (IfxConnection conn = new IfxConnection(constr))
            {
                try
                {
                    ErrorLog.createLog("Calling through Informix...");
                    ErrorLog.createLog("constr = " + constr);
                    ErrorLog.createLog("sqlQuery = " + sqlQuery);
                    IfxCommand query = new IfxCommand(sqlQuery);
                    IBM.Data.Informix.IfxDataAdapter adapter = new IfxDataAdapter();
                    query.Connection = conn;
                    query.CommandTimeout = BusinessUtility.GetInt(Utility.GetConfigValue("InformixCommandTimeout"));  //you can define your command timeout time
                    conn.Open();
                    adapter.SelectCommand = new IfxCommand("SET ISOLATION TO DIRTY READ", conn);
                    adapter.SelectCommand.ExecuteNonQuery(); //Tells the program to wait in case of a lock.     
                    adapter.SelectCommand = query;
                    adapter.Fill(dt);
                    conn.Close();
                    adapter.Dispose();
                }

                catch (Exception ex)
                {
                    ErrorLog.createLog("error while processing infomix record");
                    ErrorLog.createLog(ex.Message);
                    //throw;
                }
                finally
                {
                    conn.Close();

                }
            }
            return dt;

        }
    }
}
