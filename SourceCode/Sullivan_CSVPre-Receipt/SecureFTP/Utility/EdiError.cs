﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Text;
using iTECH.Library.Utilities;
using System.Diagnostics;
using System.Reflection;
using System.Net.Mail;
using System.Data.Odbc;

namespace SecureFTP
{
    public static class EdiError
    {
        static string m_sSql = string.Empty;
        static string sMysqlConnectionString = ConfigurationManager.ConnectionStrings["NewConnectionString"].ToString();
        static string sStratixConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ToString();
        private static string EdiFile = string.Empty;
        private static int iSendMail = BusinessUtility.GetInt(ConfigurationManager.AppSettings["SendMail"]);

        private static string Processed_PATH = ConfigurationManager.AppSettings["Processed_PATH"];
        private static string Upload_PATH = ConfigurationManager.AppSettings["Upload_PATH"];
        private static string Unprocessed_PATH = ConfigurationManager.AppSettings["Unprocessed_PATH"];


        public struct ErrorLogType
        {
            public const int ERROR = 1;
            public const int WARNING = 2;
            public const int INFO = 3;
        }


        public static void LogError(string sFileType, string sFileId, string sFileName, Exception oException, string sComment, int iHeader856 = 0, int iDetail856 =0, int iHeader863 = 0, int iDetail863 = 0, int iLogType=0)
        {
            if (BusinessUtility.GetInt(sFileId) <= 0)
            {
                sFileId = Globalcl.FileId.ToString();
            }
            Files oFiles = new Files(BusinessUtility.GetInt(sFileId));
            EdiFile = oFiles.EdiFileType;
            if(!string.IsNullOrEmpty(sFileName))
            {
                sFileName = oFiles.FileName;
            }
            string sSystemName = System.Environment.MachineName;
            StringBuilder sbError = new StringBuilder();

            StackTrace stackTrace = new StackTrace();
            MethodBase methodBase = stackTrace.GetFrame(1).GetMethod();
            string typeName = methodBase.DeclaringType.Name;
            string methodName = methodBase.Name;

            if (typeName.Contains("QdsService") || typeName.Contains("STX856"))
            {
                sbError.AppendLine(Globalcl.StratixGlobalErrorMessage);
            }
            else
            {
                sbError.AppendLine(Globalcl.SystemGlobalErrorMessage);
            }


            sbError.AppendLine("Detail error message..");
            sbError.AppendLine();
            if (!string.IsNullOrEmpty(sSystemName))
            {
                sbError.Append("System Name :");
                sbError.AppendLine(sSystemName);
            }
            sbError.Append("File ID :");
            sbError.AppendLine(sFileId);
            if (!string.IsNullOrEmpty(sFileName))
            {
                sbError.AppendLine("File Name :");
                sbError.Append(sFileName);
            }
            sbError.AppendLine();
            if (oException != null)
            {
                sbError.AppendLine("Stack Trace :");
                sbError.Append(oException.StackTrace);
                sbError.AppendLine();
                sbError.Append(oException.ToString());
                sbError.AppendLine("Source of Error :");
                sbError.Append(oException.Source);
            }

            sbError.Replace("'", string.Empty);

            ErrorLog.createLog(sbError.ToString());

            m_sSql = "INSERT INTO edierror(FileType, FileId, FileName, ErrorDetail, Comment, Header856ID, Detail856ID, Header863ID, Detail863ID, EdiLogType)" +
                                  "VALUES (@FileType, @FileId, @FileName, @ErrorDetail, @Comment, @Header856ID, @Detail856ID, @Header863ID, @Detail863ID, @EdiLogType)";
            iTECH.Library.DataAccess.MySql.DbHelper oDbHelper = new iTECH.Library.DataAccess.MySql.DbHelper(sMysqlConnectionString, true);

            try
            {
                MySqlParameter[] oMySqlParameter = { 
                                                       iTECH.Library.DataAccess.MySql.DbUtility.GetParameter("@FileType",    EdiFile,   iTECH.Library.DataAccess.MySql.MyDbType.Int),
                                                       iTECH.Library.DataAccess.MySql.DbUtility.GetParameter("@FileId",      sFileId,   iTECH.Library.DataAccess.MySql.MyDbType.Int),
                                                       iTECH.Library.DataAccess.MySql.DbUtility.GetParameter("@FileName",    sFileName, iTECH.Library.DataAccess.MySql.MyDbType.String),
                                                       iTECH.Library.DataAccess.MySql.DbUtility.GetParameter("@ErrorDetail", sbError.ToString(), iTECH.Library.DataAccess.MySql.MyDbType.String),
                                                       iTECH.Library.DataAccess.MySql.DbUtility.GetParameter("@Comment",     sComment,  iTECH.Library.DataAccess.MySql.MyDbType.String), 
                                                       iTECH.Library.DataAccess.MySql.DbUtility.GetParameter("@Header856ID", iHeader856, iTECH.Library.DataAccess.MySql.MyDbType.Int),
                                                       iTECH.Library.DataAccess.MySql.DbUtility.GetParameter("@Detail856ID", iDetail856, iTECH.Library.DataAccess.MySql.MyDbType.Int),
                                                       iTECH.Library.DataAccess.MySql.DbUtility.GetParameter("@Header863ID", iHeader863, iTECH.Library.DataAccess.MySql.MyDbType.Int),
                                                       iTECH.Library.DataAccess.MySql.DbUtility.GetParameter("@Detail863ID", iDetail863, iTECH.Library.DataAccess.MySql.MyDbType.Int),
                                                       iTECH.Library.DataAccess.MySql.DbUtility.GetParameter("@EdiLogType", iLogType, iTECH.Library.DataAccess.MySql.MyDbType.Int)
                                                   };
                oDbHelper.ExecuteNonQuery(m_sSql, CommandType.Text, oMySqlParameter);
            }
            catch (Exception ex)
            {
                ErrorLog.createLog("Error in Log Error method of EdiError class : " + ex.ToString());
                ErrorLog.createLog(m_sSql);
            }
            finally
            {
                oDbHelper.CloseDatabaseConnection();
                if (BusinessUtility.GetInt(sFileId) > 0)
                {
                   // MoveFile(oFiles);
                }
                EmailFile(oFiles, sbError.ToString());
            }

        }

        public static void LogError(string sFileType, string sFileId, string sComment, int iHeader856 = 0, int iDetail856 = 0, int iHeader863 = 0, int iDetail863 = 0, int iLogType = 0, bool bSendMail =false)
        {
            Files oFiles = new Files(BusinessUtility.GetInt(sFileId));
            EdiFile = oFiles.EdiFileType;
            string sSystemName = System.Environment.MachineName;
            StringBuilder sbError = new StringBuilder();
            sbError.AppendLine(sComment);
            sbError.AppendLine();
            sbError.AppendLine(oFiles.FileName + " : " + oFiles.EdiFileType);
            sbError.AppendLine();
            if (!string.IsNullOrEmpty(sSystemName))
            {
                sbError.Append("System Name :");
                sbError.AppendLine(sSystemName);
            }
            sbError.Append("File ID :");
            sbError.AppendLine(sFileId);
            
            sbError.AppendLine();
            

            sbError.Replace("'", string.Empty);

            m_sSql = "INSERT INTO edierror(FileType, FileId, ErrorDetail, Comment , Header856ID, Detail856ID, Header863ID, Detail863ID, EdiLogType) " +
                " values (@FileType, @FileId, @ErrorDetail,@Comment, @Header856ID, @Detail856ID, @Header863ID, @Detail863ID, @EdiLogType)";
                
                //('" + EdiFile + "','" + sFileId + "','" + sbError.ToString() + "','" + sComment + "')";
            iTECH.Library.DataAccess.MySql.DbHelper oDbHelper = new iTECH.Library.DataAccess.MySql.DbHelper(sMysqlConnectionString, true);

            try
            {
                MySqlParameter[] oMySqlParameter = {
                                                       iTECH.Library.DataAccess.MySql.DbUtility.GetParameter("@FileType", EdiFile, iTECH.Library.DataAccess.MySql.MyDbType.Int),
                                                       iTECH.Library.DataAccess.MySql.DbUtility.GetParameter("@FileId", sFileId, iTECH.Library.DataAccess.MySql.MyDbType.Int),
                                                       iTECH.Library.DataAccess.MySql.DbUtility.GetParameter("@ErrorDetail", sbError.ToString(), iTECH.Library.DataAccess.MySql.MyDbType.String),
                                                       iTECH.Library.DataAccess.MySql.DbUtility.GetParameter("@Comment", sComment, iTECH.Library.DataAccess.MySql.MyDbType.String),
                                                       iTECH.Library.DataAccess.MySql.DbUtility.GetParameter("@Header856ID", iHeader856, iTECH.Library.DataAccess.MySql.MyDbType.Int),
                                                       iTECH.Library.DataAccess.MySql.DbUtility.GetParameter("@Detail856ID", iDetail856, iTECH.Library.DataAccess.MySql.MyDbType.Int),
                                                       iTECH.Library.DataAccess.MySql.DbUtility.GetParameter("@Header863ID", iHeader863, iTECH.Library.DataAccess.MySql.MyDbType.Int),
                                                       iTECH.Library.DataAccess.MySql.DbUtility.GetParameter("@Detail863ID", iDetail863, iTECH.Library.DataAccess.MySql.MyDbType.Int),
                                                       iTECH.Library.DataAccess.MySql.DbUtility.GetParameter("@EdiLogType", iLogType, iTECH.Library.DataAccess.MySql.MyDbType.Int)
                                                   };
                oDbHelper.ExecuteNonQuery(m_sSql, CommandType.Text, oMySqlParameter);
            }
            catch (Exception ex)
            {
                ErrorLog.createLog("Error in Log Error method of EdiError class : " + ex.ToString());
                ErrorLog.createLog(m_sSql);
            }
            finally
            {
                oDbHelper.CloseDatabaseConnection();
                if (BusinessUtility.GetInt(sFileId) > 0)
                {
                    //MoveFile(oFiles);
                }
                if (bSendMail)
                {
                    EmailFile(oFiles, sbError.ToString());
                }
            }

        }


        public static void MoveFile(Files oFiles)
        {
            try
            {
                System.IO.File.Copy(Processed_PATH + "/" + oFiles.FileName, Unprocessed_PATH + "/" + oFiles.FileName, true);
            }
            catch (Exception ex)
            {
                ErrorLog.createLog("Error in MoveFile method of EdiError class : " + ex.ToString());
                EdiError.LogError(oFiles.FileType.ToString(), Globalcl.FileId.ToString(), "MoveFile error");
            }
        }



        static void EmailFile(Files oFiles, string sMessage)
        {
            string Subject = ConfigurationManager.AppSettings["ErrEmailSub"] +" : " + oFiles.CustomerName + " : " + oFiles.VendorName + " : " + oFiles.FileName + " : " + System.DateTime.Now;
            string EmailFrom = ConfigurationManager.AppSettings["ErrEmailFrom"];
            string EmailTo = ConfigurationManager.AppSettings["ErrEmailTo"];
            string EmailCC = ConfigurationManager.AppSettings["ErrEmailCC"];
            if (!string.IsNullOrEmpty(sMessage))
            {
                MailMessage Mail = new MailMessage(EmailFrom, EmailTo, Subject, sMessage);
                //Mail.IsBodyHtml = true;
                //Mail.CC.Add(EmailCC);
                string serverName = ConfigurationManager.AppSettings["SMTPServer"];
                SmtpClient emailClient = new SmtpClient(serverName);

                emailClient.Host = serverName;
                emailClient.UseDefaultCredentials = false;
                emailClient.Port = Convert.ToInt16(ConfigurationManager.AppSettings["SMTPPort"]);
                emailClient.Credentials = new System.Net.NetworkCredential(ConfigurationManager.AppSettings["SMTPUserEmail"], ConfigurationManager.AppSettings["SMTPPassword"]);
                if (iSendMail > 0)
                {
                    try
                    {
                        emailClient.Send(Mail);
                    }
                    catch (Exception ex)
                    {
                        ErrorLog.createLog("Error in EmailFile method of EdiError class : " + ex.ToString());
                    }
                }
            }
        }



        public static void SendErrorMail(int iFileId, string sSub, string sMessage)
        {

            Files oFiles = new Files(iFileId);
            string Subject = sSub + " : " + oFiles.CustomerName + " : " + oFiles.VendorName + " : " + oFiles.FileName + " : " + System.DateTime.Now;

            string sSystemName = System.Environment.MachineName;
            StringBuilder sbError = new StringBuilder();
            sbError.AppendLine(oFiles.FileName + " : " + oFiles.EdiFileType);
            sbError.AppendLine();
            if (!string.IsNullOrEmpty(sSystemName))
            {
                sbError.Append("System Name :");
                sbError.AppendLine(sSystemName);
                sbError.AppendLine();
            }
            sMessage = sbError.ToString() + sMessage;

            string EmailFrom = ConfigurationManager.AppSettings["ErrEmailFrom"];
            string EmailTo = ConfigurationManager.AppSettings["ErrEmailTo"];
            string EmailCC = ConfigurationManager.AppSettings["ErrEmailCC"];
            MailMessage Mail = new MailMessage(EmailFrom, EmailTo, Subject, sMessage);
            //Mail.IsBodyHtml = true;
            //Mail.CC.Add(EmailCC);
            string serverName = ConfigurationManager.AppSettings["SMTPServer"];
            SmtpClient emailClient = new SmtpClient(serverName);

            emailClient.Host = serverName;
            emailClient.UseDefaultCredentials = false;
            emailClient.Port = Convert.ToInt16(ConfigurationManager.AppSettings["SMTPPort"]);
            emailClient.Credentials = new System.Net.NetworkCredential(ConfigurationManager.AppSettings["SMTPUserEmail"], ConfigurationManager.AppSettings["SMTPPassword"]);
            if (iSendMail > 0)
            {
                try
                {
                    emailClient.Send(Mail);
                }
                catch (Exception ex)
                {
                    ErrorLog.createLog("Error in EmailFile method of EdiError class : " + ex.ToString());
                }
            }
        }







        public static string GetClassAndMethodDetail()
        {
            StackTrace st = new StackTrace();
            StackFrame sf = st.GetFrame(0);

            MethodBase currentMethodName = sf.GetMethod();
            return currentMethodName.ToString();
        }


        public static void ValidateFormGrdSize(string sForm, string sGrd, string sSize, string sComment, string sFileId)
        {
            try
            {
                if (BusinessUtility.GetInt(sFileId) <= 0)
                {
                    sFileId = Globalcl.FileId.ToString();
                }
                if (string.IsNullOrEmpty(sForm) || string.IsNullOrEmpty(sGrd) || string.IsNullOrEmpty(sSize))
                {
                    string sMessage = Utility.GetConfigValue("FORMGRDERRORMESSAGE") + " : " + sComment;
                    SendErrorMail(BusinessUtility.GetInt(sFileId), Utility.GetConfigValue("FORMGRDERRORSUBJECT"), sMessage);
                }
            }
            catch(Exception ex)
            {
                ErrorLog.CreateLog(ex);
            }
        }

        public static void ValidatePO(string spo, string sComment, string sFileId)
        {
            try
            {
                if (BusinessUtility.GetInt(sFileId) <= 0)
                {
                    sFileId = Globalcl.FileId.ToString();
                }
                if (string.IsNullOrEmpty(spo))
                {
                    string sMessage = Utility.GetConfigValue("POERRORMESSAGE") + " : " + sComment;
                    SendErrorMail(BusinessUtility.GetInt(sFileId), Utility.GetConfigValue("POERRORSUBJECT"), sMessage);
                }
            }
            catch(Exception ex)
            {
                ErrorLog.CreateLog(ex);
            }
        }

        public static void ValidateHEAT(string sHEAT, string sComment, string sFileId)
        {
            try
            {
                if (BusinessUtility.GetInt(sFileId) <= 0)
                {
                    sFileId = Globalcl.FileId.ToString();
                }
                if (string.IsNullOrEmpty(sHEAT))
                {
                    string sMessage = Utility.GetConfigValue("HEATERRORMESSAGE") + " : " + sComment;
                    SendErrorMail(BusinessUtility.GetInt(sFileId), Utility.GetConfigValue("HEATERRORSUBJECT"), sMessage);
                }
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLog(ex);
            }
        }

        public static void ValidateWAREHOUSE(string sWareHouse, string sComment, string sFileId)
        {
            try
            {
                if (BusinessUtility.GetInt(sFileId) <= 0)
                {
                    sFileId = Globalcl.FileId.ToString();
                }
                if (string.IsNullOrEmpty(sWareHouse))
                {
                    string sMessage = Utility.GetConfigValue("WAREHOUSEERRORMESSAGE") + " : " + sComment;
                    SendErrorMail(BusinessUtility.GetInt(sFileId), Utility.GetConfigValue("WAREHOUSEERRORSUBJECT"), sMessage);
                }
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLog(ex);
            }
        }

        public static void ValidateOriginZone(string sOriginZone, string sComment, string sFileId)
        {
            try
            {
                if (BusinessUtility.GetInt(sFileId) <= 0)
                {
                    sFileId = Globalcl.FileId.ToString();
                }
                if (string.IsNullOrEmpty(sOriginZone))
                {
                    string sMessage = Utility.GetConfigValue("OriginZoneERRORMESSAGE") + " : " + sComment;
                    SendErrorMail(BusinessUtility.GetInt(sFileId), Utility.GetConfigValue("OriginZoneERRORSUBJECT"), sMessage);
                }
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLog(ex);
            }
        }

        public static DataTable LatestsctmsgError()
        {
            DataTable dtResult = new DataTable();
            OdbcConnection con = new OdbcConnection(sStratixConnectionString);
            con.Open();
            try
            {
                StringBuilder sqlSelect = new StringBuilder();
                sqlSelect.Append(" Select first 5 * from sctmsg_rec  where msg_lgn_id = 'itechedi' and msg_err_msg_typ = 'E' order by msg_clnt_Dtts desc; ");
                OdbcDataAdapter da = new OdbcDataAdapter(sqlSelect.ToString(), con);
                da.Fill(dtResult);
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLog(ex);
                //throw;
            }
            finally
            {
                con.Close();
            }
            return dtResult;
        }

        public static void LogLatestSctmsgError(int iFileId, int iFileType, int iHeaderId)
        {
            DataTable dtPOError = new DataTable();
            try
            {
                dtPOError = LatestsctmsgError();
                for (int iRow = 0; iRow < dtPOError.Rows.Count; iRow++)
                {
                    ErrorLog.createLog("Error Line " + iRow + " " + BusinessUtility.GetString(dtPOError.Rows[iRow][3]) + "\t Error Type: " + BusinessUtility.GetString(dtPOError.Rows[iRow][10]) + "\t Error Msg:" + BusinessUtility.GetString(dtPOError.Rows[iRow][11]));
                    EdiError.LogError(iFileType.ToString(), iFileId.ToString(), "Error Line " + iRow + " " + BusinessUtility.GetString(dtPOError.Rows[iRow][3]) + "\t Error Type: " + BusinessUtility.GetString(dtPOError.Rows[iRow][10]) + "\t Error Msg:" + BusinessUtility.GetString(dtPOError.Rows[iRow][11]), 0, 0, iHeaderId, 0, EdiError.ErrorLogType.ERROR);
                }
            }
            catch (Exception exError)
            {
                ErrorLog.createLog("Error during geting Error data from sctmsg_rec table from database " + exError.Message);
                ErrorLog.CreateLog(exError);
            }   
        }


    }
}
