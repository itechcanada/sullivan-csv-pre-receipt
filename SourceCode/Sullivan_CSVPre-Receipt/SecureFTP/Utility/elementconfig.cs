﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Text;

namespace SecureFTP
{
    public static class elementconfig
    {
        static string m_sSql = string.Empty;
        static string sMysqlConnectionString = ConfigurationManager.ConnectionStrings["NewConnectionString"].ToString();
        public static string DunsNumber { get; private set; }
        public static string FileType { get; private set; }
        public static string Key { get; private set; }
        public static string ValueCombination { get; private set; }
        public static int Active { get; private set; }

        public static DataTable ElementDetails { get; private set; }


        public struct Elements
        {
            public const string PO = "PO";
            public const string POITEM = "POITEM";
            public const string PIECES = "PIECES";

            // Edi 990 element
            public const string CARRIERALPHACODE = "CARRIERALPHACODE";
            public const string CARRIERDATE = "CARRIERDATE";
            public const string CONTROLNO = "CONTROLNO";
            public const string FILETYPE = "FILETYPE";
            public const string REFERENCEIDENTIFICATION = "REFERENCEIDENTIFICATION";
            public const string REFERENCEIDENTIFICATIONQUALIFIER = "REFERENCEIDENTIFICATIONQUALIFIER";
            public const string RESERVATIONACTIONCODE = "RESERVATIONACTIONCODE";
            public const string SHIPMENTIDENTIFICATIONNUMBER = "SHIPMENTIDENTIFICATIONNUMBER";

            //  Edi 214 element
            public const string TransactionSetControlNumber214 = "TransactionSetControlNumber214";
            public const string TransactionSetIdentifierCode214 = "TransactionSetIdentifierCode214";
            public const string CarrierOrderNumber214 = "CarrierOrderNumber214";
         
            public const string Address214 = "Address214";
            public const string City214 = "City214";
            public const string State214 = "State214";
            public const string ZipCode214 = "ZipCode214";
            public const string CountryCode214 = "CountryCode214";

            public const string StopNumber214 = "StopNumber214";
            public const string Date214 = "Date214";
            public const string Time214 = "Time214";
            public const string PurchaseOrderNumber214 = "PurchaseOrderNumber214"; 



        }

        public static void PopulateElementConfig(string sDunsNo, int iFileType)
        {
            FileType = iFileType.ToString();
            DunsNumber = sDunsNo;
            m_sSql = "SELECT * FROM elementconfig WHERE DunsNumber = @DunsNumber AND FileType = @FileType AND Active = 1";

            iTECH.Library.DataAccess.MySql.DbHelper oDbHelper = new iTECH.Library.DataAccess.MySql.DbHelper(sMysqlConnectionString, true);

            try
            {
                MySqlParameter[] oMySqlParameter = { iTECH.Library.DataAccess.MySql.DbUtility.GetParameter("@DunsNumber", sDunsNo, iTECH.Library.DataAccess.MySql.MyDbType.String),
                                                 iTECH.Library.DataAccess.MySql.DbUtility.GetParameter("@FileType", iFileType, iTECH.Library.DataAccess.MySql.MyDbType.Int)
                                               };

                ElementDetails = oDbHelper.GetDataTable(m_sSql, CommandType.Text, oMySqlParameter);
            }
            catch (Exception ex)
            {
                ErrorLog.createLog("Error in PopulateElementConfig : " + ex.ToString());
                ErrorLog.CreateLog(ex);
            }
            finally
            {
                oDbHelper.CloseDatabaseConnection();
            }
        }


        


       
    }
}
