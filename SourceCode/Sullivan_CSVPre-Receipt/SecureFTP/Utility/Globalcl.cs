﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;

namespace SecureFTP
{
    public static class Globalcl
    {
        // public get, and private set for strict access control
        public static int FileId { get; private set; }

        // GlobalInt can be changed only via this method
        public static void SetFileId(int newInt)
        {
            FileId = newInt;
        }

        // public get, and private set for strict access control
        public static string FileType { get; private set; }

        // GlobalInt can be changed only via this method
        public static void SetFileType(string s)
        {
            FileType = s;
        }

        //StratixGlobalErrorMessage
        private static string sStratixGlobalErrorMessage = ConfigurationManager.AppSettings["StratixGlobalErrorMessage"].ToString();
        public static string StratixGlobalErrorMessage { get { return sStratixGlobalErrorMessage; } }

        //SystemGlobalErrorMessage
        private static string sSystemGlobalErrorMessage = ConfigurationManager.AppSettings["SystemGlobalErrorMessage"].ToString();
        public static string SystemGlobalErrorMessage { get { return sSystemGlobalErrorMessage; } }

        //HeatQDS
        private static string sHeatQDS = "H";
        public static string HeatQDS { get { return sHeatQDS; } }

        //ProductQDS
        private static string sProductQDS = "P";
        public static string ProductQDS { get { return sProductQDS; } }

        //ProductQDS
        private static char sElementSeperator = '*';
        public static char ElementSeperator { get { return sElementSeperator; } }

        //StratixGlobalErrorMessage
        private static string sSingleFilePAth = ConfigurationManager.AppSettings["SingleFilePAth"].ToString();
        public static string SingleFilePAth { get { return sSingleFilePAth; } }

    }
}
