﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

    namespace SecureFTP
    {
    class EdiSeperator
    {
       public string[] m_sCompleteFile = null;
       public  List<string> lstComplete863Content = new List<string>();



        public void ProcessFile(string[] sArr)
        {
            try
            {
                TransactionSeperator oTransactionSeperator = new SecureFTP.TransactionSeperator();
                oTransactionSeperator.GetStSeSegment(sArr);

                for (int i = 0; i < oTransactionSeperator.m_dict.Count; i++)
                {
                    //Create header
                    if ((i > 0) && (i < oTransactionSeperator.m_dict.Count - 1))
                    {
                        // this.StartProcess(Utility.ConvertArrayListToArray(oTransactionSeperator.m_dict[i]));
                        this.Get863Content(Utility.ConvertArrayListToArray(oTransactionSeperator.m_dict[i]));
                        this.Get856Content(Utility.ConvertArrayListToArray(oTransactionSeperator.m_dict[i]));
                    }

                }
            }
            catch(Exception ex)
            {
                ErrorLog.CreateLog(ex);
            }
        }

        public List<string> Get863Content(string[] sArr)
        {
            try
            {
                //LIN**PO*17236*SN*2203036A*HN*NLK1660953~
                //LIN**SN*796052110*HN*0120961

                lstComplete863Content.Add(" ISA * 00 * *00 * *01 * 046798059 * 01 * 094972056 * 161010 * 1140 * U * 00401 * 000266940 * 0 * T *> ");

                lstComplete863Content.Add(" GS * RT * 046798059 * 094972056 * 20161010 * 1140 * 66940 * X * 004010 ");

                lstComplete863Content.Add("ST*863*0000");

                string sLinSection = Utility.GetElementLine("LIN*", sArr);
                string sHeatSection = Utility.GetElementLine("*HN*", sArr);
                string sPoSection = Utility.GetElementLine("PRF*", sArr);
                string sRefSi = Utility.GetElementLine("REF*BM*", sArr);
                sRefSi = sRefSi.Replace("REF*BM*", "REF*SI*");

                string sHeat = Utility.GetElementValue(sLinSection, "HN");
                string spo = Utility.GetElementValue(sPoSection, "PRF");
                string sTag = Utility.GetElementValue(sLinSection, "SN");

                string sNewLinSection = "LIN**PO*" + spo + "*SN*" + sTag + "*HN*" + sHeat ;

                lstComplete863Content.Add(sRefSi);
                lstComplete863Content.Add(sNewLinSection);

                lstComplete863Content.Add("CID * *69~");
                lstComplete863Content.Add(" MEA * PD * TH * .0152 * IN");
                

                                        // lstComplete863Content.Add(sHeatSection);
               // lstComplete863Content.Add(sPoSection);
               

                List<string> lstChemistry = Utility.GetChemistryContent(sArr);
                List<string> lstTest = Utility.GetTestContent(sArr);
                var allItem = lstComplete863Content.Concat(lstChemistry).Concat(lstTest).ToList();


                allItem.Add("CTT*1");
                allItem.Add("SE*00*00000");
                allItem.Add("GE*1*0000"); 
                allItem.Add("IEA*1*00000"); 
                lstComplete863Content = allItem;
            }
            catch(Exception ex)
            {
                ErrorLog.CreateLog(ex);
            }
            return lstComplete863Content;
        }

        public void Get856Content(string[] sArr)
        {

        }


        
    }

}