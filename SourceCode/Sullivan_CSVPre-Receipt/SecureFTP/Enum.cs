﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SecureFTP
{
    public struct FileStatus
    {
        public const int Processed = 1;
        public const int UnProcessed = 0;  
    }


    public struct EdiKeyWords
    {
        public const string CTT = "CTT";
        public const string BSN = "BSN";
        public const string DTM = "DTM";
        public const string TD1 = "TD1";
        public const string TD5 = "TD5";
        public const string TD3 = "TD3";
        public const string REF = "REF";
        public const string N1 = "N1";
        public const string LIN = "LIN";
        public const string HL = "HL";
        public const string PRF = "PRF";
        public const string MEA = "MEA";
        public const string SN1 = "SN1";
        public const string CID = "CID";
        public const string TMD = "TMD";
        public const string PSD = "PSD";

    }



}
