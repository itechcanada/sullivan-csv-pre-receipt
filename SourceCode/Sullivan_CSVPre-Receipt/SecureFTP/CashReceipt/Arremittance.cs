﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using iTECH.Library.DataAccess.MySql;
using iTECH.Library.Utilities;
using System.Data;
using MySql.Data.MySqlClient;
using System.Configuration;

namespace SecureFTP.CashReceipt
{
    class Arremittance
    {
        public StringBuilder m_sbSql = null;
        public string m_sSql = string.Empty;

        static string sStratixConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ToString();
        static string sMysqlConnectionString = ConfigurationManager.ConnectionStrings["NewConnectionString"].ToString();

        int iArremittanceId = 0;
        public int ArremittanceId
        {
            get { return iArremittanceId; }
            set { iArremittanceId = value; }
        }

        int iArPaymentId = 0;
        public int ArPaymentId
        {
            get { return iArPaymentId; }
            set { iArPaymentId = value; }
        }

        int iFileId = 0;
        public int FileId
        {
            get { return iFileId; }
            set { iFileId = value; }
        }

        string sRecordTypeIndicator = string.Empty;
        public string RecordTypeIndicator
        {
            get { return sRecordTypeIndicator; }
            set { sRecordTypeIndicator = value; }
        }

        string sRemittanceSequenceNumber = string.Empty;
        public string RemittanceSequenceNumber
        {
            get { return sRemittanceSequenceNumber; }
            set { sRemittanceSequenceNumber = value; }
        }

        string sOverflowSequenceQualifier = string.Empty;
        public string OverflowSequenceQualifier
        {
            get { return sOverflowSequenceQualifier; }
            set { sOverflowSequenceQualifier = value; }
        }

        string sRemittanceQualifierDescription = string.Empty;
        public string RemittanceQualifierDescription
        {
            get { return sRemittanceQualifierDescription; }
            set { sRemittanceQualifierDescription = value; }
        }

        string sRemittanceReference = string.Empty;
        public string RemittanceReference
        {
            get { return sRemittanceReference; }
            set { sRemittanceReference = value; }
        }

        string sRemittanceNetAmountPaid = string.Empty;
        public string RemittanceNetAmountPaid
        {
            get { return sRemittanceNetAmountPaid; }
            set { sRemittanceNetAmountPaid = value; }
        }

        string sRemittanceGrossAmount = string.Empty;
        public string RemittanceGrossAmount
        {
            get { return sRemittanceGrossAmount; }
            set { sRemittanceGrossAmount = value; }
        }

        string sRemittanceDiscountAmount = string.Empty;
        public string RemittanceDiscountAmount
        {
            get { return sRemittanceDiscountAmount; }
            set { sRemittanceDiscountAmount = value; }
        }

        string sRemittanceAmountDue = string.Empty;
        public string RemittanceAmountDue
        {
            get { return sRemittanceAmountDue; }
            set { sRemittanceAmountDue = value; }
        }

        string sRemittanceLateAmount = string.Empty;
        public string RemittanceLateAmount
        {
            get { return sRemittanceLateAmount; }
            set { sRemittanceLateAmount = value; }
        }

        string sRemittanceDate = string.Empty;
        public string RemittanceDate
        {
            get { return sRemittanceDate; }
            set { sRemittanceDate = value; }
        }


        public Arremittance()
        {

        }

        public Arremittance(int iRemittanceId)
        {
            this.ArremittanceId = iRemittanceId;
            DataTable dtRemittance = new DataTable();
            DbHelper oDbHelper = new DbHelper(true);
            m_sSql = "Select * from arremittance where ArremittanceId = " + this.ArremittanceId + " AND Active = 1";
            try
            {
                MySqlParameter[] oMySqlParameter = { };
                dtRemittance = oDbHelper.GetDataTable(m_sSql, CommandType.Text, oMySqlParameter);
                foreach (DataRow oDataRow in dtRemittance.Rows)
                {
                    ArPaymentId = BusinessUtility.GetInt(oDataRow["ArPaymentId"]);
                    FileId = BusinessUtility.GetInt(oDataRow["FileId"]);
                    RemittanceSequenceNumber = BusinessUtility.GetString(oDataRow["RemittanceSequenceNumber"]);
                    OverflowSequenceQualifier = BusinessUtility.GetString(oDataRow["OverflowSequenceQualifier"]);
                    RemittanceQualifierDescription = BusinessUtility.GetString(oDataRow["RemittanceQualifierDescription"]);
                    RemittanceReference = BusinessUtility.GetString(oDataRow["RemittanceReference"]);
                    RemittanceNetAmountPaid = BusinessUtility.GetString(oDataRow["RemittanceNetAmountPaid"]);
                }
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLog(ex);
                EdiError.LogError("AR", iFileId.ToString(), "", ex, "");
            }
            finally
            {
                oDbHelper.CloseDatabaseConnection();
            }
        }


        public int InsertRemittance(Arremittance oArremittance)
        {
            DbHelper dbHelp = new DbHelper(true);
            try
            {
                m_sbSql = new StringBuilder();
                m_sbSql.Append(" INSERT INTO arremittance ");
                m_sbSql.Append(" ( ");
                m_sbSql.Append(" ArPaymentId,FileId,RecordTypeIndicator,RemittanceSequenceNumber,OverflowSequenceQualifier, ");
                m_sbSql.Append(" RemittanceQualifierDescription,RemittanceReference,RemittanceNetAmountPaid,RemittanceGrossAmount,RemittanceDiscountAmount, ");
                m_sbSql.Append(" RemittanceAmountDue,RemittanceLateAmount,RemittanceDate ");
                m_sbSql.Append(" ) ");
                m_sbSql.Append("  values ('" + oArremittance.ArPaymentId + "', '" + oArremittance.FileId + "', '" + oArremittance.RecordTypeIndicator + "', '" 
                    + oArremittance.RemittanceSequenceNumber + "', '" + oArremittance.OverflowSequenceQualifier + "', '" + oArremittance.RemittanceQualifierDescription + "', '"
                    + oArremittance.RemittanceReference + "' , '" + oArremittance.RemittanceNetAmountPaid + "' , '" + oArremittance.RemittanceGrossAmount + "' , '"
                    + oArremittance.RemittanceDiscountAmount + "' , '" + oArremittance.RemittanceAmountDue + "' , '" + oArremittance.RemittanceLateAmount + "' , '" 
                    + oArremittance.RemittanceDate + "' );");

                MySqlParameter[] oMySqlParameter = { };
                if (dbHelp.ExecuteNonQuery(BusinessUtility.GetString(m_sbSql), CommandType.Text, oMySqlParameter) > 0)
                {
                    oArremittance.ArremittanceId = dbHelp.GetLastInsertID();
                }
                else
                {
                    oArremittance.ArremittanceId = 0;
                }

            }
            catch (Exception ex)
            {
                ErrorLog.CreateLog(ex);
                EdiError.LogError("", oArremittance.FileId.ToString(), "", ex, "");
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
            return oArremittance.ArremittanceId;

        }


        public DataTable GetRemittanceDetail(int iPaymentId, int iFileId)
        {
            DataTable dtCheck = new DataTable();
            DbHelper oDbHelper = new DbHelper(true);
            m_sSql = "Select * from arremittance where ArPaymentId = " + iPaymentId + " AND FileId = " + iFileId + " AND Active = 1";
            try
            {
                MySqlParameter[] oMySqlParameter = { };
                dtCheck = oDbHelper.GetDataTable(m_sSql, CommandType.Text, oMySqlParameter);
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLog(ex);
                EdiError.LogError("AR", iFileId.ToString(), "", ex, "");
            }
            finally
            {
                oDbHelper.CloseDatabaseConnection();
            }
            return dtCheck;

        }







    }
}
