﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using iTECH.Library.DataAccess.MySql;
using iTECH.Library.Utilities;
using System.Data;
using MySql.Data.MySqlClient;
using System.Configuration;

namespace SecureFTP.CashReceipt
{
    class Arpayment
    {
        public StringBuilder m_sbSql = null;
        public string m_sSql = string.Empty;

        static string sStratixConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ToString();
        static string sMysqlConnectionString = ConfigurationManager.ConnectionStrings["NewConnectionString"].ToString();

        int iArPaymentId = 0;
        public int ArPaymentId
        {
            get { return iArPaymentId; }
            set { iArPaymentId = value; }
        }

        int iFileId = 0;
        public int FileId
        {
            get { return iFileId; }
            set { iFileId = value; }
        }

        string sRecordType = string.Empty;
        public string RecordType
        {
            get { return sRecordType; }
            set { sRecordType = value; }
        }

        string sItemSequenceNo = "0";
        public string ItemSequenceNo
        {
            get { return sItemSequenceNo; }
            set { sItemSequenceNo = value; }
        }

        string sOdfiRtn = string.Empty;
        public string OdfiRtn
        {
            get { return sOdfiRtn; }
            set { sOdfiRtn = value; }
        }

        string sConsumerNumber = string.Empty;
        public string ConsumerNumber
        {
            get { return sConsumerNumber; }
            set { sConsumerNumber = value; }
        }

        string sCreditDebitIndicator = string.Empty;
        public string CreditDebitIndicator
        {
            get { return sCreditDebitIndicator; }
            set { sCreditDebitIndicator = value; }
        }

        string sAmountPaid = string.Empty;
        public string AmountPaid
        {
            get { return sAmountPaid; }
            set { sAmountPaid = value; }
        }

        string sGrossAmount = string.Empty;
        public string GrossAmount
        {
            get { return sGrossAmount; }
            set { sGrossAmount = value; }
        }

        string sPaymentMethod = string.Empty;
        public string PaymentMethod
        {
            get { return sPaymentMethod; }
            set { sPaymentMethod = value; }
        }

        string sPaymentMethodTypeCode = string.Empty;
        public string PaymentMethodTypeCode
        {
            get { return sPaymentMethodTypeCode; }
            set { sPaymentMethodTypeCode = value; }
        }

        string sSenderPaymentReferenceNumber = string.Empty;
        public string SenderPaymentReferenceNumber
        {
            get { return sSenderPaymentReferenceNumber; }
            set { sSenderPaymentReferenceNumber = value; }
        }

        string sProcessingSystemReferenceNumber = string.Empty;
        public string ProcessingSystemReferenceNumber
        {
            get { return sProcessingSystemReferenceNumber; }
            set { sProcessingSystemReferenceNumber = value; }
        }

        string sWellsFargoReferenceNumber = string.Empty;
        public string WellsFargoReferenceNumber
        {
            get { return sWellsFargoReferenceNumber; }
            set { sWellsFargoReferenceNumber = value; }
        }

        string sDepositSettlementDate = string.Empty;
        public string DepositSettlementDate
        {
            get { return sDepositSettlementDate; }
            set { sDepositSettlementDate = value; }
        }

        string sPaymentEffectiveDate = string.Empty;
        public string PaymentEffectiveDate
        {
            get { return sPaymentEffectiveDate; }
            set { sPaymentEffectiveDate = value; }
        }

        string sTransactionProcessingDateTime = string.Empty;
        public string TransactionProcessingDateTime
        {
            get { return sTransactionProcessingDateTime; }
            set { sTransactionProcessingDateTime = value; }
        }

        string sReference1 = string.Empty;
        public string Reference1
        {
            get { return sReference1; }
            set { sReference1 = value; }
        }

        string sNoOfRemittance = string.Empty;
        public string NoOfRemittance
        {
            get { return sNoOfRemittance; }
            set { sNoOfRemittance = value; }
        }

        string sCustomerId = string.Empty;
        public string CustomerId
        {
            get { return sCustomerId; }
            set { sCustomerId = value; }
        }

        string sCashRctSessionIdentity = string.Empty;
        public string CashRctSessionIdentity
        {
            get { return sCashRctSessionIdentity; }
            set { sCashRctSessionIdentity = value; }
        }

        string sCashRctIdentity = string.Empty;
        public string CashRctIdentity
        {
            get { return sCashRctIdentity; }
            set { sCashRctIdentity = value; }
        }

        public Arpayment()
        {

        }

        public Arpayment(int iPaymentId)
        {
            this.ArPaymentId = iPaymentId;
            DataTable dtCheck = new DataTable();
            DbHelper oDbHelper = new DbHelper(true);
            m_sSql = "Select * from arpayment where ArPaymentId = " + this.ArPaymentId + " AND Active = 1";
            try
            {
                MySqlParameter[] oMySqlParameter = { };
                dtCheck = oDbHelper.GetDataTable(m_sSql, CommandType.Text, oMySqlParameter);
                foreach (DataRow oDataRow in dtCheck.Rows)
                {
                    ArPaymentId = BusinessUtility.GetInt(oDataRow["ArPaymentId"]);
                    FileId = BusinessUtility.GetInt(oDataRow["FileId"]);
                    ItemSequenceNo = BusinessUtility.GetString(oDataRow["ItemSequenceNo"]);
                    OdfiRtn = BusinessUtility.GetString(oDataRow["OdfiRtn"]);
                    ConsumerNumber = BusinessUtility.GetString(oDataRow["ConsumerNumber"]);
                    CreditDebitIndicator = BusinessUtility.GetString(oDataRow["CreditDebitIndicator"]);
                    AmountPaid = BusinessUtility.GetString(oDataRow["AmountPaid"]);
                    PaymentMethod = BusinessUtility.GetString(oDataRow["PaymentMethod"]);
                    DepositSettlementDate = BusinessUtility.GetString(oDataRow["DepositSettlementDate"]);
                    TransactionProcessingDateTime = BusinessUtility.GetString(oDataRow["TransactionProcessingDateTime"]);
                    CustomerId = BusinessUtility.GetString(oDataRow["CustomerId"]);
                    CashRctSessionIdentity = BusinessUtility.GetString(oDataRow["CashRctSessionIdentity"]);
                    CashRctIdentity = BusinessUtility.GetString(oDataRow["CashRctIdentity"]);
                    SenderPaymentReferenceNumber = BusinessUtility.GetString(oDataRow["SenderPaymentReferenceNumber"]);
                }
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLog(ex);
                EdiError.LogError("100", iFileId.ToString(), "", ex, "");
            }
            finally
            {
                oDbHelper.CloseDatabaseConnection();
            }
        }

        public int InsertPayment(Arpayment oArpayment)
        {
            DbHelper dbHelp = new DbHelper(true);
            try
            {
                m_sbSql = new StringBuilder();
                m_sbSql.Append(" INSERT INTO arpayment ");
                m_sbSql.Append(" ( ");
                m_sbSql.Append(" FileId,RecordType,ItemSequenceNo,OdfiRtn,ConsumerNumber,CreditDebitIndicator,AmountPaid,GrossAmount,PaymentMethod, ");
                m_sbSql.Append(" PaymentMethodTypeCode,SenderPaymentReferenceNumber,ProcessingSystemReferenceNumber,WellsFargoReferenceNumber, ");
                m_sbSql.Append(" DepositSettlementDate,PaymentEffectiveDate,TransactionProcessingDateTime,Reference1,NoOfRemittance ");
                m_sbSql.Append(" ) ");
                m_sbSql.Append(" VALUES ");
                m_sbSql.Append(" ( ");
                m_sbSql.Append(oArpayment.FileId);
                m_sbSql.Append(",'");
                m_sbSql.Append(oArpayment.RecordType);
                m_sbSql.Append("',");

                m_sbSql.Append("'");
                m_sbSql.Append(oArpayment.ItemSequenceNo);
                m_sbSql.Append("',");
                m_sbSql.Append("'");
                m_sbSql.Append(oArpayment.OdfiRtn);
                m_sbSql.Append("',");
                m_sbSql.Append("'");
                m_sbSql.Append(oArpayment.ConsumerNumber);
                m_sbSql.Append("',");
                m_sbSql.Append("'");
                m_sbSql.Append(oArpayment.CreditDebitIndicator);
                m_sbSql.Append("',");
                m_sbSql.Append("'");
                m_sbSql.Append(oArpayment.AmountPaid);
                m_sbSql.Append("',");
                m_sbSql.Append("'");
                m_sbSql.Append(oArpayment.GrossAmount);
                m_sbSql.Append("',");
                m_sbSql.Append("'");
                m_sbSql.Append(oArpayment.PaymentMethod);
                m_sbSql.Append("',");
                m_sbSql.Append("'");
                m_sbSql.Append(oArpayment.PaymentMethodTypeCode);
                m_sbSql.Append("',");
                m_sbSql.Append("'");
                m_sbSql.Append(oArpayment.SenderPaymentReferenceNumber);
                m_sbSql.Append("',");
                m_sbSql.Append("'");
                m_sbSql.Append(oArpayment.ProcessingSystemReferenceNumber);
                m_sbSql.Append("',");
                m_sbSql.Append("'");
                m_sbSql.Append(oArpayment.WellsFargoReferenceNumber);
                m_sbSql.Append("',");
                m_sbSql.Append("'");
                m_sbSql.Append(oArpayment.DepositSettlementDate);
                m_sbSql.Append("',");
                m_sbSql.Append("'");
                m_sbSql.Append(oArpayment.PaymentEffectiveDate);
                m_sbSql.Append("',");
                m_sbSql.Append("'");
                m_sbSql.Append(oArpayment.TransactionProcessingDateTime);
                m_sbSql.Append("',");
                m_sbSql.Append("'");
                m_sbSql.Append(oArpayment.Reference1);
                m_sbSql.Append("',");
                m_sbSql.Append("'");
                m_sbSql.Append(oArpayment.NoOfRemittance);
                m_sbSql.Append("'");
            

                m_sbSql.Append(" ) ");

                MySqlParameter[] oMySqlParameter = {};
                if (dbHelp.ExecuteNonQuery(BusinessUtility.GetString(m_sbSql), CommandType.Text, oMySqlParameter) > 0)
                {
                    oArpayment.ArPaymentId = dbHelp.GetLastInsertID();
                }
                else
                {
                     oArpayment.ArPaymentId = 0;
                } 

            }
            catch (Exception ex)
            {
                ErrorLog.CreateLog(ex);
                EdiError.LogError("100", oArpayment.FileId.ToString(), "", ex, "");
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
            return oArpayment.ArPaymentId;

        }

        
        public DataTable GetCheckDetail(int iFileId)
        {
            DataTable dtCheck = new DataTable();
            DbHelper oDbHelper = new DbHelper(true);
            m_sSql = "Select * from arpayment where Fileid = " + iFileId + " AND Active = 1";
            try
            {
                MySqlParameter[] oMySqlParameter = { };
                dtCheck = oDbHelper.GetDataTable(m_sSql, CommandType.Text, oMySqlParameter);
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLog(ex);
                EdiError.LogError("100", iFileId.ToString(), "", ex, "");
            }
            finally
            {
                oDbHelper.CloseDatabaseConnection();
            }
            return dtCheck;

        }



        public void UpdateCustomerId(int iPaymentId, string sCustid, int iFileId)
        {
            DbHelper oDbHelper = new DbHelper(true);
            m_sSql = "update arpayment set CustomerId = '" + sCustid + "' where Fileid = " + iFileId + " AND ArPaymentId = " + iPaymentId + " AND Active = 1";
            try
            {
                MySqlParameter[] oMySqlParameter = { };
                oDbHelper.ExecuteNonQuery(m_sSql, CommandType.Text, oMySqlParameter);
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLog(ex);
                EdiError.LogError("100", iFileId.ToString(), "", ex, "");
            }
            finally
            {
                oDbHelper.CloseDatabaseConnection();
            }

        }


        public void UpdateSessionIdentity(int iPaymentId, string sCashRctSessionIdentity,string sCashRctIdentity, int iFileId)
        {
            DbHelper oDbHelper = new DbHelper(true);
            m_sSql = "update arpayment set CashRctSessionIdentity = '" + sCashRctSessionIdentity + "', CashRctIdentity = '" + sCashRctIdentity + "'  where Fileid = " + iFileId + " AND ArPaymentId = " + iPaymentId + " AND Active = 1";
            try
            {
                MySqlParameter[] oMySqlParameter = { };
                oDbHelper.ExecuteNonQuery(m_sSql, CommandType.Text, oMySqlParameter);
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLog(ex);
                EdiError.LogError("100", iFileId.ToString(), "", ex, "");
            }
            finally
            {
                oDbHelper.CloseDatabaseConnection();
            }     

        }


        public void UpdateSessionToFailedPayment(int iPaymentId, string sCashRctSessionIdentity, int iFileId)
        {
            EdiError.LogError("100", Globalcl.FileId.ToString(), "updating session to failed payment " + iPaymentId);
            sCashRctIdentity = string.Empty;
            DbHelper oDbHelper = new DbHelper(true);
            m_sSql = "update arpayment set CashRctSessionIdentity = '" + sCashRctSessionIdentity + "', CashRctIdentity = '" + sCashRctIdentity + "'  where Fileid = " + iFileId + " AND ArPaymentId = " + iPaymentId + " AND Active = 1";
            try
            {
                MySqlParameter[] oMySqlParameter = { };
                oDbHelper.ExecuteNonQuery(m_sSql, CommandType.Text, oMySqlParameter);
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLog(ex);
                EdiError.LogError("100", iFileId.ToString(), "", ex, "");
            }
            finally
            {
                oDbHelper.CloseDatabaseConnection();
            }

        }



        public void UpdateARReferenceItemNumber(int iRemitanceId, string sReferenceItemNumber, int iFileId)
        {
            DbHelper oDbHelper = new DbHelper(true);
            m_sSql = "update arremittance set ReferenceItemNumber = '" + sReferenceItemNumber + "' where Fileid = " + iFileId + " AND ArremittanceId = " + iRemitanceId + " AND Active = 1";
            try
            {
                MySqlParameter[] oMySqlParameter = { };
                oDbHelper.ExecuteNonQuery(m_sSql, CommandType.Text, oMySqlParameter);
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLog(ex);
                EdiError.LogError("100", iFileId.ToString(), "", ex, "UpdateARReferenceItemNumber error");
            }
            finally
            {
                oDbHelper.CloseDatabaseConnection();
            }

        }

        public void GetCheckNoAndAmount(int iFileId, ref decimal lngAmount, ref int iTotalCheck)
        {
            DataTable dtCheck = new DataTable();
            DbHelper oDbHelper = new DbHelper(true);
            m_sSql = " select sum( cast(REPLACE(AmountPaid, ',','') as decimal(12,2)) ) as TotalAmount, count(FileId) as NoOfCheck FROM arpayment WHERE CustomerId != '' AND FileId = " + iFileId + "  GROUP BY FileId ";
            try
            {
                MySqlParameter[] oMySqlParameter = { };
                dtCheck = oDbHelper.GetDataTable(m_sSql, CommandType.Text, oMySqlParameter);
                if (dtCheck.Rows.Count > 0)
                {
                    lngAmount = BusinessUtility.GetDecimal(dtCheck.Rows[0]["TotalAmount"]);
                    iTotalCheck = BusinessUtility.GetInt(dtCheck.Rows[0]["NoOfCheck"]);
                }
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLog(ex);
                EdiError.LogError("100", iFileId.ToString(), "", ex, "");
            }
            finally
            {
                oDbHelper.CloseDatabaseConnection();
            }

        }

        internal Arremittance Arremittance
        {
            get => default(Arremittance);
            set
            {
            }
        }
    }
}
