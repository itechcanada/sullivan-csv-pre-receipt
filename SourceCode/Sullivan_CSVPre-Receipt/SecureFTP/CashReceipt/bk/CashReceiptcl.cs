﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SecureFTP.CashReceiptSsession;
using iTECH.Library.Utilities;
using SecureFTP;
using System.Configuration;

    /*! \brief Class
     *  \details Class holds fuctions to create cash receipt.
     */
public class CashReceiptcl
{
    int m_iCashRctSessionIdentity = 0;
    string m_sCashRctIdentity = string.Empty;
    int m_iReferenceItemNumber = 0;
    string m_sSql = string.Empty;
    Authentication oAuthentication ;
    SecureFTP.CashReceiptSsession.AuthenticationToken oAuthenticationToken;
    SecureFTP.GatewayAuthService.AuthenticationToken oAuthenticationToken1 = new SecureFTP.GatewayAuthService.AuthenticationToken();

    static string m_sBankId = BusinessUtility.GetString(ConfigurationManager.AppSettings["CashRctBankId"]);
    static string m_sBranchId = BusinessUtility.GetString(ConfigurationManager.AppSettings["CashRctBranchId"]);
    static int m_iJournalDate = BusinessUtility.GetInt(ConfigurationManager.AppSettings["JournalDate"]);

    public SecureFTP.GatewayAuthService.AuthenticationToken AuthenticationTokenProp
    {
        get { return oAuthenticationToken1; }
    }


    public CashReceiptcl()
    {
    }

    /*! \brief Function
     *  \details Function to  create CashReceiptSession. This operation creates a new Cash Receipt Deposit Session with a system-generated Session ID.
     *  \details select * from scrbnk_rec  : Table for bank detail
     *  \details 4366 - The journal date must be within 03 days of today's date.
     *  \param[out] m_iCashRctSessionIdentity.
     */
    public int CreateCashReceiptSession()
    {
        oAuthentication = new Authentication();
        try
        {
            DateTime dtDateTime = DateTime.Now;
            DateTime dtJurnalDate = dtDateTime.Date.AddDays(1);
            DateTime dtDepositDate = dtDateTime.Date;


            oAuthenticationToken1 = oAuthentication.UserAuthentication();

            oAuthenticationToken = new AuthenticationToken();
            oAuthenticationToken.username = oAuthenticationToken1.username;
            oAuthenticationToken.value = oAuthenticationToken1.value;

            CashRctSession oCashRctSession = new CashRctSession();
            oCashRctSession.cashRctBranchId = m_sBranchId;  // i84_crcp_brh of XCTI84_REC
            oCashRctSession.bankId = m_sBankId;           // i84_bnk of  XCTI84_REC
            oCashRctSession.journalDate = dtJurnalDate;
            oCashRctSession.depositDate = dtDepositDate;
            //oCashRctSession.controlDepositAmount
            //oCashRctSession.controlNumberOfChecks

            ServiceMessages smsg = new ServiceMessages();
            CashReceiptSessionServiceClient oCashReceiptSessionServiceClient = new CashReceiptSessionServiceClient();
            smsg = oCashReceiptSessionServiceClient.CreateCashRctSession(ref oAuthenticationToken, oCashRctSession, out m_iCashRctSessionIdentity);
            
            ErrorLog.createLog(" CreateCashReceiptSession method m_iCashRctSessionIdentity = " + m_iCashRctSessionIdentity);

            oAuthentication.UserAuthenticationLogOut(oAuthenticationToken1);
        }
        catch (Exception ex)
        {
            ErrorLog.createLog(" CreateCashReceiptSession error . control in catch block");
            ErrorLog.createLog(ex.ToString());
            oAuthentication.UserAuthenticationLogOut(oAuthenticationToken1);
        }

        return m_iCashRctSessionIdentity;
    }

    public int CreateCashReceiptSession(int icontrolNumberOfChecks, decimal dcontrolDepositAmount, DateTime dtDepDate)
    {
        oAuthentication = new Authentication();
        try
        {
            DateTime dtDateTime = DateTime.Now;
            DateTime dtJurnalDate = dtDepDate.AddDays(m_iJournalDate);
            DateTime dtDepositDate = dtDepDate;

            ErrorLog.createLog("Hit auth in CreateCashReceiptSession ");
            oAuthenticationToken1 = oAuthentication.UserAuthentication();

            oAuthenticationToken = new AuthenticationToken();
            oAuthenticationToken.username = oAuthenticationToken1.username;
            oAuthenticationToken.value = oAuthenticationToken1.value;

            CashRctSession oCashRctSession = new CashRctSession();
            oCashRctSession.cashRctBranchId = m_sBranchId;  // i84_crcp_brh of XCTI84_REC
            oCashRctSession.bankId = m_sBankId;           // i84_bnk of  XCTI84_REC
            oCashRctSession.journalDate = dtJurnalDate;
            oCashRctSession.depositDate = dtDepositDate;

            oCashRctSession.controlDepositAmount = dcontrolDepositAmount;
            oCashRctSession.controlDepositAmountSpecified = true;
            oCashRctSession.controlNumberOfChecks = icontrolNumberOfChecks;
            oCashRctSession.controlNumberOfChecksSpecified = true;


            ErrorLog.createLog("dtJurnalDate = " + dtJurnalDate);

            ServiceMessages smsg = new ServiceMessages();
            CashReceiptSessionServiceClient oCashReceiptSessionServiceClient = new CashReceiptSessionServiceClient();
            smsg = oCashReceiptSessionServiceClient.CreateCashRctSession(ref oAuthenticationToken, oCashRctSession, out m_iCashRctSessionIdentity);

            ErrorLog.createLog(" CreateCashReceiptSession method m_iCashRctSessionIdentity = " + m_iCashRctSessionIdentity);

            oAuthentication.UserAuthenticationLogOut(oAuthenticationToken1);
        }
        catch (Exception ex)
        {
            ErrorLog.createLog(" CreateCashReceiptSession error . control in catch block");
            ErrorLog.createLog(ex.ToString());
            oAuthentication.UserAuthenticationLogOut(oAuthenticationToken1);
        }

        return m_iCashRctSessionIdentity;
    }


    /*! \brief Function
     *  \details Function to  create CashReceipt.
     *  \param[in] iCashRctSessionIdentity Unique identity.
     *  \param[out] m_sCashRctIdentity.
     */
    public string CreateCashReceipt(int iCashRctSessionIdentity, string sCustomerId, string sCheckNumber, decimal dCheckAmt)
    {
        ErrorLog.createLog(" CreateCashReceipt method start sCustomerId = " + sCustomerId + " and sCheckNumber = " + sCheckNumber + " and dCheckAmt = " + dCheckAmt);
        SecureFTP.CashReceipt.AuthenticationToken oAuthenticationToken = new SecureFTP.CashReceipt.AuthenticationToken();
        try
        {
            oAuthenticationToken1 = oAuthentication.UserAuthentication();
            oAuthenticationToken.username = oAuthenticationToken1.username;
            oAuthenticationToken.value = oAuthenticationToken1.value;

            SecureFTP.CashReceipt.CashRct oCashRct = new SecureFTP.CashReceipt.CashRct();
            oCashRct.customerId = sCustomerId;
            oCashRct.sessionIdentity = iCashRctSessionIdentity;
            oCashRct.sessionIdentitySpecified = true;
            oCashRct.customerCheckNumber = sCheckNumber;
            oCashRct.customerCheckAmount = dCheckAmt;
            oCashRct.customerCheckAmountSpecified = true;


            SecureFTP.CashReceipt.CashReceiptServiceClient oCashReceiptServiceClient = new SecureFTP.CashReceipt.CashReceiptServiceClient();
            oCashReceiptServiceClient.CreateCashRct(ref oAuthenticationToken, oCashRct, out m_sCashRctIdentity);
            ErrorLog.createLog(" CreateCashReceipt method m_sCashRctIdentity = " + m_sCashRctIdentity);
            oAuthentication.UserAuthenticationLogOut(oAuthenticationToken1);
        }
        catch (Exception ex)
        {
            ErrorLog.createLog(" CreateCashReceipt error . control in catch block");
            ErrorLog.CreateLog(ex);
            oAuthentication.UserAuthenticationLogOut(oAuthenticationToken1);
        }
        return m_sCashRctIdentity;
    }


    /*! \brief Function
     *  \details Function to  create AccountReceivableDist.
     *  \param[in] sCashRctIdentity Unique identity.
     *  \param[out] m_iReferenceItemNumber.
     */
    public int CreateAccountReceivableDist(string sCashRctIdentity, int iCashRctSessionIdentity, string sInvoiceNo, decimal dPaymentAmount)
    {
        SecureFTP.AccountsReceivable.AuthenticationToken oAuthenticationToken = new SecureFTP.AccountsReceivable.AuthenticationToken();
        ErrorLog.createLog("CreateAccountReceivableDist method start .. with sCashRctIdentity = " + sCashRctIdentity + " and iCashRctSessionIdentity = " + iCashRctSessionIdentity);
        ErrorLog.createLog(" CreateAccountReceivableDist method sInvoiceNo = " + sInvoiceNo + " and dPaymentAmount = " + dPaymentAmount);
        try
        {
            oAuthenticationToken1 = oAuthentication.UserAuthentication();
            oAuthenticationToken.username = oAuthenticationToken1.username;
            oAuthenticationToken.value = oAuthenticationToken1.value;

            SecureFTP.AccountsReceivable.ARDist oARDist = new SecureFTP.AccountsReceivable.ARDist();
            oARDist.invoiceNumber = sInvoiceNo;
            oARDist.paymentAmount = dPaymentAmount;
            oARDist.paymentAmountSpecified = true;

            SecureFTP.AccountsReceivable.AccountsReceivableDistributionServiceClient oAccountsReceivableDistributionServiceClient = new SecureFTP.AccountsReceivable.AccountsReceivableDistributionServiceClient();
            ErrorLog.createLog("CreateAccountReceivableDist hitting service for CreateARDist");
            oAccountsReceivableDistributionServiceClient.CreateARDist(ref oAuthenticationToken, sCashRctIdentity, oARDist, out m_iReferenceItemNumber);
            oAuthentication.UserAuthenticationLogOut(oAuthenticationToken1);
        }
        catch (Exception ex)
        {
            ErrorLog.createLog("Error occur in CreateAccountReceivableDist so m_iReferenceItemNumber = " + m_iReferenceItemNumber);
            ErrorLog.CreateLog(ex);
            oAuthentication.UserAuthenticationLogOut(oAuthenticationToken1);
        }
        return m_iReferenceItemNumber;
    }


    public int GetCustomerId(string sInvoiceNo)
    {
        int iCustomerId = 0;
        m_sSql = "select * from ivtivh_rec where ivh_upd_ref_no ='" + sInvoiceNo + "'";
        return iCustomerId;
    }



}