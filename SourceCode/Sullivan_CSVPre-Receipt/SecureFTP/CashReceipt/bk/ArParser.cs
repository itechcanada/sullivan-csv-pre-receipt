﻿using iTECH.Library.Utilities;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;

using SecureFTP.CashReceipt;
using System.Data;
using System.Globalization;

namespace SecureFTP
{
    class ArParser
    {
        static string sStratixConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ToString();
        static string m_sFileType = BusinessUtility.GetString(ConfigurationManager.AppSettings["FileType"]);

        Dictionary<int, ArrayList> m_dict = new Dictionary<int, ArrayList>();
        string[] m_sArr = null;
        ArrayList m_arl = new ArrayList();
        int m_iFileId = 0;
        DateTime dtDepositDate = new DateTime();


        public ArParser()
        {
        }

        public ArParser(string[] obj, int iFileId)
        {
          
            m_iFileId = iFileId;
            m_sArr = obj;

           try
           {

            this.StartProcessing();

            for (int i = 0; i < m_dict.Count; i++)
            {
                //Create header
                if ((i > 0) && (i < m_dict.Count ))
                {
                    this.FetchData(Utility.ConvertArrayListToArray(m_dict[i]));
                }

            }

            Arpayment oArpayment = new Arpayment();
            Arremittance oArremittance = new Arremittance();
            DataTable dtCheckPayment = new DataTable();
            DataTable dtRemittance = new DataTable();
            int iCashReceiptSession = 0;
            string sCashRctIdentity = string.Empty;
            int iARReferenceItemNumber = 0;
            tctipd otctipd = new tctipd();
            dtCheckPayment = oArpayment.GetCheckDetail(m_iFileId);

            decimal lngTotalCheckAmount = 0;
            int iNoOfCheck = 0;

            int iCreateOneCashReceiptPerFile = BusinessUtility.GetInt(ConfigurationManager.AppSettings["CreateOneCashReceiptPerFile"]);

            if (iCreateOneCashReceiptPerFile > 0)
            {

                CashReceiptcl oCashReceiptcl = new CashReceiptcl();
                oArpayment.GetCheckNoAndAmount(m_iFileId, ref lngTotalCheckAmount, ref iNoOfCheck);
                dtDepositDate = DateTime.Now;
                iCashReceiptSession = oCashReceiptcl.CreateCashReceiptSession(iNoOfCheck, lngTotalCheckAmount, dtDepositDate);

                ErrorLog.createLog("iCashReceiptSession = " + iCashReceiptSession);

                ErrorLog.createLog(" dtCheckPayment.Rows = " + dtCheckPayment.Rows.Count);
                foreach (DataRow oDataRow in dtCheckPayment.Rows)
                {
                    oArpayment.ArPaymentId = BusinessUtility.GetInt(oDataRow["ArPaymentId"]);
                    Console.WriteLine("Processing paymentid : " + oArpayment.ArPaymentId);
                    if (oArpayment.ArPaymentId > 0)
                    {
                        oArpayment = new Arpayment(oArpayment.ArPaymentId);
                        dtRemittance = oArremittance.GetRemittanceDetail(oArpayment.ArPaymentId, m_iFileId);
                        // create session here

                        if (!otctipd.CheckCHQExists(oArpayment.SenderPaymentReferenceNumber))
                        {
                            Console.WriteLine("CHQ processing started " + oArpayment.SenderPaymentReferenceNumber);
                            ErrorLog.createLog("CHQ processing started " + oArpayment.SenderPaymentReferenceNumber);
                            if (oArpayment.CustomerId.Length > 0)
                            {
                              
                                if (iCashReceiptSession > 0)
                                {
                                    sCashRctIdentity = oCashReceiptcl.CreateCashReceipt(iCashReceiptSession, oArpayment.CustomerId, oArpayment.SenderPaymentReferenceNumber, BusinessUtility.GetDecimal(oArpayment.AmountPaid));
                                }

                                ErrorLog.createLog("iCashReceiptSession  = " + iCashReceiptSession + " sCashRctIdentity = " + sCashRctIdentity);
                                oArpayment.UpdateSessionIdentity(oArpayment.ArPaymentId, BusinessUtility.GetString(iCashReceiptSession), sCashRctIdentity, m_iFileId);


                                foreach (DataRow oDataRow1 in dtRemittance.Rows)
                                {
                                    oArremittance.ArremittanceId = BusinessUtility.GetInt(oDataRow1["ArremittanceId"]);
                                    oArremittance = new Arremittance(oArremittance.ArremittanceId);

                                    if (!string.IsNullOrEmpty(sCashRctIdentity))
                                    {
                                        iARReferenceItemNumber = oCashReceiptcl.CreateAccountReceivableDist(sCashRctIdentity, iCashReceiptSession, oArremittance.RemittanceReference, BusinessUtility.GetDecimal(oArremittance.RemittanceNetAmountPaid));
                                        ErrorLog.createLog("iARReferenceItemNumber in arparser is : " + iARReferenceItemNumber);
                                        oArpayment.UpdateARReferenceItemNumber(oArremittance.ArremittanceId, BusinessUtility.GetString(iARReferenceItemNumber), m_iFileId);
                                    }

                                    // create ar dist here
                                }

                                Authentication oAuthentication = new Authentication();
                                oAuthentication.UserAuthenticationLogOut(oCashReceiptcl.AuthenticationTokenProp);

                            }
                            else
                            {

                                ErrorLog.createLog("Customer id not available.");
                                EdiError.LogError("100", Globalcl.FileId.ToString(), "Customer id not available.");
                                Console.WriteLine("Updating Session to failed payment");
                                oArpayment.UpdateSessionToFailedPayment(oArpayment.ArPaymentId, BusinessUtility.GetString(iCashReceiptSession), m_iFileId);
                            }
                            Console.WriteLine("CHQ processing completed " + oArpayment.SenderPaymentReferenceNumber);
                            ErrorLog.createLog("CHQ processing completed " + oArpayment.SenderPaymentReferenceNumber);
                            Utility.WaitForSecond(2);
                        }
                        else
                        {
                            Console.WriteLine("CHQ NO : '" + oArpayment.SenderPaymentReferenceNumber + "' already processed.");
                            ErrorLog.createLog("CHQ NO : '" + oArpayment.SenderPaymentReferenceNumber + "' already processed.");
                            EdiError.LogError("100", Globalcl.FileId.ToString(), "CHQ NO : ''" + oArpayment.SenderPaymentReferenceNumber + "'' already processed.");
                        }


                    }
                } // foreach  dtCheckPayment end here..


            }
            else
            {

                foreach (DataRow oDataRow in dtCheckPayment.Rows)
                {
                    oArpayment.ArPaymentId = BusinessUtility.GetInt(oDataRow["ArPaymentId"]);
                    Console.WriteLine("Processing paymentid : " + oArpayment.ArPaymentId);
                    if (oArpayment.ArPaymentId > 0)
                    {
                        oArpayment = new Arpayment(oArpayment.ArPaymentId);
                        dtRemittance = oArremittance.GetRemittanceDetail(oArpayment.ArPaymentId, m_iFileId);
                        // create session here

                        if (!otctipd.CheckCHQExists(oArpayment.SenderPaymentReferenceNumber))
                        {
                            Console.WriteLine("CHQ processing started " + oArpayment.SenderPaymentReferenceNumber);
                            ErrorLog.createLog("CHQ processing started " + oArpayment.SenderPaymentReferenceNumber);
                            if (oArpayment.CustomerId.Length > 0)
                            {
                                CashReceiptcl oCashReceiptcl = new CashReceiptcl();
                                iCashReceiptSession = oCashReceiptcl.CreateCashReceiptSession();
                                if (iCashReceiptSession > 0)
                                {
                                    sCashRctIdentity = oCashReceiptcl.CreateCashReceipt(iCashReceiptSession, oArpayment.CustomerId, oArpayment.SenderPaymentReferenceNumber, BusinessUtility.GetDecimal(oArpayment.AmountPaid));
                                }

                                ErrorLog.createLog("iCashReceiptSession  = " + iCashReceiptSession + " sCashRctIdentity = " + sCashRctIdentity);
                                oArpayment.UpdateSessionIdentity(oArpayment.ArPaymentId, BusinessUtility.GetString(iCashReceiptSession), sCashRctIdentity, m_iFileId);


                                foreach (DataRow oDataRow1 in dtRemittance.Rows)
                                {
                                    oArremittance.ArremittanceId = BusinessUtility.GetInt(oDataRow1["ArremittanceId"]);
                                    oArremittance = new Arremittance(oArremittance.ArremittanceId);

                                    if (!string.IsNullOrEmpty(sCashRctIdentity))
                                    {
                                        iARReferenceItemNumber = oCashReceiptcl.CreateAccountReceivableDist(sCashRctIdentity, iCashReceiptSession, oArremittance.RemittanceReference, BusinessUtility.GetDecimal(oArremittance.RemittanceNetAmountPaid));
                                        ErrorLog.createLog("iARReferenceItemNumber in arparser is : " + iARReferenceItemNumber);
                                        oArpayment.UpdateARReferenceItemNumber(oArremittance.ArremittanceId, BusinessUtility.GetString(iARReferenceItemNumber), m_iFileId);
                                    }

                                    // create ar dist here
                                }

                                Authentication oAuthentication = new Authentication();
                                oAuthentication.UserAuthenticationLogOut(oCashReceiptcl.AuthenticationTokenProp);

                            }
                            else
                            {
                                ErrorLog.createLog("Customer id not available.");
                                EdiError.LogError("100", Globalcl.FileId.ToString(), "Customer id not available.");
                            }
                            Console.WriteLine("CHQ processing completed " + oArpayment.SenderPaymentReferenceNumber);
                            ErrorLog.createLog("CHQ processing completed " + oArpayment.SenderPaymentReferenceNumber);
                            Utility.WaitForSecond(2);
                        }
                        else
                        {
                            Console.WriteLine("CHQ NO : '" + oArpayment.SenderPaymentReferenceNumber + "' already processed.");
                            ErrorLog.createLog("CHQ NO : '" + oArpayment.SenderPaymentReferenceNumber + "' already processed.");
                            EdiError.LogError("100", Globalcl.FileId.ToString(), "CHQ NO : ''" + oArpayment.SenderPaymentReferenceNumber + "'' already processed.");
                        }


                    }
                } // foreach  dtCheckPayment end here..
            }
        }
           catch (Exception ex)
            {
                ErrorLog.CreateLog(ex);
                EdiError.LogError("100", Globalcl.FileId.ToString(), "", ex, "Error in ar ArParser constructor");
            }


        }


        public void FetchData(string[] sArr)
        {
            string sText = string.Empty;
            string[] sSplit = null;
            int iPaymentId = 0;
            try
            {
                foreach (string s in sArr)
                {
                    sText = s;
                    sSplit = sText.Split('|');
                    if (sSplit[0].Equals("PR"))
                    {
                        if (sSplit[7].Equals("CHK"))
                        {
                            Arpayment oArpayment = new Arpayment();
                            oArpayment.AmountPaid = Utility.PutDecimalAtSpecificPlace(BusinessUtility.GetInt(sSplit[5])).ToString("N2");
                            oArpayment.GrossAmount = sSplit[6];
                            oArpayment.PaymentMethod = sSplit[7];
                            oArpayment.PaymentMethodTypeCode = sSplit[8];
                            oArpayment.SenderPaymentReferenceNumber = sSplit[9];
                            oArpayment.ProcessingSystemReferenceNumber = sSplit[10];
                            oArpayment.WellsFargoReferenceNumber = sSplit[11];
                            oArpayment.DepositSettlementDate = sSplit[12];
                            oArpayment.PaymentEffectiveDate = sSplit[13];
                            oArpayment.TransactionProcessingDateTime = sSplit[14];
                            oArpayment.CreditDebitIndicator = sSplit[4];
                            oArpayment.ItemSequenceNo = sSplit[1];
                            oArpayment.OdfiRtn = sSplit[2];
                            oArpayment.ConsumerNumber = sSplit[3];
                            oArpayment.FileId = m_iFileId;
                            oArpayment.InsertPayment(oArpayment);
                            iPaymentId = oArpayment.ArPaymentId;

                            ErrorLog.createLog("iPaymentId = " + iPaymentId);
                            ErrorLog.createLog("m_iFileId = " + m_iFileId);
                        }
                    }
                    else if (sSplit[0].Equals("REM"))
                    {
                        Arpayment oArpayment1 = new Arpayment(iPaymentId);
                        Arremittance oArremittance = new Arremittance();
                        oArremittance.ArPaymentId = iPaymentId;
                        oArremittance.RemittanceSequenceNumber = sSplit[1];
                        oArremittance.OverflowSequenceQualifier = sSplit[2];
                        oArremittance.RemittanceQualifierDescription = sSplit[3];
                        oArremittance.RemittanceReference = sSplit[4];
                        oArremittance.RemittanceNetAmountPaid = Utility.PutDecimalAtSpecificPlace(BusinessUtility.GetInt(sSplit[5])).ToString("N2");
                        oArremittance.FileId = m_iFileId;
                        oArremittance.InsertRemittance(oArremittance);

                        if (oArpayment1.CustomerId.Trim().Length <= 0)
                        {
                            tctipd otctipd = new tctipd();
                            oArpayment1.CustomerId = otctipd.GetCustomerId(oArremittance.RemittanceReference);
                            if (oArpayment1.CustomerId.Length > 0)
                            {
                                oArpayment1.UpdateCustomerId(iPaymentId, oArpayment1.CustomerId, m_iFileId);
                            }
                        }
                    }
                    
                }
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLog(ex);
                EdiError.LogError("100", Globalcl.FileId.ToString(), "", ex, "Error in ar FetchData");
            }
        }


        public void StartProcessing()
        {
            try
            {
                bool bStartBlockFound = false;
                bool bEndBlockFound = false;
                bool bRemFound = false;
                bool bRntFound = false;
                string[] sSegment = null;
                int iNoOfblock = 0;
                bool bInEndBlock = false;
                string sText = string.Empty;
                for (int i = 0; i < m_sArr.Length; i++)
                {
                    sText = m_sArr[i];
                    sSegment = m_sArr[i].Split('|');
                    bStartBlockFound = sSegment[0].Equals("PR");
                    bEndBlockFound = sSegment[0].Equals("FT");
                    bRemFound = sSegment[0].Equals("REM");
                    bRntFound = sSegment[0].Equals("RNT");

                    if (sSegment[0].Equals("FH"))
                    {
                        string sDate = sSegment[2].Substring(0,8);
             
                        dtDepositDate = DateTime.ParseExact(sDate, "yyyyMMdd", null, DateTimeStyles.None);
                    }

                    if (!bStartBlockFound)
                    {
                        m_arl.Add(sText);
                    }
                    else if (bStartBlockFound)
                    {
                        m_dict.Add(iNoOfblock, m_arl);
                        iNoOfblock++;
                        m_arl = new ArrayList();
                        m_arl.Add(sText);
                    }
                    if (bEndBlockFound)
                    {
                        bInEndBlock = true;
                        m_dict.Add(iNoOfblock, m_arl);
                        iNoOfblock++;
                        m_arl = new ArrayList();
                        //m_arl.Add(sSegment);
                    }

                }

            } 
            catch (Exception ex)
            {
                ErrorLog.CreateLog(ex);
                EdiError.LogError("100", Globalcl.FileId.ToString(), "", ex, "Error in ar StartProcessing");
            }

        } // start processing end here..    506782


    }
}
