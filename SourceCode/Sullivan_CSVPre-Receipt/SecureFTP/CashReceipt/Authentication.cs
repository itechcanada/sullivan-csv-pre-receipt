﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SecureFTP.GatewayAuthService;
using iTECH.Library;
using SecureFTP.CashReceiptSsession;
using SecureFTP;

/// <summary>
/// Summary description for Authentication
/// </summary>
public class Authentication
{
    Configuration oConfiguration;
    int iSessionIdentity = 0;
	public Authentication()
	{
		//
		// TODO: Add constructor logic here
        //sCompanyId = "STX";
        //sConnectedAccessType = "I";
        //sEnvironmentClass = "LIV";
        //sEnvironmentName = "livitec";
        ////forceDisconnect = true;
        //sUsername = "prabh";
        //sPassword = "password";
		//

        //GatewayAuthService.AuthenticationToken oAuthenticationToken = new GatewayAuthService.AuthenticationToken();
        //oAuthenticationToken = UserAuthentication();
        //if (oAuthenticationToken.value.Length > 10)
        //{


        //    UserAuthenticationLogOut(oAuthenticationToken);
        //}


	}



    // Login to stratix client
    SecureFTP.GatewayAuthService.AuthenticationToken oAuthenticationToken;
    public SecureFTP.GatewayAuthService.AuthenticationToken UserAuthentication()
    {
        SecureFTP.GatewayAuthService.GatewayLoginResponse oGatewayLoginResponse = new SecureFTP.GatewayAuthService.GatewayLoginResponse();
        try
        {
            //oConfiguration = new Configuration(oFiles.VendorDunsNo, BusinessUtility.GetInt(oFiles.EdiFileType));
            oAuthenticationToken = new SecureFTP.GatewayAuthService.AuthenticationToken();

            string sTokenValue = string.Empty;

            SecureFTP.GatewayAuthService.GatewayLoginRequestType oGatewayLoginRequestType = new SecureFTP.GatewayAuthService.GatewayLoginRequestType();

            // Local Credential

            if (Globalcl.FileType == Files.ExecutionFileType.AR)
            {

                Console.WriteLine("Configuration.ApplicationMode = " + Configuration.ApplicationMode);
                if (Configuration.ApplicationMode == Configuration.ApplicationType.QC)
                {
                    ErrorLog.createLog("Ar login start for qc ");
                    oGatewayLoginRequestType.companyId = "USS";
                    oGatewayLoginRequestType.connectedAccessType = "I";
                    oGatewayLoginRequestType.environmentClass = "T01";
                    oGatewayLoginRequestType.environmentName = "tstusstx";
                    oGatewayLoginRequestType.forceDisconnect = true;
                    oGatewayLoginRequestType.forceDisconnectSpecified = true;

                    oGatewayLoginRequestType.username = "Itech";
                    oGatewayLoginRequestType.password = "Pass465798";
                }
                else if (Configuration.ApplicationMode == Configuration.ApplicationType.PRODUCTION)
                {
                    ErrorLog.createLog("Ar login start for live ");
                    oGatewayLoginRequestType.companyId = Utility.GetConfigValue("ArCompanyId");
                    oGatewayLoginRequestType.connectedAccessType = Utility.GetConfigValue("ArConnectedAccessType");
                    oGatewayLoginRequestType.environmentClass = Utility.GetConfigValue("ArEnvironmentClass");
                    oGatewayLoginRequestType.environmentName = Utility.GetConfigValue("ArEnvironmentName");
                    oGatewayLoginRequestType.forceDisconnect = true;
                    oGatewayLoginRequestType.forceDisconnectSpecified = true;

                    oGatewayLoginRequestType.username = Utility.GetConfigValue("ArUsername");
                    oGatewayLoginRequestType.password = Utility.GetConfigValue("ArPassword");

                    ErrorLog.createLog("auth properties set ");
                }

            }
            else
            {
                ErrorLog.createLog("Ar login start for local");
                oGatewayLoginRequestType.companyId = "STX";
                oGatewayLoginRequestType.connectedAccessType = "I";
                oGatewayLoginRequestType.environmentClass = "LIV";
                oGatewayLoginRequestType.environmentName = "livitec";
                oGatewayLoginRequestType.forceDisconnect = true;

                oGatewayLoginRequestType.username = "prabh";
                oGatewayLoginRequestType.password = "password";
            }


            // Live Credential

            //oGatewayLoginRequestType.companyId = "USS";
            //oGatewayLoginRequestType.connectedAccessType = "I";
            //oGatewayLoginRequestType.environmentClass = "T01";
            //oGatewayLoginRequestType.environmentName = "tstusstx";
            //oGatewayLoginRequestType.forceDisconnect = true;

            //oGatewayLoginRequestType.username = "itech2";
            //oGatewayLoginRequestType.password = "itech2";

            SecureFTP.GatewayAuthService.GatewayLoginRequest oGatewayLoginRequest = new SecureFTP.GatewayAuthService.GatewayLoginRequest(oGatewayLoginRequestType);
            SecureFTP.GatewayAuthService.AuthenticationService oAuthenticationService = new SecureFTP.GatewayAuthService.AuthenticationServiceClient();
            oGatewayLoginResponse = oAuthenticationService.GatewayLogin(oGatewayLoginRequest);

            String sTokenUser = oGatewayLoginResponse.response.authenticationToken.username;
            sTokenValue = oGatewayLoginResponse.response.authenticationToken.value;

            oAuthenticationToken = oGatewayLoginResponse.response.authenticationToken;
            // Write log for success
            ErrorLog.createLog("User Login to use service ");
            ErrorLog.createLog("User authentication success " + "\n sTokenUser: " + sTokenUser + "\n sTokenValue: " + sTokenValue + " ");
        }
        catch (Exception MyError)
        {
             ErrorLog.createLog("User authentication error: " + oGatewayLoginResponse.ServiceMessagesHeader.messageList[0].ToString() + "\n" + MyError.Message);
        }
        return oAuthenticationToken;

    }



    // used to logout from stratix client
    public void UserAuthenticationLogOut(SecureFTP.GatewayAuthService.AuthenticationToken oAuthenticationToken)
    {
        try
        {
            SecureFTP.GatewayAuthService.AuthenticationService oAuthenticationService = new SecureFTP.GatewayAuthService.AuthenticationServiceClient();
            SecureFTP.GatewayAuthService.LogoutRequest oLogoutRequest = new SecureFTP.GatewayAuthService.LogoutRequest();
            oLogoutRequest.authenticationToken = oAuthenticationToken;
            SecureFTP.GatewayAuthService.LogoutResponse oLogoutResponse = new SecureFTP.GatewayAuthService.LogoutResponse();
            oLogoutResponse = oAuthenticationService.Logout(oLogoutRequest);
            //Log write for success logout
            ErrorLog.createLog("User LogOut  ");
            ErrorLog.createLog("User authentication success LogOut ");
        }
        catch (Exception MyError)
        {
            ErrorLog.createLog("User authentication LogOut error: " + MyError.Message);
            UserAuthenticationLogOut(oAuthenticationToken);
        }
    }



}