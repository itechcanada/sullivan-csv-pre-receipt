26-04

CREATE TABLE `inbiz_edi`.`arpayment` (
  `ArPaymentId` INT NOT NULL AUTO_INCREMENT,
  `FileId` INT NULL DEFAULT 0 COMMENT 'Reference of file table.',
  `RecordType` VARCHAR(3) NULL DEFAULT 'PR',
  `ItemSequenceNo` INT NULL DEFAULT 0 COMMENT 'PR01\n\nItem sequence number.\nFor DTD and LBX, the ISN value provided is as assigned by\nsource processing system. For all other payment types, ISN is\nincremental.',
  `OdfiRtn` VARCHAR(12) NULL DEFAULT '' COMMENT 'PR02\n\nBank ID for the originating financial institution. This can be an\nABA routing/transit number, SWIFT ID, or CHIPS ID.',
  `ConsumerNumber` VARCHAR(35) NULL DEFAULT '' COMMENT 'PR03\n\nFor ACH820 and ACH835, the ACH company ID.\nFor EBE, the last four digits of the ACH account number, credit\ncard number, or ATM/debit card number.\nFor EBX, the consumer account number.\nFor PPS, the last four digits of the credit card or account\nnumber.\nFor all other payment types,',
  `CreditDebitIndicator` VARCHAR(1) NULL DEFAULT '' COMMENT 'PR04\n\nIndicates whether the payment is a credit (C) or debit (D).',
  `AmountPaid` VARCHAR(14) NULL DEFAULT '' COMMENT 'PR05\n\nTransaction amount.\nFor MCA, this will be zeroes.',
  `GrossAmount` VARCHAR(14) NULL DEFAULT '' COMMENT 'PR06\n\nFor WPG eChecks, this will be the Consumer Amount.\nFor other payment WPG payment methods and EBX, this will be\nthe gross amount.\nFor MCA, this will be zeroes.',
  `PaymentMethod` VARCHAR(3) NULL DEFAULT '' COMMENT 'PR07\n\nIndicates the payment method.\nFor ACH820 and ACH835, this is ACH.\nFor DTD:\nARC � Converted to ACH\nCHK � Processed as a check\nFor EBE:\nEBA � E-Bill Express ACH\nEBC � E-Bill Express credit card\nEBD � E-Bill Express ATM/debit card',
  `PaymentMethodTypeCode` VARCHAR(6) NULL DEFAULT '',
  `SenderPaymentReferenceNumber` VARCHAR(80) NULL DEFAULT '' COMMENT 'PR09\n\nReference ID assigned to the transaction by the originating\nsystem.',
  `ProcessingSystemReferenceNumber` VARCHAR(80) NULL DEFAULT '' COMMENT 'PR10\n\nReference ID assigned to the transaction by an outside\nprocessing system.\n',
  `WellsFargoReferenceNumber` VARCHAR(80) NULL DEFAULT '' COMMENT 'PR11\n\nReference ID assigned to the transaction by Wells Fargo.\n',
  `DepositSettlementDate` VARCHAR(45) NULL DEFAULT '' COMMENT 'PR12\n\nFormat YYYYMMDD\n',
  `PaymentEffectiveDate` VARCHAR(45) NULL DEFAULT '' COMMENT 'PR13\n\nEffective date of the payment in format YYYYMMDD.',
  `TransactionProcessingDateTime` VARCHAR(45) NULL DEFAULT '' COMMENT 'PR14\n\nFormat YYYYMMDDhhmmssss with hh expressed in 24-hour\nformat.',
  `Reference1` VARCHAR(45) NULL DEFAULT '' COMMENT 'PR21\n\nDescription of the reference information in PR22A or PR22B in\nformat:\nThree-character qualifier, dash, qualifier description\n',
  `NoOfRemittance` VARCHAR(45) NULL DEFAULT '',
  PRIMARY KEY (`ArPaymentId`))
COMMENT = 'Holds account recevable payment.';



CREATE TABLE `inbiz_edi`.`arremittance` (
  `ArremittanceId` INT NOT NULL AUTO_INCREMENT,
  `ArPaymentId` INT NULL DEFAULT 0,
  `FileId` INT NULL DEFAULT 0,
  `RecordTypeIndicator` VARCHAR(3) NULL DEFAULT 'REM',
  `RemittanceSequenceNumber` VARCHAR(6) NULL DEFAULT '',
  `OverflowSequenceQualifier` VARCHAR(1) NULL DEFAULT '' COMMENT 'REM01B\n\nthis value can be 0 or 9 to indicate how\nmany additional remittance records follow this record:\n\n0 : One or more additional remittance records follow\n9 : No additional remittance records follow',
  `RemittanceQualifierDescription` VARCHAR(35) NULL DEFAULT '',
  `RemittanceReference` VARCHAR(50) NULL DEFAULT '' COMMENT 'REM03\n\nReference information as specified by REM02.',
  `RemittanceNetAmountPaid` VARCHAR(45) NULL DEFAULT '' COMMENT 'REM04\n\nNet payment amount being applied.\nFor retail LBX, this is the coupon applied amount.\nFor wholesale LBX, this is the net invoice amount.\n',
  `RemittanceGrossAmount` VARCHAR(45) NULL DEFAULT '' COMMENT 'REM05\n\nGross invoice amount (including charges, less allowances)\nbefore terms discount (if discount is applicable).',
  `RemittanceDiscountAmount` VARCHAR(45) NULL DEFAULT '' COMMENT 'REM06\n\nDiscount or adjustment amount.',
  `RemittanceAmountDue` VARCHAR(45) NULL DEFAULT '' COMMENT 'REM07\n\nAmount due.',
  `RemittanceLateAmount` VARCHAR(45) NULL COMMENT 'REM08\n\nLate amount, if applicable.',
  `RemittanceDate` VARCHAR(45) NULL DEFAULT '' COMMENT 'REM09\n\nDate of the remittance in format YYMMDD.\nFor retail LBX, this is the coupon date.\nFor wholesale LBX, this is invoice date.\n',
  `Active` BIT(1) NULL DEFAULT 1,
  `CreatedOn` DATETIME NULL DEFAULT Now(),
  PRIMARY KEY (`ArremittanceId`))
COMMENT = 'Holds payments segments..';








ALTER TABLE `inbiz_edi`.`files` 
CHANGE COLUMN `FileType` `FileType` VARCHAR(15) NULL DEFAULT '' ;




ALTER TABLE `inbiz_edi`.`arpayment` 
ADD COLUMN `Active` BIT(1) NULL DEFAULT 1 AFTER `NoOfRemittance`,
ADD COLUMN `CreatedOn` DATETIME NULL DEFAULT now() AFTER `Active`;



ALTER TABLE `inbiz_edi`.`arpayment` 
ADD COLUMN `CustomerId` VARCHAR(45) NULL DEFAULT '' AFTER `NoOfRemittance`;




ALTER TABLE `inbiz_edi`.`arpayment` 
ADD COLUMN `CashRctSessionIdentity` VARCHAR(100) NULL DEFAULT '' AFTER `CustomerId`,
ADD COLUMN `CashRctIdentity` VARCHAR(100) NULL DEFAULT '' AFTER `CashRctSessionIdentity`;










