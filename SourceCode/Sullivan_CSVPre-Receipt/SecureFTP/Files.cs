﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using iTECH.Library.DataAccess.MySql;
using iTECH.Library.Utilities;
using System.Data;
using MySql.Data.MySqlClient;
using System.Configuration;

namespace SecureFTP
{
    public class Files
    {

        string m_sSql = string.Empty;

        static string sStratixConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ToString();

        #region to check vendors -- to process only selected vendorlist without checking active flag   2020/03/16    Sumit
        string IsVendorFilter = Utility.GetConfigValue("IsVendorFilter");
        string FilteredVendorConfig = Utility.GetConfigValue("VendorDunsList");
        string[] VendorArr;
        string FilteredVendorDB = string.Empty;
        #endregion to check vendors -- to process only selected vendorlist   2020/03/16 Sumit

        int iFileID = 0;
        public int FileID
        {
            get { return iFileID; }
            set { iFileID = value; }
        }

        string sVendorId = "";
        public string VendorId
        {
            get { return sVendorId; }
            set { sVendorId = value; }
        }

        int iFileType = 0;
        public int FileType
        {
            get { return iFileType; }
            set { iFileType = value; }
        }

        int iReceiptNo = 0;
        public int ReceiptNo
        {
            get { return iReceiptNo; }
            set { iReceiptNo = value; }
        }

        string sFileName = string.Empty;
        public string FileName
        {
            get { return sFileName; }
            set { sFileName = value; }
        }

        string sEdiFileType = string.Empty;
        public string EdiFileType
        {
            get { return sEdiFileType; }
            set { sEdiFileType = value; }
        }

        string sVendorID = string.Empty;
        public string VendorID
        {
            get { return sVendorID; }
            set { sVendorID = value; }
        }

        string sVendorName = string.Empty;
        public string VendorName
        {
            get { return sVendorName; }
            set { sVendorName = value; }
        }

        string sIntchgNo = string.Empty;
        public string IntchgNo
        {
            get { return sIntchgNo; }
            set { sIntchgNo = value; }
        }

        string sMill = string.Empty;
        public string Mill
        {
            get { return sMill; }
            set { sMill = value; }
        }

        string sCustomerName = string.Empty;
        public string CustomerName
        {
            get { return sCustomerName; }
            set { sCustomerName = value; }
        }

        int iHeaderId = 0;
        public int HeaderId
        {
            get { return iHeaderId; }
            set { iHeaderId = value; }
        }

        int iDetailId = 0;
        public int DetailId
        {
            get { return iDetailId; }
            set { iDetailId = value; }
        }


        string sVendorDunsNo = string.Empty;
        public string VendorDunsNo
        {
            get { return sVendorDunsNo; }
            set { sVendorDunsNo = value; }
        }

        int iFileReceivedId = 0;
        public int FileReceivedId
        {
            get { return iFileReceivedId; }
            set { iFileReceivedId = value; }
        }

        
        string sFileText = string.Empty;
        public string FileText
        {
            get { return sFileText; }
            set { sFileText = value; }
        }


        string sBsn = string.Empty;
        public string Bsn
        {
            get { return sBsn; }
            set { sBsn = value; }
        }

        string sPo = string.Empty;
        public string Po
        {
            get { return sPo; }
            set { sPo = value; }
        }

        string sUniqueFileName = string.Empty;
        public string UniqueFileName
        {
            get { return sUniqueFileName; }
            set { sUniqueFileName = value; }
        }
        


        string sCertFileToUpload = string.Empty;
        public string CertFileToUpload
        {
            get { return sCertFileToUpload; }
            set { sCertFileToUpload = value; }
        }

        public struct ExecutionFileType
        {
            public const string EDI856 = "856";
            public const string EDI863 = "863";
            public const string AR = "100";
            public const string EDI8561 = "8561"; // customer own pre receipt
            public const string EDI856863 = "856863"; // get 856 863 from single sc st
        }

            
        public Files()
        {

        }



        public Files(int iFileId)
        {
            DbHelper dbHelp = new DbHelper(true);  
            m_sSql = "select * from files where FileId = " +iFileId;
            MySqlParameter[] oMySqlParameter = {  
                                         DbUtility.GetParameter("FileId", iFileId, MyDbType.Int)                                                                
                                     };
            DataTable oDtAddress = dbHelp.GetDataTable(m_sSql, CommandType.Text, oMySqlParameter);
            foreach (DataRow oDataRow in oDtAddress.Rows)
            {
                FileID = BusinessUtility.GetInt(oDataRow["FileId"]);
                sFileName = BusinessUtility.GetString(oDataRow["FileName"]);
                FileType = BusinessUtility.GetInt(oDataRow["FileType"]);
                sEdiFileType = BusinessUtility.GetString(oDataRow["FileType"]);
                sVendorID = BusinessUtility.GetString(oDataRow["VendorID"]);
                sVendorName = BusinessUtility.GetString(oDataRow["VendorName"]);
                sIntchgNo = BusinessUtility.GetString(oDataRow["IntchgNo"]);
                sMill = BusinessUtility.GetString(oDataRow["Mill"]);    
                sCustomerName = BusinessUtility.GetString(oDataRow["CustomerName"]);
                VendorId = BusinessUtility.GetString(sVendorID);
                iReceiptNo = BusinessUtility.GetInt(oDataRow["ReceiptNo"]);
                sVendorDunsNo = BusinessUtility.GetString(oDataRow["VendorDunsNo"]);
                iFileReceivedId = BusinessUtility.GetInt(oDataRow["FileReceivedId"]);
                CertFileToUpload = BusinessUtility.GetString(oDataRow["CertFileToUpload"]);
            }

        }

        public Files(string sFileName)
        {
            DbHelper dbHelp = new DbHelper(true);
            //m_sSql = "select * from files where UniqueFileName = " + sFileName;   //error input string was not in correct format  2020/03/14  Sumit
            //MySqlParameter[] oMySqlParameter = {
            //                             DbUtility.GetParameter("UniqueFileName", sFileName, MyDbType.Int)
            //                         };
            m_sSql = "select * from files where UniqueFileName = @UniqueFileName";
            MySqlParameter[] oMySqlParameter = {
                                         DbUtility.GetParameter("UniqueFileName", sFileName, MyDbType.String)
                                     };
            DataTable oDtAddress = dbHelp.GetDataTable(m_sSql, CommandType.Text, oMySqlParameter);
            foreach (DataRow oDataRow in oDtAddress.Rows)
            {
                //assign file id and filetype properties value becasue for bulk processing these function calling but not getting these fields
                FileID = BusinessUtility.GetInt(oDataRow["FileId"]);        
                sFileName = BusinessUtility.GetString(oDataRow["FileName"]);
                FileType = BusinessUtility.GetInt(oDataRow["FileType"]);
                sEdiFileType = BusinessUtility.GetString(oDataRow["FileType"]);
                sVendorID = BusinessUtility.GetString(oDataRow["VendorID"]);
                sVendorName = BusinessUtility.GetString(oDataRow["VendorName"]);
                sIntchgNo = BusinessUtility.GetString(oDataRow["IntchgNo"]);
                sMill = BusinessUtility.GetString(oDataRow["Mill"]);
                sCustomerName = BusinessUtility.GetString(oDataRow["CustomerName"]);
                VendorId = BusinessUtility.GetString(sVendorID);
                iReceiptNo = BusinessUtility.GetInt(oDataRow["ReceiptNo"]);
                sVendorDunsNo = BusinessUtility.GetString(oDataRow["VendorDunsNo"]);
                iFileReceivedId = BusinessUtility.GetInt(oDataRow["FileReceivedId"]);
                CertFileToUpload = BusinessUtility.GetString(oDataRow["CertFileToUpload"]);
                UniqueFileName = BusinessUtility.GetString(oDataRow["UniqueFileName"]);
            }

        }


        public int CheckFileExists(string sFileName)
        {
            int iCount = 0;
            DbHelper dbHelp = new DbHelper(true);
            try
            {
                m_sSql = "select 1 as count from files where FileName =@FileName";
                MySqlParameter[] oMySqlParameter = {  
                                         DbUtility.GetParameter("FileName", sFileName, MyDbType.String)                                                                
                                     };
                object obj = dbHelp.GetValue(m_sSql, CommandType.Text, oMySqlParameter);
                iCount = BusinessUtility.GetInt(obj);
            }
            catch(Exception ex)
            {
                ErrorLog.createLog("Error in CheckFileExists");
                ErrorLog.createLog(ex.ToString());
                ErrorLog.CreateLog(ex);
            }
            return iCount;
        }




        public int insertFileStatus(FTPTraking objRecord)
        {
            DbHelper dbHelp = new DbHelper(true);           
            try
            {
                StringBuilder sqlString = new StringBuilder();
                sqlString.Append("  insert into files (FileName, FilePath, Status, ReceivedOn,ProcessedOn, FileType, VendorID, ReceiptNo, VendorName, Mill, CustomerName, VendorDunsNo, FileReceivedId, FileText, CertFileToUpload, UniqueFileName)  ");
                sqlString.Append("  values (@FileName, @FilePath, @Status, now(), now(), '" + iFileType + "', '" + VendorId + "', '" + iReceiptNo + "', '" + sVendorName + "', '" + sMill + "', '" + sCustomerName + "', '" + sVendorDunsNo + "' , " + iFileReceivedId + ", '" + sFileText + "', '" + CertFileToUpload + "', '" + UniqueFileName + "' );");
                MySqlParameter[] p = {  
                                         DbUtility.GetParameter("FileName", objRecord.ftpFileName, MyDbType.String),
                                         DbUtility.GetParameter("FilePath", objRecord.ftpFilePath, MyDbType.String),
                                         DbUtility.GetParameter("Status", objRecord.Status, MyDbType.Int)                                                                    
                                     };
                if (dbHelp.ExecuteNonQuery(Convert.ToString(sqlString), CommandType.Text, p) > 0)
                {
                    return dbHelp.GetLastInsertID();
                }
                else 
                {
                    return 0;
                }               
            }
            catch( Exception ex) 
            {
                ErrorLog.CreateLog(ex);
                throw ex;
            }
            finally { 
                
                dbHelp.CloseDatabaseConnection();
            
            } 
        }


        public void InsertFileDetail(int iFileId, string sBsnNo, string sPo, int iHeaderId, int iDetailId)
        {
            DbHelper dbHelp = new DbHelper(true);
            try
            {
                StringBuilder sqlString = new StringBuilder();
                sqlString.Append("  INSERT INTO filedetail (FileId,Bsn,Po,HeaderId,DetailId) ");
                sqlString.Append("  VALUES(@FileId, @Bsn, @Po, @HeaderId,@DetailId); ");
                MySqlParameter[] p = {  
                                         DbUtility.GetParameter("@FileId", iFileId, MyDbType.Int),
                                         DbUtility.GetParameter("@Bsn", sBsnNo, MyDbType.String),
                                         DbUtility.GetParameter("@Po", sPo, MyDbType.String),
                                         DbUtility.GetParameter("@HeaderId", iHeaderId, MyDbType.Int),
                                         DbUtility.GetParameter("@DetailId", iDetailId, MyDbType.Int)
                                     };
                dbHelp.ExecuteNonQuery(Convert.ToString(sqlString), CommandType.Text, p);
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLog(ex);
                //throw ex;
            }
            finally
            {

                dbHelp.CloseDatabaseConnection();

            }
        }

        public bool CheckBsnExists(int iFileId, string sBsn)
        {
            bool bExists = false;
            DbHelper dbHelp = new DbHelper(true);
            try
            {
                m_sSql = "select * from filedetail where fileid = "+iFileId+" and bsn= '"+sBsn+"' and active = 1";
                //MySqlParameter[] oMySqlParameter = { };
                DataTable dtBsn = dbHelp.GetDataTable(m_sSql, CommandType.Text,  null);
                bExists = dtBsn.Rows.Count > 0;

                foreach (DataRow oDataRow in dtBsn.Rows)
                {
                    iHeaderId = BusinessUtility.GetInt(oDataRow["HeaderId"]);
                    iDetailId = BusinessUtility.GetInt(oDataRow["DetailId"]);
                }
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLog(ex);
                EdiError.LogError(sEdiFileType, iFileId.ToString(), sFileName, ex, "Error in UpdateReceipt of files class");
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
            return bExists;
        }



        public bool CheckPoExists(int iFileId, string sPo)
        {
            bool bExists = false;
            DbHelper dbHelp = new DbHelper(true);
            try
            {
                m_sSql = "select * from filedetail where fileid = " + iFileId + " and Po= '" + sPo + "' and active = 1";
                MySqlParameter[] oMySqlParameter = { };
                DataTable dtPo = dbHelp.GetDataTable(m_sSql, CommandType.Text, oMySqlParameter);
                bExists = dtPo.Rows.Count > 0;
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLog(ex);
                EdiError.LogError(sEdiFileType, iFileId.ToString(), sFileName, ex, "Error in UpdateReceipt of files class");
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
            return bExists;
        }

        public bool CheckFilePoBsnExists(int iFileId, string sPo, string sBsn)
        {
            bool bExists = false;
            DbHelper dbHelp = new DbHelper(true);
            try
            {
                m_sSql = "select * from filedetail where fileid = " + iFileId + " and Po= '" + sPo + "' and bsn= '" + sBsn + "' and active = 1";
                MySqlParameter[] oMySqlParameter = { };
                DataTable dtPo = dbHelp.GetDataTable(m_sSql, CommandType.Text, oMySqlParameter);
                bExists = dtPo.Rows.Count > 0;
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLog(ex);
                EdiError.LogError(sEdiFileType, iFileId.ToString(), sFileName, ex, "Error in UpdateReceipt of files class");
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
            return bExists;
        }


        public int GetBsnCount(int iBsn, string sPo)
        {
            int iCnt = 0;
            DbHelper dbHelp = new DbHelper(true);
            try
            {
                m_sSql = "select * from filedetail where bsn = " + iBsn + "  and active = 1";
                MySqlParameter[] oMySqlParameter = { };
                DataTable dtPo = dbHelp.GetDataTable(m_sSql, CommandType.Text, oMySqlParameter);
                iCnt = dtPo.Rows.Count;
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLog(ex);
                //EdiError.LogError(sEdiFileType, iFileId.ToString(), sFileName, ex, "Error in UpdateReceipt of files class");
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
            return iCnt;
        }

        public int GetPoBsnCount(int iBsn, string sPo)
        {
            int iCnt = 0;
            DbHelper dbHelp = new DbHelper(true);
            try
            {
                m_sSql = "select * from filedetail where bsn = " + iBsn + " and bsn= '" + sBsn + "' and active = 1";
                MySqlParameter[] oMySqlParameter = { };
                DataTable dtPo = dbHelp.GetDataTable(m_sSql, CommandType.Text, oMySqlParameter);
                iCnt = dtPo.Rows.Count;
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLog(ex);
                //EdiError.LogError(sEdiFileType, iFileId.ToString(), sFileName, ex, "Error in UpdateReceipt of files class");
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
            return iCnt;
        }

         public int HeaderCount(int iHeader)
        {
            int iCnt = 0;
            DbHelper dbHelp = new DbHelper(true);
            try
            {
                //m_sSql = "select * from filedetail where bsn = " + iBsn + " and bsn= '" + sBsn + "' and active = 1";
                m_sSql = " select b.headerid, count(b.headerid) from files a  inner join header b on a.fileid = b.fileid  inner join detail c on b.HeaderId = c.headerid where b.headerid = " + iHeader + " GROUP BY b.headerid ";
                MySqlParameter[] oMySqlParameter = { };
                var obj = dbHelp.GetValue(m_sSql, CommandType.Text, oMySqlParameter);
                iCnt = BusinessUtility.GetInt(obj);
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLog(ex);
                //EdiError.LogError(sEdiFileType, iFileId.ToString(), sFileName, ex, "Error in UpdateReceipt of files class");
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
            return iCnt;
        }


        

        public void UpdatePoBsnForCurrentFile(int iFileId)
        {
            DbHelper dbHelp = new DbHelper(true);
            try
            {
                m_sSql = "update filedetail set Active = 0 where FileId = " + iFileId;
                MySqlParameter[] oMySqlParameter = { };
                dbHelp.ExecuteNonQuery(m_sSql, CommandType.Text, oMySqlParameter);
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLog(ex);
                EdiError.LogError(sEdiFileType, iFileId.ToString(), sFileName, ex, "Error in UpdateReceipt of files class");
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
        }


        public void UpdateReceipt()
        {
            DbHelper dbHelp = new DbHelper(true);
            int iFileId = 0;
            int iReceiptNo = 0;
            string iPoNumber = string.Empty;
            int iPoItem = 0;
            try
            {
                m_sSql = "select * from detail where  ReceiptNo = 0 AND IsReceiptUpdated = 0";
                MySqlParameter[] oMySqlParameter = { };
                DataTable dtReceiptList = dbHelp.GetDataTable(m_sSql, CommandType.Text, oMySqlParameter);
                foreach (DataRow oDataRow in dtReceiptList.Rows)
                {
                    sIntchgNo = BusinessUtility.GetString(oDataRow["IntchgNo"]);
                    iHeaderId = BusinessUtility.GetInt(oDataRow["HeaderId"]);
                    //adding po number and po item for mail Notification     2020/05/04  Sumit
                    iPoNumber = BusinessUtility.GetString(oDataRow["PrntNo"]);
                    iPoItem = BusinessUtility.GetInt(oDataRow["PrntItm"]);
                    //iFileId = BusinessUtility.GetInt(oDataRow["FileId"]);
                    //sEdiFileType = BusinessUtility.GetString(oDataRow["FileType"]);
                    //sFileName = BusinessUtility.GetString(oDataRow["FileName"]);

                    if ((!string.IsNullOrEmpty(sIntchgNo)) && (iHeaderId > 0))
                    {
                        iReceiptNo = this.GetReceiptNumber(sIntchgNo);

                        Console.WriteLine("Updating header id : " + iHeaderId + " With Receipt no : " + iReceiptNo);
                        m_sSql = "update detail set ReceiptNo = '" + iReceiptNo + "' , IsReceiptUpdated = 1 where HeaderId = '" + iHeaderId + "'";
                        dbHelp.ExecuteNonQuery(m_sSql, CommandType.Text, oMySqlParameter);

                        if (iReceiptNo >0)
                        {
                            //adding mail notification code     2020/04/24  Sumit
                            if (BusinessUtility.GetInt(Utility.GetConfigValue("isPRNotification")) == 1)
                            {
                                MailNotify.MailNotifyManager mailNotifyManager = new MailNotify.MailNotifyManager();
                                mailNotifyManager.poNumber = iPoNumber;
                                mailNotifyManager.poItem = iPoItem;
                                mailNotifyManager.NotificationStatus = "OnSuccess";
                                //mailNotifyManager.prNumber = Convert.ToString(iReceiptNo);
                                mailNotifyManager.NotificationMessage = "PR Number: " + iReceiptNo;

                                mailNotifyManager.SendNotification();
                            }
                        }
                        //  check and update error description of interchange number     2021/03/23  Sumit
                        if (iReceiptNo == 0)
                        {
                            if (BusinessUtility.GetInt(Utility.GetConfigValue("GetPRErrorInformix")) == 1)
                                UpdatePRError(sIntchgNo, Convert.ToString(iHeaderId));
                        }

                    }

                }

            }
            catch (Exception ex)
            {
                ErrorLog.CreateLog(ex);
                EdiError.LogError(sEdiFileType, iFileId.ToString(), sFileName, ex, "Error in UpdateReceipt of files class");
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
        }




        public int GetReceiptNumber(string IntchgNo)
        {
            iTECH.Library.DataAccess.ODBC.DbHelper oDbHelper = new iTECH.Library.DataAccess.ODBC.DbHelper(sStratixConnectionString, true);
            int iReceiptNo = 0;
            tctipd tctipd = new tctipd();
            try
            {
                m_sSql = "select i53_new_rcpt_no from XCTI53_rec where i53_intchg_no =" + IntchgNo;
                ErrorLog.createLog(m_sSql);
                DataTable dtReceipt = new DataTable();
                if (BusinessUtility.GetInt(Utility.GetConfigValue("InformixRead")) == 1)        // use informix read function   2021/03/23  Sumit
                    dtReceipt = tctipd.GetInfomixRecord(m_sSql.ToString());
                else
                    dtReceipt = oDbHelper.GetDataTable(m_sSql, CommandType.Text, null);

                if (dtReceipt.Rows.Count > 0)
                {
                    iReceiptNo = BusinessUtility.GetInt(dtReceipt.Rows[0]["i53_new_rcpt_no"]);
                }                    
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLog(ex);
            }
            finally
            {
                oDbHelper.CloseDatabaseConnection();
            }

            return iReceiptNo;
        }


        public void FailedVendorTag(int iFileId, string sVendorId, string sVenTagId, string sPo, string sHeat, string sWeight, string sComment)
        {
            m_sSql = "INSERT INTO skippedvendortagid (FileId,VendorID,VendorTagID,PoNo,Heat,Weight, Comment)VALUES(" + iFileId + ", '" + sVendorId + "', '" + sVenTagId + "', '" + sPo + "', '" + sHeat + "', '" + sWeight + "', '" + sComment + "')";
            iTECH.Library.DataAccess.MySql.DbHelper oDbHelper = new iTECH.Library.DataAccess.MySql.DbHelper();

            try
            {
                oDbHelper.ExecuteNonQuery(m_sSql, CommandType.Text, null);
            }
            catch (Exception ex)
            {
                ErrorLog.createLog(ex.ToString());
                EdiError.LogError("", iFileId.ToString(), "", ex, "");
                ErrorLog.CreateLog(ex);
            }
            finally
            {
                oDbHelper.CloseDatabaseConnection();
            }
        }


        public int CheckQdsExists(string sMill, string sHeat, string sEdiFileIdentity)
        {
            int iHeatQds = 0;
            m_sSql = "select b.QdsNo from header863 a join files b on a.Fileid = b.Fileid and b.QdsNo <> ''''  WHERE a.mill = '" + sMill + "' AND a.heat = '" + sHeat + "' AND a.ShipingRef = '" + sEdiFileIdentity + "' ";
            iTECH.Library.DataAccess.MySql.DbHelper oDbHelper = new iTECH.Library.DataAccess.MySql.DbHelper();

            try
            {
                object obj = oDbHelper.GetValue(m_sSql, CommandType.Text, null);
                iHeatQds = BusinessUtility.GetInt(obj);
                

            }
            catch (Exception ex)
            {
                ErrorLog.createLog(ex.ToString());
                ErrorLog.CreateLog(ex);
                EdiError.LogError(Globalcl.FileType, Globalcl.FileId.ToString(), "", ex, "");
            }
            finally
            {
                oDbHelper.CloseDatabaseConnection();
            }
            return iHeatQds;
        }


        public DataTable GetUnprocessedFile()
        {
            DataTable dt = null;
            DbHelper dbHelp = new DbHelper(true);
            try
            {
                m_sSql = "select * from files where Processed = 0";

                #region to process only selected vendorlist without checking active flag    2020/03/16  Sumit
                if (!string.IsNullOrEmpty(IsVendorFilter) && BusinessUtility.GetInt(IsVendorFilter) == 1)
                {
                    if (!string.IsNullOrEmpty(FilteredVendorConfig))
                    {
                        VendorArr = FilteredVendorConfig.Split(',');
                        FilteredVendorDB = "";
                        for (int i = 0; i < VendorArr.Length; i++)
                        {
                            if (i == 0)
                                FilteredVendorDB = "'" + Convert.ToString(VendorArr[i]) + "'";
                            else
                                FilteredVendorDB = FilteredVendorDB + "," + "'" + Convert.ToString(VendorArr[i]) + "'";
                        }
                        m_sSql = "SELECT * FROM files WHERE VendorDunsNo in (" + FilteredVendorDB + ") and Processed = 0";
                    }

                }
                #endregion to process only selected vendorlist without checking active flag    2020/03/16  Sumit   

                MySqlParameter[] oMySqlParameter = { };
                dt = dbHelp.GetDataTable(m_sSql, CommandType.Text, oMySqlParameter);
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLog(ex);
                EdiError.LogError(sEdiFileType, Globalcl.FileId.ToString(), sFileName, ex, "Error in GetUnprocessedFile of files class");
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
            return dt;
        }

        public void InactiveFile(int iFileid)
        {
            DbHelper dbHelp = new DbHelper(true);
            try
            {
                ErrorLog.createLog("Updating file to processed..");
                MySqlParameter[] oMySqlParameter = { };
                m_sSql = "update files set processed = 1 where FileId = " + iFileid;

                ErrorLog.createLog("m_sSql = " + m_sSql);
                dbHelp.ExecuteNonQuery(m_sSql, CommandType.Text, oMySqlParameter);
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLog(ex);
                //EdiError.LogError(sEdiFileType, iFileId.ToString(), sFileName, ex, "Error in UpdateReceipt of files class");
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
        }

        public static string GetUniqueFileName()
        {
            string sReturn = string.Empty;
            try
            {
               sReturn = Utility.GetGUID() + DateTime.Now.Ticks + ".txt";
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLog(ex);
            }
            return sReturn;
        }

        /// <summary>
        /// to update error for PR interchange number   2021/03/23  Sumit
        /// </summary>
        /// <param name="IntchgNo"></param>
        /// <param name="HId"></param>
        /// <returns></returns>
        public void UpdatePRError(string IntchgNo, string HId)
        {
            DbHelper dbHelp = new DbHelper(true);
            string PRError = string.Empty;
            try
            {
                MySqlParameter[] oMySqlParameter = { };
                Console.WriteLine("Fetching error for InterChangeNo : " + IntchgNo + " Header ID : " + HId);
                tctipd tctipd = new tctipd();
                PRError = tctipd.GetPRErrorCodeDescription(IntchgNo);
                if (!string.IsNullOrEmpty(PRError))
                {
                    Console.WriteLine("Updating error for InterChangeNo : " + IntchgNo + " Header ID : " + HId);

                    m_sSql = "update detail set PRError = '" + PRError + "' , IsReceiptUpdated = 1 where IntchgNo = '" + IntchgNo + "' and HeaderId = '" + HId + "'";
                    dbHelp.ExecuteNonQuery(m_sSql, CommandType.Text, oMySqlParameter);
                }
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLog(ex);
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
        }

    }
    public class FTPTraking
    {
        public  string ftpFileName{ get; set;}
        public string ftpFilePath { get;set;}
        public int Status {get; set;}  
    }
     
}
