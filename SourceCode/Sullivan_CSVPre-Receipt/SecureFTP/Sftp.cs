﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Configuration;
using Tamir.SharpSsh.jsch;
using System.Collections;
using Tamir.SharpSsh;
using iTECH.Library.Utilities;
using System.Net.Mail;
using System.IO;

namespace SecureFTP
{       
    public class Sftp
    {
        private bool connected; // default not connected
        private Session session = null;
        private ChannelSftp channelSftp = null;
        //private SftpProgressMonitor monitor = null;
        //private int bytesTransferred = 0;

        private  String host = ConfigurationManager.AppSettings["SFTP_HOST"] == null ? "" : Convert.ToString(ConfigurationManager.AppSettings["SFTP_HOST"]);
        private  int port = ConfigurationManager.AppSettings["SFTP_PORT"] == null ? 22 : Convert.ToInt32(ConfigurationManager.AppSettings["SFTP_PORT"]);
        private  String username = ConfigurationManager.AppSettings["SFTP_USER_NAME"] == null ? "" : Convert.ToString(ConfigurationManager.AppSettings["SFTP_USER_NAME"]);
        private  String password = ConfigurationManager.AppSettings["SFTP_PASSWORD"] == null ? "" : Convert.ToString(ConfigurationManager.AppSettings["SFTP_PASSWORD"]);
        private  String destFolder = ConfigurationManager.AppSettings["SFTP_TARGET_DIR"] == null ? "/home" : Convert.ToString(ConfigurationManager.AppSettings["SFTP_TARGET_DIR"]);

        private  string SFTP_PROCESSED = ConfigurationManager.AppSettings["SFTP_PROCESSED"];
        private  string SFTP_UNPROCESSED = ConfigurationManager.AppSettings["SFTP_UNPROCESSED"];

        private  string Processed_PATH = ConfigurationManager.AppSettings["Processed_PATH"];
        private  string Upload_PATH = ConfigurationManager.AppSettings["Upload_PATH"];
        private  string Unprocessed_PATH = ConfigurationManager.AppSettings["Unprocessed_PATH"];

        private  string sFirstLineText = ConfigurationManager.AppSettings["FirstLineText"];
        private  string sLastLineText = ConfigurationManager.AppSettings["LastLineText"];

        private static string SFTP_ACKDIR = ConfigurationManager.AppSettings["SFTP_ACKDIR"];
        private static string ACK_DIR = ConfigurationManager.AppSettings["ACK_DIR"];


        ArrayList arlFileNames = new ArrayList();
        public ArrayList fileNames
        {
            get { return arlFileNames; }
            set { arlFileNames = value; }
        }

        static int m_iFileId = 0;

        public Sftp()
        {
        }

        public Sftp(int iEdiDataSourceId)
        {
            EdiDataSource oEdiDataSource = new EdiDataSource(iEdiDataSourceId);
            host = oEdiDataSource.Host;
            port = BusinessUtility.GetInt(oEdiDataSource.Port);
            username = oEdiDataSource.UserId;
            password = oEdiDataSource.Password;
            destFolder = oEdiDataSource.DataSourceDirectory;

            Processed_PATH = oEdiDataSource.DataProcessedDirectory;
            Upload_PATH = oEdiDataSource.DataUploadDirectory;
            Unprocessed_PATH = oEdiDataSource.DataUnProcessedDirectory;

            ErrorLog.createLog("Sftp constructor. : " + host + " : " + port);
        }
        
        public void Connect()
        {
            Connect(host, username, password);
        }
        
        public void Connect(String server, String user, String password)
        {
            if (!connected)
            {
                JSch jsch = new JSch();
                try
                {
                    //string sCErtificateDirectory = Path.Combine(Environment.CurrentDirectory, "Majestic.pub");
                    //string str = "D:\\Mukesh\\WorkingProject\\Majestic\\SecureFTP\\bin\\Debug\\Majestic.pub";
                    //jsch.addIdentity(sCErtificateDirectory);

                    session = jsch.getSession(user, server, port);

                    Hashtable ht = new Hashtable();
                    ht.Add("StrictHostKeyChecking", "no");
                     
                    session.setConfig(ht);
                    session.setPassword(password);

                    session.connect();
                    Channel channel = session.openChannel("sftp");
                    channel.connect();
                    channelSftp = (ChannelSftp)channel;
                    connected = true;
                     


                    Console.WriteLine("connected");
                }
                catch (Exception e)
                {
                    ErrorLog.CreateLog(e);
                    throw e;
                }
            }
        }

        public void Disconnect()
        {
            if (connected)
            {
                connected = false;
               // channelSftp.quit();
              //  channelSftp.disconnect();
                session.disconnect();
                
            }
        }

        public bool IsConnected()
        {
            return connected;
        }

        public void PutFile(String filePath, String destinationPath, SftpProgressMonitor monitor)
        {
            //bytesTransferred = 0;
            int mode = ChannelSftp.OVERWRITE;
            try
            {
                if (monitor != null)
                {
                    channelSftp.put(filePath, destinationPath, monitor, mode);
                }
                else
                {
                    channelSftp.put(filePath, destinationPath, mode);
                }
            }
            catch (SftpException e)
            {
                throw e;
            }
        }

        public void GetFile(String filePath, String destinationPath, SftpProgressMonitor monitor)
        {
            int mode = ChannelSftp.OVERWRITE;
            try
            {
                if (monitor != null)
                {
                    channelSftp.get(filePath, destinationPath, monitor, mode);
                }
                else
                {
                    channelSftp.get(filePath, destinationPath);
                    channelSftp.rm(filePath);
                }
            }
            catch (SftpException e)
            {
                throw e;
            }
        }

        public void GetAllFiles(String sourcePath, String destinationPath, SftpProgressMonitor monitor)
        {
            int mode = ChannelSftp.OVERWRITE;
            String[] folders;
            String folderName;
            try
            {
                // Gets all files of current dir and process one by one
                fileNames = GetFileList(sourcePath);
                foreach (String _file in fileNames)
                {
                    if (_file == "." || _file == ".." || _file == ".cache" || _file == "processed" || _file == "unprocessed" || _file == "997-ACK")
                        continue;

                    if (monitor != null)
                    {
                        channelSftp.get(_file, destinationPath, monitor, mode);
                        
                    }
                    else
                    {
                        try
                        {
                            channelSftp.get(sourcePath + "/" + _file, destinationPath);
                            //channelSftp.get(sourcePath + "856_244548.txt", destinationPath);
                            // check if file is valid
                            var fileLines = System.IO.File.ReadAllLines(BusinessUtility.GetString(destinationPath + "/" + _file));
                           
                            EdiParser oEdiParser = new EdiParser();
                            if (0==0) // changed logic for this customer
                            {
                                //channelSftp.put(SFTP_PROCESSED + "/" + _file);

                                try
                                {
                                    #region ACKNOWLEDGE LOGIC 
                                    // ACK LOGIC  START

                                    string sFileName = string.Empty;
                                    Configuration oConfiguration = new Configuration();
                                    oConfiguration.PopulateConfigurationData(fileLines);
                                    ErrorLog.createLog("(oConfiguration.Active997 = " + oConfiguration.Active997.ToString());
                                    if (oConfiguration.Active997)
                                    {
                                        ErrorLog.createLog("ACK 997 IS ENABLE");
                                        string sAckText = oConfiguration.AckText;
                                        ErrorLog.createLog("sAckText = " + sAckText);
                                        if (!string.IsNullOrEmpty(sAckText))
                                        {
                                            EdiDataSource oEdiDataSource = new EdiDataSource(oConfiguration.EdiDataSourceId);
                                            sAckText = oConfiguration.CreateAck(sAckText, fileLines);
                                            string myFileName = String.Format("{0}{1}", oConfiguration.VendorId + "997-SH-", DateTime.Now.ToString("yyyyMMddhhmmss") + ".txt");
                                            if (oConfiguration.PrependDunsInAckFileName)
                                            {
                                                myFileName = oConfiguration.VendorDunsNo + "-" + myFileName;
                                            }

                                            ACK_DIR = oEdiDataSource.Ack997LocalDirectory;
                                            SFTP_ACKDIR = oEdiDataSource.Ack997SftpDirectory;

                                            string myFullPath = Path.Combine(ACK_DIR, myFileName);
                                            Utility.CheckDirectoryAndCreate(ACK_DIR);

                                            Utility.CreateAndWriteFile(myFullPath, sAckText, false);

                                            if (File.Exists(myFullPath))
                                            {
                                                ErrorLog.createLog("myFullPath = " + myFullPath + " SFTP_ACKDIR = " + SFTP_ACKDIR + " myFileName =" + myFileName);
                                                try
                                                {
                                                    channelSftp.put(myFullPath, SFTP_ACKDIR + "/" + myFileName);
                                                    ErrorLog.createLog("SFTP ACK CREATED");
                                                }
                                                catch (Exception ex)
                                                { ErrorLog.createLog("SFTP ACK COPY FAIL"); }
                                            }

                                        }
                                    }
                                    else
                                    {
                                        ErrorLog.createLog("ACK 997 IS DISABLED");
                                    }

                                    // ACK LOGIC END HERE
                                    #endregion
                                }
                                catch (Exception ex) { ErrorLog.CreateLog(ex); }


                                channelSftp.rm(sourcePath + "/" + _file);
                            }
                            else
                            {
                                //channelSftp.put(SFTP_UNPROCESSED + "/" + _file);
                               // channelSftp.rm(sourcePath + "/" + _file);
                              //  System.IO.File.Copy(destinationPath + "/" + _file, Unprocessed_PATH + "/" + _file, true);
                              //  System.IO.File.Delete(destinationPath + "/" + _file);
                                ErrorLog.createLog("invalid file : " + sourcePath + "/" + _file);
                            }

                        }
                        catch (SftpException e)
                        {
                            ErrorLog.createLog("Error inside  GetAllFiles of sFtp class");
                            ErrorLog.createLog(e.ToString() + e.Message);
                        }

                    }
                }
            }
            catch (SftpException e)
            {
                ErrorLog.createLog("Error inside GetAllFiles main");
            }
        }


        public ArrayList GetFileList(string path)
        {
            ArrayList list = new ArrayList();
            foreach (ChannelSftp.LsEntry entry in channelSftp.ls(path))
            {
                list.Add(entry.getFilename().ToString());
                //list.Add(entry.ToString());
            }
            return list;
        }

        #region ProgressMonitor Implementation 
        public class MyProgressMonitor : SftpProgressMonitor
        {
            private long transferred = 0;
            private long total = 0;
            private int elapsed = -1;
            private Sftp m_sftp;
            private string src;
            private string dest;

            System.Timers.Timer timer;

            public MyProgressMonitor(Sftp sftp)
            {
                m_sftp = sftp;  
            }

            public override void init(int op, String src, String dest, long max)
            {
                this.src = src;
                this.dest = dest;
                this.elapsed = 0;
                this.total = max;
                timer = new System.Timers.Timer(1000);
                timer.Start();
                timer.Elapsed += new System.Timers.ElapsedEventHandler(timer_Elapsed);

                string note;
                if (op.Equals(GET))
                {
                    note = "Downloading " + System.IO.Path.GetFileName(src) + "...";
                }
                else
                {
                    note = "Uploading " + System.IO.Path.GetFileName(src) + "...";
                }
                //m_sftp.SendStartMessage(src, dest, (int)total, note);
                Console.WriteLine("Total : " + total + " " + note);
            }
            public override bool count(long c)
            {
                this.transferred += c;
                string note = ("Transfering... [Elapsed time: " + elapsed + "]");
                //m_sftp.SendProgressMessage(src, dest, (int)transferred, (int)total, note);
                Console.WriteLine("Transfered :" + transferred + " Total : " + total + " " + note);
                return true;
            }
            public override void end()
            {
                timer.Stop();
                timer.Dispose();
                string note = ("Done in " + elapsed + " seconds!");
                //m_sftp.SendEndMessage(src, dest, (int)transferred, (int)total, note);
                Console.WriteLine("Transfered :" + transferred + " Total : " + total + " " + note);
                transferred = 0;
                total = 0;
                elapsed = -1;
                src = null;
                dest = null;
            }

            private void timer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
            {
                this.elapsed++;
            }
        }
        #endregion ProgressMonitor Implementation 
    } 
}
