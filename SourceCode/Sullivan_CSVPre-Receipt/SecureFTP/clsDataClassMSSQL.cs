using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;

namespace FaxInbox
{
    class clsDataClassMSSQL
    {
        public SqlConnection dbConn = new SqlConnection();
public string dbConnectionString;
//Set the Connction string
public clsDataClassMSSQL()
{
	dbConnectionString = ConfigurationManager.AppSettings["cieTrade"];
	dbConn.ConnectionString = dbConnectionString;
}
//Open the database connection
public void OpenDatabaseConnection()
{
	if ((dbConn.State == System.Data.ConnectionState.Closed)) {
		//dbConnectionString = ConfigurationManager.ConnectionStrings("ConnectionStringMSSQL").ConnectionString
		dbConn.ConnectionString = dbConnectionString;
		dbConn.Open();
	}
}
//Close the connection
public void CloseDatabaseConnection()
{
	if ((dbConn.State == System.Data.ConnectionState.Open)) {
		dbConn.Dispose();
		dbConn.Close();
	}
}
//To get the Data Reader 
public SqlDataReader GetDataReader(string strSql)
{
	if ((dbConn.State == System.Data.ConnectionState.Closed)) {
		dbConn.Open();
	}
	SqlCommand dbCommand = new SqlCommand(strSql);
	dbCommand.Connection = dbConn;
	dbCommand.CommandTimeout = 600;
	SqlDataReader dbReader = dbCommand.ExecuteReader();
	return dbReader;
}
//To get the Data dbAdapter 
public SqlDataAdapter GetDataAdapter(string strSql)
{
	if ((dbConn.State == System.Data.ConnectionState.Closed)) {
		dbConn.Open();
	}
	SqlCommand dbCommand = new SqlCommand(strSql);
	dbCommand.Connection = dbConn;
	SqlDataAdapter dbAdapter = new SqlDataAdapter(dbCommand);
	return dbAdapter;
}

//To get the single value record
//public string GetScalarData(string strSql)
//{
//    if ((dbConn.State == System.Data.ConnectionState.Closed)) {
//        dbConn.Open();
//    }
//    SqlCommand dbCommand = new SqlCommand(strSql);
//    dbCommand.Connection = dbConn;
//    dbCommand.CommandTimeout = 500;
//    string strData = Convert.ToString(dbCommand.ExecuteScalar);
//    if ((dbConn.State == System.Data.ConnectionState.Open)) {
//        dbConn.Close();
//    }
//    dbCommand.Dispose();
//    return strData;
//}
//To Execute the query such as Update,Insert & Delete    
public Int32 SetData(string strSql)
{
	SqlCommand dbCommand = new SqlCommand(strSql);
	if ((dbConn.State == System.Data.ConnectionState.Closed)) {
		dbConn.Open();
	}

	dbCommand.Connection = dbConn;
	Int32 intCount = dbCommand.ExecuteNonQuery();
	if ((dbConn.State == System.Data.ConnectionState.Open)) {
		dbConn.Close();
	}
	dbCommand.Dispose();
	//dbConn.Dispose()
	return (intCount);
}
public SqlDataReader GetDataReader_SP(SqlCommand _Command)
{
	SqlDataReader drX = default(SqlDataReader);
	if ((dbConn.State == System.Data.ConnectionState.Closed)) {
		dbConn.Open();
	}
	_Command.Connection = dbConn;
	drX = _Command.ExecuteReader();
	_Command.Dispose();
	return drX;
}
//Returns Data Table
public DataTable GetDataTable(string strSql)
{
	if ((dbConn.State == System.Data.ConnectionState.Closed)) {
		dbConn.Open();
	}
	SqlCommand dbCommand = new SqlCommand(strSql);
	dbCommand.Connection = dbConn;
	DataSet ds = new DataSet();
	SqlDataAdapter da = new SqlDataAdapter();
	da.SelectCommand = dbCommand;
	da.Fill(ds);
	da.Dispose();
	if ((dbConn.State == System.Data.ConnectionState.Open)) {
		dbConn.Close();
	}
	return ds.Tables[0];
}

////To get the single value record
//public string GetScalar(string strSql)
//{
//    SqlCommand dbCommand = new SqlCommand(strSql);
//    dbCommand.Connection = dbConn;
//    string strData = Convert.ToString(dbCommand.ExecuteScalar);
//    dbCommand.Dispose();
//    return strData;
//}
////To get the single value record
//public byte[] GetScalarDataByte(string strSql)
//{
//    if ((dbConn.State == System.Data.ConnectionState.Closed)) {
//        dbConn.Open();
//    }
//    SqlCommand dbCommand = new SqlCommand(strSql);
//    dbCommand.Connection = dbConn;
//    dbCommand.CommandTimeout = 600;
//    byte[] strData = dbCommand.ExecuteScalar;
//    if ((dbConn.State == System.Data.ConnectionState.Open)) {
//        dbConn.Close();
//    }
//    dbCommand.Dispose();
//    return strData;
//}


    }
}
