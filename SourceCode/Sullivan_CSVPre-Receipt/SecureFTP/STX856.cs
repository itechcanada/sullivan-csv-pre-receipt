﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using iTECH.Library.DataAccess.ODBC;
using iTECH.Library.Utilities;
using System.Data;
using MySql.Data.MySqlClient;

namespace SecureFTP
{
    class STX856
    {

        StringBuilder sbSql = null;
        string m_sSql = string.Empty;
        static string sStratixConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ToString();
        static string sMysqlConnectionString = ConfigurationManager.ConnectionStrings["NewConnectionString"].ToString();

        private static string sXctiUserId = ConfigurationManager.AppSettings["xctiUserId"];
        private static string EdiFile = ConfigurationManager.AppSettings["EdiFile"];
        private static int m_iMaxInerchangeNo = BusinessUtility.GetInt(ConfigurationManager.AppSettings["MaxInerchangeNo"]);


        static string m_sCompanyId = ConfigurationManager.AppSettings["CompanyId"];
        static string m_sIntchgPfx = ConfigurationManager.AppSettings["IntchgPfx"];
        static string m_sCusVenId = ConfigurationManager.AppSettings["CusVenId"];
        static string m_sRcptType = ConfigurationManager.AppSettings["RcptType"];

        int m_iHeaderId = 0;
        int m_iFileId = 0;

        string sCusVenId = string.Empty;
        public string CusVenId
        {
            get { return sCusVenId; }
            set { sCusVenId = value; }
        }


        string sShipTo = string.Empty;
        public string ShipTo
        {
            get { return sShipTo; }
            set { sShipTo = value; }
        }

        string sReceiptDate = string.Empty;
        public string ReceiptDate
        {
            get { return sReceiptDate; }
            set { sReceiptDate = value; }
        }

        string sVenShpRef = string.Empty;
        public string VenShpRef
        {
            get { return sVenShpRef; }
            set { sVenShpRef = value; }
        }

        string sCarrierVehicleDesc = string.Empty;
        public string CarrierVehicleDesc
        {
            get { return sCarrierVehicleDesc; }
            set { sCarrierVehicleDesc = value; }
        }

        string sRcvWhs = string.Empty;
        public string RcvWhs
        {
            get { return sRcvWhs; }
            set { sRcvWhs = value; }
        }

        string sDeliveryMethod = string.Empty;
        public string DeliveryMethod
        {
            get { return sDeliveryMethod; }
            set { sDeliveryMethod = value; }
        }

        string sFreightVenId = string.Empty;
        public string FreightVenId
        {
            get { return sFreightVenId; }
            set { sFreightVenId = value; }
        }

        string sCarrierName = string.Empty;
        public string CarrierName
        {
            get { return sCarrierName; }
            set { sCarrierName = value; }
        }

        string sCarrierRefNo = string.Empty;
        public string CarrierRefNo
        {
            get { return sCarrierRefNo; }
            set { sCarrierRefNo = value; }
        }

        string sPreRcptStatus = string.Empty;
        public string PreRcptStatus
        {
            get { return sPreRcptStatus; }
            set { sPreRcptStatus = value; }
        }

        int iInterChangeNo = 0;
        public int InterChangeNo
        {
            get { return iInterChangeNo; }
            set { iInterChangeNo = value; }
        }

        string sCompanyId = string.Empty;
        public string CompanyId
        {
            get { return sCompanyId; }
            set { sCompanyId = value; }
        }

        string sIntchgPfx = string.Empty;
        public string IntchgPfx
        {
            get { return sIntchgPfx; }
            set { sIntchgPfx = value; }
        }

        string sRcptType = string.Empty;
        public string RcptType
        {
            get { return sRcptType; }
            set { sRcptType = value; }
        }

        string sNewRcptPfx = string.Empty;
        public string NewRcptPfx
        {
            get { return sNewRcptPfx; }
            set { sNewRcptPfx = value; }
        }

        string sNewRcptNo = "0";
        public string NewRcptNo
        {
            get { return sNewRcptNo; }
            set { sNewRcptNo = value; }
        }

        string sNcrPfx = string.Empty;
        public string NcrPfx
        {
            get { return sNcrPfx; }
            set { sNcrPfx = value; }
        }

        string sNcrNo = "0";
        public string NcrNo
        {
            get { return sNcrNo; }
            set { sNcrNo = value; }
        }

        string sFileNo = string.Empty;
        public string FileNo
        {
            get { return sFileNo; }
            set { sFileNo = value; }
        }

        string sCusVenShp = string.Empty;
        public string CusVenShp
        {
            get { return sCusVenShp; }
            set { sCusVenShp = value; }
        }

        string sIntchgItm = "0";
        public string IntchgItm
        {
            get { return sIntchgItm; }
            set { sIntchgItm = value; }
        }

        string sPrntPfx = "PO";
        public string PrntPfx
        {
            get { return sPrntPfx; }
            set { sPrntPfx = value; }
        }

        public STX856()
        {

        }

        public bool CheckConnection()
        {
            bool bReturn = false;
            DbHelper oDbHelper = new DbHelper(sStratixConnectionString, true);
            m_sSql = "SELECT (MAX(i53_intchg_no) + 1) AS i00_intchg_no   FROM xcti53_rec";

            try
            {
                object objIntChgNo = oDbHelper.GetValue(m_sSql, CommandType.Text);
                iInterChangeNo = BusinessUtility.GetInt(objIntChgNo);
                bReturn = iInterChangeNo > 0;
                ErrorLog.createLog("Connection successful");
            }
            catch (Exception ex)
            {
                ErrorLog.createLog( "Error in Check connection : " +ex.ToString());
                ErrorLog.CreateLog(ex);
                // EdiError.LogError(EdiFile, m_iFileId.ToString(), "", ex, "");
            }

            return bReturn;
        }

        public STX856(int iHeaderId)
        {
            m_iHeaderId = iHeaderId;
            iTECH.Library.DataAccess.MySql.DbHelper oDbHelper = new iTECH.Library.DataAccess.MySql.DbHelper(sMysqlConnectionString, true);
            sbSql = new StringBuilder();
            sbSql.Append("select * from header where HeaderId = '" + iHeaderId + "'");
            MySqlParameter[] oMySqlParameter = { iTECH.Library.DataAccess.MySql.DbUtility.GetParameter("HeaderId", iHeaderId, iTECH.Library.DataAccess.MySql.MyDbType.Int), };

            DataTable oDtAddress = new iTECH.Library.DataAccess.MySql.DbHelper(sMysqlConnectionString, true).GetDataTable(sbSql.ToString(), CommandType.Text, oMySqlParameter);
            foreach (DataRow oDataRow in oDtAddress.Rows)
            {
                // HeaderId, FileId, CusVenId, ShipTo, ReceiptDate, VenShpRef, CarrierVehicleDesc, RcvWhs, DeliveryMethod, FreightVenId, CarrierName, CarrierRefNo, PreRcptStatus
                sFileNo = oDataRow["FileId"].ToString();
                m_iFileId = BusinessUtility.GetInt(sFileNo);
                sCusVenId = oDataRow["CusVenId"].ToString();
                sShipTo = oDataRow["ShipTo"].ToString();
                sReceiptDate = oDataRow["ReceiptDate"].ToString();
                sVenShpRef = oDataRow["VenShpRef"].ToString();
                sCarrierVehicleDesc = oDataRow["CarrierVehicleDesc"].ToString();
                sRcvWhs = oDataRow["RcvWhs"].ToString();
                sDeliveryMethod = oDataRow["DeliveryMethod"].ToString();
                sFreightVenId = oDataRow["FreightVenId"].ToString();
                sCarrierName = oDataRow["CarrierName"].ToString();
                sCarrierRefNo = oDataRow["CarrierRefNo"].ToString();
                sPreRcptStatus = oDataRow["PreRcptStatus"].ToString();
                m_sCusVenId = sCusVenId;
            }

        }

        public void ProcessReceipt(int iFileId)
        {
            try
            {
                Header oHeader = new Header();
                DataTable dtHeader = oHeader.GetHeaderList(iFileId);
                for (int i = 0; i < dtHeader.Rows.Count; i++)
                {
                    oHeader.ReceiptCreated = BusinessUtility.GetBool(dtHeader.Rows[i]["ReceiptCreated"]);
                    oHeader.HeaderId = BusinessUtility.GetString(dtHeader.Rows[i]["HeaderId"]);
                    oHeader.FileId = BusinessUtility.GetString(dtHeader.Rows[i]["FileId"]);

                    m_iHeaderId = BusinessUtility.GetInt(oHeader.HeaderId);

                    if (m_iHeaderId > 0)
                    {
                        STX856 oSTX856 = new STX856(m_iHeaderId);
                        oSTX856.CompanyId = m_sCompanyId;
                        oSTX856.IntchgPfx = m_sIntchgPfx;
                        oSTX856.InterChangeNo = 0;
                        oSTX856.RcptType = m_sRcptType;
                        oSTX856.CusVenId = m_sCusVenId;
                        //if (string.IsNullOrEmpty(m_sShipFrom))
                        //{
                        //    m_sShipFrom = "0";       // implemented this change due to error in nlmk : Cannot insert a null into column (xcti53_rec.i53_cus_ven_shp)
                        //}
                        oSTX856.CusVenShp = "0";    // poh_shp_fm
                        oSTX856.StartInsert();

                        // ErrorLog.createLog("m_sShipFrom = " + m_sShipFrom);

                        oHeader.UpdateHeader(m_iHeaderId, BusinessUtility.GetInt(Configuration.EdiFileProcessingType.ONERECEIPTPERPO));

                        m_iHeaderId = 0;
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorLog.CreateLog(ex);
            }
            finally
            {

            }
        }

        public void StartInsert()
        {
            ErrorLog.createLog("Header ID : " + m_iHeaderId.ToString());
            Details oDetails = new Details();
            DataTable dtDetails = oDetails.GetDetails(m_iHeaderId);
            XCTI55 oXCTI55 = new XCTI55();
            DataTable dtDetailItem = oXCTI55.GetDetailItem(m_iHeaderId);
            ErrorLog.createLog("Detail  count : " + dtDetails.Rows.Count);
            ErrorLog.createLog("Detail Item count : " + dtDetailItem.Rows.Count);

            if (dtDetails.Rows.Count > 0 & dtDetailItem.Rows.Count > 0)
            {

                Files oFiles = new Files();
                oFiles.UpdatePoBsnForCurrentFile(m_iFileId);

                DbHelper oDbHelper = new DbHelper(sStratixConnectionString, true);
                m_sSql = "SELECT (MAX(i53_intchg_no) + 1) AS i00_intchg_no   FROM xcti53_rec";

                //try
                //{
                //    object objIntChgNo = oDbHelper.GetValue(m_sSql, CommandType.Text);
                //    iInterChangeNo = BusinessUtility.GetInt(objIntChgNo);
                //}
                //catch (Exception ex)
                //{
                //    ErrorLog.createLog(ex.ToString());
                //    EdiError.LogError(EdiFile, m_iFileId.ToString(), "", ex, "");
                //}

                iInterChangeNo = m_iMaxInerchangeNo + m_iFileId + m_iHeaderId - 1;  // new logic applied due to error "Could not do a physical-order read to fetch next row"
                ErrorLog.createLog("Interchange no created for file id " + m_iFileId + ", HeaderId = '" + m_iHeaderId + "'   :    " + iInterChangeNo);

                sbSql = new StringBuilder();
                // 2021/09/06   Informix to postgres - replace current with now()
                sbSql.Append(" insert into xcti53_rec (i53_cmpy_id, i53_intchg_pfx, i53_intchg_no, i53_rcpt_typ, i53_cus_ven_id, i53_cus_ven_shp, i53_rcpt_dt,");
                sbSql.Append(" i53_ven_shp_ref, i53_crr_vcl_desc, i53_rcvg_whs, i53_dlvy_mthd, i53_frt_ven_id, i53_crr_nm, i53_crr_ref_no, i53_prcpt_sts, ");
                sbSql.Append(" i53_new_rcpt_pfx, i53_new_rcpt_no, i53_ncr_pfx, i53_ncr_no) ");
                sbSql.Append(" VALUES ('" + sCompanyId + "', '" + sIntchgPfx + "', '" + iInterChangeNo + "', '" + sRcptType + "', '" + sCusVenId + "',");
                sbSql.Append(" '" + sCusVenShp + "', now(), '" + sVenShpRef + "', '" + sCarrierVehicleDesc + "', '" + sRcvWhs + "', '" + sDeliveryMethod + "',");
                sbSql.Append(" '" + sFreightVenId + "', '" + sCarrierName + "', '" + sCarrierRefNo + "','" + sPreRcptStatus + "', '" + sNewRcptPfx + "', '" + sNewRcptNo + "', '" + sNcrPfx + "','" + sNcrNo + "')");

                ErrorLog.createLog(BusinessUtility.GetString(sbSql));
                ErrorLog.createLog("sPreRcptStatus = " + sPreRcptStatus);

                int iStatus = 0;
                try
                {

                    iStatus = oDbHelper.ExecuteNonQuery(BusinessUtility.GetString(sbSql), CommandType.Text);
                    if (iStatus > 0)
                    {
                        iStatus = 0;

                        if (dtDetails.Rows.Count > 0 && dtDetailItem.Rows.Count > 0)
                        {
                            for (int i = 0; i < dtDetails.Rows.Count; i++)
                            {
                                oDetails.PrntNo = BusinessUtility.GetString(dtDetails.Rows[i]["PrntNo"]);
                                oDetails.PrntItm = BusinessUtility.GetString(dtDetails.Rows[i]["PrntItm"]);
                                oDetails.PrntSubItm = BusinessUtility.GetString(dtDetails.Rows[i]["PrntSubItm"]);
                                oDetails.CusRtnMthd = BusinessUtility.GetString(dtDetails.Rows[i]["CusRtnMthd"]);
                                oDetails.ShptNo = BusinessUtility.GetString(dtDetails.Rows[i]["ShptNo"]);
                                oDetails.ShptTagNo = BusinessUtility.GetString(dtDetails.Rows[i]["ShptTagNo"]);
                                oDetails.VenShptRef = BusinessUtility.GetString(dtDetails.Rows[i]["VenShptRef"]);
                                oDetails.OrigShpntPcs = BusinessUtility.GetString(dtDetails.Rows[i]["OrigShpntPcs"]);
                                oDetails.OrigShpntMsr = BusinessUtility.GetString(dtDetails.Rows[i]["OrigShpntMsr"]);
                                oDetails.OrigShptWgt = BusinessUtility.GetString(dtDetails.Rows[i]["OrigShptWgt"]);
                                oDetails.ShpGrsPcs = BusinessUtility.GetString(dtDetails.Rows[i]["ShpGrsPcs"]);
                                oDetails.ShpGrsMsr = BusinessUtility.GetString(dtDetails.Rows[i]["ShpGrsMsr"]);
                                oDetails.ShpGrsWgt = BusinessUtility.GetString(dtDetails.Rows[i]["ShpGrsWgt"]);
                                oDetails.Form = BusinessUtility.GetString(dtDetails.Rows[i]["Form"]);
                                oDetails.Grade = BusinessUtility.GetString(dtDetails.Rows[i]["Grade"]);
                                oDetails.Size = BusinessUtility.GetString(dtDetails.Rows[i]["Size"]);
                                oDetails.Finish = BusinessUtility.GetString(dtDetails.Rows[i]["Finish"]);
                                oDetails.ExtendedFinish = BusinessUtility.GetString(dtDetails.Rows[i]["ExtendedFinish"]);
                                oDetails.Width = BusinessUtility.GetString(dtDetails.Rows[i]["Width"]);
                                oDetails.Length = BusinessUtility.GetString(dtDetails.Rows[i]["Length"]);
                                oDetails.DimDsgn = BusinessUtility.GetString(dtDetails.Rows[i]["DimDsgn"]);
                                oDetails.InnerDia = BusinessUtility.GetString(dtDetails.Rows[i]["InnerDia"]);
                                oDetails.OuterDia = BusinessUtility.GetString(dtDetails.Rows[i]["OuterDia"]);
                                oDetails.WallThck = BusinessUtility.GetString(dtDetails.Rows[i]["WallThck"]);
                                oDetails.GaSize = BusinessUtility.GetString(dtDetails.Rows[i]["GaSize"]);
                                oDetails.GaType = BusinessUtility.GetString(dtDetails.Rows[i]["GaType"]);
                                oDetails.RndmDim1 = BusinessUtility.GetString(dtDetails.Rows[i]["RndmDim1"]);
                                oDetails.RndmDim2 = BusinessUtility.GetString(dtDetails.Rows[i]["RndmDim2"]);
                                oDetails.RndmDim3 = BusinessUtility.GetString(dtDetails.Rows[i]["RndmDim3"]);
                                oDetails.RndmDim4 = BusinessUtility.GetString(dtDetails.Rows[i]["RndmDim4"]);
                                oDetails.RndmDim5 = BusinessUtility.GetString(dtDetails.Rows[i]["RndmDim5"]);
                                oDetails.RndmDim6 = BusinessUtility.GetString(dtDetails.Rows[i]["RndmDim6"]);
                                oDetails.RndmDim7 = BusinessUtility.GetString(dtDetails.Rows[i]["RndmDim7"]);
                                oDetails.RndmDim8 = BusinessUtility.GetString(dtDetails.Rows[i]["RndmDim8"]);
                                oDetails.OfcRvw = BusinessUtility.GetString(dtDetails.Rows[i]["OfcRvw"]);
                                oDetails.SlsCat = BusinessUtility.GetString(dtDetails.Rows[i]["SlsCat"]);
                                oDetails.IntchgItm = BusinessUtility.GetString(dtDetails.Rows[i]["IntchgItm"]);

                                sbSql = new StringBuilder();
                                // informix to postgres - add column ,i54_bgt_as,i54_bgt_as_ven_id,i54_bgt_as_part,i54_part_cus_id,i54_part,i54_part_revno  2021/09/06
                                sbSql.Append(" insert into XCTI54_rec (i54_cmpy_id, i54_intchg_pfx, i54_intchg_no, i54_intchg_itm,i54_prnt_pfx, i54_prnt_no,i54_prnt_itm, ");
                                sbSql.Append(" i54_prnt_sitm, i54_cus_rtn_mthd, i54_shpt_no, i54_shp_tag_no, i54_ven_shp_ref, i54_orig_shpnt_pcs,i54_orig_shpnt_msr, ");
                                sbSql.Append(" i54_orig_shpnt_wgt, i54_shp_grs_pcs, i54_shp_grs_msr, i54_shp_grs_wgt,  i54_frm , i54_grd, i54_size, ");
                                sbSql.Append(" i54_fnsh, i54_ef_evar , i54_wdth,  i54_lgth , i54_dim_dsgn ,  i54_idia , i54_odia , i54_wthck, i54_ga_size, i54_ga_typ,  i54_rdm_dim_1 , i54_rdm_dim_2, ");
                                sbSql.Append(" i54_rdm_dim_3, i54_rdm_dim_4 , i54_rdm_dim_5 ,  i54_rdm_dim_6 , i54_rdm_dim_7 , i54_rdm_dim_8 , i54_ofc_rvw , i54_new_rcpt_pfx, i54_new_rcpt_no, ");
                                sbSql.Append(" i54_new_rcpt_itm, i54_sls_cat , i54_ncr_pfx, i54_ncr_no, i54_ncr_itm,i54_bgt_as,i54_bgt_as_ven_id,i54_bgt_as_part,i54_part_cus_id,i54_part,i54_part_revno)  ");
                                sbSql.Append(" values('" + sCompanyId + "', '" + sIntchgPfx + "', '" + iInterChangeNo + "', '" + oDetails.IntchgItm + "','" + sPrntPfx + "', '" + oDetails.PrntNo + "','" + oDetails.PrntItm + "', ");
                                sbSql.Append(" " + oDetails.PrntSubItm + ", '" + oDetails.CusRtnMthd + "', '" + oDetails.ShptNo + "', '', '" + oDetails.VenShptRef + "', '" + oDetails.OrigShpntPcs + "','" + oDetails.OrigShpntMsr + "', ");
                                sbSql.Append(" '" + oDetails.OrigShptWgt + "', '" + oDetails.ShpGrsPcs + "', '" + oDetails.ShpGrsMsr + "', '" + oDetails.ShpGrsWgt + "',  '" + oDetails.Form + "' , '" + oDetails.Grade + "', '" + oDetails.Size + "', ");
                                sbSql.Append(" '" + oDetails.Finish + "', '" + oDetails.ExtendedFinish + "' , '" + oDetails.Width + "',  '" + oDetails.Length + "' , '" + oDetails.DimDsgn + "' ,  '" + oDetails.InnerDia + "' , '" + oDetails.OuterDia + "' , '" + oDetails.WallThck + "', '" + oDetails.GaSize + "', '" + oDetails.GaType + "',  '" + oDetails.RndmDim1 + "' , '" + oDetails.RndmDim2 + "',");
                                sbSql.Append(" '" + oDetails.RndmDim3 + "', '" + oDetails.RndmDim4 + "' , '" + oDetails.RndmDim5 + "' ,  '" + oDetails.RndmDim6 + "' , '" + oDetails.RndmDim7 + "' , '" + oDetails.RndmDim8 + "' , '" + oDetails.OfcRvw + "' , '0', '0',  ");
                                sbSql.Append(" '0', '0' , '0', '0', '0','0','','','','','') ");

                                ErrorLog.createLog(BusinessUtility.GetString(sbSql));
                                iStatus = oDbHelper.ExecuteNonQuery(BusinessUtility.GetString(sbSql), CommandType.Text);


                                int iHdrId = BusinessUtility.GetInt(dtDetails.Rows[i]["HeaderId"]);
                                int iDtlid = BusinessUtility.GetInt(dtDetails.Rows[i]["DetailId"]);
                                ErrorLog.createLog("iHdrId = " + iHdrId + " iDtlid = " + iDtlid);
                                dtDetailItem = oXCTI55.GetDetailItem(iHdrId, iDtlid);
                                ErrorLog.createLog("dtDetailItem count inside loop = " + dtDetailItem.Rows.Count);

                                for (int ii = 0; ii < dtDetailItem.Rows.Count; ii++)
                                {
                                    oXCTI55.InvtWgt = BusinessUtility.GetString(dtDetailItem.Rows[ii]["InvtWgt"]);
                                    oXCTI55.InvtPcs = BusinessUtility.GetString(dtDetailItem.Rows[ii]["InvtPcs"]);
                                    oXCTI55.InvtMsr = BusinessUtility.GetString(dtDetailItem.Rows[ii]["InvtMsr"]);
                                    oXCTI55.CmpyId = sCompanyId;
                                    oXCTI55.IntchgPfx = sIntchgPfx;
                                    oXCTI55.IntchgNo = iInterChangeNo.ToString();
                                    oXCTI55.IntchgItm = BusinessUtility.GetString(dtDetailItem.Rows[ii]["IntchgItm"]);
                                    oXCTI55.IntchgSitm = BusinessUtility.GetString(dtDetailItem.Rows[ii]["IntchgSitm"]);
                                    oXCTI55.Form = oDetails.Form;
                                    oXCTI55.Grid = oDetails.Grade;
                                    oXCTI55.Size = oDetails.Size;
                                    oXCTI55.Finish = oDetails.Finish;
                                    oXCTI55.EfEver = oDetails.ExtendedFinish;
                                    oXCTI55.Length = BusinessUtility.GetString(dtDetailItem.Rows[ii]["Length"]);
                                    oXCTI55.DimDsgn = oDetails.DimDsgn;
                                    oXCTI55.RjctRsn = BusinessUtility.GetString(dtDetailItem.Rows[ii]["RjctRsn"]);
                                    oXCTI55.InvtPcsTyp = BusinessUtility.GetString(dtDetailItem.Rows[ii]["InvtPcsTyp"]);
                                    oXCTI55.InvtMsrTyp = BusinessUtility.GetString(dtDetailItem.Rows[ii]["InvtMsrTyp"]);
                                    oXCTI55.InvtWgtTyp = BusinessUtility.GetString(dtDetailItem.Rows[ii]["InvtWgtTyp"]);
                                    oXCTI55.ShpntPcs = BusinessUtility.GetString(dtDetailItem.Rows[ii]["ShpntPcs"]);
                                    // oXCTI55.ShpntMsr = BusinessUtility.GetString(ConfigurationManager.AppSettings["ShpntMsr"]);

                                    oXCTI55.ShpntMsr = BusinessUtility.GetString(dtDetailItem.Rows[ii]["ShpntMsr"]);

                                    // oXCTI55.ShpntWgt = oDetails.OrigShptWgt;
                                    oXCTI55.ShpntWgt = BusinessUtility.GetString(dtDetailItem.Rows[ii]["ShpntWgt"]);

                                    oXCTI55.ShpntPcsTyp = BusinessUtility.GetString(dtDetailItem.Rows[ii]["ShpntPcsTyp"]);
                                    oXCTI55.ShpntMsrTyp = BusinessUtility.GetString(dtDetailItem.Rows[ii]["ShpntMsrTyp"]);
                                    oXCTI55.ShpntWgtTyp = BusinessUtility.GetString(dtDetailItem.Rows[ii]["ShpntWgtTyp"]);
                                    oXCTI55.Heat = BusinessUtility.GetString(dtDetailItem.Rows[ii]["Heat"]);
                                    oXCTI55.GaType = oDetails.GaType;
                                    oXCTI55.GaSize = oDetails.GaSize;
                                    oXCTI55.DimDsgn = oDetails.DimDsgn;
                                    oXCTI55.Loc = BusinessUtility.GetString(dtDetailItem.Rows[ii]["Loc"]);
                                    oXCTI55.Mill = BusinessUtility.GetString(dtDetailItem.Rows[ii]["Mill"]);

                                    oXCTI55.Width = oDetails.Width;
                                    oXCTI55.Pkg = BusinessUtility.GetString(dtDetailItem.Rows[ii]["Pkg"]);

                                    oXCTI55.InvtQlty = BusinessUtility.GetString(dtDetailItem.Rows[ii]["InvtQlty"]);
                                    oXCTI55.InvtType = BusinessUtility.GetString(dtDetailItem.Rows[ii]["InvtType"]);


                                    oXCTI55.CoilLengthTyp = BusinessUtility.GetString(dtDetailItem.Rows[ii]["CoilLengthTyp"]); ;
                                    oXCTI55.CoilLength = BusinessUtility.GetString(dtDetailItem.Rows[ii]["CoilLength"]);
                                    oXCTI55.ActGa1 = BusinessUtility.GetString(dtDetailItem.Rows[ii]["ActGa1"]);

                                    oXCTI55.I55Id = BusinessUtility.GetString(dtDetailItem.Rows[ii]["I55Id"]);
                                    oXCTI55.I55Od = BusinessUtility.GetString(dtDetailItem.Rows[ii]["I55Od"]);

                                    oXCTI55.MillId = BusinessUtility.GetString(dtDetailItem.Rows[ii]["MillId"]);

                                    oXCTI55.CutNo = BusinessUtility.GetString(dtDetailItem.Rows[ii]["CutNo"]);

                                    oXCTI55.TagNo = BusinessUtility.GetString(dtDetailItem.Rows[ii]["TagNo"]);
                                    //  oXCTI55.ActGa2 = ".0123";
                                    //oXCTI55.I55Id = "0";
                                    //oXCTI55.I55Od = "0";
                                    // detail item insertion here..
                                    //oXCTI55.Length = "5";
                                    oXCTI55.InsertDetailItemsInInfomix(oXCTI55, oDbHelper);
                                    EdiError.ValidateFormGrdSize(oXCTI55.Form, oXCTI55.Grid, oXCTI55.Size, "INSIDE StartInsert", BusinessUtility.GetString(Globalcl.FileId));

                                }
                            }
                            if (iStatus > 0)
                            {
                                iStatus = 0;
                                sbSql = new StringBuilder();
                                // 2021/09/06   Informix to postgres - replace current with now()
                                sbSql.Append(" insert into XCTI52_rec (i52_cmpy_id, i52_intchg_pfx, i52_intchg_no, i52_prs_md, i52_rcpt_pfx, i52_rcpt_no, i52_rcpt_itm ");
                                sbSql.Append(" , i52_itm_ctl_no, i52_tag_no, i52_prd_chg, i52_cond_chg, i52_hld_chg, i52_instr_chg, i52_whs_ofc ");
                                sbSql.Append(" , i52_cnv_rcpt_no, i52_crtd_dtts, i52_crtd_dtms, i52_upd_dtts,i52_upd_dtms ");
                                sbSql.Append(" ) ");
                                sbSql.Append(" values ('" + sCompanyId + "', '" + sIntchgPfx + "', '" + iInterChangeNo + "', 'A', 'PR', 0, 0, '0', '', 0, 0, 0, 0, 'W', 0, now(), '1', now(),'1') ");

                                ErrorLog.createLog(BusinessUtility.GetString(sbSql));

                                iStatus = oDbHelper.ExecuteNonQuery(BusinessUtility.GetString(sbSql), CommandType.Text);

                                if (iStatus > 0)
                                {
                                    //select * from mxrusr_rec    // i00_usr_id came from this table
                                    iStatus = 0;
                                    sbSql = new StringBuilder();
                                    // 2021/09/06   Informix to postgres - replace current with now()
                                    sbSql.Append(" INSERT INTO xcti00_rec (i00_cmpy_id,i00_intchg_pfx,i00_intchg_no,i00_evnt,i00_usr_id,i00_crtd_dtts,i00_crtd_dtms,i00_upd_dtts,i00_upd_dtms, ");
                                    sbSql.Append(" i00_sts_cd,i00_ssn_log_ctl_no,i00_intrf_cl) VALUES ('" + sCompanyId + "','" + sIntchgPfx + "','" + iInterChangeNo + "','PRC','" + sXctiUserId + "',now(),0,now(),0,'N',0,'E') ");
                                    ErrorLog.createLog(BusinessUtility.GetString(sbSql));
                                    iStatus = oDbHelper.ExecuteNonQuery(BusinessUtility.GetString(sbSql), CommandType.Text);

                                    ErrorLog.createLog(BusinessUtility.GetString(sbSql));

                                    if (iStatus > 0)
                                    {
                                        //   sbSql = new StringBuilder();
                                        //sbSql.Append("update  files SET IntchgNo = '" + iInterChangeNo + "' where FileId ='" + m_iFileId + "'");
                                        //iStatus = oMysql.ExecuteNonQuery(BusinessUtility.GetString(sbSql), CommandType.Text, null);


                                        int iReceiptNo = 0;
                                        //sbSql = new StringBuilder();
                                        //sbSql.Append("select i53_new_rcpt_no from xcti53_rec where i53_intchg_no = '" + iInterChangeNo + "' ");
                                        //object oRcptNo = oDbHelper.GetValue(sbSql.ToString(), CommandType.Text);
                                        //iReceiptNo = BusinessUtility.GetInt(oRcptNo);

                                        ErrorLog.createLog("iReceiptNo =" + iReceiptNo);

                                        //ALTER TABLE `header` ADD COLUMN `IntchgNo` INT NULL;
                                        sbSql = new StringBuilder();
                                        sbSql.Append(" UPDATE header SET  IntchgNo = '" + iInterChangeNo + "'  WHERE HeaderId = '" + m_iHeaderId + "' ");
                                        iTECH.Library.DataAccess.MySql.DbHelper oDbHelper1 = new iTECH.Library.DataAccess.MySql.DbHelper(sMysqlConnectionString, true);
                                        iStatus = oDbHelper1.ExecuteNonQuery(BusinessUtility.GetString(sbSql), CommandType.Text, null);


                                        sbSql = new StringBuilder();
                                        sbSql.Append(" UPDATE detail SET  IntchgNo = '" + iInterChangeNo + "'  WHERE HeaderId = '" + m_iHeaderId + "' ");
                                        iStatus = oDbHelper1.ExecuteNonQuery(BusinessUtility.GetString(sbSql), CommandType.Text, null);

                                        // code commented  2020/06/03  receipt number will fetch on next run
                                        //Console.WriteLine("Waiting for PR Number from Stratix");
                                        //ErrorLog.createLog("Waiting for PR Number from Stratix");
                                        //System.Threading.Thread.Sleep(Convert.ToInt32(Utility.GetConfigValue("PRPause")));        //wait for 10 sec to check pre receipt number from stratix   2020/04/30  Sumit
                                        //Files ofiles = new Files();
                                        //iReceiptNo = ofiles.GetReceiptNumber(Convert.ToString(iInterChangeNo));


                                        ErrorLog.createLog("iReceiptNo =" + iReceiptNo);
                                        Console.WriteLine("iReceiptNo =" + iReceiptNo);


                                        //Update File status 
                                        sbSql = new StringBuilder();
                                        sbSql.Append(" UPDATE files SET  Status = 1 , IntchgNo = '" + iInterChangeNo + "' , ReceiptNo = '" + iReceiptNo + "'  WHERE FileId = '" + m_iFileId + "' ");
                                        iStatus = oDbHelper1.ExecuteNonQuery(BusinessUtility.GetString(sbSql), CommandType.Text, null);
                                        ErrorLog.createLog("sbSql =" + sbSql.ToString());

                                        #region To get and update receipt Number    2020/04/30  Sumit   code commented  2020/06/03  receipt number will fetch on next run
                                        //if (iReceiptNo > 0)
                                        //{
                                        //    Console.WriteLine("Updating header id : " + m_iHeaderId + " With Receipt no : " + iReceiptNo);

                                        //    m_sSql = "update detail set ReceiptNo = '" + iReceiptNo + "' , IsReceiptUpdated = 1 where HeaderId = '" + m_iHeaderId + "'";
                                        //    ErrorLog.createLog(m_sSql);
                                        //    oDbHelper1.ExecuteNonQuery(m_sSql, CommandType.Text, null);

                                        //    //adding mail notification code     2020/04/30  Sumit
                                        //    if (BusinessUtility.GetInt(Utility.GetConfigValue("isPRNotification")) == 1)
                                        //    {
                                        //        MailNotify.MailNotifyManager mailNotifyManager = new MailNotify.MailNotifyManager();
                                        //        mailNotifyManager.poNumber = oDetails.PrntNo;
                                        //        mailNotifyManager.poItem = BusinessUtility.GetInt(oDetails.PrntItm);
                                        //        mailNotifyManager.NotificationStatus = "OnSuccess";
                                        //        //mailNotifyManager.prNumber = Convert.ToString(iReceiptNo);
                                        //        mailNotifyManager.NotificationMessage = "PR Number: " + iReceiptNo;

                                        //        mailNotifyManager.SendNotification();
                                        //    }
                                        //}
                                        #endregion To get and update receipt Number 2020/04/30  Sumit

                                        oDbHelper1.CloseDatabaseConnection();

                                        ErrorLog.createLog("Successfully created pre receipt ");
                                        Console.WriteLine("Successfully created pre receipt");
                                    }
                                }
                            }



                        }
                    }
                }
                catch (Exception ex)
                {
                    ErrorLog.createLog("Error while creating pre receipt.");
                    ErrorLog.createLog(ex.ToString());
                    EdiError.LogError(EdiFile, m_iFileId.ToString(), "", ex, "");
                    EdiError.LogError(EdiFile, m_iFileId.ToString(), ConfigurationManager.AppSettings["StratixError856"]);
                    ErrorLog.CreateLog(ex);
                }
                finally
                {
                    oDbHelper.CloseDatabaseConnection();
                }

            }
            else
            {
                ErrorLog.createLog("Detail data is empty so omiting creating receipt.");
            }

        } // start insert end here..




    }
}
