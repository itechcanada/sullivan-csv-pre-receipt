﻿using iTECH.Library.Utilities;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Odbc;
using System.Linq;
using System.Text;

namespace CsxShortDescrptionUpdate
{
    static class StratixActivity
    {
		//<add key="IBPSConnectionString" value="DRIVER={PostgreSQL ANSI};Host=10.1.1.30;server=10.1.1.30;uid=obsodbc;pwd=obsodbc;database=trnobsdb;PORT=12204;" />
        //public static DataTable GetData(StringBuilder sbSql)
        //{
        //    string Error = String.Empty;
        //    DataTable dt = new DataTable();
        //    using (IBM.Data.Informix.IfxConnection conn = new IfxConnection(ConfigurationManager.ConnectionStrings["DSNConnection"].ConnectionString))
        //    {

        //        ErrorLog.createLog(BusinessUtility.GetString(sbSql));
        //        try
        //        {
        //            IfxCommand query = new IfxCommand(sbSql.ToString());
        //            IBM.Data.Informix.IfxDataAdapter adapter = new IfxDataAdapter();
        //            query.Connection = conn;
        //            query.CommandTimeout = BusinessUtility.GetInt(ConfigurationManager.AppSettings["CommandTimeout"]);
        //            conn.Open();
        //            adapter.SelectCommand = new IfxCommand("SET ISOLATION TO DIRTY READ", conn);
        //            adapter.SelectCommand.ExecuteNonQuery(); //Tells the program to wait in case of a lock.     
        //            adapter.SelectCommand = query;
        //            adapter.Fill(dt);
        //            conn.Close();
        //            adapter.Dispose();
        //        }
        //        catch (Exception ex)
        //        {
        //            ErrorLog.createLog("Error in GetFileError");
        //            ErrorLog.CreateLog(ex);
        //            throw;
        //        }
        //    }



        //    return dt;
        //}


        public static DataTable GetData(StringBuilder sbSql)
        {
            string Error = String.Empty;
            DataTable dt = new DataTable();
            ErrorLog.createLog(BusinessUtility.GetString(sbSql));
            try
            {
                
                using (OdbcConnection odbccon = new OdbcConnection(ConfigurationManager.ConnectionStrings["DSNConnection"].ConnectionString))
                {
                    OdbcCommand odbccmd = new OdbcCommand(sbSql.ToString());
                    odbccmd.CommandType = CommandType.Text;
                    odbccmd.Connection = odbccon;
                    //  odbccmd.CommandTimeout = BusinessUtility.GetInt(ConfigurationManager.AppSettings["CommandTimeout"]);
                    odbccon.Open();
                    OdbcDataAdapter odbcda = new OdbcDataAdapter(odbccmd);
                    odbcda.Fill(dt);
                    odbccon.Close();
                    odbcda.Dispose();
                }
               
            }
            catch (Exception ex)
            {
                ErrorLog.createLog("Error in StratixActivity  GetData");
                ErrorLog.createLog(ex.ToString());
                throw;
            }

            finally
            {

            }


            return dt;
        }




        public static string GetStrVal(StringBuilder sbSql)
        {
            string strval = string.Empty;
            ErrorLog.createLog(BusinessUtility.GetString(sbSql));
            try
            {

                using (OdbcConnection odbccon = new OdbcConnection(ConfigurationManager.ConnectionStrings["DSNConnection"].ConnectionString))
                {
                    OdbcCommand odbccmd = new OdbcCommand(sbSql.ToString());
                    odbccmd.CommandType = CommandType.Text;
                    odbccmd.Connection = odbccon;
                    //  odbccmd.CommandTimeout = BusinessUtility.GetInt(ConfigurationManager.AppSettings["CommandTimeout"]);
                    odbccon.Open();
                    strval = BusinessUtility.GetString(odbccmd.ExecuteScalar());
                    odbccon.Close();
                }

            }
            catch (Exception ex)
            {
                ErrorLog.createLog("Error in StratixActivity  GetData");
                ErrorLog.createLog(ex.ToString());
                throw;
            }

            finally
            {

            }


            return strval;
        }




        public static int GetIntVal(StringBuilder sbSql)
        {
            int intval = 0;
            ErrorLog.createLog(BusinessUtility.GetString(sbSql));
            try
            {

                using (OdbcConnection odbccon = new OdbcConnection(ConfigurationManager.ConnectionStrings["DSNConnection"].ConnectionString))
                {
                    OdbcCommand odbccmd = new OdbcCommand(sbSql.ToString());
                    odbccmd.CommandType = CommandType.Text;
                    odbccmd.Connection = odbccon;
                    //  odbccmd.CommandTimeout = BusinessUtility.GetInt(ConfigurationManager.AppSettings["CommandTimeout"]);
                    odbccon.Open();
                    intval = BusinessUtility.GetInt(odbccmd.ExecuteScalar());
                    odbccon.Close();
                }

            }
            catch (Exception ex)
            {
                ErrorLog.createLog("Error in StratixActivity  GetData");
                ErrorLog.createLog(ex.ToString());
                throw;
            }

            finally
            {

            }


            return intval;
        }


        public static decimal GetDecimalVal(StringBuilder sbSql)
        {
            Decimal decimalval = BusinessUtility.GetDecimal(0);
            ErrorLog.createLog(BusinessUtility.GetString(sbSql));
            try
            {

                using (OdbcConnection odbccon = new OdbcConnection(ConfigurationManager.ConnectionStrings["DSNConnection"].ConnectionString))
                {
                    OdbcCommand odbccmd = new OdbcCommand(sbSql.ToString());
                    odbccmd.CommandType = CommandType.Text;
                    odbccmd.Connection = odbccon;
                    //  odbccmd.CommandTimeout = BusinessUtility.GetInt(ConfigurationManager.AppSettings["CommandTimeout"]);
                    odbccon.Open();
                    decimalval = BusinessUtility.GetDecimal(odbccmd.ExecuteScalar());
                    odbccon.Close();
                }

            }
            catch (Exception ex)
            {
                ErrorLog.createLog("Error in StratixActivity  GetData");
                ErrorLog.createLog(ex.ToString());
                throw;
            }

            finally
            {

            }


            return decimalval;
        }

        public static void  InsertData(StringBuilder sbSql)
        {
            ErrorLog.createLog(BusinessUtility.GetString(sbSql));
            
            try
            {

                using (OdbcConnection odbccon = new OdbcConnection(ConfigurationManager.ConnectionStrings["DSNConnection"].ConnectionString))
                {
                    OdbcCommand odbccmd = new OdbcCommand(sbSql.ToString());
                    odbccmd.Connection = odbccon;
                    odbccmd.CommandType = CommandType.Text;
                    //  odbccmd.CommandTimeout = BusinessUtility.GetInt(ConfigurationManager.AppSettings["CommandTimeout"]);
                    odbccon.Open();
                    odbccmd.ExecuteNonQuery();
                    odbccon.Close();
                }

            }
            catch (Exception ex)
            {
                ErrorLog.createLog("Error in StratixActivity InsertData");
                ErrorLog.createLog(ex.ToString());
                throw;
            }

            finally
            {

            }


            
        }


        public static void  UpdateData(StringBuilder sbSql)
        {
            ErrorLog.createLog(BusinessUtility.GetString(sbSql));
            try
            {

                using (OdbcConnection odbccon = new OdbcConnection(ConfigurationManager.ConnectionStrings["DSNConnection"].ConnectionString))
                {
                    OdbcCommand odbccmd = new OdbcCommand(sbSql.ToString());
                    odbccmd.Connection = odbccon;
                    odbccmd.CommandType = CommandType.Text;
                    //  odbccmd.CommandTimeout = BusinessUtility.GetInt(ConfigurationManager.AppSettings["CommandTimeout"]);
                    odbccon.Open();
                    odbccmd.ExecuteNonQuery();
                    odbccon.Close();
                }

            }
            catch (Exception ex)
            {
                ErrorLog.createLog("Error in StratixActivity UpdateData");
                ErrorLog.createLog(ex.ToString());
                throw;
            }

            finally
            {

            }


        }

    }
}
